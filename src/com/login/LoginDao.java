package com.login;

import com.pasmtAdmin.user.UserBean;

public interface LoginDao {
	public UserBean loadUserDetails(String username);
}
