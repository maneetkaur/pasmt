package com.login;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.pasmtAdmin.user.UserBean;

@Component
public class UserDetailsServiceImpl implements UserDetailsService{
	
	@Autowired
	LoginDao loginDao;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		LoginBean login = null;
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		if(!"".equals(username)){
			UserBean employee = loginDao.loadUserDetails(username);
			if(null != employee){
				if(null != employee.getAccessRole()){
					authorities.add(new GrantedAuthorityImpl(employee.getAccessRole()));
					
					login = new LoginBean(username, employee.getPassword(), true, true, true, true, authorities, 
							employee.getUserId(), employee.getEmployeeName());
				}
				else{
					login = new LoginBean(username, "NA", true, true, true, true, authorities, null, null);
				}
			}
			else{
				login = new LoginBean(username, "NA", true, true, true, true, authorities, null, null);
			}
		}
		return login;
	}

}
