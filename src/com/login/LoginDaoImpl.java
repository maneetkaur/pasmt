package com.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.pasmtAdmin.user.UserBean;

@Repository
public class LoginDaoImpl implements LoginDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public UserBean loadUserDetails(String username) {
		UserBean employee = new UserBean();
		String sql = "SELECT employee_id, employee_name, username, password FROM pasmt_employee WHERE username='" + 
				username + "' AND status=1";
		List<UserBean> employeeList = jdbcTemplate.query(sql, new RowMapper<UserBean>(){

			@Override
			public UserBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				UserBean tempEmployee = new UserBean();
				tempEmployee.setUserId(rs.getString("employee_id"));
				tempEmployee.setEmployeeName(rs.getString("employee_name"));
				tempEmployee.setUsername(rs.getString("username"));
				tempEmployee.setPassword(rs.getString("password"));
				return tempEmployee;
			}			
		});
		if(null != employeeList && employeeList.size()>0 && null != employeeList.get(0)){
			employee = employeeList.get(0);
			sql = "SELECT lgv.generic_value FROM xref_employee_role er JOIN lu_generic_values lgv ON " +
					"er.role_id = lgv.generic_value_id WHERE er.employee_id=" + employee.getUserId();
			List<String> roleList = jdbcTemplate.query(sql, new RowMapper<String>(){

				@Override
				public String mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					return rs.getString("lgv.generic_value");					
				}
				
			});
			
			if(null != roleList && roleList.size()>0 && null != roleList.get(0)){
				employee.setAccessRole(roleList.get(0));
			}
		}
		return employee;
	}
	
	
}
