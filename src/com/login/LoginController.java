package com.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class LoginController {
	
	@Autowired
	LoginDao loginDao;
	
	static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	
	/* Default Mapping. When the website is hit for the first time, the controller redirects to the login page 
	 * by default using web.xml*/
	@RequestMapping(value="/welcome", method=RequestMethod.GET)
	public String welcome(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		LoginBean login = (LoginBean)auth.getPrincipal();
		LOG.info(login.getEmployeeName() + " has logged in.");
		return "login/welcome";
	}
	
	@RequestMapping(value="/accessDenied")
	public String accessDenied(){
		return "login/accessDenied";
	}

}