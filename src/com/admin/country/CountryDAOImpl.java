package com.admin.country;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Repository
@Transactional
public class CountryDAOImpl implements CountryDAO
{
	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	public List listCountry(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		List<CountryBean> countryList=new ArrayList<CountryBean>();
		String sql="";
		try
			{
			sb.append("where ");
			if(session.getAttribute("CountryBean")!=null)
			{
			CountryBean CountryBean=(CountryBean)session.getAttribute("CountryBean");	
				if(CountryBean.getCountry()!=null)
	 		   	{
		 			if(!CountryBean.getCountry().equals(""))
		 			{
		 				sb.append("country_name like '%"+CountryBean.getCountry()+"%'").append(" and ");
		 				
		 			}
	 		   }	
				if(CountryBean.getStatus()!=null)
	 		   	{
		 			if(!CountryBean.getStatus().equals("NONE"))
		 			{
		 				sb.append("is_active_pasmt='"+CountryBean.getStatus()+"'").append(" and ");
		 				
		 			}
	 		   }
	 		   if(CountryBean.getCountryCode()!=null)
	 		   {
		 			if(!CountryBean.getCountryCode().equals(""))
		 			{
		 				sb.append(" country_code like '%"+CountryBean.getCountryCode()+"%'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select country_id,country_name,country_code,is_active_pasmt from lu_country ";
	 			qrylast="order by is_active_pasmt, country_name limit "+startIndex+ "," +range;
	 			
				sql=qryStartPart+qryPart+qrylast; 
				System.out.println("country list sql-->"+sql);
				countryList=jdbcTemplate.query(sql,new CountryRowMapper());
				
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_country ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in listCountry "+e.toString());
			}
		
		session.removeAttribute("CountryBean");
		return countryList;
	}
	
	public String add(CountryBean CountryBean)
	{
		String msg="error";
		String sql="";
		if(CountryBean.getCountryId()!=null)
		{
			sql="update lu_country set country_name = '"+CountryBean.getCountry()+"',country_code ='"+CountryBean.getCountryCode()+"' where country_id ="+CountryBean.getCountryId();
			//jdbcTemplate.update(query);
		}
		else
		{
			 sql="insert into lu_country(country_name,country_code) values(?,?)";	
		}
		try{
			jdbcTemplate.update(sql, new Object[]{
					CountryBean.getCountry(),CountryBean.getCountryCode()});
			msg="success";
		}
		catch(Exception e)
		{
			System.out.println("exception in add() in country--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg; 
	}
	
	public String updateStatus(HttpServletRequest request)
	{

		List<CountryBean> list=new ArrayList<CountryBean>();
		String sql="select is_active_pasmt from lu_country where country_id='"+request.getParameter("countryId")+"'";
		//System.out.println("query "+sql);
		try
		{
			list=jdbcTemplate.query(sql, new RowMapper()
			{
				public CountryBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					CountryBean bean=new CountryBean();
					bean.setStatus(rs.getString(1));
					return bean;
					
				}
			});
			if(Integer.parseInt(list.get(0).getStatus().toString())==1)
			{
				jdbcTemplate.update("update lu_country set is_active_pasmt='2' where country_id='"+request.getParameter("countryId")+"'");
			}
			else if(Integer.parseInt(list.get(0).getStatus().toString())==3)
			{
				jdbcTemplate.update("update lu_country set is_active_pasmt='1' where country_id='"+request.getParameter("countryId")+"'");
			}
			else
			{
				jdbcTemplate.update("update lu_country set is_active_pasmt='1' where country_id='"+request.getParameter("countryId")+"'");
			}
		}
		catch(Exception e)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return "Success";
	}
	public CountryBean checkCountry(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		CountryBean bean=new CountryBean();
		List<CountryBean> list=new ArrayList<>();
		String id=request.getParameter("countryId");
		String sql="select country_id,country_name,country_code,is_active_pasmt from lu_country where country_id='"+id+"'";
		list=jdbcTemplate.query(sql, new CountryRowMapper());
		bean=list.get(0);
		session.setAttribute("countryId",id);
		return bean;
	}
	
	public void editCountry(String countryId,CountryBean CountryBean)
	{
		String sql="update lu_country set country_name='"+CountryBean.getCountry()+"',country_code='"+CountryBean.getCountryCode()+"' where country_id='"+countryId+"'";
		jdbcTemplate.update(sql);
	}
	
	public String validateCountryName(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select * from country where country_name='"+request.getParameter("Country")+"'";
			List<CountryBean> list=jdbcTemplate.query(sql, new CountryRowMapper());
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateCountryName() "+e.toString());	
		}		
		return target;
	}
	public String validateCountryCode(HttpServletRequest request)
	{
		String target="";
		
		try
		{
			String sql="select * from country where country_code='"+request.getParameter("CountryCode")+"'";
			List<CountryBean> list=jdbcTemplate.query(sql,new CountryRowMapper());
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
				
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateCountryCode() "+e.toString());	
		}		
		return target;
	}
	
}
