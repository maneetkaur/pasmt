package com.admin.country;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;


@Controller
public class CountryController {
	
	@Autowired
	CountryDAO countryDao;
	@Autowired
	CommonFunction commonFunc;
	HttpServletRequest request;
	
	@RequestMapping(value="/countryList" ,method=RequestMethod.GET)
	public ModelAndView countryList(HttpServletRequest request)
	{
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		List<CountryBean> list=null;
		list=countryDao.listCountry(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/country/countryList");
		model.addObject("countryList",list);
		model.addObject("search",new CountryBean());
		return model;
	}
	
	@RequestMapping(value="/addCountry",method=RequestMethod.GET)
	public ModelAndView addCountryPage()
	{
		
		//List<CommonBean> countryList=commonFunc.getAllCountryList();
		ModelAndView model= new ModelAndView("admin/country/addCountry");
		model.addObject("add",new CountryBean());
		//model.addObject("allCountryList", countryList);
		return model;
	}
	
	@RequestMapping(value="/AddCountry",method=RequestMethod.POST)
	public String AddCountry(@ModelAttribute("add")CountryBean country,ModelMap model)
	{
		String message=countryDao.add(country);
		model.addAttribute("search", new CountryBean());
		return "redirect:/countryList";
	}
	
	@RequestMapping(value="/updateStatus",method=RequestMethod.GET)
	public String updateCountryStatus(@ModelAttribute("search")CountryBean CountryBean,HttpServletRequest request)
	{
		String checkStatus=countryDao.updateStatus(request);
		return "redirect:/countryList";
	}
	
	@RequestMapping(value="/searchCountry",method=RequestMethod.POST)
	public String searchCountry(@ModelAttribute("search")CountryBean CountryBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("CountryBean", CountryBean);
		return "redirect:/countryList";
	}
	
	@RequestMapping(value="/updateCountry",method=RequestMethod.GET)
	public ModelAndView updateCountry(HttpServletRequest request)
	{
		CountryBean CountryBean=countryDao.checkCountry(request);
		ModelAndView model=new ModelAndView("admin/country/updateCountry");
		model.addObject("edit",CountryBean);
		return model;
	}
	
	@RequestMapping(value="/editCountry",method=RequestMethod.POST)
	public String EditCountry(@ModelAttribute("edit")CountryBean CountryBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("countryId");
		countryDao.editCountry(id,CountryBean);
		return "redirect:/countryList";
	}
	
	@RequestMapping(value="/ValidateCountry",method=RequestMethod.GET)
	public String checkCountryName(HttpServletRequest request,ModelMap model)
	{
		String chkCountry=countryDao.validateCountryName(request);
		model.addAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}
	
	@RequestMapping(value="/ValidateCountryCode",method=RequestMethod.GET)
	public String checkCountryCode(HttpServletRequest request,ModelMap model)
	{
		String chkCode=countryDao.validateCountryCode(request);
		model.addAttribute("checkCountryCode", chkCode);
		return "ajaxOperation/ajaxOperation";
	}

}
