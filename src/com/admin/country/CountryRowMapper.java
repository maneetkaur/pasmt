package com.admin.country;

import java.sql.ResultSet;
import java.sql.SQLException;
 
import org.springframework.jdbc.core.RowMapper;

public class CountryRowMapper implements RowMapper<CountryBean>
{
	@Override
	public CountryBean mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		CountryBean country=new CountryBean();
		country.setCountryId(resultSet.getString("country_id"));
		country.setCountry(resultSet.getString("country_name"));
		country.setCountryCode(resultSet.getString("country_code"));
		country.setStatus(resultSet.getString("is_active_pasmt"));
		return country;
	}
}
