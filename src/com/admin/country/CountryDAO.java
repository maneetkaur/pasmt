package com.admin.country;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface CountryDAO {
	
	public List listCountry(HttpServletRequest request,int startIndex,int range);
	public String add(CountryBean country);
	public String updateStatus(HttpServletRequest request);
	public void editCountry(String countryId,CountryBean CountryBean);
	public CountryBean checkCountry(HttpServletRequest request);
	public String validateCountryName(HttpServletRequest request);
	public String validateCountryCode(HttpServletRequest request);

}
