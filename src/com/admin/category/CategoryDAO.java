package com.admin.category;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface CategoryDAO {
	
	public void add(CategoryBean categoryBean,HttpServletRequest request,String filePath);
	public List getCatgeoryList(HttpServletRequest request,int startIndex,int range);
	public String updateStatus(HttpServletRequest request);
	public CategoryBean getCategoryDetail(HttpServletRequest request,String filePath);
	public void editCategory(String categoryId,CategoryBean categoryBean,String filePath);
	public String validateCategoryName(HttpServletRequest request);

}
