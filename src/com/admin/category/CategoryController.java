package com.admin.category;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class CategoryController {
	
	@Autowired
	CommonFunction commFunc;
	
	@Autowired
	CategoryDAO categoryDao;
	
	@Autowired
	ServletContext servletContext;
	
	
	@RequestMapping(value="/categoryList",method=RequestMethod.GET)
	public ModelAndView categoryList(HttpServletRequest request)
	{
		
		List<CategoryBean> list=null;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		list=categoryDao.getCatgeoryList(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/category/categoryList");
		model.addObject("categoryList",list);
		model.addObject("search_category",new CategoryBean());
		return model;
	}
	
	@RequestMapping(value="/addCategory",method=RequestMethod.GET)
	public ModelAndView addCategory()
	{
		List<CommonBean> language=new ArrayList<CommonBean>();
		language=commFunc.getlanguageList();
		ModelAndView model=new ModelAndView("admin/category/addCategory");
		model.addObject("add_category",new CategoryBean());
		model.addObject("languageList",language);
		//model.addObject("message",status);
		return model;
	}
	
	@RequestMapping(value="/submitCategory",method=RequestMethod.POST)
	public String AddCategory(@ModelAttribute("add_category") CategoryBean categoryBean,ModelMap model,HttpServletRequest request)
	{		
		String filePath="",fileName="";
		filePath=servletContext.getRealPath("");
		categoryDao.add(categoryBean,request,filePath);
		model.addAttribute("search_category", new CategoryBean());
		return "redirect:/categoryList";
	}
	
	@RequestMapping(value="/searchCategory",method=RequestMethod.POST)
	public String searchCategory(@ModelAttribute("search_category")CategoryBean categoryBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("categoryBean",categoryBean);
		return "redirect:/categoryList";
	}
	@RequestMapping(value="/updateCategoryStatus",method=RequestMethod.GET)
	public String updateCategoryStatus(@ModelAttribute("search_category")CategoryBean categoryBean,HttpServletRequest request)
	{
		String checkStatus=categoryDao.updateStatus(request);
		return "redirect:/categoryList";
	}
	
	@RequestMapping(value="/updateCategory",method=RequestMethod.GET)
	public ModelAndView updateCategory(HttpServletRequest request)
	{
		String filePath="",fileName="";
		filePath=servletContext.getRealPath("");
		CategoryBean categoryBean=categoryDao.getCategoryDetail(request,filePath);
		List<CommonBean> languageList=commFunc.getlanguageList();
		ModelAndView model=new ModelAndView("admin/category/updateCategory");
		model.addObject("update_category",categoryBean);
		model.addObject("languageList", languageList);
		return model;
	}
	
	@RequestMapping(value="/editCategory",method=RequestMethod.POST)
	public String EditCountry(@ModelAttribute("update_category")CategoryBean categoryBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String filePath=servletContext.getRealPath("");
		String id=(String)session.getAttribute("categoryId");
		categoryDao.editCategory(id,categoryBean,filePath);
		return "redirect:/categoryList";
	}
	
	@RequestMapping(value="/ValidateCategoryName",method=RequestMethod.GET)
	public String checkCompanyName(HttpServletRequest request,ModelMap model)
	{
		String chkCategory=categoryDao.validateCategoryName(request);
		model.addAttribute("checkCategory", chkCategory);
		return "ajaxOperation/ajaxOperation";
		
	}
	
}
