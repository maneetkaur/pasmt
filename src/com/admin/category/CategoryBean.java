package com.admin.category;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.admin.subCategory.SubCategoryBean;

public class CategoryBean {
	
	private String categoryId,languageId,categoryName,categoryTypeId;
	private List<Category> categoryList;
	private String status,imagePath;
	private String sizeOfList,categoryIdList;
	private CommonsMultipartFile categoryImage;
	private List<SubCategoryBean> subCategoryList;
	
	public CategoryBean()
	{
		categoryList=LazyList.decorate(new ArrayList<Category>(), new InstantiateFactory(Category.class));
	}
	public CommonsMultipartFile getCategoryImage() {
		return categoryImage;
	}
	public void setCategoryImage(CommonsMultipartFile categoryImage) {
		this.categoryImage = categoryImage;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getCategoryIdList() {
		return categoryIdList;
	}
	public void setCategoryIdList(String categoryIdList) {
		this.categoryIdList = categoryIdList;
	}
	public List<SubCategoryBean> getSubCategoryList() {
		return subCategoryList;
	}
	public void setSubCategoryList(List<SubCategoryBean> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	public String getSizeOfList() {
		return sizeOfList;
	}
	public void setSizeOfList(String sizeOfList) {
		this.sizeOfList = sizeOfList;
	}	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryTypeId() {
		return categoryTypeId;
	}

	public void setCategoryTypeId(String categoryTypeId) {
		this.categoryTypeId = categoryTypeId;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	
}
