package com.admin.category;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


import com.admin.category.CategoryBean;

@Repository
@Transactional
public class CategoryDAOImpl implements CategoryDAO {
	
	private JdbcTemplate jdbcTemplate;
	@Autowired		
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		//System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}

	public void add(CategoryBean categoryBean, HttpServletRequest request,String filePath)
	{
		String languageId,category,catgoryNameInNative,fileName="";
		List<Category> cList=new ArrayList<>();
		String sql="",sql1="",sql2="";
		int row;
		 sql="insert into lu_category_type(category_type,created_on) values(?,?)";
			//System.out.println("query "+sql);	
		try{
			//System.out.println("1");
			jdbcTemplate.update(sql, new Object[]{
					categoryBean.getCategoryName(),new Date()});
			//System.out.println("2");
			sql1="select category_type_id from lu_category_type where category_type='"+categoryBean.getCategoryName()+"'";
			List<CategoryBean> list=jdbcTemplate.query(sql1, new RowMapper(){
				public CategoryBean mapRow(ResultSet rs,int rownumber) throws SQLException {
					CategoryBean bean=new CategoryBean();
					bean.setCategoryTypeId(rs.getString(1));
					return bean;
				}
			});
			
			//System.out.println("3");
			//System.out.println(categoryBean.getRowCount());
			cList=removeRow(categoryBean);
			System.out.println("cList value is ->"+cList);
			for(int i=0;i<cList.size();i++) 
			{
				System.out.println("UTF8 value is->"+cList.get(i).getCategoryName());
				String test=new String(cList.get(i).getCategoryName().getBytes("ISO-8859-1"),"UTF-8");
				System.out.println("UTF8 test value is->"+test);
				
				//dataStr = new String(dataStr.getBytes("ISO-8859-1"),"UTF-8");
				sql2="insert into lu_category(category_type_id,language_id,category_name,created_on) values(?,?,?,?)";
				
				jdbcTemplate.update(sql2, new Object[]
						{
					list.get(0).getCategoryTypeId(),cList.get(i).getLanguageId(),cList.get(i).getCategoryName(),new Date()});
			}
			fileName=uploadImage(categoryBean,filePath);
			//throw new RuntimeException("simulate Error condition") ;
		}
		
		catch(Exception e)
		{
			System.out.println("exception in add() in category--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			//throw e;
		}
	}
	
	public List getCatgeoryList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		//String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		//Boolean status=false;
		StringBuffer sb=new StringBuffer();
		List<CategoryBean> categoryList=new ArrayList<CategoryBean>();
		String sql="";
		try
			{
			sb.append(" and ");
			if(session.getAttribute("categoryBean")!=null)
			{
				CategoryBean categoryBean=(CategoryBean)session.getAttribute("categoryBean");	
				if(categoryBean.getCategoryName()!=null)
	 		   	{
		 			if(!categoryBean.getCategoryName().equals(""))
		 			{
		 				//status=true;
		 				sb.append("ct.category_type like '%"+categoryBean.getCategoryName()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(categoryBean.getLanguageId()!=null)
	 		   {
		 			if(!categoryBean.getLanguageId().equals(""))
		 			{
		 				//status=true;
		 				sb.append(" l.language like '%"+categoryBean.getLanguageId()+"%'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}	
	 			qryStartPart="select distinct ct.category_type_id,ct.category_type,ct.status from lu_category_type ct ,lu_language l,lu_category c where ct.category_type_id=c.category_type_id and l.lang_id=c.language_id   ";
	 			qrylast="order by ct.category_type_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart+qrylast; 
				categoryList=jdbcTemplate.query(sql,new RowMapper()
				{
					public CategoryBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						CategoryBean bean=new CategoryBean();
						bean.setCategoryTypeId(rs.getString(1));
						bean.setCategoryName(rs.getString(2));
						bean.setStatus(rs.getString(3));
						return bean;
					}
				});
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(distinct ct.category_type_id) from lu_category_type ct ,lu_language l,lu_category c where ct.category_type_id=c.category_type_id and l.lang_id=c.language_id  ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in getListCategory "+e.toString());
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		
		session.removeAttribute("categoryBean");
		return categoryList;
	}

	public String updateStatus(HttpServletRequest request)
	{

		List<CategoryBean> list=new ArrayList<CategoryBean>();
		//System.out.println("in ChangeStatus");
		String sql="select status from lu_category_type where category_type_id='"+request.getParameter("categoryTypeId")+"'";
		//System.out.println("query "+sql);
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public CategoryBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				CategoryBean bean=new CategoryBean();
				bean.setStatus(rs.getString(1));
				return bean;
				
			}
		});
		if(Integer.parseInt(list.get(0).getStatus().toString())==1)
		{
			jdbcTemplate.update("update lu_category_type set status='0' where category_type_id='"+request.getParameter("categoryTypeId")+"'");
		}
		else
		{
			jdbcTemplate.update("update lu_category_type set status='1' where category_type_id='"+request.getParameter("categoryTypeId")+"'");
		}
		return "Success";
	}
	
	public CategoryBean getCategoryDetail(HttpServletRequest request,String filePath)
	{
		HttpSession session=request.getSession(true);
		String sql="",sql1="",path="";
		CategoryBean categoryBean=new CategoryBean();
		List<CategoryBean> list=new ArrayList<>();
		List<Category> clist=new ArrayList<>();
		//CommonsMultipartFile categoryImage;
		String id=request.getParameter("categoryId");
		sql="select category_type_id,category_type from lu_category_type where category_type_id='"+id+"'";
		sql1="select category_id,language_id,category_name from lu_category where category_type_id='"+id+"'";
		list=jdbcTemplate.query(sql, new RowMapper() {
			public CategoryBean mapRow(ResultSet rs,int rowNumber) throws SQLException{
				CategoryBean bean=new CategoryBean();
				bean.setCategoryTypeId(rs.getString(1));
				bean.setCategoryName(rs.getString(2));
				return bean;
			}
		});
		path="/Document/Admin/Category_Images/"+list.get(0).getCategoryName()+".jpg";
		categoryBean=list.get(0);
		clist=jdbcTemplate.query(sql1,new CategoryRowMapper());
		categoryBean.setCategoryList(clist);
		categoryBean.setImagePath(path);
		session.setAttribute("categoryId",id);
		//System.out.println(list);
		return categoryBean;
	}
	
	/*
	 * update category information
	 */
	public void editCategory(String categoryId,CategoryBean categoryBean, String filePath)
	{
		String sql="",sql1="",sql2="",fileName="";
		StringBuffer sb;	
		List<Category> clist=new ArrayList<>();	
		sql="update lu_category_type set category_type='"+categoryBean.getCategoryName()+"' where category_type_id='"+categoryId+"'";
		jdbcTemplate.update(sql);
		clist=removeRow(categoryBean);
		fileName=uploadImage(categoryBean,filePath);
		if(clist!=null)
		{
			Integer currentSize=Integer.parseInt(categoryBean.getSizeOfList());
			if(categoryBean.getCategoryIdList().length()>0)
			{
				String[] categoryids=categoryBean.getCategoryIdList().split(",");
				Category newCategory;
				
				/********
				 *  for update already exists rows *
				 *******/
				if(currentSize>0 && categoryids.length>0)
				{
					for(int i=0;i<categoryids.length;i++)
					{
						newCategory=clist.get(Integer.parseInt(categoryids[i]));
						String query="update lu_category set language_id='"+newCategory.getLanguageId()+"',category_name='"+newCategory.getCategoryName()+"' where category_id='"+newCategory.getCategoryId()+"'";
						jdbcTemplate.update(query);
					}
				}
			}
			/*****
			 ** for insert newly added rows *
			 *********/
			if(clist.size()>currentSize)
			{
				sb=new StringBuffer("insert into lu_category(category_type_id,language_id,category_name) values ");
				for(int i=currentSize;i<clist.size();i++)
				{
					sb.append("("+categoryId+","+clist.get(i).getLanguageId()+",'"+clist.get(i).getCategoryName()+"'),");
				}
				sql2=sb.toString();
				/**** to remove , from query *****/
				if(sql2.charAt(sql2.length()-1)==','){
				   sql2 = sql2.substring(0, sql2.length()-1);
				  }		 
				jdbcTemplate.update(sql2);
			}
		}
		
	}
	
	/***** 
	 * To Remove Empty Rows *
	 *****/
	
	public List<Category> removeRow(CategoryBean categoryBean)
	{
		List<Category> clist=new ArrayList<>();
		for(Category cate:categoryBean.getCategoryList())
		{
			if(cate.getCategoryName()!=null && cate.getLanguageId()!=null)
			{
				clist.add(cate);
			}
		}
		return clist;
	}
	
	
	/*******
	 * check category name already exists or not
	 */
	public String validateCategoryName(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select category_type_id from lu_category_type where category_type='"+request.getParameter("Category")+"'";
			List<CategoryBean> list=jdbcTemplate.query(sql, new RowMapper(){
					public CategoryBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						CategoryBean bean=new CategoryBean();
						bean.setCategoryId(rs.getString(1));
						return bean;
					}
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateCategoryName() "+e.toString());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}		
		return target;
	}
	
	/*************************************************
	 ********* image uploading function **************
	 *************************************************/
	
	public String uploadImage(CategoryBean categoryBean,String path)
	{
		String fileName="",file="";
		String fileExt="";
		CommonsMultipartFile multipartFile=categoryBean.getCategoryImage();
		
		/*List<RegionBean> list=new ArrayList<>();
		String sql="select country_name from lu_country where country_id='"+regionBean.getCountryId()+"'";
		//System.out.println("query--> "+sql);
		list=jdbcTemplate.query(sql, new RowMapper(){
			public RegionBean mapRow(ResultSet rs,int rownumber) throws SQLException {
				RegionBean bean=new RegionBean();
				bean.setCountry(rs.getString(1));
				return bean;
			}
		});*/
		if(multipartFile!=null)
		{	
			int index;
			file=categoryBean.getCategoryName();
			fileName=multipartFile.getOriginalFilename();
			index=fileName.lastIndexOf(".");
			fileExt=fileName.substring(index+1);
			
		}
		//fileName=file+"."+fileExt;
		fileName=file+".jpg";
		System.out.println("category image name-->"+fileName);
		String filePath = path+"/Document/Admin/Category_Images/"+fileName;	
		System.out.println("category image name-->"+filePath);
	    File destination = new File(filePath);
	    String status ="success";
	    try
	    {
	    	multipartFile.transferTo(destination);
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in upload category Image--> "+e);
	    	TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
	    }
	    return fileName;
	}
	
}
