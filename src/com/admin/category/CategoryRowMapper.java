package com.admin.category;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CategoryRowMapper implements RowMapper<Category>{
	
	@Override
	public Category mapRow(ResultSet rs,int rowNumber) throws SQLException{
		Category bean=new Category();
		bean.setCategoryId(rs.getString(1));
		bean.setLanguageId(rs.getString(2));
		bean.setCategoryName(rs.getString(3));
		return bean;
	}

}
