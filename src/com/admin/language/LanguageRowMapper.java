package com.admin.language;

import java.sql.ResultSet;
import java.sql.SQLException;
 
import org.springframework.jdbc.core.RowMapper;
 
public class LanguageRowMapper implements RowMapper<LangBean>{
 
    @Override
    public LangBean mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
        LangBean language = new LangBean();
        language.setLangId(Integer.parseInt(resultSet.getString("lang_id")));
        language.setLang(resultSet.getString("language"));
        language.setLangCode(resultSet.getString("lang_code"));
        language.setStatus(resultSet.getString("status"));
        return language;
    }
 
}
