package com.admin.language;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface LanguageDAO {
	public String add(LangBean language);
	public List searchLang(LangBean LangBean,HttpServletRequest request,int startIndex,int range);
	public List listLang(HttpServletRequest request,int startIndex,int range);
	public String validateLangName(HttpServletRequest request);
	public String validateLangCode(HttpServletRequest request);
	public String ChangeStatus(HttpServletRequest request);
	public LangBean checkLanguage(HttpServletRequest request);
	public void editLanguage(String langId,LangBean LangBean);
}
