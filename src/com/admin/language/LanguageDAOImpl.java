package com.admin.language;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Repository
@Transactional
public class LanguageDAOImpl implements LanguageDAO {
	

	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	public List listLang(HttpServletRequest request,int startIndex,int range)
	{
	
		HttpSession session=request.getSession(true);
		List<LangBean> langList=new ArrayList<>();
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		String sql="";
		//System.out.println("qurey "+sql);
		try
		{	 			
			sb.append("where ");
			if(session.getAttribute("LangList")!=null)
			{
				LangBean langBean=(LangBean)session.getAttribute("LangList");
				if(langBean.getLang()!=null)
		 		   {
			 			if(!langBean.getLang().equals(""))
			 			{
			 				sb.append("language like '%"+langBean.getLang()+"%'").append(" and ");
			 				
			 			}
		 		   }	
		 		   if(langBean.getLangCode()!=null)
		 		   {
			 			if(! langBean.getLangCode().equals(""))
			 			{
			 				sb.append(" lang_code like '%"+langBean.getLangCode()+"%'").append(" and ");
			 				
			 			}
		 		   }  
			}
		 		qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select * from lu_language  ";
	 			qrylast="order by lang_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart; 
				langList=jdbcTemplate.query(sql, new LanguageRowMapper());

				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_language ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
		}
		catch(Exception e)
		{
			System.out.println("Exception in searchLang "+e);
		}	
		session.removeAttribute("LangList");
		return langList;
	}
	public String add(LangBean LangBean)
	{
		String msg="error";
		String sql="";
		if(LangBean.getLangId()!=null)
		{
			sql="update lu_language set language = '"+LangBean.getLang()+"',lang_code ='"+LangBean.getLangCode()+"' where lang_id ="+LangBean.getLangId();
			//jdbcTemplate.update(query);
		}
		else
		{
			 sql="insert into lu_language(language,lang_code) values(?,?)";
			//System.out.println("query "+sql);	
		}
		try{
			//System.out.println("query "+sql);
			jdbcTemplate.update(sql, new Object[]{
				LangBean.getLang(),LangBean.getLangCode()
			});
			msg="success";
		}
		catch(Exception e)
		{
			System.out.println("exception in add language "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg; 
	}
	public List searchLang(LangBean LangBean,HttpServletRequest request,int startIndex,int range)
	{
		List<LangBean> langList=new ArrayList<>();
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		String sql="";
		//System.out.println("qurey "+sql);
		try
		{	 			
			sb.append("where ");
			if(LangBean.getLang()!=null)
	 		   {
		 			if(!LangBean.getLang().equals(""))
		 			{
		 				sb.append("language like '%"+LangBean.getLang()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(LangBean.getLangCode()!=null)
	 		   {
		 			if(! LangBean.getLangCode().equals(""))
		 			{
		 				sb.append(" lang_code like '%"+LangBean.getLangCode()+"%'").append(" and ");
		 				
		 			}
	 		   }  		 			
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select * from lu_language  ";
	 			qrylast="order by lang_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart; 
				langList=jdbcTemplate.query(sql, new LanguageRowMapper());

				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_country ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
		}
		catch(Exception e)
		{
			System.out.println("Exception in searchLang "+e);
		}
		return langList;
	}
	public String validateLangName(HttpServletRequest request)
	{
	
		String target="";
		
		try
		{
			String sql="select * from lu_language where language='"+request.getParameter("lang")+"'";
			//System.out.println("lang sql="+sql);
			List<LangBean> list=jdbcTemplate.query(sql, new LanguageRowMapper());
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in checkLangName() "+e.toString());	
		}		
		return target;
	}
	public String validateLangCode(HttpServletRequest request)
	{
		
		String target="";
		
		try
		{
			String sql="select * from lu_language where lang_code='"+request.getParameter("langCode")+"'";
			List<LangBean> list=jdbcTemplate.query(sql,new LanguageRowMapper());
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
				
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateLangCode() "+e.toString());	
		}		
		return target;
	}
	public String ChangeStatus(HttpServletRequest request)
	{
		List<LangBean> list=new ArrayList<LangBean>();
		String sql="select * from lu_language where lang_id='"+request.getParameter("langId")+"'";
		//System.out.println("query "+sql);
		list=jdbcTemplate.query(sql, new LanguageRowMapper());
		if(Integer.parseInt(list.get(0).getStatus().toString())==1)
		{
			jdbcTemplate.update("update lu_language set status='0' where lang_id='"+request.getParameter("langId")+"'");
		}
		else
		{
			jdbcTemplate.update("update lu_language set status='1' where lang_id='"+request.getParameter("langId")+"'");
		}
		return "Success";
	}
	public LangBean checkLanguage(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		LangBean bean=new LangBean();
		List<LangBean> list=new ArrayList<>();
		String id=request.getParameter("langId");
		String sql="select * from lu_language where lang_id='"+id+"'";
		list=jdbcTemplate.query(sql, new LanguageRowMapper());
		bean=list.get(0);
		session.setAttribute("langId",id);
		return bean;
	}
	public void editLanguage(String langId,LangBean LangBean)
	{
		String sql="update lu_language set language='"+LangBean.getLang()+"',lang_code='"+LangBean.getLangCode()+"' where lang_id='"+langId+"'";
		//System.out.println("edit language="+sql);
		jdbcTemplate.update(sql);
	}
}
