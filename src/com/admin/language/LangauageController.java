package com.admin.language;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LangauageController {
		
		@Autowired
		LanguageDAO langDao;
		HttpServletRequest request;
		@RequestMapping(value="/addLang",method=RequestMethod.GET)
		public ModelAndView addLanguage(){
			return new ModelAndView("admin/language/addlanguage","add",new LangBean());
		}
		
		@RequestMapping(value="/addLanguage" ,method=RequestMethod.POST)
		public String AddLanguage(@ModelAttribute("add")LangBean language,ModelMap model)
		{
			String message=langDao.add(language);
			model.addAttribute("search", new LangBean());
			return "redirect:/languagelist";
		}
		
		@RequestMapping(value="/updateLanguage",method=RequestMethod.GET)
		public ModelAndView updatelang(HttpServletRequest request)
		{
			LangBean LangBean=langDao.checkLanguage(request);
			ModelAndView model=new ModelAndView("admin/language/updateLanguage");
			model.addObject("edit",LangBean);
			return model;
		}
		
		@RequestMapping(value="/editLanguage",method=RequestMethod.POST)
		public String editLang(@ModelAttribute("edit")LangBean LangBean,HttpServletRequest request)
		{
			HttpSession session=request.getSession(true);
			String id=(String)session.getAttribute("langId");
			langDao.editLanguage(id,LangBean);
			return "redirect:/languagelist";
		}
		
		@RequestMapping(value="/searchLang", method=RequestMethod.POST)
		public String searchLanguage(@ModelAttribute("search")LangBean LangBean,ModelMap model,HttpServletRequest request)
		{
			HttpSession session=request.getSession();
			//list=langDao.searchLang(LangBean,request,startIndex,15);
			//model.addAttribute("LangList",list);
			session.setAttribute("LangList",LangBean);
			return "redirect:/languagelist";
		}
		
		@RequestMapping(value="/languagelist",method=RequestMethod.GET)
		public ModelAndView languageList(HttpServletRequest request)
		{
			int startIndex=0;
			List<LangBean> list=null;
			if(request.getParameter("pager.offset")!=null)
			{
				startIndex=Integer.parseInt(request.getParameter("pager.offset"));
			}
			list=langDao.listLang(request,startIndex,15);
			ModelAndView model=new ModelAndView("admin/language/languagelist");
			model.addObject("langList",list);
			model.addObject("search",new LangBean());
			//model.addObject("pageListHolder", pagelistHolder);
			//model.addObject("edit",new langBean());
			return model;
		}
		
		@RequestMapping(value="/ValidateLanguage",method=RequestMethod.GET)
		public String checkLangName(HttpServletRequest request,ModelMap model)
		{
			String chkLang=langDao.validateLangName(request);
			//model.addAttribute("checkLang", chkLang);
			request.setAttribute("checkLang", chkLang);
			return "ajaxOperation/ajaxOperation";
			//chkLang=null;
		}
		
		@RequestMapping(value="/ValidateLanguageCode",method=RequestMethod.GET)
		public String checkLangCode(HttpServletRequest request)
		{
			String chkCode=langDao.validateLangCode(request);
			request.setAttribute("checkCode", chkCode);
			return "ajaxOperation/ajaxOperation";
		}
		@RequestMapping(value="/editStatus",method=RequestMethod.GET)
		public String updateStatus(@ModelAttribute("search")LangBean lang,HttpServletRequest request)
		{
			langDao.ChangeStatus(request);
			//return "language/languagelist";
			return "redirect:/languagelist";
		}
			

}
