package com.admin.registrationModule;

public class RegistrationBean {

	private String regQuestionId,regQuestion;

	public String getRegQuestionId() {
		return regQuestionId;
	}

	public void setRegQuestionId(String regQuestionId) {
		this.regQuestionId = regQuestionId;
	}

	public String getRegQuestion() {
		return regQuestion;
	}

	public void setRegQuestion(String regQuestion) {
		this.regQuestion = regQuestion;
	}
}
