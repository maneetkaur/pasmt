package com.admin.registrationModule;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class RegistrationController {
	
	@Autowired
	HttpServletRequest request;
	
	@RequestMapping(value="/abcurl", method=RequestMethod.GET)
	public ModelAndView regisatrtionQuestion(HttpServletRequest request)
	{
		ModelAndView model=new ModelAndView("admin/registrationPageQuest/registration");
		model.addObject("registartionQuesForm", new RegistrationBean());
		return model;
	}

}
