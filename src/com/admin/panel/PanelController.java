package com.admin.panel;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class PanelController {
	
	@Autowired
	PanelDAO panelDao;
	@Autowired
	CommonFunction commFunc;
	
	@RequestMapping(value="/panelList",method=RequestMethod.GET)
	public ModelAndView listPanel(HttpServletRequest request)
	{
		List<PanelBean> panelList;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		panelList=panelDao.getPanelList(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/panel/panelList");
		model.addObject("panelList", panelList);
		model.addObject("search",new PanelBean());
		return model;
	}
	
	@RequestMapping(value="/addPanel",method=RequestMethod.GET)
	public ModelAndView addCountry(HttpServletRequest request)
	
	{
		String status="";
		if(request.getParameter("message")!=null)
		{
			status=(String)request.getParameter("message");
		}
		List<CommonBean> cultureList=commFunc.getCultureList();
		List<CommonBean> companyList=commFunc.getCompanyList();
		ModelAndView model=new ModelAndView("admin/panel/addPanel");
		model.addObject("add", new PanelBean());
		model.addObject("cultureList", cultureList);
		model.addObject("companyList", companyList);
		model.addObject("message", status);
		return model;
	}
	
	@RequestMapping(value="/AddPanel",method=RequestMethod.POST)
	public String AddCountry(@ModelAttribute("add")PanelBean panelBean,ModelMap model)
	{
		String status=panelDao.checkPanelExist(panelBean);
		if(status.equals("Already Exist"))
		{
			model.addAttribute("message", status);
			return "redirect:/addPanel";
		}
		else
		{
			String message=panelDao.add(panelBean);
			model.addAttribute("search", new PanelBean());
			return "redirect:/panelList";
		}	
	}
	
	@RequestMapping(value="/searchPanel",method=RequestMethod.POST)
	public String searchCountry(@ModelAttribute("search")PanelBean panelBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("panelBean", panelBean);
		return "redirect:/panelList";
	}
	
	@RequestMapping(value="/updatePanelStatus",method=RequestMethod.GET)
	public String updatePanelStatus(@ModelAttribute("search")PanelBean panelBean,HttpServletRequest request)
	{
		String checkStatus=panelDao.updateStatus(request);
		return "redirect:/panelList";
	}
	
	/*@RequestMapping(value="/viewPanel",method=RequestMethod.GET)
	public ModelAndView viewPanelDetail(HttpServletRequest request)
	{
		PanelBean panelBean=new PanelBean();
		panelBean=panelDao.getPanelInfo(request);		
		List<commonBean> cultureList=commFunc.getCultureList();
		List<commonBean> companyList=commFunc.getCompanyList();
		ModelAndView model=new ModelAndView("company/updateCompany");
		model.addObject("cultureList", cultureList);
		model.addObject("companyList", companyList);
		model.addObject("view",panelBean);
		return model;		
	}*/
	
	@RequestMapping(value="/updatePanel",method=RequestMethod.GET)
	public ModelAndView updateCulture(HttpServletRequest request)
	{
		/*String status="";
		CultureBean CultureBean=new CultureBean();
		if(request.getParameter("message")!=null)
		{
			status=(String)request.getParameter("message");
		}
		if(request.getParameter("cultureId")!=null)
		{
			CultureBean=cultureDao.checkCulture(request);
		}	*/

		PanelBean panelBean=new PanelBean();
		panelBean=panelDao.getPanelInfo(request);		
		//List<commonBean> cultureList=commFunc.getCultureList();
		//List<commonBean> companyList=commFunc.getCompanyList();
		ModelAndView model=new ModelAndView("admin/panel/updatePanel");
		//model.addObject("cultureList", cultureList);
		//model.addObject("companyList", companyList);
		model.addObject("edit",panelBean);
		return model;
				
	}
	@RequestMapping(value="/editPanel",method=RequestMethod.POST)
	public String EditCountry(@ModelAttribute("edit")PanelBean panelBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("panelId");
		panelDao.editPanel(id,panelBean);
		return "redirect:/panelList";
	}
	
	@RequestMapping(value="/ValidatePanelName",method=RequestMethod.GET)
	public String checkPanelName(HttpServletRequest request,ModelMap model)
	{
		String chkPanel=panelDao.validatePanelName(request);
		model.addAttribute("checkPanelName", chkPanel);
		//System.out.println("check country"+chkCountry);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}

}
