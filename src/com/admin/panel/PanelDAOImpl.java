package com.admin.panel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Repository
@Transactional
public class PanelDAOImpl implements PanelDAO{
	
	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	public List<PanelBean> getPanelList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		List<PanelBean> panelList=new ArrayList<PanelBean>();
		String sql="";
		try
			{
			sb.append(" and ");
			if(session.getAttribute("panelBean")!=null)
			{
			PanelBean panelBean=(PanelBean)session.getAttribute("panelBean");	
				if(panelBean.getCompany()!=null)
	 		   	{
		 			if(!panelBean.getCompany().equals(""))
		 			{
		 				sb.append("company_name like '%"+panelBean.getCompany()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(panelBean.getCountryName()!=null)
	 		   {
		 			if(!panelBean.getCountryName().equals(""))
		 			{
		 				sb.append(" country_name like '%"+panelBean.getCountryName()+"%'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
			
			qryStartPart="select p.panel_id,p.panel_name,comp.company_name,p.status,lang.lang_code,con.country_code from lu_culture cul,lu_country con,lu_language lang,panel p,lu_company comp where cul.country_id=con.country_id and cul.lang_id=lang.lang_id and p.company_id=comp.company_id and cul.culture_id=p.culture_id ";	 			
			qrylast	="order by p.panel_id limit "+startIndex+ "," +range;
			sql=qryStartPart+qryPart+qrylast; 
			panelList=jdbcTemplate.query(sql,new PanelRowMapper());
			/*
			 * count for pagination 
			 */
			qryStartPart="select count(1) from lu_culture cul,lu_country con,lu_language lang,panel p,lu_company comp where cul.country_id=con.country_id and cul.lang_id=lang.lang_id and p.company_id=comp.company_id and cul.culture_id=p.culture_id  ";
			sql=qryStartPart+qryPart;
			int total_rows = jdbcTemplate.queryForInt(sql);
			request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in getPanelList "+e.toString());
			}
		
		session.removeAttribute("panelBean");
		return panelList;
	}
	
	public String add(PanelBean panelBean)
	{
		String msg="error";
		String sql="",sql1="",sql2="";
		if(panelBean.getPanelId()!=null)
		{
			sql="update panel set company_id = '"+panelBean.getCompany()+"',culture_id ='"+panelBean.getCultureId()+"',panel_name='"+panelBean.getPanelName()+"' where panel_id ="+panelBean.getPanelId();
			//jdbcTemplate.update(query);
		}
		else
		{
			 sql="insert into panel(company_id,culture_id,panel_name) values(?,?,?)";
			 sql1="insert into panel_module_rights(panel_id,module_id) values(?,?)";
			//System.out.println("query "+);	
		}
		try{
			jdbcTemplate.update(sql, new Object[]{
					panelBean.getCompanyId(),panelBean.getCultureId(),panelBean.getPanelName()});
			//System.out.println("1");
			sql2="select panel_id from panel where panel_name='"+panelBean.getPanelName()+"' and company_id='"+panelBean.getCompanyId()+"' and culture_id='"+panelBean.getCultureId()+"'";
			List<PanelBean> bean=jdbcTemplate.query(sql2,new RowMapper()
			{
				public PanelBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					PanelBean bean=new PanelBean();
					bean.setPanelId(rs.getString(1));
					return bean;
				}
			});
			
			for(int i=0;i<panelBean.getSelectModule().size();i++)
			{
				jdbcTemplate.update(sql1,new Object[]{
						bean.get(0).getPanelId(),panelBean.getSelectModule().get(i)
				});
			}
			msg="success";
		}
		catch(Exception e)
		{
			System.out.println("exception in add() in panel "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg; 
		
		
	}
	
	public String updateStatus(HttpServletRequest request)
	{
		List<PanelBean> list=new ArrayList<PanelBean>();
		//System.out.println("in ChangeStatus");
		String sql="select status from panel where panel_id='"+request.getParameter("panelId")+"'";
		//System.out.println("query "+sql);
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public PanelBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				PanelBean bean=new PanelBean();
				bean.setStatus(rs.getString(1));
				return bean;
			}
		});
		if(Integer.parseInt(list.get(0).getStatus().toString())==1)
		{
			jdbcTemplate.update("update panel set status='0' where panel_id='"+request.getParameter("panelId")+"'");
		}
		else
		{
			jdbcTemplate.update("update panel set status='1' where panel_id='"+request.getParameter("panelId")+"'");
		}
		return "Success";
	}
	
	public String checkPanelExist(PanelBean panelBean)
	{
		String checkStatus="";
		List<PanelBean> list=new ArrayList<PanelBean>();
		String sql="select panel_id from panel where company_id='"+panelBean.getCompanyId()+"' and culture_id='"+panelBean.getCultureId()+"' and panel_name='"+panelBean.getPanelName()+"'";
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public PanelBean mapRow(ResultSet rs,int rownumber) throws SQLException {
				PanelBean bean=new PanelBean();
				bean.setPanelId(rs.getString(1));
				return bean;
			}
		});
		if(list!=null && !list.isEmpty())
		{
			checkStatus="Already Exist";
		}
		else
		{
			checkStatus="Not Have";
		}
		return checkStatus;
	}
	
	public PanelBean getPanelInfo(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		PanelBean bean=new PanelBean();
		List<PanelBean> list=new ArrayList<>();
		List<String> list1=new ArrayList<>();
		String id=request.getParameter("panelId");
		String sql="select p.panel_id,p.panel_name,comp.company_name,p.status,lang.lang_code,con.country_code from lu_culture cul,lu_country con,lu_language lang,panel p,lu_company comp where cul.country_id=con.country_id and cul.lang_id=lang.lang_id and p.company_id=comp.company_id and cul.culture_id=p.culture_id and p.panel_id='"+id+"'";
		list=jdbcTemplate.query(sql, new PanelRowMapper());
		bean=list.get(0);
		String sql1="select module_id from panel_module_rights where panel_id='"+id+"'";
		list1=jdbcTemplate.query(sql1, new RowMapper(){
			public String mapRow(ResultSet rs,int rownumber) throws SQLException {
				String value=rs.getString(1);
				return value;
			}
		});
		bean.setSelectModule(list1);
		//System.out.println("module list" +list1);
		session.setAttribute("panelId",id);
		//System.out.println(list);
		return bean;
	}
	
	public void editPanel(String panelId,PanelBean panelBean)
	{
		String sql="delete from panel_module_rights where panel_id='"+panelId+"'";
		//System.out.println(sql);
		try
		{
			jdbcTemplate.update(sql);
			String sql1="insert into panel_module_rights(panel_id,module_id) values(?,?)";
			//String sql1="update panel_module_rights set country_name='"+CountryBean.getCountry()+"',country_code='"+CountryBean.getCountryCode()+"' where country_id='"+countryId+"'";
			for(int i=0;i<panelBean.getSelectModule().size();i++)
			{
				jdbcTemplate.update(sql1,new Object[]{
						panelId,panelBean.getSelectModule().get(i)
				});
			}
		}
		catch(Exception e)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public String validatePanelName(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select panel_id from panel where panel_name='"+request.getParameter("panel")+"'";
			List<PanelBean> list=jdbcTemplate.query(sql, new RowMapper(){
					public PanelBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						PanelBean bean=new PanelBean();
						bean.setPanelId(rs.getString(1));
						return bean;
					}
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateCountryName() "+e.toString());	
		}		
		return target;
	}
	

}
