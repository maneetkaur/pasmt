package com.admin.panel;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PanelRowMapper implements RowMapper<PanelBean> {
	
	@Override
	public PanelBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
		PanelBean panel=new PanelBean();
		panel.setPanelId(rs.getString(1));
		panel.setPanelName(rs.getString(2));
		//panel.setCompanyId(rs.getString(3));
		panel.setCompany(rs.getString(3));
		//panel.setCultureId(rs.getString(6));
		panel.setStatus(rs.getString(4));
		String languageCode=rs.getString(5);
		String CountryCode=rs.getString(6);
		panel.setCultureCode(languageCode+"-"+CountryCode);
		return panel;
	}

}
