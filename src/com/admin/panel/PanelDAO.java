package com.admin.panel;

import java.util.List;

import javax.servlet.http.HttpServletRequest;


public interface PanelDAO {
	
	public List<PanelBean> getPanelList(HttpServletRequest request,int startIndex,int range);
	public String add(PanelBean panelBean);
	public String updateStatus(HttpServletRequest request);
	public String checkPanelExist(PanelBean panelBean);
	public PanelBean getPanelInfo(HttpServletRequest request);
	public void editPanel(String panelId,PanelBean panelBean);
	public String validatePanelName(HttpServletRequest request);

}
