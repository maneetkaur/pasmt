package com.admin.panel;

import java.util.ArrayList;
import java.util.List;

public class PanelBean {
	
	String panelId,companyId,cultureId,countryId,cultureCode;
	String panelName,countryName,status,panelSize,activePanelSize,company;
	String moduleName,moduleId;
	List<String> selectModule=new ArrayList<>();
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public List<String> getSelectModule() {
		return selectModule;
	}
	public void setSelectModule(List<String> selectModule) {
		this.selectModule = selectModule;
	}
	public String getCultureCode() {
		return cultureCode;
	}
	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPanelId() {
		return panelId;
	}
	public void setPanelId(String panelId) {
		this.panelId = panelId;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCultureId() {
		return cultureId;
	}
	public void setCultureId(String cultureId) {
		this.cultureId = cultureId;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getPanelName() {
		return panelName;
	}
	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPanelSize() {
		return panelSize;
	}
	public void setPanelSize(String panelSize) {
		this.panelSize = panelSize;
	}
	public String getActivePanelSize() {
		return activePanelSize;
	}
	public void setActivePanelSize(String activePanelSize) {
		this.activePanelSize = activePanelSize;
	}
	

}
