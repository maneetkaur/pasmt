package com.admin.questionType;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;



@Controller
public class QuestionTypeController {
	
	@Autowired
	QuestionTypeDAO questionTypeDao;
	@Autowired 
	CommonFunction commFunc;
	
	@RequestMapping(value="/questionTypeList" ,method=RequestMethod.GET)
	public ModelAndView getQuestionTypeList(HttpServletRequest request)
	{
		
		List<QuestionTypeBean> list=null;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		list=questionTypeDao.getQuestionTypeList(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/questionType/questionTypeList");
		model.addObject("questionTypeList",list);
		model.addObject("search_Question_Type",new QuestionTypeBean());
		//model.addObject("pageListHolder", pagelistHolder);
		//model.addObject("edit",new langBean());
		return model;
	}
	
	@RequestMapping(value="/addQuestionType",method=RequestMethod.GET)
	public ModelAndView addQuestionType()
	{
		List<CommonBean> categoryTypeList=commFunc.getSubCategoryTypeList();
		ModelAndView model=new ModelAndView("admin/questionType/addQuestionType");
		model.addObject("categoryTypeList", categoryTypeList);
		model.addObject("add_question_type",new QuestionTypeBean());
		return model;
	}
	
	@RequestMapping(value="/AddQuestionType",method=RequestMethod.POST)
	public String AddCountry(@ModelAttribute("add_question_type")QuestionTypeBean questionTypeBean,ModelMap model)
	{
		//System.out.println("in addLanguage");
		String message=questionTypeDao.addQuestionType(questionTypeBean);
		model.addAttribute("search", new QuestionTypeBean());
		return "redirect:/questionTypeList";
	}
	
	@RequestMapping(value="/updateQuestionType",method=RequestMethod.GET)
	public ModelAndView updateQuestionType(HttpServletRequest request)
	{
		QuestionTypeBean questionTypeBean=questionTypeDao.getQuestionTypeDetail(request);
		List<CommonBean> categoryTypeList=commFunc.getSubCategoryTypeList();
		ModelAndView model=new ModelAndView("admin/questionType/updateQuestionType");
		model.addObject("categoryTypeList", categoryTypeList);
		model.addObject("edit_question_type",questionTypeBean);
		return model;
	}
	
	@RequestMapping(value="/editQuestionType",method=RequestMethod.POST)
	public String EditCountry(@ModelAttribute("edit_question_type")QuestionTypeBean questionTypeBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("questionTypeId");
		questionTypeDao.editQuestionType(id,questionTypeBean);
		return "redirect:/questionTypeList";
	}
	
	@RequestMapping(value="/searchQuestionType",method=RequestMethod.POST)
	public String searchCountry(@ModelAttribute("search_Question_Type")QuestionTypeBean questionTypeBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("questionTypeBean", questionTypeBean);
		return "redirect:/questionTypeList";
	}
	
	@RequestMapping(value="/ValidateQuestionType",method=RequestMethod.GET)
	public String checkQuestionType(HttpServletRequest request,ModelMap model)
	{
		String chkQuestiontype=questionTypeDao.validateQuestionType(request);
		model.addAttribute("checkQuestionType", chkQuestiontype);
		//System.out.println("check country"+chkCountry);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}
}

