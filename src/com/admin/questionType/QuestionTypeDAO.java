package com.admin.questionType;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface QuestionTypeDAO {

	public String addQuestionType(QuestionTypeBean questionTypeBean);
	public QuestionTypeBean getQuestionTypeDetail(HttpServletRequest request);
	public void editQuestionType(String questionTypeId,QuestionTypeBean questionTypeBean);
	public List getQuestionTypeList(HttpServletRequest request,int startIndex,int range);
	public String validateQuestionType(HttpServletRequest request);
}
