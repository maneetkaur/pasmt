package com.admin.questionType;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class QuestionTypeRowMapper implements RowMapper<QuestionTypeBean>{
	
		@Override
		public QuestionTypeBean mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
			QuestionTypeBean questionTypeBean=new QuestionTypeBean();
			questionTypeBean.setQuestionTypeId(resultSet.getString("question_type_id"));
			questionTypeBean.setCategoryTypeId(resultSet.getString("sub_category_type_id"));
			questionTypeBean.setQuestionTypeHeading(resultSet.getString("question_type_header"));
			questionTypeBean.setQuestionType(resultSet.getString("question_type"));
			questionTypeBean.setQuestionNature(resultSet.getString("question_nature"));
			return questionTypeBean;
		}
	

}
