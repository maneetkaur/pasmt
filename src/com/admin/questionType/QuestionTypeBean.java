package com.admin.questionType;

public class QuestionTypeBean {

	private String questionTypeId,questionTypeHeading;
	private String categoryTypeId,categoryType,subCategory;
	private String questionType,questionNature;
	
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getQuestionTypeId() {
		return questionTypeId;
	}
	public void setQuestionTypeId(String questionTypeId) {
		this.questionTypeId = questionTypeId;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getQuestionTypeHeading() {
		return questionTypeHeading;
	}
	public void setQuestionTypeHeading(String questionTypeHeading) {
		this.questionTypeHeading = questionTypeHeading;
	}
	public String getCategoryTypeId() {
		return categoryTypeId;
	}
	public void setCategoryTypeId(String categoryTypeId) {
		this.categoryTypeId = categoryTypeId;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getQuestionNature() {
		return questionNature;
	}
	public void setQuestionNature(String questionNature) {
		this.questionNature = questionNature;
	}
}
