package com.admin.questionType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.admin.company.CompanyBean;

@Repository
@Transactional
public class QuestionTypeDAOImpl implements QuestionTypeDAO{
	

	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	public List getQuestionTypeList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qryPart="",qryStartPart="",qryLastPart="";
		StringBuffer sb=new StringBuffer();
		List<QuestionTypeBean> questionTypeList=new ArrayList<QuestionTypeBean>();
		String sql="";
		try
			{
			sb.append(" and ");
			if(session.getAttribute("questionTypeBean")!=null)
			{
				QuestionTypeBean questionTypeBean=(QuestionTypeBean)session.getAttribute("questionTypeBean");	
				if(questionTypeBean.getSubCategory()!=null)
	 		   	{
					//System.out.println("in null");
		 			if(!questionTypeBean.getSubCategory().equals(""))
		 			{
		 				//System.out.println("in if");
		 				//System.out.println(questionTypeBean.getCategoryType());
		 				sb.append("cate.category_type like '%"+questionTypeBean.getSubCategory()+"%'").append(" and ");
		 				//System.out.println("after append");
		 			}
	 		   }
				if(questionTypeBean.getCategoryType()!=null)
	 		   	{
					//System.out.println("in null");
		 			if(!questionTypeBean.getCategoryType().equals(""))
		 			{
		 				//System.out.println("in if");
		 				//System.out.println(questionTypeBean.getCategoryType());
		 				sb.append("ct.sub_category_type like '%"+questionTypeBean.getCategoryType()+"%'").append(" and ");
		 				//System.out.println("after append");
		 			}
	 		   }
				
	 		   if(questionTypeBean.getQuestionTypeHeading()!=null)
	 		   {
		 			if(!questionTypeBean.getQuestionTypeHeading().equals(""))
		 			{
		 				System.out.println();
		 				sb.append("qt.question_type_header like '%"+questionTypeBean.getQuestionTypeHeading()+"%'").append(" and ");	
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select ct.sub_category_type,qt.question_type_header ,qt.question_type_id,qt.question_nature,cate.category_type from lu_question_type qt,lu_sub_category_type ct,lu_category_type cate where ct.sub_category_type_id=qt.sub_category_type_id and ct.category_type_id=cate.category_type_id ";
	 			qryLastPart="order by qt.question_type_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart+qryLastPart; 
			
				System.out.println("query-->"+sql);
				questionTypeList=jdbcTemplate.query(sql,new RowMapper()
				{
					public QuestionTypeBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
						QuestionTypeBean bean=new QuestionTypeBean();
						bean.setCategoryType(rs.getString(1));
						bean.setQuestionTypeHeading(rs.getString(2));
						bean.setQuestionTypeId(rs.getString(3));
						bean.setQuestionNature(rs.getString(4));
						bean.setSubCategory(rs.getString("cate.category_type"));
						return bean;	
						
					}		
				});

				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_question_type qt,lu_sub_category_type ct,lu_category_type cate where ct.sub_category_type_id=qt.sub_category_type_id and ct.category_type_id=cate.category_type_id ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in getQuestionTypeList--> "+e.toString());
			}
		
		session.removeAttribute("questionTypeBean");
		return questionTypeList ;
	}
	
	public String addQuestionType(QuestionTypeBean questionTypeBean)
	{
		String msg="error";
		String sql="";
		if(questionTypeBean.getQuestionTypeId()!=null)
		{
			sql="update lu_question_type set sub_category_type_id = '"+questionTypeBean.getCategoryTypeId()+"',question_type_header ='"+questionTypeBean.getQuestionTypeHeading()+"' question_type='"+questionTypeBean.getQuestionType()+"' ,question_nature='"+questionTypeBean.getQuestionNature()+"' where question_type_id ="+questionTypeBean.getQuestionTypeId();
			//jdbcTemplate.update(query);
		}
		else
		{
			 sql="insert into lu_question_type(sub_category_type_id,question_type_header,question_type,question_nature,created_on) values(?,?,?,?,?)";
			//System.out.println("query "+sql);	
		}
		try{
			jdbcTemplate.update(sql, new Object[]{
					questionTypeBean.getCategoryTypeId(),questionTypeBean.getQuestionTypeHeading(),questionTypeBean.getQuestionType(),questionTypeBean.getQuestionNature(),new Date()});
			msg="success";
			//System.out.println(questionTypeBean.getQuestionNature());
		}
		catch(Exception e)
		{
			System.out.println("exception in addQuestionType() in Question Type --> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg; 
	}
	
	public QuestionTypeBean getQuestionTypeDetail(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		QuestionTypeBean bean=new QuestionTypeBean();
		List<QuestionTypeBean> list=new ArrayList<>();
		String id=request.getParameter("questionTypeId");
		String sql="select * from lu_question_type where question_type_id='"+id+"'";
		//System.out.println("sql-->"+sql);
		list=jdbcTemplate.query(sql, new QuestionTypeRowMapper());
		bean=list.get(0);
		session.setAttribute("questionTypeId",id);
		//System.out.println(list);
		return bean;
	}
	
	public void editQuestionType(String questionTypeId,QuestionTypeBean questionTypeBean)
	{
		//String sql="update lu_question_type set category_type_id = '"+questionTypeBean.getCategoryTypeId()+"',question_type_header ='"+questionTypeBean.getQuestionTypeHeading()+"' question_type='"+questionTypeBean.getQuestionType()+"' ,question_nature='"+questionTypeBean.getQuestionNature()+"' where question_type_id ='"+questionTypeId+"'";
		String sql="update lu_question_type set sub_category_type_id = '"+questionTypeBean.getCategoryTypeId()+"',question_type_header ='"+questionTypeBean.getQuestionTypeHeading()+"' where question_type_id ='"+questionTypeId+"'";
		jdbcTemplate.update(sql);
	}
	
	public String validateQuestionType(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select question_type_id from lu_question_type where question_type_header='"+request.getParameter("QuestionType")+"'";
			List<QuestionTypeBean> list=jdbcTemplate.query(sql, new RowMapper(){
					public QuestionTypeBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						QuestionTypeBean bean=new QuestionTypeBean();
						bean.setQuestionTypeId(rs.getString(1));
						return bean;
					}
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateCountryName() "+e.toString());	
		}		
		return target;
	}
	
}
