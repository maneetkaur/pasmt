package com.admin.cultureQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.util.StringUtils;

import com.admin.category.CategoryBean;
import com.admin.globalQuestion.GlobalAnswerMapper;
import com.admin.globalQuestion.GlobalQuestion;
import com.admin.globalQuestion.GlobalQuestionBean;
import com.admin.globalQuestion.GlobalQuestionDAO;
import com.admin.globalQuestion.GlobalQuestionServ;
import com.admin.globalQuestion.GlobalSubQuestionBean;
import com.admin.questionType.QuestionTypeBean;
import com.admin.questionType.QuestionTypeRowMapper;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Repository
@Transactional
public class CultureQuestionDAOImpl implements CultureQuestionDAO {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	CultureQuestionServ cultureQuesServ;
	@Autowired
	GlobalQuestionDAO globalquesDao;
	
	
	@Autowired
	@Qualifier("dataSource")
	
	public void setJdbcTemplate(DataSource dataSource)
	{
		System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	
	public List<CultureQuestionBean> getCultureQuestionList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qryPart="",qryStartPart="",sql1="",qryLastPart="";
		StringBuffer sb=new StringBuffer();
		List<CultureQuestionBean> questionList=new ArrayList<CultureQuestionBean>();
		int total_rows;
		String sql="";
		try
			{
			sb.append(" and ");
			if(session.getAttribute("cultureQuestionBean")!=null)
			{
				CultureQuestionBean questionBean=(CultureQuestionBean)session.getAttribute("cultureQuestionBean");	
				if(questionBean.getCultureId()!=null)
	 		   	{
		 			if(!questionBean.getCultureId().equals("NONE"))
		 			{
		 				sb.append(" qu.culture_id = '"+questionBean.getCultureId()+"'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(questionBean.getQuestionTypeId()!=null)
	 		   {
		 			if(!questionBean.getQuestionTypeId().equals("NONE"))
		 			{
		 				sb.append(" qu.question_type_id= '"+questionBean.getQuestionTypeId()+"'").append(" and ");
		 				
		 			}
	 		   } 
	 		  if(questionBean.getCategoryId()!=null)
	 		   {
		 			if(!questionBean.getCategoryId().equals("NONE"))
		 			{
		 				sb.append(" qty.sub_category_type_id = '"+questionBean.getCategoryId()+"'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select qu.question_id,cul.culture_id,con.country_code,lang.lang_code,ct.sub_category_type,qty.question_type_header,qu.status from lu_question qu,lu_culture cul,lu_language lang,lu_country con,lu_sub_category_type ct,lu_question_type qty where qu.question_type_id=qty.question_type_id and qty.sub_category_type_id=ct.sub_category_type_id and cul.culture_id=qu.culture_id and cul.country_id=con.country_id and cul.lang_id=lang.lang_id ";
	 			qryLastPart="order by qu.question_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart; 
	 			System.out.println(sql);
				questionList=jdbcTemplate.query(sql,new CultureQuestionRowMapper());
				
				
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_question qu,lu_culture cul,lu_language lang,lu_country con,lu_sub_category_type ct,lu_question_type qty where qu.question_type_id=qty.question_type_id and qty.sub_category_type_id=ct.sub_category_type_id and cul.culture_id=qu.culture_id and cul.country_id=con.country_id and cul.lang_id=lang.lang_id ";
				sql=qryStartPart+qryPart;
				total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
			}
			catch(Exception e)
			{
				System.out.println("exception in getCultureQuestionList--> "+e.toString());
			}
			finally
			{
				qryPart=null;qryStartPart=null;sql1=null;qryLastPart=null;
				sb=null;
				sql=null;
				
			}
		session.removeAttribute("questionBean");
		return questionList;
	}
	public List<CommonBean> findCategoryList(HttpServletRequest request)
	{
		HttpSession session=request.getSession();
		List<CommonBean> categoryList=new ArrayList<>();
		List<String> languageId;
		String sql,sql1,cultureId;
		cultureId=request.getParameter("cultureId");
		try
		{
			sql1="select lang_id from lu_culture where culture_id='"+cultureId+"'";
			languageId=jdbcTemplate.query(sql1, new RowMapper() {
				public String mapRow(ResultSet rs, int rowNumber)throws SQLException  {
					String langId="";
					langId=rs.getString("lang_id");
					return langId;
				}
			});
			if(languageId!=null && languageId.size()>0)
			{
				sql="select ct.sub_category_type_id,ct.sub_category_type from lu_sub_category_type ct,lu_sub_category c where c.sub_category_type_id=ct.sub_category_type_id and c.language_id='"+languageId.get(0)+"'";
				categoryList=jdbcTemplate.query(sql,new RowMapper() {
					public CommonBean mapRow(ResultSet rs, int rowNumber)throws SQLException  {
						CommonBean bean=new CommonBean();
						bean.setId(rs.getString("ct.sub_category_type_id"));
						bean.setName(rs.getString("ct.sub_category_type"));
						return bean;
					}
				});
			}
			
		}
		catch(Exception e)
		{
			System.out.println("Exception in findCategoryList()--> "+e.toString());	
		}
		finally
		{
			sql=null;
			sql1=null;
		}
		session.setAttribute("cultureId", cultureId);
		return categoryList;
	}
	/***** get question ,answer,parent question and subquestion text from global culture  ********/
	public CultureQuestion getQuestionText(String questionTypeId,String type,String cultureId)
	{ 
		String sql="",questionTypeValue[],sql1,sql2,questionId,sql3,sql4,parentAnswerId="";
		String answerIdList[],answertext;
		List<String> answervalues=new ArrayList<>();
		questionTypeValue=type.split(",");
		List<CommonBean> questionText=new ArrayList<>();
		List<CommonBean> globalAnswerList=new ArrayList<>();
		List<CommonBean> gridSubquestionList=new ArrayList<>();
		List<CommonBean> parentAnswerList=new ArrayList<>();
		List<String> parentGlobalAnswerList=new ArrayList<>();
		List<String> parentQuestionId=new ArrayList<>();
		GlobalQuestionBean globalBean=new GlobalQuestionBean();
		CultureQuestion cultureQuestion=new CultureQuestion();
		try
		{
			/***** find question and global question text  *******/
			sql="select question_id,question,sub_question from lu_question where global_question_id=0 and culture_id=0 and question_type_id='"+questionTypeId+"'";
			questionText=jdbcTemplate.query(sql, new RowMapper() {
				public CommonBean mapRow(ResultSet rs, int rowNumber)throws SQLException  {
					CommonBean bean=new CommonBean();
					bean.setId(rs.getString("question_id"));
					bean.setName(rs.getString("question"));
					bean.setCode(rs.getString("sub_question"));
					return bean;
				}
			});
			questionId=questionText.get(0).getId();
			
			/****** find global answer *********/
			if(questionTypeValue.length<=1)
			{
				sql1="select global_answer_id,answer from xref_global_answer where question_id='"+questionId+"'";
				globalAnswerList=jdbcTemplate.query(sql1, new RowMapper() {
					public CommonBean mapRow(ResultSet rs, int rowNumber)throws SQLException  {
						CommonBean bean=new CommonBean();
						bean.setId(rs.getString("global_answer_id"));
						bean.setName(rs.getString("answer"));
						return bean;
					}
				});
			}
			/****** find subquestion if question is type of grid *********/
			if(questionTypeValue[0].equals("both") || questionTypeValue[0].equals("grid") || questionTypeValue[0].equals("ParentChild") )
			{
				sql2="select grid_subquestion_id,subquestion from lu_grid_subquestions where question_id='"+questionId+"'";
				gridSubquestionList=jdbcTemplate.query(sql2, new RowMapper() {
					public CommonBean mapRow(ResultSet rs, int rowNumber)throws SQLException  {
						CommonBean bean=new CommonBean();
						bean.setId(rs.getString("grid_subquestion_id"));
						bean.setName(rs.getString("subquestion"));
						return bean;
					}
				});
			}
			/***** find parent question answer if question is child *********/
			if(questionTypeValue[0].equals("both")|| questionTypeValue[0].equals("multipleChild")|| questionTypeValue[0].equals("child"))
			{
				sql3="select parent_question_id,parent_question_answer_id from xref_parent_child_question where child_question_id='"+questionId+"'";
				parentAnswerList=jdbcTemplate.query(sql3, new RowMapper() {
					public CommonBean mapRow(ResultSet rs, int rowNumber)throws SQLException  {
						CommonBean bean=new CommonBean();
						bean.setId(rs.getString("parent_question_id"));
						bean.setName(rs.getString("parent_question_answer_id"));
						return bean;
					}
				});
				if(parentAnswerList!=null && parentAnswerList.size()>0)
				{
					sql4="select question_id from lu_question where global_question_id='"+parentAnswerList.get(0).getId()+"' and culture_id='"+cultureId+"'";
					parentQuestionId=jdbcTemplate.query(sql4, new RowMapper() {
						public String mapRow(ResultSet rs, int rowNumber)throws SQLException  {
							return rs.getString("question_id");
						}
					 });
					answerIdList=parentAnswerList.get(0).getName().split(",");
					if(parentQuestionId!=null && parentQuestionId.size()>0 && answerIdList.length>0)
					{
						for(int i=0;i<answerIdList.length;i++)
						{
							sql="select question_answer_id from lu_question_answer where global_answer_id='"+answerIdList[i]+"' and question_id='"+parentQuestionId+"'";
							answervalues=jdbcTemplate.query(sql,new RowMapper() {
								public String mapRow(ResultSet rs, int rowNumber)throws SQLException  {
									return rs.getString("question_answer_id");
								}
							});
							answertext=StringUtils.collectionToCommaDelimitedString(answervalues);
							parentGlobalAnswerList.add(answertext);
						}
						if(parentGlobalAnswerList!=null && parentGlobalAnswerList.size()>0)
						{
							parentAnswerId=parentGlobalAnswerList.get(0);
							for(int i=1;i<parentGlobalAnswerList.size();i++)
							{
								parentAnswerId=parentAnswerId+","+parentGlobalAnswerList.get(i);
							}
							
						}
						System.out.println("parent question id-->"+parentQuestionId);
						cultureQuestion.setParentQuestionId(parentQuestionId.get(0));
						cultureQuestion.setParentAnswerId(parentAnswerId);
						
						
					}
				}
				cultureQuestion.setQuesType("child");
			}
			cultureQuestion.setGlobalAnswerList(globalAnswerList);
			cultureQuestion.setSubQuestionList(gridSubquestionList);
			cultureQuestion.setGlobalQuestionId(questionId);
			cultureQuestion.setGlobalQuestionText(questionText.get(0).getName());
			cultureQuestion.setGlobalSubQuesText(questionText.get(0).getCode());
		}
		catch(Exception e)
		{
			System.out.println("exception in getQuestionText()-->"+e);
		}
		finally
		{
			sql=null;
			sql1=null;
			sql2=null;
			sql3=null;
			sql4=null;
		}
		return cultureQuestion;
	}
	
	
	public String addCultureQuestionInfo(CultureQuestionBean questionBean,HttpServletRequest request)
	{
		String msg="error",filename="",filepath="";
		String sql="",sql1="",sql2="",query="",sql3="",sql4="",query1="";
		List<CommonBean> answerList=new ArrayList<>();
		List<CultureQuestionBean> list=new ArrayList<>();
		List<CommonBean> subQuestions=new ArrayList<>();
		CommonsMultipartFile csv=questionBean.getAnswerfile();
		System.out.println("csv file-->"+csv.getFileItem().getName());
		StringBuffer sb=new StringBuffer("insert into lu_question_answer(question_id,global_answer_id,answer,created_on) values");
		StringBuffer sb1=new StringBuffer("insert into lu_grid_subquestions(question_id,subquestion,created_on) values");
		if(questionBean.getCultureQuestionId()!=null)
		{
			sql="update lu_question set question_type_id = '"+questionBean.getQuestionTypeId()+"',question='"+questionBean.getQuestion()+"',registration_enable='"+questionBean.getRegEnable()+"' where question_id ="+questionBean.getQuestionId();
			jdbcTemplate.update(sql);
		}
		else
		{
			 sql4="insert into lu_question(question_type_id,global_question_id,culture_id,question,sub_question,registration_enable,created_on) values(?,?,?,?,?,?,?)";
			 query="select question_id from lu_question where question_type_id='"+questionBean.getQuestionTypeId()+"' and culture_id='"+questionBean.getCultureId()+"' and global_question_id='"+questionBean.getQuestionId()+"'";
			 query1="insert into xref_parent_child_question(parent_question_id,parent_question_answer_id,child_question_id,created_on) values(?,?,?,?)";	
			
		}
		try
		{ 
			//System.out.println("before insert culture question");
			if(questionBean.getCiltureSubQuesText()!=null && !questionBean.getCiltureSubQuesText().equals(""))
			{
				jdbcTemplate.update(sql4, new Object[]{
					questionBean.getQuestionTypeId(),questionBean.getQuestionId(),questionBean.getCultureId(),questionBean.getCultureQuestion(),questionBean.getCiltureSubQuesText(),questionBean.getRegEnable(),new Date()});
			}
			else
			{
				jdbcTemplate.update(sql4, new Object[]{
						questionBean.getQuestionTypeId(),questionBean.getQuestionId(),questionBean.getCultureId(),questionBean.getCultureQuestion(),null,questionBean.getRegEnable(),new Date()});
			}
			//System.out.println("after insert culture question");
			list=jdbcTemplate.query(query, new RowMapper()
			{
				public CultureQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					CultureQuestionBean bean=new CultureQuestionBean();
					bean.setCultureQuestionId(rs.getString("question_id"));
					return bean;
				}
			});
			
			
		   /******* insert anwsers in question_anwser table ********/
		    
			if(questionBean.getCultureAnswer()!=null && questionBean.getCultureAnswer().size()>0)
			{
				if(csv.getFileItem().getName()!="")
				{
					FileItem items= csv.getFileItem();
					//System.out.println("fileitem-->"+items);
					String content=items.getString();
					System.out.println("content-->"+content);
					String[] row=content.split("\n");
					int k=0;
					for(int j=1;j<questionBean.getCultureAnswer().size();j++)
					{
						if(k<row.length)
						{
							String answer=CommonFunction.filterWithBackSlash(row[k]);
							//System.out.println("answer value after filter--->"+answer);
							sb.append("('"+list.get(0).getCultureQuestionId()+"','"+questionBean.getCultureAnswer().get(j).getId()+"','"+answer+"',now()),");
						}
						else
						{
							sb.append("('"+list.get(0).getCultureQuestionId()+"','"+questionBean.getCultureAnswer().get(j).getId()+"','',now()),");
						}
						k++;
					}
				} 
				else
				{
					//answerList=cultureQuesServ.removeRow(questionBean);
					answerList=questionBean.getCultureAnswer();
					//System.out.println("size-->"+answerList.size());
					if(answerList!=null && answerList.size()>0 )
					{
						for(int i=1;i<answerList.size();i++)	
						{
							if(answerList.get(i).getName()!=null)
							{
								sb.append("('"+list.get(0).getCultureQuestionId()+"','"+answerList.get(i).getId()+"','"+answerList.get(i).getName()+"',now()),");
							}
							else
							{
								sb.append("('"+list.get(0).getCultureQuestionId()+"','"+answerList.get(i).getId()+"','',now()),");
							}
									
						}  
							
					}
				
				}
				sql1=sb.toString();
				 if(sql1.charAt(sql1.length()-1)==','){
					  sql1 = sql1.substring(0, sql1.length()-1);
					  jdbcTemplate.update(sql1);
				 }
			}
				
				
				
					
				   /******* insert subquestion in grid_subquestion table ********/
				    
					if(questionBean.getSubQuestionList()!=null && questionBean.getSubQuestionList().size()>0)
					{
						//subQuestions=cultureQuesServ.removeSubquestionRow(questionBean);
						subQuestions=questionBean.getSubQuestionList();
						//System.out.println("size-->"+answerList.size());
						if(subQuestions!=null && subQuestions.size()>0)
						{
							for(int i=0;i<subQuestions.size();i++)	
							{
								if(subQuestions.get(i).getName()!=null)
								{
									sb1.append("('"+list.get(0).getCultureQuestionId()+"','"+subQuestions.get(i).getName()+"',now()),");
								}
								else
								{
									sb1.append("('"+list.get(0).getCultureQuestionId()+"','',now()),");
								}
										
							}  
								
						}
						sql3=sb1.toString();
						 if(sql3.charAt(sql3.length()-1)==','){
							  sql3 = sql3.substring(0, sql3.length()-1);
							 }
						 System.out.println("insert culture question query-->"+sql3);
						 jdbcTemplate.update(sql3);
					}
					
					
							
		    /******* insert parent question info ******/
				if(questionBean.getParentQuestionId()!=null && questionBean.getParentAnswerId().length()>0)
				{
					jdbcTemplate.update(query1,new Object[]{
							questionBean.getParentQuestionId(),questionBean.getParentAnswerId(),list.get(0).getCultureQuestionId(),new Date()});	
				}
			
			msg="success";
			
		}
		catch(Exception e)
		{
			System.out.println("exception in addCultureQuestionInfo() in question --> "+e.toString());
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg; 
	}
	
	public String updateCultureQuestionStatus(HttpServletRequest request)
	{

		List<CultureQuestionBean> list=new ArrayList<CultureQuestionBean>();
		//System.out.println("in ChangeStatus");
		String sql="select status from lu_question where question_id='"+request.getParameter("questionId")+"'";
		//System.out.println("query "+sql);
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public CultureQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				CultureQuestionBean bean=new CultureQuestionBean();
				bean.setStatus(rs.getString(1));
				return bean;
				
			}
		});
		if(Integer.parseInt(list.get(0).getStatus().toString())==1)
		{
			jdbcTemplate.update("update lu_question set status='0' where question_id='"+request.getParameter("questionId")+"'");
		}
		else
		{
			jdbcTemplate.update("update lu_question set status='1' where question_id='"+request.getParameter("questionId")+"'");
		}
		return "Success";
	}
	
	/**** find culturequestiondetail to display on edit page ********/
	public CultureQuestionBean getCultureQuestionDetail(HttpServletRequest request)
	{
		HttpSession session=request.getSession();
		String sql="",sql1="",sql2="",questionType,sql3="",sql4="";
		CultureQuestionBean cultureQuesBean=new CultureQuestionBean();
		CultureQuestionBean cultureQues=new CultureQuestionBean();
		CultureQuestion globalQuestionDetail=new CultureQuestion();
		List<CommonBean> cultureAnswerList=new ArrayList<>();
		List<CommonBean> cultureSubquestionList=new ArrayList<>();
		List<String> globalsubquestion=new ArrayList<>();
		String questionValues;
		sql="select question_type_id,culture_id,global_question_id,question,registration_enable from lu_question where question_id='"+request.getParameter("cultureQuestionId")+"'";
		System.out.println("sql-->"+sql);
		cultureQuesBean=jdbcTemplate.queryForObject(sql, new RowMapper() {
			public CultureQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				CultureQuestionBean bean=new CultureQuestionBean();
				bean.setQuestionTypeId(rs.getString("question_type_id"));
				bean.setCultureId(rs.getString("culture_id"));
				bean.setQuestionId(rs.getString("global_question_id"));
				bean.setCultureQuestion(rs.getString("question"));
				bean.setRegEnable(Integer.parseInt(rs.getString("registration_enable")));				
				return bean;
			}
		});
		
		sql1="select cty.sub_category_type,qu.question_type_header from lu_question_type qu,lu_sub_category_type cty where qu.sub_category_type_id=cty.sub_category_type_id and qu.question_type_id='"+cultureQuesBean.getQuestionTypeId()+"'";
		
		cultureQues=jdbcTemplate.queryForObject(sql1,  new RowMapper() {
			public CultureQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				CultureQuestionBean bean=new CultureQuestionBean();
				bean.setCategoryId(rs.getString("cty.sub_category_type"));
				bean.setQuestionType(rs.getString("qu.question_type_header"));
				return bean;
			}
		});
		cultureQuesBean.setCategoryType(cultureQues.getCategoryId());
		cultureQuesBean.setQuestionType(cultureQues.getQuestionType());
		
		/***** find detail regarding global question **********/
		sql2="select question from lu_question where question_id='"+cultureQuesBean.getQuestionId()+"'";
		questionValues=jdbcTemplate.queryForObject(sql2,String.class);
		cultureQuesBean.setQuestion(questionValues);
		questionType=globalquesDao.getQuestionTypeDetail(cultureQuesBean.getQuestionTypeId());
		globalQuestionDetail.setQuesType(questionType);
		
		cultureQuesBean.setCultureQues(globalQuestionDetail);
		//globalQuestionDetail=getQuestionText(cultureQuesBean.getQuestionTypeId(),questionType,cultureQuesBean.getCultureId());
		
		/****** find detail regarding culture question ********/
		// get answer list
		sql3="select qans.question_answer_id,gans.answer,qans.answer from lu_question_answer qans,xref_global_answer gans where qans.global_answer_id=gans.global_answer_id and qans.question_id='"+request.getParameter("cultureQuestionId")+"'";
		cultureAnswerList=jdbcTemplate.query(sql3,  new RowMapper() {
			public CommonBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString("qans.question_answer_id"));
				bean.setName(rs.getString("gans.answer"));
				bean.setCode(rs.getString("qans.answer"));
				return bean;
			}
		});
		cultureQuesBean.setCultureAnswer(cultureAnswerList);
		
		//get subquestionlist
		sql4="select grid_subquestion_id,subquestion from lu_grid_subquestions where question_id='"+request.getParameter("cultureQuestionId")+"'";
		cultureSubquestionList=jdbcTemplate.query(sql4,  new RowMapper() {
			public CommonBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString("grid_subquestion_id"));
				//bean.setName(rs.getString("gans.answer"));
				bean.setCode(rs.getString("subquestion"));
				return bean;
			}
		});
		sql3="select subquestion from lu_grid_subquestions where question_id='"+cultureQuesBean.getQuestionId()+"'";
		globalsubquestion=jdbcTemplate.query(sql3,  new RowMapper() {
			public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
				return rs.getString("subquestion"); 
			}
		});
		for(int i=0;i<globalsubquestion.size();i++)
		{
			cultureSubquestionList.get(i).setName(globalsubquestion.get(i));
		}
		
		
		cultureQuesBean.setCultureAnswer(cultureAnswerList);
		cultureQuesBean.setSubQuestionList(cultureSubquestionList);
		session.setAttribute("cultureQuestionId", request.getParameter("cultureQuestionId"));
		sql1=null;
		sql2=null;
		sql3=null;
		sql4=null;
		questionType=null;
		return cultureQuesBean;
	}
	
	/******** update culture question detail ***********/
	public void editCultureQuestionDetail(String questionId,CultureQuestionBean questionBean)
	{
		String sql="",query="",query1="";
		try
		{
			sql="update lu_question set question ='"+questionBean.getCultureQuestion()+"',registration_enable='"+questionBean.getRegEnable()+"' where question_id='"+questionId+"'";
			
			jdbcTemplate.update(sql);
			System.out.println("sl in update query is-->"+sql);
			
			/***** for answer *********/	
			if(questionBean.getCultureAnswer()!=null && questionBean.getCultureAnswer().size()>0)
			{
				if(questionBean.getCultureAnsIdList().length()>0)
				{
					String[] answerids=questionBean.getCultureAnsIdList().split(",");
					CommonBean newCultureAnswer;
					/******** for update already exists rows *******/
					if(answerids.length>0)
					{
						for(int i=0;i<answerids.length;i++)
						{
							newCultureAnswer=questionBean.getCultureAnswer().get(Integer.parseInt(answerids[i]));
							query1="update lu_question_answer set answer='"+newCultureAnswer.getCode()+"'where question_answer_id='"+newCultureAnswer.getId()+"'";
							jdbcTemplate.update(query1);
						}
					}
				}
			}
			/****** edit subquestion *********/
			
			if(questionBean.getSubQuestionList()!=null && questionBean.getSubQuestionList().size()>0)
			{
				if(questionBean.getCultureSubQuesIdList().length()>0)
				{
					String[] questionids=questionBean.getCultureSubQuesIdList().split(",");
					CommonBean newCultureSubquestion;
					/******** for update already exists rows *******/
					if(questionids.length>0)
					{
						for(int i=0;i<questionids.length;i++)
						{
							newCultureSubquestion=questionBean.getSubQuestionList().get(Integer.parseInt(questionids[i]));
							query="update lu_grid_subquestions set subquestion='"+newCultureSubquestion.getCode()+"'where grid_subquestion_id='"+newCultureSubquestion.getId()+"'";
							jdbcTemplate.update(query);
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		sql=null;
		query=null;
		query1=null;
	}
	
	public void copyCultureInfo(CultureQuestionBean questionBean)
	{
		String sql="",sql1="",sql2="",sql3="";
		String query="",query2="",questionIds="",destAnswer="",tempString="",destChild="",destAns="",temp;
		String []subCatIds;
		List<String> tempValue=new ArrayList<>();
		List<String> newCultureValue=new ArrayList<>();
		List<CommonBean> srcParentList=new ArrayList<>();
		List<String> desPrentList=new ArrayList<>();
		List<String> globalChildId=new ArrayList<>();
		List<String> destQuesid=new ArrayList<>();
		List<String> destGlobalAnsd=new ArrayList<>();
		List<String> globalChildQustId=new ArrayList<>();
		List<String> destQuesChildid=new ArrayList<>();
		List<String> destAnsId=new ArrayList<>();
		try
		{
			subCatIds=questionBean.getSubCategory().split(",");
			sql="insert into lu_question(question_type_id,global_question_id,culture_id,question,created_on) select question_type_id,global_question_id,'"+questionBean.getDestinationCultureId()+"',question,now() from lu_question where culture_id='"+questionBean.getCultureId()+"'";
			sql2="insert into xref_parent_child_question(parent_question_id,parent_question_answer_id,child_question_id,created_on) values(?,?,?,?)";
			System.out.println("copy sql-->"+sql);
			//jdbcTemplate.update(sql);
			
			/***** get question id for source culture ********
			 * 
			 */
			System.out.println("123");
			StringBuffer sb=new StringBuffer();
			System.out.println("123hghjgh");
			for(int k=0;k<subCatIds.length;k++)
			{
				query2="select qu.question_id from lu_question qu,lu_question_type qty ,lu_sub_category_type scat where qu.question_type_id=qty.question_type_id and qty.sub_category_type_id=scat.sub_category_type_id and scat.sub_category_type_id='"+subCatIds[k]+"' order by question_id ASC";
				System.out.println("sql quesy-->"+query2);
				tempValue=jdbcTemplate.query(query2, new RowMapper() {
					public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
						return rs.getString("qu.question_id");
					}
				});
				//System.out.println("before"+tempValue);
				sb.append(tempValue);
				//System.out.println("after");
			}
			String tempquesValue=sb.toString().replace("[", "").replace("]", "");
			System.out.println("tempquesvalue-->"+tempquesValue);
			/***** get question id for destination culture ********
			 * 
			 */
			query="select question_id from lu_question where culture_id='"+questionBean.getDestinationCultureId()+"' order by question_id ASC";
			newCultureValue=jdbcTemplate.query(query,  new RowMapper() {
				public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
					return rs.getString("question_id");
				}
			});
			
			/******* copy answer ******/
			for(int i=0;i<newCultureValue.size();i++)
			{
				sql1="insert into lu_question_answer(question_id,global_answer_id,answer,created_on) select '"+newCultureValue.get(i)+"',global_answer_id,answer,now()from lu_question_answer where question_id in ("+tempquesValue+")";
				System.out.println("answer add->"+sql1);
				jdbcTemplate.update(sql1);
			}
			
			/**** copr grid subquestion *********/
			for(int i=0;i<tempValue.size()&& i<newCultureValue.size();i++)
			{
				sql3="insert into lu_grid_subquestions(question_id,subquestion,created_on) select '"+newCultureValue.get(i)+"',subquestion,now() from lu_grid_subquestions where question_id='"+tempValue.get(i)+"'";
				jdbcTemplate.update(sql3);
			}
			
			/****** insert parent question *******/
			questionIds=StringUtils.collectionToCommaDelimitedString(tempValue);
			
			System.out.println("question ids-->"+questionIds);
			sql="select parent_question_id,parent_question_answer_id,child_question_id from xref_parent_child_question where child_question_id in("+questionIds+") order by parent_child_question_id ASC";
			srcParentList=jdbcTemplate.query(sql, new RowMapper() 
			{	
				public CommonBean mapRow(ResultSet rs, int rowNumber) throws SQLException 
				{
					CommonBean bean=new CommonBean();
					bean.setId(rs.getString("parent_question_id"));
					bean.setName(rs.getString("parent_question_answer_id"));
					bean.setCode(rs.getString("child_question_id"));
					return bean; 
				}
			});
			
			
			for(int i=0;i<srcParentList.size();i++)
			{
				
				/*  Get Parent Destincation Question Id block start*/
				sql1="select global_question_id from lu_question where question_id='"+srcParentList.get(i).getId()+"'";
				globalChildId=jdbcTemplate.query(sql1, new RowMapper() 
				{	
					public String mapRow(ResultSet rs, int rowNumber) throws SQLException 
					{
						return rs.getString("global_question_id");
					}
				});
				tempString=StringUtils.collectionToCommaDelimitedString(globalChildId);
				//System.out.println("globalChildId-->"+tempString);
				
				sql1="select question_id from lu_question where global_question_id='"+tempString+"' and culture_id='"+questionBean.getDestinationCultureId()+"'";
				destQuesid=jdbcTemplate.query(sql1, new RowMapper() 
				{	
					public String mapRow(ResultSet rs, int rowNumber) throws SQLException 
					{
						return rs.getString("question_id");
					}
				});
				tempString=StringUtils.collectionToCommaDelimitedString(destQuesid);
				//System.out.println("tempString-->"+tempString);
				/*  Get Parent Destincation Question Id block end*/
				
				
				/*  Get Child Destincation Question Id block start*/
				sql1="select global_question_id from lu_question where question_id='"+srcParentList.get(i).getCode()+"'";
				globalChildQustId=jdbcTemplate.query(sql1, new RowMapper() 
				{	
					public String mapRow(ResultSet rs, int rowNumber) throws SQLException 
					{
						return rs.getString("global_question_id");
					}
				});
				temp=StringUtils.collectionToCommaDelimitedString(globalChildQustId);
				//System.out.println("globalChildQustId-->"+temp);
				
				sql1="select question_id from lu_question where global_question_id='"+temp+"' and culture_id='"+questionBean.getDestinationCultureId()+"'";
				destQuesChildid=jdbcTemplate.query(sql1, new RowMapper() 
				{	
					public String mapRow(ResultSet rs, int rowNumber) throws SQLException 
					{
						return rs.getString("question_id");
					}
				});
				destChild=StringUtils.collectionToCommaDelimitedString(destQuesChildid);
				//System.out.println("destChild-->"+destChild);
				/*  Get Child Destincation Question Id block end*/
				
				
				
				/*  Get Parent Question Anwer Id block start*/
			//System.out.println("srcParentList.get(0).getName()-->"+srcParentList.get(0).getId()+","+srcParentList.get(0).getName()+","+srcParentList.get(0).getCode());
				sql1="select global_answer_id from lu_question_answer where question_answer_id in("+srcParentList.get(i).getName()+") ";
				System.out.println("sql1-->"+sql1);
					destGlobalAnsd=jdbcTemplate.query(sql1, new RowMapper() 
					{	
						public String mapRow(ResultSet rs, int rowNumber) throws SQLException 
						{
							return rs.getString("global_answer_id");
						}
					});
				//System.out.println("destGlobalAnsd-->"+destGlobalAnsd);
				destAnswer=StringUtils.collectionToCommaDelimitedString(destGlobalAnsd);
				System.out.println("destAnswer-->"+destAnswer);
				if( destAnswer.length()>0)
				{
					sql1="select question_answer_id from lu_question_answer where global_answer_id in("+destAnswer+") and question_id='"+tempString+"'";
					System.out.println("sql1-->"+sql1);
					destAnsId=jdbcTemplate.query(sql1, new RowMapper() 
					{	
						public String mapRow(ResultSet rs, int rowNumber) throws SQLException 
						{
							return rs.getString("question_answer_id");
						}
					});
				}
				
				destAns=StringUtils.collectionToCommaDelimitedString(destAnsId);
				System.out.println("destAns-->"+destAns);
				/*  Get Parent Question Anwer Id block end*/
				
				if(destAns.length()>0 && destChild.length()>0)
				{
					sql1="insert into xref_parent_child_question(parent_question_id,parent_question_answer_id,child_question_id,created_on) values('"+tempString+"','"+destAns+"','"+destChild+"',now())";
					
					jdbcTemplate.update(sql1);
				}
			}
			
		}
		catch(Exception e)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}
	public List<CommonBean> getGlobalAnswer(String questionId)
	{
		List<CommonBean> answerList=new ArrayList<CommonBean>();
		String globalQuesId,sql,sql1;
		//System.out.println("in ChangeStatus");
		//sql="select global_question_id from lu_question where question_id='"+questionId+"'";
		//System.out.println("query "+sql);
		//globalQuesId=jdbcTemplate.queryForObject(sql, String.class);	
		sql1="select global_answer_id,answer from xref_global_answer where question_id='"+questionId+"'";
		answerList=jdbcTemplate.query(sql1, new RowMapper() 
		{	
			public CommonBean mapRow(ResultSet rs, int rowNumber) throws SQLException 
			{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString("global_answer_id"));
				bean.setName(rs.getString("answer"));
				return bean;
			}
		});
		return answerList;
	}
}
