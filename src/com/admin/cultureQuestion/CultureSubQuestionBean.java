package com.admin.cultureQuestion;

public class CultureSubQuestionBean {
	
	private String globalSubQuesId,globalSubQues,cultureSubQuesId,cultureSubQues;

	public String getGlobalSubQuesId() {
		return globalSubQuesId;
	}

	public void setGlobalSubQuesId(String globalSubQuesId) {
		this.globalSubQuesId = globalSubQuesId;
	}

	public String getGlobalSubQues() {
		return globalSubQues;
	}

	public void setGlobalSubQues(String globalSubQues) {
		this.globalSubQues = globalSubQues;
	}

	public String getCultureSubQuesId() {
		return cultureSubQuesId;
	}

	public void setCultureSubQuesId(String cultureSubQuesId) {
		this.cultureSubQuesId = cultureSubQuesId;
	}

	public String getCultureSubQues() {
		return cultureSubQues;
	}

	public void setCultureSubQues(String cultureSubQues) {
		this.cultureSubQues = cultureSubQues;
	}

}
