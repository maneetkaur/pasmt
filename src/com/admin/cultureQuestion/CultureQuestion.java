package com.admin.cultureQuestion;

import java.util.List;

import com.admin.globalQuestion.GlobalQuestionBean;
import com.commonFunc.CommonBean;

public class CultureQuestion {
	
	private String globalQuestionId,globalQuestionText;
	private List<CommonBean> globalAnswerList;
	private List<CommonBean> subQuestionList;
	private String parentQuestionId,parentAnswerId,quesType,globalSubQuesText;
	
	public String getGlobalQuestionId() {
		return globalQuestionId;
	}
	
	public String getParentQuestionId() {
		return parentQuestionId;
	}
	public String getGlobalSubQuesText() {
		return globalSubQuesText;
	}

	public void setGlobalSubQuesText(String globalSubQuesText) {
		this.globalSubQuesText = globalSubQuesText;
	}
	public String getQuesType() {
		return quesType;
	}

	public void setQuesType(String quesType) {
		this.quesType = quesType;
	}

	public void setParentQuestionId(String parentQuestionId) {
		this.parentQuestionId = parentQuestionId;
	}

	public String getParentAnswerId() {
		return parentAnswerId;
	}

	public void setParentAnswerId(String parentAnswerId) {
		this.parentAnswerId = parentAnswerId;
	}

	public void setGlobalQuestionId(String globalQuestionId) {
		this.globalQuestionId = globalQuestionId;
	}
	public String getGlobalQuestionText() {
		return globalQuestionText;
	}
	public void setGlobalQuestionText(String globalQuestionText) {
		this.globalQuestionText = globalQuestionText;
	}
	public List<CommonBean> getGlobalAnswerList() {
		return globalAnswerList;
	}
	public void setGlobalAnswerList(List<CommonBean> globalAnswerList) {
		this.globalAnswerList = globalAnswerList;
	}
	public List<CommonBean> getSubQuestionList() {
		return subQuestionList;
	}
	public void setSubQuestionList(List<CommonBean> subQuestionList) {
		this.subQuestionList = subQuestionList;
	}
	
	


}
