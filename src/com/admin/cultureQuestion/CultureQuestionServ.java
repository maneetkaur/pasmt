package com.admin.cultureQuestion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.admin.globalQuestion.GlobalQuestion;
import com.admin.globalQuestion.GlobalQuestionBean;
import com.admin.globalQuestion.GlobalSubQuestionBean;
import com.commonFunc.CommonBean;

@Repository
public class CultureQuestionServ {

	/***** To Remove Empty Rows *****/
	public List<CommonBean> removeRow(CultureQuestionBean questionBean)
	{
		List<CommonBean> clist=new ArrayList<>();
		if(questionBean.getCultureAnswer().size()==1)
		{
			CommonBean tempList=questionBean.getCultureAnswer().get(0);
			if(tempList.getName().equals(""))
			{
				questionBean.setAnswerList(null);
			}
		}
		
		if(questionBean.getCultureAnswer()!=null)
		{
			for(CommonBean ques:questionBean.getCultureAnswer())
			{
				if(ques.getName()!=null )
				{
					clist.add(ques);
				}
			}
		}
		return clist;
	}
	/***** To Remove subquestion list Empty Rows *****/
	public List<CommonBean> removeSubquestionRow(CultureQuestionBean questionBean)
	{
		List<CommonBean> clist=new ArrayList<>();
		if(questionBean.getSubQuestionList().size()==1)
		{
			CommonBean tempList=questionBean.getSubQuestionList().get(0);
			if(tempList.getName().equals(""))
			{
				questionBean.setSubQuestionList(null);
			}
		}
		
		if(questionBean.getSubQuestionList()!=null)
		{
			for(CommonBean ques:questionBean.getSubQuestionList())
			{
				if(ques.getName()!=null )
				{
					clist.add(ques);
				}
			}
		}
		return clist;
	}

}
