package com.admin.cultureQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.admin.globalQuestion.GlobalQuestionBean;

public class CultureQuestionRowMapper implements RowMapper<CultureQuestionBean>{
	@Override
	public CultureQuestionBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		CultureQuestionBean bean=new CultureQuestionBean();
		String countrycode,langCode,cultureCode;
		bean.setQuestionId(rs.getString(1));
		bean.setCultureId(rs.getString(2));
		countrycode=rs.getString(3);
		langCode=rs.getString(4);
		cultureCode=langCode+"-"+countrycode;
		bean.setCultureCode(cultureCode);
		bean.setCategoryType(rs.getString(5));
		bean.setQuestionType(rs.getString(6));
		bean.setStatus(rs.getString(7));
		return bean;
	}

}
