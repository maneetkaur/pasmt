package com.admin.cultureQuestion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.admin.category.CategoryBean;
import com.admin.globalQuestion.GlobalQuestionBean;
import com.admin.globalQuestion.GlobalQuestionDAO;
import com.admin.questionType.QuestionTypeBean;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;
import com.sales.proposal.ProposalBean;

@Controller
public class CultureQuestionController{
	
	@Autowired
	CommonFunction commonFunc;
	
	@Autowired
	CultureQuestionDAO cultureQuesDao;
	
	@Autowired
	GlobalQuestionDAO globalQuesDao;
	
	@RequestMapping(value="/cultureQuestionView" ,method=RequestMethod.GET)
	public ModelAndView getQuestionList(HttpServletRequest request)
	{
		List<CommonBean> cultureList,categoryList,questionTypeList;
		List<CultureQuestionBean> cultureQuestionList=null;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		cultureList=commonFunc.getCultureList();
		categoryList=commonFunc.getSubCategoryTypeList();
		questionTypeList=commonFunc.getQuestionTypeList();
		cultureQuestionList=cultureQuesDao.getCultureQuestionList(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/cultureQuestion/cultureQuestionList");
		model.addObject("questionTypeList", questionTypeList);
		model.addObject("cultureList", cultureList);
		model.addObject("categoryList", categoryList);
		model.addObject("cultureQuestionList", cultureQuestionList);
		model.addObject("search_culture_question",new CultureQuestionBean());
		return model;
	}
	
	@RequestMapping(value="/searchCultureQuestion",method=RequestMethod.POST)
	public String searchQuestion(@ModelAttribute("search_culture_question")CultureQuestionBean questionBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("cultureQuestionBean", questionBean);
		return "redirect:/cultureQuestionView";
	}
	
	@RequestMapping(value="/addCultureQuestion",method=RequestMethod.GET)
	public ModelAndView addQuestionPage()
	{
		List<CommonBean> cultureList,categoryTypeList;
		//List<QuestionTypeBean> QuestionTypeList=new ArrayList<>();
		List<QuestionTypeBean> parentQuestionList=new ArrayList<>();
		cultureList=commonFunc.getCultureList();
		categoryTypeList=commonFunc.getSubCategoryTypeList();
		//parentQuestionList=cultureQuesDao.AddedQuestionType();
		//QuestionTypeList=globalQuesDao.getQuestionTypeList(id,1);
		ModelAndView model=new ModelAndView("admin/cultureQuestion/addCultureQuestion");
		model.addObject("cultureList", cultureList);
		model.addObject("categoryTypeList", categoryTypeList);
		//model.addObject("parentQuestionList",parentQuestionList);
		//model.addObject("QuestionTypeList", QuestionTypeList);
		model.addObject("add_culture_question",new CultureQuestionBean());
		return model;
	}
	@RequestMapping(value="/getCategoryList",method=RequestMethod.GET)
	public String getCategoryInfo(HttpServletRequest request,ModelMap model)
	{
		String chkQuestion="";
		List<CommonBean> CategoryList=new ArrayList<>();
		CategoryList=cultureQuesDao.findCategoryList(request);
		model.addAttribute("chkQuestion", chkQuestion);
		model.addAttribute("CategoryList", CategoryList);
		return "ajaxOperation/ajaxOperation";
		
	}
	
	@RequestMapping(value="/findQuestionTypeList",method=RequestMethod.GET)
	public String getQuestionTypeInfo(HttpServletRequest request,ModelMap model)
	{
		String chkQuestion="",categoryId="",cultureId="";
		HttpSession session=request.getSession();
		List<QuestionTypeBean> QuestionTypeList=new ArrayList<>();
		categoryId=request.getParameter("categoryId");
		cultureId=(String)session.getAttribute("cultureId");
		QuestionTypeList=globalQuesDao.getQuestionTypeList(cultureId,categoryId,1);
		model.addAttribute("QuestionTypeList", QuestionTypeList);
		return "ajaxOperation/ajaxOperation";
	
	}
	
	@RequestMapping(value="/findQuestionText",method=RequestMethod.GET)
	public String getQuestionText(HttpServletRequest request,ModelMap model)
	{
		HttpSession session=request.getSession();
		String questionTypeid=request.getParameter("questionTypeId");
		String cultureId=(String)session.getAttribute("cultureId");
		CultureQuestion questionText=new CultureQuestion();
		String questionType=globalQuesDao.getQuestionTypeDetail(questionTypeid);
		questionText=cultureQuesDao.getQuestionText(request.getParameter("questionTypeId"),questionType,cultureId);
		model.addAttribute("globalQuestionText",questionText);
		model.addAttribute("questionTypeValue",questionType);
		return "ajaxOperation/ajaxOperation";
		
	}
	
	@RequestMapping(value="/submitCultureQuestion",method=RequestMethod.POST)
	public String AddQuestion(@ModelAttribute("add_culture_question")CultureQuestionBean questionBean,ModelMap model,HttpServletRequest request)
	{
		String message=cultureQuesDao.addCultureQuestionInfo(questionBean,request);
		model.addAttribute("search_culture_question", new CultureQuestionBean());
		return "redirect:/cultureQuestionView";
	}
	
	@RequestMapping(value="/updateCultureQuestionStatus",method=RequestMethod.GET)
	public String updateQuestionStatus(@ModelAttribute("search_culture_question")CultureQuestionBean questionBean,HttpServletRequest request)
	{
		String checkStatus=cultureQuesDao.updateCultureQuestionStatus(request);
		return "redirect:/cultureQuestionView";
	}
	
	@RequestMapping(value="/updateCultureQuestion",method=RequestMethod.GET)
	public ModelAndView updateCultureQuestion(HttpServletRequest request)
	{
		CultureQuestionBean questionBean=cultureQuesDao.getCultureQuestionDetail(request);
		ModelAndView model=new ModelAndView("admin/cultureQuestion/updateCultureQuestion");
		model.addObject("edit_culture_question",questionBean);
		model.addObject("cultureList",commonFunc.getCultureList());
		return model;
	}	
	@RequestMapping(value="/editCultureQuestion",method=RequestMethod.POST)
	public String EditCultureQuestion(@ModelAttribute("edit_culture_question")CultureQuestionBean questionBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("cultureQuestionId");
		cultureQuesDao.editCultureQuestionDetail(id,questionBean);
		return "redirect:/cultureQuestionView";
	}
	
	@RequestMapping(value="/copyCultureQuestion",method=RequestMethod.GET)
	public ModelAndView copyCultureData(HttpServletRequest request)
	{
		ModelAndView model=new ModelAndView("admin/cultureQuestion/copyCultureInfo");
		model.addObject("cultureList", commonFunc.getCultureList());
		CultureQuestionBean culQuesBean=new CultureQuestionBean();
		if(request.getParameter("cultureId")!=null && !(request.getParameter("cultureId").equals("NONE")))
		{
			culQuesBean.setCultureId(request.getParameter("cultureId"));
			List<CategoryBean> categorylist = commonFunc.getCategories(request.getParameter("cultureId"));
			model.addObject("categoryList", categorylist);
		}
		model.addObject("copy_culture_question",culQuesBean);
		return model;
	}
	
	@RequestMapping(value="/saveCultureInfo",method=RequestMethod.POST)
	public String saveCultureDetail(@ModelAttribute("copy_culture_question")CultureQuestionBean questionBean,ModelMap model)
	{
		cultureQuesDao.copyCultureInfo(questionBean);
		model.addAttribute("search_culture_question", new CultureQuestionBean());
		return "redirect:/cultureQuestionView";
	}
	
	@RequestMapping(value="/getGlobalAnswerList",method=RequestMethod.GET)
	public ModelAndView getGlobalAnswerDetail(@ModelAttribute("add_culture_question")CultureQuestionBean questionBean,HttpServletRequest request)
	{
		String questionId=request.getParameter("questionId");
		List<CommonBean> globalAnswers=new ArrayList<>();
		globalAnswers=cultureQuesDao.getGlobalAnswer(questionId);
		System.out.println("global answer list-->"+globalAnswers);
		ModelAndView model=new ModelAndView("admin/cultureQuestion/downloadGobalAnswer");
		model.addObject("globalAnswers", globalAnswers);
		return model;
	}
	
	
	/*@RequestMapping(value="/previewCultureQuestion",method=RequestMethod.GET)
	public String getCultureQuestionPreview(HttpServletRequest request,ModelMap model)
	{
		//List<global> answerList=questionDao.getAnswerList(request);
		HttpSession session=request.getSession();
		Boolean checkPreview=true;
		//GlobalQuestionBean previewQuestions=new GlobalQuestionBean();
		List<CultureQuestionBean> previewCulQuestions=new ArrayList<>();
		previewCulQuestions=cultureQuesDao.getCultureQuestion(request.getParameter("cultureQuestionId"));
		System.out.println("list values--"+previewCulQuestions);
		model.addAttribute("checkPreview", checkPreview);
		session.setAttribute("previewQuestions", previewCulQuestions);
		System.out.println("value of checkPreview-->"+checkPreview);
		return "redirect:/cultureQuestionView";
		
	}*/
}
