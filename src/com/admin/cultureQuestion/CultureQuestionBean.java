package com.admin.cultureQuestion;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.admin.globalQuestion.GlobalQuestion;
import com.admin.globalQuestion.GlobalSubQuestionBean;
import com.commonFunc.CommonBean;

public class CultureQuestionBean {
	
	private String cultureId,categoryId,questionTypeId,cultureCode,categoryType,questionType,destinationCultureId;;
	private String questionId,status,question,cultureQuestion,cultureQuestionId,global_answer,globalSubQuesText,ciltureSubQuesText;
	private String parentQuestionId,parentAnswerId,cultureAnsIdList,cultureSubQuesIdList,answerTypeId;
	private String subCategory,completeCategory; 
	int regEnable;
	List<CommonBean> cultureAnswer;
	List<CommonBean> subQuestionList;
	CultureQuestion cultureQues=new CultureQuestion();
	CommonsMultipartFile Answerfile;
	
	
	public CultureQuestionBean()
	{
		cultureAnswer=LazyList.decorate(new ArrayList<CommonBean>(), new InstantiateFactory(CommonBean.class));
		subQuestionList=LazyList.decorate(new ArrayList<CommonBean>(), new InstantiateFactory(CommonBean.class));
	}
	
	public String getGlobalSubQuesText() {
		return globalSubQuesText;
	}

	public void setGlobalSubQuesText(String globalSubQuesText) {
		this.globalSubQuesText = globalSubQuesText;
	}

	public String getCiltureSubQuesText() {
		return ciltureSubQuesText;
	}

	public void setCiltureSubQuesText(String ciltureSubQuesText) {
		this.ciltureSubQuesText = ciltureSubQuesText;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getDestinationCultureId() {
		return destinationCultureId;
	}

	public void setDestinationCultureId(String destinationCultureId) {
		this.destinationCultureId = destinationCultureId;
	}

	public CommonsMultipartFile getAnswerfile() {
		return Answerfile;
	}
	public void setAnswerfile(CommonsMultipartFile answerfile) {
		Answerfile = answerfile;
	}
	public String getAnswerTypeId() {
		return answerTypeId;
	}
	public void setAnswerTypeId(String answerTypeId) {
		this.answerTypeId = answerTypeId;
	}
	public String getCultureAnsIdList() {
		return cultureAnsIdList;
	}
	public void setCultureAnsIdList(String cultureAnsIdList) {
		this.cultureAnsIdList = cultureAnsIdList;
	}
	public String getCultureSubQuesIdList() {
		return cultureSubQuesIdList;
	}
	public void setCultureSubQuesIdList(String cultureSubQuesIdList) {
		this.cultureSubQuesIdList = cultureSubQuesIdList;
	}
	public CultureQuestion getCultureQues() {
		return cultureQues;
	}
	public void setCultureQues(CultureQuestion cultureQues) {
		this.cultureQues = cultureQues;
	}
	public List<CommonBean> getAnswerList() {
		return cultureAnswer;
	}

	public void setAnswerList(List<CommonBean> answerList) {
		this.cultureAnswer = answerList;
	}
	public String getParentQuestionId() {
		return parentQuestionId;
	}
	public void setParentQuestionId(String parentQuestionId) {
		this.parentQuestionId = parentQuestionId;
	}
	public String getParentAnswerId() {
		return parentAnswerId;
	}
	public void setParentAnswerId(String parentAnswerId) {
		this.parentAnswerId = parentAnswerId;
	}
	public String getGlobal_answer() {
		return global_answer;
	}
	public void setGlobal_answer(String global_answer) {
		this.global_answer = global_answer;
	}
	public List<CommonBean> getSubQuestionList() {
		return subQuestionList;
	}

	public void setSubQuestionList(List<CommonBean> subQuestionList) {
		this.subQuestionList = subQuestionList;
	}

	public String getCultureQuestionId() {
		return cultureQuestionId;
	}

	public void setCultureQuestionId(String cultureQuestionId) {
		this.cultureQuestionId = cultureQuestionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
	
	public List<CommonBean> getCultureAnswer() {
		return cultureAnswer;
	}

	public void setCultureAnswer(List<CommonBean> cultureAnswer) {
		this.cultureAnswer = cultureAnswer;
	}

	public String getCultureQuestion() {
		return cultureQuestion;
	}

	public void setCultureQuestion(String cultureQuestion) {
		this.cultureQuestion = cultureQuestion;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getCultureCode() {
		return cultureCode;
	}

	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getCultureId() {
		return cultureId;
	}

	public void setCultureId(String cultureId) {
		this.cultureId = cultureId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getQuestionTypeId() {
		return questionTypeId;
	}

	public void setQuestionTypeId(String questionTypeId) {
		this.questionTypeId = questionTypeId;
	}
	public int getRegEnable()
	{
		return regEnable;
	}
	public void setRegEnable(int regEnable)
	{
		this.regEnable=regEnable;
	}

}
