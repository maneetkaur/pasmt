package com.admin.cultureQuestion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.admin.category.CategoryBean;
import com.admin.globalQuestion.GlobalQuestionBean;
import com.commonFunc.CommonBean;

public interface CultureQuestionDAO {
	
	public List<CultureQuestionBean> getCultureQuestionList(HttpServletRequest request,int startIndex,int range);
	public List<CommonBean> findCategoryList(HttpServletRequest request);
	public CultureQuestion getQuestionText(String questionTypeId,String type,String cultureId);
	public String addCultureQuestionInfo(CultureQuestionBean questionBean,HttpServletRequest request);
	public String updateCultureQuestionStatus(HttpServletRequest request);
	public CultureQuestionBean getCultureQuestionDetail(HttpServletRequest request);
	public void editCultureQuestionDetail(String questionId,CultureQuestionBean questionBean);
	public void copyCultureInfo(CultureQuestionBean questionBean);
	public List<CommonBean> getGlobalAnswer(String questionId);

}
