package com.admin.culture;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CultureRowMapper implements RowMapper<CultureBean>
{
	@Override
	public CultureBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		CultureBean bean=new CultureBean();
		bean.setCultureId(rs.getString(1));
		bean.setCountryId(rs.getString(2));
		bean.setCountry(rs.getString(3));
		bean.setLanguageId(rs.getString(4));
		bean.setLanguage(rs.getString(5));
		String languageCode=rs.getString(7);
		String countryCode=rs.getString(8);
		String cultureCode=languageCode+"-"+countryCode;
		bean.setCulture(cultureCode);
		bean.setOsbtStatus(rs.getString(6));
		return bean;
	}
}
