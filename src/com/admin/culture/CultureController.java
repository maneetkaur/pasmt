package com.admin.culture;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.admin.country.CountryBean;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class CultureController {
	
	@Autowired
	CommonFunction commonFunc;
	@Autowired
	CultureDAO cultureDao;
	
	static final Logger LOG = LoggerFactory.getLogger(CultureController.class);
	
	@RequestMapping(value="/cultureList",method=RequestMethod.GET)
	public ModelAndView cultureList(HttpServletRequest request)
	{
		List<CultureBean> list=null;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		list=cultureDao.listCulture(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/culture/cultureList");
		model.addObject("cultureList",list);
		model.addObject("search",new CultureBean());
		return model;
	}
	
	@RequestMapping(value="/searchCulture",method=RequestMethod.POST)
	public String searchCulture(@ModelAttribute("search")CultureBean CultureBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("cultureBean",CultureBean);
		return "redirect:/cultureList";
	}
	
	@RequestMapping(value="/addCulture", method=RequestMethod.GET)
	public ModelAndView addCulturePage(HttpServletRequest request)
	{
		String status="";
		if(request.getParameter("message")!=null)
		{
			status=(String)request.getParameter("message");
		}
		List<CommonBean> country=new ArrayList<CommonBean>();
		List language=new ArrayList();
		country=commonFunc.getCountryList();
		language=commonFunc.getlanguageList();
		ModelAndView model=new ModelAndView("admin/culture/addCulture");
		model.addObject("add",new CultureBean());
		model.addObject("countryList",country);
		model.addObject("message",status);
		model.addObject("languageList", language);
		return model;
	}
	
	@RequestMapping(value="/AddCulture",method=RequestMethod.POST)
	public String addCulture(@ModelAttribute("add")CultureBean culture,ModelMap model,HttpServletRequest request)
	{
		String status=cultureDao.checkCultureExist(culture);
		if(status.equals("Already Exist"))
		{
			model.addAttribute("message", status);
			//model.addAttribute("edit",CultureBean);
			//request.setAttribute("message", status);
			return "redirect:/addCulture";
		}
		else
		{
			cultureDao.add(culture);
			model.addAttribute("search",new CountryBean());
			return "redirect:/cultureList";
		}
		
	}
	
	@RequestMapping(value="/updateCultureStatus",method=RequestMethod.GET)
	public String updateCultureStatus(@ModelAttribute("search")CultureBean bean,HttpServletRequest request)
	{
		cultureDao.updateStatus(request);
		return "redirect:/cultureList";
	}
	
	@RequestMapping(value="/updateCulture",method=RequestMethod.GET)
	public ModelAndView updateCulture(HttpServletRequest request)
	{
		String status="";
		CultureBean CultureBean=new CultureBean();
		if(request.getParameter("message")!=null)
		{
			status=(String)request.getParameter("message");
		}
		if(request.getParameter("cultureId")!=null)
		{
			CultureBean=cultureDao.checkCulture(request);
		}	
		List country=new ArrayList();
		List language=new ArrayList();
		country=commonFunc.getCountryList();
		language=commonFunc.getlanguageList();
		ModelAndView model=new ModelAndView("admin/culture/updateCulture");
		model.addObject("countryList",country);
		model.addObject("languageList",language);
		model.addObject("message", status);
		model.addObject("edit",CultureBean);
		LOG.info("update Culture.");
		return model;		
	}
	
	@RequestMapping(value="/editCulture",method=RequestMethod.POST)
	public String editCulture(@ModelAttribute("edit")CultureBean CultureBean,HttpServletRequest request,ModelMap model)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("cultureId");
		String status=cultureDao.checkCultureExist(CultureBean);
		if(status.equals("Already Exist"))
		{
			model.addAttribute("message", status);
			//model.addAttribute("edit",CultureBean);
			model.addAttribute("cultureId", id);
			return "redirect:/updateCulture";
		}
		else
		{
			cultureDao.editCulture(id,CultureBean);
			return "redirect:/cultureList";
		}
		
	}
	
	
	
	
	
	
}
