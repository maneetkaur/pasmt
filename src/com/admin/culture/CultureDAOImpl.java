	package com.admin.culture;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Repository
public class CultureDAOImpl implements CultureDAO {

	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	 /* public List<cultureBean> getCountryList()
	{
		List countryList=new ArrayList();
		String sql="select country_id,country_name from country";
		countryList=jdbcTemplate.query(sql, new RowMapper(){
			public cultureBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				cultureBean bean=new cultureBean();
				bean.setCountryId(rs.getString("country_id"));
				bean.setCountry(rs.getString("country_name"));
				return bean;
			}
		});
		return countryList;
	}
	
	public List<cultureBean> getlanguageList()
	{
		List languageList=new ArrayList();
		String sql="select lang_id,language from language";
		languageList=jdbcTemplate.query(sql,new RowMapper(){
			public cultureBean mapRow(ResultSet rs,int rownumber) throws SQLException{
				cultureBean bean=new cultureBean();
				bean.setLanguageId(rs.getString("lang_id"));
				bean.setLanguage(rs.getString("language"));
				return bean;
			}
		});
		return languageList;
	} */
	
	
	public List<CultureBean> listCulture(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		List<CultureBean> cultureList=new ArrayList<CultureBean>();
		String sql="";
		try
			{
			sb.append("and ");
			if(session.getAttribute("cultureBean")!=null)
			{
			CultureBean CultureBean=(CultureBean)session.getAttribute("cultureBean");	
				if(CultureBean.getCountry()!=null)
	 		   	{
		 			if(!CultureBean.getCountry().equals(""))
		 			{
		 				sb.append("country_name like '%"+CultureBean.getCountry()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(CultureBean.getLanguage()!=null)
	 		   {
		 			if(!CultureBean.getLanguage().equals(""))
		 			{
		 				sb.append(" language like '%"+CultureBean.getLanguage()+"%'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
			
			qryStartPart="select cul.culture_id,cul.country_id,con.country_name,cul.lang_id,lang.language,cul.osbt_status,lang.lang_code,con.country_code from lu_culture cul,lu_country con,lu_language lang where cul.country_id=con.country_id and cul.lang_id=lang.lang_id ";
	 			//qryStartPart="select cul.culture_id,cul.country_id,con.country_name,cul.lang_id,lang.language,cul.osbt_status,lang.lang_code,con.country_code from culture cul,country con,language lang where cul.country_id=con.country_id and cul.lang_id=lang.lang_id ";
			qrylast="order by cul.culture_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart; 
				cultureList=jdbcTemplate.query(sql,new CultureRowMapper());
				//System.out.println("culture list-->"+cultureList);
				
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_culture cul,lu_country con,lu_language lang where cul.country_id=con.country_id and cul.lang_id=lang.lang_id  ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				
				request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in listCulture "+e.toString());
			}
		
		session.removeAttribute("cultureBean");
		return cultureList;
	}
	
	
	public void add(CultureBean culture)
	{	
		String sql="";
		if(culture.getCultureId()!=null)
		{
			sql="update lu_culture set country_id = '"+culture.getCountryId()+"',lang_id ='"+culture.getLanguageId()+"' where culture_id ="+culture.getCultureId();
			//jdbcTemplate.update(query);
		}
		else
		{
			sql="insert into lu_culture(country_id,lang_id) values(?,?)";
		}
		try{
			jdbcTemplate.update(sql, new Object[]{
					culture.getCountryId(),culture.getLanguageId()});
			
		}
		catch(Exception e)
		{
			System.out.println("exception in add() in culture "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	 
	}
	public void updateStatus(HttpServletRequest request)
	{
		List<CultureBean> list=new ArrayList<CultureBean>();
		String sql="select osbt_status from lu_culture where culture_id='"+request.getParameter("cultureId")+"'";
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public CultureBean mapRow(ResultSet rs,int rownumber) throws SQLException {
				CultureBean bean=new CultureBean();
				bean.setOsbtStatus(rs.getString(1));
				return bean;
			}
		});
		if(Integer.parseInt(list.get(0).getOsbtStatus().toString())==1)
		{
			jdbcTemplate.update("update lu_culture set osbt_status='0' where culture_id='"+request.getParameter("cultureId")+"'");
		}
		else
		{
			jdbcTemplate.update("update lu_culture set osbt_status='1' where culture_id='"+request.getParameter("cultureId")+"'");
		}
	}
	public CultureBean checkCulture(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		CultureBean bean=new CultureBean();
		List<CultureBean> list=new ArrayList<>();
		String id=request.getParameter("cultureId");
		String sql="select country_id,lang_id from lu_culture where culture_id='"+id+"'";
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public CultureBean mapRow(ResultSet rs,int rownumber) throws SQLException {
				CultureBean bean=new CultureBean();
				bean.setCountryId(rs.getString(1));
				bean.setLanguageId(rs.getString(2));
				return bean;
			}
		});
		bean=list.get(0);
		session.setAttribute("cultureId",id);
		return bean;
	}
	
	public String checkCultureExist(CultureBean CultureBean)
	{
		String checkStatus="";
		List<CultureBean> list=new ArrayList<CultureBean>();
		String sql="select culture_id from lu_culture where country_id='"+CultureBean.getCountryId()+"' and lang_id='"+CultureBean.getLanguageId()+"'";
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public CultureBean mapRow(ResultSet rs,int rownumber) throws SQLException {
				CultureBean bean=new CultureBean();
				bean.setCountryId(rs.getString(1));
				return bean;
			}
		});
		if(list!=null && !list.isEmpty())
		{
			checkStatus="Already Exist";
		}
		else
		{
			checkStatus="Not Have";
		}
		return checkStatus;
	}
	
	public void editCulture(String cultureId,CultureBean bean)
	{
		String sql="update lu_culture set country_id='"+bean.getCountryId()+"',lang_id='"+bean.getLanguageId()+"' where culture_id='"+cultureId+"'";
		jdbcTemplate.update(sql);
	}
}
