package com.admin.culture;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface CultureDAO {

	public void add(CultureBean culture);
	public List<CultureBean> listCulture(HttpServletRequest request,int startIndex,int range);
	public void updateStatus(HttpServletRequest request);
	public CultureBean checkCulture(HttpServletRequest request);
	public void editCulture(String id,CultureBean bean);
	public String checkCultureExist(CultureBean bean);

}
