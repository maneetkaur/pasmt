package com.admin.globalQuestion;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.admin.questionType.QuestionTypeBean;
import com.commonFunc.CommonBean;

public interface GlobalQuestionDAO {

	public String adQuestionInfo(GlobalQuestionBean questionBean,HttpServletRequest request);
	public String updateQuestionStatus(HttpServletRequest request);
	public List getQuestionList(HttpServletRequest request,int startIndex,int range);
	public String getQuestionTypeDetail(String questionTypeId);
	public GlobalQuestionBean getQuestionDetail(HttpServletRequest request);
	public void editQuestionDetail(String questionId,GlobalQuestionBean questionBean);
	public List getQuestionTypeList(String cultureId,String categoryId,int fromMethod);
	public List<GlobalQuestion> getAnswerList(HttpServletRequest request);
	public List<QuestionTypeBean> AddedQuestionType();
	public List<QuestionTypeBean> ParentQuestionType(HttpServletRequest request);
	public List<CommonBean> getParentGlobalAnswer(HttpServletRequest request);
	public Map<String, String> getParentAnswerMap(String parentGlobalAnswer);
	public List<GlobalQuestionBean>  getglobalQuestion(String questionId);
	//public List<GlobalQuestionBean> getCultureQuestion(String questionId);

}
