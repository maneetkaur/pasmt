package com.admin.globalQuestion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.admin.category.Category;
import com.admin.category.CategoryBean;
import com.admin.questionType.QuestionTypeBean;

@Repository
public class GlobalQuestionServ {

	public String getResult(List<QuestionTypeBean> list)
	{
		String target="";
		Integer question_type,question_nature;
		if(list!=null && ! list.isEmpty())
		{
			question_type=(Integer)Integer.parseInt(list.get(0).getQuestionType());
			question_nature=(Integer)Integer.parseInt(list.get(0).getQuestionNature());
			int grid=0, child=0, inputBox=0,multipleFields=0,parent_child=0;
			if(question_type==6 || question_type==7 || question_type==8)
			{
				grid=1;
			}
			if(question_type==5 || question_type==8)
			{
				inputBox=1;
			}
			if(question_nature==1)
			{
				child=1;
			}
			if(question_nature==2)
			{
				parent_child=1;
			}
			if(question_type==1 || question_type==2 || question_type==3 || question_type==4)
			{
				multipleFields=1;
			}
			if(grid==1&&child==1){
				target="both";
			}
			else if(multipleFields==1 && child==1)
			{
				target="multipleChild";
			}
			else if(grid==1){
				target="grid";
			}
			else if(child==1){
				target="child";
			}
			else if(multipleFields==1)
			{
				target="multipleFields";
			}
			else
			{
				target="NotHave";
			
			}
			if(inputBox==1)
			{
				target=target+","+"inputBox";
			}
			if(parent_child==1)
			{
				target="ParentChild";
			}
		}
		return target;
	}
	public String getTargetValue(List list)
	{
		String target="";
		if(list!=null && ! list.isEmpty())
		{
			target="Exists";
		}	
		else
		{
			target="NotHave";
			
		}
		return target;
	}

	/***** To Remove Empty Rows *****/
	public List<GlobalQuestion> removeRow(GlobalQuestionBean questionBean)
	{
		List<GlobalQuestion> clist=new ArrayList<>();
		if(questionBean.getAnswerList().size()==1)
		{
			GlobalQuestion tempQues=questionBean.getAnswerList().get(0);
			if(tempQues.getAnswer().equals(""))
			{
				questionBean.setAnswerList(null);
			}
		}
		if(questionBean.getAnswerList()!=null)
		{
			for(GlobalQuestion ques:questionBean.getAnswerList())
			{
				if(ques.getAnswer()!=null )
				{
					clist.add(ques);
				}
			}
		}
		return clist;
	}
	/***** remove empty rows for subquestion *****/
	public List<GlobalSubQuestionBean> removeSubquestionRow(GlobalQuestionBean questionBean)
	{
		List<GlobalSubQuestionBean> clist=new ArrayList<>();
		if(questionBean.getSubQuestionList().size()==1)
		{
			GlobalSubQuestionBean tempList=questionBean.getSubQuestionList().get(0);
			if(tempList.getSubQuestion().equals(""))
			{
				questionBean.setSubQuestionList(null);
			}
		}
		if(questionBean.getSubQuestionList()!=null)
		{
			for(GlobalSubQuestionBean ques:questionBean.getSubQuestionList())
			{
				if(ques.getSubQuestion()!=null )
				{
					clist.add(ques);
				}
			}
		}
		
		return clist;
	}
	
/****** remove empty rows for child question horizontal rows **********/
public List<GlobalChildQuestionBean> removeChildquestionRow(GlobalQuestionBean questionBean)
	{
		List<GlobalChildQuestionBean> clist=new ArrayList<>();
		if(questionBean.getChildQuestionList().size()==1)
		{
			GlobalChildQuestionBean tempList=questionBean.getChildQuestionList().get(0);
			if(tempList.getChildQuestion().equals(""))
			{
				questionBean.setChildQuestionList(null);
			}
		}
		if(questionBean.getChildQuestionList()!=null)
		{
			for(GlobalChildQuestionBean ques:questionBean.getChildQuestionList())
			{
				if(ques.getChildQuestion()!=null )
				{
					clist.add(ques);
				}
			}
		}
		
		return clist;
	}
}

