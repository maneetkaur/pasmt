	package com.admin.globalQuestion;

import java.io.File;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

	

public class GlobalQuestionBean {
	
	private String questionId,typeId;
	private String question,categoryId,subCategoryId;
	private String categoryType,questionType,subCtaegory;
	private String cultureId,answerTypeId,cultureCode;
	private List<GlobalQuestion> answerList;
	private List<GlobalSubQuestionBean> subQuestionList;
	private String status,sortingId,marRowForChild,childVerticalQuestion;
	private File answerFile;
	private String parent_question_id,parent_question_answer_id;
	private CommonsMultipartFile CSvFile;
	private List<String> extra_ans_option=new ArrayList<>();
	private String sizeOfList,anwserIdList;
	private String listSize,subquestionIdList;
	private List<String> parentAnswersList=new ArrayList<>();
	private List<GlobalChildQuestionBean> childQuestionList=new ArrayList<>(); 
	private String childQuestionIdList,sizeOfChildList;
	
	
	public GlobalQuestionBean()
	{
		answerList=LazyList.decorate(new ArrayList<GlobalQuestion>(), new InstantiateFactory(GlobalQuestion.class));
		subQuestionList=LazyList.decorate(new ArrayList<GlobalSubQuestionBean>(), new InstantiateFactory(GlobalSubQuestionBean.class));
		childQuestionList=LazyList.decorate(new ArrayList<GlobalChildQuestionBean>(), new InstantiateFactory(GlobalChildQuestionBean.class));
	}
	
	public String getChildQuestionIdList() {
		return childQuestionIdList;
	}	

	public void setChildQuestionIdList(String childQuestionIdList) {
		this.childQuestionIdList = childQuestionIdList;
	}
	
	public String getSizeOfChildList() {
		return sizeOfChildList;
	}

	public void setSizeOfChildList(String sizeOfChildList) {
		this.sizeOfChildList = sizeOfChildList;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getSubCtaegory() {
		return subCtaegory;
	}

	public void setSubCtaegory(String subCtaegory) {
		this.subCtaegory = subCtaegory;
	}

	public void setChildVerticalQuestion(String childVerticalQuestion) {
		this.childVerticalQuestion = childVerticalQuestion;
	}

	public List<GlobalChildQuestionBean> getChildQuestionList() {
		return childQuestionList;
	}

	public void setChildQuestionList(List<GlobalChildQuestionBean> childQuestionList) {
		this.childQuestionList = childQuestionList;
	}

	public String getMarRowForChild() {
		return marRowForChild;
	}

	public void setMarRowForChild(String marRowForChild) {
		this.marRowForChild = marRowForChild;
	}

	public String getChildVerticalQuestion() {
		return childVerticalQuestion;
	}

	public void setChildVertivalQuestion(String childVerticalQuestion) {
		this.childVerticalQuestion = childVerticalQuestion;
	}

	public List<String> getParentAnswersList() {
		return parentAnswersList;
	}
	public void setParentAnswersList(List<String> parentAnswersList) {
		this.parentAnswersList = parentAnswersList;
	}
	public CommonsMultipartFile getCSvFile() {
		return CSvFile;
	}
	public void setCSvFile(CommonsMultipartFile cSvFile) {
		CSvFile = cSvFile;
	}
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getParent_question_id() {
		return parent_question_id;
	}
	public void setParent_question_id(String parent_question_id) {
		this.parent_question_id = parent_question_id;
	}
	public List<GlobalQuestion> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(List<GlobalQuestion> answerList) {
		this.answerList = answerList;
	}
	public String getParent_question_answer_id() {
		return parent_question_answer_id;
	}
	public void setParent_question_answer_id(String parent_question_answer_id) {
		this.parent_question_answer_id = parent_question_answer_id;
	}
	public List<GlobalSubQuestionBean> getSubQuestionList() {
		return subQuestionList;
	}
	public void setSubQuestionList(List<GlobalSubQuestionBean> subQuestionList) {
		this.subQuestionList = subQuestionList;
	}
	public File getAnswerFile() {
		return answerFile;
	}
	public void setAnswerFile(File answerFile) {
		this.answerFile = answerFile;
	}
	public String getCultureCode() {
		return cultureCode;
	}
	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}	
	public String getStatus() {
		return status;
	}
	public String getSortingId() {
		return sortingId;
	}
	public void setSortingId(String sortingId) {
		this.sortingId = sortingId;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	public String getAnswerTypeId() {
		return answerTypeId;
	}
	public void setAnswerTypeId(String answerTypeId) {
		this.answerTypeId = answerTypeId;
	}
	public String getCultureId() {
		return cultureId;
	}
	public void setCultureId(String cultureId) {
		this.cultureId = cultureId;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public List<String> getExtra_ans_option() {
		return extra_ans_option;
	}
	public void setExtra_ans_option(List<String> extra_ans_option) {
		this.extra_ans_option = extra_ans_option;
	}
	public String getSizeOfList() {
		return sizeOfList;
	}
	public void setSizeOfList(String sizeOfList) {
		this.sizeOfList = sizeOfList;
	}
	public String getAnwserIdList() {
		return anwserIdList;
	}
	public void setAnwserIdList(String anwserIdList) {
		this.anwserIdList = anwserIdList;
	}
	public String getListSize() {
		return listSize;
	}
	public void setListSize(String listSize) {
		this.listSize = listSize;
	}
	public String getSubquestionIdList() {
		return subquestionIdList;
	}
	public void setSubquestionIdList(String subquestionIdList) {
		this.subquestionIdList = subquestionIdList;
	}
}
