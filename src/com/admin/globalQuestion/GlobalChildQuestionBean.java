package com.admin.globalQuestion;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.commonFunc.CommonBean;

public class GlobalChildQuestionBean {
	
	private String childQuestion, childQuestionId,childQuesOptionIdList;
	private List<CommonBean> childAnswerList=new ArrayList<>();
	private CommonsMultipartFile answerFile;
	
	public GlobalChildQuestionBean()
	{
		childAnswerList=LazyList.decorate(new ArrayList<CommonBean>(), new InstantiateFactory(CommonBean.class));
	}
	public String getChildQuestion() {
		return childQuestion;
	}
	public void setChildQuestion(String childQuestion) {
		this.childQuestion = childQuestion;
	}
	public String getChildQuesOptionIdList() {
		return childQuesOptionIdList;
	}
	public void setChildQuesOptionIdList(String childQuesOptionIdList) {
		this.childQuesOptionIdList = childQuesOptionIdList;
	}
	public String getChildQuestionId() {
		return childQuestionId;
	}
	public void setChildQuestionId(String childQuestionId) {
		this.childQuestionId = childQuestionId;
	}
	public List<CommonBean> getChildAnswerList() {
		return childAnswerList;
	}
	public void setChildAnswerList(List<CommonBean> childAnswerList) {
		this.childAnswerList = childAnswerList;
	}
	public CommonsMultipartFile getAnswerFile() {
		return answerFile;
	}
	public void setAnswerFile(CommonsMultipartFile answerFile) {
		this.answerFile = answerFile;
	}
	
}
