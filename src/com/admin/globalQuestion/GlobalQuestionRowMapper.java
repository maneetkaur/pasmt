package com.admin.globalQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.admin.culture.CultureBean;

public class GlobalQuestionRowMapper implements RowMapper<GlobalQuestionBean>{
	@Override
	public GlobalQuestionBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		GlobalQuestionBean bean=new GlobalQuestionBean();
		bean.setQuestionId(rs.getString("qu.question_id"));
		bean.setCategoryType(rs.getString("cate.category_type"));
		bean.setQuestionType(rs.getString("qty.question_type_header"));
		bean.setStatus(rs.getString("qu.status"));
		bean.setSubCtaegory(rs.getString("ct.sub_category_type"));
		return bean;
	}

}
