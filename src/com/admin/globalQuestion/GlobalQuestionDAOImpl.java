package com.admin.globalQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.fileupload.FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.admin.questionType.QuestionTypeBean;
import com.admin.questionType.QuestionTypeRowMapper;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Repository
@Transactional
public class GlobalQuestionDAOImpl implements GlobalQuestionDAO {
	
	private JdbcTemplate jdbcTemplate;
	@Autowired
	ServletContext servletContext;
	@Autowired
	GlobalQuestionServ questionServ;
	
	@Autowired
	@Qualifier("dataSource")
	
	public void setJdbcTemplate(DataSource dataSource)
	{
		System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	public String adQuestionInfo(GlobalQuestionBean questionBean,HttpServletRequest request)
	{
		String msg="error",filename="",filepath="";
		String sql="",sql1="",sql2="",query="",sql3="",sql4="",query1="",query2="",query3="";
		List<GlobalQuestion> answerList=new ArrayList<>();
		List<GlobalQuestionBean> list=new ArrayList<>();
		List<GlobalQuestionBean> parentId=new ArrayList<>();
		List<GlobalSubQuestionBean> subQuestions=new ArrayList<>();
		List<GlobalChildQuestionBean> childQuestions=new ArrayList<>();
		CommonsMultipartFile csv=questionBean.getCSvFile();
		CommonsMultipartFile childAnswerFile;
		String Extra_option_id="";
		StringBuffer sb=new StringBuffer("insert into xref_global_answer(question_id,answer,created_on) values");
		StringBuffer sb2=new StringBuffer("insert into xref_global_answer(question_id,grid_subquestion_id,answer,created_on) values");
		StringBuffer sb1=new StringBuffer("insert into lu_grid_subquestions(question_id,subquestion,created_on) values");
		if(questionBean.getQuestionId()!=null)
		{
			sql="update lu_question set question_type_id = '"+questionBean.getTypeId()+"',question='"+questionBean.getQuestion()+"' where question_id ="+questionBean.getQuestionId();
			jdbcTemplate.update(query);
		}
		else
		{
			 sql="insert into lu_question(question_type_id,question,sub_question,max_rows,created_on) values(?,?,?,?,?)";
			 System.out.println("qyestion_type_id--->"+questionBean.getTypeId());
			 //answerList=questionBean.getList();
			 query="select question_id from lu_question where question_type_id='"+questionBean.getTypeId()+"' and culture_id=0 and global_question_id=0";
			 sql3="insert into xref_parent_child_question(parent_question_id,parent_question_answer_id,child_question_id,created_on) values(?,?,?,?)";	
			
		}
		try
		{ 
			System.out.println("abcd-->"+questionBean.getTypeId());			
			if(!questionBean.getMarRowForChild().equals("") && !questionBean.getChildVerticalQuestion().equals("") && questionBean.getMarRowForChild()!=null)
			{
				jdbcTemplate.update(sql, new Object[]{
						questionBean.getTypeId(),questionBean.getQuestion(),questionBean.getChildVerticalQuestion(),questionBean.getMarRowForChild(),new Date()});
			}
			else
			{
				jdbcTemplate.update(sql, new Object[]{
						questionBean.getTypeId(),questionBean.getQuestion(),questionBean.getChildVerticalQuestion(),null,new Date()});
			}
			
			list=jdbcTemplate.query(query, new RowMapper()
			{
				public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					GlobalQuestionBean bean=new GlobalQuestionBean();
					bean.setQuestionId(rs.getString(1));
					return bean;
				}
			});
			
			
			/***** insert parent question if question is child ********/
			System.out.println("value of parent question id-->"+questionBean.getParent_question_id());
			System.out.println("parent answerlist"+ questionBean.getParentAnswersList());
			if(questionBean.getParent_question_id()!=null && !questionBean.getParent_question_id().equals("NONE,NONE") && questionBean.getParentAnswersList()!=null && !questionBean.getParentAnswersList().equals("NONE"))
			{
				System.out.println("in parent question insertion");
				/****** find parent question id *********/
				String parentQuesId[]=questionBean.getParent_question_id().split(",");
				System.out.println("parentQuesId[0]---->"+parentQuesId[0]);
				System.out.println("parentQuesId[1]---->"+parentQuesId[1]);
				if(!parentQuesId[0].equals("NONE"))
				{
					query1="select question_id from lu_question where question_type_id='"+parentQuesId[0]+"' and culture_id=0 and global_question_id=0";
				}
				else
				{
					query1="select question_id from lu_question where question_type_id='"+parentQuesId[1]+"' and culture_id=0 and global_question_id=0";
				}
				System.out.println("after parent question insertion");
				parentId=jdbcTemplate.query(query1, new RowMapper()
				{
					public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
						GlobalQuestionBean bean=new GlobalQuestionBean();
						bean.setQuestionId(rs.getString(1));
						return bean;
					}
				});
				/***** parent id answr block end *******/
				System.out.println("parent answer list-->"+questionBean.getParentAnswersList());
				if(questionBean.getParentAnswersList()!=null && questionBean.getParentAnswersList().size()>0
						&& questionBean.getParentAnswersList().get(0)!=null)
				{
					System.out.println("in parent");
					String answerIds=questionBean.getParentAnswersList().get(0);
					if(answerIds!=null)
					{
						for(int i=1;i<questionBean.getParentAnswersList().size();i++)
						{
							answerIds=answerIds+","+questionBean.getParentAnswersList().get(i);
						}
						//System.out.println("answer id-->"+answerIds);
						//System.out.println("in parent question--"+sql3);
						jdbcTemplate.update(sql3, new Object[]{parentId.get(0).getQuestionId(),answerIds,list.get(0).getQuestionId(),new Date()});
					}
				}
				else
				{
					jdbcTemplate.update(sql3, new Object[]{parentId.get(0).getQuestionId(),null,list.get(0).getQuestionId(),new Date()});
				}
				
				
			}
			
		   /******* insert anwsers in question_anwser table ********/
		    System.out.println("insert answer");
			if(questionBean.getAnswerList()!=null && questionBean.getAnswerList().size()>0)
			{
				System.out.println("in if 1--"+questionBean.getAnswerList().size());
				/*for(GlobalQuestion ques:questionBean.getAnswerList())
				{
					if(ques.getAnswer()!=null )
					{
						answerList.add(ques);
					}
				}*/
				answerList=questionServ.removeRow(questionBean);
				if(answerList.size()>0 && answerList!=null)
				{
						//System.out.println("size-->"+answerList.size());
						if(answerList.size()>0 && answerList!=null)
						{
							for(int i=0;i<answerList.size();i++)	
					    	{
					    		System.out.println("i");
								sb.append("('"+list.get(0).getQuestionId()+"','"+answerList.get(i).getAnswer()+"',now()), ");		
					    	}  
					    	sql1=sb.toString();
							  if(sql1.charAt(sql1.length()-2)==','){
								   sql1 = sql1.substring(0, sql1.length()-2);
								  }
							  jdbcTemplate.update(sql1);
						}
				}
			else if(csv!=null)
			{
				System.out.println("1"); 
					
				//String line="",splitBy=",";
				FileItem items= csv.getFileItem();
				//System.out.println("fileitem-->"+items);
				String content=items.getString();
				System.out.println("content-->"+content);
				String[] row=content.split("\n");
				//System.out.println("array length--->"+row.length);
				for(int i=0;i<row.length;i++)
				{
						
					/*****code for spliting on , and inserting in database**********/
					String[] col=row[i].split(",");
					for(int j=0;j<col.length;j++)
					{
						String answer=CommonFunction.filterWithBackSlash(col[j]);
						//System.out.println("answer value after filter--->"+answer);
						sb.append("('"+list.get(0).getQuestionId()+"','"+answer+"',now()),");
					}
						
					
				}
				sql1=sb.toString();
				 if(sql1.charAt(sql1.length()-1)==','){
					  sql1 = sql1.substring(0, sql1.length()-1);
					 }
				 //System.out.println("query-------->"+sql1);
				 jdbcTemplate.update(sql1);
			} 
				
			}
			
				if(questionBean.getExtra_ans_option()!=null && questionBean.getExtra_ans_option().size()>0)
				{
					for(String str:questionBean.getExtra_ans_option())
					{
						if(Extra_option_id!=null && Extra_option_id!="")
						{
							Extra_option_id=Extra_option_id+","+str;
						}
						else
						{
							Extra_option_id=str;
						}		
					}
				}
				//System.out.println("extra_option_ids--->"+Extra_option_id);
				if(!questionBean.getSortingId().equals("NONE"))
				{
					sql4="update lu_question_type set sorting_type='"+questionBean.getSortingId()+"' where question_type_id='"+questionBean.getTypeId()+"'";
					jdbcTemplate.update(sql4);
				}
				if(!Extra_option_id.equals(""))
				{
					sql4="update lu_question_type set extra_ans_option='"+Extra_option_id+"' where question_type_id='"+questionBean.getTypeId()+"'";
					jdbcTemplate.update(sql4);
				}
		    
		    /****** insert subquestions if question type is grid ********/
			//System.out.println("in subquestion");
			  
		    if( questionBean.getSubQuestionList()!=null && questionBean.getSubQuestionList().size()>0 )
		    {
		    	System.out.println("size of subquestionList-->"+questionBean.getSubQuestionList().size());
		    	
		    	subQuestions=questionServ.removeSubquestionRow(questionBean);
		    	System.out.println("size of subQuestion--->"+subQuestions.size());
		    	if(subQuestions.size()>0 && subQuestions!=null)
		    	{
			    	for(int i=0;i<subQuestions.size();i++)
					{
						sb1.append("('"+list.get(0).getQuestionId()+"','"+subQuestions.get(i).getSubQuestion()+"',now()),");
					}
			    	sql2=sb1.toString();
					  if(sql2.charAt(sql2.length()-1)==','){
						   sql2 = sql2.substring(0, sql2.length()-1);
						  }
					  //System.out.println("subquestion query-->"+sql2);
					   jdbcTemplate.update(sql2);
		    	}
		    }
		    /**** insert child horizental question ***********/
		    if( questionBean.getChildQuestionList()!=null && questionBean.getChildQuestionList().size()>0 )
		    {
		    	System.out.println("size of getChildQuestionList-->"+questionBean.getChildQuestionList().size());
		    	
		    	childQuestions=questionServ.removeChildquestionRow(questionBean);
		    	System.out.println("size of childQuestions--->"+childQuestions.size());
		    	if(childQuestions.size()>0 && childQuestions!=null)
		    	{
			    	for(int i=0;i<childQuestions.size();i++)
					{
						sql2="insert into lu_grid_subquestions(question_id,subquestion,created_on) values(?,?,?)";
			    		//sb1.append("('"+list.get(0).getQuestionId()+"','"+childQuestions.get(i).getChildQuestion()+"',now()),");
						jdbcTemplate.update(sql2,new Object[]{
								   list.get(0).getQuestionId(),childQuestions.get(i).getChildQuestion(),new Date()});
						
						 /******* insert answers for this into gloabl answer regarding subquestionid ********
						    * 
						    */
						query2="select grid_subquestion_id from lu_grid_subquestions order by grid_subquestion_id DESC";
						List<String> childQuestionId=jdbcTemplate.query(query2, new RowMapper() {
							public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
								return rs.getString("grid_subquestion_id");
							}
						});
						FileItem items= childQuestions.get(i).getAnswerFile().getFileItem();
						String content=items.getString();
						String[] row=content.split("\n");
						for(int j=0;j<row.length;j++)
						{
							/*****code for spliting on , and inserting in database**********/
							System.out.println("rows-->"+row.length);
							String[] col=row[j].split(",");
							System.out.println("col-->"+col.length);
							for(int k=0;k<col.length;k++)
							{
								String childAnswer=CommonFunction.filterWithBackSlash(col[k]);
								System.out.println("answer value after filter--->"+childAnswer);
								sb2.append("('"+list.get(0).getQuestionId()+"','"+childQuestionId.get(0)+"','"+childAnswer+"',now()),");
								childAnswer=null;
							}
						}
						
					}
			    	query3=sb2.toString();
					  if(query3.charAt(query3.length()-1)==','){
						  query3 = query3.substring(0, query3.length()-1);
						  }
					jdbcTemplate.update(query3);
			    }
			}
		    msg="success";
		}
		catch(Exception e)
		{
			System.out.println("exception in adQuestionInfo() in question --> "+e.toString());
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return list.get(0).getQuestionId(); 
	}
	
	public String updateQuestionStatus(HttpServletRequest request)
	{

		List<GlobalQuestionBean> list=new ArrayList<GlobalQuestionBean>();
		//System.out.println("in ChangeStatus");
		String sql="select status from lu_question where question_id='"+request.getParameter("questionId")+"'";
		//System.out.println("query "+sql);
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				GlobalQuestionBean bean=new GlobalQuestionBean();
				bean.setStatus(rs.getString(1));
				return bean;
				
			}
		});
		if(Integer.parseInt(list.get(0).getStatus().toString())==1)
		{
			jdbcTemplate.update("update lu_question set status='0' where question_id='"+request.getParameter("questionId")+"'");
		}
		else
		{
			jdbcTemplate.update("update lu_question set status='1' where question_id='"+request.getParameter("questionId")+"'");
		}
		return "Success";
	}
	
	public List getQuestionList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qryPart="",qryStartPart="",sql1="",qryLastPart="";
		StringBuffer sb=new StringBuffer();
		List<GlobalQuestionBean> questionList=new ArrayList<GlobalQuestionBean>();
		//List<GlobalQuestionBean> cultureList=new ArrayList<>();
		String sql="";
		try
			{
			sb.append(" and ");
			if(session.getAttribute("questionBean")!=null)
			{
				GlobalQuestionBean questionBean=(GlobalQuestionBean)session.getAttribute("questionBean");	
				if(questionBean.getSubCategoryId()!=null)
	 		   	{
		 			if(!questionBean.getSubCategoryId().equals("NONE"))
		 			{
		 				sb.append(" cate.category_type_id = '"+questionBean.getSubCategoryId()+"'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(questionBean.getTypeId()!=null)
	 		   {
		 			if(!questionBean.getTypeId().equals("NONE"))
		 			{
		 				sb.append(" qu.question_type_id= '"+questionBean.getTypeId()+"'").append(" and ");
		 				
		 			}
	 		   } 
	 		  if(questionBean.getCategoryId()!=null)
	 		   {
		 			if(!questionBean.getCategoryId().equals("NONE"))
		 			{
		 				sb.append(" qty.sub_category_type_id = '"+questionBean.getCategoryId()+"'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select qu.question_id,cate.category_type,ct.sub_category_type,qty.question_type_header,qu.status from lu_question qu,lu_sub_category_type ct,lu_category_type cate,lu_question_type qty where qu.question_type_id=qty.question_type_id and qty.sub_category_type_id=ct.sub_category_type_id and ct.category_type_id=cate.category_type_id and qu.culture_id=0 and global_question_id=0 ";
	 			qryLastPart="order by qu.question_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart; 
	 			System.out.println(sql);
				questionList=jdbcTemplate.query(sql,new GlobalQuestionRowMapper());
				
				/**** to get culture code ******/
				/*sql1="select culture_id,lang_code,country_code from culture cul,language l,lu_country c ,lu_question where cul.country_id=c.country_id nad cul.lang_id=l.lang_id";
				cultureList=jdbcTemplate.query(sql, new RowMapper(){
					public QuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
						QuestionBean bean=new QuestionBean();
						bean.setCultureId(rs.getString(1));
						String languageCode=rs.getString(2);
						String countryCode=rs.getString(3);
						String cultureCode=languageCode+"-"+countryCode;
						bean.setCultureCode(cultureCode);
						return bean;
					}
				}); */

				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_question qu,lu_sub_category_type ct,lu_question_type qty where qu.question_type_id=qty.question_type_id and qty.sub_category_type_id=ct.sub_category_type_id and qu.culture_id=0 and global_question_id=0 ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
			}
			catch(Exception e)
			{
				System.out.println("exception in getQuestionList--> "+e.toString());
			}
		finally
		{
			qryPart=null;qryStartPart=null;sql1=null;qryLastPart=null;
			sb=null;
			sql=null;
		}
		
		session.removeAttribute("questionBean");
		return questionList;
	}
	
	public String getQuestionTypeDetail(String questionTypeId)
	{
		String target="",nature="";
		Integer question_type,question_nature;;
		try
		{
			//System.out.println("nvbhjbhj"+request.getParameter("Country"));
			//System.out.println("ID------->"+request.getParameter("questionTypeId"));
			String sql="select question_type_id,sub_category_type_id,question_type_header,question_type,question_nature from lu_question_type where question_type_id='"+questionTypeId+"'";
			//System.out.println("lang sql="+sql);
			List<QuestionTypeBean> list=jdbcTemplate.query(sql, new QuestionTypeRowMapper());
			target=questionServ.getResult(list);
		}
		catch(Exception e)
		{
			System.out.println("Exception in getQuestionTypeDetail()--> "+e.toString());	
		}		
		return target;
	}

	
	public GlobalQuestionBean getQuestionDetail(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		GlobalQuestionBean bean=new GlobalQuestionBean();
		List<GlobalQuestionBean> list=new ArrayList<>();
		List<GlobalQuestion> getAnswerList=new ArrayList<>();
		List<GlobalSubQuestionBean> getSubquestionList=new ArrayList<>();
		List<GlobalQuestionBean> parentQuestionAnswerList=new ArrayList<>();
		Map<String, String> parentAnswerMap = new HashMap<String, String>();
		List<CommonBean> parentGlobalAnswer=new ArrayList<>();
		List<CommonBean> childHorizontalAnswer=new ArrayList<>();
		List<GlobalChildQuestionBean> childQuestionList=new ArrayList<>();
		Integer questionnature;
		String sql="",sql1="",sql2="",sql3="",sql4="",sql5="";
		String id=request.getParameter("questionId");
		session.setAttribute("questionId", id);
		sql="select que.question_id,que.question_type_id,qt.question_type_header,que.question,qt.sorting_type,qt.extra_ans_option,que.sub_question,que.max_rows from lu_question que,lu_question_type qt where que.question_type_id=qt.question_type_id and question_id='"+id+"'";
		sql1="select global_answer_id,answer from xref_global_answer where question_id='"+id+"'";
		sql2="select grid_subquestion_id,subquestion from lu_grid_subquestions where question_id='"+id+"'";
		sql3="select qt.question_type_id,xref.parent_question_answer_id from xref_parent_child_question xref,lu_question qt where qt.question_id=xref.parent_question_id and child_question_id='"+id+"'";
		list=jdbcTemplate.query(sql, new RowMapper() {
			public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				GlobalQuestionBean bean=new GlobalQuestionBean();
				List<String> extraOptionList=new ArrayList<>(); 	 	
				bean.setQuestionId(rs.getString(1));
				bean.setTypeId(rs.getString(2));
				bean.setQuestionType(rs.getString(3));
				bean.setQuestion(rs.getString(4));
				bean.setSortingId(rs.getString(5));
				if(rs.getString(6)!=null && !rs.getString(6).equals(""))
				{
					extraOptionList=getExtraOptionList(rs.getString(6));
				}
				if(extraOptionList!=null && extraOptionList.size()>0)
				{
					bean.setExtra_ans_option(extraOptionList);					
				}
				bean.setChildVertivalQuestion(rs.getString(7));
				bean.setMarRowForChild(rs.getString(8));
				return bean;
			}

		});		
		bean=list.get(0);
		getAnswerList=jdbcTemplate.query(sql1,new GlobalAnswerMapper());
		if(getAnswerList!=null && getAnswerList.size()>0)
		{
			bean.setAnswerList(getAnswerList);
		}
		getSubquestionList=jdbcTemplate.query(sql2, new RowMapper(){
			public GlobalSubQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				GlobalSubQuestionBean bean=new GlobalSubQuestionBean();
				bean.setSubquestionId(rs.getString(1));
				bean.setSubQuestion(rs.getString(2));
				return bean;
			}
		});
		if(getSubquestionList!=null && getSubquestionList.size()>0)
		{
			sql4="select qt.question_nature from lu_question que,lu_question_type qt where que.question_type_id=qt.question_type_id and question_id='"+id+"'";
			questionnature=jdbcTemplate.queryForObject(sql4,Integer.class);
			if(questionnature==2)
			{
				System.out.println("in child question");
				for(int i=0;i<getSubquestionList.size();i++)
				{
					GlobalChildQuestionBean globalChild=new GlobalChildQuestionBean();
					sql5="select global_answer_id,answer from xref_global_answer where grid_subquestion_id='"+getSubquestionList.get(i).getSubquestionId()+"'";
					childHorizontalAnswer=jdbcTemplate.query(sql5,new RowMapper() {
						public CommonBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
							CommonBean bean=new CommonBean();
							bean.setId(rs.getString("global_answer_id"));
							bean.setName(rs.getString("answer"));
							return bean;
						}
					});
					System.out.println("in child question end");
					System.out.println("in child list-->"+getSubquestionList.get(i).getSubquestionId());
					globalChild.setChildQuestionId(getSubquestionList.get(i).getSubquestionId());
					globalChild.setChildQuestion(getSubquestionList.get(i).getSubQuestion());
					globalChild.setChildAnswerList(childHorizontalAnswer);
					System.out.println("cjhild answer-->"+globalChild.getChildAnswerList());
					childQuestionList.add(globalChild);
					System.out.println("after add in childquestionlIst");
				}
				bean.setChildQuestionList(childQuestionList);
			}
			else
			{
				bean.setSubQuestionList(getSubquestionList);
			}
		}
		System.out.println("before search parent question answer");
		parentQuestionAnswerList=jdbcTemplate.query(sql3, new RowMapper(){
			public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				GlobalQuestionBean bean=new GlobalQuestionBean();
				bean.setParent_question_id(rs.getString(1));
				if(rs.getString(2)!=null && !rs.getString(2).equals("") )
				{
					bean.setParent_question_answer_id(rs.getString(2));
				}
				
				return bean;
			}
		});
		System.out.println("after search parent question answer");
		if(parentQuestionAnswerList!=null && parentQuestionAnswerList.size()>0)
		{
			
			List<String> AnswerList=new ArrayList<>();
			String gobalList[];
			for(GlobalQuestionBean ques:parentQuestionAnswerList)
			{
				bean.setParent_question_id(ques.getParent_question_id());
				if(ques.getParent_question_answer_id()!=null && !ques.getParent_question_answer_id().equals(""))
				{
					gobalList=ques.getParent_question_answer_id().split(",");
					AnswerList=Arrays.asList(gobalList);
					bean.setParentAnswersList(AnswerList);
					parentAnswerMap=getParentAnswerMap(parentQuestionAnswerList.get(0).getParent_question_answer_id());
				}
				
				//bean.setParent_question_answer_id(ques.getParent_question_answer_id());
			}
		}
		
		//System.out.println(list);
		return bean;
	}
	
	public void editQuestionDetail(String questionId,GlobalQuestionBean questionBean)
	{
		String sql="",sql1="",sql2="",sql3="",query2="",query3="";
		String Extra_option_id="",parentQuestionIds[];
		StringBuffer sb=new StringBuffer();
		StringBuffer sb1=new StringBuffer();
		List<GlobalQuestion> answerList=new ArrayList<>();
		List<GlobalSubQuestionBean> subquestionList=new ArrayList<>();
		List<GlobalChildQuestionBean> childQuesList=new ArrayList<>();
		List<GlobalQuestionBean> questionTypeId=new ArrayList<>();
		if(questionBean.getMarRowForChild()!=null && !questionBean.getMarRowForChild().equals(""))
		{
			sql="update lu_question set question ='"+questionBean.getQuestion()+"',sub_question='"+questionBean.getChildVerticalQuestion()+"',max_rows='"+questionBean.getMarRowForChild()+"' where question_id='"+questionId+"'";
		}
		else
		{
			sql="update lu_question set question ='"+questionBean.getQuestion()+"' where question_id='"+questionId+"'";
		}
		
		jdbcTemplate.update(sql);
		System.out.println("sl-->"+sql);
		StringBuffer sb2=new StringBuffer("insert into xref_global_answer(question_id,grid_subquestion_id,answer,created_on) values");
		try
		{
			/******* get question type id *****/
			sql1="select question_type_id from lu_question where question_id='"+questionId+"'";
			System.out.println("sql1-->"+sql1);
			questionTypeId=jdbcTemplate.query(sql1, new RowMapper(){
					public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
						GlobalQuestionBean bean=new GlobalQuestionBean();
						bean.setTypeId(rs.getString(1));
						return bean;
					}
				});
			
			if(questionBean.getExtra_ans_option()!=null && questionBean.getExtra_ans_option().size()>0)
			{
				for(String str:questionBean.getExtra_ans_option())
				{
					if(Extra_option_id!=null && Extra_option_id!="")
					{
						Extra_option_id=Extra_option_id+","+str;
					}
					else
					{
						Extra_option_id=str;
					}		
				}
			}
			//System.out.println("type id value="+questionTypeId.get(0).getTypeId());
			if(questionBean.getSortingId()!="NONE" && questionBean.getSortingId()!=null)
			{
				sql2="update lu_question_type set sorting_type='"+questionBean.getSortingId()+"' where question_type_id='"+questionTypeId.get(0).getTypeId()+"'";
				System.out.println("sorting sql-->"+sql2);
				jdbcTemplate.update(sql2);
			}
			//System.out.println("update sorting");
			if(!Extra_option_id.equals(""))
			{
				sql2="update lu_question_type set extra_ans_option='"+Extra_option_id+"' where question_type_id='"+questionTypeId.get(0).getTypeId()+"'";
				jdbcTemplate.update(sql2);
			}
			//System.out.println("update additional answer");
		 /***** for answer *********/	
			answerList=questionServ.removeRow(questionBean);
			if(answerList!=null && answerList.size()>0)
			{
				Integer currentSize=Integer.parseInt(questionBean.getSizeOfList());
				if(questionBean.getAnwserIdList().length()>0)
				{
					
					String[] answerids=questionBean.getAnwserIdList().split(",");
					//System.out.println("answer ids-->"+answerids);
					GlobalQuestion newGlobalAnswer;
					/******** for update already exists rows *******/
					if(currentSize>0 && answerids.length>0)
					{
						for(int i=0;i<answerids.length;i++)
						{
							newGlobalAnswer=answerList.get(Integer.parseInt(answerids[i]));
							String query="update xref_global_answer set answer='"+newGlobalAnswer.getAnswer()+"'where global_answer_id='"+newGlobalAnswer.getAnswerId()+"'";
							System.out.println("answer update query-->"+query);
							jdbcTemplate.update(query);
						}
					}
				}
				/****** for insert newly added rows *********/
				if(answerList.size()>currentSize)
				{
					sb=new StringBuffer("insert into xref_global_answer(question_id,answer) values ");
					for(int i=currentSize;i<answerList.size();i++)
					{
						sb.append("("+questionId+",'"+answerList.get(i).getAnswer()+"'),");
					}
					sql2=sb.toString();
					/**** to remove , from query *****/
					if(sql2.charAt(sql2.length()-1)==','){
					   sql2 = sql2.substring(0, sql2.length()-1);
					  }		 
					jdbcTemplate.update(sql2);
				}
			}
			//System.out.println("update answer");
			/****** edit subquestion *********/
			subquestionList=questionServ.removeSubquestionRow(questionBean);
			if(subquestionList!=null && subquestionList.size()>0)
			{
				Integer currentSize=Integer.parseInt(questionBean.getListSize());
				
				if(questionBean.getSubquestionIdList().length()>0)
				{
					String[] questionids=questionBean.getSubquestionIdList().split(",");
					GlobalSubQuestionBean newGlobalSubquestion;
					/******** for update already exists rows *******/
					if(currentSize>0 && questionids.length>0)
					{
						for(int i=0;i<questionids.length;i++)
						{
							newGlobalSubquestion=subquestionList.get(Integer.parseInt(questionids[i]));
							String query="update lu_grid_subquestions set subquestion='"+newGlobalSubquestion.getSubQuestion()+"'where grid_subquestion_id='"+newGlobalSubquestion.getSubquestionId()+"'";
							jdbcTemplate.update(query);
						}
					}
				}
				/****** for insert newly added rows *********/
				if(subquestionList.size()>currentSize)
				{
					sb1=new StringBuffer("insert into lu_grid_subquestions(question_id,subquestion) values ");
					for(int i=currentSize;i<subquestionList.size();i++)
					{
						sb1.append("("+questionId+",'"+subquestionList.get(i).getSubQuestion()+"'),");
					}
					sql3=sb.toString();
					/**** to remove , from query *****/
					if(sql3.charAt(sql3.length()-1)==','){
					   sql3 = sql3.substring(0, sql3.length()-1);
					  }		 
					jdbcTemplate.update(sql3);
				}
			}
			
			//System.out.println("update grid subquestion");
			/******* update parent question answer *********/
			/*if(questionBean.getParent_question_id()!=null && !questionBean.getParent_question_id().equals("NONE")  )
			{
				parentQuestionIds=questionBean.getParent_question_id().split(",");
				if(questionBean.getParentAnswersList()!=null && questionBean.getParentAnswersList().size()>0 && !questionBean.getParentAnswersList().get(0).equals(""))
				{
					String answerIds=questionBean.getParentAnswersList().get(0);
					for(int i=1;i<questionBean.getParentAnswersList().size();i++)
					{
						answerIds=answerIds+","+questionBean.getParentAnswersList().get(i);
					}
					sql3="update xref_parent_child_question set parent_question_id='"+parentQuestionIds[0]+"',parent_question_answer_id='"+answerIds+"' where child_question_id='"+questionId+"'";
				}
				else
				{
					sql3="update xref_parent_child_question set parent_question_id='"+parentQuestionIds[0]+"',parent_question_answer_id='"+null+"' where child_question_id='"+questionId+"'";
				}
				jdbcTemplate.update(sql3);
			}*/
			//System.out.println("update parent question answer");
			
			/*******UPDATE CHILDQUESTION **********
			 * 
			 */
			childQuesList=questionServ.removeChildquestionRow(questionBean);
			if(childQuesList.size()>0 && childQuesList!=null)
			{
				System.out.println("size");
				Integer currentSize1=Integer.parseInt(questionBean.getSizeOfChildList());
				System.out.println("surrent size og child question list-->"+currentSize1);
				//if(questionBean.getChildQuestionIdList().length()>0)
				//{
					String[] childquestionids=questionBean.getChildQuestionIdList().split(",");
					System.out.println("child answer list-->"+childquestionids);
					GlobalChildQuestionBean newGlobalChildQues;	
					/******** for update already exists rows *******/
					if(currentSize1>0)
					{
						//System.out.println("1");
						for(int i=0;i<currentSize1;i++)
						{
							//System.out.println("2");
							newGlobalChildQues=questionBean.getChildQuestionList().get(i);
							String query="update lu_grid_subquestions set subquestion='"+newGlobalChildQues.getChildQuestion()+"'where grid_subquestion_id='"+newGlobalChildQues.getChildQuestionId()+"'";
							jdbcTemplate.update(query);
							/***** update already existing rows ********/
							for(int k=0;k<newGlobalChildQues.getChildAnswerList().size();k++)
							{
								System.out.println("child question for updateion-->"+newGlobalChildQues.getChildAnswerList().get(k).getName());
								String qury="update xref_global_answer set answer='"+newGlobalChildQues.getChildAnswerList().get(k).getName()+"' where grid_subquestion_id='"+newGlobalChildQues.getChildQuestionId()+"' and global_answer_id='"+newGlobalChildQues.getChildAnswerList().get(k).getId()+"'";
								jdbcTemplate.update(qury);
							}
							//System.out.println("3");
							//System.out.println("3---->"+newGlobalChildQues.getAnswerFile());
							if(newGlobalChildQues.getAnswerFile()!=null)
							{
								FileItem items= newGlobalChildQues.getAnswerFile().getFileItem();
								String content=items.getString();
								//System.out.println("content---->"+content);
								if(!content.equals(""))
								{
									String[] row=content.split("\n");
									for(int j=0;j<row.length;j++)
									{
										/*****code for spliting on , and inserting in database**********/
										String[] col=row[j].split(",");
										for(int k=0;k<col.length;k++)
										{
											String childAnswer=CommonFunction.filterWithBackSlash(col[k]);
											//System.out.println("answer value after filter--->"+childAnswer);
											sb2.append("('"+questionId+"','"+newGlobalChildQues.getChildQuestionId()+"','"+childAnswer+"',now()),");
										}
										
									}	
									//System.out.println("4");
									query3=sb2.toString();
									  if(query3.charAt(query3.length()-1)==','){
										  query3 = query3.substring(0, query3.length()-1);
										  }
									jdbcTemplate.update(query3);
								}
								
							}
							
						}
					}
				//}
				/****** for insert newly added rows *********/
				if(childQuesList.size()>currentSize1)
				{ 
					//System.out.println("5");
					for(int i=currentSize1;i<childQuesList.size();i++)
						{
							sql2="insert into lu_grid_subquestions(question_id,subquestion,created_on) values(?,?,?)";
				    		//sb1.append("('"+list.get(0).getQuestionId()+"','"+childQuestions.get(i).getChildQuestion()+"',now()),");
							jdbcTemplate.update(sql2,new Object[]{
									questionId,childQuesList.get(i).getChildQuestion(),new Date()});
							
							 /******* insert answers for this into gloabl answer regarding subquestionid ********
							    * 
							    */
							query2="select grid_subquestion_id from lu_grid_subquestions order by grid_subquestion_id DESC";
							List<String> childQuestionId=jdbcTemplate.query(query2, new RowMapper() {
								public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
									return rs.getString("grid_subquestion_id");
								}
							});
							FileItem items= questionBean.getChildQuestionList().get(i).getAnswerFile().getFileItem();
							String content=items.getString();
							String[] row=content.split("\n");
							for(int j=0;i<row.length;i++)
							{
								/*****code for spliting on , and inserting in database**********/
								String[] col=row[j].split(",");
								for(int k=0;j<col.length;k++)
								{
									String childAnswer=CommonFunction.filterWithBackSlash(col[j]);
									//System.out.println("answer value after filter--->"+answer);
									sb2.append("('"+questionId+"','"+childQuestionId.get(0)+"','"+childAnswer+"',now()),");
								}
								System.out.println("6");
								query3=sb2.toString();
								  if(query3.charAt(query3.length()-1)==','){
									  query3 = query3.substring(0, query3.length()-1);
									  }
								jdbcTemplate.update(query3);
							}	
						}	
					}
					
				}
		}
		
		catch(Exception e)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	public List<QuestionTypeBean> getQuestionTypeList(String cultureId,String CatergoryId,int fromMethod)
	{
		String sql="",sql1="";
		List<String> list=new ArrayList<>();
		List<QuestionTypeBean> QueTypeList=new ArrayList<>();
		try
		{
			
			if(fromMethod==0)
			{
				sql="select group_concat(question_type_id) as txt from lu_question where culture_id =0 and global_question_id=0";
			}
			else
			{
			    sql="select group_concat(qty.question_type_id) as txt from lu_question qu,lu_question_type qty where qu.culture_id='"+cultureId+"' and qty.sub_category_type_id='"+CatergoryId+"' and qu.question_type_id=qty.question_type_id";
				//sql="select group_concat(question_type_id) as txt from lu_question where culture_id ='"+cultureId+"' and ";
			}
			//System.out.println(" sql="+sql);
			 list=jdbcTemplate.query(sql, new RowMapper(){
				public String mapRow(ResultSet rs, int rowNumber) throws SQLException {
					String value="";
					value=rs.getString(1);
					return value;
					
				}
			});
			if(fromMethod==0)
			{
				if(list.get(0)!=null && list.size()>0)
				{
					sql1="select question_type_id,question_type_header from lu_question_type where question_type_id not in("+list.get(0)+")";
				}
				else
				{
					sql1="select question_type_id,question_type_header from lu_question_type";
				}
			}
			else
			{
				if(list.get(0)!=null && list.size()>0)
				{
					sql1="select question_type_id,question_type_header from lu_question_type where sub_category_type_id='"+CatergoryId+"' and question_type_id not in("+list.get(0)+")";
				}
				else
				{
					sql1="select question_type_id,question_type_header from lu_question_type where sub_category_type_id='"+CatergoryId+"'";
				}
			}
			
			System.out.println("list quesry--->"+sql1);
			QueTypeList=jdbcTemplate.query(sql1, new RowMapper(){
				public QuestionTypeBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					QuestionTypeBean bean=new QuestionTypeBean();
					bean.setQuestionTypeId(rs.getString(1));
					bean.setQuestionTypeHeading(rs.getString(2));
					return bean;
					
				}
			});
			/* target=questionServ.getTargetValue(QueTypeList);
			if(target=="Exists")
			{
				
			}*/
		}
		catch(Exception e)
		{
			System.out.println("Exception in getQuestionTypeList()--> "+e.toString());	
		}
		finally
		{
			cultureId=null;
			CatergoryId=null;
			sql=null;
			sql1=null;
		}
	
		return QueTypeList;
	}

	public List<GlobalQuestion> getAnswerList(HttpServletRequest request)
	{
		String target="";
		List<GlobalQuestion> list=new ArrayList<>();
		try
		{
			//System.out.println("nvbhjbhj"+request.getParameter("Country"));
			
			String sql="select an.global_answer_id,an.answer from lu_question_type qty,xref_global_answer an,lu_question qt where qt.question_type_id=qty.question_type_id and qt.question_id=an.question_id and qty.question_type_id='"+request.getParameter("questionTypeId")+"'";
			//runtime.exec("select qty.question_type_id,qty.question_type_header from lu_question_type qty,lu_question qt where qt.question_type_id=qty.question_type_id and qt.culture_id='"+request.getParameter("cultureId")+"'");
			System.out.println(" sql="+sql);
			list=jdbcTemplate.query(sql, new GlobalAnswerMapper());
			//target=questionServ.getTargetValue(list);
			//System.out.println("answer list values-->"+list);
			
		}
		catch(Exception e)
		{
			System.out.println("Exception in getQuestionTypeList()--> "+e.toString());	
		}		
	
		return list;
	}
	
	public List<QuestionTypeBean> AddedQuestionType()
	{
		String target="";
		List<QuestionTypeBean> list=new ArrayList<>();
		try
		{
			//System.out.println("nvbhjbhj"+request.getParameter("Country"));
			
			String sql="select qty.question_type_id,qty.question_type_header from lu_question_type qty,lu_question qt where qt.question_type_id=qty.question_type_id and qt.culture_id=0 and qt.global_question_id=0";
			//runtime.exec("select qty.question_type_id,qty.question_type_header from lu_question_type qty,lu_question qt where qt.question_type_id=qty.question_type_id and qt.culture_id='"+request.getParameter("cultureId")+"'");
			System.out.println(" sql="+sql);
			 list=jdbcTemplate.query(sql, new RowMapper(){
				public QuestionTypeBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					QuestionTypeBean bean=new QuestionTypeBean();
					bean.setQuestionTypeId(rs.getString(1));
					bean.setQuestionTypeHeading(rs.getString(2));
					return bean;
					
				}
			});
			target=questionServ.getTargetValue(list);
			if(target=="Exists")
			{
				
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in AddedQuestionType()--> "+e.toString());	
		}		
	
		return list;
	}
	public List<String> getExtraOptionList(String optionId) 
	{
		List<String> extraOptionIdList=new ArrayList<>();
		String extraOptionId[]=optionId.split(",");
		for(int i=0;i<extraOptionId.length;i++)
		{
			extraOptionIdList.add(extraOptionId[i]);
		}
		return extraOptionIdList;
	}
	public List<QuestionTypeBean> ParentQuestionType(HttpServletRequest request)
	{
		String target="";
		List<QuestionTypeBean> list=new ArrayList<>();
		try
		{
			String sql="select qty.question_type_id,qty.question_type_header from lu_question_type qty,lu_question qt where qt.question_type_id=qty.question_type_id and qt.culture_id=0 and qt.global_question_id=0 and qt.question_id not in('"+request.getParameter("questionId")+"')";
			//runtime.exec("select qty.question_type_id,qty.question_type_header from lu_question_type qty,lu_question qt where qt.question_type_id=qty.question_type_id and qt.culture_id='"+request.getParameter("cultureId")+"'");
			System.out.println("sql="+sql);
			 list=jdbcTemplate.query(sql, new RowMapper(){
				public QuestionTypeBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
					QuestionTypeBean bean=new QuestionTypeBean();
					bean.setQuestionTypeId(rs.getString(1));
					bean.setQuestionTypeHeading(rs.getString(2));
					return bean;
					
				}
			});
			/*target=questionServ.getTargetValue(list);
			if(target=="Exists")
			{
				
			}*/
		}
		catch(Exception e)
		{
			System.out.println("Exception in parentQuestionType()--> "+e.toString());	
		}		
	
		return list;
	}
	public List<CommonBean> getParentGlobalAnswer(HttpServletRequest request)
	{
		String questionId,sql,sql1;
		List<String> parentQuesId=new ArrayList<>();
		List<CommonBean> parentAnswer=new ArrayList<>();
		questionId=request.getParameter("questionId");
		sql="select xref.parent_question_id from xref_parent_child_question xref where child_question_id='"+questionId+"'";
		parentQuesId=jdbcTemplate.query(sql, new RowMapper(){
			public String mapRow(ResultSet rs, int rowNumber) throws SQLException{
				return rs.getString("xref.parent_question_id");
			}
		});
		if(parentQuesId.size()>0 && parentQuesId!=null)
		{
			sql1="select global_answer_id,answer from xref_global_answer where question_id='"+parentQuesId.get(0)+"'";
			parentAnswer=jdbcTemplate.query(sql1, new RowMapper(){
				public CommonBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
					CommonBean bean=new CommonBean();
					bean.setId(rs.getString("global_answer_id"));
					bean.setCode(rs.getString("answer"));
					return bean;
				}
			});
		}
		return parentAnswer;
	}
	/* convert gloablparentanswer list into map */
	public Map<String, String> getParentAnswerMap(String parentGlobalAnswer){
		String vTypeArray[]; 
		vTypeArray=parentGlobalAnswer.split(",");
		Map<String, String> tempMap = new HashMap<String, String>();
		for(int i=0; i<vTypeArray.length; i++){
			tempMap.put(vTypeArray[i],vTypeArray[i]);
		}
		return tempMap;
	}
	public List<GlobalQuestionBean>  getglobalQuestion(String questionId)
	{
		List<GlobalQuestionBean> previewQuestion=new ArrayList<>();
		List<GlobalQuestionBean> gloablQuestion=new ArrayList<>();
		List<GlobalQuestionBean> cultureQuestion=new ArrayList<>();
		GlobalQuestionBean gloablQuestionDetail=new GlobalQuestionBean();
		List<GlobalQuestion> globalanswer= new ArrayList<>();
		List<GlobalSubQuestionBean> globalSubquestion= new ArrayList<>();
		String sql="",sql1="",sql2="",sql3="",sql4="";
		try
		{
			/******** get global question **********/
			sql="select qu.question_id,qu.question,qty.question_type from lu_question qu,lu_question_type qty where qu.question_type_id=qty.question_type_id and qu.question_id='"+questionId+"' ";
			gloablQuestion=jdbcTemplate.query(sql, new RowMapper() {
				public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
					GlobalQuestionBean bean=new GlobalQuestionBean();
					bean.setQuestionId(rs.getString("qu.question_id"));
					bean.setQuestion(rs.getString("qu.question"));
					bean.setQuestionType(rs.getString("qty.question_type"));
					return bean;
				}
			});
			
			sql1="select global_answer_id,answer from xref_global_answer where question_id='"+questionId+"'";
			globalanswer=jdbcTemplate.query(sql1,new GlobalAnswerMapper());
			gloablQuestionDetail.setQuestionId(gloablQuestion.get(0).getQuestionId());
			gloablQuestionDetail.setQuestion(gloablQuestion.get(0).getQuestion());
			gloablQuestionDetail.setQuestionType(gloablQuestion.get(0).getQuestionType());
			if(globalanswer!=null && globalanswer.size()>0)
			{
				gloablQuestionDetail.setAnswerList(globalanswer);
			}
			sql2="select grid_subquestion_id,subquestion from lu_grid_subquestions where question_id='"+questionId+"'";
			globalSubquestion=jdbcTemplate.query(sql2, new RowMapper() {
				public GlobalSubQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
					GlobalSubQuestionBean bean=new GlobalSubQuestionBean();
					bean.setSubquestionId(rs.getString("grid_subquestion_id"));
					bean.setSubQuestion(rs.getString("subquestion"));
					return bean;
				}
			});
			if(globalSubquestion!=null && globalSubquestion.size()>0)
			{
				gloablQuestionDetail.setSubQuestionList(globalSubquestion);
			}
			/******* end global question *******/
			previewQuestion.add(gloablQuestionDetail);
			
			/******* get culture questions start **********/
			sql3="select qu.question_id,qu.question,qty.question_type,qu.culture_id from lu_question qu,lu_question_type qty where qu.question_type_id=qty.question_type_id and qu.global_question_id='"+questionId+"' ";
			System.out.println("sql3 for getting culture question-->"+sql3);
			cultureQuestion=jdbcTemplate.query(sql3, new RowMapper() {
				public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
					GlobalQuestionBean bean=new GlobalQuestionBean();
					bean.setQuestionId(rs.getString("qu.question_id"));
					bean.setQuestion(rs.getString("qu.question"));
					bean.setQuestionType(rs.getString("qty.question_type"));
					String sql="select lang.lang_code,con.country_code from lu_culture cul,lu_country con,lu_language lang where cul.country_id=con.country_id and cul.lang_id=lang.lang_id and cul.culture_id='"+rs.getString("qu.culture_id")+"' ";
					System.out.println("culture sql-->"+sql);
					List<String> cultureCode=jdbcTemplate.query(sql,new RowMapper() {
						public String mapRow(ResultSet rs, int rowNumber) throws SQLException{
							String culCode=rs.getString("lang.lang_code")+"-"+rs.getString("con.country_code");
							return culCode;
						}
					});
					if(cultureCode!=null && cultureCode.size()>0)
					{
						bean.setCultureCode(cultureCode.get(0));
					}
					System.out.println("culture code value-->"+bean.getCultureCode());
					return bean;
				}
			});
			for(int i=0;i<cultureQuestion.size();i++)	
			{
				GlobalQuestionBean gloablQuestionPreview=new GlobalQuestionBean();
				sql4="select question_answer_id,answer from lu_question_answer where question_id='"+cultureQuestion.get(i).getQuestionId()+"'";
				globalanswer=jdbcTemplate.query(sql4,new GlobalAnswerMapper());
				sql2="select grid_subquestion_id,subquestion from lu_grid_subquestions where question_id='"+questionId+"'";
				globalSubquestion=jdbcTemplate.query(sql2, new RowMapper() {
					public GlobalSubQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
						GlobalSubQuestionBean bean=new GlobalSubQuestionBean();
						bean.setSubquestionId(rs.getString("grid_subquestion_id"));
						bean.setSubQuestion(rs.getString("subquestion"));
						return bean;
					}
				});
				gloablQuestionPreview.setQuestionId(cultureQuestion.get(i).getQuestionId());
				gloablQuestionPreview.setQuestion(cultureQuestion.get(i).getQuestion());
				gloablQuestionPreview.setQuestionType(gloablQuestion.get(0).getQuestionType());
				gloablQuestionPreview.setCultureCode(cultureQuestion.get(i).getCultureCode());
				if(globalanswer!=null && globalanswer.size()>0)
				{
					gloablQuestionPreview.setAnswerList(globalanswer);
				}
				if(globalSubquestion!=null && globalSubquestion.size()>0)
				{
					gloablQuestionPreview.setSubQuestionList(globalSubquestion);
				}
				previewQuestion.add(gloablQuestionPreview);
			}
			
		}
		catch(Exception e)
		{
			System.out.println("exception in getglobalQuestion() method-->"+e);
		}
		sql="";
		sql1="";
		sql2="";
		sql3="";
		sql4="";
		return previewQuestion;
	}
	
	/*public List<GlobalQuestionBean> getCultureQuestion(String questionId)
	{
		List<GlobalQuestionBean> gloablQuestion=new ArrayList<>();
		GlobalQuestionBean gloablQuestionDetail=new GlobalQuestionBean();
		List<GlobalQuestion> globalanswer= new ArrayList<>();
		List<GlobalSubQuestionBean> globalSubquestion= new ArrayList<>();
		String sql="",sql1="",sql2="";
		try
		{
			sql="select qu.question_id,qu.question,qty.question_type from lu_question qu,lu_question_type qty where qu.question_type_id=qty.question_type_id and qu.question_id='"+questionId+"' ";
			gloablQuestion=jdbcTemplate.query(sql, new RowMapper() {
				public GlobalQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
					GlobalQuestionBean bean=new GlobalQuestionBean();
					bean.setQuestionId(rs.getString("qu.question_id"));
					bean.setQuestion(rs.getString("qu.question"));
					bean.setQuestionType(rs.getString("qty.question_type"));
					return bean;
				}
			});
			sql1="select global_answer_id,answer from xref_global_answer where question_id='"+questionId+"'";
			globalanswer=jdbcTemplate.query(sql1,new GlobalAnswerMapper());
			gloablQuestionDetail.setQuestionId(gloablQuestion.get(0).getQuestionId());
			gloablQuestionDetail.setQuestion(gloablQuestion.get(0).getQuestion());
			gloablQuestionDetail.setQuestionType(gloablQuestion.get(0).getQuestionType());
			if(globalanswer!=null && globalanswer.size()>0)
			{
				gloablQuestionDetail.setAnswerList(globalanswer);
			}
			sql2="select grid_subquestion_id,subquestion from lu_grid_subquestions where question_id='"+questionId+"'";
			globalSubquestion=jdbcTemplate.query(sql2, new RowMapper() {
				public GlobalSubQuestionBean mapRow(ResultSet rs, int rowNumber) throws SQLException{
					GlobalSubQuestionBean bean=new GlobalSubQuestionBean();
					bean.setSubquestionId(rs.getString("grid_subquestion_id"));
					bean.setSubQuestion(rs.getString("subquestion"));
					return bean;
				}
			});
			if(globalSubquestion!=null && globalSubquestion.size()>0)
			{
				gloablQuestionDetail.setSubQuestionList(globalSubquestion);
			}
		}
		catch(Exception e)
		{
			System.out.println("exception in getglobalQuestion() method-->"+e);
		}
		
		return gloablQuestion;
	}*/
}
