package com.admin.globalQuestion;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class GlobalAnswerMapper implements RowMapper<GlobalQuestion>{
	
	@Override
	public GlobalQuestion mapRow(ResultSet rs, int rowNumber) throws SQLException {
		GlobalQuestion bean=new GlobalQuestion();
		if(rs.getString(1)!=null && rs.getString(2)!=null)
		{
			bean.setAnswerId(rs.getString(1));
			bean.setAnswer(rs.getString(2));
		}
		return bean;
		
	}

}
