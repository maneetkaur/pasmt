package com.admin.globalQuestion;

public class GlobalSubQuestionBean {
	
	private String subQuestion;
	private String subquestionId;
	
	public String getSubQuestion() {
		return subQuestion;
	}
	public void setSubQuestion(String subQuestion) {
		this.subQuestion = subQuestion;
	}
	public String getSubquestionId() {
		return subquestionId;
	}
	public void setSubquestionId(String subquestionId) {
		this.subquestionId = subquestionId;
	}

}
