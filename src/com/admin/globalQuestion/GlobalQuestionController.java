package com.admin.globalQuestion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.admin.country.CountryBean;
import com.admin.questionType.QuestionTypeBean;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;


@Controller
public class GlobalQuestionController {

	@Autowired
	CommonFunction commonFunc;
	
	@Autowired
	GlobalQuestionDAO questionDao;
	
	@RequestMapping(value="/questionList" ,method=RequestMethod.GET)
	public ModelAndView getQuestionList(HttpServletRequest request)
	{
		List<GlobalQuestionBean> list=null;
		List<CommonBean> subCategoryList,categoryList,questionTypeList;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		subCategoryList=commonFunc.getCategoryTypeList();
		categoryList=commonFunc.getSubCategoryTypeList();
		questionTypeList=commonFunc.getQuestionTypeList();
		list=questionDao.getQuestionList(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/question/questionList");
		model.addObject("questionList",list);
		model.addObject("subCategoryList", subCategoryList);
		model.addObject("categoryList", categoryList);
		model.addObject("questionTypeList", questionTypeList);
		model.addObject("search_global_question",new GlobalQuestionBean());
		return model;
	}
	
	@RequestMapping(value="/addQuestion",method=RequestMethod.GET)
	public ModelAndView addQuestionPage()
	{
		List<QuestionTypeBean> QuestionTypeList=new ArrayList<>();
		List<QuestionTypeBean> parentQuestionList=new ArrayList<>();
		//List<CommonBean> cultureList=commonFunc.getCultureList();
		List<CommonBean> categoryTypeList=commonFunc.getSubCategoryTypeList();
		parentQuestionList=questionDao.AddedQuestionType();
		QuestionTypeList=questionDao.getQuestionTypeList("","",0);
		ModelAndView model=new ModelAndView("admin/question/addQuestion");
		//model.addObject("cultureList", cultureList);
		model.addObject("categoryTypeList", categoryTypeList);
		model.addObject("parentQuestionList",parentQuestionList);
		model.addObject("QuestionTypeList", QuestionTypeList);
		model.addObject("add_global_question",new GlobalQuestionBean());
		return model;
	}
	
	@RequestMapping(value="/AddQuestion",method=RequestMethod.POST)
	public String AddQuestion(@ModelAttribute("add_global_question") GlobalQuestionBean questionBean,ModelMap model,HttpServletRequest request)
	{
		//List<GlobalQuestionBean> globalQuestion=new ArrayList<>();
		String p_page = request.getParameter("typeId");
		String globalQuesionId=questionDao.adQuestionInfo(questionBean,request);
		//globalQuestion=questionDao.getglobalQuestion(globalQuesionId);
		model.addAttribute("search_global_question", new GlobalQuestionBean());
		return "redirect:/questionList";
	}
	
	@RequestMapping(value="/updateQuestionStatus",method=RequestMethod.GET)
	public String updateQuestionStatus(@ModelAttribute("search_global_question")GlobalQuestionBean questionBean,HttpServletRequest request)
	{
		String checkStatus=questionDao.updateQuestionStatus(request);
		return "redirect:/questionList";
	}
	
	@RequestMapping(value="/getQuestionTypeInfo",method=RequestMethod.GET)
	public String getQuestionTypeInfo(HttpServletRequest request,ModelMap model)
	{
		String questionTypeId="",chkQuestion;
		questionTypeId=request.getParameter("questionTypeId");
		chkQuestion=questionDao.getQuestionTypeDetail(questionTypeId);
		model.addAttribute("chkQuestion", chkQuestion);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}
	
	@RequestMapping(value="/searchQuestion",method=RequestMethod.POST)
	public String searchQuestion(@ModelAttribute("search_global_question")GlobalQuestionBean questionBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("questionBean", questionBean);
		return "redirect:/questionList";
	}
	
	@RequestMapping(value="/updateQuestion",method=RequestMethod.GET)
	public ModelAndView updateQuestion(HttpServletRequest request)
	{
		List<QuestionTypeBean> QuestionTypeList=new ArrayList<>();
		List<QuestionTypeBean> parentQuestionList=new ArrayList<>();
		List<CommonBean> parentGlobalAnswer=new ArrayList<>();
		//Map<String, String> parentAnswerMap = new HashMap<String, String>();
		GlobalQuestionBean questionBean=questionDao.getQuestionDetail(request);
		parentQuestionList=questionDao.ParentQuestionType(request);
		QuestionTypeList=questionDao.AddedQuestionType();
		parentGlobalAnswer=questionDao.getParentGlobalAnswer(request);
		//parentAnswerMap=questionDao.getParentAnswerMap(parentGlobalAnswer);
		ModelAndView model=new ModelAndView("admin/question/updateQuestion");
		model.addObject("edit_global_question",questionBean);
		//model.addObject("cultureList", cultureList);
		model.addObject("QuestionTypeList", QuestionTypeList);
		model.addObject("parentQuestionList",parentQuestionList);
		model.addObject("parentGlobalAnswer",parentGlobalAnswer);
		//model.addObject("parentAnswerMap",parentAnswerMap);
		return model;
	}
	
	@RequestMapping(value="/editQuestion",method=RequestMethod.POST)
	public String EditQuestion(@ModelAttribute("edit_global_question")GlobalQuestionBean questionBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("questionId");
		questionDao.editQuestionDetail(id,questionBean);
		return "redirect:/questionList";
	}
	
	/*@RequestMapping(value="/getQuestionTypeList",method=RequestMethod.GET)
	public String getQuestionTypeList(HttpServletRequest request,ModelMap model)
	{
		
		List<QuestionTypeBean> QuestionTypeList=new ArrayList<>();
		List<QuestionTypeBean> QuestionList=new ArrayList<>();
		QuestionTypeList=questionDao.AddedQuestionType(request);
		QuestionList=questionDao.getQuestionTypeList(request);
		model.addAttribute("QuestionList",QuestionList);
		model.addAttribute("QuestionTypeList", QuestionTypeList);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}*/
	
	@RequestMapping(value="/findAnswerList",method=RequestMethod.GET)
	public String getAnswerList(HttpServletRequest request,ModelMap model)
	{
		List<GlobalQuestion> answerList=questionDao.getAnswerList(request);
		model.addAttribute("answerList",answerList);
		//System.out.println("list values--"+answerList);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}
	
	@RequestMapping(value="/previewQuestion",method=RequestMethod.GET)
	public String getQuestionPreview(HttpServletRequest request,ModelMap model)
	{
		//List<global> answerList=questionDao.getAnswerList(request);
		HttpSession session=request.getSession();
		Boolean checkPreview=true;
		//GlobalQuestionBean previewQuestions=new GlobalQuestionBean();
		List<GlobalQuestionBean> previewQuestions=new ArrayList<>();
		previewQuestions=questionDao.getglobalQuestion(request.getParameter("questionId"));
		//culturePreviewQuestion=questionDao.getCultureQuestion(request.getParameter("questionId"));
		//model.addAttribute("previewQuestions",previewQuestions);
		System.out.println("list values--"+previewQuestions);
		model.addAttribute("checkPreview", checkPreview);
		session.setAttribute("previewQuestions", previewQuestions);
		System.out.println("value of checkPreview-->"+checkPreview);
		return "redirect:/questionList";
		
	}
}
