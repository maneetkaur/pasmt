package com.admin.company;

import java.util.List;

import javax.servlet.http.HttpServletRequest;


public interface CompanyDAO {
	
	public List listCompany(HttpServletRequest request,int startIndex,int range);
	public String add(CompanyBean companyBean);
	public String validateCompanyName(HttpServletRequest request);
	public CompanyBean checkCompany(HttpServletRequest request);
	public void editCompany(String companyId,CompanyBean companyBean);
	public String validateWebsiteLink(HttpServletRequest request);

}
