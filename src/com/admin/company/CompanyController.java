package com.admin.company;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class CompanyController {
	
	@Autowired
	CompanyDAO companyDao;
	@Autowired
	CommonFunction commFunc;

	@RequestMapping(value="/companyList",method=RequestMethod.GET)
	public ModelAndView companyList(HttpServletRequest request)
	{
		
		List<CompanyBean> list=null;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		list=companyDao.listCompany(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/company/companyList");
		model.addObject("companyList",list);
		model.addObject("search",new CompanyBean());
		//model.addObject("pageListHolder", pagelistHolder);
		//model.addObject("edit",new langBean());
		return model;
	}
	@RequestMapping(value="/addCompany",method=RequestMethod.GET)
	public ModelAndView addCompany()
	{
		List<CommonBean> country=new ArrayList<CommonBean>();
		country=commFunc.getAllCountryList();
		ModelAndView model=new ModelAndView("admin/company/addCompany");
		model.addObject("add",new CompanyBean());
		model.addObject("countryList",country);
		//model.addObject("message",status);
		return model;
	}
	
	@RequestMapping(value="/AddCompany",method=RequestMethod.POST)
	public String AddCompany(@ModelAttribute("add")CompanyBean company,ModelMap model)
	{
		String message=companyDao.add(company);
		model.addAttribute("search", new CompanyBean());
		return "redirect:/companyList";
	}
	
	@RequestMapping(value="/updateCompany",method=RequestMethod.GET)
	public ModelAndView updateCompany(HttpServletRequest request)
	{
		CompanyBean companyBean=new CompanyBean();
		companyBean=companyDao.checkCompany(request);		
		List country=new ArrayList();
		country=commFunc.getAllCountryList();
		ModelAndView model=new ModelAndView("admin/company/updateCompany");
		model.addObject("allCountryList",country);
		model.addObject("edit",companyBean);
		return model;		
	}
	
	@RequestMapping(value="/ValidateCompany",method=RequestMethod.GET)
	public String checkCompanyName(HttpServletRequest request,ModelMap model)
	{
		String chkCompany=companyDao.validateCompanyName(request);
		model.addAttribute("checkCompany", chkCompany);
		//System.out.println("check country"+chkCountry);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}
	
	@RequestMapping(value="/searchCompany",method=RequestMethod.POST)
	public String searchCompany(@ModelAttribute("search")CompanyBean companyBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("companyBean",companyBean);
		return "redirect:/companyList";
	}
	
	@RequestMapping(value="/editCompany",method=RequestMethod.POST)
	public String EditCompany(@ModelAttribute("edit")CompanyBean companyBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("companyId");
		companyDao.editCompany(id,companyBean);
		return "redirect:/companyList";
	}
	
	@RequestMapping(value="/ValidateWebsiteURL",method=RequestMethod.GET)
	public String checkWebsiteURL(HttpServletRequest request,ModelMap model)
	{
		String chkWebsite=companyDao.validateWebsiteLink(request);
		model.addAttribute("checkWebsite", chkWebsite);
		return "ajaxOperation/ajaxOperation";
		
	}
}
