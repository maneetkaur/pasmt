package com.admin.company;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Repository
@Transactional
public class CompanyDAOImpl implements CompanyDAO
{
		private JdbcTemplate jdbcTemplate;
		@Autowired		
		@Qualifier("dataSource")
		public void setJdbcTemplate(DataSource dataSource)
		{
			//System.out.println("set");
			this.jdbcTemplate=new JdbcTemplate(dataSource);
		}
			
		
		public List listCompany(HttpServletRequest request,int startIndex,int range)
		{
			HttpSession session=request.getSession(true);
			String qry="";
			String qrylast="";
			String qryPart="",qryStartPart="",qryEndPart="",query2="";
			StringBuffer sb=new StringBuffer();
			List<CompanyBean> companyList=new ArrayList<CompanyBean>();
			String sql="";
			try
				{
				sb.append(" and ");
				if(session.getAttribute("companyBean")!=null)
				{
					CompanyBean companyBean=(CompanyBean)session.getAttribute("companyBean");	
					if(companyBean.getCompanyName()!=null)
		 		   	{
			 			if(!companyBean.getCompanyName().equals(""))
			 			{
			 				sb.append("company_name like '%"+companyBean.getCompanyName()+"%'").append(" and ");
			 				
			 			}
		 		   }	
		 		   if(companyBean.getCountry()!=null)
		 		   {
			 			if(!companyBean.getCountry().equals(""))
			 			{
			 				sb.append(" country_name like '%"+companyBean.getCountry()+"%'").append(" and ");
			 				
			 			}
		 		   } 
				}
		 			qryPart=(sb.toString()).trim();
		 			int qryPartLength=qryPart.length();
		 			
		 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
		 			{
		 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
		 			}
		 			
		 			if(qryPartLength==5)
		 			{
		 				qryPart="";
		 			}
		 			
		 			qryStartPart="select com.company_id,con.country_id,com.company_name,con.country_name from lu_company com,lu_country con where com.country_id=con.country_id ";
		 			
					sql=qryStartPart+qryPart; 
				
				
					companyList=jdbcTemplate.query(sql,new RowMapper()
					{
						public CompanyBean mapRow(ResultSet rs,int rownumber) throws SQLException {
							CompanyBean bean=new CompanyBean();
							bean.setCompanyId(rs.getString(1));
							bean.setCountryId(rs.getString(2));
							bean.setCompanyName(rs.getString(3));
							bean.setCountry(rs.getString(4));
							return bean;
						}
					});
					
				}
				catch(Exception e)
				{
					System.out.println("exception in listCompany "+e.toString());
					//TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				}
			
			session.removeAttribute("companyBean");
			return companyList;
			
		}
	public String add(CompanyBean companyBean)
	{
		String msg="error";
		String sql="";
		if(companyBean.getCompanyId()!=null)
		{
			sql="update lu_company set country_id = '"+companyBean.getCountryId()+"',contact_person='"+companyBean.getContactPerson()+"',email_id='"+companyBean.getEmailId()+"',phone_no='"+companyBean.getPhoneNo()+"',address='"+companyBean.getAddress()+"' where company_id ="+companyBean.getCompanyId();
			//jdbcTemplate.update(query);
		}
		else
		{
			 sql="insert into lu_company(country_id,company_name,website,contact_person,email_id,phone_no,address) values(?,?,?,?,?,?,?)";
			//System.out.println("query "+sql);	
		}
		try{
			jdbcTemplate.update(sql, new Object[]{
					companyBean.getCountryId(),companyBean.getCompanyName(),companyBean.getWebsite(),companyBean.getContactPerson(),companyBean.getEmailId(),companyBean.getPhoneNo(),companyBean.getAddress()});
			msg="success";
		}
		catch(Exception e)
		{
			System.out.println("exception in add() in company--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return msg; 
	}
	
	public CompanyBean checkCompany(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		CompanyBean bean=new CompanyBean();
		List<CompanyBean> list=new ArrayList<>();
		String id=request.getParameter("companyId");
		String sql="select company_id,country_id,company_name,website,contact_person,email_id,phone_no,address from lu_company where company_id='"+id+"'";
		list=jdbcTemplate.query(sql, new CompanyRowMapper());
		bean=list.get(0);
		session.setAttribute("companyId",id);
		//System.out.println(list);
		return bean;
	}
	
	public String validateCompanyName(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select company_id from lu_company where company_name='"+request.getParameter("Company")+"'";
			List<CompanyBean> list=jdbcTemplate.query(sql, new RowMapper(){
					public CompanyBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						CompanyBean bean=new CompanyBean();
						bean.setCompanyName(rs.getString(1));
						return bean;
					}
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e) 
		{
			System.out.println("Exception in validateCountryName() "+e.toString());	
			//TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}		
		return target;
	}
	
	public void editCompany(String companyId,CompanyBean companyBean)
	{
		String sql="update lu_company set country_id='"+companyBean.getCountryId()+"',company_name='"+companyBean.getCompanyName()+"',website='"+companyBean.getWebsite()+"',contact_person='"+companyBean.getContactPerson()+"',email_id='"+companyBean.getEmailId()+"',phone_no='"+companyBean.getPhoneNo()+"',address='"+companyBean.getAddress()+"' where company_id='"+companyId+"'";
		jdbcTemplate.update(sql);
	}
	public String validateWebsiteLink(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select company_id from lu_company where website like '%"+request.getParameter("Website")+"%'";
			List<CompanyBean> list=jdbcTemplate.query(sql, new RowMapper(){
					public CompanyBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						CompanyBean bean=new CompanyBean();
						bean.setCompanyId(rs.getString(1));
						return bean;
					}
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in validateWebsiteLink() "+e.toString());	
		}		
		return target;
	}
	
	
}
