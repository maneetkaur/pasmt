package com.admin.company;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CompanyRowMapper implements RowMapper<CompanyBean>{
	
	@Override
	public CompanyBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		CompanyBean bean=new CompanyBean();
		bean.setCompanyId(rs.getString(1));
		bean.setCountryId(rs.getString(2));
		bean.setCompanyName(rs.getString(3));
		//bean.setCountry(rs.getString(4));
		bean.setWebsite(rs.getString(4));
		bean.setContactPerson(rs.getString(5));
		bean.setEmailId(rs.getString(6));
		bean.setPhoneNo(rs.getString(7));
		bean.setAddress(rs.getString(8));
		return bean;
	}

}
