package com.admin.subCategory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class SubCategoryTypeBean {
	
	private String subCategoryTypeId,subCategoryType,languageId,status,sizeOfList,subCategoryIdList,categoryId,imagePath,category;
	private List<SubCategoryBean> subCategoryList;
	private CommonsMultipartFile subcategoryImage;
	
	public SubCategoryTypeBean()
	{
		subCategoryList=LazyList.decorate(new ArrayList<SubCategoryBean>(), new InstantiateFactory(SubCategoryBean.class));
	}
	public String getSubCategoryTypeId() {
		return subCategoryTypeId;
	}
	public void setSubCategoryTypeId(String subCategoryTypeId) {
		this.subCategoryTypeId = subCategoryTypeId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getSubCategoryType() {
		return subCategoryType;
	}
	public void setSubCategoryType(String subCategoryType) {
		this.subCategoryType = subCategoryType;
	}
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSizeOfList() {
		return sizeOfList;
	}
	public void setSizeOfList(String sizeOfList) {
		this.sizeOfList = sizeOfList;
	}
	public String getSubCategoryIdList() {
		return subCategoryIdList;
	}
	public void setSubCategoryIdList(String subCategoryIdList) {
		this.subCategoryIdList = subCategoryIdList;
	}
	public List<SubCategoryBean> getSubCategoryList() {
		return subCategoryList;
	}
	public void setSubCategoryList(List<SubCategoryBean> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	public CommonsMultipartFile getSubcategoryImage() {
		return subcategoryImage;
	}
	public void setSubcategoryImage(CommonsMultipartFile subcategoryImage) {
		this.subcategoryImage = subcategoryImage;
	}

}
