package com.admin.subCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SubCategoryRowMapper implements RowMapper<SubCategoryBean>{

	@Override
	public SubCategoryBean mapRow(ResultSet rs,int rowNumber) throws SQLException{
		SubCategoryBean bean=new SubCategoryBean();
		bean.setSubCategoryId(rs.getString("sub_category_id"));
		bean.setLanguageId(rs.getString("language_id"));
		bean.setSubCategory(rs.getString("sub_category_name"));
		return bean;
	}

}
