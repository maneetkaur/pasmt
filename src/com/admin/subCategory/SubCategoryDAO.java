package com.admin.subCategory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface SubCategoryDAO {

	public List getSubCatgeoryList(HttpServletRequest request,int startIndex,int range);
	public void addSubcategory(SubCategoryTypeBean subCategoryBean, HttpServletRequest request,String filePath);
	public String updatesubcatStatus(HttpServletRequest request);
	public SubCategoryTypeBean getSubCategoryDetail(HttpServletRequest request);
	public void editSubCategory(String subCategoryId,SubCategoryTypeBean subCategoryBean, String filePath);
	public String validatesubCategoryName(HttpServletRequest request);
}
