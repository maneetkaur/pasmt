package com.admin.subCategory;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.admin.category.Category;
import com.admin.category.CategoryBean;
import com.admin.category.CategoryRowMapper;

@Repository
@Transactional
public class SubCategoryDAOImpl implements SubCategoryDAO{

	private JdbcTemplate jdbcTemplate;
	@Autowired		
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		//System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}

	public List getSubCatgeoryList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		//String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		//Boolean status=false;
		StringBuffer sb=new StringBuffer();
		List<SubCategoryTypeBean> categoryList=new ArrayList<SubCategoryTypeBean>();
		String sql="";
		try
		{
			sb.append(" and ");
			if(session.getAttribute("subCategoryBean")!=null)
			{
				SubCategoryTypeBean subCategoryBean=(SubCategoryTypeBean)session.getAttribute("subCategoryBean");	
				{
		 			if(!subCategoryBean.getCategory().equals(""))
		 			{
		 				//status=true;
		 				sb.append("cate.category_type like '%"+subCategoryBean.getCategory()+"%'").append(" and ");
		 				
		 			}
	 		   }	
				if(subCategoryBean.getSubCategoryType()!=null)
	 		   	{
		 			if(!subCategoryBean.getSubCategoryType().equals(""))
		 			{
		 				//status=true;
		 				sb.append("ct.sub_category_type like '%"+subCategoryBean.getSubCategoryType()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(subCategoryBean.getLanguageId()!=null)
	 		   {
		 			if(!subCategoryBean.getLanguageId().equals("NONE"))
		 			{
		 				//status=true;
		 				sb.append("l.lang_id ='"+subCategoryBean.getLanguageId()+"'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}	
	 			qryStartPart="select distinct ct.sub_category_type_id,ct.sub_category_type,ct.status,cate.category_type from lu_sub_category_type ct,lu_category_type cate ,lu_language l,lu_sub_category c where ct.sub_category_type_id=c.sub_category_type_id and ct.category_type_id=cate.category_type_id and l.lang_id=c.language_id ";
	 			qrylast="order by ct.sub_category_type_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart+qrylast; 
				System.out.println("list sql-->"+sql);
				categoryList=jdbcTemplate.query(sql,new RowMapper()
				{
					public SubCategoryTypeBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						SubCategoryTypeBean bean=new SubCategoryTypeBean();
						bean.setSubCategoryTypeId(rs.getString("ct.sub_category_type_id"));
						bean.setSubCategoryType(rs.getString("ct.sub_category_type"));
						bean.setStatus(rs.getString("ct.status"));
						bean.setCategory(rs.getString("cate.category_type"));
						return bean;
					}
				});
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(distinct ct.sub_category_type_id) from lu_sub_category_type ct,lu_category_type cate ,lu_language l,lu_sub_category c where ct.sub_category_type_id=c.sub_category_type_id and ct.category_type_id=cate.category_type_id and l.lang_id=c.language_id  ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in getListCategory "+e.toString());
			}
		
		session.removeAttribute("subCategoryBean");
		return categoryList;
	}

	public void addSubcategory(SubCategoryTypeBean subCategoryBean, HttpServletRequest request,String filePath)
	{
		String languageId,category,catgoryNameInNative,fileName="";
		List<SubCategoryBean> subCatList=new ArrayList<>();
		List<SubCategoryTypeBean> subList=new ArrayList<>();
		String sql="",sql1="",sql2="";
		int row;
		 sql="insert into lu_sub_category_type(category_type_id,sub_category_type,created_on) values(?,?,?)";
			
		try{
			
			jdbcTemplate.update(sql, new Object[]{
					subCategoryBean.getCategoryId(),subCategoryBean.getSubCategoryType(),new Date()});
			
			sql1="select sub_category_type_id from lu_sub_category_type where sub_category_type='"+subCategoryBean.getSubCategoryType()+"'";
			subList=jdbcTemplate.query(sql1, new RowMapper(){
				public SubCategoryTypeBean mapRow(ResultSet rs,int rownumber) throws SQLException {
					SubCategoryTypeBean bean=new SubCategoryTypeBean();
					bean.setSubCategoryTypeId(rs.getString("sub_category_type_id"));
					return bean;
				}	
			});
			
			subCatList=removeRow(subCategoryBean);
			//System.out.println("cList value is ->"+subCatList);
			for(int i=0;i<subCatList.size();i++) 
			{
				//System.out.println("UTF8 value is->"+cList.get(i).getCategoryName());
				//String test=new String(subCatList.get(i).getSubCategory().getBytes("ISO-8859-1"),"UTF-8");
				//System.out.println("UTF8 test value is->"+test);
				
				//dataStr = new String(dataStr.getBytes("ISO-8859-1"),"UTF-8");
				sql2="insert into lu_sub_category(sub_category_type_id,language_id,sub_category_name,created_on) values(?,?,?,?)";
				
				jdbcTemplate.update(sql2, new Object[]
						{
						subList.get(0).getSubCategoryTypeId(),subCatList.get(i).getLanguageId(),subCatList.get(i).getSubCategory(),new Date()});
			}
			fileName=uploadImage(subCategoryBean,filePath);
		}
		
		catch(Exception e)
		{
			System.out.println("exception in addSubcategory() in category--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public String updatesubcatStatus(HttpServletRequest request)
	{

		List<SubCategoryTypeBean> list=new ArrayList<SubCategoryTypeBean>();
		//System.out.println("in ChangeStatus");
		String sql="select status from lu_sub_category_type where sub_category_type_id='"+request.getParameter("subCategoryTypeId")+"'";
		//System.out.println("query "+sql);
		list=jdbcTemplate.query(sql, new RowMapper()
		{
			public SubCategoryTypeBean mapRow(ResultSet rs, int rowNumber) throws SQLException {
				SubCategoryTypeBean bean=new SubCategoryTypeBean();
				bean.setStatus(rs.getString(1));
				return bean;
				
			}
		});
		if(Integer.parseInt(list.get(0).getStatus().toString())==1)
		{
			jdbcTemplate.update("update lu_sub_category_type set status='0' where sub_category_type_id='"+request.getParameter("subCategoryTypeId")+"'");
		}
		else
		{
			jdbcTemplate.update("update lu_sub_category_type set status='1' where sub_category_type_id='"+request.getParameter("subCategoryTypeId")+"'");
		}
		return "Success";
	}
	
	/***** 
	 * To Remove Empty Rows *
	 *****/
	
	public List<SubCategoryBean> removeRow(SubCategoryTypeBean categoryBean)
	{
		List<SubCategoryBean> clist=new ArrayList<>();
		for(SubCategoryBean cate:categoryBean.getSubCategoryList())
		{
			if(cate.getSubCategory()!=null && cate.getLanguageId()!=null)
			{
				clist.add(cate);
			}
		}
		return clist;
	}
	

	/*************************************************
	 ********* image uploading function **************
	 *************************************************/
	
	public String uploadImage(SubCategoryTypeBean categoryBean,String path)
	{
		String fileName="",file="";
		String fileExt="";
		CommonsMultipartFile multipartFile=categoryBean.getSubcategoryImage();
		if(multipartFile!=null)
		{	
			int index;
			file=categoryBean.getSubCategoryType();
			fileName=multipartFile.getOriginalFilename();
			index=fileName.lastIndexOf(".");
			fileExt=fileName.substring(index+1);
			
		}
		//fileName=file+"."+fileExt;
		fileName=file+".jpg";
		System.out.println("filepath-->"+path);
		String filePath = path+"/Document/Admin/Category_Images/Subcategory_Images/"+fileName;	
	    File destination = new File(filePath);
	    String status ="success";
	    
	    try
	    {
	    	multipartFile.transferTo(destination);
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in upload Subcategory Image in addSubcategory--> "+e);
	    	TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
	    }
	    return fileName;
	}
	
	public SubCategoryTypeBean getSubCategoryDetail(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String sql="",sql1="",path="";
		SubCategoryTypeBean subCategoryBean=new SubCategoryTypeBean();
		List<SubCategoryTypeBean> list=new ArrayList<>();
		List<SubCategoryBean> clist=new ArrayList<>();
		String id=request.getParameter("subCategoryTypeId");
		sql="select sub_category_type_id,category_type_id,sub_category_type from lu_sub_category_type where sub_category_type_id='"+id+"'";
		sql1="select sub_category_id,language_id,sub_category_name from lu_sub_category where sub_category_type_id='"+id+"'";
		list=jdbcTemplate.query(sql, new RowMapper() {
			public SubCategoryTypeBean mapRow(ResultSet rs,int rowNumber) throws SQLException{
				SubCategoryTypeBean bean=new SubCategoryTypeBean();
				bean.setSubCategoryTypeId(rs.getString("sub_category_type_id"));
				bean.setSubCategoryType(rs.getString("sub_category_type"));
				bean.setCategoryId(rs.getString("category_type_id"));
				return bean;
			}
		});
		subCategoryBean=list.get(0);
		path="/Document/Admin/Category_Images/Subcategory_Images/"+list.get(0).getSubCategoryType()+".jpg";
		clist=jdbcTemplate.query(sql1,new SubCategoryRowMapper());
		subCategoryBean.setSubCategoryList(clist);
		subCategoryBean.setImagePath(path);
		session.setAttribute("subCategoryId",id);
		//System.out.println(list);
		return subCategoryBean;
	}
	
	/*
	 * update subcategory information
	 */
	public void editSubCategory(String subCategoryId,SubCategoryTypeBean subCategoryBean, String filePath)
	{
		String sql="",sql1="",sql2="",fileName="";
		StringBuffer sb;	
		List<SubCategoryBean> clist=new ArrayList<>();	
		sql="update lu_sub_category_type set sub_category_type='"+subCategoryBean.getSubCategoryType()+"' where sub_category_type_id='"+subCategoryId+"'";
		try
		{
			jdbcTemplate.update(sql);
			clist=removeRow(subCategoryBean);
			fileName=uploadImage(subCategoryBean,filePath);
			if(clist!=null)
			{
				Integer currentSize=Integer.parseInt(subCategoryBean.getSizeOfList());
				if(subCategoryBean.getSubCategoryIdList().length()>0)
				{
					String[] categoryids=subCategoryBean.getSubCategoryIdList().split(",");
					SubCategoryBean newCategory;
					System.out.println("in edit sub category gfd");
					/********
					 *  for update already exists rows *
					 *******/
					if(currentSize>0 && categoryids.length>0)
					{
						for(int i=0;i<categoryids.length;i++)
						{
							System.out.println("in update already exists subcategory");
							newCategory=clist.get(Integer.parseInt(categoryids[i]));
							String query="update lu_sub_category set language_id='"+newCategory.getLanguageId()+"',sub_category_name='"+newCategory.getSubCategory()+"' where sub_category_id='"+newCategory.getSubCategoryId()+"'";
							jdbcTemplate.update(query);
						}
					}
				}
				/*****
				 ** for insert newly added rows *
				 *********/
				if(clist.size()>currentSize)
				{
					sb=new StringBuffer("insert into lu_sub_category(sub_category_type_id,language_id,sub_category_name) values ");
					for(int i=currentSize;i<clist.size();i++)
					{
						sb.append("("+subCategoryId+","+clist.get(i).getLanguageId()+",'"+clist.get(i).getSubCategory()+"'),");
					}
					sql2=sb.toString();
					/**** to remove , from query *****/
					if(sql2.charAt(sql2.length()-1)==','){
					   sql2 = sql2.substring(0, sql2.length()-1);
					  }		 
					jdbcTemplate.update(sql2);
				}
			}
		}
		catch(Exception e)
		{
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}
	
	/*******
	 * check subcategory name already exists or not
	 */
	public String validatesubCategoryName(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select sub_category_type_id from lu_sub_category_type where sub_category_type='"+request.getParameter("subCategory")+"'";
			List<SubCategoryTypeBean> list=jdbcTemplate.query(sql, new RowMapper(){
					public SubCategoryTypeBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						SubCategoryTypeBean bean=new SubCategoryTypeBean();
						bean.setSubCategoryTypeId(rs.getString("sub_category_type_id"));
						return bean;
					}
			});
			System.out.println("list-->"+list);
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validatesubCategoryName() "+e.toString());	
		}		
		return target;
	}
	
}
