package com.admin.subCategory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.admin.category.CategoryBean;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class SubCategoryController {
	
	@Autowired
	SubCategoryDAO subCateDao;
	@Autowired
	CommonFunction commFunc;
	@Autowired
	ServletContext servletContext;
	
	@RequestMapping(value="/subCategoryList",method=RequestMethod.GET)
	public ModelAndView subCategoryList(HttpServletRequest request)
	{
		
		List<SubCategoryTypeBean> list=null;
		List<CommonBean> languagelist=new ArrayList<>();
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}    
		list=subCateDao.getSubCatgeoryList(request,startIndex,15);
		languagelist=commFunc.getlanguageList();
		ModelAndView model=new ModelAndView("admin/subCategory/subCategoryList");
		model.addObject("subCategoryList",list);
		model.addObject("languagelist",languagelist);
		model.addObject("search_sub_category",new SubCategoryTypeBean());
		return model;
	}
	
	@RequestMapping(value="/addSubCategory",method=RequestMethod.GET)
	public ModelAndView addCategory()
	{
		List<CommonBean> language=new ArrayList<CommonBean>();
		List<CommonBean> category=new ArrayList<CommonBean>();
		language=commFunc.getlanguageList();
		category=commFunc.getCategoryTypeList();
		ModelAndView model=new ModelAndView("admin/subCategory/addSubCategory");
		model.addObject("add_sub_category",new SubCategoryTypeBean());
		model.addObject("languageList",language);
		model.addObject("categoryList",category);
		return model;
	}

	@RequestMapping(value="/submitSubCategory",method=RequestMethod.POST)
	public String AddCategory(@ModelAttribute("add_sub_category") SubCategoryTypeBean subCategoryBean,ModelMap model,HttpServletRequest request)
	{		
		String filePath="",fileName="";
		filePath=servletContext.getRealPath("");
		subCateDao.addSubcategory(subCategoryBean,request,filePath);
		model.addAttribute("search_sub_category", new SubCategoryBean());
		return "redirect:/subCategoryList";
	}
	
	@RequestMapping(value="/searchSubCategory",method=RequestMethod.POST)
	public String searchCategory(@ModelAttribute("search_sub_category")SubCategoryTypeBean categoryBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("subCategoryBean",categoryBean);
		return "redirect:/subCategoryList";
	}
	@RequestMapping(value="/updateSubCategoryStatus",method=RequestMethod.GET)
	public String updateCategoryStatus(@ModelAttribute("search_sub_category")SubCategoryTypeBean subCategoryBean,HttpServletRequest request)
	{
		String checkStatus=subCateDao.updatesubcatStatus(request);
		return "redirect:/subCategoryList";
	}
	
	@RequestMapping(value="/updateSubCategory",method=RequestMethod.GET)
	public ModelAndView updateCategory(HttpServletRequest request)
	{
		List<CommonBean> category=new ArrayList<CommonBean>();
		SubCategoryTypeBean subCategoryBean=subCateDao.getSubCategoryDetail(request);
		List<CommonBean> languageList=commFunc.getlanguageList();
		category=commFunc.getCategoryTypeList();
		ModelAndView model=new ModelAndView("admin/subCategory/updateSubCategory");
		model.addObject("update_sub_category",subCategoryBean);
		model.addObject("languageList", languageList);
		model.addObject("categoryList",category);
		return model;
	}
	
	@RequestMapping(value="/editSubCategory",method=RequestMethod.POST)
	public String EditCountry(@ModelAttribute("update_sub_category")SubCategoryTypeBean subCategoryBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String filePath=servletContext.getRealPath("");
		String id=(String)session.getAttribute("subCategoryId");
		subCateDao.editSubCategory(id,subCategoryBean,filePath);
		return "redirect:/subCategoryList";
	}
	
	@RequestMapping(value="/ValidateSubCategoryName",method=RequestMethod.GET)
	public String checkSubcategoryName(HttpServletRequest request,ModelMap model)
	{
		String chkCategory=subCateDao.validatesubCategoryName(request);
		model.addAttribute("checkSubCategory", chkCategory);
		return "ajaxOperation/ajaxOperation";
		
	}
}
