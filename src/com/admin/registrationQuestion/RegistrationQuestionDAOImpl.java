package com.admin.registrationQuestion;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.admin.category.Category;
import com.admin.category.CategoryBean;
import com.admin.category.CategoryRowMapper;
import com.admin.registrationQuestion.RegistrationQueastionBean;
import com.admin.registrationQuestion.CultureAddMoreBean;

@Repository
@Transactional
public class RegistrationQuestionDAOImpl implements RegistrationQuestionDAO 
{	
	private JdbcTemplate jdbcTemplate;
	@Autowired		
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		//System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}	
	public void add(RegistrationQueastionBean registrationQueastionBean, HttpServletRequest request)
	{
		String languageId,category,catgoryNameInNative;		
		List<CultureAddMoreBean> cList=new ArrayList<>();
		String sql="",sql1="",sql2="";
		int row;
		 sql="insert into lu_registration_question(question,culture_id,status,created_on) values(?,?,?,?)";
			System.out.println("query "+sql);			 
		try{
			System.out.println("1");
			jdbcTemplate.update(sql, new Object[]{
					registrationQueastionBean.getRegQuestion(),"0","1",new Date()});			
			
			sql1="select registration_question_id from lu_registration_question where question='"+registrationQueastionBean.getRegQuestion()+"' and status='1' ";			
			List<RegistrationQueastionBean> list=jdbcTemplate.query(sql1, new RowMapper(){
				public RegistrationQueastionBean mapRow(ResultSet rs,int rownumber) throws SQLException{
					RegistrationQueastionBean bean=new RegistrationQueastionBean();
					bean.setRegQuestionId(rs.getString(1));
					
					return bean;
				}
			});			
			
			System.out.println("3");
			//System.out.println(categoryBean.getRowCount());
			cList=removeRow(registrationQueastionBean);
			
			System.out.println("cList value is ->"+cList);
			
			for(int i=0;i<cList.size();i++) 
			{
				System.out.println("UTF8 value is->"+cList.get(i).getRegistraionQuestion());
				System.out.println("culture id value is ->"+cList.get(i).getCultureId());
				//dataStr = new String(dataStr.getBytes("ISO-8859-1"),"UTF-8");
				sql2="insert into lu_registration_question(culture_id,main_question_id,question,registration_status,status,created_on) values(?,?,?,?,?,?)";
				System.out.println("sql2 value is ->"+sql2);
				jdbcTemplate.update(sql2, new Object[]
						{
						cList.get(i).getCultureId(),list.get(0).getRegQuestionId(),cList.get(i).getRegistraionQuestion(),cList.get(i).getRegistration_status(),"1",new Date()
						});					
			}
		}
		
		catch(Exception e)
		{
			System.out.println("exception in add() in RegistrationQuestionDAOImpl.java--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			//throw e;
		}		
	}	
	public List getRegistrationList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="",qryGlobalQuest="",globalid="";
		//Boolean status=false;
		StringBuffer sb=new StringBuffer();
		List<RegistrationQueastionBean> registrationList=new ArrayList<RegistrationQueastionBean>();
		List<String> getGlobalId=new ArrayList<String>();
		String sql="";
		try
			{
			sb.append(" and ");		
			
			if(session.getAttribute("registrationBean")!=null)
			{
				RegistrationQueastionBean registrationBean=(RegistrationQueastionBean)session.getAttribute("registrationBean");	
				if(registrationBean.getRegQuestion()!=null)
	 		   	{
		 			if(!registrationBean.getRegQuestion().equals(""))
		 			{
		 				//status=true;
		 				sb.append("question like '%"+registrationBean.getRegQuestion()+"%'").append(" and ");		 				
		 			}
	 		   }
				System.out.println("cult id is ->"+registrationBean.getCultureId());
	 		   
				if(registrationBean.getCultureId()!=null)
	 		   {
		 			if(!registrationBean.getCultureId().equals("NONE"))
		 			{	
		 				//status=true;
		 				qryGlobalQuest="select distinct main_question_id from lu_registration_question where culture_id='"+registrationBean.getCultureId()+"'";
		 				
		 				getGlobalId=jdbcTemplate.query(qryGlobalQuest, new RowMapper<String>(){
			 				   @Override
			 				   public String mapRow(ResultSet rs, int rowNum) throws SQLException {    
			 				    return rs.getString("main_question_id");
			 				   }   
			 				  });
		 				int size=0;
		 				size=getGlobalId.size()-1;		 				
		 				for(int i=0;i<getGlobalId.size();i++)
		 				{		 				
		 					globalid=globalid+getGlobalId.get(i);
		 					if(i<size)
		 					{
		 						globalid=globalid+",";	
		 					}		 					
		 				}							 				
		 				sb.append(" registration_question_id in("+globalid+") ").append(" and ");	 				
		 			}
	 		   } 
	 		   
			}
			
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}	
	 		
	 			
	 			qryStartPart="select registration_question_id,question,status from lu_registration_question where culture_id='0' ";
	 			//qryStartPart="select distinct ct.category_type_id,ct.category_type,ct.status from lu_category_type ct ,lu_language l,lu_category c where ct.category_type_id=c.category_type_id and l.lang_id=c.language_id   ";
	 			qrylast=" order by registration_question_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart+qrylast; 
				System.out.println("sql query is ->"+sql);
				registrationList=jdbcTemplate.query(sql,new RowMapper()
				{
					public RegistrationQueastionBean mapRow(ResultSet rs,int rownumber) throws SQLException {
						RegistrationQueastionBean bean=new RegistrationQueastionBean();	
						bean.setRegQuestionId(rs.getString(1));						
						bean.setRegQuestion(rs.getString(2));
						bean.setStatus(rs.getString(3));						
						return bean;
					}
				});
				
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(registration_question_id) from lu_registration_question where culture_id='0' ";
				//qryStartPart="select count(distinct ct.category_type_id) from lu_category_type ct ,lu_language l,lu_category c where ct.category_type_id=c.category_type_id and l.lang_id=c.language_id  ";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);				
			}
			catch(Exception e)
			{
				System.out.println("exception in getRegistrationList in RegistrationQuestionDAPImpl.java file->"+e.toString());
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		
		session.removeAttribute("registrationBean");
		return registrationList;
	}
	
	/***** 
	 * To Remove Empty Rows from Add more*
	 *****/	
	
	public List<CultureAddMoreBean> removeRow(RegistrationQueastionBean registrationQueastionBean)
	{
		List<CultureAddMoreBean> clist=new ArrayList<>();
		for(CultureAddMoreBean culBean:registrationQueastionBean.getRegistraionList())
		{	
			if(culBean.getRegistraionQuestion()!=null && culBean.getCultureId()!=null)
			{
				clist.add(culBean);
				
			}
		}
		return clist;
	}
	
	/*******
	 * for checking duplicate Registration question value 
	 */
	public String validateRegQuestionName(HttpServletRequest request)
	{
		String target="";
		try
		{
			String sql="select registration_question_id from lu_registration_question where question='"+request.getParameter("question")+"' and culture_id='0' ";
			List<RegistrationQueastionBean> list=jdbcTemplate.query(sql, new RowMapper(){
				public RegistrationQueastionBean mapRow(ResultSet rs,int rownumber) throws SQLException{
					RegistrationQueastionBean bean=new RegistrationQueastionBean();
					bean.setRegQuestionId(rs.getString(1));
					return bean;
				}	
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}

		}
		catch(Exception e)
		{
			System.out.println("Exception in validateRegQuestionName() under RegistrationQuestionDAOImpl.java  "+e.toString());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}		
		return target;
	}
	public RegistrationQueastionBean getRegisterDetail(HttpServletRequest request)
	{
		//HttpSession session=request.getSession(true);
		String sql="",sql1="";
		RegistrationQueastionBean regQuestBean=new RegistrationQueastionBean();		
		List<RegistrationQueastionBean> list=new ArrayList<>();
		List<CultureAddMoreBean> clist=new ArrayList<>();			
		sql="select registration_question_id,question from lu_registration_question where registration_question_id='"+request.getParameter("id")+"' ";
		sql1="select registration_question_id,question,culture_id,registration_status from lu_registration_question where main_question_id='"+request.getParameter("id")+"' and status='1' ";
		list=jdbcTemplate.query(sql, new RowMapper<RegistrationQueastionBean>() {
			public RegistrationQueastionBean mapRow(ResultSet rs,int rowNumber) throws SQLException{
				RegistrationQueastionBean bean=new RegistrationQueastionBean();
				bean.setRegQuestionId(rs.getString(1));
				bean.setRegQuestion(rs.getString(2));
				return bean;
			}
		});
		
		regQuestBean=list.get(0);
		clist=jdbcTemplate.query(sql1,new RowMapper<CultureAddMoreBean>(){
			public CultureAddMoreBean mapRow(ResultSet rs1,int rowNumber) throws SQLException{
				CultureAddMoreBean culBean=new CultureAddMoreBean();
				culBean.setQuestionId(rs1.getString(1));			
				culBean.setRegistraionQuestion(rs1.getString(2));
				culBean.setCultureId(rs1.getString(3));
				culBean.setRegistration_status(rs1.getString(4));
				return culBean;
			}
		});
		System.out.println("sql1 query is ->"+sql1);
		
		System.out.println("clist size is ->"+clist.size());
		regQuestBean.setRegistrationList(clist);
		
		//session.setAttribute("categoryId",id);
		//System.out.println(list);
		return regQuestBean;
	}
	public String updateStatus(HttpServletRequest request)
	{	
			jdbcTemplate.update("update lu_registration_question set status='"+request.getParameter("st")+"' where registration_question_id='"+request.getParameter("id")+"'");
	
		return "Success";
	}
	/*
	 * update category information
	 */
	
	public void editRegQuest(RegistrationQueastionBean regQuestBean)
	{
		
		String sql="",sql1="",sql2="",fileName="",query="";
		StringBuffer sb;	
		List<CultureAddMoreBean> clist=new ArrayList<>();	
		sql="update lu_registration_question set question='"+regQuestBean.getRegQuestion()+"' where registration_question_id='"+regQuestBean.getRegQuestionId()+"' ";
		System.out.println("sql value is ->"+sql);
		jdbcTemplate.update(sql);
		clist=removeRow(regQuestBean);		
		if(clist!=null)
		{
			Integer currentSize=Integer.parseInt(regQuestBean.getSizeOfList());
			if(regQuestBean.getCategoryIdList().length()>0)
			{
				String[] categoryids=regQuestBean.getCategoryIdList().split(",");
				CultureAddMoreBean newCategory;
				
				/********
				 *  for update already exists rows *
				 *******/
				if(currentSize>0 && categoryids.length>0)
				{
					for(int i=0;i<categoryids.length;i++)
					{
						newCategory=clist.get(Integer.parseInt(categoryids[i]));
						query="update lu_registration_question set culture_id='"+newCategory.getCultureId()+"',question='"+newCategory.getRegistraionQuestion()+"',registration_status='"+newCategory.getRegistration_status()+"' where registration_question_id='"+newCategory.getQuestionId()+"'";
						System.out.println("query value is ->"+query);
						jdbcTemplate.update(query);
					}
				}
			}
			/*****
			 ** for insert newly added rows *
			 *********/
			if(clist.size()>currentSize)
			{	
				sb=new StringBuffer("insert into lu_registration_question(culture_id,main_question_id,question,registration_status,status,created_on) values ");
				for(int i=currentSize;i<clist.size();i++)
				{
					//sb.append("("+clist.get(i).getLanguageId()+","+clist.get(i).getLanguageId()+",'"+clist.get(i).getCategoryName()+"'),");
					sb.append("("+clist.get(i).getCultureId()+","+regQuestBean.getRegQuestionId()+",'"+clist.get(i).getRegistraionQuestion()+"',"+clist.get(i).getRegistration_status()+",'1',now()),");					
				}
				sql2=sb.toString();
				/**** to remove , from query *****/
				if(sql2.charAt(sql2.length()-1)==','){
				   sql2 = sql2.substring(0, sql2.length()-1);
				  }		
				System.out.println("sql2 in edit registration question is ->"+sql2);
				jdbcTemplate.update(sql2);
				
				
			}
		}
		
	}
	

}
