package com.admin.registrationQuestion;

public class CultureAddMoreBean {
	private String cultureId,registraionQuestion,registration_status,questionId;
	
	public String getCultureId()
	{
		return cultureId;		
	}	
	public void setCultureId(String cultureId)
	{
		this.cultureId=cultureId;		
	}	
	public String getRegistraionQuestion()
	{
		return registraionQuestion;
	}
	public void setRegistraionQuestion(String registraionQuestion)
	{
		this.registraionQuestion=registraionQuestion;
	}
	public String getRegistration_status()
	{
		return registration_status;
	}
	public void setRegistration_status(String registration_status)
	{
		this.registration_status=registration_status;
	}
	public String getQuestionId()
	{
		return questionId;
	}
	public void setQuestionId(String questionId)
	{
		this.questionId=questionId;		
	}
	
}
