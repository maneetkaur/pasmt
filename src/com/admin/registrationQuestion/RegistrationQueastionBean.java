package com.admin.registrationQuestion;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

import com.admin.registrationQuestion.CultureAddMoreBean;

public class RegistrationQueastionBean {
	 
	private String regQuestionId,regQuestion,status,cultureId,sizeOfList,categoryIdList;
	private List<CultureAddMoreBean> registraionList;

	public List<CultureAddMoreBean> getRegistraionList()
	{
		return registraionList;
	}
	public void setRegistrationList(List<CultureAddMoreBean>  registraionList)
	{
		this.registraionList=registraionList;
	}
	public RegistrationQueastionBean()
	{
		registraionList=LazyList.decorate(new ArrayList<CultureAddMoreBean>(), new InstantiateFactory(CultureAddMoreBean.class));
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status=status;
	}
	public String getRegQuestionId() {
		return regQuestionId;
	}

	public void setRegQuestionId(String regQuestionId) {
		this.regQuestionId = regQuestionId;
	}

	public String getRegQuestion() {
		return regQuestion;
	}

	public void setRegQuestion(String regQuestion) {
		this.regQuestion = regQuestion;
	}
	
	public String getCultureId()
	{
		return cultureId;
	}
	
	public void setCultureId(String cultureId)
	{
		this.cultureId=cultureId;
	}
	public String getSizeOfList() {
		return sizeOfList;
	}
	public void setSizeOfList(String sizeOfList) {
		this.sizeOfList = sizeOfList;
	}
	public String getCategoryIdList() {
		return categoryIdList;
	}
	public void setCategoryIdList(String categoryIdList) {
		this.categoryIdList = categoryIdList;
	}
}
