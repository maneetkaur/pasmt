package com.admin.registrationQuestion;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import com.admin.category.CategoryBean;

public interface RegistrationQuestionDAO 
{
	public void add(RegistrationQueastionBean registrationQueastionBean,HttpServletRequest request);
	public List getRegistrationList(HttpServletRequest request,int startIndex,int range);
	public String validateRegQuestionName(HttpServletRequest request);
	public RegistrationQueastionBean getRegisterDetail(HttpServletRequest request);
	public String updateStatus(HttpServletRequest request);
	public void editRegQuest(RegistrationQueastionBean regQuestBean);
	

}
