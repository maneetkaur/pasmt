package com.admin.registrationQuestion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.admin.category.CategoryBean;
import com.admin.category.CategoryDAO;
import com.admin.registrationQuestion.RegistrationQueastionBean;
import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class RegistrationQuestionController {
	
	@Autowired
	CommonFunction commFunc;
	
	@Autowired
	RegistrationQuestionDAO registrationDao;
	
	@RequestMapping(value="/addRegistraionQuestion", method=RequestMethod.GET)
	public ModelAndView regisatrtionQuestion(HttpServletRequest request)
	{
		List<CommonBean> culture=new ArrayList<CommonBean>();
		culture=commFunc.getCultureList();		
		ModelAndView model=new ModelAndView("admin/registrationPageQuest/addRegistrationQuestion");
		model.addObject("registartionQuesForm", new RegistrationQueastionBean());
		model.addObject("cultureList",culture);		
		return model;
	}
	@RequestMapping(value="/submitRegistrationForm",method=RequestMethod.POST)
	public String AddCategory(@ModelAttribute("registartionQuesForm") RegistrationQueastionBean registrationBean,ModelMap model,HttpServletRequest request)
	{		
		registrationDao.add(registrationBean, request);
		//model.addAttribute("search_category", new CategoryBean());
		return "redirect:/registrationQuestionList";
	}
	
	@RequestMapping(value="/registrationQuestionList",method=RequestMethod.GET)
	public ModelAndView registrationList(HttpServletRequest request)
	{	
		List<CategoryBean> list=null;
		List<CommonBean> culture=new ArrayList<CommonBean>();
		culture=commFunc.getCultureList();
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		list=registrationDao.getRegistrationList(request, startIndex, 15);		
		ModelAndView model=new ModelAndView("admin/registrationPageQuest/registrationQuestionList");
		model.addObject("registrationList",list);
		model.addObject("cultureList",culture);
		model.addObject("search_registration",new RegistrationQueastionBean());
		return model;
	}
	@RequestMapping(value="/searchRegistration",method=RequestMethod.POST)
	public String searchCategory(@ModelAttribute("search_registration")RegistrationQueastionBean registrationBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("registrationBean",registrationBean);
		return "redirect:/registrationQuestionList";
	}
	@RequestMapping(value="/ValidateRegQuestionName",method=RequestMethod.GET)
	public String checkCompanyName(HttpServletRequest request,ModelMap model)
	{	
		String chkRegStatus=registrationDao.validateRegQuestionName(request);
		model.addAttribute("checkRegistration", chkRegStatus);
		return "ajaxOperation/ajaxOperation";		
	}
	@RequestMapping(value="/updateRegQuestion",method=RequestMethod.GET)
	public ModelAndView updateQuestion(HttpServletRequest request)
	{
		String fileName="";
		RegistrationQueastionBean questionBean=registrationDao.getRegisterDetail(request);
		List<CommonBean> culture=new ArrayList<CommonBean>();
		culture=commFunc.getCultureList();	
		
		ModelAndView model=new ModelAndView("admin/registrationPageQuest/updateRegistrationQuestion");
		model.addObject("update_RegQuestion",questionBean);
		model.addObject("cultureList",culture);		
		return model;
	}
	@RequestMapping(value="/updateRegistrationStatus",method=RequestMethod.GET)
	public String updateRegistrationStatus(@ModelAttribute("search_registration")RegistrationQueastionBean registrationBean,HttpServletRequest request)
	{
		String checkStatus=registrationDao.updateStatus(request);
		return "redirect:/registrationQuestionList";
	}
	@RequestMapping(value="/editRegQuestion",method=RequestMethod.POST)
	public String EditCountry(@ModelAttribute("update_registration")RegistrationQueastionBean regQuestBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);		
		String id=(String)session.getAttribute("categoryId");
		registrationDao.editRegQuest(regQuestBean);		
		return "redirect:/registrationQuestionList";
	}
	
}
