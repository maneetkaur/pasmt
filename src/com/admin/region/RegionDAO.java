package com.admin.region;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RegionDAO {
	
	public String RegionAdd(RegionBean bean,String filePath);
	public String uploadFile(RegionBean regionBean, String filepath);
	public List<RegionBean> getRegionList(HttpServletRequest request,int startIndex,int range);
	public void downloadFile(HttpServletRequest request,HttpServletResponse response,String filepath);
	public String validateRegion(HttpServletRequest request);

}
