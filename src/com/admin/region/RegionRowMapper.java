package com.admin.region;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RegionRowMapper implements RowMapper<RegionBean>{

	@Override
	public RegionBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		RegionBean bean=new RegionBean();
		bean.setCountryId(rs.getString(1));
		bean.setCountry(rs.getString(2));
		bean.setCountryCode( rs.getString(3));
		return bean;
	}
	

}
