package com.admin.region;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Repository
@Transactional
public class RegionDAOImpl implements RegionDAO
{
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")

	public void setJdbcTemplate(DataSource dataSource)
	{
		System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	public String RegionAdd(RegionBean regionBean,String filepath)
	{
		String sql="",fileName="";
		if(regionBean.getRegionId()!=null)
		{
			sql="update region set country_id= '"+regionBean.getCountryId()+"' where region_id ="+regionBean.getRegionId();
			jdbcTemplate.update(sql);
		}
		else
		{
			sql="insert into region(country_id) values(?)";
			System.out.println("country--> "+regionBean.getCountry());
			fileName=uploadFile(regionBean,filepath);
			//System.out.println("query "+sql);	
		}
		try{
			jdbcTemplate.update(sql, new Object[]{
					regionBean.getCountryId()});
			
		}
		catch(Exception e)
		{
			System.out.println("exception in add() in culture "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return fileName;
	 
	}
	public String uploadFile(RegionBean regionBean,String path)
	{
		String fileName="",file="";
		String fileExt=".txt";
		List<RegionBean> list=new ArrayList<>();
		CommonsMultipartFile multipartFile=regionBean.getFile();
		String sql="select country_name from lu_country where country_id='"+regionBean.getCountryId()+"'";
		//System.out.println("query--> "+sql);
		list=jdbcTemplate.query(sql, new RowMapper(){
			public RegionBean mapRow(ResultSet rs,int rownumber) throws SQLException {
				RegionBean bean=new RegionBean();
				bean.setCountry(rs.getString(1));
				return bean;
			}
		});
		if(multipartFile!=null)
		{
			file=list.get(0).getCountry();
			//fileName=multipartFile.getOriginalFilename();
		}
		fileName=file+fileExt;
		String filePath = path+"/Document/Admin/ZipCode/"+fileName;	
	    File destination = new File(filePath);
	    String status ="success";
	    try
	    {
	    	multipartFile.transferTo(destination);
	    }
	    catch(Exception e)
	    {
	    	System.out.println("Exception in upload zipCode file--> "+e);
	    	TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
	    }
	    return fileName;
	}
	
	public List<RegionBean> getRegionList(HttpServletRequest request,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qry="",sql1="";
		String qrylast="";
		String countryIds="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		List<RegionBean> regionList=new ArrayList<RegionBean>();
		List<String> idList=new ArrayList<>();
		String sql="";
		try
			{
			sb.append("and ");
			if(session.getAttribute("regionBean")!=null)
			{
				RegionBean regionBean=(RegionBean)session.getAttribute("regionBean");	
				if(regionBean.getCountry()!=null)
	 		   	{
		 			if(!regionBean.getCountry().equals(""))
		 			{
		 				sb.append("country_name like '%"+regionBean.getCountry()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(regionBean.getCountryCode()!=null)
	 		   {
		 			if(!regionBean.getCountryCode().equals(""))
		 			{
		 				sb.append(" country_code like '%"+regionBean.getCountryCode()+"%'").append(" and ");
		 				
		 			}
	 		   } 
			}
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5) 
	 			{
	 				qryPart="";
	 			}
			
	 			//qryStartPart="select cul.culture_id,cul.country_id,con.country_name,cul.lang_id,lang.language,cul.osbt_status,lang.lang_code,con.country_code from culture cul,country con,language lang where cul.country_id=con.country_id and cul.lang_id=lang.lang_id ";
	 			sql1="select distinct(country_id) from lu_region";
	 			idList=jdbcTemplate.query(sql1, new RowMapper() 
	 			{
	 				public String mapRow(ResultSet rs, int rowNumber)throws SQLException {
	 						String val="";
	 						val=rs.getString(1);
	 					return val;
	 				}
				});
	 			if(idList!=null & idList.size()>0)
	 			{
	 				countryIds=idList.get(0);
	 				for(int i=1;i<idList.size();i++)
	 				{
	 					countryIds=countryIds+","+idList.get(i);
	 				}
	 				
	 			}
	 			qryStartPart="select country_id,country_name,country_code from lu_country where country_id in("+countryIds+")";
	 			qrylast="order by country_id limit "+startIndex+ "," +range;
				sql=qryStartPart+qryPart+qrylast; 
				regionList=jdbcTemplate.query(sql,new RegionRowMapper());
				
				/*
				 * count for pagination 
				 */
				qryStartPart="select count(1) from lu_country where country_id in("+countryIds+")";
				sql=qryStartPart+qryPart;
				int total_rows = jdbcTemplate.queryForInt(sql);
				request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in getRegionList "+e.toString());
			}
		
		session.removeAttribute("regionBean");
		return regionList;
	}
	
	public void downloadFile(HttpServletRequest request,HttpServletResponse response,String filepath)
	{
		ServletContext servletContext = request.getServletContext();
		String path=filepath+"/Document/Admin/ZipCode/";
		String fileExt=".csv";
		String fileName=request.getParameter("country");
		String fullPath=path+fileName+fileExt;
		System.out.println("path="+fullPath);
		try
		{
			 File downloadFile = new File(fullPath);
		     FileInputStream inputStream = new FileInputStream(downloadFile);
		     String mimeType = servletContext.getMimeType(fullPath);
	         if(mimeType == null)
	         {
	            mimeType = "application/octet-stream";
	         }
	        System.out.println("MIME type: " + mimeType);
	        response.setContentType(mimeType);
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	        OutputStream outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	        inputStream.close();
	        outStream.close();
		}
		catch(Exception e)
		{
			System.out.println("exception : "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public String validateRegion(HttpServletRequest request)
	{
		String target="";
		try
		{
			//System.out.println("nvbhjbhj"+(String)request.getParameter("CountryId"));
			String sql="select country_id from region where country_id='"+request.getParameter("CountryId")+"'";
			//System.out.println("lang sql="+sql);
			List<RegionBean> list=jdbcTemplate.query(sql, new RowMapper(){
				public RegionBean mapRow(ResultSet rs,int rownumber) throws SQLException {
					RegionBean bean=new RegionBean();
					bean.setCountryId(rs.getString(1));
					return bean;
				}
			});
			if(list!=null && ! list.isEmpty())
			{
				target="AlreadyExist";
			}
			else
			{
				target="NotHave";
				
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in validateRegion() "+e.toString());	
		}		
		return target;
	}
}
