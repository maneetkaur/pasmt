package com.admin.region;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class RegionController {
	
	@Autowired
	CommonFunction commonFunc;
	
	@Autowired
	RegionDAO regionDao;
	
	@Autowired
	ServletContext servletContext;
	
	@RequestMapping(value="/regionList",method=RequestMethod.GET)
	public ModelAndView listRegion(HttpServletRequest request)
	{
		List<RegionBean> regionList;
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			   startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		regionList=regionDao.getRegionList(request,startIndex,15);
		ModelAndView model=new ModelAndView("admin/region/regionList");
		model.addObject("regionList", regionList);
		model.addObject("search",new RegionBean());
		model.addObject("ZipCode",new RegionBean() );
		return model;
	}
	
	@RequestMapping(value="/addRegion",method=RequestMethod.GET)
	public ModelAndView addRegion()
	{
		List< CommonBean> countryList=commonFunc.getCountryList();
		ModelAndView model=new ModelAndView("admin/region/addRegion");
		model.addObject("add",new RegionBean());
		model.addObject("countryList",countryList);
		return model;
	}
	
	@RequestMapping(value="/AddRegion",method=RequestMethod.POST)
	public String RegionAdd(@ModelAttribute("add")RegionBean regionBean,ModelMap model)
	{
		String fileName="",filepath="";
		filepath=servletContext.getRealPath("");
		System.out.println("path-->"+filepath);
		fileName=regionDao.RegionAdd(regionBean,filepath);
		model.addAttribute("filename",fileName);
		return "redirect:/regionList";
	}
	
	@RequestMapping(value="/uploadZips",method=RequestMethod.POST)
	public String uploadZip(@ModelAttribute("ZipCode")RegionBean regionBean,ModelMap model)
	{
		String filename="",filepath;
		//System.out.println("region country= "+regionBean.getCountryId());
		filepath=servletContext.getRealPath("");
		filename=regionDao.uploadFile(regionBean,filepath);
		return "redirect:/regionList";
	}
	 
	@RequestMapping(value="/downloadFile",method=RequestMethod.GET)
	public String downloadZip(HttpServletRequest request,HttpServletResponse response)
	{
		String filepath=servletContext.getRealPath("");
		regionDao.downloadFile(request,response,filepath);
		return "redirect:/regionList";
	}
	
	@RequestMapping(value="/searchRegion",method=RequestMethod.POST)
	public String searchRegion(@ModelAttribute("search")RegionBean regionBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		session.setAttribute("regionBean",regionBean);
		return "redirect:/regionList";
	}
	
	@RequestMapping(value="/ValidateRegion",method=RequestMethod.GET)
	public String checkRegionName(HttpServletRequest request,ModelMap model)
	{
		String chkCountry=regionDao.validateRegion(request); 
		model.addAttribute("checkRegion", chkCountry);
		//System.out.println("check country"+chkCountry);
		//request.setAttribute("checkCountry", chkCountry);
		return "ajaxOperation/ajaxOperation";
		
	}

}
