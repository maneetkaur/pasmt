package com.pasmtAdmin.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.commonFunc.CommonBean;

@Repository
@Transactional
public class UserDaoImpl implements UserDao{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<UserBean> userList(HttpServletRequest request,
			int startIndex, int range) {
		try{
			boolean whereCondition = false;
			String sql = "SELECT pe.employee_id, pe.employee_name, pe.employee_code, pe.team,  pe.status, " +
					"(select lgv.generic_value from lu_generic_values lgv where lgv.generic_value_id = pe.department)" +
					" department, " +
					"(select lgv.generic_value from lu_generic_values lgv where lgv.generic_value_id = pe.role)" +
					" role, " +
					"(select lgv.description from lu_generic_values lgv where lgv.generic_value_id = xer.role_id)" +
					" access_role FROM  pasmt_employee pe, xref_employee_role xer ";
			
			//calculate count for pagination
			String sql_count = "SELECT count(1) FROM  pasmt_employee pe, xref_employee_role xer ";
			
			StringBuffer whereClause = new StringBuffer("WHERE xer.employee_id = pe.employee_id AND" +
					" pe.employee_id!=1 AND ");
			if(request.getParameter("employeeName") != null && !(request.getParameter("employeeName").equals(""))){
				whereCondition = true;
				whereClause.append("pe.employee_name LIKE '%" + request.getParameter("employeeName") + "%' AND ");
			}
			if(request.getParameter("userCode") != null && !(request.getParameter("userCode").equals(""))){
				whereCondition = true;
				whereClause.append("pe.employee_code LIKE '%" + request.getParameter("userCode") + "%' AND ");
			}
			
			//status
			if(request.getParameter("status")!=null){
				String status = request.getParameter("status");
				if(status.equals("NONE")){
					/*if some other search criteria has been selected then status until selected should not 
					 * be there in where clause*/
					if(!whereCondition){
						whereCondition = true;
						whereClause.append("pe.status=1 AND ");
					}
				}
				else if(status.equals("1")){
					whereCondition = true;
					whereClause.append("pe.status=1 AND ");
				}
				else if (status.equals("0")) {
					whereCondition = true;
					whereClause.append("pe.status=0 AND ");
				}
			}
			/*if the request parameter is null, then listing is called for the first time and status should be active
			 *  by default*/
			else{
				whereCondition = true;
				whereClause.append("pe.status=1 AND ");
			}
			String where=whereClause.toString().trim();
			whereClause=null;
			
			//remove the extra 'and' in the where clause
			where=where.substring(0, where.length()-4);
			
			sql_count = sql_count + where;
			int total_rows = jdbcTemplate.queryForInt(sql_count);
			request.setAttribute("total_rows", total_rows);
			String limit = " ORDER BY pe.employee_code LIMIT " + startIndex + ", " + range;
			sql = sql + where + limit;
			
			List<UserBean> userList = jdbcTemplate.query(sql, new UserListMapper());
			return userList;
		}
		catch(Exception e){
			System.out.println("Exception in userList of UserDaoImpl.java: " + e.toString());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updateStatus(String userId, int status) {
		try{
			if(status==1){
				status=0;
			}
			else{
				status=1;
			}
			String sql = "UPDATE pasmt_employee SET status=? WHERE employee_id=?";
			jdbcTemplate.update(sql, new Object[]{status, userId});
		}
		catch(Exception e){
			System.out.println("Exception in updateStatus of EmployeeDaoImpl.java: " + e);			
		}
	}

	@Override
	public List<CommonBean> getDepartmentList() {
		List<CommonBean> departmentList = new ArrayList<CommonBean>();
		String sql = "SELECT generic_value_id, generic_value FROM lu_generic_values WHERE generic_key='Dpt' " +
				"ORDER BY generic_value";
		
		departmentList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean department = new CommonBean();
				department.setId(rs.getString("generic_value_id"));
				department.setName(rs.getString("generic_value"));
				return department;
			}
		});
		return departmentList;
	}
	

	@Override
	public List<CommonBean> getAccessRoleList() {
		String sql = "SELECT generic_value_id, description FROM lu_generic_values WHERE generic_key='Erole' " +
				"ORDER BY description";
		List<CommonBean> accessRoleList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean accessRole = new CommonBean();
				accessRole.setId(rs.getString("generic_value_id"));
				accessRole.setName(rs.getString("description"));
				return accessRole;
			}
		});
		return accessRoleList;
	}

	@Override
	public void addUser(UserBean user) {
		try{
			String sql = "INSERT INTO pasmt_employee (username, password, employee_name, email_id, phone, address," +
					" employee_code, company_name, department, team, role, status, created_on) VALUES(?,?,?,?,?,?," +
					"?,?,?,?,?,?,?)";
			
			jdbcTemplate.update(sql, new Object[]{user.getUsername(), user.getPassword(), user.getEmployeeName(),
					user.getEmailId(), user.getPhone(), user.getAddress(), user.getUserCode(), user.getCompany(),
					user.getDepartment(), user.getTeam(), user.getRole(), user.getStatus(), new Date()});
			
			//get the user id of the latest entry
			sql = "SELECT MAX(employee_id) FROM pasmt_employee WHERE employee_code='" + user.getUserCode() + "'";
			String userId = (String)jdbcTemplate.queryForObject(sql, String.class);
			
			//Enter the access role in database
			sql = "INSERT INTO xref_employee_role (employee_id, role_id, status, created_on) VALUES (?,?,?,?)";
			jdbcTemplate.update(sql, new Object[]{userId, user.getAccessRole(), 1, new Date()});
		}
		catch(Exception e){
			System.out.println("Exception in addUser of UserDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public UserBean getUserInformation(String userId) {
		UserBean user = new UserBean();
		try{
			String sql = "SELECT pe.employee_id, pe.employee_name, pe.username, pe.password, pe.email_id, pe.phone" +
					", pe.address, pe.employee_code, pe.company_name, pe.department, pe.team, pe.role, pe.status, " +
					"xer.role_id FROM pasmt_employee pe JOIN xref_employee_role xer ON xer.employee_id = " +
					"pe.employee_id WHERE pe.employee_id=" + userId;
			
			user = jdbcTemplate.queryForObject(sql, new EditUserMapper());
		}
		catch(Exception e){
			System.out.println("Exception in getUserInformation of UserDaoImpl.java: " + e);
		}
		return user;
	}

	@Override
	public void editUser(UserBean user) {
		try{
			String sql = "UPDATE pasmt_employee SET employee_name=?, username=?, password=?, email_id=?, phone=?," +
					" address=?, employee_code=?, company_name=?, department=?, team=?, role=?, status=? WHERE " +
					"employee_id=?";
			
			jdbcTemplate.update(sql, new Object[]{user.getEmployeeName(), user.getUsername(), user.getPassword(),
					user.getEmailId(), user.getPhone(), user.getAddress(), user.getUserCode(), user.getCompany(),
					user.getDepartment(), user.getTeam(), user.getRole(), user.getStatus(), user.getUserId()});
			
			sql = "UPDATE xref_employee_role SET role_id=? WHERE employee_id=?";
			jdbcTemplate.update(sql, new Object[]{user.getAccessRole(), user.getUserId()});
		}
		catch(Exception e){
			System.out.println("Exception in editUser of UserDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
		
	}

	@Override
	public int validateUsername(String username) {
		int target = -1;
		
		String sql="SELECT count(1) FROM pasmt_employee WHERE username='" + username + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateUsername of UserDaoImpl.java" + e);
		}
		return target;
	}

}
