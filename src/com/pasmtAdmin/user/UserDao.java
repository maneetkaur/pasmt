package com.pasmtAdmin.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;

public interface UserDao {
	public List<UserBean> userList(HttpServletRequest request, int startIndex, int range);
	
	public void updateStatus(String userId, int status);
	
	public List<CommonBean> getDepartmentList();
	
	public List<CommonBean> getAccessRoleList();
	
	public void addUser(UserBean user);
	
	public UserBean getUserInformation(String userId);
	
	public void editUser(UserBean user);
	
	public int validateUsername(String username);

}
