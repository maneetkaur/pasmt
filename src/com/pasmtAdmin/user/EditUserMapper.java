package com.pasmtAdmin.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EditUserMapper implements RowMapper<UserBean>{

	@Override
	public UserBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserBean user = new UserBean();
		user.setUserId(rs.getString("pe.employee_id"));
		user.setEmployeeName(rs.getString("pe.employee_name"));
		user.setUsername(rs.getString("pe.username"));
		user.setPassword(rs.getString("pe.password"));
		user.setEmailId(rs.getString("pe.email_id"));
		user.setPhone(rs.getString("pe.phone"));
		user.setAddress(rs.getString("pe.address"));
		user.setUserCode(rs.getString("pe.employee_code"));
		user.setCompany(rs.getString("pe.company_name"));
		user.setDepartment(rs.getString("pe.department"));
		user.setTeam(rs.getString("pe.team"));
		user.setRole(rs.getString("pe.role"));
		user.setStatus(rs.getString("pe.status"));
		user.setAccessRole(rs.getString("xer.role_id"));
		return user;
	}

}
