package com.pasmtAdmin.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserListMapper implements RowMapper<UserBean>{

	@Override
	public UserBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserBean employee = new UserBean();
		employee.setUserId(rs.getString("pe.employee_id"));
		employee.setEmployeeName(rs.getString("pe.employee_name"));
		employee.setUserCode(rs.getString("pe.employee_code"));
		employee.setTeam(rs.getString("pe.team"));
		employee.setStatus(rs.getString("pe.status"));
		employee.setDepartment(rs.getString("department"));
		employee.setRole(rs.getString("role"));
		employee.setAccessRole(rs.getString("access_role"));
		return employee;
	}

}
