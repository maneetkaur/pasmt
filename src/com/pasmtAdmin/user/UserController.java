package com.pasmtAdmin.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class UserController {
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	UserDao userDao;
	
	@RequestMapping(value="/sAdminUserList")
	public ModelAndView employeeList(HttpServletRequest request){
		int startIndex=0;
        if(request.getParameter("pager.offset")!=null)
        {
            startIndex=Integer.parseInt(request.getParameter("pager.offset"));
        }
        ModelAndView employee = new ModelAndView("pasmtAdmin/user/userList");
        //fetch list and add object to model
        List<UserBean> userList = userDao.userList(request, startIndex, 15);
        employee.addObject("userList", userList);
        employee.addObject("searchUser", new UserBean());
        //assign list to null
        userList = null;
        return employee;
	}
	
	@RequestMapping(value="/sAdminUpdateUserStatus",method=RequestMethod.GET)
	public String updateCategoryStatus(HttpServletRequest request)
	{
		userDao.updateStatus(request.getParameter("userId"), 
									Integer.parseInt(request.getParameter("status")));
		return "redirect:/sAdminUserList";
	}

	@RequestMapping(value="/sAdminAddUserForm")
	public ModelAndView addUserForm(){
		ModelAndView addUser = new ModelAndView("pasmtAdmin/user/addUser");
		addUser.addObject("addUser", new UserBean());
		addUser.addObject("departmentList", userDao.getDepartmentList());
		addUser.addObject("roleList", commonFunction.getRoleList());
		addUser.addObject("accessRoleList", userDao.getAccessRoleList());
		return addUser;	
	}
	
	@RequestMapping(value="/sAdminAddUser", method=RequestMethod.POST)
	public String addUser(@ModelAttribute("addUser") UserBean user, ModelMap model){
		userDao.addUser(user);
		return "redirect:/sAdminUserList";
	}
	
	@RequestMapping(value="/sAdminEditUserForm")
	public ModelAndView editUserForm(HttpServletRequest request){
		ModelAndView editUser = new ModelAndView("pasmtAdmin/user/editUser");
		UserBean user = userDao.getUserInformation(request.getParameter("userId"));
		editUser.addObject("editUser", user);
		editUser.addObject("departmentList", userDao.getDepartmentList());
		editUser.addObject("roleList", commonFunction.getRoleList());
		editUser.addObject("accessRoleList", userDao.getAccessRoleList());
		return editUser;	
	}
	
	@RequestMapping(value="/sAdminEditUser", method=RequestMethod.POST)
	public String editUser(@ModelAttribute("editUser") UserBean user, ModelMap model){
		userDao.editUser(user);
		return "redirect:/sAdminUserList";
	}
	
	@RequestMapping(value="/validateUsername", method=RequestMethod.GET)
	public String validateUsername(HttpServletRequest request, ModelMap model){
		int checkName = userDao.validateUsername(request.getParameter("userName"));;
		model.addAttribute("checkName", checkName);
		return "ajaxOperation/ajaxOperationPasmtAdmin";
	}
	
}
