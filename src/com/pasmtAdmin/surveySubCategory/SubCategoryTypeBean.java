package com.pasmtAdmin.surveySubCategory;

import java.util.List;

import com.commonFunc.CommonBean;

public class SubCategoryTypeBean {
	private String subCategoryTypeId, subCategoryType, categoryType, categoryTypeId, subCategorySize, subCategoryIdList,
	language, status;
	
	private List<SubCategoryBean> subCategoryList;
	
	private List<CommonBean> languageList;

	public String getSubCategoryTypeId() {
		return subCategoryTypeId;
	}
	public void setSubCategoryTypeId(String subCategoryTypeId) {
		this.subCategoryTypeId = subCategoryTypeId;
	}
	public String getSubCategoryType() {
		return subCategoryType;
	}
	public void setSubCategoryType(String subCategoryType) {
		this.subCategoryType = subCategoryType;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getCategoryTypeId() {
		return categoryTypeId;
	}
	public void setCategoryTypeId(String categoryTypeId) {
		this.categoryTypeId = categoryTypeId;
	}
	public String getSubCategorySize() {
		return subCategorySize;
	}
	public void setSubCategorySize(String subCategorySize) {
		this.subCategorySize = subCategorySize;
	}
	public String getSubCategoryIdList() {
		return subCategoryIdList;
	}
	public void setSubCategoryIdList(String subCategoryIdList) {
		this.subCategoryIdList = subCategoryIdList;
	}
	public List<SubCategoryBean> getSubCategoryList() {
		return subCategoryList;
	}
	public void setSubCategoryList(List<SubCategoryBean> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
	public List<CommonBean> getLanguageList() {
		return languageList;
	}
	public void setLanguageList(List<CommonBean> languageList) {
		this.languageList = languageList;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
