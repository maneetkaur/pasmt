package com.pasmtAdmin.surveySubCategory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface SurveySubCategoryDao {
	public List<SubCategoryTypeBean> subCategoryList(HttpServletRequest request, int startIndex, int range);
	
	public SubCategoryTypeBean getSurveyCategoryInfo(String categoryTypeId);
	
	public List<SubCategoryBean> removeEmptyRows(List<SubCategoryBean> subCategoryList, int startIndex);
	
	public void addSurveySubCategory(SubCategoryTypeBean subCategoryType);
	
	public int validateSubCategoryName(String subCategoryName);
	
	public SubCategoryTypeBean getSSubCategoryInfo(String subCategoryTypeId);
	
	public void editSurveySubCategory(SubCategoryTypeBean subCategoryType);
	
	public void updateStatus(String categoryTypeId, int status);

}
