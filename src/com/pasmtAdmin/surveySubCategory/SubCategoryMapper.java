package com.pasmtAdmin.surveySubCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SubCategoryMapper implements RowMapper<SubCategoryTypeBean>{

	@Override
	public SubCategoryTypeBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		SubCategoryTypeBean subCategoryType = new SubCategoryTypeBean();
		subCategoryType.setSubCategoryTypeId(rs.getString("survey_sub_category_type_id"));
		subCategoryType.setSubCategoryType(rs.getString("survey_sub_category_type"));
		return subCategoryType;
	}

}
