package com.pasmtAdmin.surveySubCategory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.pasmtAdmin.surveyCategory.LanguageMapper;

@Repository
@Transactional
public class SurveySubCategoryDaoImpl implements SurveySubCategoryDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public SubCategoryTypeBean getSurveyCategoryInfo(String categoryTypeId) {
		String sql = "SELECT survey_category_type_id, survey_category_type FROM lu_survey_category_type" +
				" WHERE survey_category_type_id=" + categoryTypeId;
		SubCategoryTypeBean subCategoryType= jdbcTemplate.queryForObject(sql, new RowMapper<SubCategoryTypeBean>(){

			@Override
			public SubCategoryTypeBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				SubCategoryTypeBean subCategoryTypeTemp = new SubCategoryTypeBean();
				subCategoryTypeTemp.setCategoryTypeId(rs.getString("survey_category_type_id"));
				subCategoryTypeTemp.setCategoryType(rs.getString("survey_category_type"));
				return subCategoryTypeTemp;
			}
		});
		
		sql = "SELECT lang_id, language FROM lu_language WHERE lang_id IN " +
				"(SELECT language_id FROM lu_survey_category WHERE survey_category_type_id=" + categoryTypeId + ")" +
				" ORDER BY language";
		subCategoryType.setLanguageList(jdbcTemplate.query(sql, new LanguageMapper()));
		return subCategoryType;
	}

	@Override
	public List<SubCategoryBean> removeEmptyRows(
			List<SubCategoryBean> subCategoryList, int startIndex) {
		
		List<SubCategoryBean> tempList = new ArrayList<SubCategoryBean>();
		SubCategoryBean subCategory;
		
		//add the existing rows
		for(int i=0; i<startIndex; i++){
			subCategory = subCategoryList.get(i);
			tempList.add(subCategory);
		}
		
		//for newly added rows, check if the row is not empty, then add it to templist
		for(int i=startIndex; i<subCategoryList.size(); i++){
			subCategory = subCategoryList.get(i);
			if(subCategory.getSubCategoryName()!=null && !(subCategory.getSubCategoryName().equals(""))){
				tempList.add(subCategory);
			}
		}
		return tempList;
	}

	@Override
	public void addSurveySubCategory(SubCategoryTypeBean subCategoryType) {
		try{
			StringBuffer sb;
			//insert the sub-category type in database
			String sql = "INSERT INTO lu_survey_sub_category_type (survey_category_type_id, survey_sub_category_type," +
					" created_on) VALUES (?,?,?)";
			jdbcTemplate.update(sql, new Object[]{subCategoryType.getCategoryTypeId(), subCategoryType.getSubCategoryType(),
					new Date()});
			
			//get the sub category type id of latest entry from the database
			sql="SELECT MAX(survey_sub_category_type_id) FROM lu_survey_sub_category_type WHERE survey_sub_category_type=?";
			String subCategoryTypeId = jdbcTemplate.queryForObject(sql, new Object[]{subCategoryType.getSubCategoryType()},
					String.class);
			
			//insert the sub category translations in the database
			if(subCategoryType.getSubCategoryList()!=null && subCategoryType.getSubCategoryList().size()>0){
				sb = new StringBuffer("INSERT INTO lu_survey_sub_category (survey_sub_category_type_id, language_id, " +
						"survey_sub_category_name, created_on) VALUES ");
				for(SubCategoryBean subCategory : subCategoryType.getSubCategoryList()){
					sb = sb.append("(" + subCategoryTypeId + "," + subCategory.getLanguage() + ", '" + 
							subCategory.getSubCategoryName() + "',now()), ");
				}
				sql = sb.toString();
				sb=null;
				if(sql.charAt(sql.length()-2)==','){
					sql = sql.substring(0, sql.length()-2);
				}
				jdbcTemplate.update(sql);
			}
		}
		catch(Exception e){
			System.out.println("Exception in addSurveySubCategory of SurveySubCategoryDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public int validateSubCategoryName(String subCategoryName) {
		int target = -1;
		
		String sql="SELECT count(1) FROM lu_survey_sub_category_type WHERE survey_sub_category_type='" + 
					subCategoryName + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateSubCategoryName of SurveySubCategoryDaoImpl.java");
		}
		return target;
	}

	@Override
	public SubCategoryTypeBean getSSubCategoryInfo(String subCategoryTypeId) {
		//get category and sub category info
		String sql = "SELECT ssc.survey_sub_category_type_id, ssc.survey_category_type_id, sc.survey_category_type," +
				" ssc.survey_sub_category_type FROM lu_survey_sub_category_type ssc JOIN lu_survey_category_type sc" +
				" ON ssc.survey_category_type_id=sc.survey_category_type_id WHERE ssc.survey_sub_category_type_id=?";
		
		SubCategoryTypeBean subCategoryType = jdbcTemplate.queryForObject(sql, new Object[]{subCategoryTypeId},
				new RowMapper<SubCategoryTypeBean>(){

					@Override
					public SubCategoryTypeBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						SubCategoryTypeBean subCategoryTypeTemp = new SubCategoryTypeBean();
						subCategoryTypeTemp.setSubCategoryTypeId(rs.getString("ssc.survey_sub_category_type_id"));
						subCategoryTypeTemp.setCategoryType(rs.getString("sc.survey_category_type"));
						subCategoryTypeTemp.setCategoryTypeId(rs.getString("ssc.survey_category_type_id"));
						subCategoryTypeTemp.setSubCategoryType(rs.getString("ssc.survey_sub_category_type"));
						return subCategoryTypeTemp;
					}
		});
		
		//Get existing translations from the database
		sql = "SELECT ssc.survey_sub_category_id, l.language, ssc.survey_sub_category_name FROM " +
				"lu_survey_sub_category ssc JOIN lu_language l ON l.lang_id=ssc.language_id " +
				"WHERE ssc.survey_sub_category_type_id=? ORDER BY l.language";
		List<SubCategoryBean> subCategoryList = jdbcTemplate.query(sql, new Object[]{subCategoryTypeId},
				new RowMapper<SubCategoryBean>(){

					@Override
					public SubCategoryBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						SubCategoryBean subCategory = new SubCategoryBean();
						subCategory.setSubCategoryId(rs.getString("ssc.survey_sub_category_id"));
						subCategory.setLanguage(rs.getString("l.language"));
						subCategory.setSubCategoryName(rs.getString("ssc.survey_sub_category_name"));
						return subCategory;
					}
				});
		subCategoryType.setSubCategoryList(subCategoryList);
		subCategoryType.setSubCategorySize(String.valueOf(subCategoryList.size()));
		
		//Get the list of languages for which translation exists in category but not in subcategory
		sql = "SELECT lang_id, language FROM lu_language l JOIN  lu_survey_category lsc FORCE INDEX " +
				"(idx_language_id) ON l.lang_id = lsc.language_id AND lsc.survey_category_type_id =" + 
				subCategoryType.getCategoryTypeId() + " LEFT JOIN lu_survey_sub_category lssc ON l.lang_id = " +
				"lssc.language_id AND survey_sub_category_type_id=" + subCategoryTypeId + 
				" WHERE lssc.language_id IS NULL ORDER BY language;";
		subCategoryType.setLanguageList(jdbcTemplate.query(sql, new LanguageMapper()));
		
		return subCategoryType;
	}

	@Override
	public void editSurveySubCategory(SubCategoryTypeBean subCategoryType) {
		try{
			StringBuffer sb;
			String sql = "UPDATE lu_survey_sub_category_type SET survey_sub_category_type=? WHERE " +
					"survey_sub_category_type_id =?";
			jdbcTemplate.update(sql, new Object[]{subCategoryType.getSubCategoryType(), 
					subCategoryType.getSubCategoryTypeId()});
			
			if(subCategoryType.getSubCategoryList()!=null){
				int currentSize = Integer.parseInt(subCategoryType.getSubCategorySize());
				List<SubCategoryBean> subCategoryList = subCategoryType.getSubCategoryList();
				SubCategoryBean newSubCategory;
				
				//Update the existing sub category translation if any
				if(subCategoryType.getSubCategoryIdList().length()>0){
					String subCategoryArray[] = subCategoryType.getSubCategoryIdList().split(",");
					if(currentSize>0 && subCategoryArray.length>0){
						for(int i=0; i<subCategoryArray.length; i++){
							newSubCategory = subCategoryList.get(Integer.parseInt(subCategoryArray[i]));
							sql = "UPDATE lu_survey_sub_category SET survey_sub_category_name='" + 
									newSubCategory.getSubCategoryName() + "' WHERE survey_sub_category_id=" + 
									newSubCategory.getSubCategoryId();
							jdbcTemplate.update(sql);
						}
					}
				}
				
				//Insert the new translations
				if(subCategoryList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO lu_survey_sub_category (survey_sub_category_name, " +
							"survey_sub_category_type_id, language_id, created_on) VALUES ");
					for(int i = currentSize; i<subCategoryList.size(); i++){
						newSubCategory = subCategoryList.get(i);
						sb = sb.append("('" + newSubCategory.getSubCategoryName() + "'," + 
							subCategoryType.getSubCategoryTypeId() + "," + newSubCategory.getLanguage() + ", now()), ");
					}
					sql = sb.toString();
					System.out.println("sql: " + sql);
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}			
					jdbcTemplate.update(sql);
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in editSurveySubCategory of SurveySubCategoryDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public List<SubCategoryTypeBean> subCategoryList(
			HttpServletRequest request, int startIndex, int range) {
		
		boolean whereCondition = false;
		String sql = "SELECT ssct.survey_sub_category_type_id, ssct.survey_sub_category_type, ssct.status FROM " +
				"lu_survey_sub_category_type ssct ";
		//calculate count for pagination
		String sql_count = "SELECT count(1) FROM lu_survey_sub_category_type ssct ";
		
		StringBuffer whereClause = new StringBuffer("WHERE ssct.survey_category_type_id =" + 
				request.getParameter("categoryTypeId") + " AND ");
		if(request.getParameter("subCategoryType") != null && !(request.getParameter("subCategoryType").equals(""))){
			whereCondition = true;
			whereClause.append("ssct.survey_sub_category_type LIKE '%" + request.getParameter("subCategoryType") + 
					"%' AND ");
		}
		if(request.getParameter("language") != null && !(request.getParameter("language").equals("0"))){
			whereCondition = true;
			sql = "SELECT DISTINCT ssct.survey_sub_category_type_id, ssct.survey_sub_category_type, ssct.status" +
					" FROM lu_survey_sub_category_type ssct JOIN lu_survey_sub_category ssc ON " +
					"ssct.survey_sub_category_type_id = ssc.survey_sub_category_type_id ";
			sql_count = "SELECT count(DISTINCT ssct.survey_sub_category_type) FROM lu_survey_sub_category_type ssct" +
					" JOIN lu_survey_sub_category ssc ON ssct.survey_sub_category_type_id = " +
					"ssc.survey_sub_category_type_id ";
			whereClause.append("ssc.language_id=" + request.getParameter("language") + " AND ");
		}
		if(request.getParameter("status")!=null){
			String status = request.getParameter("status");
			if(status.equals("NONE")){
				/*if some other search criteria has been selected then status until selected should not 
				 * be there in where clause*/
				if(!whereCondition){
					whereClause.append("ssct.status=1 AND ");
				}
			}
			else if(status.equals("1")){
				whereClause.append("ssct.status=1 AND ");
			}
			else if (status.equals("0")) {
				whereClause.append("ssct.status=0 AND ");
			}
		}
		//if the request parameter is null, then listing is called for the first time and status should be active by default
		else{
			whereClause.append("ssct.status=1 AND ");
		}
		
		String where=whereClause.toString().trim();
		whereClause=null;
		//remove the extra 'and' in the where clause
		where=where.substring(0, where.length()-4);
		
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);
		request.setAttribute("total_rows", total_rows);
		
		String limit = " ORDER BY ssct.survey_sub_category_type_id LIMIT " + startIndex + ", " + range;
		sql = sql + where + limit;
		
		List<SubCategoryTypeBean> subCategoryTypeList = jdbcTemplate.query(sql, new RowMapper<SubCategoryTypeBean>(){

			@Override
			public SubCategoryTypeBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				SubCategoryTypeBean subCategoryType = new SubCategoryTypeBean();
				subCategoryType.setSubCategoryTypeId(rs.getString("ssct.survey_sub_category_type_id"));
				subCategoryType.setSubCategoryType(rs.getString("ssct.survey_sub_category_type"));
				subCategoryType.setStatus(rs.getString("ssct.status"));
				return subCategoryType;
			}
		});

		return subCategoryTypeList;
	}
	
	@Override
	public void updateStatus(String subCategoryTypeId, int status) {
		try{
			if(status==1){
				status=0;
			}
			else{
				status=1;
			}
			String sql = "UPDATE lu_survey_sub_category_type SET status=? WHERE survey_sub_category_type_id=?";
			jdbcTemplate.update(sql, new Object[]{status, subCategoryTypeId});
		}
		catch(Exception e){
			System.out.println("Exception in updateStatus of SurveySubCategoryDaoImpl.java: " + e);
		}
	}

}
