package com.pasmtAdmin.surveySubCategory;

public class SubCategoryBean {
	private String subCategoryId, subCategoryName, subCategoryTypeId, language;

	public String getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getSubCategoryTypeId() {
		return subCategoryTypeId;
	}
	public void setSubCategoryTypeId(String subCategoryTypeId) {
		this.subCategoryTypeId = subCategoryTypeId;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
}
