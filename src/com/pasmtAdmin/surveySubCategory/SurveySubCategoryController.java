package com.pasmtAdmin.surveySubCategory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class SurveySubCategoryController {
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	SurveySubCategoryDao subCategoryDao;
	
	@RequestMapping(value="/surveySubCategoryList")
	public ModelAndView surveySubCategoryList(HttpServletRequest request){
		int startIndex=0;
        if(request.getParameter("pager.offset")!=null)
        {
            startIndex=Integer.parseInt(request.getParameter("pager.offset"));
        }
        ModelAndView surveyCategory = new ModelAndView("pasmtAdmin/surveySubCategory/surveySubCategoryList");
        //fetch list and add object to model
        List<SubCategoryTypeBean> subCategoryTypeList = subCategoryDao.subCategoryList(request, startIndex, 15);
        surveyCategory.addObject("subCategoryTypeList", subCategoryTypeList);
        SubCategoryTypeBean searchSSubCategory = new SubCategoryTypeBean();
        searchSSubCategory.setCategoryTypeId(request.getParameter("categoryTypeId"));
        surveyCategory.addObject("searchSSubCategory", searchSSubCategory);
        surveyCategory.addObject("languageList", commonFunction.getlanguageList());
        //assign list to null
        subCategoryTypeList = null;
        return surveyCategory;
	}
	
	@RequestMapping(value="/addSSubCatForm")
	public ModelAndView addSSubCatForm(HttpServletRequest request){	
		ModelAndView addSurveySubCategory = new ModelAndView("pasmtAdmin/surveySubCategory/addSubCategory");
		SubCategoryTypeBean subCategoryType = 
				subCategoryDao.getSurveyCategoryInfo(request.getParameter("categoryTypeId"));
		addSurveySubCategory.addObject("addSurveySubCategory", subCategoryType);
		subCategoryType=null;
		return addSurveySubCategory;
	}
	
	@RequestMapping(value="/addSurveySubCategory", method=RequestMethod.POST)
	public String addSurveySubCategory(@ModelAttribute("addSurveySubCategory") SubCategoryTypeBean subCategoryType,
			ModelMap model) {
		if(subCategoryType.getSubCategoryList()!=null){
			subCategoryType.setSubCategoryList(subCategoryDao.removeEmptyRows(subCategoryType.getSubCategoryList(),0));
		}
		subCategoryDao.addSurveySubCategory(subCategoryType);
		return "redirect:/surveySubCategoryList?categoryTypeId=" + subCategoryType.getCategoryTypeId();
	}
	
	@RequestMapping(value="/validateSSubCatName", method=RequestMethod.GET)
	public String validateSCategoryName(HttpServletRequest request, ModelMap model){
		int checkName = subCategoryDao.validateSubCategoryName(request.getParameter("subCategory"));
		model.addAttribute("checkName", checkName);
		return "ajaxOperation/ajaxOperationPasmtAdmin";
	}
	
	@RequestMapping(value="editSSubCatForm", method=RequestMethod.GET)
	public ModelAndView editSSubCatForm(HttpServletRequest request) {
		ModelAndView editSSubCategory = new ModelAndView("pasmtAdmin/surveySubCategory/editSubCategory");
		SubCategoryTypeBean subCategoryType = subCategoryDao.getSSubCategoryInfo(request.getParameter("subCategoryId"));
		editSSubCategory.addObject("editSurveySubCategory", subCategoryType);
		return editSSubCategory;
	}
	
	@RequestMapping(value="/editSurveySubCategory", method=RequestMethod.POST)
	public String editSurveySubCategory(@ModelAttribute("editSurveySubCategory") SubCategoryTypeBean subCategoryType,
			ModelMap model) {
		if(subCategoryType.getSubCategoryList()!=null){
			subCategoryType.setSubCategoryList(subCategoryDao.removeEmptyRows(subCategoryType.getSubCategoryList(),
					Integer.parseInt(subCategoryType.getSubCategorySize())));
		}
		subCategoryDao.editSurveySubCategory(subCategoryType);
		return "redirect:/surveySubCategoryList?categoryTypeId="+subCategoryType.getCategoryTypeId();
	}
	
	//updateSSubCategoryStatus?subCategoryId=${ll.subCategoryTypeId}&status=${ll.status}
	@RequestMapping(value="/updateSSubCategoryStatus",method=RequestMethod.GET)
	public String updateCategoryStatus(HttpServletRequest request)
	{
		subCategoryDao.updateStatus(request.getParameter("subCategoryId"), 
									Integer.parseInt(request.getParameter("status")));
		return "redirect:/surveySubCategoryList?categoryTypeId="+ request.getParameter("categoryTypeId");
	}

}
