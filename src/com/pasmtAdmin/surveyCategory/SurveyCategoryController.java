package com.pasmtAdmin.surveyCategory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class SurveyCategoryController {
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	SurveyCategoryDao categoryDao;
	
	@RequestMapping(value="/surveyCategoryList")
	public ModelAndView surveyCategoryList(HttpServletRequest request){
		int startIndex=0;
        if(request.getParameter("pager.offset")!=null)
        {
            startIndex=Integer.parseInt(request.getParameter("pager.offset"));
        }
        ModelAndView surveyCategory = new ModelAndView("pasmtAdmin/surveyCategory/surveyCategoryList");
        //fetch list and add object to model
        List<CategoryTypeBean> categoryTypeList = categoryDao.categoryList(request, startIndex, 15);
        surveyCategory.addObject("categoryTypeList", categoryTypeList);
        surveyCategory.addObject("searchSCategory", new CategoryTypeBean());
        surveyCategory.addObject("languageList", commonFunction.getlanguageList());
        //assign list to null
        categoryTypeList = null;
        return surveyCategory;
	}
	
	@RequestMapping(value="/addSCategoryForm")
	public ModelAndView addSurveyCategoryForm(){
		ModelAndView addSurveyCategory = new ModelAndView("pasmtAdmin/surveyCategory/addSurveyCategory");
		addSurveyCategory.addObject("addSurveyCategory", new CategoryTypeBean());
		addSurveyCategory.addObject("languageList", commonFunction.getlanguageList());
		return addSurveyCategory;
	}
	
	@RequestMapping(value="/addSurveyCategory", method=RequestMethod.POST)
	public String addSurveyCategory(@ModelAttribute("addSurveyCategory") CategoryTypeBean categoryType, 
			ModelMap model){
		if(categoryType.getCategoryList()!=null){
			categoryType.setCategoryList(categoryDao.removeEmptyRows(categoryType.getCategoryList(),0));
		}
		categoryDao.addSurveyCategory(categoryType);
		return "redirect:/surveyCategoryList";
	}
	
	@RequestMapping(value="/validateSCategoryName", method=RequestMethod.GET)
	public String validateSCategoryName(HttpServletRequest request, ModelMap model){
		int checkName = categoryDao.validateSCategoryName(request.getParameter("category"));
		model.addAttribute("checkName", checkName);
		return "ajaxOperation/ajaxOperationPasmtAdmin";
	}
	
	@RequestMapping(value="/editSCategoryForm", method=RequestMethod.GET)
	public ModelAndView editSCategoryForm(HttpServletRequest request){
		ModelAndView editSCategory = new ModelAndView("pasmtAdmin/surveyCategory/editSurveyCategory");
		CategoryTypeBean categoryType = categoryDao.getSCategoryInformation(request.getParameter("categoryId"));
		editSCategory.addObject("editSurveyCategory", categoryType);
		editSCategory.addObject("languageList", categoryDao.languageList(categoryType.getCategoryTypeId()));
		return editSCategory;
	}
	
	@RequestMapping(value="/editSurveyCategory", method=RequestMethod.POST)
	public String editSurveyCategory(@ModelAttribute("editSurveyCategory") CategoryTypeBean categoryType,
		ModelMap model) {
			if(categoryType.getCategoryList()!=null){
				categoryType.setCategoryList(categoryDao.removeEmptyRows(categoryType.getCategoryList(),
						Integer.parseInt(categoryType.getCategorySize())));
			}
			categoryDao.editSurveyCategory(categoryType);
			return "redirect:/surveyCategoryList";
	}
	
	@RequestMapping(value="/updateSurveyCategoryStatus",method=RequestMethod.GET)
	public String updateCategoryStatus(HttpServletRequest request)
	{
		categoryDao.updateStatus(request.getParameter("categoryTypeId"), 
									Integer.parseInt(request.getParameter("status")));
		return "redirect:/surveyCategoryList";
	}

}
