package com.pasmtAdmin.surveyCategory;

import java.util.List;

import com.pasmtAdmin.surveySubCategory.SubCategoryTypeBean;

public class CategoryTypeBean {
	private String categoryTypeId, categoryType, languageId, categorySize, categoryIdList, status;
	
	private List<CategoryBean> categoryList;
	
	private List<SubCategoryTypeBean> subCategoryList;

	public String getCategoryTypeId() {
		return categoryTypeId;
	}
	public void setCategoryTypeId(String categoryTypeId) {
		this.categoryTypeId = categoryTypeId;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getLanguageId() {
		return languageId;
	}
	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}
	public List<CategoryBean> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<CategoryBean> categoryList) {
		this.categoryList = categoryList;
	}
	public String getCategorySize() {
		return categorySize;
	}
	public void setCategorySize(String categorySize) {
		this.categorySize = categorySize;
	}
	public String getCategoryIdList() {
		return categoryIdList;
	}
	public void setCategoryIdList(String categoryIdList) {
		this.categoryIdList = categoryIdList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<SubCategoryTypeBean> getSubCategoryList() {
		return subCategoryList;
	}
	public void setSubCategoryList(List<SubCategoryTypeBean> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}
}
