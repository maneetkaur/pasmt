package com.pasmtAdmin.surveyCategory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;

public interface SurveyCategoryDao {
	public List<CategoryTypeBean> categoryList(HttpServletRequest request, int startIndex, int range);
	
	public void addSurveyCategory(CategoryTypeBean categoryType);
	
	public List<CategoryBean> removeEmptyRows(List<CategoryBean> categoryList, int startIndex);
	
	public int validateSCategoryName(String sCategoryName);
	
	public CategoryTypeBean getSCategoryInformation(String categoryTypeId);
	
	public void editSurveyCategory(CategoryTypeBean categoryType);
	
	public List<CommonBean> languageList(String categoryTypeId);
	
	public void updateStatus(String subCategoryTypeId, int status);
}
