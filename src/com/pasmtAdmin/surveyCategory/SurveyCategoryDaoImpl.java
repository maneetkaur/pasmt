package com.pasmtAdmin.surveyCategory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.commonFunc.CommonBean;

@Repository
@Transactional
public class SurveyCategoryDaoImpl implements SurveyCategoryDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void addSurveyCategory(CategoryTypeBean categoryType) {
		try{
			StringBuffer sb;
			//insert the category type in database
			String sql = "INSERT INTO lu_survey_category_type (survey_category_type, created_on) VALUES (?, ?)";
			jdbcTemplate.update(sql, new Object[]{categoryType.getCategoryType(), new Date()});
			
			//get the category type id of latest entry from database
			sql = "SELECT MAX(survey_category_type_id) FROM lu_survey_category_type WHERE survey_category_type=?";
			String categoryTypeId = (String)jdbcTemplate.queryForObject(sql, new Object[]{categoryType.getCategoryType()},
					String.class);
			
			//insert the category translations in database
			if(categoryType.getCategoryList()!=null && categoryType.getCategoryList().size()>0){
				sb = new StringBuffer("INSERT INTO lu_survey_category (survey_category_type_id, language_id, " +
						"survey_category_name, created_on) VALUES ");
				for(CategoryBean category: categoryType.getCategoryList()){
					sb = sb.append("(" + categoryTypeId + "," + category.getLanguage() + ",'" + category.getCategoryName()
							+ "', now()), ");
				}
				sql = sb.toString();
				sb=null;
				if(sql.charAt(sql.length()-2)==','){
					sql = sql.substring(0, sql.length()-2);
				}
				System.out.println("sql: " + sql);
				jdbcTemplate.update(sql);
			}
		}
		catch(Exception e){
			System.out.println("Exception in addSurveyCategory of SurveyCategoryDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public List<CategoryBean> removeEmptyRows(List<CategoryBean> categoryList, int startIndex){
		List<CategoryBean> tempList = new ArrayList<CategoryBean>();
		CategoryBean category;
		
		//add the existing rows
		for(int i=0; i<startIndex; i++){
			category = categoryList.get(i);
			tempList.add(category);
		}
		//for newly added rows check if the row is not empty, then add it to templist
		for(int i=startIndex; i<categoryList.size(); i++){
			category = categoryList.get(i);
			if(category.getCategoryName()!=null && !(category.getCategoryName().equals(""))){
				tempList.add(category);
			}
		}
		return tempList;
	}

	@Override
	public int validateSCategoryName(String sCategoryName) {
		int target = -1;
		
		String sql="SELECT count(1) FROM lu_survey_category_type WHERE survey_category_type='" + sCategoryName + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateSCategoryName of SurveyCategoryDaoImpl.java");
		}
		return target;
	}

	@Override
	public CategoryTypeBean getSCategoryInformation(String categoryTypeId) {
		String sql="SELECT survey_category_type_id, survey_category_type FROM lu_survey_category_type WHERE " +
				"survey_category_type_id=?";
		CategoryTypeBean categoryType= jdbcTemplate.queryForObject(sql, new Object[]{categoryTypeId}, 
				new RowMapper<CategoryTypeBean>(){

					@Override
					public CategoryTypeBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						CategoryTypeBean categoryType = new CategoryTypeBean();
						categoryType.setCategoryTypeId(rs.getString("survey_category_type_id"));
						categoryType.setCategoryType(rs.getString("survey_category_type"));
						return categoryType;
					}
				});
		
		sql = "SELECT sc.survey_category_id, l.language, sc.survey_category_name FROM lu_survey_category sc " +
				"JOIN lu_language l ON l.lang_id=sc.language_id WHERE sc.survey_category_type_id=? ORDER BY l.language";
		List<CategoryBean> categoryList = jdbcTemplate.query(sql, new Object[]{categoryTypeId}, 
				new RowMapper<CategoryBean>(){

					@Override
					public CategoryBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						CategoryBean category = new CategoryBean();
						category.setCategoryId(rs.getString("sc.survey_category_id"));
						category.setCategoryName(rs.getString("sc.survey_category_name"));
						category.setLanguage(rs.getString("l.language"));
						return category;
					}
				});
		categoryType.setCategoryList(categoryList);
		categoryType.setCategorySize(String.valueOf(categoryList.size()));
		
		return categoryType;
	}

	@Override
	public void editSurveyCategory(CategoryTypeBean categoryType) {
		try{
			StringBuffer sb;
			String sql="UPDATE lu_survey_category_type SET survey_category_type=? WHERE survey_category_type_id=?";
			jdbcTemplate.update(sql, new Object[]{categoryType.getCategoryType(), categoryType.getCategoryTypeId()});
			
			if(categoryType.getCategoryList()!=null){
				int currentSize = Integer.parseInt(categoryType.getCategorySize());
				List<CategoryBean> categoryList = categoryType.getCategoryList();
				CategoryBean newCategory;
				
				//Update the existing category translation if any
				if(categoryType.getCategoryIdList().length()>0){
					String categoryArray[] = categoryType.getCategoryIdList().split(",");
					if(currentSize>0 && categoryArray.length>0){
						for(int i=0; i<categoryArray.length; i++){
							newCategory = categoryList.get(Integer.parseInt(categoryArray[i]));
							sql = "UPDATE lu_survey_category SET survey_category_name='"+ newCategory.getCategoryName() 
									+"' WHERE survey_category_id=" + newCategory.getCategoryId();
							jdbcTemplate.update(sql);
						}
					}
				}
				
				//Insert the new translations
				if(categoryList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO lu_survey_category (survey_category_name, survey_category_type_id" +
							", language_id, created_on) VALUES ");
					for(int i=currentSize; i<categoryList.size(); i++){
						CategoryBean category = categoryList.get(i);
						sb = sb.append("('" + category.getCategoryName() + "'," + categoryType.getCategoryTypeId() + "," +
								category.getLanguage() + ",now()), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}			
					jdbcTemplate.update(sql);
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in editSurveyCategory of SurveyCategoryDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public List<CommonBean> languageList(String categoryTypeId) {
		String sql = "SELECT lang_id, language FROM lu_language l LEFT JOIN lu_survey_category lsc ON l.lang_id =" +
				" lsc.language_id AND survey_category_type_id =" + categoryTypeId + " WHERE lsc.language_id IS NULL" +
				" ORDER BY language";
		List<CommonBean> languageList = jdbcTemplate.query(sql, new LanguageMapper());
		return languageList;
	}

	@Override
	public List<CategoryTypeBean> categoryList(HttpServletRequest request,
			int startIndex, int range) {
		boolean whereCondition = false;
		String sql ="SELECT sct.survey_category_type_id, sct.survey_category_type, sct.status FROM " +
				"lu_survey_category_type sct ";
		//calculate count for pagination
		String sql_count = "SELECT count(1) FROM lu_survey_category_type sct ";
		StringBuffer whereClause = new StringBuffer("WHERE ");
		if(request.getParameter("categoryType") != null && !(request.getParameter("categoryType").equals(""))){
			whereCondition = true;
			whereClause.append("sct.survey_category_type LIKE '%" + request.getParameter("categoryType") + "%' AND ");
		}
		if(request.getParameter("languageId") != null && !(request.getParameter("languageId").equals("0"))){
			sql = "SELECT DISTINCT sct.survey_category_type_id, sct.survey_category_type, sct.status FROM " +
					"lu_survey_category_type sct JOIN lu_survey_category sc ON sct.survey_category_type_id = " +
					"sc.survey_category_type_id ";
			
			//calculate count for pagination
			sql_count = "SELECT count(DISTINCT sct.survey_category_type) FROM lu_survey_category_type" +
					" sct JOIN lu_survey_category sc ON sct.survey_category_type_id=sc.survey_category_type_id ";
			whereCondition = true;
			whereClause.append("sc.language_id=" + request.getParameter("languageId") + " AND ");
		}
		if(request.getParameter("status")!=null){
			String status = request.getParameter("status");
			if(status.equals("NONE")){
				/*if some other search criteria has been selected then status until selected should not 
				 * be there in where clause*/
				if(!whereCondition){
					whereCondition = true;
					whereClause.append("sct.status=1 AND ");
				}
			}
			else if(status.equals("1")){
				whereCondition = true;
				whereClause.append("sct.status=1 AND ");
			}
			else if (status.equals("0")) {
				whereCondition = true;
				whereClause.append("sct.status=0 AND ");
			}
		}
		//if the request parameter is null, then listing is called for the first time and status should be active by default
		else{
			whereCondition = true;
			whereClause.append("sct.status=1 AND ");
		}
		
		String where=whereClause.toString().trim();
		whereClause=null;
		
		//Check if there is any condition in where and if there is remove the extra 'and' in the where clause
		if (!whereCondition) {
			where="";
		}
		else{
			where=where.substring(0, where.length()-4);
		}
		
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);
		request.setAttribute("total_rows", total_rows);
		
		String limit = " ORDER BY sct.survey_category_type_id LIMIT " + startIndex + ", " + range;
		sql = sql + where + limit;
		List<CategoryTypeBean> categoryTypeList = jdbcTemplate.query(sql, new RowMapper<CategoryTypeBean>(){
			
			@Override
			public CategoryTypeBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CategoryTypeBean categoryType = new CategoryTypeBean();
				categoryType.setCategoryTypeId(rs.getString("sct.survey_category_type_id"));
				categoryType.setCategoryType(rs.getString("sct.survey_category_type"));
				categoryType.setStatus(rs.getString("sct.status"));
				return categoryType;
			}
		});
		return categoryTypeList;
	}

	@Override
	public void updateStatus(String categoryTypeId, int status) {
		try{
			if(status==1){
				status=0;
			}
			else{
				status=1;
			}
			String sql = "UPDATE lu_survey_category_type SET status=? WHERE survey_category_type_id=?";
			jdbcTemplate.update(sql, new Object[]{status, categoryTypeId});
		}
		catch(Exception e){
			System.out.println("Exception in updateStatus of SurveyCategoryDaoImpl.java: " + e);			
		}
	}

}
