package com.pasmtAdmin.surveyCategory;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.commonFunc.CommonBean;

public class LanguageMapper implements RowMapper<CommonBean>{

	@Override
	public CommonBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		CommonBean language = new CommonBean();
		language.setId(rs.getString("lang_id"));
		language.setName(rs.getString("language"));
		return language;
	}

}
