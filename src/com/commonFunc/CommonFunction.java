package com.commonFunc;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.admin.category.CategoryBean;
import com.admin.subCategory.SubCategoryBean;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.pasmtAdmin.surveyCategory.CategoryTypeBean;
import com.pasmtAdmin.surveySubCategory.SubCategoryMapper;
import com.pasmtAdmin.surveySubCategory.SubCategoryTypeBean;


@Repository
public class CommonFunction {
	
	private JdbcTemplate jdbcTemplate;
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	/***** to get the list of the countries which are active in panel *********/
	public List<CommonBean> getCountryList()
	{
		List countryList=new ArrayList();
		String sql="select country_id,country_name from lu_country where is_active_pasmt='1'";
		countryList=jdbcTemplate.query(sql, new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString("country_id"));
				bean.setName(rs.getString("country_name"));
				return bean;
			}
		});
		return countryList;
	}
	
	public List<CommonBean> getlanguageList()
	{
		List languageList=new ArrayList();
		String sql="select lang_id,language from lu_language order by language";
		languageList=jdbcTemplate.query(sql,new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber) throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString("lang_id"));
				bean.setName(rs.getString("language"));
				return bean;
			}
		});
		return languageList;
	}
	
	public List<CommonBean> getAllCountryList()
	{
		List allCountryList=new ArrayList();
		String sql="select country_id,country_name from lu_country";
		allCountryList=jdbcTemplate.query(sql, new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString("country_id"));
				bean.setName(rs.getString("country_name"));
				return bean;
			}
		});
		return allCountryList;
	}
	
	public List<CommonBean> getCultureList()
	{
		List<CommonBean> cultureList=new ArrayList<>();
		String sql="select cul.culture_id,lang.lang_code,con.country_code from lu_culture as cul,lu_country as con,lu_language as lang where con.country_id=cul.country_id and lang.lang_id=cul.lang_id";
		cultureList=jdbcTemplate.query(sql,new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean = new CommonBean();
				bean.setId(rs.getString(1));
				String languageCode=rs.getString(2);
				String countryCode=rs.getString(3);
				String cultureCode=languageCode+"-"+countryCode;
				bean.setCode(cultureCode);
				return bean;
			}
		});
		return cultureList;
	}
	
	public List<CommonBean> getCompanyList()
	{
		List<CommonBean> cultureList=new ArrayList<>();
		String sql="select company_id,company_name from lu_company";
		cultureList=jdbcTemplate.query(sql,new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString(1));
				bean.setName(rs.getString(2));
				return bean;
			}
		});
		return cultureList;
	}
	
	/******** to get question type list ******/
	public List<CommonBean> getQuestionTypeList()
	{
		List<CommonBean> questionTypeList=new ArrayList<>();
		String sql="select question_type_id,question_type_header from lu_question_type";
		questionTypeList=jdbcTemplate.query(sql,new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString(1));
				bean.setName(rs.getString(2));
				return bean;
			}
		});
		return questionTypeList;
	}
	
	/**** to get category type list ****/
	public List<CommonBean> getCategoryTypeList()
	{
		List<CommonBean> categoryList=new ArrayList<>();
		String sql="select category_type_id,category_type from lu_category_type";
		categoryList=jdbcTemplate.query(sql,new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString(1));
				bean.setName(rs.getString(2));
				return bean;
			}
		});
		return categoryList;
	}
	
	/**** to get subcategory type list ****/
	public List<CommonBean> getSubCategoryTypeList()
	{
		List<CommonBean> subCategoryList=new ArrayList<>();
		String sql="select sub_category_type_id,sub_category_type from lu_sub_category_type";
		subCategoryList=jdbcTemplate.query(sql,new RowMapper(){
			public CommonBean mapRow(ResultSet rs,int rownumber)throws SQLException{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getString(1));
				bean.setName(rs.getString(2));
				return bean;
			}
		});
		return subCategoryList;
	}

	/*
	 * Code to download file
	 */
	public void downloadFile(ServletContext servletContext, HttpServletResponse response, String fullPath){
		try
		{
			 File downloadFile = new File(fullPath);
		     FileInputStream inputStream = new FileInputStream(downloadFile);
		     String mimeType = servletContext.getMimeType(fullPath);
	         if (mimeType == null)
	         {
	            mimeType = "application/octet-stream";
	         }
	        response.setContentType(mimeType);
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",
	                downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	        OutputStream outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	        inputStream.close();
	        outStream.close();
		}
		catch(Exception e)
		{
			System.out.println("exception : "+e);
		}
	}
	
	//code for filtering the special characters by using back slash
		public static String filterWithBackSlash(String input)
		{

			StringBuffer filtered=new StringBuffer(input.length());
			char c;
			for(int i=0;i<input.length();i++)
			{
				c=input.charAt(i);
				switch(c)
				{
					case '"' : filtered.append("\"");
								break;
					case '\'' : filtered.append("\\'");
								break;
					case '\\' : filtered.append("\\\\");
								break;
					default :  filtered.append(c);
				}
			}
			return(filtered.toString());
		}
		/*
		 * get the list of category and subcategory in Panel Admin
		 */
		public List<CategoryBean> getCategories(String cultureId){
			List<CategoryBean> categoryTypeList;
			String sql,sql1,langId;
			sql1="SELECT lang_id from lu_culture where culture_id='"+cultureId+"'";
			langId=jdbcTemplate.queryForObject(sql1, String.class);
			sql = "SELECT ctype.category_type_id,ctype.category_type FROM lu_category_type ctype,lu_category cat where cat.category_type_id=ctype.category_type_id and cat.language_id='"+langId+"' ";
			categoryTypeList = jdbcTemplate.query(sql, new RowMapper<CategoryBean>(){

				@Override
				public CategoryBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CategoryBean categoryType = new CategoryBean();
					categoryType.setCategoryTypeId(rs.getString("category_type_id"));
					categoryType.setCategoryName(rs.getString("category_type"));
					return categoryType;
				}
			});
			
			List<SubCategoryBean> subCategoryTypeList;
			//get sub categories in each category
			for(CategoryBean categoryType : categoryTypeList){
				sql = "SELECT sub_category_type_id, sub_category_type FROM lu_sub_category_type" +
						" WHERE category_type_id =" + categoryType.getCategoryTypeId();
				subCategoryTypeList = jdbcTemplate.query(sql, new RowMapper<SubCategoryBean>(){

					@Override
					public SubCategoryBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						SubCategoryBean categoryType = new SubCategoryBean();
						categoryType.setSubCategoryId(rs.getString("sub_category_type_id"));
						categoryType.setSubCategory(rs.getString("sub_category_type"));
						return categoryType;
					}
				});
				categoryType.setSubCategoryList(subCategoryTypeList);
			}			
			return categoryTypeList;
		}
		
		/***** get added bank List ********/
	public List<CommonBean> getBankList()
	{
		List<CommonBean> bankList=new ArrayList<CommonBean>();
		String sql="select banking_id,bank_name,account_number from pasmt_banking";
		bankList=jdbcTemplate.query(sql,new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean bank = new CommonBean();
				bank.setId(rs.getString("banking_id"));
				bank.setName(rs.getString("bank_name") + " - " + rs.getString("account_number"));				
				return bank;
			}			
		});
		return bankList;
	}
		
	//Added by Maneet
	public List<CommonBean> getManagerList(){
		List<CommonBean> managerList = new ArrayList<CommonBean>();
		//String sql = "SELECT employee_id, employee_name FROM pasmt_employee WHERE role=10 and department=5";
		String sql = "SELECT employee_id, employee_name FROM pasmt_employee WHERE status=1 AND employee_id!=1";
		managerList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean manager = new CommonBean();
				manager.setId(rs.getString("employee_id"));
				manager.setName(rs.getString("employee_name"));
				return manager;
			}			
		});
		return managerList;
	}
	
	public List<CommonBean> getClientList(){
		List<CommonBean> clientList = new ArrayList<CommonBean>();
		//Display only active clients
		String sql = "SELECT client_id, client_name FROM pasmt_client WHERE status=1";
		clientList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean client = new CommonBean();
				client.setId(rs.getString("client_id"));
				client.setName(rs.getString("client_name"));
				return client;
			}			
		});
		return clientList;
	}
	
	/*
	 * Get the billing currencies from the generic master table
	 */
	public List<CommonBean> getBillingCurrency(){
		List<CommonBean> currencyList = new ArrayList<CommonBean>();
		String sql = "SELECT generic_value_id, generic_value FROM lu_generic_values WHERE generic_key='Bcur' " +
				"ORDER BY generic_value";
		currencyList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean currency = new CommonBean();
				currency.setId(rs.getString("generic_value_id"));
				currency.setName(rs.getString("generic_value"));
				return currency;
			}			
		});
		return currencyList;
	}
	
		public boolean createDirectory(String filePath){
			boolean status=false;
			File files = new File(filePath);
			if (!files.exists()) {
				if (files.mkdirs()) {
					status = true;
				} else {
					System.out.println("Failed to create directories for " + filePath + " in CommonFunction.java!");
				}
			}						
			return status;
		}
		
		/*
		 * Gets the list of categories and the sub categories contained in it
		 */
		public List<CategoryTypeBean> getSurveyCategories(){
			List<CategoryTypeBean> categoryTypeList;
			String sql = "SELECT survey_category_type_id, survey_category_type FROM lu_survey_category_type";
			categoryTypeList = jdbcTemplate.query(sql, new RowMapper<CategoryTypeBean>(){

				@Override
				public CategoryTypeBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CategoryTypeBean categoryType = new CategoryTypeBean();
					categoryType.setCategoryTypeId(rs.getString("survey_category_type_id"));
					categoryType.setCategoryType(rs.getString("survey_category_type"));
					return categoryType;
				}
			});
			
			List<SubCategoryTypeBean> subCategoryTypeList;
			//get sub categories in each category
			for(CategoryTypeBean categoryType : categoryTypeList){
				sql = "SELECT survey_sub_category_type_id, survey_sub_category_type FROM lu_survey_sub_category_type" +
						" WHERE survey_category_type_id =" + categoryType.getCategoryTypeId();
				subCategoryTypeList = jdbcTemplate.query(sql, new SubCategoryMapper());
				categoryType.setSubCategoryList(subCategoryTypeList);
			}			
			return categoryTypeList;
		}
		
		/*
		 * Get the list of roles from generic master
		 */
		
		public List<CommonBean> getRoleList(){
			List<CommonBean> roleList = new ArrayList<CommonBean>();
			String sql = "SELECT generic_value_id, generic_value FROM lu_generic_values WHERE generic_key='Role' " +
					"ORDER BY generic_value";
			
			roleList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

				@Override
				public CommonBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CommonBean role = new CommonBean();
					role.setId(rs.getString("generic_value_id"));
					role.setName(rs.getString("generic_value"));
					return role;
				}
			});
			return roleList;
		}
		
		/*
		 * Get the list of project status from generic master
		 */
		
		public List<CommonBean> getProjectStatusList(){
			List<CommonBean> projectStatusList = new ArrayList<CommonBean>();
			String sql = "SELECT generic_value_id, generic_value FROM lu_generic_values WHERE generic_key='Prjs' " +
					"ORDER BY generic_value";
			
			projectStatusList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

				@Override
				public CommonBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CommonBean projectStatus = new CommonBean();
					projectStatus.setId(rs.getString("generic_value_id"));
					projectStatus.setName(rs.getString("generic_value"));
					return projectStatus;
				}
			});
			return projectStatusList;
		}
		
		/*
		 * Change date format from YYYY-MM-DD to MMM DD, YYYY - By Maneet
		 */
		public String changeDateFormat(String date){
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat formatter2 = new SimpleDateFormat("MMM dd, yyyy");
			String newDate="";
			  try {
			    newDate = formatter2.format(formatter.parse(date));
			  }catch(Exception e){
				  System.out.println("Error in converting date from one format to another: " + e);
			  }
			  return newDate;
		}
		
		/* Author- Maneet Kaur
		 * Create map from list of common bean
		 * Map id to key and name to value
		 * Returns map
		 */
		public Map<String, String> createMapFromList(List<CommonBean> list){
			Map<String, String> valueMap = new HashMap<String, String>();
			for(CommonBean commonBean: list){
				valueMap.put(commonBean.getId(), commonBean.getName());
			}
			return valueMap;
		}
		
		/*
		 * Author - Maneet Kaur
		 * Date - 19Feb'14
		 * Change month from words to number
		 * Returns the number as string
		 */
		
		public String changeMonthToNumber(String monthName) throws ParseException{
			int monthNumber=0;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new SimpleDateFormat("MMM").parse(monthName));
			monthNumber = cal.get(Calendar.MONTH) + 1;
			return String.valueOf(monthNumber);
		}
		
		/*
		 * Author - Maneet Kaur
		 * Date -20 Feb'14
		 * Change month from words to number
		 * Returns the number as string
		 */
		
		public String changeMonthToWords(String monthNumber) throws ParseException{
			String monthName="";
			int mNumber = Integer.parseInt(monthNumber);
			monthName = new DateFormatSymbols().getMonths()[mNumber-1];			
			return monthName;
		}
		
		/*
		 * Author - Maneet Kaur
		 * Date -21 Feb'14
		 * This method returns the list of all active vendors as a list of common bean object
		 */
		public List<CommonBean> getActiveVendorList() {
			String sql = "SELECT vendor_id, vendor_name FROM pasmt_vendor WHERE status=1 AND vendor_status=1";
			List<CommonBean> vendorList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){
				@Override
				public CommonBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CommonBean vendor = new CommonBean();
					vendor.setId(rs.getString("vendor_id"));
					vendor.setName(rs.getString("vendor_name"));
					return vendor;
				}				
			});
			return vendorList;
		}
		
		/*
		 * This method returns the list of all active projects as a list of common bean object
		 */
		public List<CommonBean> getProjectList() {
			try{
				String sql = "SELECT lp.project_id, pp.proposal_number FROM lu_project lp JOIN pasmt_proposal pp " +
						"ON lp.proposal_id=pp.proposal_id WHERE lp.status=1";
				List<CommonBean> projectList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

					@Override
					public CommonBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						CommonBean project = new CommonBean();
						project.setId(rs.getString("lp.project_id"));
						project.setName(rs.getString("pp.proposal_number"));
						return project;
					}				
				});
				return projectList;
			}
			catch(Exception e){
				System.out.println("Exception in getProjectList of ProjectSummaryDaoImpl.java: " + e);
			}
			return null;
		}
		
		/*
		 * Function to change currency into INR format
		 * Author - Maneet Kaur
		 * Date - 12 June'14
		 */
		public static String inrCurrencyformat(double value) {
		    if(value < 1000) {
		        return format("###.00", value);
		    } else {
		        double hundreds = value % 1000;
		        int other = (int) (value / 1000);
		        return format(",##", other) + ',' + format("000.00", hundreds);
		    }
		}
		
		private static String format(String pattern, Object value) {
		    return new DecimalFormat(pattern).format(value);
		}
}
