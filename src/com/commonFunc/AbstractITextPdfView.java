package com.commonFunc;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public abstract class AbstractITextPdfView extends AbstractView{
	public AbstractITextPdfView(){
		setContentType("application/pdf");
	}
	
	@Override
	protected boolean generatesDownloadContent(){
		return true;
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		//IE workaround: write into byte array first
		ByteArrayOutputStream baos = createTemporaryOutputStream();
		
		//Apply preferences and build metadata
		Document document = newDocument();
		PdfWriter writer = newWriter(document, baos);
		prepareWriter(model, writer, request);
		
		//Build PDF document
		document.open();
		buildPdfDocument(model, document, writer, request, response);
		document.close();
		
		//Flush to HTTP response
		writeToResponse(response, baos);
	}
	
	protected Document newDocument(){
		Rectangle pageSize = new Rectangle(PageSize.A4);
		pageSize.setBackgroundColor(new BaseColor(221, 232, 198));
		return new Document(pageSize);
	}
	
	protected PdfWriter newWriter(Document document, OutputStream os) throws DocumentException{
		return PdfWriter.getInstance(document, os);
	}
	
	protected void prepareWriter(Map<String, Object> model, PdfWriter writer, HttpServletRequest request)
		throws DocumentException{
		
		writer.setViewerPreferences(getViewerPreferences());		
	}
	
	protected int getViewerPreferences(){
		//return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
		return PdfWriter.PageModeUseNone;
	}
	
	protected void buildPdfMetadata(Map<String, Object> model, Document document, HttpServletRequest request){		
	}
	
	protected abstract void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter pdfWriter,
			HttpServletRequest request, HttpServletResponse response) throws Exception;
}
