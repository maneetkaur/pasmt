package com.commonFunc;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class FileBean {
	CommonsMultipartFile file;

	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}
	
}
