package com.commonFunc;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public abstract class AbstractITextPdfViewBgWhite extends AbstractView{
	private static final String tagline = "Our Sample, Your Research";
	private static Font headerWhiteFont;
	private static BaseColor greenColor = new BaseColor(23, 160, 134);
	
	public AbstractITextPdfViewBgWhite(){
		setContentType("application/pdf");		
	}
	
	@Override
	protected boolean generatesDownloadContent(){
		return true;
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		//IE workaround: write into byte array first
		ByteArrayOutputStream baos = createTemporaryOutputStream();
		
		//Apply preferences and build metadata
		Document document = newDocument();
		PdfWriter writer = newWriter(document, baos);
		prepareWriter(model, writer, request);
		
		//Register fonts
		String path = request.getServletContext().getRealPath("");
		FontFactory.registerDirectory(path + "/resources/fonts");
		headerWhiteFont = FontFactory.getFont("Calibri", 11.0f, BaseColor.WHITE);
		
		//Set header and footer
		HeaderFooter event = new HeaderFooter();
		writer.setPageEvent(event);
		
		//Build PDF document
		document.open();
		buildPdfDocument(model, document, writer, request, response);
		document.close();
		
		//Flush to HTTP response
		writeToResponse(response, baos);
	}
	
	protected Document newDocument(){
		Rectangle pageSize = new Rectangle(PageSize.A4.rotate());
		return new Document(pageSize);
	}
	
	protected PdfWriter newWriter(Document document, OutputStream os) throws DocumentException{
		return PdfWriter.getInstance(document, os);
	}
	
	protected void prepareWriter(Map<String, Object> model, PdfWriter writer, HttpServletRequest request)
		throws DocumentException{
		
		writer.setViewerPreferences(getViewerPreferences());		
	}
	
	protected int getViewerPreferences(){
		//return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
		return PdfWriter.PageModeUseNone;
	}
	
	protected void buildPdfMetadata(Map<String, Object> model, Document document, HttpServletRequest request){		
	}
	
	protected abstract void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter pdfWriter,
			HttpServletRequest request, HttpServletResponse response) throws Exception;
	
	//Code for header and footer
	class HeaderFooter extends PdfPageEventHelper{	 
        /**
         * Adds the header and the footer.
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onEndPage(PdfWriter writer, Document document) {
            Rectangle page = document.getPageSize();
            
            //For header
            //Print the header with tag line
            PdfPTable headerTable = new PdfPTable(1);
            PdfPCell headerCell = new PdfPCell();
    		headerCell.setBorder(Rectangle.NO_BORDER);
    		headerCell.setBackgroundColor(greenColor);
    		headerCell.setPaddingBottom(5.0f);
    		headerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    		headerCell.setPhrase(new Phrase(tagline, headerWhiteFont));
    		headerTable.addCell(headerCell);

    		headerTable.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
    		headerTable.writeSelectedRows(0, -1, document.leftMargin(), 
            		page.getHeight() - document.topMargin() + headerTable.getTotalHeight(),
            		writer.getDirectContent());		            
        }
	}
	//Code for header ends
}
