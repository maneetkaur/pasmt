package com.sales.bid;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class BidController {
	
	@Autowired
	BidDao bidDao;
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	ServletContext servletContext;
	
	BidService bidService = new BidService();
	
	@RequestMapping(value="/bidList")
	public ModelAndView bidList(HttpServletRequest request, @ModelAttribute("searchBid") BidBean searchBid) {
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView bid = new ModelAndView("sales/bid/bidList");
		
		List<BidBean> bidList = bidDao.bidList(request, startIndex, 15);
		bid.addObject("searchBid", new BidBean());
		bid.addObject("bidList", bidList);
		bid.addObject("managerList", commonFunction.getManagerList());
		bidList = null;
		return bid;
	}
	
	@RequestMapping(value="/addBidForm")
	public ModelAndView addBidForm() {
		ModelAndView addBid = new ModelAndView("sales/bid/addBid");
		addBid.addObject("addBid", new BidBean());
		addBid.addObject("managerList", commonFunction.getManagerList());
		addBid.addObject("clientList", commonFunction.getClientList());
		addBid.addObject("countryList", commonFunction.getAllCountryList());
		addBid.addObject("billCurrencyList", commonFunction.getBillingCurrency());
		return addBid;
	}
	
	@RequestMapping(value="/addBid", method=RequestMethod.POST)
	public String addClient(@ModelAttribute("addBid") BidBean bid, ModelMap model){
		bidService.removeEmptyRows(bid);
		String filepath = servletContext.getRealPath("");
		bidDao.addBid(bid, filepath);
		return "redirect:/bidList";
	}
	
	@RequestMapping(value="/editBidForm", method=RequestMethod.GET)
	public ModelAndView editBidForm(HttpServletRequest request) {
		ModelAndView editBid = new ModelAndView("sales/bid/editBid");
		//get the selected bid values
		BidBean editBidBean = bidDao.getBidInformation(request.getParameter("bid_id"));
		editBid.addObject("editBid", editBidBean);
		editBid.addObject("managerList", commonFunction.getManagerList());
		editBid.addObject("clientList", commonFunction.getClientList());
		editBid.addObject("countryList", commonFunction.getAllCountryList());
		editBid.addObject("billCurrencyList", commonFunction.getBillingCurrency());
		return editBid;
	}
		
	//editBid
	@RequestMapping(value="/editBid", method=RequestMethod.POST)
	public String editBid(@ModelAttribute("editBid") BidBean bid, ModelMap model){		
		bidService.removeEmptyRows(bid);
		String filepath = servletContext.getRealPath("");
		bidDao.editBid(bid, filepath);
		return "redirect:/bidList";
	}
	
	@RequestMapping(value="/downloadBidFile", method=RequestMethod.GET)
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		bidService.downloadFile(request, response);
	}
	
	@RequestMapping(value="/getAccountManager", method=RequestMethod.GET)
	public String getAccountManager(HttpServletRequest request, ModelMap model){
		String acctManagerId = bidDao.getAccountManagerId(request.getParameter("clientId"));
		model.addAttribute("acctManagerId", acctManagerId);
		return "ajaxOperation/ajaxOperationSales";
	}
}
