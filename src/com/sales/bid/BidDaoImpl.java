package com.sales.bid;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.commonFunc.FileBean;
import com.login.LoginBean;
import com.sales.client.FileNameBean;

@Repository
@Transactional
public class BidDaoImpl implements BidDao{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<BidBean> bidList(HttpServletRequest request, int startIndex,
			int range) {
		//date_format(bid_date, '%b %e, %Y')
		String sql="SELECT pb.bid_id, pb.bid_code, pb.bid_name, pc.client_name, pb.bidding_status, pe.employee_name," +
				" date_format(pb.updated_on, '%b %e, %Y') FROM pasmt_bid pb JOIN pasmt_client pc ON pb.client_id=pc.client_id " +
				"JOIN pasmt_employee pe ON pb.account_manager_id=pe.employee_id ";
		StringBuffer where_clause = new StringBuffer("where pb.status=1 and ");
		if(request.getParameter("bidName") != null && !(request.getParameter("bidName").equals(""))){
			where_clause.append("pb.bid_name like '%" + request.getParameter("bidName") + "%' and ");
		}		
		if(request.getParameter("bidCode") != null && !(request.getParameter("bidCode").equals(""))){
			where_clause.append("pb.bid_code like '%" + request.getParameter("bidCode") + "%' and ");
		}
		
		if(request.getParameter("biddingStatus")!=null){
			String biddingStatus = request.getParameter("biddingStatus");
			if(!biddingStatus.equals("")){
				if(!biddingStatus.equals("0") && !biddingStatus.equals("5")){
					where_clause.append("pb.bidding_status=" + biddingStatus + " and ");
				}
			}			
		}
		//Search for Account Manager
		if(request.getParameter("accountManager")!= null){
			String accountManager = request.getParameter("accountManager");
			if(!accountManager.equals("") && !accountManager.equals("0")){
				where_clause.append("pb.account_manager_id=" + accountManager + " and ");
			}
		}
		
		String where=where_clause.toString().trim();
		where_clause = null;
		
		//Remove the extra 'and' in the where clause
		where=where.substring(0, where.length()-4);
		
		//calculate count for pagination
		String sql_count = "SELECT count(1) FROM pasmt_bid pb JOIN pasmt_client pc ON pb.client_id=pc.client_id " +
				"JOIN pasmt_employee pe ON pb.account_manager_id=pe.employee_id ";
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);
		request.setAttribute("total_rows", total_rows);
		
		String limit = " ORDER BY pb.bid_id DESC LIMIT " + startIndex + ", " + range;
		sql = sql + where + limit;
		//System.out.println("Count: " + sql_count);
		//System.out.println("sql: " + sql);
		
		return jdbcTemplate.query(sql, new BidListMapper());
	}

	@Override
	public void addBid(BidBean bid, String filePath) {
		try{
			StringBuffer sb;
			BidService service = new BidService();
			//System.out.println("Latest bid code" + createBidCode());
			bid.setBidCode(createBidCode());
			//Get this value from session
			LoginBean login = (LoginBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			bid.setCreatedBy(login.getEmployeeId());
			login=null;
			
			//set status reason as blank if bid is not lost or pass
			if(!bid.getBiddingStatus().equals("2") && !bid.getBiddingStatus().equals("4")){
				bid.setStatusReason("");
			}
			String sql = "INSERT INTO pasmt_bid (bid_code, client_id, bid_name, project_type, product_type," +
					"billing_currency_id, bidding_status, account_manager_id, project_specs, bid_date, created_by," +
					" status_reason, created_on, updated_on) " +
					"VALUES (?,?,?,?,?, ?,?,?,?,?,?, ?,?,?)";
			
			jdbcTemplate.update(sql, new Object[]{ bid.getBidCode(), bid.getBidClient(), bid.getBidName(), bid.getProjectType(),
			bid.getProductType(), bid.getBillingCurrency(), bid.getBiddingStatus(), bid.getAccountManager(),
			bid.getProjectSpecs(), bid.getBidDate(), bid.getCreatedBy(), bid.getStatusReason(), new Date(), new Date()});
			
			//Get the bid id of the latest entry
			sql = "SELECT MAX(bid_id) FROM pasmt_bid WHERE bid_code = ?";
			String bid_id = (String) jdbcTemplate.queryForObject(sql, new Object[] {bid.getBidCode()}, String.class);
			
			//Insert the bid requirement values, if any
			if(bid.getBidRequirements()!=null){
				List<RequirementBean> reqmtList = bid.getBidRequirements();
				if(reqmtList.size()>0){
					sb = new StringBuffer("INSERT INTO pasmt_sales_requirement (source_id, country_id, services_id," +
							"target_audience, description, sample_size, incidence, total_questions, length_of_interview," +
							"max_feasibility, quoted_cpi, status, created_on, source) VALUES");
					for(RequirementBean reqmt: reqmtList){
						/*
						 * change sample size and feasibility to 0 if service to 2
						 * Maneet 03 Feb'14
						 */
						if(!reqmt.getServices().equals("2")){
							reqmt.setSampleSize("0");
							reqmt.setMaximumFeasibility("0");
						}
						sb = sb.append("(" + bid_id + "," + reqmt.getCountry() + "," + reqmt.getServices() + "," + 
								reqmt.getTargetAudience() + ", '" + reqmt.getDescription() + "'," + reqmt.getSampleSize() + 
								", '" + reqmt.getIncidence() + "'," + reqmt.getNoOfQuestions() + "," +
								reqmt.getLoi() + "," + reqmt.getMaximumFeasibility() + "," + reqmt.getQuotedCpi() + 
								",1, now(),1), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
				}
			}				
			
			//Create the directory structure for the documents
			if(bid.getDocumentList() != null && bid.getDocumentList().size()>0){
				String path = filePath + "/Document/Bid/" + bid_id;
				if(service.createDirectory(path)){
					addBidFile(bid.getDocumentList(), path, bid_id,0);
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in addBid of BidDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}		
	}

	public void addBidFile(List<FileBean> fileList, String filePath, String bidId, int from) {
		try{
			String extension, name, sql, nameCompare;
			int dotIndex, count=0, sequence=0;
			CommonsMultipartFile file;
			File destination;
			List<FileNameBean> fileNameList=new ArrayList<FileNameBean>();
			if(from==1){
				/*Get the list of already existing filenames with sequence and extension
				 */
				fileNameList = getDocumentFNameList(bidId);
			}
			for(FileBean file_temp: fileList){
				sequence=0;
				name= file_temp.getFile().getOriginalFilename();
				if(from==1){
					//Compare with the existing files
					for(int i=0; i<fileNameList.size(); i++){
						//concatenate name and extension for comparison
						nameCompare = fileNameList.get(i).getName() + "." + fileNameList.get(i).getExtension();
						if(name.equals(nameCompare)){
							sequence = Integer.parseInt(fileNameList.get(i).getSequence());
							sequence++;
							break;
						}
					}
				}
				//Add _sequence to name if sequence>0
				for(int i=0; i<count; i++){
					nameCompare = fileList.get(i).getFile().getOriginalFilename();
					if(name.equals(nameCompare)){
						sequence++;
					}
				}
							
				dotIndex = name.lastIndexOf(".");
				extension = name.substring(dotIndex+1, name.length());			
				name = name.substring(0, dotIndex);
				if (sequence!=0) {
					destination = new File(filePath + "/" + name + "(" + Integer.toString(sequence) + ")." + extension);
				}
				else{
					destination = new File(filePath + "/" + name + "." + extension);
				}
				file = file_temp.getFile();
				try {
					file.transferTo(destination);
					sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, " +
							"sequence, document_source_type, created_on)" +
							" VALUES(" + bidId + ", '" + extension + "',2, '" + name + "'," + sequence + ",1, now())";
					System.out.println("doc sql: " + sql);
					jdbcTemplate.update(sql);
				} catch (IllegalStateException e) {
					System.out.println("Illegal state Exception in addBidFile of BidDaoImpl.java"+e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("IO Exception in addBidFile of BidDaoImpl.java" + e.toString());
					e.printStackTrace();
				}
				count++;
			}
		}
		catch(Exception e){
			System.out.println("Exception in addBidFile of BidDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public List<FileNameBean> getDocumentFNameList(String clientId) {
		String sql = "SELECT extension, document_name, sequence FROM pasmt_document WHERE document_source_id=" + clientId
				+ " AND document_type=2 AND document_source_type=1 ORDER BY sequence DESC";
		List<FileNameBean> documentList = jdbcTemplate.query(sql, new FileNameMapper());
		return documentList;
	}	
	
	public String createBidCode(){
		Date date = new Date();  
		DateFormat formatter = new SimpleDateFormat("yyMM");
		String bidCode="", temp, sql;
		try{
			sql = "SELECT count(*) FROM pasmt_bid";
			int count = jdbcTemplate.queryForInt(sql);
			temp = formatter.format(date);
			//if count is zero, this is the first entry. So, a fresh bid code has to be generated.
			if(count==0){				
				bidCode = "Bid#" + temp + "001";		
			}
			//there are existing rows in bid table in database
			else{
				String curYear, curMonth, dbYear, dbMonth, dbCode;				
				curYear= temp.substring(0,2);
				curMonth = temp.substring(2,4);
				
				//Find the last entry in bid table
				sql = "SELECT bid_code FROM pasmt_bid WHERE bid_id in (SELECT MAX(bid_id) FROM pasmt_bid)";
				BidBean bid = (BidBean)jdbcTemplate.queryForObject(sql, new RowMapper<BidBean>(){
					@Override
					public BidBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {					
						BidBean bid = new BidBean();
						bid.setBidCode(rs.getString("bid_code"));						
						return bid;
					}					
				});
				
				dbCode = bid.getBidCode();
				dbYear = dbCode.substring(4,6);
				dbMonth = dbCode.substring(6,8);
				
				//if current month and year match the latest entry
				if(curYear.equals(dbYear) && curMonth.equals(dbMonth)){
					int tempCount = Integer.parseInt(dbCode.substring(dbCode.length()-3));
					tempCount++;
					if(tempCount < 10){
						bidCode = "Bid#" + temp + "00" + Integer.toString(tempCount);
					}
					else if(tempCount>=10 && tempCount<100){
						bidCode = "Bid#" + temp + "0" + Integer.toString(tempCount);
					}
					else {
						bidCode = "Bid#" + temp + Integer.toString(tempCount);
					}
					//assign all variables to null
					curMonth = curYear = dbCode = dbYear = dbMonth = null;
				}
				else{
					bidCode = "Bid#" + temp + "001";
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in createBidCode of BidDaoImpl.java");
		}
		finally{
			date = null;
			formatter = null;
			temp = sql = null;
		}
		return bidCode;
	}

	@Override
	public BidBean getBidInformation(String bidId) {
		String sql = "SELECT pb.bid_id, pb.bid_code, pb.client_id, pe.employee_name, pb.bid_name, pb.project_type, " +
			"pb.product_type, pb.billing_currency_id, pb.bidding_status, pb.account_manager_id, pb.project_specs, " +
			"DATE(pb.bid_date), date_format(pb.created_on, '%b %e, %Y'), pb.status_reason FROM pasmt_bid pb, " +
			"pasmt_employee pe WHERE pb.bid_id=" + bidId + " AND pe.employee_id=pb.created_by";
		
		BidBean bid = (BidBean)jdbcTemplate.queryForObject(sql, new EditBidMapper());
		
		//Get requirement information from the database
		sql = "SELECT bid_requirement_id, source_id, country_id, services_id, target_audience, description," +
				" sample_size, incidence, total_questions, length_of_interview, max_feasibility, setup_fee, " +
				" quoted_cpi FROM pasmt_sales_requirement WHERE source_id=" + bidId + " AND source=1 ORDER BY " +
				"bid_requirement_id";
		List<RequirementBean> reqmtList = jdbcTemplate.query(sql, new BidRequirementMapper());
		bid.setBidRequirements(reqmtList);
		
		//Set the current list size of requirement
		bid.setBidReqmtSize(String.valueOf(bid.getBidRequirements().size()));
		
		sql = "SELECT extension, document_name, sequence  FROM pasmt_document WHERE document_source_id=" + 
				bidId + " AND document_source_type=1 AND document_type=2 ORDER BY sequence DESC";
		List<String> documentList = jdbcTemplate.query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				String name, sequence;
				sequence = rs.getString("sequence");
				if(sequence.equals("0")){
					name = rs.getString("document_name") + "." + rs.getString("extension");
				}
				else{
					name = rs.getString("document_name") + "(" + sequence + ")." + rs.getString("extension");
				}
				return name;
			}
			
		});
		bid.setDocumentNames(documentList);
		
		return bid;
	}

	@Override
	public void editBid(BidBean bid, String filePath) {
		try{
			StringBuffer sb;
			BidService service = new BidService();
			if(!bid.getBiddingStatus().equals("2") && !bid.getBiddingStatus().equals("4")){
				bid.setStatusReason("");
			}
			String sql = "UPDATE pasmt_bid SET client_id=?, bid_name=?, project_type=?, status_reason=?, " +
					"product_type=?, billing_currency_id=?, bidding_status=?, account_manager_id=?, project_specs=?," +
					" bid_date=?, updated_on=? WHERE bid_id=?";
			
			jdbcTemplate.update(sql, new Object[]{bid.getBidClient(), bid.getBidName(), bid.getProjectType(), 
					bid.getStatusReason(), bid.getProductType(), bid.getBillingCurrency(), bid.getBiddingStatus(),
					bid.getAccountManager(), bid.getProjectSpecs(), bid.getBidDate(), new Date(), bid.getBidId()});
			
			//Create the directory structure for the documents
			String bid_id = bid.getBidId();
			if(bid.getDocumentList() != null && bid.getDocumentList().size()>0){
				String path = filePath + "/Document/Bid/" + bid_id;
				service.createDirectory(path);
				addBidFile(bid.getDocumentList(), path, bid_id,1);
			}
			
			if(bid.getBidRequirements()!=null){
				int currentSize = Integer.parseInt(bid.getBidReqmtSize());
				List<RequirementBean> reqmtList = bid.getBidRequirements();
				RequirementBean newReqmt;
				
				//Update the existing rows if there are changes in any
				if(bid.getReqmtIdList().length()>0){
					String reqmtArray[] = bid.getReqmtIdList().split(",");
					if(currentSize>0 && reqmtArray.length>0){
						for(int i=0; i<reqmtArray.length; i++){
							newReqmt = reqmtList.get(Integer.parseInt(reqmtArray[i]));
							
							/*
							 * change sample size to 0 if service to 2
							 * Maneet 03 Feb'14
							 */
							if(!newReqmt.getServices().equals("2")){
								newReqmt.setSampleSize("0");
								newReqmt.setMaximumFeasibility("0");
							}
							sql = "UPDATE pasmt_sales_requirement SET country_id=" + newReqmt.getCountry() + ", services_id=" 
									+ newReqmt.getServices() + ", target_audience=" + newReqmt.getTargetAudience() + 
									", description='" + newReqmt.getDescription() + "', sample_size=" + newReqmt.getSampleSize()
									+ ", incidence=" + newReqmt.getIncidence() + ", total_questions=" + 
									newReqmt.getNoOfQuestions() +  ", length_of_interview=" + newReqmt.getLoi() + 
									", max_feasibility=" + newReqmt.getMaximumFeasibility() + ", quoted_cpi='" + 
									newReqmt.getQuotedCpi() + "',source=1 WHERE bid_requirement_id=" + newReqmt.getRequirementId();
							jdbcTemplate.update(sql);
						}
					}
				}
				//Insert the newly added requirement rows
				if(reqmtList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_sales_requirement (source_id, country_id, services_id, target_audience," +
							" description, sample_size, incidence, total_questions, length_of_interview, max_feasibility," +
							" quoted_cpi, source) VALUES");
					for(int i=currentSize; i<reqmtList.size(); i++){
						RequirementBean reqmt = reqmtList.get(i);
						
						/*
						 * change sample size and feasibility to 0 if service to 2
						 * Maneet 03 Feb'14
						 */
						if(!reqmt.getServices().equals("2")){
							reqmt.setSampleSize("0");
							reqmt.setMaximumFeasibility("0");
						}
						sb = sb.append("(" + bid_id + "," + reqmt.getCountry() + "," + reqmt.getServices() + "," + 
								reqmt.getTargetAudience() + ", '" + reqmt.getDescription() + "'," + reqmt.getSampleSize() +
								"," + reqmt.getIncidence() + "," + reqmt.getNoOfQuestions() + "," + reqmt.getLoi() +
								"," + reqmt.getMaximumFeasibility() + ", '" + reqmt.getQuotedCpi() + "',1), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in editBid of BidDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	@Override
	public String getAccountManagerId(String clientId) {
		String sql="SELECT irb_acct_manager_id FROM pasmt_client WHERE client_id=" + clientId;
		String acctManagerId = (String) jdbcTemplate.queryForObject(sql, String.class);
		return acctManagerId;
	}
}
