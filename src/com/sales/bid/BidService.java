package com.sales.bid;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.commonFunc.CommonFunction;
import com.commonFunc.FileBean;

public class BidService {
	public BidBean removeEmptyRows(BidBean bid){
		
		//remove empty requirements rows
		if(bid.getBidRequirements().size()==1){
			RequirementBean tempReqmt = bid.getBidRequirements().get(0);
			//if all the fields are empty
			if(tempReqmt.getCountry().equals("0") && tempReqmt.getServices().equals("0") &&
					tempReqmt.getTargetAudience().equals("0") && tempReqmt.getSampleSize().equals("") && 
					tempReqmt.getDescription().equals("") && tempReqmt.getIncidence().equals("-1") &&
					tempReqmt.getNoOfQuestions().equals("") && tempReqmt.getLoi().equals("") &&
					tempReqmt.getMaximumFeasibility().equals("") && tempReqmt.getQuotedCpi().equals("")){
				bid.setBidRequirements(null);
			}
		}
		else{			
			List<RequirementBean> tempBids = new ArrayList<RequirementBean>();			
			for(RequirementBean tempReqmt: bid.getBidRequirements()){
				//if the row has been removed, every element of the row becomes null. So, check if the first element is not null				
				if(tempReqmt.getCountry()!=null){
					if(!(tempReqmt.getCountry().equals("0") && tempReqmt.getServices().equals("0") &&
						tempReqmt.getTargetAudience().equals("0") && tempReqmt.getSampleSize().equals("") && 
						tempReqmt.getDescription().equals("") && tempReqmt.getIncidence().equals("-1") &&
						tempReqmt.getNoOfQuestions().equals("") && tempReqmt.getLoi().equals("") &&
						tempReqmt.getMaximumFeasibility().equals("") && tempReqmt.getQuotedCpi().equals(""))){
							tempBids.add(tempReqmt);
					}
				}
			}
			bid.setBidRequirements(tempBids);
		}
		
		//remove empty Document rows
		if(bid.getDocumentList().size()==1){
			FileBean tempFile = bid.getDocumentList().get(0);
			if(tempFile.getFile().getOriginalFilename().equals("")){				
				bid.setDocumentList(null);
			}
		}
		else{			
			
			List<FileBean> tempDocs = new ArrayList<FileBean>();
			for(FileBean tempFile: bid.getDocumentList()){
				if(tempFile.getFile()!=null && !(tempFile.getFile().getOriginalFilename().equals(""))){
					tempDocs.add(tempFile);
				}
			}
			bid.setDocumentList(tempDocs);
		}
		return bid;
	}
	
	public boolean createDirectory(String filePath){
		boolean status=false;
		File files = new File(filePath);
		if (!files.exists()) {
			if (files.mkdirs()) {
				status = true;
			} else {
				System.out.println("Failed to create directories for " + filePath + " in BidService.java!");
			}
		}						
		return status;
	}
	
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		
		ServletContext servletContext = request.getServletContext();
		String path=servletContext.getRealPath("");
		String fileName= request.getParameter("fileName");
		String bidId = request.getParameter("bidId");
		String fullPath=path+"/Document/Bid/" + bidId + "/"+fileName;		
		CommonFunction commonFunction = new CommonFunction();
		commonFunction.downloadFile(servletContext, response, fullPath);
	}
	
}
