package com.sales.bid;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BidListMapper implements RowMapper<BidBean>{

	@Override
	public BidBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		BidBean bid = new BidBean();
		bid.setBidId((String) rs.getString("pb.bid_id"));
		bid.setBidCode((String) rs.getString("pb.bid_code"));
		bid.setBidName((String) rs.getString("pb.bid_name"));
		bid.setBidClient((String) rs.getString("pc.client_name"));
		String bidStatus = (String) rs.getString("pb.bidding_status");
		if(bidStatus.equals("1")){
			bidStatus = "Bidding";
		}
		else if(bidStatus.equals("2")){
			bidStatus = "Lost";
		}
		else if (bidStatus.equals("3")) {
			bidStatus = "Won";
		}
		else if (bidStatus.equals("4")) {
			bidStatus = "Pass";
		}
		bid.setBiddingStatus(bidStatus);
		bid.setAccountManager((String) rs.getString("pe.employee_name"));
		bid.setUpdateDate((String) rs.getString("date_format(pb.updated_on, '%b %e, %Y')"));
		return bid;
	}
	
}
