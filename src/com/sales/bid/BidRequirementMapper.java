package com.sales.bid;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BidRequirementMapper implements RowMapper<RequirementBean>{

	@Override
	public RequirementBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		RequirementBean reqmt = new RequirementBean();
		reqmt.setRequirementId(rs.getString("bid_requirement_id"));
		reqmt.setBidId(rs.getString("source_id"));
		reqmt.setCountry(rs.getString("country_id"));
		reqmt.setServices(rs.getString("services_id"));
		reqmt.setTargetAudience(rs.getString("target_audience"));
		reqmt.setDescription(rs.getString("description"));
		reqmt.setSampleSize(rs.getString("sample_size"));
		reqmt.setIncidence(rs.getString("incidence"));
		reqmt.setNoOfQuestions(rs.getString("total_questions"));
		reqmt.setLoi(rs.getString("length_of_interview"));
		reqmt.setMaximumFeasibility(rs.getString("max_feasibility"));
		reqmt.setQuotedCpi(rs.getString("quoted_cpi"));
		reqmt.setSetupFee(rs.getString("setup_fee"));
		return reqmt;
	}
	
}
