package com.sales.bid;

import java.util.List;

import javax.servlet.http.HttpServletRequest;


public interface BidDao {

	public List<BidBean> bidList(HttpServletRequest request, int startIndex, int range);
	
	public void addBid(BidBean bid, String filePath);
	
	public BidBean getBidInformation(String bidId);
	
	public void editBid(BidBean bid, String filePath);
	
	public String getAccountManagerId(String clientId);
}
