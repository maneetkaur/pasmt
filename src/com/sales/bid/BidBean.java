package com.sales.bid;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

import com.commonFunc.FileBean;

public class BidBean {
	private String bidId, bidCode, bidClient, bidName, projectType, productType, billingCurrency, biddingStatus,
	accountManager, projectSpecs, bidDate, updateDate, createdBy, createdOn, clientAddress, clientCode, statusReason;	

	private List<FileBean> documentList;
	private List<String> documentNames;
	
	private List<RequirementBean> bidRequirements;
	private String bidReqmtSize;
	private String reqmtIdList;

	public BidBean(){
		bidRequirements = LazyList.decorate(
							new ArrayList<RequirementBean>()
							, new InstantiateFactory(RequirementBean.class));
		
		documentList = LazyList.decorate(new ArrayList<FileBean>(),
						new InstantiateFactory(FileBean.class));
	}
	
	public List<RequirementBean> getBidRequirements() {
		return bidRequirements;
	}

	public void setBidRequirements(List<RequirementBean> bidRequirements) {
		this.bidRequirements = bidRequirements;
	}

	public String getBidId() {
		return bidId;
	}

	public void setBidId(String bidId) {
		this.bidId = bidId;
	}

	public String getBidCode() {
		return bidCode;
	}

	public void setBidCode(String bidCode) {
		this.bidCode = bidCode;
	}

	public String getBidClient() {
		return bidClient;
	}

	public void setBidClient(String bidClient) {
		this.bidClient = bidClient;
	}

	public String getBidName() {
		return bidName;
	}

	public void setBidName(String bidName) {
		this.bidName = bidName;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	public String getBiddingStatus() {
		return biddingStatus;
	}

	public void setBiddingStatus(String biddingStatus) {
		this.biddingStatus = biddingStatus;
	}

	public String getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	public String getProjectSpecs() {
		return projectSpecs;
	}

	public void setProjectSpecs(String projectSpecs) {
		this.projectSpecs = projectSpecs;
	}

	public String getBidDate() {
		return bidDate;
	}

	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public List<FileBean> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<FileBean> documentList) {
		this.documentList = documentList;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public List<String> getDocumentNames() {
		return documentNames;
	}

	public void setDocumentNames(List<String> documentNames) {
		this.documentNames = documentNames;
	}

	public String getReqmtIdList() {
		return reqmtIdList;
	}

	public void setReqmtIdList(String reqmtIdList) {
		this.reqmtIdList = reqmtIdList;
	}

	public String getBidReqmtSize() {
		return bidReqmtSize;
	}

	public void setBidReqmtSize(String bidReqmtSize) {
		this.bidReqmtSize = bidReqmtSize;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	
}
