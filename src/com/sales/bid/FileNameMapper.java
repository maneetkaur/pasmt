package com.sales.bid;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.client.FileNameBean;

public class FileNameMapper implements RowMapper<FileNameBean>{

	@Override
	public FileNameBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		FileNameBean file = new FileNameBean();
		file.setName(rs.getString("document_name"));
		file.setExtension(rs.getString("extension"));
		file.setSequence(rs.getString("sequence"));
		return file;
	}
	
}
