package com.sales.bid;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EditBidMapper implements RowMapper<BidBean>{

	@Override
	public BidBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		BidBean bid = new BidBean();
		bid.setBidId(rs.getString("pb.bid_id"));
		bid.setBidCode(rs.getString("pb.bid_code"));
		bid.setBidClient(rs.getString("pb.client_id"));
		bid.setCreatedBy(rs.getString("pe.employee_name"));
		bid.setBidName(rs.getString("pb.bid_name"));
		bid.setProjectType(rs.getString("pb.project_type"));
		bid.setProductType(rs.getString("pb.product_type"));
		bid.setBillingCurrency(rs.getString("pb.billing_currency_id"));
		bid.setBiddingStatus(rs.getString("pb.bidding_status"));
		bid.setAccountManager(rs.getString("pb.account_manager_id"));
		bid.setProjectSpecs(rs.getString("pb.project_specs"));
		bid.setBidDate(rs.getString("DATE(pb.bid_date)"));
		bid.setCreatedOn(rs.getString("date_format(pb.created_on, '%b %e, %Y')"));
		bid.setStatusReason(rs.getString("pb.status_reason"));
		return bid;
	}

}
