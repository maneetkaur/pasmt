package com.sales.bid;

public class RequirementBean {
	private String requirementId, bidId, country, services, targetAudience, description, sampleSize, incidence,
		noOfQuestions, loi, maximumFeasibility, quotedCpi, setupFee, reason;
	
	//for project summary
	private String surveyId;

	public String getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(String requirementId) {
		this.requirementId = requirementId;
	}

	public String getBidId() {
		return bidId;
	}

	public void setBidId(String bidId) {
		this.bidId = bidId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public String getTargetAudience() {
		return targetAudience;
	}

	public void setTargetAudience(String targetAudience) {
		this.targetAudience = targetAudience;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSampleSize() {
		return sampleSize;
	}

	public void setSampleSize(String sampleSize) {
		this.sampleSize = sampleSize;
	}

	public String getIncidence() {
		return incidence;
	}

	public void setIncidence(String incidence) {
		this.incidence = incidence;
	}

	public String getNoOfQuestions() {
		return noOfQuestions;
	}

	public void setNoOfQuestions(String noOfQuestions) {
		this.noOfQuestions = noOfQuestions;
	}

	public String getLoi() {
		return loi;
	}

	public void setLoi(String loi) {
		this.loi = loi;
	}

	public String getMaximumFeasibility() {
		return maximumFeasibility;
	}

	public void setMaximumFeasibility(String maximumFeasibility) {
		this.maximumFeasibility = maximumFeasibility;
	}

	public String getQuotedCpi() {
		return quotedCpi;
	}

	public void setQuotedCpi(String quotedCpi) {
		this.quotedCpi = quotedCpi;
	}

	public String getSetupFee() {
		return setupFee;
	}

	public void setSetupFee(String setupFee) {
		this.setupFee = setupFee;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
}
