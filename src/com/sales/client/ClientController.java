package com.sales.client;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class ClientController {
	
	@Autowired
	ClientDao clientDao;
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	CommonFunction commonFunction;
	
	ClientService clientService = new ClientService();
	
	@RequestMapping(value="/inProgress")
	public String inProgress(){
		return "sales/inProgress";
	}
	
	@RequestMapping(value="/viewClientList")
	public ModelAndView viewClient(HttpServletRequest request, 
			@ModelAttribute("search_client") ClientBean searchClient){
		int startIndex=0;
        if(request.getParameter("pager.offset")!=null)
        {
            startIndex=Integer.parseInt(request.getParameter("pager.offset"));
        }
		ModelAndView client = new ModelAndView("sales/client/client_management");	
		//System.out.println("servlet real path: " + servletContext.getRealPath(""));
		List<ClientBean> clientList = clientDao.clientList(request, startIndex, 15);
		//client.addObject("search_client", new ClientBean());
		client.addObject("clientList", clientList);
		clientList = null;
		return client;
	}
	
	@RequestMapping(value="/addClientForm")
	public ModelAndView addClientForm() {
		ModelAndView addClient = new ModelAndView("sales/client/add_client");
		ClientBean client = new ClientBean();
		client.settAndC(clientDao.getTermsAndConditions());
		addClient.addObject("add_client", client);
		addClient.addObject("managerList", commonFunction.getManagerList());
		addClient.addObject("countryList", commonFunction.getAllCountryList());
		return addClient;
	}
	
	@RequestMapping(value="/addClient", method=RequestMethod.POST)
	public String addClient(@ModelAttribute("add_client") ClientBean client, ModelMap model){		
		clientService.removeEmptyRows(client);
		String filepath=servletContext.getRealPath("");
		clientDao.addClient(client, filepath);		
		return "redirect:/viewClientList";
	}
	
	@RequestMapping(value="/editClientForm", method=RequestMethod.GET)
	public ModelAndView editClientForm(HttpServletRequest request){
		ModelAndView editClient = new ModelAndView("sales/client/edit_client");
		ClientBean edit_client = clientDao.getClientInformation(request.getParameter("client_id"));		
		editClient.addObject("edit_client", edit_client);
		editClient.addObject("managerList", commonFunction.getManagerList());
		editClient.addObject("countryList", commonFunction.getAllCountryList());
		return editClient;
	}
	
	@RequestMapping(value="/downloadClientFile", method=RequestMethod.GET)
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		clientService.downloadFile(request, response);
	}
	
	@RequestMapping(value="/editClient", method=RequestMethod.POST)
	public String editClient(@ModelAttribute("edit_client") ClientBean client, ModelMap model){
		clientService.removeEmptyRows(client);
		String filepath = servletContext.getRealPath("");
		//String filepath = servletContext.getContextPath();
		clientDao.editClient(client, filepath);
		return "redirect:/viewClientList";
	}
	
	@RequestMapping(value="/validateName", method=RequestMethod.GET)
	public String validateClientName(HttpServletRequest request, ModelMap model){
		int checkName = clientDao.validateClientName(request.getParameter("clientName"));
		model.addAttribute("checkName", checkName);
		return "ajaxOperation/ajaxOperationSales";
	}
	
	@RequestMapping(value="/validateCode", method=RequestMethod.GET)
	public String validateClientCode(HttpServletRequest request, ModelMap model){
		int checkCode = clientDao.validateClientCode(request.getParameter("clientCode"));
		model.addAttribute("checkCode", checkCode);
		return "ajaxOperation/ajaxOperationSales";
	}
	
	@RequestMapping(value="/validateWebUrl", method=RequestMethod.GET)
	public String validateWebUrl(HttpServletRequest request, ModelMap model){
		int checkWeb = clientDao.validateWebUrl(request.getParameter("webUrl"));
		model.addAttribute("checkWeb", checkWeb);
		return "ajaxOperation/ajaxOperationSales";
	}
}
