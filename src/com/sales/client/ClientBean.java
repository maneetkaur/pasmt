package com.sales.client;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

import com.commonFunc.FileBean;

public class ClientBean {
	private String client_id;
	private String client_name;
	private String client_code;
	private String country_name, region, address, city, state, postal_code, client_type;
	private String websiteUrl, rfqEmail, sendCsat, csatOptions, phoneNo, faxNo;
	private String bill_name, bill_job_title, bill_email, bill_phone_no, bill_ext_no, bill_address, bill_city, 
		bill_state, bill_postal_code, bill_country, bill_acc_payable_email, bill_fax_no;
	private String acct_manager_name, status, addedOn;
	private ContactBean primaryContact;
	private List<ContactBean> additionalContacts;
	private List<FileBean> signedIOs;
	private List<FileBean> guidelineDocs;
	private List<String> signedIoNames;
	private List<String> guidelineNames;
	private String addContactsSize;
	private String contactIdList;
	
	//Fields for terms and conditions
	private TAndCBean tAndC;
	
	public ClientBean(){
		additionalContacts = LazyList.decorate
							(new ArrayList<ContactBean>(),
							 new InstantiateFactory(ContactBean.class));
		
		signedIOs = LazyList.decorate(new ArrayList<FileBean>(),
					new InstantiateFactory(FileBean.class));
		
		guidelineDocs = LazyList.decorate(new ArrayList<FileBean>(),
						new InstantiateFactory(FileBean.class));
	}
	
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getClient_code() {
		return client_code;
	}
	public void setClient_code(String client_code) {
		this.client_code = client_code;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getAcct_manager_name() {
		return acct_manager_name;
	}
	public void setAcct_manager_name(String acct_manager_name) {
		this.acct_manager_name = acct_manager_name;
	}	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}	
	public String getClient_type() {
		return client_type;
	}
	public void setClient_type(String client_type) {
		this.client_type = client_type;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getRfqEmail() {
		return rfqEmail;
	}
	public void setRfqEmail(String rfqEmail) {
		this.rfqEmail = rfqEmail;
	}
	public String getSendCsat() {
		return sendCsat;
	}
	public void setSendCsat(String sendCsat) {
		this.sendCsat = sendCsat;
	}
	public String getCsatOptions() {
		return csatOptions;
	}
	public void setCsatOptions(String csatOptions) {
		this.csatOptions = csatOptions;
	}
	public String getBill_name() {
		return bill_name;
	}
	public void setBill_name(String bill_name) {
		this.bill_name = bill_name;
	}
	public String getBill_job_title() {
		return bill_job_title;
	}
	public void setBill_job_title(String bill_job_title) {
		this.bill_job_title = bill_job_title;
	}
	public String getBill_email() {
		return bill_email;
	}
	public void setBill_email(String bill_email) {
		this.bill_email = bill_email;
	}
	public String getBill_phone_no() {
		return bill_phone_no;
	}
	public void setBill_phone_no(String bill_phone_no) {
		this.bill_phone_no = bill_phone_no;
	}
	public String getBill_ext_no() {
		return bill_ext_no;
	}
	public void setBill_ext_no(String bill_ext_no) {
		this.bill_ext_no = bill_ext_no;
	}
	public String getBill_address() {
		return bill_address;
	}
	public void setBill_address(String bill_address) {
		this.bill_address = bill_address;
	}
	public String getBill_city() {
		return bill_city;
	}
	public void setBill_city(String bill_city) {
		this.bill_city = bill_city;
	}
	public String getBill_state() {
		return bill_state;
	}
	public void setBill_state(String bill_state) {
		this.bill_state = bill_state;
	}
	public String getBill_postal_code() {
		return bill_postal_code;
	}
	public void setBill_postal_code(String bill_postal_code) {
		this.bill_postal_code = bill_postal_code;
	}
	public String getBill_country() {
		return bill_country;
	}
	public void setBill_country(String bill_country) {
		this.bill_country = bill_country;
	}
	public String getBill_acc_payable_email() {
		return bill_acc_payable_email;
	}
	public void setBill_acc_payable_email(String bill_acc_payable_email) {
		this.bill_acc_payable_email = bill_acc_payable_email;
	}	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(String addedOn) {
		this.addedOn = addedOn;
	}
	public ContactBean getPrimaryContact() {
		return primaryContact;
	}
	public void setPrimaryContact(ContactBean primaryContact) {
		this.primaryContact = primaryContact;
	}	
	public List<ContactBean> getAdditionalContacts() {
		return additionalContacts;
	}
	public void setAdditionalContacts(List<ContactBean> additionalContacts) {
		this.additionalContacts = additionalContacts;
	}	
	public List<FileBean> getSignedIOs() {
		return signedIOs;
	}
	public void setSignedIOs(List<FileBean> signedIOs) {
		this.signedIOs = signedIOs;
	}
	public List<FileBean> getGuidelineDocs() {
		return guidelineDocs;
	}
	public void setGuidelineDocs(List<FileBean> guidelineDocs) {
		this.guidelineDocs = guidelineDocs;
	}
	public List<String> getSignedIoNames() {
		return signedIoNames;
	}
	public void setSignedIoNames(List<String> signedIoNames) {
		this.signedIoNames = signedIoNames;
	}
	public List<String> getGuidelineNames() {
		return guidelineNames;
	}
	public void setGuidelineNames(List<String> guidelineNames) {
		this.guidelineNames = guidelineNames;
	}
	public String getAddContactsSize() {
		return addContactsSize;
	}
	public void setAddContactsSize(String addContactsSize) {
		this.addContactsSize = addContactsSize;
	}
	public String getContactIdList() {
		return contactIdList;
	}
	public void setContactIdList(String contactIdList) {
		this.contactIdList = contactIdList;
	}
	public String getBill_fax_no() {
		return bill_fax_no;
	}
	public void setBill_fax_no(String bill_fax_no) {
		this.bill_fax_no = bill_fax_no;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	public TAndCBean gettAndC() {
		return tAndC;
	}
	public void settAndC(TAndCBean tAndC) {
		this.tAndC = tAndC;
	}

	public String toString(){
		String print = "Client id: " + this.getClient_id() + "\nClient Name: " +this.getClient_name() +
				"\nClient code: "+ this.getClient_code() + "\nCountry: " + this.getCountry_name() +
				"\nManager Name: " + this.getAcct_manager_name();
		return print;
	}
}
