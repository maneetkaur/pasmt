package com.sales.client;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TandCMapper implements RowMapper<TAndCBean>{

	@Override
	public TAndCBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		TAndCBean tAndC = new TAndCBean();
		tAndC.setTermsOfAgreement(rs.getString("tc.terms_of_agreement"));
		tAndC.setDefinitions(rs.getString("tc.definitions"));
		tAndC.setServiceLevelAgreement(rs.getString("tc.service_level_agreement"));
		tAndC.setProjectSetupAndTesting(rs.getString("tc.project_setup_and_testing"));
		tAndC.setQualityMeasures(rs.getString("tc.quality_measures"));
		tAndC.setProposalRevision(rs.getString("tc.proposal_revision"));
		tAndC.setProjectClosure(rs.getString("tc.project_closure"));
		tAndC.setCostAndBilling(rs.getString("tc.cost_and_billing"));
		tAndC.setPaymentTerms(rs.getString("tc.payment_terms"));
		tAndC.setGoverningLawAndJurisdiction(rs.getString("tc.governing_law_and_jurisdiction"));
		return tAndC;
	}
	
}
