package com.sales.client;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ClientMapper implements RowMapper<ClientBean>{

	@Override
	public ClientBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ClientBean client = new ClientBean();
		client.setClient_id((String)rs.getString("pc.client_id"));
		client.setClient_name(rs.getString("pc.client_name"));
		client.setClient_code(rs.getString("pc.client_code"));
		client.setCountry_name(rs.getString("lc.country_name"));
		client.setAcct_manager_name(rs.getString("pe.employee_name"));
		//pc.pri_contact_name
		String temp;
		if(rs.getString("pc.region") != null){
			temp = rs.getString("pc.region");
			if(temp.equals("1")){
				temp= "North America";
			}
			else if (temp.equals("2")) {
				temp = "APAC";
			}
			else if (temp.equals("3")){
				temp = "Europe";
			}
			else if (temp.equals("4")) {
				temp = "LatAM";
			}
			else if (temp.equals("5")){
				temp = "Africa";
			}
			client.setRegion(temp);
		}
		
		
		temp =rs.getString("pc.status");
		if(temp.equals("0")){
			temp="Inactive";
		}
		else if(temp.equals("1")){
			temp = "Active";
		}
		client.setStatus(temp);
		/*ContactBean primaryContact = new ContactBean();
		primaryContact.setName(rs.getString("pc.pri_contact_name"));
		client.setPrimaryContact(primaryContact);*/
		return client;
	}

}
