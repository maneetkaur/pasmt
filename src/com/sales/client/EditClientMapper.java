package com.sales.client;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EditClientMapper implements RowMapper<ClientBean> {

	@Override
	public ClientBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		ClientBean client = new ClientBean();
		client.setClient_id((String)rs.getString("client_id"));
		client.setClient_name((String)rs.getString("client_name"));
		client.setClient_code((String)rs.getString("client_code"));
		client.setRegion((String)rs.getString("region"));
		client.setClient_type((String)rs.getString("client_type"));
		client.setAddress((String)rs.getString("address"));
		client.setCity((String)rs.getString("city"));
		client.setState((String)rs.getString("state"));
		client.setPostal_code((String)rs.getString("postal_code"));
		client.setCountry_name((String)rs.getString("country_id"));
		client.setWebsiteUrl((String)rs.getString("website_url"));
		client.setRfqEmail((String)rs.getString("rfq_email"));
		client.setAcct_manager_name((String)rs.getString("irb_acct_manager_id"));
		client.setAddedOn((String)rs.getString("date_format(client_added_on, '%b %e, %Y')"));
		client.setSendCsat((String)rs.getString("send_csat"));
		client.setCsatOptions((String)rs.getString("csat_options"));
		client.setBill_name((String)rs.getString("bill_name"));
		client.setBill_job_title((String)rs.getString("bill_job_title"));
		client.setBill_email((String)rs.getString("bill_email"));
		client.setBill_phone_no((String)rs.getString("bill_phone_no"));
		client.setBill_ext_no((String)rs.getString("bill_ext_no"));
		client.setBill_address((String)rs.getString("bill_address"));
		client.setBill_city((String)rs.getString("bill_city"));
		client.setBill_state((String)rs.getString("bill_state"));
		client.setBill_postal_code((String)rs.getString("bill_postal_code"));
		client.setBill_country((String)rs.getString("bill_country_id"));
		client.setBill_acc_payable_email((String)rs.getString("bill_acct_payable_email"));
		client.setStatus((String)rs.getString("status"));
		client.setPhoneNo(rs.getString("phone_no"));
		client.setFaxNo(rs.getString("fax_no"));
		client.setBill_fax_no(rs.getString("bill_fax_no"));
		return client;
	}
	
}
