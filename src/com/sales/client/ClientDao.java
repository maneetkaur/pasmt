package com.sales.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.FileBean;

public interface ClientDao {
	public List<ClientBean> clientList(HttpServletRequest request, int startIndex, int range);		

	public void addClient(ClientBean client, String filePath);
	
	public void addClientFile(List<FileBean> fileList, String filePath, int documentType, String clientId);
	
	public ClientBean getClientInformation(String clientId);
	
	public void editClient(ClientBean client, String filePath);
	
	public void editClientFile(List<FileBean> fileList, String filePath, int documentType, String clientId);
	
	public int validateClientName(String clientName);
	
	public int validateClientCode(String clientCode);
	
	public int validateWebUrl(String webUrl);
	
	public TAndCBean getTermsAndConditions();
}
