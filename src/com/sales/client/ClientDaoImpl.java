package com.sales.client;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.commonFunc.FileBean;

@Repository
@Transactional
public class ClientDaoImpl implements ClientDao{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/*
	 * (non-Javadoc)
	 * @see com.client.ClientDao#clientList(javax.servlet.http.HttpServletRequest, int, int)
	 * This method returns the list of client objects limited by the number of elements to be displayed.
	 * It checks if there are any values for search and if there are it returns the list with searched criteria.
	 */
	@Override
	public List<ClientBean> clientList(HttpServletRequest request, int startIndex, int range) {
		boolean whereCondition = false;
		String sql="SELECT pc.client_id, pc.client_name, pc.client_code, lc.country_name, pe.employee_name, " +
				"pc.region, pc.status FROM pasmt_client pc JOIN lu_country lc ON pc.country_id=lc.country_id " +
				"JOIN pasmt_employee pe ON pc.irb_acct_manager_id=pe.employee_id ";
		
		StringBuffer where_clause=new StringBuffer("where ");
		if(request.getParameter("client_name")!=null && !(request.getParameter("client_name").equals(""))){
			whereCondition = true;
			where_clause.append("pc.client_name like '%" + request.getParameter("client_name") + "%' and ");
		}
		if(request.getParameter("client_code")!=null && !(request.getParameter("client_code").equals(""))){
			whereCondition = true;
			where_clause.append("pc.client_code like '%" + request.getParameter("client_code") + "%' and ");
		}
		if(request.getParameter("status")!=null){
			String status = request.getParameter("status");
			if(status.equals("NONE") || status.equals("1")){
				whereCondition = true;
				where_clause.append("pc.status=1 and ");
			}
			else if (status.equals("0")) {
				whereCondition = true;
				where_clause.append("pc.status=0 and ");
			}
		}
		//if the request parameter is null, then listing is called for the first time and status should be active by default
		else{
			whereCondition = true;
			where_clause.append("pc.status=1 and ");
		}
		String where=where_clause.toString().trim();
		where_clause=null;
		
		//Check if there is any condition in where and if there is remove the extra 'and' in the where clause
		if (!whereCondition) {
			where="";
		}
		else{
			where=where.substring(0, where.length()-4);
		}
		
		//calculate count for pagination
		String sql_count = "SELECT count(1) FROM pasmt_client pc JOIN lu_country lc ON pc.country_id=lc.country_id " +
				"JOIN pasmt_employee pe ON pc.irb_acct_manager_id=pe.employee_id ";
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);
		request.setAttribute("total_rows", total_rows);
		
		String limit= " ORDER BY pc.client_id DESC LIMIT " + startIndex + ", " + range;		
		sql = sql + where + limit;
		//System.out.println("sql: " + sql);
		List<ClientBean> clientList = jdbcTemplate.query(sql, new ClientMapper());
		for(ClientBean client: clientList){
			sql="SELECT contact_name FROM pasmt_contacts WHERE contact_type=1 AND source_type=1 AND source_id=" + 
					client.getClient_id();
			ContactBean primaryContact = new ContactBean();
			List<String> names = jdbcTemplate.query(sql, new RowMapper<String>(){

				@Override
				public String mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					return rs.getString("contact_name");
				}				
			});
			if(names!=null && names.size()>0){
				if(names.get(0)!=null){
					primaryContact.setName(names.get(0));
					client.setPrimaryContact(primaryContact);
				}
			}			
		}
		return clientList;
	}

	/*
	 * (non-Javadoc)
	 * @see com.client.ClientDao#addClient(com.client.ClientBean, java.lang.String)
	 * 
	 */
	@Override
	public void addClient(ClientBean client, String filePath) {
		try{
			StringBuffer sb;
			ClientService service = new ClientService();
			String sql = "INSERT INTO pasmt_client (client_name, client_code, region, client_type, address, city, " +
					"state, postal_code, country_id, website_url, rfq_email, irb_acct_manager_id, send_csat, " +
					"csat_options, bill_name, bill_job_title, bill_email, bill_phone_no, bill_ext_no, " +
					"bill_address, bill_city, bill_state, bill_postal_code, bill_country_id, bill_acct_payable_email, " +
					"phone_no, fax_no, bill_fax_no, client_added_on, created_on) " +
					"VALUES (?,?,?,?,?,?, ?,?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?)";
					
			jdbcTemplate.update(sql, new Object[]{ client.getClient_name(), client.getClient_code(), client.getRegion(),
					client.getClient_type(), client.getAddress(), client.getCity(), client.getState(), client.getPostal_code(),
					client.getCountry_name(), client.getWebsiteUrl(), client.getRfqEmail(), client.getAcct_manager_name(),
					client.getSendCsat(), client.getCsatOptions(), client.getBill_name(), client.getBill_job_title(), 
					client.getBill_email(), client.getBill_phone_no(), client.getBill_ext_no(), client.getBill_address(), 
					client.getBill_city(), client.getBill_state(), client.getBill_postal_code(), client.getBill_country(), 
					client.getBill_acc_payable_email(), client.getPhoneNo(), client.getFaxNo(), client.getBill_fax_no(),
					new Date(), new Date()});
					
			//Get the client id of the latest entry
			sql = "SELECT MAX(client_id) FROM pasmt_client WHERE client_code = ?";
			String client_id = (String) jdbcTemplate.queryForObject(sql, new Object[] {client.getClient_code()}, String.class);
			
			//Insert the primary contact information into the database
			ContactBean primContact = client.getPrimaryContact();
			sql="INSERT INTO pasmt_contacts (source_id, contact_type, contact_name, job_title, email_id, phone_no, " +
					"ext_no, source_type, created_on) VALUES (" + client_id + ",1, '" + primContact.getName() + "', '" + 
					primContact.getJobTitle() + "', '" + primContact.getEmail() + "', '" + primContact.getPhoneNo() + "', '"
					 + primContact.getExtno() + "',1, now())";
			jdbcTemplate.update(sql);
			
			//Insert the additional contacts information, if any
			if(client.getAdditionalContacts()!=null){
				List<ContactBean> addContacts = client.getAdditionalContacts();		
				
				if (addContacts.size()>0) {
					sb = new StringBuffer("INSERT INTO pasmt_contacts (source_id, contact_name, job_title, email_id, " +
							"phone_no, ext_no, source_type, created_on, contact_type) VALUES");
					for(ContactBean contact: addContacts){
						sb = sb.append("(" + client_id + ", '"+ contact.getName() + "', '" + contact.getJobTitle() + 
								"', '" + contact.getEmail() + "', '" + contact.getPhoneNo() + "', '" + contact.getExtno() +
								"',1, now(),2), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}			
					jdbcTemplate.update(sql);
				}
			}
			
			//Create the directory structure for the signed IO and guideline documents
			if(client.getSignedIOs() != null && client.getSignedIOs().size()>0){
				String path = filePath + "/Document/Client/" + client_id + "/SignedIO";
				//String path = filePath + "\\Document\\Client\\" + client_id + "\\SignedIO";
				if(service.createDirectory(path)){
					addClientFile(client.getSignedIOs(), path, 0, client_id);
				}
			}
			
			if(client.getGuidelineDocs() != null && client.getGuidelineDocs().size()>0){
				String path = filePath + "/Document/Client/" + client_id + "/Guideline";
				//String path = filePath + "\\Document\\Client\\" + client_id + "\\Guideline";
				if(service.createDirectory(path)){
					addClientFile(client.getGuidelineDocs(), path, 1, client_id);
				}
			}
			
			TAndCBean tAndC = client.gettAndC();
			//Add terms and conditions info in the database
			sql="INSERT INTO terms_and_conditions (source_id, source_type, terms_of_agreement, definitions, " +
					"service_level_agreement, project_setup_and_testing, quality_measures, proposal_revision, " +
					"project_closure, cost_and_billing, payment_terms, governing_law_and_jurisdiction, status, " +
					"created_on) VALUES (?,?,?,?, ?,?,?,?, ?,?,?,?,?, ?)";
			jdbcTemplate.update(sql, new Object[]{client_id, 1, tAndC.getTermsOfAgreement(), tAndC.getDefinitions(),
					tAndC.getServiceLevelAgreement(), tAndC.getProjectSetupAndTesting(), tAndC.getQualityMeasures(),
					tAndC.getProposalRevision(), tAndC.getProjectClosure(), tAndC.getCostAndBilling(),
					tAndC.getPaymentTerms(), tAndC.getGoverningLawAndJurisdiction(), 1, new Date()});
		}
		catch(Exception e){
			System.out.println("Exception in addClient of ClientDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public void addClientFile(List<FileBean> fileList, String filePath,
			int documentType, String clientId) {
		try{
			String extension, name, sql, nameCompare;
			int dotIndex, count=0, sequence=0;
			CommonsMultipartFile file;
			File destination;
			for(FileBean file_temp: fileList){
				sequence=0;
				name= file_temp.getFile().getOriginalFilename();
				//System.out.println("content type: " + file_temp.getFile().getContentType());
				//Add _sequence to name if sequence>0
				for(int i=0; i<count; i++){
					nameCompare = fileList.get(i).getFile().getOriginalFilename();
					if(name.equals(nameCompare)){
						sequence++;
					}
				}
							
				dotIndex = name.lastIndexOf(".");
				extension = name.substring(dotIndex+1, name.length());			
				name = name.substring(0, dotIndex);	
				if (sequence!=0) {
					destination = new File(filePath + "/" + name + "(" + Integer.toString(sequence) + ")." + extension);
				}
				else{
					destination = new File(filePath + "/" + name + "." + extension);
				}			
				file = file_temp.getFile();
				try {
					file.transferTo(destination);				
					sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, sequence, document_source_type, created_on)" +
							" VALUES(" + clientId + ", '" + extension + "', " + documentType + ", '" + name + "'," + sequence + ",0, now())";				
					jdbcTemplate.update(sql);
				} catch (IllegalStateException e) {
					System.out.println("Illegal state Exception in addClientFile of ClientDaoImpl.java"+e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("IO Exception in addClientFile of ClientDaoImpl.java" + e.toString());
					e.printStackTrace();
				}
				count++;
			}
		}
		catch(Exception e){
			System.out.println("Exception in addClientFile of ClientDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public ClientBean getClientInformation(String clientId) {
		String sql = "SELECT client_id, client_name, client_code, region, client_type, address, city, " +
				"state, postal_code, country_id, website_url, rfq_email, irb_acct_manager_id, " +
				"date_format(client_added_on, '%b %e, %Y'), send_csat, csat_options, bill_name, bill_job_title, " +
				"bill_email, bill_phone_no, bill_ext_no, bill_address, bill_city, bill_state, bill_postal_code, " +
				"bill_country_id, bill_acct_payable_email, phone_no, fax_no, bill_fax_no, status FROM pasmt_client" +
				" WHERE client_id =" + clientId;				
		
		ClientBean client = (ClientBean)jdbcTemplate.queryForObject(sql, new EditClientMapper());
		
		/*Get primary contact information from the database*/
		sql = "SELECT contacts_id, contact_name, job_title, email_id, phone_no, ext_no FROM " +
				"pasmt_contacts WHERE source_id=" + clientId + " AND contact_type=1 AND source_type=1";		
		//get the list and then get the first element of list to avoid issues with queryForObject
		List<ContactBean> primContactList = jdbcTemplate.query(sql, new ContactRowMapper());
		if(primContactList!=null && primContactList.size()>0){
			if(primContactList.get(0)!=null){
				client.setPrimaryContact(primContactList.get(0));
			}
		}
		
		/*Get additional contact information from the database*/
		sql = "SELECT contacts_id, contact_name, job_title, email_id, phone_no, ext_no " +
				"FROM pasmt_contacts WHERE source_id=" + clientId + " AND contact_type=2 AND source_type=1" +
				" ORDER BY contacts_id";
		
		List<ContactBean> contactList = jdbcTemplate.query(sql, new ContactRowMapper());
		client.setAdditionalContacts(contactList);
		
		//Set additional contacts current size in bean
		client.setAddContactsSize(String.valueOf(client.getAdditionalContacts().size()));		
		
		/*Get Signed IO and Guideline documents list from the database*/
		client.setSignedIoNames(getDocumentList(clientId, 0));
		client.setGuidelineNames(getDocumentList(clientId, 1));
		
		/*Get terms and conditions values from the database*/
		sql="SELECT tc.terms_of_agreement, tc.definitions, tc.service_level_agreement, " +
				"tc.project_setup_and_testing, tc.quality_measures, tc.proposal_revision, tc.project_closure, " +
				"tc.cost_and_billing, tc.payment_terms, tc.governing_law_and_jurisdiction FROM " +
				"terms_and_conditions tc WHERE tc.source_id=" + clientId + " AND tc.source_type=1";
		
		TAndCBean tAndC = new TAndCBean();
		List<TAndCBean> tAndCList = jdbcTemplate.query(sql, new TandCMapper());
		if(tAndCList != null && tAndCList.size()>0 && tAndCList.get(0)!=null){
			tAndC = tAndCList.get(0);
		}
		client.settAndC(tAndC);
		tAndC = null;
		
		return client;
	}
	
	public List<String> getDocumentList(String clientId, int documentType) {
		String sql = "SELECT extension, document_name, sequence FROM pasmt_document WHERE document_source_id=" + clientId
				+ " AND document_type=" + documentType + " AND document_source_type=0 ORDER BY sequence DESC";
		List<String> documentList = jdbcTemplate.query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				String name, sequence;
				sequence = rs.getString("sequence");
				if(sequence.equals("0")){
					name = rs.getString("document_name") + "." + rs.getString("extension");
				}
				else{
					name = rs.getString("document_name") + "(" + sequence + ")." + rs.getString("extension");
				}
				return name;
			}
			
		});
		return documentList;
	}

	@Override
	public void editClient(ClientBean client, String filePath) {
		try{
			StringBuffer sb;
			String sql = "UPDATE pasmt_client SET client_name=?, client_code=?, region=?, client_type=?, address=?, " +
					"city=?, state=?, postal_code=?, country_id=?, website_url=?, rfq_email=?, irb_acct_manager_id=?, " +
					"send_csat=?, csat_options=?, bill_name=?, bill_job_title=?, bill_email=?, bill_phone_no=?, " +
					"bill_ext_no=?, bill_address=?, bill_city=?, bill_state=?, bill_postal_code=?, " +
					"bill_country_id=?, bill_acct_payable_email=?, phone_no=?, fax_no=?, bill_fax_no=?, status=? " +
					"WHERE client_id=?";
			
			jdbcTemplate.update(sql, new Object[]{ client.getClient_name(), client.getClient_code(), client.getRegion(),
					client.getClient_type(), client.getAddress(), client.getCity(), client.getState(), client.getPostal_code(),
					client.getCountry_name(), client.getWebsiteUrl(), client.getRfqEmail(), client.getAcct_manager_name(),
					client.getSendCsat(), client.getCsatOptions(), client.getBill_name(), client.getBill_job_title(), 
					client.getBill_email(), client.getBill_phone_no(), client.getBill_ext_no(), client.getBill_address(), 
					client.getBill_city(), client.getBill_state(), client.getBill_postal_code(), client.getBill_country(), 
					client.getBill_acc_payable_email(), client.getPhoneNo(), client.getFaxNo(), client.getBill_fax_no(),
					client.getStatus(), client.getClient_id()});
			
			ClientService service = new ClientService();
			String client_id = client.getClient_id();
			
			//Update the primary contact information
			ContactBean primContact = client.getPrimaryContact();
			sql = "UPDATE pasmt_contacts SET contact_name='" + primContact.getName() + "', job_title='" + 
					primContact.getJobTitle() + "', email_id ='"+ primContact.getEmail() + "', phone_no='" + 
					primContact.getPhoneNo() +"', ext_no='" + primContact.getExtno() + "' WHERE source_id =" + 
					client_id + " AND contact_type=1 AND source_type=1";
			jdbcTemplate.update(sql);
			
			//Create the directory structure for the signed IO and guideline documents
			if(client.getSignedIOs() != null && client.getSignedIOs().size()>0){
				String path = filePath + "/Document/Client/" + client_id+ "/SignedIO";					
				//String path = filePath + "\\Document\\Client\\" + client_id+ "\\SignedIO";
				service.createDirectory(path);
				editClientFile(client.getSignedIOs(), path, 0, client_id);
			}
					
			if(client.getGuidelineDocs() != null && client.getGuidelineDocs().size()>0){
				String path = filePath + "/Document/Client/" + client_id + "/Guideline";
				//String path = filePath + "\\Document\\Client\\" + client_id + "\\Guideline";
				service.createDirectory(path);
				editClientFile(client.getGuidelineDocs(), path, 1, client_id);
			}
					
			if(client.getAdditionalContacts()!=null){
				int currentSize = Integer.parseInt(client.getAddContactsSize());
				List<ContactBean> addContacts = client.getAdditionalContacts();
				ContactBean newContact;
				
				//Update the existing rows if there are changes in any
				if(client.getContactIdList().length()>0){
					String contactArray[] = client.getContactIdList().split(",");			
					if(currentSize>0 && contactArray.length>0){
						for(int i=0; i<contactArray.length; i++){
							newContact = addContacts.get(Integer.parseInt(contactArray[i]));
							sql = "UPDATE pasmt_contacts SET contact_name='" + newContact.getName() + 
								"', job_title='" + newContact.getJobTitle() + "', email_id ='"+ newContact.getEmail()
									+ "', phone_no='" + newContact.getPhoneNo() +"', ext_no='" + newContact.getExtno() + 
									"' WHERE contacts_id=" + newContact.getId();
							jdbcTemplate.update(sql);
						}
					}
				}
				//Insert the newly added contacts
				if(addContacts.size()> currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_contacts (source_id, contact_name, job_title, email_id, " +
							"phone_no, ext_no, created_on, contact_type, source_type) VALUES");
					for(int i=currentSize; i<addContacts.size(); i++){
						ContactBean contact = addContacts.get(i);
						sb = sb.append("(" + client_id + ", '"+ contact.getName() + "', '" + contact.getJobTitle() + 
							"', '" + contact.getEmail() + "', '" + contact.getPhoneNo() + "', '" + contact.getExtno()
								+ "', now(),2,1), ");
					}
					
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}			
					jdbcTemplate.update(sql);				
				}
			}
			
			//Escape special characters
			TAndCBean tAndC = client.gettAndC();
			//Update the terms and conditions details
			sql = "UPDATE terms_and_conditions SET terms_of_agreement=?, definitions=?, service_level_agreement=?, " +
					"project_setup_and_testing=?, quality_measures=?, proposal_revision=?, project_closure=?, " +
					"cost_and_billing=?, payment_terms=?, governing_law_and_jurisdiction=? WHERE source_id=? AND " +
					"source_type=?";
			jdbcTemplate.update(sql, new Object[]{tAndC.getTermsOfAgreement(), tAndC.getDefinitions(),
					tAndC.getServiceLevelAgreement(), tAndC.getProjectSetupAndTesting(), tAndC.getQualityMeasures(),
					tAndC.getProposalRevision(), tAndC.getProjectClosure(), tAndC.getCostAndBilling(), 
					tAndC.getPaymentTerms(), tAndC.getGoverningLawAndJurisdiction(), client_id, 1});
		}
		catch(Exception e){
			System.out.println("Exception in editClient of ClientDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}
	
	@Override
	public int validateClientName(String clientName) {
		int target = -1;
		
		String sql="SELECT count(1) FROM pasmt_client WHERE client_name='" + clientName + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateClientName of ClientDaoImpl.java" + e);
		}
		return target;
	}

	@Override
	public int validateClientCode(String clientCode) {
		int target = -1;
		
		String sql="SELECT count(1) FROM pasmt_client WHERE client_code='" + clientCode + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateClientCode of ClientDaoImpl.java" + e);
		}
		return target;
	}

	@Override
	public int validateWebUrl(String webUrl) {
		int target = -1;
		
		String sql="SELECT count(1) FROM pasmt_client WHERE website_url='" + webUrl + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateWebUrl of ClientDaoImpl.java" + e);
		}
		return target;
	}

	@Override
	public void editClientFile(List<FileBean> fileList, String filePath,
			int documentType, String clientId) {
		try{
			String extension, name, sql, nameCompare;
			int dotIndex, count=0, sequence=0;
			CommonsMultipartFile file;
			File destination;
			/*Get the list of already existing filenames with sequence and extension
			 */
			List<FileNameBean> fileNameList = getDocumentFNameList(clientId, documentType);
			for(FileBean file_temp: fileList){
				sequence=0;
				name= file_temp.getFile().getOriginalFilename();
				//Compare with the existing files
				for(int i=0; i<fileNameList.size(); i++){
					//concatenate name and extension for comparison
					nameCompare = fileNameList.get(i).getName() + "." + fileNameList.get(i).getExtension();
					if(name.equals(nameCompare)){
						sequence = Integer.parseInt(fileNameList.get(i).getSequence());
						sequence++;
						break;
					}
				}
				//Add _sequence to name if sequence>0
				for(int i=0; i<count; i++){
					nameCompare = fileList.get(i).getFile().getOriginalFilename();
					if(name.equals(nameCompare)){
						sequence++;
					}
				}
							
				dotIndex = name.lastIndexOf(".");
				extension = name.substring(dotIndex+1, name.length());			
				name = name.substring(0, dotIndex);	
				if (sequence!=0) {
					destination = new File(filePath + "/" + name + "(" + Integer.toString(sequence) + ")." + extension);
				}
				else{
					destination = new File(filePath + "/" + name + "." + extension);
				}			
				file = file_temp.getFile();
				try {
					file.transferTo(destination);				
					sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, sequence, document_source_type, created_on)" +
							" VALUES(" + clientId + ", '" + extension + "', " + documentType + ", '" + name + "'," + sequence + ",0, now())";				
					jdbcTemplate.update(sql);
				} catch (IllegalStateException e) {
					System.out.println("Illegal state Exception in addClientFile of ClientDaoImpl.java"+e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("IO Exception in addClientFile of ClientDaoImpl.java" + e.toString());
					e.printStackTrace();
				}
				count++;
			}
		}
		catch(Exception e){
			System.out.println("Exception in editClientFile of ClientDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}
	
	public List<FileNameBean> getDocumentFNameList(String clientId, int documentType) {
		String sql = "SELECT extension, document_name, sequence FROM pasmt_document WHERE document_source_id=" + clientId
				+ " AND document_type=" + documentType + " AND document_source_type=0 ORDER BY sequence DESC";
		List<FileNameBean> documentList = jdbcTemplate.query(sql, new RowMapper<FileNameBean>(){

			@Override
			public FileNameBean mapRow(ResultSet rs, int rowNum) throws SQLException {
				FileNameBean file = new FileNameBean();
				file.setName(rs.getString("document_name"));
				file.setExtension(rs.getString("extension"));
				file.setSequence(rs.getString("sequence"));
				return file;
			}
			
		});
		return documentList;
	}

	@Override
	public TAndCBean getTermsAndConditions() {
		TAndCBean tAndCBean = new TAndCBean();
		String sql="SELECT tc.terms_of_agreement, tc.definitions, tc.service_level_agreement, " +
				"tc.project_setup_and_testing, tc.quality_measures, tc.proposal_revision, tc.project_closure, " +
				"tc.cost_and_billing, tc.payment_terms, tc.governing_law_and_jurisdiction FROM " +
				"terms_and_conditions tc WHERE tc.source_id=0 AND tc.source_type=1 AND tc.status=1";
		
		List<TAndCBean> tAndCList = jdbcTemplate.query(sql, new TandCMapper());
		if(tAndCList != null && tAndCList.size()>0 && tAndCList.get(0)!=null){
			tAndCBean = tAndCList.get(0);
		}
		return tAndCBean;
	}	
	
}
