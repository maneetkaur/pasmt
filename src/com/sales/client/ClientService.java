package com.sales.client;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.commonFunc.CommonFunction;
import com.commonFunc.FileBean;

public class ClientService {
	public ClientBean removeEmptyRows(ClientBean client){
		//remove empty contact rows
		if(client.getAdditionalContacts().size()==1){
			ContactBean tempContact = client.getAdditionalContacts().get(0);
			if(tempContact.getName().equals("")){
				client.setAdditionalContacts(null);
			}
		}
		if(client.getAdditionalContacts()!=null){
			List<ContactBean> tempContacts = new ArrayList<ContactBean>();
			for(ContactBean contact: client.getAdditionalContacts()){
				if (contact.getName()!=null && !(contact.getName().equals(""))) {
					tempContacts.add(contact);
				}
			}
			client.setAdditionalContacts(tempContacts);
		} 
		
		
		//remove empty Signed IO rows
		if(client.getSignedIOs().size()==1){
			FileBean tempFile = client.getSignedIOs().get(0);
			if(tempFile.getFile().getOriginalFilename().equals("")){
				client.setSignedIOs(null);
			}
		}
		if(client.getSignedIOs()!=null){			
			List<FileBean> tempSigned = new ArrayList<FileBean>();
			for(FileBean tempFile: client.getSignedIOs()){
				if (tempFile.getFile()!=null && !(tempFile.getFile().getOriginalFilename().equals(""))) {
					tempSigned.add(tempFile);
				}
			}
			client.setSignedIOs(tempSigned);
		}
				
		//remove empty Guideline documents rows
		if(client.getGuidelineDocs().size()==1){
			FileBean tempFile = client.getGuidelineDocs().get(0);
			if (tempFile.getFile().getOriginalFilename().equals("")) {
				client.setGuidelineDocs(null);
			}
		}
		if(client.getGuidelineDocs()!=null){
			List<FileBean> tempGuide = new ArrayList<FileBean>();
			for(FileBean tempFile: client.getGuidelineDocs()){
				if (tempFile.getFile()!=null && !(tempFile.getFile().getOriginalFilename().equals(""))) {
					tempGuide.add(tempFile);
				}
			}
			client.setGuidelineDocs(tempGuide);
		}		
		return client;
	}
	
	public boolean createDirectory(String filePath){
		boolean status=false;
		File files = new File(filePath);
		if (!files.exists()) {
			if (files.mkdirs()) {
				status = true;
			} else {
				System.out.println("Failed to create directories for " + filePath + " in ClientService.java!");
			}
		}						
		return status;
	}
	
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		ServletContext servletContext = request.getServletContext();
		String path=servletContext.getRealPath("");
		//String path = servletContext.getContextPath();
		String fileName= request.getParameter("fileName");
		String clientId = request.getParameter("client_id");
		String documentType = request.getParameter("documentType");
		if(documentType.equals("0")){
			documentType="SignedIO";
		}
		else if (documentType.equals("1")) {
			documentType="Guideline";
		}
		String fullPath=path+"/Document/Client/" + clientId + "/" + documentType + "/"+fileName;
		//String fullPath=path+"\\Document\\Client\\" + clientId + "\\" + documentType + "\\"+fileName;
		//System.out.println("complete path: " + fullPath);
		CommonFunction commonFunction = new CommonFunction();
		commonFunction.downloadFile(servletContext, response, fullPath);
	}
}
