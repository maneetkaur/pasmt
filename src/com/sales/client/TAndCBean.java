package com.sales.client;

public class TAndCBean {
	private String termsOfAgreement, definitions, serviceLevelAgreement, projectSetupAndTesting, qualityMeasures, 
		proposalRevision, projectClosure, costAndBilling, paymentTerms, governingLawAndJurisdiction;

	public String getTermsOfAgreement() {
		return termsOfAgreement;
	}

	public void setTermsOfAgreement(String termsOfAgreement) {
		this.termsOfAgreement = termsOfAgreement;
	}

	public String getDefinitions() {
		return definitions;
	}

	public void setDefinitions(String definitions) {
		this.definitions = definitions;
	}

	public String getServiceLevelAgreement() {
		return serviceLevelAgreement;
	}

	public void setServiceLevelAgreement(String serviceLevelAgreement) {
		this.serviceLevelAgreement = serviceLevelAgreement;
	}

	public String getQualityMeasures() {
		return qualityMeasures;
	}

	public void setQualityMeasures(String qualityMeasures) {
		this.qualityMeasures = qualityMeasures;
	}

	public String getProposalRevision() {
		return proposalRevision;
	}

	public void setProposalRevision(String proposalRevision) {
		this.proposalRevision = proposalRevision;
	}

	public String getProjectClosure() {
		return projectClosure;
	}

	public void setProjectClosure(String projectClosure) {
		this.projectClosure = projectClosure;
	}

	public String getCostAndBilling() {
		return costAndBilling;
	}

	public void setCostAndBilling(String costAndBilling) {
		this.costAndBilling = costAndBilling;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getGoverningLawAndJurisdiction() {
		return governingLawAndJurisdiction;
	}

	public void setGoverningLawAndJurisdiction(String governingLawAndJurisdiction) {
		this.governingLawAndJurisdiction = governingLawAndJurisdiction;
	}

	public String getProjectSetupAndTesting() {
		return projectSetupAndTesting;
	}

	public void setProjectSetupAndTesting(String projectSetupAndTesting) {
		this.projectSetupAndTesting = projectSetupAndTesting;
	}
}