package com.sales.client;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ContactRowMapper implements RowMapper<ContactBean>{
	@Override
	public ContactBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		ContactBean contact = new ContactBean();
		contact.setId((String)rs.getString("contacts_id"));
		contact.setName((String)rs.getString("contact_name"));
		contact.setJobTitle((String)rs.getString("job_title"));
		contact.setEmail((String)rs.getString("email_id"));
		contact.setPhoneNo((String)rs.getString("phone_no"));
		contact.setExtno((String)rs.getString("ext_no"));
		return contact;
	}
}
