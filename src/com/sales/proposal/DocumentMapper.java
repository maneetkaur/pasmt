package com.sales.proposal;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class DocumentMapper implements RowMapper<String>{

	@Override
	public String mapRow(ResultSet rs, int rowNum) throws SQLException {
		String name, sequence;
		sequence = rs.getString("sequence");
		if(sequence.equals("0")){
			name = rs.getString("document_name") + "." + rs.getString("extension");
		}
		else{
			name = rs.getString("document_name") + "(" + sequence + ")." + rs.getString("extension");
		}
		return name;
	}
	
}
