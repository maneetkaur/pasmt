package com.sales.proposal;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.commonFunc.CommonBean;
import com.commonFunc.FileBean;
import com.login.LoginBean;
import com.sales.bid.BidBean;
import com.sales.bid.BidRequirementMapper;
import com.sales.bid.FileNameMapper;
import com.sales.bid.RequirementBean;
import com.sales.client.ContactBean;
import com.sales.client.ContactRowMapper;
import com.sales.client.FileNameBean;
import com.sales.client.TAndCBean;
import com.sales.client.TandCMapper;

@Repository
@Transactional
public class ProposalDaoImpl implements ProposalDao{
	
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<ProposalBean> proposalList(HttpServletRequest request,
			int startIndex, int range) {
		String sql = "SELECT pp.proposal_id, pp.proposal_number, pb.bid_name, pc.client_name, pb.project_type, " +
				"pe.employee_name,date_format(pp.created_on, '%b %e, %Y') FROM pasmt_proposal pp JOIN pasmt_bid pb" +
				" ON pp.bid_number=pb.bid_id " +
				"JOIN pasmt_client pc ON pb.client_id=pc.client_id JOIN pasmt_employee pe ON " +
				"pb.account_manager_id=pe.employee_id ";		
		StringBuffer where_clause = new StringBuffer("WHERE pp.status=1 AND ");
		if(request.getParameter("bid.bidName") != null && !(request.getParameter("bid.bidName").equals(""))){
			where_clause.append("pb.bid_name like '%" + request.getParameter("bid.bidName") + "%' AND ");
		}
		if(request.getParameter("proposalNumber") != null && !(request.getParameter("proposalNumber").equals(""))){
			where_clause.append("pp.proposal_number like '%" + request.getParameter("proposalNumber") + "%' AND ");
		}
		//Search for Account Manager
		if(request.getParameter("bid.accountManager")!= null){
			String accountManager = request.getParameter("bid.accountManager");
			if(!accountManager.equals("") && !accountManager.equals("0")){
				where_clause.append("pb.account_manager_id=" + accountManager + " and ");
			}
		}		
		String where=where_clause.toString().trim();
		where_clause = null;
		
		//Remove the extra 'AND' in the where clause
		where=where.substring(0, where.length()-4);
		
		//Calculate count for pagination
		String sql_count = "SELECT count(1) FROM pasmt_proposal pp JOIN pasmt_bid pb ON pp.bid_number=pb.bid_id " +
				"JOIN pasmt_client pc ON pb.client_id=pc.client_id JOIN pasmt_employee pe ON " +
				"pb.account_manager_id=pe.employee_id ";
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);
		request.setAttribute("total_rows", total_rows);
		
		String limit = " ORDER BY pp.proposal_id DESC LIMIT " + startIndex + ", " + range;
		sql = sql + where + limit;

		return jdbcTemplate.query(sql, new ProposalListMapper());
	}

	@Override
	public List<CommonBean> getWonBids() {
		//Get the list of won bids
		String sql = "SELECT bid_id, bid_code FROM pasmt_bid WHERE bidding_status=3 AND bid_id NOT IN " +
				"(SELECT bid_number FROM pasmt_proposal)";
		return jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean bid = new CommonBean();
				bid.setId(rs.getString("bid_id"));
				bid.setName(rs.getString("bid_code"));
				return bid;
			}			
		});
	}

	@Override
	public ProposalBean getBidInformation(String bidId) {
		String temp;
		String sql="SELECT pb.bid_id, pc.client_name, pb.bid_name, pb.project_type, pb.product_type, " +
				"pb.billing_currency_id, pe.employee_name, pb.project_specs, date_format(pb.bid_date, '%b %e, %Y') " +
				"FROM pasmt_bid pb JOIN pasmt_client pc ON pc.client_id = pb.client_id JOIN pasmt_employee pe " +
				"ON pe.employee_id = pb.account_manager_id WHERE pb.bid_id=" + bidId;
				
		ProposalBean proposal = jdbcTemplate.queryForObject(sql, new BidInfoMapper());
		sql="SELECT generic_value FROM lu_generic_values WHERE generic_value_id=" + 
				proposal.getBid().getBillingCurrency();
		
		//Get the billing currency value
		temp = jdbcTemplate.queryForObject(sql, String.class);
		proposal.getBid().setBillingCurrency(temp);		
		temp=null;
		
		//Get the requirement rows
		sql = "SELECT bid_requirement_id, source_id, country_id, services_id, target_audience, description, " +
				"sample_size, incidence, total_questions, length_of_interview, max_feasibility, quoted_cpi, setup_fee" +
				" FROM pasmt_sales_requirement WHERE source_id=" + bidId + " AND status=1 AND source=1";
		
		proposal.setProposalRequirements(jdbcTemplate.query(sql, new BidRequirementMapper()));
		
		//Get the document list from the database for the selected bids
		sql = "SELECT extension, document_name, sequence  FROM pasmt_document WHERE document_source_id=" + 
				bidId + " AND document_source_type=1 AND document_type=2 ORDER BY sequence DESC";
		proposal.setDocumentNames(jdbcTemplate.query(sql, new DocumentMapper()));				
		return proposal;
	}

	@Override
	public List<CommonBean> getContactList(String bidId, int sourceType) {
		//Get the contact list of the selected bid
		String temp;
		String sql="SELECT client_id FROM pasmt_bid WHERE bid_id=" + bidId;
		temp = jdbcTemplate.queryForObject(sql, String.class);
				
		sql="SELECT contacts_id, contact_name FROM pasmt_contacts WHERE status=1 AND source_id=" + temp + 
				" AND source_type=" + sourceType;
		return jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean contact = new CommonBean();
				contact.setId(rs.getString("contacts_id"));
				contact.setName(rs.getString("contact_name"));
				return contact;
			}
			
		});
	}

	@Override
	public void addProposal(ProposalBean proposal, String filePath) {
		try{
			ProposalService service = new ProposalService();
			StringBuffer sb;
			proposal.setProposalNumber(createProposalNumber());
			String sql = "INSERT INTO pasmt_proposal (proposal_number, bid_number, client_contact_id, client_manager_id, " +
					"purchase_order_no, target_audience, notes_on_targeting, start_date, end_date, project_cost, " +
					"project_minimum, project_specs, created_on, created_by) VALUES (?,?,?,?, ?,?,?,?,?,?, ?,?,?,?)";
			LoginBean login = (LoginBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			proposal.setCreatedBy(login.getEmployeeId());
			login=null;
			jdbcTemplate.update(sql, new Object[]{ proposal.getProposalNumber(), proposal.getBid().getBidCode(), 
					proposal.getClientContact(), proposal.getClientProjectManager(), proposal.getPurchaseOrderNo(), 
					proposal.getTargetAudience(), proposal.getNotesOnTargeting(), proposal.getStartDate(), 
					proposal.getEndDate(), proposal.getProjectValue(), proposal.getProjectMinimum(),
					proposal.getBid().getProjectSpecs(), new Date(), proposal.getCreatedBy()});
			//System.out.println("project specs" + proposal.getBid().getProjectSpecs());
			//Get the proposal id of the latest entry
			sql = "SELECT MAX(proposal_id) FROM pasmt_proposal WHERE proposal_number=" + proposal.getProposalNumber();
			String proposalId = jdbcTemplate.queryForObject(sql, String.class);
			
			//Insert the proposal requirements fields, if any
			if(proposal.getProposalRequirements()!=null){
				List<RequirementBean> reqmtList = proposal.getProposalRequirements();
				if(reqmtList.size()>0){
					sb = new StringBuffer("INSERT INTO pasmt_sales_requirement (source_id, country_id, services_id," +
							"target_audience, description, sample_size, incidence, total_questions, " +
							"length_of_interview, setup_fee, quoted_cpi, status, created_on, source) VALUES");
					for(RequirementBean reqmt: reqmtList){
						/*
						 * change sample size to 0 if service to 2
						 * Maneet 30 Jan'14
						 */
						if(!reqmt.getServices().equals("2")){
							reqmt.setSampleSize("0");
						}
						sb = sb.append("(" + proposalId + "," + reqmt.getCountry() + "," + reqmt.getServices() + "," + 
								reqmt.getTargetAudience() + ", '" + reqmt.getDescription() + "'," + 
								reqmt.getSampleSize() + ", '" + reqmt.getIncidence() + "'," + reqmt.getNoOfQuestions()
								+ "," + reqmt.getLoi() + "," + reqmt.getSetupFee() + "," + reqmt.getQuotedCpi() +
								",1, now(),2), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					//System.out.println("sql: " + sql);
					jdbcTemplate.update(sql);
				}
			}
			
			//Create the directory structure for the documents
			if(proposal.getDocumentList() != null && proposal.getDocumentList().size()>0){
				String path = filePath + "/Document/Proposal/" + proposalId;
				//System.out.println("path: " + path);
				if(service.createDirectory(path)){
					addProposalFile(proposal.getDocumentList(), path, proposalId, 0, proposal.getBid().getBidCode());
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in addProposal of ProposalDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}
	
	public String createProposalNumber(){
		Date date = new Date();  
		DateFormat formatter = new SimpleDateFormat("yyMM");
		String proposalNum="", temp, sql;
		try{
			sql = "SELECT count(*) FROM pasmt_proposal";
			int count = jdbcTemplate.queryForInt(sql);
			temp = formatter.format(date);
			//if count is zero, this is the first entry. So, a fresh bid code has to be generated.
			if(count==0){				
				proposalNum = temp + "001";		
			}
			//there are existing rows in bid table in database
			else{
				String curYear, curMonth, dbYear, dbMonth, dbCode;				
				curYear= temp.substring(0,2);
				curMonth = temp.substring(2,4);
				
				//Find the last entry in bid table
				sql = "SELECT proposal_number FROM pasmt_proposal WHERE proposal_id in " +
						"(SELECT MAX(proposal_id) FROM pasmt_proposal)";
				
				dbCode = jdbcTemplate.queryForObject(sql, String.class);
				
				dbYear = dbCode.substring(0,2);
				dbMonth = dbCode.substring(2,4);
				//System.out.println("dbyear: " + dbYear + " dbmonth: " + dbMonth);
				//if current month and year match the latest entry
				if(curYear.equals(dbYear) && curMonth.equals(dbMonth)){
					int tempCount = Integer.parseInt(dbCode.substring(dbCode.length()-3));
					tempCount++;
					if(tempCount < 10){
						proposalNum = temp + "00" + Integer.toString(tempCount);
					}
					else if(tempCount>=10 && tempCount<100){
						proposalNum = temp + "0" + Integer.toString(tempCount);
					}
					else {
						proposalNum = temp + Integer.toString(tempCount);
					}
					//assign all variables to null
					curMonth = curYear = dbCode = dbYear = dbMonth = null;
				}
				else{
					proposalNum = temp + "001";
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in createProposalNumber of ProposalDaoImpl.java");
		}
		finally{
			date = null;
			formatter = null;
			temp = sql = null;
		}
		return proposalNum;
	}
	
	public void addProposalFile(List<FileBean> fileList, String filePath, String proposalId, int from, String bidId) {
		try{
			String extension, name, sql, nameCompare;
			int dotIndex, count=0, sequence=0;
			CommonsMultipartFile file;
			File destination;
			List<FileNameBean> fileNameList=new ArrayList<FileNameBean>();
			
			fileNameList = getDocumentFNameList(proposalId, bidId, from);
			
			for(FileBean file_temp: fileList){
				sequence=0;
				name= file_temp.getFile().getOriginalFilename();
				
				if(fileNameList!=null && fileNameList.size()>0){
					//Compare with the existing files
					for(int i=0; i<fileNameList.size(); i++){
						//concatenate name and extension for comparison
						nameCompare = fileNameList.get(i).getName() + "." + fileNameList.get(i).getExtension();
						if(name.equals(nameCompare)){
							sequence = Integer.parseInt(fileNameList.get(i).getSequence());
							sequence++;
							break;
						}
					}
				}
				//Add _sequence to name if sequence>0
				for(int i=0; i<count; i++){
					nameCompare = fileList.get(i).getFile().getOriginalFilename();
					if(name.equals(nameCompare)){
						sequence++;
					}
				}
							
				dotIndex = name.lastIndexOf(".");
				extension = name.substring(dotIndex+1, name.length());			
				name = name.substring(0, dotIndex);
				if (sequence!=0) {
					destination = new File(filePath + "/" + name + "(" + Integer.toString(sequence) + ")." + extension);
				}
				else{
					destination = new File(filePath + "/" + name + "." + extension);
				}
				file = file_temp.getFile();
				try {
					file.transferTo(destination);
					sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, " +
							"sequence, document_source_type, created_on)" +
							" VALUES(" + proposalId + ", '" + extension + "',3, '" + name + "'," + sequence + 
							",2, now())";
					jdbcTemplate.update(sql);
				} catch (IllegalStateException e) {
					System.out.println("Illegal state Exception in addProposalFile of ProposalDaoImpl.java"+
							e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("IO Exception in addProposalFile of ProposalDaoImpl.java" + e.toString());
					e.printStackTrace();
				}
				count++;
			}
		}
		catch(Exception e){
			System.out.println("Exception in addProposalFile of ProposalDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}
	
	public List<FileNameBean> getDocumentFNameList(String proposalId, String bidId, int from) {
		String sql;
		List<FileNameBean> documentList = new ArrayList<FileNameBean>();
		List<FileNameBean> tempdocumentList;
		
		//Get the documents for the proposal id if coming from edit page, i.e., from=1
		if(from==1){
			sql = "SELECT extension, document_name, sequence FROM pasmt_document WHERE document_source_id=" +
					proposalId + " AND document_type=3 AND document_source_type=2 ORDER BY sequence DESC";
			tempdocumentList = jdbcTemplate.query(sql, new FileNameMapper());
				
			if(tempdocumentList!=null && tempdocumentList.size()>0){
				//append the values to the existing list
				for(FileNameBean file: tempdocumentList){
					documentList.add(file);
				}
			}
			tempdocumentList=null;
		}
		
		//Get the documents for the associated bid id
		sql = "SELECT extension, document_name, sequence FROM pasmt_document WHERE document_source_id=" + bidId
				+ " AND document_type=2 AND document_source_type=1 ORDER BY sequence DESC";
		tempdocumentList = jdbcTemplate.query(sql, new FileNameMapper());
		
		if(tempdocumentList!=null && tempdocumentList.size()>0){
			//append the values to the existing list
			for(FileNameBean file: tempdocumentList){
				documentList.add(file);
			}
		}
		tempdocumentList=null;
		
		return documentList;
	}

	@Override
	public ProposalBean getProposalInformation(String proposalId) {
		String temp;
		BidBean bid;
		String sql = "SELECT pp.proposal_id, pp.proposal_number, pp.bid_number, pp.client_contact_id, " +
				"pp.client_manager_id, pp.purchase_order_no, pp.target_audience, pp.notes_on_targeting, " +
				"DATE(pp.start_date), DATE(pp.end_date), pp.project_cost, pp.project_minimum, pp.project_specs, " +
				"date_format(pp.created_on, '%b %e, %Y'), pe.employee_name FROM pasmt_proposal pp JOIN " +
				"pasmt_employee pe ON pp.created_by=pe.employee_id WHERE pp.proposal_id=" + proposalId;
		
		ProposalBean proposal = (ProposalBean)jdbcTemplate.queryForObject(sql, new EditProposalMapper());				
		sql="SELECT pb.bid_id, pb.bid_code, pc.client_name, pb.bid_name, pb.project_type, pb.product_type, " +
				"pb.billing_currency_id, pe.employee_name, pb.project_specs, date_format(pb.bid_date, '%b %e, %Y'), " +
				"pc.address, pc.client_code " +
				"FROM pasmt_bid pb JOIN pasmt_client pc ON pc.client_id = pb.client_id JOIN pasmt_employee pe " +
				"ON pe.employee_id = pb.account_manager_id WHERE pb.bid_id=" + proposal.getBid().getBidId();
		
		temp = proposal.getBid().getProjectSpecs();
		bid = jdbcTemplate.queryForObject(sql, new EditBidInfoMapper());
		bid.setProjectSpecs(temp);
		proposal.setBid(bid);
		
		sql="SELECT generic_value FROM lu_generic_values WHERE generic_value_id=" + 
				proposal.getBid().getBillingCurrency();
		
		//Get the billing currency value
		temp = jdbcTemplate.queryForObject(sql, String.class);
		proposal.getBid().setBillingCurrency(temp);		
		temp=null;
		
		//Get the requirement information from the database
		sql = "SELECT bid_requirement_id, source_id, country_id, services_id, target_audience, description," +
				" sample_size, incidence, total_questions, length_of_interview, max_feasibility, setup_fee, " +
				" quoted_cpi FROM pasmt_sales_requirement WHERE source_id=" + proposalId + " AND source=2 " +
				"ORDER BY bid_requirement_id";

		List<RequirementBean>  reqmtList = jdbcTemplate.query(sql, new BidRequirementMapper());
		proposal.setProposalRequirements(reqmtList);
		
		//Set the current list size of requirement
		proposal.setPropReqmtSize(String.valueOf(proposal.getProposalRequirements().size()));
		
		//get the document list first from bid, then from proposal
		sql = "SELECT extension, document_name, sequence  FROM pasmt_document WHERE document_source_id=" + 
				proposal.getBid().getBidId() + " AND document_source_type=1 AND document_type=2 ORDER BY sequence DESC";
		List<String> documentList = jdbcTemplate.query(sql, new DocumentMapper());
		bid = proposal.getBid();
		bid.setDocumentNames(documentList);
		proposal.setBid(bid);
		bid=null;
		
		sql = "SELECT extension, document_name, sequence  FROM pasmt_document WHERE document_source_id=" + 
				proposalId + " AND document_source_type=2 AND document_type=3 ORDER BY sequence DESC";
		documentList = jdbcTemplate.query(sql, new DocumentMapper());
		proposal.setDocumentNames(documentList);
		return proposal;
	}

	@Override
	public void editProposal(ProposalBean proposal, String filePath) {
		try{
			StringBuffer sb;
			ProposalService service = new ProposalService();
			String sql = "UPDATE pasmt_proposal SET client_contact_id=?, client_manager_id=?, purchase_order_no=?, " +
					"target_audience=?, notes_on_targeting=?, start_date=?, end_date=?, project_cost=?, " +
					"project_minimum=?, project_specs=? WHERE proposal_id=?";			
			jdbcTemplate.update(sql, new Object[]{proposal.getClientContact(), proposal.getClientProjectManager(),
					proposal.getPurchaseOrderNo(), proposal.getTargetAudience(), proposal.getNotesOnTargeting(),
					proposal.getStartDate(), proposal.getEndDate(), proposal.getProjectValue(), 
					proposal.getProjectMinimum(), proposal.getBid().getProjectSpecs(), proposal.getProposalId()});
			
			//Create the directory structure for the documents
			String proposalId = proposal.getProposalId();
			if(proposal.getDocumentList() != null && proposal.getDocumentList().size()>0){
				String path = filePath + "/Document/Proposal/" + proposalId;
				service.createDirectory(path);
				addProposalFile(proposal.getDocumentList(), path, proposalId, 1, proposal.getBid().getBidId());
			}
			
			if(proposal.getProposalRequirements()!=null){
				int currentSize = Integer.parseInt(proposal.getPropReqmtSize());
				List<RequirementBean> reqmtList = proposal.getProposalRequirements();
				RequirementBean newReqmt;
				
				//Update the existing rows if there are changes in any
				if(proposal.getReqmtIdList().length()>0){
					
					String reqmtArray[] = proposal.getReqmtIdList().split(",");
					if(currentSize>0 && reqmtArray.length>0){
						//System.out.println("length: " + reqmtArray.length);
						for(int i=0; i<reqmtArray.length; i++){
							newReqmt = reqmtList.get(Integer.parseInt(reqmtArray[i]));
							
							/*
							 * change sample size to 0 if service to 2
							 * Maneet 30 Jan'14
							 */
							if(!newReqmt.getServices().equals("2")){
								newReqmt.setSampleSize("0");
							}
							//System.out.println("reqmt id: " + newReqmt.getRequirementId());
							sql = "UPDATE pasmt_sales_requirement SET country_id=" + newReqmt.getCountry() +
									", services_id=" 
									+ newReqmt.getServices() + ", target_audience=" + newReqmt.getTargetAudience() + 
									", description='" + newReqmt.getDescription() + "', sample_size=" + 
									newReqmt.getSampleSize()
									+ ", incidence=" + newReqmt.getIncidence() + ", total_questions=" + 
									newReqmt.getNoOfQuestions() +  ", length_of_interview=" + newReqmt.getLoi() + 
									", setup_fee=" + newReqmt.getSetupFee() + ", quoted_cpi='" + 
									newReqmt.getQuotedCpi() + "',source=2 WHERE bid_requirement_id=" +
									newReqmt.getRequirementId();
							jdbcTemplate.update(sql);
						}
					}
				}
				
				//Insert the newly added requirement rows
				if(reqmtList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_sales_requirement (source_id, country_id, services_id, " +
							"target_audience," +
							" description, sample_size, incidence, total_questions, length_of_interview, " +
							"setup_fee, quoted_cpi, source) VALUES");
					for(int i=currentSize; i<reqmtList.size(); i++){
						RequirementBean reqmt = reqmtList.get(i);
						
						/*
						 * change sample size to 0 if service to 2
						 * Maneet 30 Jan'14
						 */
						if(!reqmt.getServices().equals("2")){
							reqmt.setSampleSize("0");
						}						
						sb = sb.append("(" + proposalId + "," + reqmt.getCountry() + "," + reqmt.getServices() + "," + 
								reqmt.getTargetAudience() + ", '" + reqmt.getDescription() + "'," + 
								reqmt.getSampleSize() + "," + reqmt.getIncidence() + "," + reqmt.getNoOfQuestions() + 
								"," + reqmt.getLoi() + "," + reqmt.getSetupFee() + ", '" + 
								reqmt.getQuotedCpi() + "',2), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in editProposal of ProposalDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	@Override
	public ContactBean getContactInfo(String contactId, int contactType) {
		ContactBean contact = null;
		List<ContactBean> contactList = null;
		String sql;
		try{
			if(contactType==0){
				sql = "SELECT contacts_id, contact_name, job_title, email_id, phone_no, ext_no FROM " +
						"pasmt_contacts WHERE contacts_id=" + contactId;		
				//get the list and then get the first element of list to avoid issues with queryForObject
				contactList = jdbcTemplate.query(sql, new ContactRowMapper());
			}
			else if(contactType==1){
				sql = "SELECT pe.employee_name, pe.email_id, pe.phone FROM pasmt_bid pb JOIN pasmt_employee pe" +
						" ON pb.account_manager_id = pe.employee_id WHERE pb.bid_id=" + contactId;
				
				contactList = jdbcTemplate.query(sql, new RowMapper<ContactBean>(){

					@Override
					public ContactBean mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						ContactBean tempContact = new ContactBean();
						tempContact.setName(rs.getString("pe.employee_name"));
						tempContact.setEmail(rs.getString("pe.email_id"));
						tempContact.setPhoneNo(rs.getString("pe.phone"));
						return tempContact;
					}
				});
			}
			if(contactList!=null && contactList.size()>0 && contactList.get(0)!=null){
				contact = contactList.get(0);
			}
		}
		catch(Exception e){
			System.out.println("Exception in getContactInfo of ProposalDaoImpl.java: " + e);
		}
		sql=null;
		return contact;
	}

	@Override
	public List<RequirementBean> getRequirementsForPDF(
			List<RequirementBean> requirementList) {
		String sql, temp;
		for(RequirementBean requirement : requirementList){
			//get country
			sql = "SELECT country_name FROM lu_country WHERE country_id=" + requirement.getCountry();
			temp = jdbcTemplate.queryForObject(sql, String.class);
			requirement.setCountry(temp);
			
			//get services
			temp = requirement.getServices();
			if(temp.equals("1")){
				temp = "Data Processing";
			}
			else if(temp.equals("2")){
				temp = "Sample";
			}
			else if(temp.equals("3")){
				temp = "Survey Programming";
			}
			else if(temp.equals("4")){
				temp = "Translation";
			}
			requirement.setServices(temp);
			
			//target audience
			temp = requirement.getTargetAudience();
			if(temp.equals("1")){
				temp = "B2B";
			}
			else if(temp.equals("2")){
				temp = "B2C";
			}
			else if(temp.equals("3")){
				temp = "HC";
			}
			else if(temp.equals("4")){
				temp = "NA";
			}
			requirement.setTargetAudience(temp);
			
			//incidence
			temp = requirement.getIncidence();
			if(temp != null){
				if(temp.equals("101")){
					temp = "NA";
				}
				else if(temp.equals("0")){
					temp = "<1";
				}
				requirement.setIncidence(temp);
			}			
			
			/*
			 * Maneet 30 Jan'14 Changes for Sample Size
			 */
			if(!requirement.getServices().equals("Sample")){
				requirement.setSampleSize("NA");
			}
		}
		return requirementList;
	}

	@Override
	public TAndCBean getTermsAndConditions(String proposalId) {
		String temp;
		String sql = "SELECT bid_number FROM pasmt_proposal WHERE proposal_id=" + proposalId;
		temp = jdbcTemplate.queryForObject(sql, String.class);
		
		sql = "SELECT tc.terms_of_agreement, tc.definitions, tc.service_level_agreement, tc.project_setup_and_testing" +
				", tc.quality_measures, tc.proposal_revision, tc.project_closure, tc.cost_and_billing, " +
				"tc.payment_terms, tc.governing_law_and_jurisdiction FROM terms_and_conditions tc, pasmt_bid pb WHERE" +
				" tc.source_id=pb.client_id AND tc.source_type=1 AND pb.bid_id=" + temp;
		
		List<TAndCBean> tAndCList = jdbcTemplate.query(sql, new TandCMapper());
		if(tAndCList!=null && tAndCList.size()>0 && tAndCList.get(0)!=null){
			return tAndCList.get(0);
		}
		return null;
	}
}
