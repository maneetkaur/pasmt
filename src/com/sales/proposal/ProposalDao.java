package com.sales.proposal;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;
import com.sales.bid.RequirementBean;
import com.sales.client.ContactBean;
import com.sales.client.TAndCBean;

public interface ProposalDao {
	public List<ProposalBean> proposalList(HttpServletRequest request, int startIndex, int range);
	
	public List<CommonBean> getWonBids();
	
	public ProposalBean getBidInformation(String bidId);
	
	public List<CommonBean> getContactList(String bidId, int sourceType);
	
	public void addProposal(ProposalBean proposal, String filePath);
	
	public ProposalBean getProposalInformation(String proposalId);
	
	public void editProposal(ProposalBean proposal, String filePath);
	
	public ContactBean getContactInfo(String contactId, int contactType);
	
	public List<RequirementBean> getRequirementsForPDF(List<RequirementBean> requirementList);
	
	public TAndCBean getTermsAndConditions(String proposalId);
}
