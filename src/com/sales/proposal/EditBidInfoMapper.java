package com.sales.proposal;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.bid.BidBean;

public class EditBidInfoMapper implements RowMapper<BidBean>{

	@Override
	public BidBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		String temp;
		BidBean bid = new BidBean();
		bid.setBidId(rs.getString("pb.bid_id"));
		bid.setBidCode(rs.getString("pb.bid_code"));
		bid.setBidClient(rs.getString("pc.client_name"));
		bid.setBidName(rs.getString("pb.bid_name"));		
		bid.setBillingCurrency(rs.getString("pb.billing_currency_id"));
		bid.setAccountManager(rs.getString("pe.employee_name"));
		bid.setBidDate(rs.getString("date_format(pb.bid_date, '%b %e, %Y')"));
		bid.setClientAddress(rs.getString("pc.address"));
		bid.setClientCode(rs.getString("pc.client_code"));
		
		temp = rs.getString("pb.project_type");
		if(temp.equals("1")){
			temp = "AdHoc";
		}
		else if(temp.equals("2")){
			temp = "Tracker";
		}
		bid.setProjectType(temp);
		
		temp = rs.getString("pb.product_type");
		if(temp.equals("1")){
			temp = "Full Service";
		}
		else if(temp.equals("2")){
			temp = "IHUT";
		}
		else if (temp.equals("3")) {
			temp = "Panel Building";
		}
		else if (temp.equals("4")) {
			temp = "Program & Host";
		}
		else if (temp.equals("5")) {
			temp = "Sample Only";
		}
		bid.setProductType(temp);
		return bid;
	}
	
}
