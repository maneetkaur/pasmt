package com.sales.proposal;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.commonFunc.AbstractITextPdfView;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sales.bid.BidBean;
import com.sales.bid.RequirementBean;
import com.sales.client.ContactBean;
import com.sales.client.TAndCBean;

public class ProposalPDFBuilder extends AbstractITextPdfView{
	
	private ProposalBean proposal;
	private BidBean bid;
	private ContactBean salesContact, accountManager;
	private TAndCBean tAndC;
	private String billingCurrency;
	private PdfPCell sectionHeaderCell;
	private PdfPCell sectionCell;
	private static Font sectionHeaderFont;
	private static Font contentFont;
	private static Font contentBoldFont;
	
	private static final String tagline = "Our Sample, Your Research";
	private static final String irb = "Internet Research Bureau Pvt. Ltd.";
	private static final String contactDetails = "\nPlot No. 2A, 2nd Floor, Corner Market\nMalviya Nagar, " +
			"New Delhi-110017, India\nWeb: www.irbureau.com\nEmail: sales@irbureau.com\nPh. No.: (+91) 2667 1266," +
			" (+91) 4078 9940\nFax No.: (+91) 2667 1265\n";
	private static final BaseColor sectionHeaderColor = new BaseColor(127,127,127);
	private static final BaseColor tableBgColor = new BaseColor(210, 220, 191);
	private static final BaseColor irbColor = new BaseColor(16, 91, 47);
	
	//For quotation calculations
	String totalVal, setupTotalVal;
	List<String> subtotalList = new ArrayList<String>();
	DecimalFormat df = new DecimalFormat("###,##0.00");

	@Override
	protected void buildPdfDocument(Map<String, Object> model,
			Document document, PdfWriter pdfWriter, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			//Declarations
			String path = request.getServletContext().getRealPath("");
			subtotalList = new ArrayList<String>();
			//Register the new fonts
			FontFactory.registerDirectory(path + "/resources/fonts");
			
			//populate all the values
			proposal = (ProposalBean)model.get("proposal");
			bid = proposal.getBid();
			billingCurrency = bid.getBillingCurrency();
			salesContact = (ContactBean) model.get("salesContact");
			accountManager = (ContactBean) model.get("accountManager");
			
			//Calculate project value from requirement rows formula -> sample*unitprice + setupfee
			//df.setMaximumFractionDigits(2);
			float sample, unitprice, setupfee, subtotal, total=0.0f, setupTotal=0.0f;
			Set<String> audienceType = new HashSet<String>();
			
			for(RequirementBean requirement: proposal.getProposalRequirements()){
				if(requirement.getServices().equals("Sample")){
					sample = Float.parseFloat(requirement.getSampleSize());
				}
				//set sample as 1 when it is NA
				else{
					sample=1;
				}
					
				unitprice = Float.parseFloat(requirement.getQuotedCpi());
				if(requirement.getSetupFee()!=null){
					setupfee = Float.parseFloat(requirement.getSetupFee());
				}
				else{
					setupfee = 0.0f;
					requirement.setSetupFee(df.format(setupfee));
				}
				subtotal = sample * unitprice + setupfee;
				setupTotal = setupTotal + setupfee;
				total = total + subtotal;
				subtotalList.add(df.format(subtotal));
				
				//get unique audience type
				if(!requirement.getTargetAudience().equals("4") && !requirement.getTargetAudience().equals("NA")){
					audienceType.add(requirement.getTargetAudience());
				}
			}
			setupfee = Float.parseFloat(proposal.getProjectMinimum());
			if(total<setupfee){
				total = setupfee;
			}
			totalVal = df.format(total);
			setupTotalVal = df.format(setupTotal);
			
			sectionHeaderFont = FontFactory.getFont("Calibri", 13.0f, Font.BOLD, BaseColor.WHITE);
			contentFont = FontFactory.getFont("Calibri", 11.0f, BaseColor.BLACK);
			contentBoldFont = FontFactory.getFont("Calibri", 11.0f, Font.BOLD, BaseColor.BLACK);
			
			//Set the response type of the document
			response.setHeader("Content-Disposition", "attachment; filename=\"Proposal# " + proposal.getProposalNumber()
					+ "-" + bid.getClientCode() + "\"");
			
			//Print the header
			String resource = path + "/images/irblogo.png";
			document.add(getHeader(resource, proposal.getProposalNumber(), bid.getBidClient(), bid.getClientAddress()));
			
			//Set section header cell properties
			sectionHeaderCell = new PdfPCell();
			sectionHeaderCell.setBorder(Rectangle.NO_BORDER);
			sectionHeaderCell.setBackgroundColor(sectionHeaderColor);
			sectionHeaderCell.setColspan(2);
			sectionHeaderCell.setPadding(5);
			
			//Set section content cell properties
			sectionCell = new PdfPCell();
			sectionCell.setBorder(Rectangle.NO_BORDER);
			sectionCell.setBackgroundColor(tableBgColor);
			sectionCell.setPadding(5);
			
			//Print all the sections of the PDF
			document.add(getProjectDetails(audienceType.toString().replace("[", "").replace("]", "")));
			document.newPage();
			document.add(getProjectSpecs());
			if(bid.getProjectSpecs().length()>3000 || proposal.getProposalRequirements().size()>10){
				document.newPage();
			}
			document.add(getQuotation());
			document.newPage();
			if(model.get("tAndC")!=null){
				tAndC = (TAndCBean)model.get("tAndC");
				document.add(getTAndC(true));
			}
			else{
				document.add(getTAndC(false));
			}
			document.add(getWhoWeAre(path));
			document.add(getContactDetails());
			document.add(getThankYou());
		}
		catch(Exception e){
			System.out.println("Exception in buildPdfDocument of ProposalPDFBuilder.java" + e);
			e.printStackTrace();
		}
		
	}
	
	protected PdfPTable getHeader(String filePath, String proposalNumber, String clientName, String clientAddress)
			throws DocumentException, IOException{
		BaseColor irbColor2 = new BaseColor(152, 87, 45);
		PdfPTable headerTable = new PdfPTable(2);
		headerTable.setWidthPercentage(100.0f);
		headerTable.setSpacingBefore(20);
		headerTable.setWidths(new float[]{3.0f, 7.0f});
		headerTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		Image image = Image.getInstance(filePath);
		image.scaleAbsolute(136, 70);
		image.setAlt("IRB Logo");
		//false specifies that the image should not be resized to fit
		PdfPCell cell = new PdfPCell(image,false);
		cell.setBorder(Rectangle.NO_BORDER);
		headerTable.addCell(cell);
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setLeading(2.0f, 1.0f);
		Chunk chunk1 = new Chunk(tagline + "\n\n", FontFactory.getFont("Calibri",14.0f, Font.BOLD, irbColor));
		Chunk chunk2 = new Chunk("Proposal# " + proposalNumber + "\nFor " + clientName
				, FontFactory.getFont("Calibri",11.0f, irbColor2));
		Chunk chunk3 = new Chunk("\n" + clientAddress, FontFactory.getFont("Calibri",9.0f, irbColor2));
		Phrase headerPhrase = new Phrase();
		headerPhrase.add(chunk1);
		headerPhrase.add(chunk2);
		headerPhrase.add(chunk3);
		cell.setPhrase(headerPhrase);
		chunk1 = chunk2 = chunk3 = null;
		headerTable.addCell(cell);
		
		// we complete the table (otherwise the last row won't be rendered)
        headerTable.completeRow();
		return headerTable;
	}
	
	protected PdfPTable getProjectDetails(String audienceType) throws DocumentException, ParseException{
		String temp;
		Phrase contactPhrase;
		Chunk tempChunk;
		PdfPTable projectDetailsTable = new PdfPTable(2);
		projectDetailsTable.setWidthPercentage(100.0f);
		projectDetailsTable.setSpacingBefore(20);
		projectDetailsTable.setWidths(new float[]{3.0f, 7.0f});
		projectDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		projectDetailsTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		projectDetailsTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		projectDetailsTable.getDefaultCell().setBackgroundColor(tableBgColor);
		projectDetailsTable.getDefaultCell().setPadding(8);
		
		//print header
		sectionHeaderCell.setPhrase(new Phrase("Project Details", sectionHeaderFont));
		projectDetailsTable.addCell(sectionHeaderCell);
		
		
		//print content
		projectDetailsTable.addCell(new Phrase("Proposal Number", contentBoldFont));
		projectDetailsTable.addCell(new Phrase(proposal.getProposalNumber(), contentFont));
		projectDetailsTable.addCell(new Phrase("Client",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(bid.getBidClient(),contentFont));
		projectDetailsTable.addCell(new Phrase("Project Name",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(bid.getBidName(),contentFont));
		projectDetailsTable.addCell(new Phrase("Purchase Order #",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(proposal.getPurchaseOrderNo(),contentFont));
		projectDetailsTable.addCell(new Phrase("Project Type",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(bid.getProjectType(),contentFont));
		projectDetailsTable.addCell(new Phrase("Audience Type",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(audienceType,contentFont));
		projectDetailsTable.addCell(new Phrase("Target Group",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(proposal.getTargetAudience(),contentFont));
		projectDetailsTable.addCell(new Phrase("Notes on Targeting",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(proposal.getNotesOnTargeting(),contentFont));
		projectDetailsTable.addCell(new Phrase("Start Date",contentBoldFont));
		temp = changeDateFormat(proposal.getStartDate());
		projectDetailsTable.addCell(new Phrase(temp,contentFont));
		projectDetailsTable.addCell(new Phrase("Estimated End Date",contentBoldFont));
		temp = changeDateFormat(proposal.getEndDate());
		projectDetailsTable.addCell(new Phrase(temp,contentFont));
		projectDetailsTable.addCell(new Phrase("Project Value",contentBoldFont));
		
		projectDetailsTable.addCell(new Phrase(billingCurrency + " " + totalVal, contentFont));
		
		projectDetailsTable.addCell(new Phrase("Project Minimum",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(billingCurrency + " " + 
				df.format(Double.parseDouble(proposal.getProjectMinimum())), 
				contentFont));
		projectDetailsTable.addCell(new Phrase("Project Setup Fee",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(billingCurrency + " " + setupTotalVal, contentFont));
		
		//for client contact
		projectDetailsTable.addCell(new Phrase("Client Contact\n \n ",contentBoldFont));
		contactPhrase = new Phrase();
		tempChunk = new Chunk(salesContact.getName(),contentFont);
		contactPhrase.add(tempChunk);
		tempChunk = new Chunk("\nEmail: ", contentFont);
		contactPhrase.add(tempChunk);
		if(null != salesContact.getEmail()){
			tempChunk = new Chunk(salesContact.getEmail(), contentFont);
		}
		else{
			tempChunk = new Chunk("");
		}
		contactPhrase.add(tempChunk);
		tempChunk = new Chunk("\nPhone: ", contentFont);
		contactPhrase.add(tempChunk);
		if(null != salesContact.getPhoneNo()){
			tempChunk = new Chunk(salesContact.getPhoneNo(), contentFont);
		}
		else{
			tempChunk = new Chunk("");
		}
		contactPhrase.add(tempChunk);
		projectDetailsTable.addCell(contactPhrase);
		
		//for project manager
		projectDetailsTable.addCell(new Phrase("Account Manager\n \n ",contentBoldFont));
		contactPhrase = new Phrase();
		tempChunk = new Chunk(accountManager.getName(),contentFont);
		contactPhrase.add(tempChunk);
		tempChunk = new Chunk("\nEmail: ", contentFont);
		contactPhrase.add(tempChunk);
		if(null != accountManager.getEmail()){
			tempChunk = new Chunk(accountManager.getEmail(), contentFont);
		}
		else{
			tempChunk = new Chunk("");
		}
		contactPhrase.add(tempChunk);
		tempChunk = new Chunk("\nPhone: ", contentFont);
		contactPhrase.add(tempChunk);
		if(null != accountManager.getPhoneNo()){
			tempChunk = new Chunk(accountManager.getPhoneNo(), contentFont);
		}
		else{
			tempChunk = new Chunk("");
		}
		contactPhrase.add(tempChunk);
		projectDetailsTable.addCell(contactPhrase);
		
		projectDetailsTable.addCell(new Phrase("Proposal Created by",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(proposal.getCreatedBy(),contentFont));
		projectDetailsTable.addCell(new Phrase("Proposal Created on",contentBoldFont));
		
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
	    cal.setTime(sdf.parse(proposal.getCreatedOn()));
	    projectDetailsTable.addCell(new Phrase(sdf.format(cal.getTime()),contentFont));
	    cal.add(Calendar.DATE,90);
		projectDetailsTable.addCell(new Phrase("Proposal Valid Until",contentBoldFont));
		projectDetailsTable.addCell(new Phrase(sdf.format(cal.getTime()),contentFont));
		
		//complete the table
		projectDetailsTable.completeRow();
		return projectDetailsTable;
	}
	
	private String changeDateFormat(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("MMM dd, yyyy");
		String newDate="";
		  try {
		    newDate = formatter2.format(formatter.parse(date));
		  }catch(Exception e){
			  System.out.println("Error in converting date from one format to another: " + e);
		  }
		  return newDate;
	}
	
	protected PdfPTable getProjectSpecs(){
		PdfPTable projectSpecsTable = new PdfPTable(1);
		projectSpecsTable.setWidthPercentage(100.0f);
		projectSpecsTable.setSpacingBefore(20);
		projectSpecsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		projectSpecsTable.getDefaultCell().setBackgroundColor(tableBgColor);
		projectSpecsTable.getDefaultCell().setPadding(8);
		projectSpecsTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		projectSpecsTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		sectionHeaderCell.setPhrase(new Phrase("Project Specs", sectionHeaderFont));
		projectSpecsTable.addCell(sectionHeaderCell);
		projectSpecsTable.addCell(new Phrase(bid.getProjectSpecs(), contentFont));
		return projectSpecsTable;
	}
	
	protected PdfPTable getQuotation() throws DocumentException{
		String temp;
		Font contentFont = FontFactory.getFont("Calibri", 10.0f, BaseColor.BLACK);
		Font contentBoldFont = FontFactory.getFont("Calibri", 10.0f, Font.BOLD, BaseColor.BLACK);
		PdfPTable quotationTable = new PdfPTable(1);
		quotationTable.setWidthPercentage(100.0f);
		quotationTable.setSpacingBefore(20);
		quotationTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		quotationTable.getDefaultCell().setBackgroundColor(tableBgColor);
		quotationTable.getDefaultCell().setPadding(8);
		quotationTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		quotationTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		sectionHeaderCell.setPhrase(new Phrase("Quotation", sectionHeaderFont));
		quotationTable.addCell(sectionHeaderCell);
		
		PdfPTable quotationDataTable = new PdfPTable(10);
		quotationDataTable.setWidths(new float[]{18.0f, 12.0f, 9.5f, 8.0f, 5.0f, 7.0f, 7.0f, 10.0f, 10.5f, 13.0f});
		
		//set header background color
		quotationDataTable.getDefaultCell().setBackgroundColor(new BaseColor(175, 188, 149));
		quotationDataTable.getDefaultCell().setBorderColor(new BaseColor(255,255,255));
		quotationDataTable.getDefaultCell().setBorderWidth(0.75f);
		quotationDataTable.getDefaultCell().setPadding(2.0f);
		quotationDataTable.getDefaultCell().setPaddingBottom(4.0f);
		quotationDataTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		quotationDataTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		
		quotationDataTable.addCell(new Phrase("Country", contentBoldFont));
		quotationDataTable.addCell(new Phrase("Services", contentBoldFont));
		quotationDataTable.addCell(new Phrase("Audience Type", contentBoldFont));
		//quotationDataTable.addCell(new Phrase("Description", contentBoldFont));
		quotationDataTable.addCell(new Phrase("Sample Size", contentBoldFont));
		quotationDataTable.addCell(new Phrase("IR", contentBoldFont));
		quotationDataTable.addCell(new Phrase("# Quex", contentBoldFont));
		quotationDataTable.addCell(new Phrase("LOI", contentBoldFont));
		quotationDataTable.addCell(new Phrase("Unit Price", contentBoldFont));
		quotationDataTable.addCell(new Phrase("Setup Fee", contentBoldFont));
		quotationDataTable.addCell(new Phrase("Sub Total", contentBoldFont));
		quotationDataTable.completeRow();
		
		//set table bg color
		quotationDataTable.getDefaultCell().setBackgroundColor(tableBgColor);
		int index = 0;
		int sampleTotal = 0;
		for(RequirementBean requirement: proposal.getProposalRequirements()){
			quotationDataTable.addCell(new Phrase(requirement.getCountry(),contentFont));
			quotationDataTable.addCell(new Phrase(requirement.getServices(),contentFont));
			quotationDataTable.addCell(new Phrase(requirement.getTargetAudience(),contentFont));
			//quotationDataTable.addCell(new Phrase(requirement.getDescription(),contentFont));
			quotationDataTable.addCell(new Phrase(requirement.getSampleSize(),contentFont));
			
			/*
			 * Maneet 30 Jan'14 Changes for Sample size
			 */
			if(requirement.getServices().equals("Sample")){
				sampleTotal = sampleTotal + Integer.parseInt(requirement.getSampleSize());
			}			
			temp = requirement.getIncidence();
			if(!temp.equals("NA")){
				temp = temp + " %";
			}
			quotationDataTable.addCell(new Phrase(temp, contentFont));
			quotationDataTable.addCell(new Phrase(requirement.getNoOfQuestions(),contentFont));
			quotationDataTable.addCell(new Phrase(requirement.getLoi() + " min",contentFont));
			quotationDataTable.addCell(new Phrase(billingCurrency + " " + requirement.getQuotedCpi(),
					contentFont));
			quotationDataTable.addCell(new Phrase(billingCurrency + " " + requirement.getSetupFee(),contentFont));
			quotationDataTable.addCell(new Phrase(billingCurrency + " " + subtotalList.get(index),contentFont));
			quotationDataTable.completeRow();
			index++;
		}
		//add background color to footer
		quotationDataTable.getDefaultCell().setBackgroundColor(new BaseColor(189, 200, 168));
		quotationDataTable.getDefaultCell().setMinimumHeight(8.0f);
		quotationDataTable.addCell(new Phrase("Grand Total", contentBoldFont));
		quotationDataTable.addCell("");
		quotationDataTable.addCell("");
		quotationDataTable.addCell(new Phrase(String.valueOf(sampleTotal), contentBoldFont));
		quotationDataTable.addCell("");
		quotationDataTable.addCell("");
		quotationDataTable.addCell("");
		quotationDataTable.addCell("");
		quotationDataTable.addCell(new Phrase(billingCurrency + " " + setupTotalVal, contentBoldFont));
		quotationDataTable.addCell(new Phrase(billingCurrency + " " + totalVal, contentBoldFont));
		quotationDataTable.completeRow();
		
		quotationTable.addCell(quotationDataTable);
		return quotationTable;
	}
	
	protected PdfPTable getTAndC(boolean tAndCAvailable){
		Font contentFont = FontFactory.getFont("Calibri", 10.0f, BaseColor.BLACK);
		PdfPTable tAndCTable = new PdfPTable(1);
		tAndCTable.setWidthPercentage(100.0f);
		tAndCTable.setSpacingBefore(20);
		tAndCTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		tAndCTable.getDefaultCell().setBackgroundColor(tableBgColor);
		tAndCTable.getDefaultCell().setPadding(8);
		tAndCTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		tAndCTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		sectionHeaderCell.setPhrase(new Phrase("Terms and Conditions", sectionHeaderFont));
		tAndCTable.addCell(sectionHeaderCell);
		
		//add header
		if(!tAndCAvailable){
			tAndCTable.addCell(new Phrase("No Terms and Conditions data available", contentFont));
		}
		else{
			Phrase phrase;
			Chunk chunk1, chunk2;
			chunk1 = new Chunk("1. Terms Of Agreement\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getTermsOfAgreement(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("2. Definitions\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getDefinitions(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("3. Service Level Agreement\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getServiceLevelAgreement(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("4. Project Setup and Testing\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getProjectSetupAndTesting(), contentFont);
			phrase = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("5. Quality Measures\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getQualityMeasures(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("6. Proposal Revision\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getProposalRevision(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("7. Project Closure\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getProjectClosure(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("8. Cost And Billing\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getCostAndBilling(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("9. Payment Terms\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getPaymentTerms(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
			
			chunk1 = new Chunk("10. Governing Law And Jurisdiction\n\n\n", contentBoldFont);
			chunk2 = new Chunk(tAndC.getGoverningLawAndJurisdiction(), contentFont);
			phrase = new Phrase(chunk1);
			phrase.add(chunk2);
			tAndCTable.addCell(phrase);
		}
		return tAndCTable;
	}
	
	
	protected PdfPTable getContactDetails(){
		PdfPTable contactsTable = new PdfPTable(1);
		contactsTable.setWidthPercentage(100.0f);
		contactsTable.setSpacingBefore(20);
		contactsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		contactsTable.getDefaultCell().setBackgroundColor(tableBgColor);
		contactsTable.getDefaultCell().setPadding(8);
		contactsTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		contactsTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		contactsTable.getDefaultCell().setLeading(3.0f, 1.0f);
		sectionHeaderCell.setPhrase(new Phrase("Contact Details", sectionHeaderFont));
		contactsTable.addCell(sectionHeaderCell);
		
		Phrase phrase = new Phrase();
		Chunk nameChunk = new Chunk(irb, FontFactory.getFont("Calibri", 12.0f, Font.BOLD, BaseColor.BLACK));
		Chunk detailChunk = new Chunk(contactDetails, contentFont);
		phrase.add(nameChunk);
		phrase.add(detailChunk);
		contactsTable.addCell(phrase);
		return contactsTable;
	}
	
	protected PdfPTable getThankYou(){
		PdfPTable thanksTable = new PdfPTable(1);
		thanksTable.setWidthPercentage(100.0f);
		thanksTable.setSpacingBefore(40);
		thanksTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		thanksTable.getDefaultCell().setPadding(8);
		thanksTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		thanksTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		Font thankYouFont = FontFactory.getFont("Calibri", 14.0f, Font.BOLD, BaseColor.BLACK);
		thanksTable.addCell(new Phrase("Thank You", thankYouFont));
		return thanksTable;
	}
	
	protected PdfPTable getWhoWeAre(String path) throws BadElementException, MalformedURLException, IOException{
		PdfPTable whoWeAreTable = new PdfPTable(1);
		whoWeAreTable.setWidthPercentage(100.0f);
		whoWeAreTable.setSpacingBefore(20);
		whoWeAreTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		whoWeAreTable.getDefaultCell().setPadding(8);
		whoWeAreTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		whoWeAreTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		whoWeAreTable.getDefaultCell().setBackgroundColor(tableBgColor);
		
		sectionHeaderCell.setPhrase(new Phrase("Who We Are", sectionHeaderFont));
		whoWeAreTable.addCell(sectionHeaderCell);
		
		Phrase phrase = new Phrase();
		Chunk chunk1 = new Chunk("IRB", contentBoldFont);
		Chunk chunk2 = new Chunk(" is an online market research company based in New Delhi, India. Our services " +
				"mainly include providing online sample solutions to our clients in order to conduct their research " +
				"in the most effective and efficient manner. We are 100% double opt-in panel company having our own " +
				"proprietary panel in the major English speaking countries as of now and are planning to expand our " +
				"reach in other European, Asian and South American countries soon.\n\nWe have developed an integrated " +
				"platform to ensure that we provide quality data to our clients which should be in sync with the " +
				"purpose of their research and adds value to the end results. We always strive to provide end to end " +
				"data solutions to our clients.", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		whoWeAreTable.addCell(phrase);
		
		Font greenFont = FontFactory.getFont("Calibri", 11.0f, Font.BOLD, new BaseColor(0, 80, 0));
		phrase = new Phrase();
		chunk1 = new Chunk("Speciality: ", greenFont);
		chunk2 = new Chunk("\tConsumer and B2B Sample for Market Research\n                    Sample Supply, Online Data " +
				"Collection, Survey Programing, Incidence Check Surveys", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		whoWeAreTable.addCell(phrase);
		
		phrase = new Phrase();
		chunk1 = new Chunk("Establishment: ", greenFont);
		chunk2 = new Chunk("The Company was established in May, 2011. IRB is registered (registration no. 218975)" +
				" with Ministry of Corporate Affairs, India.", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		whoWeAreTable.addCell(phrase);
		
		phrase = new Phrase();
		chunk1 = new Chunk("Key Executives: ", greenFont);
		chunk2 = new Chunk("Shakti Kumar", contentBoldFont);
		Chunk chunk3 = new Chunk(", Founder/Director", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		phrase.add(chunk3);
		chunk3 = null;
		chunk1 = new Chunk("\n                             Anurag Sinha", contentBoldFont);
		chunk2 = new Chunk(", Director", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		chunk1 = new Chunk("\n                             Ashutosh", contentBoldFont);
		chunk2 = new Chunk(", Head of Business & Operations", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		whoWeAreTable.addCell(phrase);
		
		phrase = new Phrase();
		chunk1 = new Chunk("Head Quarter: ", greenFont);
		chunk2 = new Chunk("New Delhi, India", contentFont);
		phrase.add(chunk1);
		phrase.add(chunk2);
		whoWeAreTable.addCell(phrase);
		
		phrase = new Phrase();
		chunk1 = new Chunk("Affiliations:     ", greenFont);
		Image image = Image.getInstance(path + "/images/casro-member.png");
		image.scaleAbsolute(60, 24);
		image.setAlt("Member - CASRO");
		chunk2 = new Chunk(image, 0, 0, true);
		phrase.add(chunk1);
		phrase.add(chunk2);
		whoWeAreTable.addCell(phrase);
		return whoWeAreTable;
	}
}
