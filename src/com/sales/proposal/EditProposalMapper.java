package com.sales.proposal;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.bid.BidBean;

public class EditProposalMapper implements RowMapper<ProposalBean>{

	@Override
	public ProposalBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProposalBean proposal = new ProposalBean();
		BidBean bid = new BidBean(); 
		bid.setBidId(rs.getString("pp.bid_number"));		
		bid.setProjectSpecs(rs.getString("pp.project_specs"));
		proposal.setProposalId(rs.getString("pp.proposal_id"));
		proposal.setProposalNumber(rs.getString("pp.proposal_number"));
		proposal.setClientContact(rs.getString("pp.client_contact_id"));
		proposal.setClientProjectManager(rs.getString("pp.client_manager_id"));
		proposal.setPurchaseOrderNo(rs.getString("pp.purchase_order_no"));
		proposal.setTargetAudience(rs.getString("pp.target_audience"));
		proposal.setNotesOnTargeting(rs.getString("pp.notes_on_targeting"));
		proposal.setStartDate(rs.getString("DATE(pp.start_date)"));
		proposal.setEndDate(rs.getString("DATE(pp.end_date)"));
		proposal.setProjectValue(rs.getString("pp.project_cost"));
		proposal.setProjectMinimum(rs.getString("pp.project_minimum"));
		proposal.setCreatedOn(rs.getString("date_format(pp.created_on, '%b %e, %Y')"));
		proposal.setCreatedBy(rs.getString("pe.employee_name"));
		proposal.setBid(bid);
		return proposal;
	}

}
