package com.sales.proposal;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class ProposalController {
	
	@Autowired
	ProposalDao proposalDao;
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	CommonFunction commonFunction;
	
	ProposalService proposalService = new ProposalService();
	
	@RequestMapping(value="/proposalList")
	public ModelAndView proposalList(HttpServletRequest request, 
			@ModelAttribute("searchProposal") ProposalBean searchProposal){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView proposal = new ModelAndView("sales/proposal/proposalList");
		
		List<ProposalBean> proposalList = proposalDao.proposalList(request, startIndex, 15);
		proposal.addObject("searchProposal", new ProposalBean());
		proposal.addObject("proposalList", proposalList);
		proposalList = null;
		proposal.addObject("managerList", commonFunction.getManagerList());
		return proposal;
	}
	
	@RequestMapping(value="/addProposalForm")
	public ModelAndView addProposalForm(HttpServletRequest request){
		ModelAndView addProposal = new ModelAndView("sales/proposal/addProposal");
		addProposal.addObject("wonBidList", proposalDao.getWonBids());
		addProposal.addObject("countryList", commonFunction.getAllCountryList());
		
		if(request.getParameter("bidId")!=null && !(request.getParameter("bidId").equals("0"))){
			String bidId = request.getParameter("bidId");
			ProposalBean proposal = proposalDao.getBidInformation(bidId);
			addProposal.addObject("addProposal", proposal);
			addProposal.addObject("contactList", proposalDao.getContactList(bidId, 1));
		}
		else{
			addProposal.addObject("addProposal", new ProposalBean());			
		}
		
		return addProposal;
	}
	
	@RequestMapping(value="/addProposal", method=RequestMethod.POST)
	public String addProposal(@ModelAttribute("addProposal") ProposalBean addProposal, ModelMap model){
		addProposal = proposalService.removeEmptyRows(addProposal);				
		String filepath = servletContext.getRealPath("");
		proposalDao.addProposal(addProposal, filepath);
		return "redirect:/proposalList";
	}
	
	@RequestMapping(value="/editProposalForm", method=RequestMethod.GET)
	public ModelAndView editProposalForm(HttpServletRequest request) {
		ModelAndView editProposal = new ModelAndView("sales/proposal/editProposal");
		editProposal.addObject("countryList", commonFunction.getAllCountryList());
		ProposalBean editProposalBean = proposalDao.getProposalInformation(request.getParameter("proposal_id"));
		//get the selected proposal values
		editProposal.addObject("editProposal", editProposalBean);
		editProposal.addObject("contactList", proposalDao.getContactList(editProposalBean.getBid().getBidId(), 1));		
		return editProposal;
	}
	
	@RequestMapping(value="/downloadProposalFile", method=RequestMethod.GET)
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		proposalService.downloadFile(request, response);
	}
	
	@RequestMapping(value="/editProposal", method=RequestMethod.POST)
	public String editProposal(@ModelAttribute("editProposal") ProposalBean proposal, ModelMap model){
		proposalService.removeEmptyRows(proposal);
		String filepath = servletContext.getRealPath("");
		proposalDao.editProposal(proposal, filepath);
		return "redirect:/proposalList";
	}
	
	@RequestMapping(value="/downloadPDFProposal", method=RequestMethod.GET)
	public ModelAndView downloadPDFProposal(HttpServletRequest request){
		ModelAndView downloadPDFProposal = new ModelAndView("proposalPdfView");
		String proposalId = request.getParameter("proposalId");
		ProposalBean proposal = proposalDao.getProposalInformation(proposalId);
		downloadPDFProposal.addObject("salesContact", proposalDao.getContactInfo(proposal.getClientContact(), 0));
		downloadPDFProposal.addObject("accountManager", 
				proposalDao.getContactInfo(proposal.getBid().getBidId(), 1 ));
		proposal.setProposalRequirements(proposalDao.getRequirementsForPDF(proposal.getProposalRequirements()));
		downloadPDFProposal.addObject("proposal", proposal);
		downloadPDFProposal.addObject("tAndC", proposalDao.getTermsAndConditions(proposalId));
		return downloadPDFProposal;
	}
}
