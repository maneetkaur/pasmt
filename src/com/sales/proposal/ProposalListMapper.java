package com.sales.proposal;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.bid.BidBean;

public class ProposalListMapper implements RowMapper<ProposalBean>{

	@Override
	public ProposalBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProposalBean proposal = new ProposalBean();
		//pe.employee_name, 
		proposal.setProposalId(rs.getString("pp.proposal_id"));
		proposal.setProposalNumber(rs.getString("pp.proposal_number"));
		proposal.setCreatedOn(rs.getString("date_format(pp.created_on, '%b %e, %Y')"));
		
		BidBean bid = new BidBean();
		bid.setBidName(rs.getString("pb.bid_name"));
		bid.setBidClient(rs.getString("pc.client_name"));
		if(rs.getString("pb.project_type")!=null){
			String projectType=rs.getString("pb.project_type");
			if(projectType.equals("1")){
				projectType = "AdHoc";
			}
			else if (projectType.equals("2")) {
				projectType = "Tracker";
			}
			bid.setProjectType(projectType);
		}
		bid.setAccountManager(rs.getString("pe.employee_name"));		
		proposal.setBid(bid);
		
		return proposal;
	}

}
