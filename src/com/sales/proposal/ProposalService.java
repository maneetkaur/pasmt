package com.sales.proposal;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.commonFunc.CommonFunction;
import com.commonFunc.FileBean;
import com.sales.bid.RequirementBean;

public class ProposalService {
public ProposalBean removeEmptyRows(ProposalBean proposal){
		
		//remove empty requirements rows
		if(proposal.getProposalRequirements().size()==1){
			RequirementBean tempReqmt = proposal.getProposalRequirements().get(0);
			//if all the fields are empty
			if(tempReqmt.getCountry().equals("0") && tempReqmt.getServices().equals("0") &&
					tempReqmt.getTargetAudience().equals("0") && tempReqmt.getSampleSize().equals("") && 
					tempReqmt.getDescription().equals("") && tempReqmt.getIncidence().equals("-1") &&
					tempReqmt.getNoOfQuestions().equals("") && tempReqmt.getLoi().equals("") &&
					tempReqmt.getSetupFee().equals("") && tempReqmt.getQuotedCpi().equals("")){
				proposal.setProposalRequirements(null);
			}
		}
		else{
			List<RequirementBean> tempProposals = new ArrayList<RequirementBean>();
			for(RequirementBean tempReqmt: proposal.getProposalRequirements()){
				//if the row has been removed, every element of the row becomes null. So, check if the first element is not null
				if(tempReqmt.getCountry()!=null){
					if(!(tempReqmt.getCountry().equals("0") && tempReqmt.getServices().equals("0") &&
						tempReqmt.getTargetAudience().equals("0") && tempReqmt.getSampleSize().equals("") && 
						tempReqmt.getDescription().equals("") && tempReqmt.getIncidence().equals("-1") &&
						tempReqmt.getNoOfQuestions().equals("") && tempReqmt.getLoi().equals("") &&
						tempReqmt.getSetupFee().equals("") && tempReqmt.getQuotedCpi().equals(""))){
							tempProposals.add(tempReqmt);							
					}
				}
			}
			proposal.setProposalRequirements(tempProposals);
		}
		
		//remove empty Document rows
		if(proposal.getDocumentList().size()==1){
			FileBean tempFile = proposal.getDocumentList().get(0);
			if(tempFile.getFile().getOriginalFilename().equals("")){				
				proposal.setDocumentList(null);
			}
		}
		else{						
			List<FileBean> tempDocs = new ArrayList<FileBean>();
			for(FileBean tempFile: proposal.getDocumentList()){
				if(tempFile.getFile()!=null && !(tempFile.getFile().getOriginalFilename().equals(""))){
					tempDocs.add(tempFile);
				}
			}
			proposal.setDocumentList(tempDocs);
		}
	return proposal;
	}

	public boolean createDirectory(String filePath){
		boolean status=false;
		File files = new File(filePath);
		if (!files.exists()) {
			if (files.mkdirs()) {
				status = true;
			} else {
				System.out.println("Failed to create directories for " + filePath + " in ProposalService.java!");
			}
		}						
		return status;
	}
	
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		
		ServletContext servletContext = request.getServletContext();
		String path=servletContext.getRealPath("");
		String fileName= request.getParameter("fileName");
		String proposalId = request.getParameter("proposalId");
		String fullPath=path + "/Document/Proposal/" + proposalId + "/"+fileName;
		System.out.println("path: " + fullPath);
		CommonFunction commonFunction = new CommonFunction();
		commonFunction.downloadFile(servletContext, response, fullPath);
	}
}
