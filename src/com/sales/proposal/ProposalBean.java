package com.sales.proposal;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

import com.commonFunc.FileBean;
import com.sales.bid.BidBean;
import com.sales.bid.RequirementBean;

public class ProposalBean {
	private String proposalId, proposalNumber, proposalName, clientContact, clientProjectManager, purchaseOrderNo, 
	targetAudience, notesOnTargeting, startDate, endDate, projectValue, projectMinimum, createdOn, propReqmtSize,
	reqmtIdList, createdBy;
	
	private List<RequirementBean> proposalRequirements;
	private List<FileBean> documentList;
	private List<String> documentNames;
	
	private BidBean bid;
	
	public ProposalBean(){
		documentList = LazyList.decorate(new ArrayList<FileBean>(),
				new InstantiateFactory(FileBean.class));
		
		proposalRequirements = LazyList.decorate(
				new ArrayList<RequirementBean>()
				, new InstantiateFactory(RequirementBean.class));
	}
	
	public String getProposalId() {
		return proposalId;
	}
	public void setProposalId(String proposalId) {
		this.proposalId = proposalId;
	}
	public String getProposalNumber() {
		return proposalNumber;
	}
	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}
	public String getProposalName() {
		return proposalName;
	}
	public void setProposalName(String proposalName) {
		this.proposalName = proposalName;
	}
	public String getClientContact() {
		return clientContact;
	}
	public void setClientContact(String clientContact) {
		this.clientContact = clientContact;
	}
	public String getClientProjectManager() {
		return clientProjectManager;
	}
	public void setClientProjectManager(String clientProjectManager) {
		this.clientProjectManager = clientProjectManager;
	}
	public String getPurchaseOrderNo() {
		return purchaseOrderNo;
	}
	public void setPurchaseOrderNo(String purchaseOrderNo) {
		this.purchaseOrderNo = purchaseOrderNo;
	}
	public String getTargetAudience() {
		return targetAudience;
	}
	public void setTargetAudience(String targetAudience) {
		this.targetAudience = targetAudience;
	}
	public String getNotesOnTargeting() {
		return notesOnTargeting;
	}
	public void setNotesOnTargeting(String notesOnTargeting) {
		this.notesOnTargeting = notesOnTargeting;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getProjectValue() {
		return projectValue;
	}
	public void setProjectValue(String projectValue) {
		this.projectValue = projectValue;
	}
	public String getProjectMinimum() {
		return projectMinimum;
	}
	public void setProjectMinimum(String projectMinimum) {
		this.projectMinimum = projectMinimum;
	}
	public List<RequirementBean> getProposalRequirements() {
		return proposalRequirements;
	}
	public void setProposalRequirements(List<RequirementBean> proposalRequirements) {
		this.proposalRequirements = proposalRequirements;
	}
	public List<FileBean> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<FileBean> documentList) {
		this.documentList = documentList;
	}
	public List<String> getDocumentNames() {
		return documentNames;
	}
	public void setDocumentNames(List<String> documentNames) {
		this.documentNames = documentNames;
	}
	public BidBean getBid() {
		return bid;
	}
	public void setBid(BidBean bid) {
		this.bid = bid;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getPropReqmtSize() {
		return propReqmtSize;
	}
	public void setPropReqmtSize(String propReqmtSize) {
		this.propReqmtSize = propReqmtSize;
	}
	public String getReqmtIdList() {
		return reqmtIdList;
	}
	public void setReqmtIdList(String reqmtIdList) {
		this.reqmtIdList = reqmtIdList;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}
