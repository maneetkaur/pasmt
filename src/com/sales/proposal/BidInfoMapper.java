package com.sales.proposal;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.bid.BidBean;

public class BidInfoMapper implements RowMapper<ProposalBean>{

	@Override
	public ProposalBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		String temp;
		ProposalBean proposal = new ProposalBean();
		BidBean bid = new BidBean();
		bid.setBidCode(rs.getString("pb.bid_id"));
		bid.setBidClient(rs.getString("pc.client_name"));
		bid.setBidName(rs.getString("pb.bid_name"));		
		bid.setBillingCurrency(rs.getString("pb.billing_currency_id"));
		bid.setAccountManager(rs.getString("pe.employee_name"));
		bid.setProjectSpecs(rs.getString("pb.project_specs"));
		bid.setBidDate(rs.getString("date_format(pb.bid_date, '%b %e, %Y')"));
		
		temp = rs.getString("pb.project_type");
		if(temp.equals("1")){
			temp = "AdHoc";
		}
		else if(temp.equals("2")){
			temp = "Tracker";
		}
		bid.setProjectType(temp);
		
		temp = rs.getString("pb.product_type");
		if(temp.equals("1")){
			temp = "Full Service";
		}
		else if(temp.equals("2")){
			temp = "IHUT";
		}
		else if (temp.equals("3")) {
			temp = "Panel Building";
		}
		else if (temp.equals("4")) {
			temp = "Program & Host";
		}
		else if (temp.equals("5")) {
			temp = "Sample Only";
		}
		bid.setProductType(temp);
		proposal.setBid(bid);
		return proposal;
	}
	
}
