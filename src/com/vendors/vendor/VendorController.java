package com.vendors.vendor;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class VendorController {
	@Autowired
	VendorDao vendorDao;
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	CommonFunction commonFunction;
	
	VendorService service = new VendorService();
	
	@RequestMapping(value="/vendorList")
	public ModelAndView vendorList(HttpServletRequest request){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView vendor = new ModelAndView("vendors/vendor/vendorList");
		List<VendorBean> vendorList = vendorDao.vendorList(request, startIndex, 15);
		vendor.addObject("searchVendor", new VendorBean());
		vendor.addObject("vendorList", vendorList);
		vendorList=null;
		return vendor;
	}
	
	@RequestMapping(value="/addVendorForm", method= RequestMethod.GET)
	public ModelAndView addVendorForm(HttpServletRequest request){
		ModelAndView addVendor = new ModelAndView("vendors/vendor/addVendor");
		addVendor.addObject("countryList", commonFunction.getAllCountryList());		
		addVendor.addObject("managerList", commonFunction.getManagerList());
		addVendor.addObject("billCurrencyList", commonFunction.getBillingCurrency());
		/*get vendor type list from generic master*/
		addVendor.addObject("vendorTypeList", vendorDao.vendorTypeList());
		
		VendorBean vendor = new VendorBean();
		/* get terms and conditions from the database */
		vendor.settAndC(vendorDao.getTermsAndConditions());
		addVendor.addObject("addVendor", vendor);
		return addVendor;
	}
	
	@RequestMapping(value="/addVendor", method=RequestMethod.POST)
	public String addVendor(@ModelAttribute("addVendor") VendorBean vendor, ModelMap model) {
		service.removeEmptyRows(vendor);
		String filePath = servletContext.getRealPath("");
		vendorDao.addVendor(vendor, filePath);
		return "redirect:/vendorList";
	}
	
	@RequestMapping(value="/validateVendorName", method=RequestMethod.GET)
	public String validateVendorName(HttpServletRequest request, ModelMap model) {
		int checkName = vendorDao.validateVendorName(request.getParameter("vendorName"));
		model.addAttribute("checkName", checkName);
		return "ajaxOperation/ajaxOperationVendors";
	}
	
	@RequestMapping(value="/validateVendorCode", method=RequestMethod.GET)
	public String validateVendorCode(HttpServletRequest request, ModelMap model){
		int checkCode = vendorDao.validateVendorCode(request.getParameter("vendorCode"));
		model.addAttribute("checkCode", checkCode);
		return "ajaxOperation/ajaxOperationVendors";
	}
	
	@RequestMapping(value="/validateVendorUrl", method=RequestMethod.GET)
	public String validateVendorUrl(HttpServletRequest request, ModelMap model){
		int checkUrl = vendorDao.validateWebUrl(request.getParameter("webUrl"));
		model.addAttribute("checkUrl", checkUrl);
		return "ajaxOperation/ajaxOperationVendors";
	}

	@RequestMapping(value="/editVendorForm", method=RequestMethod.GET)
	public ModelAndView editVendorForm(HttpServletRequest request){
		ModelAndView editVendor = new ModelAndView("vendors/vendor/editVendor");
		editVendor.addObject("countryList", commonFunction.getAllCountryList());		
		editVendor.addObject("managerList", commonFunction.getManagerList());
		editVendor.addObject("billCurrencyList", commonFunction.getBillingCurrency());
		/*get vendor type list from generic master*/
		editVendor.addObject("vendorTypeList", vendorDao.vendorTypeList());
		String vendorId = request.getParameter("vendorId");
		VendorBean vendor = vendorDao.getVendorInformation(vendorId);
		editVendor.addObject("editVendor", vendor);
		editVendor.addObject("vendorTypeMap", service.getVendorTypeMap(vendor.getVendorType()));
		editVendor.addObject("selectedCurrency", vendorDao.getSelectedCurrency(vendorId));
		/*
		 * Get the tracking info in different beans and set in different forms
		 * Set the hidden field in each form having vendor id
		 */
		editVendor.addObject("editVendorRecruit", vendorDao.getTrackingInformation(vendorId, 1));
		editVendor.addObject("editVendorOsbt", vendorDao.getTrackingInformation(vendorId, 2));
		editVendor.addObject("editVendorSurvey", vendorDao.getTrackingInformation(vendorId, 3));
		/* Get the category and sub category list from database
		 */
		editVendor.addObject("categoryList", commonFunction.getSurveyCategories());
		return editVendor;
	}
	
	@RequestMapping(value="/downloadVendorFile", method=RequestMethod.GET)
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		service.downloadFile(request, response);
	}
	
	@RequestMapping(value="/editVendor", method=RequestMethod.POST)
	public String editVendor(@ModelAttribute("editVendor") VendorBean vendor, ModelMap model){
		service.removeEmptyRows(vendor);
		String filePath = servletContext.getRealPath("");
		vendorDao.editVendor(vendor, filePath);
		return "redirect:/vendorList";
	}
	
	@RequestMapping(value="/editVendorBill", method=RequestMethod.POST)
	public String editVendorBill(@ModelAttribute("editVendor") VendorBean vendor, ModelMap model){
		vendorDao.editBillingInfo(vendor);
		return "redirect:/editVendorForm?vendorId=" + vendor.getVendorId() + "#tab-2";
	}
	
	@RequestMapping(value="/editVendorRecruit", method=RequestMethod.POST)
	public String editVendorRecruit(@ModelAttribute("editVendorRecruit") TrackingBean recruitment, ModelMap model){
		vendorDao.editRecruitmentTracking(recruitment);
		return "redirect:/editVendorForm?vendorId=" + recruitment.getVendorId() + "#tab-3";
	}
	
	@RequestMapping(value="/editVendorOsbt", method=RequestMethod.POST)
	public String editVendorOsbt(@ModelAttribute("editVendorOsbt") TrackingBean osbtTracking, ModelMap model){
		vendorDao.editOsbtTracking(osbtTracking);
		return "redirect:/editVendorForm?vendorId=" + osbtTracking.getVendorId() + "#tab-4";
	}
	
	@RequestMapping(value="/editVendorSurvey", method=RequestMethod.POST)
	public String editVendorSurvey(@ModelAttribute("editVendorSurvey") TrackingBean surveyTracking, ModelMap model){
		vendorDao.editSurveyTracking(surveyTracking);
		return "redirect:/editVendorForm?vendorId=" + surveyTracking.getVendorId() + "#tab-5";
	}
}
