package com.vendors.vendor;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.commonFunc.CommonFunction;
import com.sales.client.ContactBean;

public class VendorService {
	public VendorBean removeEmptyRows(VendorBean vendor) {
		//remove empty contact rows
		if(vendor.getAdditionalContacts().size()==1){
			ContactBean tempContact = vendor.getAdditionalContacts().get(0);
			if(tempContact.getName().equals("")){
				vendor.setAdditionalContacts(null);
			}
		}
		
		if(vendor.getAdditionalContacts()!=null){
			List<ContactBean> tempContacts = new ArrayList<ContactBean>();
			for(ContactBean contact: vendor.getAdditionalContacts()){
				if (contact.getName()!=null && !(contact.getName().equals(""))) {
					tempContacts.add(contact);
				}
			}
			vendor.setAdditionalContacts(tempContacts);
		}
		return vendor;
	}
	
	public boolean createDirectory(String filePath){
		boolean status=false;
		File files = new File(filePath);
		if (!files.exists()) {
			if (files.mkdirs()) {
				status = true;
			} else {
				System.out.println("Failed to create directories for " + filePath + " in VendorService.java!");
			}
		}						
		return status;
	}
	
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		ServletContext servletContext = request.getServletContext();
		String path=servletContext.getRealPath("");
		//String path = servletContext.getContextPath();
		String fileName= request.getParameter("fileName");
		String vendorId = request.getParameter("vendorId");
		String documentType = request.getParameter("documentType");
		if(documentType.equals("36")){
			documentType="VendorForm";
		}
		else if (documentType.equals("37")) {
			documentType="Guideline";
		}
		else if (documentType.equals("38")) {
			documentType="PanelBook";
		}
		else if (documentType.equals("39")) {
			documentType="CompanyBrochure";
		}
		else if (documentType.equals("40")) {
			documentType="Esomar";
		}
		else if (documentType.equals("41")) {
			documentType="SignedIO";
		}
		String fullPath=path+"/Document/Vendor/VendorDocs/" + vendorId + "/" + documentType + "/"+fileName;
		//System.out.println("complete path: " + fullPath);
		CommonFunction commonFunction = new CommonFunction();
		commonFunction.downloadFile(servletContext, response, fullPath);
	}
	
	public Map<String, String> getVendorTypeMap(String[] vTypeArray){
		Map<String, String> tempMap = new HashMap<String, String>();
		for(int i=0; i<vTypeArray.length; i++){
			tempMap.put(vTypeArray[i], vTypeArray[i]);
		}
		return tempMap;
		
		/*
		 * 
		in jsp:
		<option value="${vType.id}" <c:if test="${not empty testMap[vType.id]}">selected</c:if> >${vType.name}</option>
		 */
	}
}
