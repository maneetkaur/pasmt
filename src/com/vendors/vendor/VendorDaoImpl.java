package com.vendors.vendor;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.commonFunc.CommonBean;
import com.sales.client.ContactBean;
import com.sales.client.ContactRowMapper;

@Repository
@Transactional
public class VendorDaoImpl implements VendorDao{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<VendorBean> vendorList(HttpServletRequest request,
			int startIndex, int range) {
		boolean whereStatus=false;
		String sql = "SELECT pv.vendor_id, pv.vendor_name, pv.vendor_code, lc.country_name, pc.contact_name FROM " +
				"pasmt_vendor pv" +
				" JOIN lu_country lc ON pv.country_id=lc.country_id JOIN pasmt_contacts pc ON " +
				"pc.source_id=pv.vendor_id ";
		StringBuffer where_clause = new StringBuffer("WHERE pc.source_type=2 AND pc.contact_type=1 AND ");
		if(request.getParameter("vendorName")!=null && !(request.getParameter("vendorName").equals(""))){
			whereStatus = true;
			where_clause.append("pv.vendor_name like '%" + request.getParameter("vendorName") + "%' AND ");
		}
		if(request.getParameter("vendorCode")!=null && !(request.getParameter("vendorCode").equals(""))){
			whereStatus=true;
			where_clause.append("pv.vendor_code like '%" + request.getParameter("vendorCode") + "%' AND ");
		}
		if(request.getParameter("vendorStatus")!=null){
			String status = request.getParameter("vendorStatus");
			if(status.equals("1")){
				where_clause.append("pv.vendor_status=1 AND ");
			}
			else if (status.equals("2")) {
				where_clause.append("pv.vendor_status=2 AND ");
			}
			else if(status.equals("3")){
				where_clause.append("pv.vendor_status=3 AND ");
			}
			else if(status.equals("NONE")){
				if(!whereStatus){
					where_clause.append("pv.vendor_status=1 AND ");
				}
			}
		}
		//if the request parameter is null, then listing is called for the first time and status should be active 
		//by default
		else{
			where_clause.append("pv.vendor_status=1 AND ");
		}
		
		String where=where_clause.toString().trim();
		where_clause=null;
		//remove the extra AND form the end
		where=where.substring(0, where.length()-4);
		
		//calculate count for pagination
		String sql_count = "SELECT count(1) FROM pasmt_vendor pv JOIN lu_country lc ON pv.country_id=lc.country_id" +
				" JOIN pasmt_contacts pc ON pc.source_id=pv.vendor_id ";
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);
		request.setAttribute("total_rows", total_rows);
		
		String limit = " ORDER BY pv.vendor_id LIMIT " + startIndex + ", " + range;
		sql = sql + where + limit;
		List<VendorBean> vendorList = jdbcTemplate.query(sql, new VendorListMapper());
		return vendorList;
	}

	@Override
	public List<CommonBean> vendorTypeList() {
		String sql = "SELECT generic_value_id, generic_value FROM lu_generic_values WHERE generic_key='Vent'";
		return jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean vendorType = new CommonBean();
				vendorType.setId(rs.getString("generic_value_id"));
				vendorType.setName(rs.getString("generic_value"));
				return vendorType;
			}			
		});
	}

	@Override
	public String getTermsAndConditions() {
		String sql = "SELECT terms_and_conditions FROM pasmt_t_and_c WHERE source_type=2 AND type=0 AND status=1";
		return jdbcTemplate.queryForObject(sql, String.class);
	}

	@Override
	public void addVendor(VendorBean vendor, String filePath) {
		try{
			StringBuffer sb;
			String sql = "INSERT INTO pasmt_vendor (vendor_name, vendor_code, vendor_type, address, city, state, " +
					"postal_code, country_id, website_url, phone_number, fax_number, rfq_email, irb_account_manager" +
					", vendor_status, created_on) VALUES " +
					"(?,?,?,?,?,?, ?,?,?,?,?,?,?, ?,?)";
			
			String vendorTypeString = ("" + 
					Arrays.asList(vendor.getVendorType())).replaceAll("(^.|.$)", "").replace(", ", "," );
			jdbcTemplate.update(sql, new Object[]{vendor.getVendorName(), vendor.getVendorCode(), vendorTypeString,
				vendor.getAddress(), vendor.getCity(), vendor.getState(), vendor.getPostalCode(), vendor.getCountry(),
				vendor.getWebsiteUrl(), vendor.getPhoneNo(), vendor.getFaxNo(), vendor.getRfqEmail(),
				vendor.getAccountManager(), vendor.getVendorStatus(), new Date()});
			
			//Get the vendor id of the latest entry
			sql = "SELECT MAX(vendor_id) FROM pasmt_vendor WHERE vendor_code ='" + vendor.getVendorCode() + "'";
			String vendorId = jdbcTemplate.queryForObject(sql, String.class);
			
			//Insert the primary contact information into the database
			ContactBean primContact = vendor.getPrimaryContact();
			sql="INSERT INTO pasmt_contacts (source_id, contact_type, contact_name, job_title, email_id, phone_no, " +
					"ext_no, source_type, created_on) VALUES (" + vendorId + ",1, '" + primContact.getName() + "', '" + 
					primContact.getJobTitle() + "', '" + primContact.getEmail() + "', '" + primContact.getPhoneNo() + "', '"
					 + primContact.getExtno() + "',2, now())";
			jdbcTemplate.update(sql);
			
			//Insert the additional contacts information, if any
			if(vendor.getAdditionalContacts()!=null){
				List<ContactBean> addContacts = vendor.getAdditionalContacts();		
						
				if (addContacts.size()>0) {
					sb = new StringBuffer("INSERT INTO pasmt_contacts (source_id, contact_name, job_title, email_id, " +
							"phone_no, ext_no, source_type, created_on, contact_type) VALUES");
					for(ContactBean contact: addContacts){
						sb = sb.append("(" + vendorId + ", '"+ contact.getName() + "', '" + contact.getJobTitle() + 
								"', '" + contact.getEmail() + "', '" + contact.getPhoneNo() + "', '" + contact.getExtno() +
								"',2, now(),2), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}		
					jdbcTemplate.update(sql);
				}
			}
			
			//Add terms and conditions information into the database
			sql = "INSERT INTO pasmt_t_and_c (source_id, source_type, terms_and_conditions, type, created_on, status)" +
					" VALUES (" + vendorId + ",2, '" + vendor.gettAndC() + "',1, now(),1)";
			jdbcTemplate.update(sql);
			
			//Create the directory structure for the documents
			String path = filePath + "/Document/Vendor/VendorDocs/" + vendorId;		
			addDocuments(vendor.getVendorForm(), path + "/VendorForm", vendorId, 36, 0);
			addDocuments(vendor.getGuidelineDocs(), path + "/Guideline", vendorId, 37, 0);
			addDocuments(vendor.getPanelBook(), path + "/PanelBook", vendorId, 38, 0);
			addDocuments(vendor.getCompanyBrochure(), path + "/CompanyBrochure", vendorId, 39, 0);
			addDocuments(vendor.getEsomar(), path + "/Esomar", vendorId, 40, 0);
			addDocuments(vendor.getSignedIO(), path + "/SignedIO", vendorId, 41, 0);
		}
		catch(Exception e){
			System.out.println("Exception in addVendor of VendorDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	public void addDocuments(CommonsMultipartFile file, String path, String vendorId, int documentType, int from){
		try{
			//Check if there is any existing file
			if(file.getOriginalFilename()!=""){
				VendorService service = new VendorService();
				service.createDirectory(path);
				String extension, name, sql="";
				int dotIndex;
				File destination;
				name = file.getOriginalFilename();						
				dotIndex = name.lastIndexOf(".");
				extension = name.substring(dotIndex+1, name.length());			
				name = name.substring(0, dotIndex);	
					
				destination = new File(path + "/" + name + "." + extension);			
				try {
					file.transferTo(destination);
					if(from==0){
						sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, " +
							"document_source_type, created_on)" +
							" VALUES(" + vendorId + ", '" + extension + "', " + documentType + ", '" + name + "',3, now())";
					}
					else if(from==1){
						sql = "SELECT count(1) FROM pasmt_document WHERE document_source_id =" + vendorId + 
								" AND document_type =" + documentType + " AND document_source_type=3";
						int count = jdbcTemplate.queryForInt(sql);
						if(count==0){
							sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, "
									+ "document_source_type, created_on) VALUES(" + vendorId + ", '" + extension + "', " + 
									documentType + ", '" + name + "',3, now())";
						}
						else{
							sql = "UPDATE pasmt_document SET extension ='" + extension + "',document_name='" + name +
									"' WHERE document_source_id =" + vendorId + " AND document_type=" + documentType + 
									" AND document_source_type=3";
						}
					}
					jdbcTemplate.update(sql);
				} catch (IllegalStateException e) {
					System.out.println("Illegal state Exception in addDocuments of VendorDaoImpl.java"+e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("IO Exception in addDocuments of VendorDaoImpl.java" + e.toString());
					e.printStackTrace();
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in addDocuments of VendorDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	@Override
	public int validateVendorName(String vendorName) {
		int target = -1;
		
		String sql = "SELECT count(1) FROM pasmt_vendor WHERE vendor_name='" + vendorName + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateVendorName of VendorDaoImpl.java");
		}
		return target;
	}

	@Override
	public int validateVendorCode(String vendorCode) {
		int target = -1;
		
		String sql = "SELECT count(1) FROM pasmt_vendor WHERE vendor_code='" + vendorCode + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateVendorCode of VendorDaoImpl.java");
		}
		return target;
	}

	@Override
	public int validateWebUrl(String webUrl) {
		int target = -1;
		
		String sql = "SELECT count(1) FROM pasmt_vendor WHERE website_url='" + webUrl + "'";
		try{
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				target=0;
			}
			else{
				target=1;
			}
		}
		catch(Exception e){
			System.out.println("Exception in validateWebUrl of VendorDaoImpl.java");
		}
		return target;
	}

	@Override
	public VendorBean getVendorInformation(String vendorId) {
		String sql = "SELECT vendor_id, vendor_name, vendor_code, vendor_type, address, city, state, postal_code," +
				" country_id, website_url, phone_number, fax_number, rfq_email, payment_type, payment_mode," +
				" bank_name, beneficiary_name, account_number, ifsc_code, iban_number, swift_code, sort_code," +
				" billing_currency_id, bank_physical_address, intermediary_bank_name, intermediary_beneficiary_name,  "
				+ "intermediary_bank_address, intermediary_bank_acc_no, intermediary_bank_iban, " +
				"intermediary_bank_swift_code, intermediary_bank_sort_code, paypal_email, pan_number, " +
				"service_tax_number, tan, vat_number, account_receivable_email, irb_account_manager, vendor_status," +
				"date_format(created_on, '%b %e, %Y')  FROM pasmt_vendor WHERE vendor_id=" + vendorId;
		VendorBean vendor = (VendorBean)jdbcTemplate.queryForObject(sql, new EditVendorMapper());
		/*Get primary contact information from the database*/
		sql = "SELECT contacts_id, contact_name, job_title, email_id, phone_no, ext_no FROM " +
				"pasmt_contacts WHERE source_id=" + vendorId + " AND contact_type=1 AND source_type=2";		
		//get the list and then get the first element of list to avoid issues with queryForObject
		List<ContactBean> primContactList = jdbcTemplate.query(sql, new ContactRowMapper());
		if(primContactList!=null && primContactList.size()>0){
			if(primContactList.get(0)!=null){
				vendor.setPrimaryContact(primContactList.get(0));
			}
		}
		/*Get additional contact information from the database*/
		sql = "SELECT contacts_id, contact_name, job_title, email_id, phone_no, ext_no " +
				"FROM pasmt_contacts WHERE source_id=" + vendorId + " AND contact_type=2 AND source_type=2" +
				" ORDER BY contacts_id";
		
		List<ContactBean> contactList = jdbcTemplate.query(sql, new ContactRowMapper());
		vendor.setAdditionalContacts(contactList);
		//Set additional contacts current size in bean
		vendor.setAddContactsSize(String.valueOf(vendor.getAdditionalContacts().size()));
		//Get document names
		vendor.setVendorFormName(getDocumentName(vendorId, 36));
		vendor.setGuidelineName(getDocumentName(vendorId, 37));
		vendor.setPanelBookName(getDocumentName(vendorId, 38));
		vendor.setBrochureName(getDocumentName(vendorId, 39));
		vendor.setEsomarName(getDocumentName(vendorId, 40));
		vendor.setSignedIOName(getDocumentName(vendorId, 41));
		
		//get terms and conditions value from the database
		sql = "SELECT terms_and_conditions FROM pasmt_t_and_c WHERE source_id=" + vendorId + " AND source_type=2 AND" +
				" type=1";
		List<String> tAndCList = jdbcTemplate.query(sql, new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("terms_and_conditions");
			}			
		});
		if(tAndCList!=null && tAndCList.size()>0 && tAndCList.get(0)!=null){
			vendor.settAndC(tAndCList.get(0));
		}
		return vendor;
	}
	
	public String getDocumentName(String vendorId, int documentType) {
		String docName = "";
		String sql = "SELECT extension, document_name FROM pasmt_document WHERE document_source_id=" + vendorId
				+ " AND document_type=" + documentType + " AND document_source_type=3";
		List<String> documentList = jdbcTemplate.query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				String name;
				name = rs.getString("document_name") + "." + rs.getString("extension");
				return name;
			}
			
		});
		if(documentList!=null && documentList.size()>0 && documentList.get(0)!=null){
			docName = documentList.get(0);
		}
		return docName;
	}

	@Override
	public void editVendor(VendorBean vendor, String filePath) {
		try{
			StringBuffer sb;
			String sql = "UPDATE pasmt_vendor SET vendor_name=?, vendor_code=?, vendor_type=?, address=?, city=?, " +
					"state=?, postal_code=?, country_id=?, website_url=?, phone_number=?, fax_number=?, rfq_email=?, " +
					" irb_account_manager=?, vendor_status=? WHERE vendor_id=?";
			
			String vendorTypeString = ("" + 
					Arrays.asList(vendor.getVendorType())).replaceAll("(^.|.$)", "").replace(", ", "," );
			jdbcTemplate.update(sql, new Object[]{vendor.getVendorName(), vendor.getVendorCode(), vendorTypeString,
				vendor.getAddress(), vendor.getCity(), vendor.getState(), vendor.getPostalCode(), vendor.getCountry(),
				vendor.getWebsiteUrl(), vendor.getPhoneNo(), vendor.getFaxNo(), vendor.getRfqEmail(), 
				vendor.getAccountManager(), vendor.getVendorStatus(), vendor.getVendorId()});
			
			String vendorId = vendor.getVendorId();
			
			//Update the primary contact information
			ContactBean primContact = vendor.getPrimaryContact();
			sql = "UPDATE pasmt_contacts SET contact_name='" + primContact.getName() + "', job_title='" + 
					primContact.getJobTitle() + "', email_id ='"+ primContact.getEmail() + "', phone_no='" + 
					primContact.getPhoneNo() +"', ext_no='" + primContact.getExtno() + "' WHERE source_id =" + 
					vendorId + " AND contact_type=1 AND source_type=2";
			jdbcTemplate.update(sql);
			
			//Update Additional contacts information
			if(vendor.getAdditionalContacts()!=null){
				int currentSize = Integer.parseInt(vendor.getAddContactsSize());
				List<ContactBean> addContacts = vendor.getAdditionalContacts();
				ContactBean newContact;
				
				//Update the existing rows if there are changes in any
				if(vendor.getContactIdList().length()>0){
					String contactArray[] = vendor.getContactIdList().split(",");			
					if(currentSize>0 && contactArray.length>0){
						for(int i=0; i<contactArray.length; i++){
							newContact = addContacts.get(Integer.parseInt(contactArray[i]));
							sql = "UPDATE pasmt_contacts SET contact_name='" + newContact.getName() + 
									"', job_title='" + newContact.getJobTitle() + "', email_id ='"+ newContact.getEmail()
									+ "', phone_no='" + newContact.getPhoneNo() +"', ext_no='" + newContact.getExtno() + 
									"' WHERE contacts_id=" + newContact.getId();
							jdbcTemplate.update(sql);
						}
					}
				}
				//Insert the newly added contacts
				if(addContacts.size()> currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_contacts (source_id, contact_name, job_title, email_id, " +
							"phone_no, ext_no, created_on, contact_type, source_type) VALUES");
					for(int i=currentSize; i<addContacts.size(); i++){
						ContactBean contact = addContacts.get(i);
						sb = sb.append("(" + vendorId + ", '"+ contact.getName() + "', '" + contact.getJobTitle() + 
								"', '" + contact.getEmail() + "', '" + contact.getPhoneNo() + "', '" + contact.getExtno() +
								"', now(),2,2), ");
					}
					
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}			
					jdbcTemplate.update(sql);
				}
			}
			
			//Update the terms and conditions details
			sql = "UPDATE pasmt_t_and_c SET terms_and_conditions='" + vendor.gettAndC() + "' WHERE source_id=" + 
			vendorId + " AND source_type=2 AND type=1";
			jdbcTemplate.update(sql);
			
			//Replace or add documents as required
			String path = filePath + "/Document/Vendor/VendorDocs/" + vendorId;
			if(vendor.getVendorForm()!=null){
				addDocuments(vendor.getVendorForm(), path + "/VendorForm", vendorId, 36, 1);
			}
			if(vendor.getGuidelineDocs()!=null){
				addDocuments(vendor.getGuidelineDocs(), path + "/Guideline", vendorId, 37, 1);
			}
			if(vendor.getPanelBook()!=null){
				addDocuments(vendor.getPanelBook(), path + "/PanelBook", vendorId, 38, 1);
			}
			if(vendor.getCompanyBrochure()!=null){
				addDocuments(vendor.getCompanyBrochure(), path + "/CompanyBrochure", vendorId, 39, 1);
			}
			if(vendor.getEsomar()!=null){
				addDocuments(vendor.getEsomar(), path + "/Esomar", vendorId, 40, 1);
			}
			if(vendor.getSignedIO()!=null){
				addDocuments(vendor.getSignedIO(), path + "/SignedIO", vendorId, 41, 1);
			}
		}
		catch(Exception e){
			System.out.println("Exception in editVendor of VendorDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.vendors.vendor.VendorDao#editBillingInfo(com.vendors.vendor.VendorBean)
	 * 
	 */
	@Override
	public void editBillingInfo(VendorBean vendor) {
		try{
			String sql = "UPDATE pasmt_vendor SET payment_type=?, billing_currency_id=?, payment_mode=?, bank_name=?" +
					", beneficiary_name=?, account_number=?, ifsc_code=?, iban_number=?, swift_code=?, sort_code=?, " +
					"bank_physical_address=?, intermediary_bank_name=?, intermediary_beneficiary_name=?, " +
					"intermediary_bank_address=?, intermediary_bank_acc_no=?, intermediary_bank_iban=?, " +
					"intermediary_bank_swift_code=?, intermediary_bank_sort_code=?, paypal_email=?, pan_number=?, " +
					"service_tax_number=?, tan=?, vat_number=?, account_receivable_email=? where vendor_id=?";
			
			jdbcTemplate.update(sql, new Object[]{vendor.getPaymentType(), vendor.getBillingCurrency(), 
					vendor.getPaymentMode(), vendor.getBankName(), vendor.getBeneficiaryName(), vendor.getAccountNo(),
					vendor.getIfscCode(), vendor.getIbanNo(), vendor.getSwiftCode(), vendor.getSortCode(), 
					vendor.getBankAddress(), vendor.getIntBankName(), vendor.getIntBeneficiaryName(), 
					vendor.getIntBankAddress(), vendor.getIntAccountNo(), vendor.getIntIbanNo(), vendor.getIntSwiftCode(),
					vendor.getIntSortCode(), vendor.getPaypalEmail(), vendor.getPanNo(), vendor.getServiceTaxNo(), 
					vendor.getTan(), vendor.getVatNo(), vendor.getAccountRecEmail(), vendor.getVendorId()});
		}
		catch(Exception e){
			System.out.println("Exception in editBillingInfo of VendorDaoImpl.java: " + e);
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.vendors.vendor.VendorDao#editRecruitmentTracking(com.vendors.vendor.TrackingBean)
	 * This function checks if the tracking information exists or not in the database.
	 * If it exists, then the current information is updated, else new information is inserted in database
	 */
	@Override
	public void editRecruitmentTracking(TrackingBean recruitmentTracking) {
		try{
			String sql = "SELECT count(1) FROM vendor_tracking_method WHERE vendor_id=" + recruitmentTracking.getVendorId()
					+ " AND tracking_method_type=1";
			int count = jdbcTemplate.queryForInt(sql);
			if(count==0){
				sql = "INSERT INTO vendor_tracking_method (vendor_id, tracking_method_type, tracking_type, cpa, " +
						"url_code, success_conversion_page, generate_co_reg_url, created_on) VALUES (" + 
						recruitmentTracking.getVendorId() + ",1," + recruitmentTracking.getTrackingType() + ",'" + 
						recruitmentTracking.getCpa() + "', '" + recruitmentTracking.getUrlCode() + "'," + 
						recruitmentTracking.getSuccessPage() + "," + recruitmentTracking.getGenerateCoReg() + ", now())";
				jdbcTemplate.update(sql);
				
				//Get the id of the last insertion
				sql = "SELECT vendor_tracking_method_id FROM vendor_tracking_method WHERE vendor_id=" + 
						recruitmentTracking.getVendorId() + " AND tracking_method_type=1";
				String trackingMethodId = jdbcTemplate.queryForObject(sql, String.class);
				
				//Insert the variable value
				sql = "INSERT INTO vendor_tracking_variables (vendor_tracking_method_id, variable_name, created_on) " +
						"VALUES(?,?,?)";
				jdbcTemplate.update(sql, new Object[]{trackingMethodId, 
						recruitmentTracking.getVariables().get(0).getVariableName(), new Date()});
			}
			else{
				sql = "UPDATE vendor_tracking_method SET tracking_type=" + recruitmentTracking.getTrackingType() + ", cpa='"
						+ recruitmentTracking.getCpa() + "', url_code='" + recruitmentTracking.getUrlCode() + "', " +
						"success_conversion_page=" + recruitmentTracking.getSuccessPage() + ", generate_co_reg_url=" +
						recruitmentTracking.getGenerateCoReg() + " WHERE vendor_tracking_method_id=" +
						recruitmentTracking.getTrackingMethodId();
				jdbcTemplate.update(sql);
				
				VariableBean variable = recruitmentTracking.getVariables().get(0);
				//vendor_tracking_variables_id
				sql = "UPDATE vendor_tracking_variables SET variable_name='" + variable.getVariableName() + "' WHERE " +
						"vendor_tracking_variables_id=" + variable.getVariableId();
				jdbcTemplate.update(sql);
			}
		}
		catch(Exception e){
			System.out.println("Exception in editRecruitmentTracking of VendorDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public void editOsbtTracking(TrackingBean osbtTracking) {
		try{
			String sql = "SELECT count(1) FROM vendor_tracking_method WHERE vendor_id=" + osbtTracking.getVendorId()
					+ " AND tracking_method_type=2";
			int count = jdbcTemplate.queryForInt(sql);
			String audienceTypeString = ("" + 
				Arrays.asList(osbtTracking.getAudienceType())).replaceAll("(^.|.$)", "").replace(", ", "," );
			/*
			 * Check if the values with the data-type integer and decimal in database have blanks, then assign them
			 * to null so as to avoid 'incorrect column value' exception
			 */
			if(osbtTracking.getCpa().equals("")){
				osbtTracking.setCpa(null);
			}
			if(osbtTracking.getMaxRouted().equals("")){
				osbtTracking.setMaxRouted(null);
			}
			if(osbtTracking.getMaxQualified().equals("")){
				osbtTracking.setMaxQualified(null);
			}
			if(osbtTracking.getMinConversion().equals("")){
				osbtTracking.setMinConversion(null);
			}
			if(osbtTracking.getMaxLoi().equals("")){
				osbtTracking.setMaxLoi(null);
			}
			/*
			 * If routing is not allowed, then set the max. no. of routed surveys and max. no. of qualified surveys
			 * to null, to avoid exceptions which might have been missed
			 */
			if(!osbtTracking.getRoutingAllowed().equals("1")){
				osbtTracking.setMaxRouted(null);
				osbtTracking.setMaxQualified(null);
			}
			/*
			 * If the tracking type is pixel, then the fields for server and Url tracking should be null and vice versa
			 */
			String trackingType = osbtTracking.getTrackingType();
			if(trackingType.equals("1")){
				osbtTracking.setUrlCode(null);
				osbtTracking.setCompleteUrl(null);
				osbtTracking.setTerminateUrl(null);
				osbtTracking.setQuotaFullUrl(null);
				osbtTracking.setSecurityTerminateUrl(null);
				osbtTracking.setErrorsUrl(null);
			}
			else if(trackingType.equals("2")){
				osbtTracking.setUrlCode(null);
			}
			else if (trackingType.equals("3") || trackingType.equals("4")) {
				osbtTracking.setCompleteUrl(null);
				osbtTracking.setTerminateUrl(null);
				osbtTracking.setQuotaFullUrl(null);
				osbtTracking.setSecurityTerminateUrl(null);
				osbtTracking.setErrorsUrl(null);
			}
			
			if(count==0){
				sql = "INSERT INTO vendor_tracking_method (vendor_id, tracking_method_type, tracking_type, cpa, " +
						"osbt_signup_allowed, url_code, complete_page_url, terminate_page_url, quotafull_page_url, " +
						"security_terminate_page_url, error_page_url, created_on) VALUES (?,?,?,?, ?,?,?,?,?, ?,?,?)";
				jdbcTemplate.update(sql, new Object[]{osbtTracking.getVendorId(), 2, osbtTracking.getTrackingType(),
						osbtTracking.getCpa(), osbtTracking.getSignUpAllowed(), osbtTracking.getUrlCode(),
						osbtTracking.getCompleteUrl(), osbtTracking.getTerminateUrl(), osbtTracking.getQuotaFullUrl(),
						osbtTracking.getSecurityTerminateUrl(), osbtTracking.getErrorsUrl(), new Date()});
				//Get the id of the last insertion
				sql = "SELECT vendor_tracking_method_id FROM vendor_tracking_method WHERE vendor_id=" + 
						osbtTracking.getVendorId() + " AND tracking_method_type=2";
				String trackingMethodId = jdbcTemplate.queryForObject(sql, String.class);
				
				//Insert the variable value
				sql = "INSERT INTO vendor_tracking_variables (vendor_tracking_method_id, variable_name, created_on) " +
						"VALUES(?,?,?)";
				jdbcTemplate.update(sql, new Object[]{trackingMethodId, 
						osbtTracking.getVariables().get(0).getVariableName(), new Date()});
				
				//Insert routing and branding preferences
				sql = "INSERT INTO vendor_routing_preferences (vendor_tracking_method_id, routing_allowed, " +
						"max_routed_surveys_per_respondent, max_qualified_surveys_per_respondent, audience_type, " +
						"survey_sub_category, complete_category, min_conversion, max_loi, branding, help_link, " +
						"send_url_userid, email_collection, created_on) VALUES(?,?, ?,?,?, ?,?,?,?,?,?, ?,?,?)";
				jdbcTemplate.update(sql, new Object[]{trackingMethodId, osbtTracking.getRoutingAllowed(),
						osbtTracking.getMaxRouted(), osbtTracking.getMaxQualified(), audienceTypeString,
						osbtTracking.getSurveySubCategory(), osbtTracking.getCompleteCategory(), 
						osbtTracking.getMinConversion(), osbtTracking.getMaxLoi(), osbtTracking.getBrandingType(),
						osbtTracking.getHelpLink(), osbtTracking.getSendUrl(), osbtTracking.getEmailCollection(),
						new Date()});
			}
			else{
				sql = "UPDATE vendor_tracking_method SET tracking_type=?, cpa=?, osbt_signup_allowed=?, url_code=?," +
						" complete_page_url=?, terminate_page_url=?, quotafull_page_url=?, security_terminate_page_url"
						+ "=?, error_page_url=? WHERE vendor_tracking_method_id=?";
				jdbcTemplate.update(sql, new Object[]{osbtTracking.getTrackingType(), osbtTracking.getCpa(),
						osbtTracking.getSignUpAllowed(), osbtTracking.getUrlCode(), osbtTracking.getCompleteUrl(),
						osbtTracking.getTerminateUrl(), osbtTracking.getQuotaFullUrl(), 
						osbtTracking.getSecurityTerminateUrl(), osbtTracking.getErrorsUrl(), 
						osbtTracking.getTrackingMethodId()});
				
				//Update variables info
				VariableBean variable = osbtTracking.getVariables().get(0);
				sql = "UPDATE vendor_tracking_variables SET variable_name='" + variable.getVariableName() + "' WHERE " +
						"vendor_tracking_variables_id=" + variable.getVariableId();
				jdbcTemplate.update(sql);
				
				//Update routing and branding preferences
				sql = "UPDATE vendor_routing_preferences SET routing_allowed=?, max_routed_surveys_per_respondent=?, " +
						"max_qualified_surveys_per_respondent=?, audience_type=?, survey_sub_category=?, " +
						"complete_category=?, min_conversion=?, max_loi=?, branding=?, help_link=?, send_url_userid=?" +
						", email_collection=? WHERE vendor_routing_preference_id=?";
				jdbcTemplate.update(sql, new Object[]{osbtTracking.getRoutingAllowed(), osbtTracking.getMaxRouted(),
						osbtTracking.getMaxQualified(), audienceTypeString, osbtTracking.getSurveySubCategory(), 
						osbtTracking.getCompleteCategory(), osbtTracking.getMinConversion(), osbtTracking.getMaxLoi(), 
						osbtTracking.getBrandingType(), osbtTracking.getHelpLink(), osbtTracking.getSendUrl(), 
						osbtTracking.getEmailCollection(), osbtTracking.getRoutingId()});				
			}
		}catch(Exception ex){
			System.out.println("Exception in editOsbtTracking: " + ex.toString());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public void editSurveyTracking(TrackingBean surveyTracking) {
		try{
			StringBuffer sb;
			String sql = "SELECT count(1) FROM vendor_tracking_method WHERE vendor_id=" + surveyTracking.getVendorId()
					+ " AND tracking_method_type=3";
			int count = jdbcTemplate.queryForInt(sql);
			String audienceTypeString = ("" + 
				Arrays.asList(surveyTracking.getAudienceType())).replaceAll("(^.|.$)", "").replace(", ", "," );
			/*
			 * Check if the values with the data-type integer and decimal in database have blanks, then assign them
			 * to null so as to avoid 'incorrect column value' exception
			 */			
			if(surveyTracking.getMaxRouted().equals("")){
				surveyTracking.setMaxRouted(null);
			}
			if(surveyTracking.getMaxQualified().equals("")){
				surveyTracking.setMaxQualified(null);
			}
			if(surveyTracking.getMinConversion().equals("")){
				surveyTracking.setMinConversion(null);
			}
			if(surveyTracking.getMaxLoi().equals("")){
				surveyTracking.setMaxLoi(null);
			}
			if(surveyTracking.getCpc().equals("")){
				surveyTracking.setCpc(null);
			}
			/*
			 * If routing is not allowed, then set the max. no. of routed surveys and max. no. of qualified surveys
			 * and CPC for routed completes to null, to avoid exceptions which might have been missed
			 */
			if(!surveyTracking.getRoutingAllowed().equals("1")){
				surveyTracking.setMaxRouted(null);
				surveyTracking.setMaxQualified(null);
				surveyTracking.setCpc(null);
			}
			/*
			 * If the tracking type is pixel, then the fields for server and Url tracking should be null and vice versa
			 */
			String trackingType = surveyTracking.getTrackingType();
			if(trackingType.equals("1")){
				surveyTracking.setUrlCode(null);
				surveyTracking.setCompleteUrl(null);
				surveyTracking.setTerminateUrl(null);
				surveyTracking.setQuotaFullUrl(null);
				surveyTracking.setSecurityTerminateUrl(null);
				surveyTracking.setErrorsUrl(null);
			}
			else if(trackingType.equals("2")){
				surveyTracking.setUrlCode(null);
			}
			else if (trackingType.equals("3") || trackingType.equals("4")) {
				surveyTracking.setCompleteUrl(null);
				surveyTracking.setTerminateUrl(null);
				surveyTracking.setQuotaFullUrl(null);
				surveyTracking.setSecurityTerminateUrl(null);
				surveyTracking.setErrorsUrl(null);
			}
			
			if(count==0){
				sql = "INSERT INTO vendor_tracking_method (vendor_id, tracking_method_type, tracking_type, url_code, " +
						"complete_page_url, terminate_page_url, quotafull_page_url, security_terminate_page_url, " +
						"error_page_url, created_on) VALUES (?,?,?,?, ?,?,?,?, ?,?)";
				jdbcTemplate.update(sql, new Object[]{surveyTracking.getVendorId(), 3, surveyTracking.getTrackingType(),
						surveyTracking.getUrlCode(), surveyTracking.getCompleteUrl(), surveyTracking.getTerminateUrl(),
						surveyTracking.getQuotaFullUrl(), surveyTracking.getSecurityTerminateUrl(),
						surveyTracking.getErrorsUrl(), new Date()});
				
				//Get the id of the last insertion
				sql = "SELECT vendor_tracking_method_id FROM vendor_tracking_method WHERE vendor_id=" + 
						surveyTracking.getVendorId() + " AND tracking_method_type=3";
				String trackingMethodId = jdbcTemplate.queryForObject(sql, String.class);
				
				List<VariableBean> variables = surveyTracking.getVariables();
				VariableBean tempVariable;
				sb = new StringBuffer("INSERT INTO vendor_tracking_variables (vendor_tracking_method_id, " +
						"variable_name, created_on) VALUES ");
				for(int i=0; i<3; i++){
					tempVariable = variables.get(i);
					if(!tempVariable.getVariableName().equals("")){						
						sb.append("(" + trackingMethodId + ",'" + tempVariable.getVariableName() + "',now()), ");
					}
				}
				sql = sb.toString();
				sb=null;
				if(sql.charAt(sql.length()-2)==','){
					sql = sql.substring(0, sql.length()-2);
				}			
				jdbcTemplate.update(sql);
				
				//Insert routing and branding preferences				
				sql = "INSERT INTO vendor_routing_preferences (vendor_tracking_method_id, routing_allowed, " +
						"max_routed_surveys_per_respondent, max_qualified_surveys_per_respondent, " +
						"cpc_routed_completes, audience_type, survey_sub_category, complete_category, " +
						"min_conversion, max_loi, branding, help_link, send_url_userid, email_collection, " +
						"created_on) VALUES(?,?, ?,?, ?,?,?,?, ?,?,?,?,?,?, ?)";
				jdbcTemplate.update(sql, new Object[]{trackingMethodId, surveyTracking.getRoutingAllowed(),
						surveyTracking.getMaxRouted(), surveyTracking.getMaxQualified(), surveyTracking.getCpc(), 
						audienceTypeString,surveyTracking.getSurveySubCategory(), surveyTracking.getCompleteCategory(), 
						surveyTracking.getMinConversion(), surveyTracking.getMaxLoi(), surveyTracking.getBrandingType(),
						surveyTracking.getHelpLink(), surveyTracking.getSendUrl(), surveyTracking.getEmailCollection(),
						new Date()});
			}
			//If the details have been added once already, then Update query is fired
			else{
				sql = "UPDATE vendor_tracking_method SET tracking_type=?, url_code=?, complete_page_url=?, " +
						"terminate_page_url=?, quotafull_page_url=?, security_terminate_page_url=?, error_page_url=?" +
						" WHERE vendor_tracking_method_id=?";
				jdbcTemplate.update(sql, new Object[]{surveyTracking.getTrackingType(), surveyTracking.getUrlCode(),
						surveyTracking.getCompleteUrl(), surveyTracking.getTerminateUrl(), 
						surveyTracking.getQuotaFullUrl(), surveyTracking.getSecurityTerminateUrl(),
						surveyTracking.getErrorsUrl(), surveyTracking.getTrackingMethodId()});
				
				//Update routing and branding preferences
				sql = "UPDATE vendor_routing_preferences SET routing_allowed=?, max_routed_surveys_per_respondent=?, " +
						"max_qualified_surveys_per_respondent=?, audience_type=?, survey_sub_category=?, " +
						"complete_category=?, min_conversion=?, max_loi=?, branding=?, help_link=?, send_url_userid=?" +
						", email_collection=?, cpc_routed_completes=? WHERE vendor_routing_preference_id=?";
				jdbcTemplate.update(sql, new Object[]{surveyTracking.getRoutingAllowed(), surveyTracking.getMaxRouted(),
						surveyTracking.getMaxQualified(), audienceTypeString, surveyTracking.getSurveySubCategory(), 
						surveyTracking.getCompleteCategory(), surveyTracking.getMinConversion(), surveyTracking.getMaxLoi(), 
						surveyTracking.getBrandingType(), surveyTracking.getHelpLink(), surveyTracking.getSendUrl(), 
						surveyTracking.getEmailCollection(), surveyTracking.getCpc(), surveyTracking.getRoutingId()});

				//Insert or update variables
				List<VariableBean> variables = surveyTracking.getVariables();
				VariableBean tempVariable;
				
				tempVariable = variables.get(0);
				sql = "UPDATE vendor_tracking_variables SET variable_name='" + tempVariable.getVariableName() +
						"' WHERE vendor_tracking_variables_id=" + tempVariable.getVariableId();
				jdbcTemplate.update(sql);
				//If variable id has a value then the row already exists in db. so update the row else insert the row
				for(int i=1; i<3; i++){
					tempVariable = variables.get(i);
					if(!tempVariable.getVariableName().equals("")){						
						if(tempVariable.getVariableId().equals("")){
							sql = "INSERT INTO vendor_tracking_variables (vendor_tracking_method_id, " +
									"variable_name, created_on) VALUES (" + surveyTracking.getTrackingMethodId() + 
									",'" + tempVariable.getVariableName() + "', now())";
							jdbcTemplate.update(sql);
						}
						else{
							sql = "UPDATE vendor_tracking_variables SET variable_name='" + 
									tempVariable.getVariableName() + "' WHERE vendor_tracking_variables_id=" + 
									tempVariable.getVariableId();
							jdbcTemplate.update(sql);
						}
					}
				}
			}
		}catch(Exception ex){
			System.out.println("Exception in editOsbtTracking: " + ex.toString());
			ex.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public TrackingBean getTrackingInformation(String vendorId,
			int trackingMethodType) {
		TrackingBean trackingBean = new TrackingBean();
		List<TrackingBean> trackingList;
		String sql;
		
		//For recruitment tracking
		if(trackingMethodType==1){
			sql = "SELECT vendor_tracking_method_id, tracking_type, cpa, url_code, success_conversion_page, " +
					"generate_co_reg_url FROM vendor_tracking_method WHERE vendor_id=" + vendorId + " AND " +
					"tracking_method_type=" + trackingMethodType;
			trackingList = jdbcTemplate.query(sql, new RowMapper<TrackingBean>(){

				@Override
				public TrackingBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					TrackingBean tracking = new TrackingBean();
					tracking.setTrackingMethodId(rs.getString("vendor_tracking_method_id"));
					tracking.setTrackingType(rs.getString("tracking_type"));
					tracking.setCpa(rs.getString("cpa"));
					tracking.setUrlCode(rs.getString("url_code"));
					tracking.setSuccessPage(rs.getString("success_conversion_page"));
					tracking.setGenerateCoReg(rs.getString("generate_co_reg_url"));
					return tracking;
				}
			});
		}
		//For OSBT and Survey tracking
		else{
			sql = "SELECT vtm.vendor_tracking_method_id, vtm.tracking_type, vtm.cpa, vtm.osbt_signup_allowed, " +
					"vtm.url_code, vtm.complete_page_url, vtm.terminate_page_url, vtm.quotafull_page_url, " +
					"vtm.security_terminate_page_url, vtm.error_page_url, vtm.success_conversion_page, " +
					"vtm.generate_co_reg_url, vrp.vendor_routing_preference_id, vrp.routing_allowed, " +
					"vrp.max_routed_surveys_per_respondent, vrp.max_qualified_surveys_per_respondent, " +
					"vrp.cpc_routed_completes, vrp.audience_type, vrp.survey_sub_category, vrp.complete_category, " +
					"vrp.min_conversion, vrp.max_loi, vrp.branding, vrp.help_link, vrp.send_url_userid, " +
					"vrp.email_collection FROM vendor_tracking_method vtm JOIN vendor_routing_preferences vrp " +
					"ON vrp.vendor_tracking_method_id = vtm.vendor_tracking_method_id WHERE vtm.vendor_id=" + vendorId
					+ " AND vtm.tracking_method_type=" + trackingMethodType;
			trackingList = jdbcTemplate.query(sql, new TrackingMapper());
		}

		if(trackingList!= null && trackingList.size()>0 && trackingList.get(0)!=null){
			trackingBean = trackingList.get(0);
		}
		
		/*
		 * For recruitment tracking, a hard coded URL is being displayed
		 * Change it once opinion bureau has been redesigned
		 */
		if(trackingMethodType==1){
			trackingBean.setCoRegUrl("https://www.opinionbureau.com/UserAction?action=vndregistration&vid=#vid#&" +
				"fname=#fname#&lname=#lname#&emailid=#email#&dob=#dob#&gen=#gender#&zcode=#zip#&city=#city#&ip=#ip#&" +
				"cname=#country#");
		}
		
		//get variable value for tracking bean
		if(trackingBean!=null && trackingBean.getTrackingMethodId()!=null){
			sql = "SELECT vendor_tracking_variables_id, variable_name FROM vendor_tracking_variables WHERE " +
					"vendor_tracking_method_id=" + trackingBean.getTrackingMethodId();
			trackingBean.setVariables(jdbcTemplate.query(sql, new RowMapper<VariableBean>(){

				@Override
				public VariableBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					VariableBean variable = new VariableBean();
					variable.setVariableId(rs.getString("vendor_tracking_variables_id"));
					variable.setVariableName(rs.getString("variable_name"));
					return variable;
				}
			}));
		}
		
		//set vendor id in all the tracking beans		
		trackingBean.setVendorId(vendorId);		
		return trackingBean;
	}

	@Override
	public String getSelectedCurrency(String vendorId) {
		String sql = "SELECT lgv.generic_value FROM pasmt_vendor pv JOIN lu_generic_values lgv ON " +
				"lgv.generic_value_id = pv.billing_currency_id WHERE pv.vendor_id=" + vendorId;
		String currency = "";
		List<String> tempList = jdbcTemplate.query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {				
				return rs.getString("lgv.generic_value");
			}			
		});
		if(tempList!=null && tempList.size()>0 && tempList.get(0)!=null){
			currency = tempList.get(0);
		}
		return currency;
	}
}
