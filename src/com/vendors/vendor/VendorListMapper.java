package com.vendors.vendor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.client.ContactBean;

public class VendorListMapper implements RowMapper<VendorBean>{
	
	@Override
	public VendorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		VendorBean vendor = new VendorBean();
		//, , , ,
		vendor.setVendorId(rs.getString("pv.vendor_id"));
		vendor.setVendorName(rs.getString("pv.vendor_name"));
		vendor.setVendorCode(rs.getString("pv.vendor_code"));
		vendor.setCountry(rs.getString("lc.country_name"));
		
		ContactBean primaryContact = new ContactBean();
		primaryContact.setName(rs.getString("pc.contact_name"));
		vendor.setPrimaryContact(primaryContact);
		return vendor;
	}
	
}
