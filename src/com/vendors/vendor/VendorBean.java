package com.vendors.vendor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.sales.client.ContactBean;

public class VendorBean {
	private String vendorId, vendorName, vendorCode, address, city, state, postalCode, country, websiteUrl,
		phoneNo, faxNo, rfqEmail, accountManager, vendorStatus, addedOn, tAndC, addContactsSize,contactIdList, 
		billingCurrency;
	
	private String[] vendorType;
	
	private ContactBean primaryContact;
	private List<ContactBean> additionalContacts;
	private CommonsMultipartFile vendorForm, guidelineDocs, signedIO, panelBook, companyBrochure, esomar;
	private String vendorFormName, guidelineName, signedIOName, panelBookName, brochureName, esomarName;
	
	//fields for billing information
	private String paymentType, paymentMode, bankName, beneficiaryName, accountNo, ifscCode, ibanNo, swiftCode, sortCode,
		bankAddress, intBankName, intBeneficiaryName, intBankAddress, intAccountNo, intIbanNo, intSwiftCode, intSortCode,
		paypalEmail, panNo, serviceTaxNo, tan, vatNo, accountRecEmail;
	
	//tracking bean for recruitment, osbt and surveys
	private TrackingBean recruitmentTracking, osbtTracking, surveyTracking;
	
	public VendorBean(){
		additionalContacts = LazyList.decorate
							(new ArrayList<ContactBean>(),
							 new InstantiateFactory(ContactBean.class));
	}
	
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	public String getRfqEmail() {
		return rfqEmail;
	}
	public void setRfqEmail(String rfqEmail) {
		this.rfqEmail = rfqEmail;
	}
	public String getAccountManager() {
		return accountManager;
	}
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}
	public String getVendorStatus() {
		return vendorStatus;
	}
	public void setVendorStatus(String vendorStatus) {
		this.vendorStatus = vendorStatus;
	}
	public String getAddedOn() {
		return addedOn;
	}
	public void setAddedOn(String addedOn) {
		this.addedOn = addedOn;
	}
	public ContactBean getPrimaryContact() {
		return primaryContact;
	}
	public void setPrimaryContact(ContactBean primaryContact) {
		this.primaryContact = primaryContact;
	}
	public List<ContactBean> getAdditionalContacts() {
		return additionalContacts;
	}
	public void setAdditionalContacts(List<ContactBean> additionalContacts) {
		this.additionalContacts = additionalContacts;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getIbanNo() {
		return ibanNo;
	}
	public void setIbanNo(String ibanNo) {
		this.ibanNo = ibanNo;
	}
	public String getSwiftCode() {
		return swiftCode;
	}
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}
	public String getSortCode() {
		return sortCode;
	}
	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}
	public String getBankAddress() {
		return bankAddress;
	}
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	public String getIntBankName() {
		return intBankName;
	}
	public void setIntBankName(String intBankName) {
		this.intBankName = intBankName;
	}
	public String getIntBeneficiaryName() {
		return intBeneficiaryName;
	}
	public void setIntBeneficiaryName(String intBeneficiaryName) {
		this.intBeneficiaryName = intBeneficiaryName;
	}
	public String getIntBankAddress() {
		return intBankAddress;
	}
	public void setIntBankAddress(String intBankAddress) {
		this.intBankAddress = intBankAddress;
	}
	public String getIntAccountNo() {
		return intAccountNo;
	}
	public void setIntAccountNo(String intAccountNo) {
		this.intAccountNo = intAccountNo;
	}
	public String getIntIbanNo() {
		return intIbanNo;
	}
	public void setIntIbanNo(String intIbanNo) {
		this.intIbanNo = intIbanNo;
	}
	public String getIntSwiftCode() {
		return intSwiftCode;
	}
	public void setIntSwiftCode(String intSwiftCode) {
		this.intSwiftCode = intSwiftCode;
	}
	public String getIntSortCode() {
		return intSortCode;
	}
	public void setIntSortCode(String intSortCode) {
		this.intSortCode = intSortCode;
	}
	public String getPaypalEmail() {
		return paypalEmail;
	}
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public String getServiceTaxNo() {
		return serviceTaxNo;
	}
	public void setServiceTaxNo(String serviceTaxNo) {
		this.serviceTaxNo = serviceTaxNo;
	}
	public String getTan() {
		return tan;
	}
	public void setTan(String tan) {
		this.tan = tan;
	}
	public String getVatNo() {
		return vatNo;
	}
	public void setVatNo(String vatNo) {
		this.vatNo = vatNo;
	}
	public String getAccountRecEmail() {
		return accountRecEmail;
	}
	public void setAccountRecEmail(String accountRecEmail) {
		this.accountRecEmail = accountRecEmail;
	}
	public CommonsMultipartFile getVendorForm() {
		return vendorForm;
	}
	public void setVendorForm(CommonsMultipartFile vendorForm) {
		this.vendorForm = vendorForm;
	}
	public CommonsMultipartFile getGuidelineDocs() {
		return guidelineDocs;
	}
	public void setGuidelineDocs(CommonsMultipartFile guidelineDocs) {
		this.guidelineDocs = guidelineDocs;
	}
	public CommonsMultipartFile getSignedIO() {
		return signedIO;
	}
	public void setSignedIO(CommonsMultipartFile signedIO) {
		this.signedIO = signedIO;
	}
	public CommonsMultipartFile getPanelBook() {
		return panelBook;
	}
	public void setPanelBook(CommonsMultipartFile panelBook) {
		this.panelBook = panelBook;
	}
	public CommonsMultipartFile getCompanyBrochure() {
		return companyBrochure;
	}
	public void setCompanyBrochure(CommonsMultipartFile companyBrochure) {
		this.companyBrochure = companyBrochure;
	}
	public CommonsMultipartFile getEsomar() {
		return esomar;
	}
	public void setEsomar(CommonsMultipartFile esomar) {
		this.esomar = esomar;
	}
	public String gettAndC() {
		return tAndC;
	}
	public void settAndC(String tAndC) {
		this.tAndC = tAndC;
	}
	public String getAddContactsSize() {
		return addContactsSize;
	}
	public void setAddContactsSize(String addContactsSize) {
		this.addContactsSize = addContactsSize;
	}
	public String getVendorFormName() {
		return vendorFormName;
	}
	public void setVendorFormName(String vendorFormName) {
		this.vendorFormName = vendorFormName;
	}
	public String getGuidelineName() {
		return guidelineName;
	}
	public void setGuidelineName(String guidelineName) {
		this.guidelineName = guidelineName;
	}
	public String getSignedIOName() {
		return signedIOName;
	}
	public void setSignedIOName(String signedIOName) {
		this.signedIOName = signedIOName;
	}
	public String getPanelBookName() {
		return panelBookName;
	}
	public void setPanelBookName(String panelBookName) {
		this.panelBookName = panelBookName;
	}
	public String getBrochureName() {
		return brochureName;
	}
	public void setBrochureName(String brochureName) {
		this.brochureName = brochureName;
	}
	public String getEsomarName() {
		return esomarName;
	}
	public void setEsomarName(String esomarName) {
		this.esomarName = esomarName;
	}
	public String getContactIdList() {
		return contactIdList;
	}
	public void setContactIdList(String contactIdList) {
		this.contactIdList = contactIdList;
	}
	public String getBillingCurrency() {
		return billingCurrency;
	}
	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}
	public TrackingBean getOsbtTracking() {
		return osbtTracking;
	}
	public void setOsbtTracking(TrackingBean osbtTracking) {
		this.osbtTracking = osbtTracking;
	}
	public TrackingBean getSurveyTracking() {
		return surveyTracking;
	}
	public void setSurveyTracking(TrackingBean surveyTracking) {
		this.surveyTracking = surveyTracking;
	}
	public TrackingBean getRecruitmentTracking() {
		return recruitmentTracking;
	}
	public void setRecruitmentTracking(TrackingBean recruitmentTracking) {
		this.recruitmentTracking = recruitmentTracking;
	}

	public String[] getVendorType() {
		return vendorType;
	}

	public void setVendorType(String[] vendorType) {
		this.vendorType = vendorType;
	}
}
