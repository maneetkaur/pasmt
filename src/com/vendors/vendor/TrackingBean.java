package com.vendors.vendor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TrackingBean {
	/*
	 * url code will store Pixel tracking code if tracking type is pixel. If tracking type is server to server,
	 * it will store S2S url*/
	private String trackingType, cpa, urlCode, successPage, signUpAllowed, completeUrl, terminateUrl, quotaFullUrl, 
	securityTerminateUrl, errorsUrl, generateCoReg, vendorId, trackingMethodId, coRegUrl;
	
	//tracking variables
	private List<VariableBean> variables = new ArrayList<VariableBean>();
	
	//variables for routing preferences
	private String routingAllowed, maxRouted, maxQualified, minConversion, maxLoi, cpc, surveySubCategory, 
		completeCategory, routingId;
	private String[] audienceType;
	
	//variables for branding preferences
	private String brandingType, helpLink, sendUrl, emailCollection;
	
	//Maps to select already added categories and sub categories
	private Map<String, String> subCategoryMap, completeCategoryMap;
	
	public String getTrackingType() {
		return trackingType;
	}

	public void setTrackingType(String trackingType) {
		this.trackingType = trackingType;
	}

	public String getCpa() {
		return cpa;
	}

	public void setCpa(String cpa) {
		this.cpa = cpa;
	}

	public String getUrlCode() {
		return urlCode;
	}

	public void setUrlCode(String urlCode) {
		this.urlCode = urlCode;
	}

	public String getSuccessPage() {
		return successPage;
	}

	public void setSuccessPage(String successPage) {
		this.successPage = successPage;
	}

	public String getSignUpAllowed() {
		return signUpAllowed;
	}

	public void setSignUpAllowed(String signUpAllowed) {
		this.signUpAllowed = signUpAllowed;
	}

	public String getCompleteUrl() {
		return completeUrl;
	}

	public void setCompleteUrl(String completeUrl) {
		this.completeUrl = completeUrl;
	}

	public String getTerminateUrl() {
		return terminateUrl;
	}

	public void setTerminateUrl(String terminateUrl) {
		this.terminateUrl = terminateUrl;
	}

	public String getQuotaFullUrl() {
		return quotaFullUrl;
	}

	public void setQuotaFullUrl(String quotaFullUrl) {
		this.quotaFullUrl = quotaFullUrl;
	}

	public String getSecurityTerminateUrl() {
		return securityTerminateUrl;
	}

	public void setSecurityTerminateUrl(String securityTerminateUrl) {
		this.securityTerminateUrl = securityTerminateUrl;
	}

	public String getErrorsUrl() {
		return errorsUrl;
	}

	public void setErrorsUrl(String errorsUrl) {
		this.errorsUrl = errorsUrl;
	}

	public String getRoutingAllowed() {
		return routingAllowed;
	}

	public void setRoutingAllowed(String routingAllowed) {
		this.routingAllowed = routingAllowed;
	}

	public String getMaxRouted() {
		return maxRouted;
	}

	public void setMaxRouted(String maxRouted) {
		this.maxRouted = maxRouted;
	}

	public String getMaxQualified() {
		return maxQualified;
	}

	public void setMaxQualified(String maxQualified) {
		this.maxQualified = maxQualified;
	}

	public String getMinConversion() {
		return minConversion;
	}

	public void setMinConversion(String minConversion) {
		this.minConversion = minConversion;
	}

	public String getMaxLoi() {
		return maxLoi;
	}

	public void setMaxLoi(String maxLoi) {
		this.maxLoi = maxLoi;
	}

	public String getCpc() {
		return cpc;
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

	public String[] getAudienceType() {
		return audienceType;
	}

	public void setAudienceType(String[] audienceType) {
		this.audienceType = audienceType;
	}

	public String getBrandingType() {
		return brandingType;
	}

	public void setBrandingType(String brandingType) {
		this.brandingType = brandingType;
	}

	public String getHelpLink() {
		return helpLink;
	}

	public void setHelpLink(String helpLink) {
		this.helpLink = helpLink;
	}

	public String getSendUrl() {
		return sendUrl;
	}

	public void setSendUrl(String sendUrl) {
		this.sendUrl = sendUrl;
	}

	public String getEmailCollection() {
		return emailCollection;
	}

	public void setEmailCollection(String emailCollection) {
		this.emailCollection = emailCollection;
	}

	public String getGenerateCoReg() {
		return generateCoReg;
	}

	public void setGenerateCoReg(String generateCoReg) {
		this.generateCoReg = generateCoReg;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getSurveySubCategory() {
		return surveySubCategory;
	}

	public void setSurveySubCategory(String surveySubCategory) {
		this.surveySubCategory = surveySubCategory;
	}

	public String getCompleteCategory() {
		return completeCategory;
	}

	public void setCompleteCategory(String completeCategory) {
		this.completeCategory = completeCategory;
	}

	public Map<String, String> getSubCategoryMap() {
		return subCategoryMap;
	}

	public void setSubCategoryMap(Map<String, String> subCategoryMap) {
		this.subCategoryMap = subCategoryMap;
	}

	public Map<String, String> getCompleteCategoryMap() {
		return completeCategoryMap;
	}

	public void setCompleteCategoryMap(Map<String, String> completeCategoryMap) {
		this.completeCategoryMap = completeCategoryMap;
	}

	public String getTrackingMethodId() {
		return trackingMethodId;
	}

	public void setTrackingMethodId(String trackingMethodId) {
		this.trackingMethodId = trackingMethodId;
	}

	public List<VariableBean> getVariables() {
		return variables;
	}

	public void setVariables(List<VariableBean> variables) {
		this.variables = variables;
	}

	public String getRoutingId() {
		return routingId;
	}

	public void setRoutingId(String routingId) {
		this.routingId = routingId;
	}

	public String getCoRegUrl() {
		return coRegUrl;
	}

	public void setCoRegUrl(String coRegUrl) {
		this.coRegUrl = coRegUrl;
	}
	
}