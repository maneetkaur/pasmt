package com.vendors.vendor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;

public interface VendorDao {
	public List<VendorBean> vendorList(HttpServletRequest request, int startIndex, int range);
	
	public List<CommonBean> vendorTypeList();
	
	public String getTermsAndConditions();
	
	public void addVendor(VendorBean vendor, String filePath);
	
	public int validateVendorName(String vendorName);
	
	public int validateVendorCode(String vendorCode);
	
	public int validateWebUrl(String webUrl);
	
	public VendorBean getVendorInformation(String vendorId);
	
	public void editVendor(VendorBean vendor, String filePath);
	
	public void editBillingInfo(VendorBean vendor);
	
	public void editRecruitmentTracking(TrackingBean recruitmentTracking);
	
	public void editOsbtTracking(TrackingBean osbtTracking);
	
	public void editSurveyTracking(TrackingBean surveyTracking);
	
	public TrackingBean getTrackingInformation(String vendorId, int trackingMethodType);
	
	public String getSelectedCurrency(String vendorId);

}
