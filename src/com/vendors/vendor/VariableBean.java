package com.vendors.vendor;

public class VariableBean {
	private String variableId, variableName, trackingMethodId;

	public String getVariableId() {
		return variableId;
	}

	public void setVariableId(String variableId) {
		this.variableId = variableId;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getTrackingMethodId() {
		return trackingMethodId;
	}

	public void setTrackingMethodId(String trackingMethodId) {
		this.trackingMethodId = trackingMethodId;
	}

}
