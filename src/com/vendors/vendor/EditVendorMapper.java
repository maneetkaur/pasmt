package com.vendors.vendor;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EditVendorMapper implements RowMapper<VendorBean>{

	@Override
	public VendorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		VendorBean vendor = new VendorBean();
		vendor.setVendorId(rs.getString("vendor_id"));
		vendor.setVendorName(rs.getString("vendor_name"));
		vendor.setVendorCode(rs.getString("vendor_code"));
		String temp = rs.getString("vendor_type");
		vendor.setVendorType(temp.split(","));
		vendor.setAddress(rs.getString("address"));
		vendor.setCity(rs.getString("city"));
		vendor.setState(rs.getString("state"));
		vendor.setPostalCode(rs.getString("postal_code"));
		vendor.setCountry(rs.getString("country_id"));
		vendor.setWebsiteUrl(rs.getString("website_url"));
		vendor.setPhoneNo(rs.getString("phone_number"));
		vendor.setFaxNo(rs.getString("fax_number"));
		vendor.setRfqEmail(rs.getString("rfq_email"));
		vendor.setPaymentType(rs.getString("payment_type"));
		vendor.setPaymentMode(rs.getString("payment_mode"));
		vendor.setBankName(rs.getString("bank_name"));
		vendor.setBeneficiaryName(rs.getString("beneficiary_name"));
		vendor.setAccountNo(rs.getString("account_number"));
		vendor.setIfscCode(rs.getString("ifsc_code"));
		vendor.setIbanNo(rs.getString("iban_number"));
		vendor.setSwiftCode(rs.getString("swift_code"));
		vendor.setSortCode(rs.getString("sort_code"));
		vendor.setBankAddress(rs.getString("bank_physical_address"));
		vendor.setIntBankName(rs.getString("intermediary_bank_name"));
		vendor.setIntBeneficiaryName(rs.getString("intermediary_beneficiary_name"));
		vendor.setIntBankAddress(rs.getString("intermediary_bank_address"));
		vendor.setIntAccountNo(rs.getString("intermediary_bank_acc_no"));
		vendor.setIntIbanNo(rs.getString("intermediary_bank_iban"));
		vendor.setIntSwiftCode(rs.getString("intermediary_bank_swift_code"));
		vendor.setIntSortCode(rs.getString("intermediary_bank_sort_code"));
		vendor.setPaypalEmail(rs.getString("paypal_email"));
		vendor.setPanNo(rs.getString("pan_number"));
		vendor.setServiceTaxNo(rs.getString("service_tax_number"));
		vendor.setTan(rs.getString("tan"));
		vendor.setVatNo(rs.getString("vat_number"));
		vendor.setAccountRecEmail(rs.getString("account_receivable_email"));
		vendor.setAccountManager(rs.getString("irb_account_manager"));
		vendor.setVendorStatus(rs.getString("vendor_status"));
		vendor.setAddedOn(rs.getString("date_format(created_on, '%b %e, %Y')"));
		vendor.setBillingCurrency(rs.getString("billing_currency_id"));
		return vendor;
	}

}
