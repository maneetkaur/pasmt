package com.vendors.vendor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

public class TrackingMapper implements RowMapper<TrackingBean>{

	@Override
	public TrackingBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		String tempArray[];
		Map<String, String> tempMap;
		
		TrackingBean trackingBean = new TrackingBean();
		trackingBean.setTrackingMethodId(rs.getString("vtm.vendor_tracking_method_id"));
		trackingBean.setTrackingType(rs.getString("vtm.tracking_type"));
		trackingBean.setCpa(rs.getString("vtm.cpa"));
		trackingBean.setSignUpAllowed(rs.getString("vtm.osbt_signup_allowed"));
		trackingBean.setUrlCode(rs.getString("vtm.url_code"));
		trackingBean.setCompleteUrl(rs.getString("vtm.complete_page_url"));
		trackingBean.setTerminateUrl(rs.getString("vtm.terminate_page_url"));
		trackingBean.setQuotaFullUrl(rs.getString("vtm.quotafull_page_url"));
		trackingBean.setSecurityTerminateUrl(rs.getString("vtm.security_terminate_page_url"));
		trackingBean.setErrorsUrl(rs.getString("vtm.error_page_url"));
		trackingBean.setSuccessPage(rs.getString("vtm.success_conversion_page"));
		trackingBean.setGenerateCoReg(rs.getString("vtm.generate_co_reg_url"));
		trackingBean.setRoutingId(rs.getString("vrp.vendor_routing_preference_id"));
		trackingBean.setRoutingAllowed(rs.getString("vrp.routing_allowed"));
		trackingBean.setMaxRouted(rs.getString("vrp.max_routed_surveys_per_respondent"));
		trackingBean.setMaxQualified(rs.getString("vrp.max_qualified_surveys_per_respondent"));
		trackingBean.setCpc(rs.getString("vrp.cpc_routed_completes"));
		String temp = rs.getString("vrp.audience_type");
		if(temp!= null){
		trackingBean.setAudienceType(temp.split(","));
		}
		trackingBean.setSurveySubCategory(rs.getString("vrp.survey_sub_category"));
		trackingBean.setCompleteCategory(rs.getString("vrp.complete_category"));
		trackingBean.setMinConversion(rs.getString("vrp.min_conversion"));
		trackingBean.setMaxLoi(rs.getString("vrp.max_loi"));
		trackingBean.setBrandingType(rs.getString("vrp.branding"));
		trackingBean.setHelpLink(rs.getString("vrp.help_link"));
		trackingBean.setSendUrl(rs.getString("vrp.send_url_userid"));
		trackingBean.setEmailCollection(rs.getString("vrp.email_collection"));
		
		//populate maps
		if(trackingBean.getSurveySubCategory() != null){
			tempArray = trackingBean.getSurveySubCategory().split(",");
			tempMap = new HashMap<String, String>();
			for(int i=0; i<tempArray.length; i++){
				tempMap.put(tempArray[i], tempArray[i]);
			}
			trackingBean.setSubCategoryMap(tempMap);
		}
		
		if(trackingBean.getCompleteCategory() != null){
			tempArray = trackingBean.getCompleteCategory().split(",");
			tempMap = new HashMap<String, String>();
			for(int i=0; i<tempArray.length; i++){
				tempMap.put(tempArray[i], tempArray[i]);
			}
			trackingBean.setCompleteCategoryMap(tempMap);
		}		
		
		return trackingBean;
	}

}
