package com.profile;

import com.pasmtAdmin.user.UserBean;

public interface ProfileDao {
	
	public int editProfile(UserBean user);
	
	public int changePassword(UserBean user);

}
