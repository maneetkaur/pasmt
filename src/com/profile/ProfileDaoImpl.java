package com.profile;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.login.LoginBean;
import com.pasmtAdmin.user.UserBean;

@Repository
@Transactional
public class ProfileDaoImpl implements ProfileDao{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public int editProfile(UserBean user) {
		int returnValue=0;
		try{
			String sql = "UPDATE pasmt_employee SET employee_name=?, company_name=?, department=?, team=?, role=?, " +
					"email_id=?, phone=?, address=? WHERE employee_id=?";
			returnValue = jdbcTemplate.update(sql, new Object[]{user.getEmployeeName(), user.getCompany(), user.getDepartment(),
					user.getTeam(), user.getRole(), user.getEmailId(), user.getPhone(), user.getAddress(),
					user.getUserId()});
		}catch(Exception e){
			System.out.println("Exception in editProfile of ProfileDaoImpl.java: " + e);
		}
		return returnValue;
	}

	@Override
	public int changePassword(UserBean user) {
		int returnValue=0;
		LoginBean login = (LoginBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userId = login.getEmployeeId();
		login = null;
		String sql = "SELECT password FROM pasmt_employee WHERE employee_id=" + userId;
		String temp = (String)jdbcTemplate.queryForObject(sql, String.class);
		if(!temp.equals(user.getOldPassword())){
			returnValue=2;
		}
		else{
			sql = "UPDATE pasmt_employee SET password=? WHERE employee_id=?";
			returnValue = jdbcTemplate.update(sql, new Object[]{user.getPassword(), userId});
		}
		return returnValue;
	}

}
