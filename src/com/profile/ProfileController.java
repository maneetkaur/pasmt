package com.profile;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;
import com.login.LoginBean;
import com.pasmtAdmin.user.UserBean;
import com.pasmtAdmin.user.UserDao;

@Controller
public class ProfileController {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	ProfileDao profileDao;
	
	@RequestMapping(value="/editProfileForm")
	public ModelAndView editProfileForm(HttpServletRequest request){
		ModelAndView editUser = new ModelAndView("profile/editProfile");
		LoginBean login = (LoginBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserBean user = userDao.getUserInformation(login.getEmployeeId());
		login=null;
		editUser.addObject("editUser", user);
		editUser.addObject("departmentList", userDao.getDepartmentList());
		editUser.addObject("roleList", commonFunction.getRoleList());
		return editUser;	
	}
	
	@RequestMapping(value="/editProfile", method=RequestMethod.POST)
	public String editProfile(@ModelAttribute("editUser") UserBean user, ModelMap model, HttpServletRequest request){
		int returnValue = profileDao.editProfile(user);
		model.addAttribute("success", returnValue);
		return "redirect:/editProfileForm";
	}
	
	@RequestMapping(value="/changePasswordForm", method=RequestMethod.GET)
	public ModelAndView changePasswordForm(){
		ModelAndView changePassword = new ModelAndView("profile/changePassword");
		changePassword.addObject("changePassword", new UserBean());
		return changePassword;
	}
	
	@RequestMapping(value="/changePassword", method=RequestMethod.POST)
	public String changePassword(@ModelAttribute("changePassword") UserBean user, ModelMap model){
		int returnValue = profileDao.changePassword(user);
		model.addAttribute("success", returnValue);
		if(returnValue==1){
			return "redirect:/editProfileForm";
		}
		else{
			return "redirect:/changePasswordForm";
		}
	}
}
