package com.finance.invoiceReceivable;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;
import com.finance.banking.BankingBean;
import com.sales.bid.RequirementBean;
import com.sales.client.ContactBean;
import com.sales.proposal.ProposalBean;

public interface InvoiceReceivableDao {
	public List<InvoiceReceivableBean> invoiceReceivableList(InvoiceReceivableBean invoiceReceivable, 
			HttpServletRequest request, int startIndex, int range);
	
	public ProposalBean getProjectInfo(String projectSummaryId);
	
	public List<CommonBean> getProjectSummaryList();
	
	public void addInvoiceReceivable(InvoiceReceivableBean invoiceReceivable);
	
	public InvoiceReceivableBean getInvoiceReceivableInformation(String invoiceReceivableId);
	
	public void editInvoiceReceivable(InvoiceReceivableBean invoiceReceivable);
	
	public ContactBean getClientContact(String clientId, int contactType);
	
	public BankingBean getRemittanceInstruction(String bankId, String clientId, String transactionType);

	public List<CommonBean> getBankList();

	List<CommonBean> getBankAccNoList(String bankName);
	
	public List<RequirementBean> getDeductionDetails(String projectSummaryId);
}
