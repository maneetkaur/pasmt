package com.finance.invoiceReceivable;

public class InvoiceReceivableBean {
	private String invoiceReceivableId, invoiceCode, projectSummaryId, month, year, invoiceTo, clientPO,
		waveMonth, transactionType, description, serviceTaxApplicable, serviceTax, amount, amountUsd, paymentMode, 
		paymentTerms, bankName, bank_detail_id, userId, raisedOn, dueOn, paymentStatus, paymentMadeOn, 
		paymentModeReceived, receivedBankName, receivedBankId, transactionId, netBankingNo, chequeNo, projectNumber, 
		agingFor, paymentRemarks, billingCurrency;
	
	//fields required for listing only
	private String clientCode, overDueBy;

	public String getInvoiceReceivableId() {
		return invoiceReceivableId;
	}

	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	public void setInvoiceReceivableId(String invoiceReceivableId) {
		this.invoiceReceivableId = invoiceReceivableId;
	}

	public String getProjectSummaryId() {
		return projectSummaryId;
	}

	public void setProjectSummaryId(String projectSummaryId) {
		this.projectSummaryId = projectSummaryId;
	}

	public String getInvoiceTo() {
		return invoiceTo;
	}

	public void setInvoiceTo(String invoiceTo) {
		this.invoiceTo = invoiceTo;
	}

	public String getClientPO() {
		return clientPO;
	}

	public void setClientPO(String clientPO) {
		this.clientPO = clientPO;
	}

	public String getWaveMonth() {
		return waveMonth;
	}

	public void setWaveMonth(String waveMonth) {
		this.waveMonth = waveMonth;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceTaxApplicable() {
		return serviceTaxApplicable;
	}

	public void setServiceTaxApplicable(String serviceTaxApplicable) {
		this.serviceTaxApplicable = serviceTaxApplicable;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmountUsd() {
		return amountUsd;
	}

	public void setAmountUsd(String amountUsd) {
		this.amountUsd = amountUsd;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getBank_detail_id() {
		return bank_detail_id;
	}

	public void setBank_detail_id(String bank_detail_id) {
		this.bank_detail_id = bank_detail_id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public String getRaisedOn() {
		return raisedOn;
	}

	public void setRaisedOn(String raisedOn) {
		this.raisedOn = raisedOn;
	}

	public String getDueOn() {
		return dueOn;
	}

	public void setDueOn(String dueOn) {
		this.dueOn = dueOn;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentMadeOn() {
		return paymentMadeOn;
	}

	public void setPaymentMadeOn(String paymentMadeOn) {
		this.paymentMadeOn = paymentMadeOn;
	}

	public String getPaymentModeReceived() {
		return paymentModeReceived;
	}

	public void setPaymentModeReceived(String paymentModeReceived) {
		this.paymentModeReceived = paymentModeReceived;
	}

	public String getReceivedBankId() {
		return receivedBankId;
	}

	public void setReceivedBankId(String receivedBankId) {
		this.receivedBankId = receivedBankId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getNetBankingNo() {
		return netBankingNo;
	}

	public void setNetBankingNo(String netBankingNo) {
		this.netBankingNo = netBankingNo;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getOverDueBy() {
		return overDueBy;
	}

	public void setOverDueBy(String overDueBy) {
		this.overDueBy = overDueBy;
	}

	public String getProjectNumber() {
		return projectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	public String getAgingFor() {
		return agingFor;
	}

	public void setAgingFor(String agingFor) {
		this.agingFor = agingFor;
	}

	public String getPaymentRemarks() {
		return paymentRemarks;
	}

	public void setPaymentRemarks(String paymentRemarks) {
		this.paymentRemarks = paymentRemarks;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getReceivedBankName() {
		return receivedBankName;
	}

	public void setReceivedBankName(String receivedBankName) {
		this.receivedBankName = receivedBankName;
	}
}
