package com.finance.invoiceReceivable;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.commonFunc.AbstractITextPdfViewBgWhite;
import com.commonFunc.CommonFunction;
import com.finance.banking.BankingBean;
import com.finance.banking.IntermediaryBankingBean;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sales.bid.BidBean;
import com.sales.bid.RequirementBean;
import com.sales.client.ContactBean;
import com.sales.proposal.ProposalBean;

public class InvoicePDFBuilder extends AbstractITextPdfViewBgWhite{
	
	private InvoiceReceivableBean invoiceReceivable;
	private ProposalBean projectInfo;
	private BidBean bid;
	private String billingCurrency;
	
	private static final String contactDetails = "\nPlot No. 2A, 2nd Floor\nCorner Market, Malviya Nagar\n" +
			"New Delhi-110017, India";
	private static final BaseColor headerBgColor = new BaseColor(23, 160, 134), 
			grayColor = new BaseColor(126, 140, 140), grayBgColor = new BaseColor(236, 240, 241);
	private static Font sectionHeaderFont, contentBlackFont, contentGrayFont, contentRedFont,
		contentGrayFontSmall, contentBlackFontSmall, contentGreenFont, headerWhiteFont;
	
	private DecimalFormat currencyFormat = new DecimalFormat("###,###,##0.00");;

	@Override
	protected void buildPdfDocument(Map<String, Object> model,
			Document document, PdfWriter pdfWriter, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			//String path = request.getServletContext().getRealPath("");
			//Register the new fonts
			//FontFactory.registerDirectory(path + "/resources/fonts");
			sectionHeaderFont = FontFactory.getFont("Calibri", 14.0f, Font.BOLD, BaseColor.BLACK);
			contentBlackFont = FontFactory.getFont("Calibri", 11.0f, BaseColor.BLACK);
			contentGrayFont = FontFactory.getFont("Calibri", 11.0f, grayColor);
			contentGrayFontSmall = FontFactory.getFont("Calibri", 10.0f, grayColor);
			contentRedFont = FontFactory.getFont("Calibri", 11.0f, Font.BOLD, new BaseColor(232, 76, 61));
			contentGreenFont = FontFactory.getFont("Calibri", 11.0f, headerBgColor);
			headerWhiteFont = FontFactory.getFont("Calibri", 11.0f, BaseColor.WHITE);
			contentBlackFontSmall = FontFactory.getFont("Calibri", 10.0f, BaseColor.BLACK);
			
			//populate all the values
			invoiceReceivable = (InvoiceReceivableBean)model.get("editInvoiceReceivable");
			projectInfo = (ProposalBean)model.get("proposal");
			bid = projectInfo.getBid();
			billingCurrency = bid.getBillingCurrency();
			ContactBean clientContact = (ContactBean)model.get("clientContact");
			ContactBean clientSalesContact = (ContactBean)model.get("clientSalesContact");
			
			//deduction list
			List<RequirementBean> deductionList = (List<RequirementBean>)model.get("deductionList");
			
			response.setHeader("Content-Disposition", "attachment; filename=\"Invoice# " + 
					invoiceReceivable.getInvoiceCode());
			
			/*//Set currency format according to transaction type; 1- local
			if(invoiceReceivable.getTransactionType().equals("1")){
				currencyFormat = new DecimalFormat("##,##,##,##0.00");
			}
			else{
				currencyFormat = new DecimalFormat("###,###,##0.00");
			}*/
			
			//Print the header table
			String path = request.getServletContext().getRealPath("");
			String resource = path + "/images/irblogo.png";
			document.add(getHeader(resource));
			
			//Get client and payment details
			document.add(getPaymentDetails(clientContact, clientSalesContact));
			clientContact = clientSalesContact = null;
			document.add(getPricingInfo(deductionList));
			BankingBean bank = (BankingBean)model.get("remittanceInstruction");
			document.newPage();
			document.add(getRemittanceInstructions(bank, invoiceReceivable.getTransactionType()));
			bank = null;
		}
		catch(Exception ex){
			System.out.println("Exception in buildPdfDocument of InvoicePDFBuilder.java" + ex);
			ex.printStackTrace();
		}				
	}
	
	protected PdfPTable getHeader(String filePath)
			throws DocumentException, IOException{		
		PdfPTable headerTable = new PdfPTable(2);
		headerTable.setWidthPercentage(100.0f);
		headerTable.setSpacingBefore(20);
		headerTable.setWidths(new float[]{7.0f, 3.0f});
		headerTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		headerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);		
		headerTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Image image = Image.getInstance(filePath);
		image.scaleAbsolute(108, 56);
		image.setAlt("IRB Logo");
		Chunk chunk1 = new Chunk(image, 0, 0, true);
		Chunk chunk2 = new Chunk("\n" + contactDetails, contentGrayFontSmall);
		//chunk2.setLineHeight();
		Phrase headerPhrase = new Phrase();
		headerPhrase.add(chunk1);
		headerPhrase.add(chunk2);
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setPhrase(headerPhrase);		
		headerTable.addCell(cell);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable subTable = new PdfPTable(2);
		subTable.setWidths(new float[]{4.0f, 6.0f});
		subTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);				
		subTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell subcell = new PdfPCell();
		subcell.setBorder(Rectangle.NO_BORDER);
		subcell.setColspan(2);
		chunk1 = new Chunk("INVOICE# " + invoiceReceivable.getInvoiceCode(), sectionHeaderFont);
		subcell.addElement(chunk1);
		subTable.addCell(subcell);
		
		//Convert the payment mode to string
		String paymentMode = invoiceReceivable.getPaymentMode();
		if(paymentMode.equals("1")){
			paymentMode = "Wire Transfer";
		}
		else if(paymentMode.equals("2")){
			paymentMode = "Paypal";
		}
		else if(paymentMode.equals("3")){
			paymentMode = "Cheque";
		}
		else if(paymentMode.equals("4")){
			paymentMode = "Net Banking";
		}
		
		subTable.addCell(new Phrase("Invoice Date", contentBlackFont));
		subTable.addCell(new Phrase(": " + invoiceReceivable.getRaisedOn(), contentBlackFont));
		subTable.addCell(new Phrase("Amount Due", contentRedFont));
		subTable.addCell(new Phrase(": " + billingCurrency + " " +
				formatCurrency(Double.parseDouble(invoiceReceivable.getAmount())),
				contentRedFont));
		subTable.addCell(new Phrase("Payment Terms", contentBlackFont));
		subTable.addCell(new Phrase(": " + invoiceReceivable.getPaymentTerms() + " days", 
				contentBlackFont));
		subTable.addCell(new Phrase("Payment Mode", contentBlackFont));
		subTable.addCell(new Phrase(": " + paymentMode, contentBlackFont));
		subTable.addCell(new Phrase("Due Date", contentRedFont));
		subTable.addCell(new Phrase(": " + invoiceReceivable.getDueOn(), contentRedFont));
		
		// we complete the table (otherwise the last row won't be rendered)
		headerTable.addCell(subTable);
        headerTable.completeRow();
		return headerTable;
	}
	
	protected PdfPTable getPaymentDetails(ContactBean contact, ContactBean salesContact) throws DocumentException{
		PdfPTable paymentTable = new PdfPTable(6);
		paymentTable.setWidthPercentage(100.0f);
		paymentTable.setSpacingBefore(20);
		paymentTable.setWidths(new float[]{3.25f, 0.5f, 1.25f, 2.25f, 1.5f, 2.0f});
		paymentTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);	
		paymentTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);		
		paymentTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setBorderWidthBottom(1.25f);
		cell.setBorderColorBottom(headerBgColor);
		cell.setPaddingBottom(7.5f);
		cell.setPhrase(new Phrase("Invoiced To", sectionHeaderFont));
		paymentTable.addCell(cell);
		paymentTable.addCell("");
		cell.setPhrase(new Phrase("Project Details", sectionHeaderFont));
		paymentTable.addCell(cell);
		cell.setPhrase(new Phrase(""));
		paymentTable.addCell(cell);
		paymentTable.addCell(cell);
		paymentTable.addCell(cell);
		
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setPhrase(new Phrase(contact.getName(), contentBlackFontSmall));
		paymentTable.addCell(cell);
		paymentTable.addCell("");
		Chunk chunk1 = new Chunk(": " , contentGrayFontSmall);
		Chunk chunk2;
		
		cell.setPhrase(new Phrase("Project Name" , contentGrayFontSmall));
		paymentTable.addCell(cell);
		chunk2 = new Chunk(bid.getBidName() , contentBlackFontSmall);
		Phrase phrase = new Phrase();
		phrase.add(chunk1);
		phrase.add(chunk2);				
		cell.setPhrase(phrase);
		paymentTable.addCell(cell);
		
		cell.setPhrase(new Phrase("Project Type", contentGrayFontSmall));
		paymentTable.addCell(cell);
		chunk2 = new Chunk(bid.getProjectType() , contentBlackFontSmall);
		phrase = new Phrase();
		phrase.add(chunk1);
		phrase.add(chunk2);				
		cell.setPhrase(phrase);
		paymentTable.addCell(cell);
		
		cell.setPhrase(new Phrase(bid.getClientCode(), contentBlackFontSmall));
		paymentTable.addCell(cell);
		paymentTable.addCell("");
		
		cell.setPhrase(new Phrase("IRB Project No.", contentGrayFontSmall));
		paymentTable.addCell(cell);
		chunk2 = new Chunk(invoiceReceivable.getProjectNumber() , contentBlackFontSmall);
		phrase = new Phrase();
		phrase.add(chunk1);
		phrase.add(chunk2);				
		cell.setPhrase(phrase);
		paymentTable.addCell(cell);
		
		cell.setPhrase(new Phrase("Client Sales Contact", contentGrayFontSmall));
		paymentTable.addCell(cell);
		chunk2 = new Chunk(salesContact.getName() , contentBlackFontSmall);
		phrase = new Phrase();
		phrase.add(chunk1);
		phrase.add(chunk2);				
		cell.setPhrase(phrase);
		paymentTable.addCell(cell);
		
		cell.setPhrase(new Phrase(bid.getClientAddress(), contentBlackFontSmall));
		paymentTable.addCell(cell);
		paymentTable.addCell("");
		if(bid.getProjectType().equals("Adhoc")){
			cell.setPhrase(new Phrase("Client PO#", contentGrayFontSmall));
			paymentTable.addCell(cell);
			chunk2 = new Chunk(invoiceReceivable.getClientPO() , contentBlackFontSmall);
			phrase = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);				
			cell.setPhrase(phrase);
			paymentTable.addCell(cell);
			
			cell.setPhrase(new Phrase("IRB Account Manager", contentGrayFontSmall));
			paymentTable.addCell(cell);
			chunk2 = new Chunk(bid.getAccountManager() , contentBlackFontSmall);
			phrase = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);				
			cell.setPhrase(phrase);
			paymentTable.addCell(cell);
		}
		else{
			cell.setPhrase(new Phrase("Wave/Month", contentGrayFontSmall));
			paymentTable.addCell(cell);
			chunk2 = new Chunk(projectInfo.getNotesOnTargeting() , contentBlackFontSmall);
			phrase = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);				
			cell.setPhrase(phrase);
			paymentTable.addCell(cell);
			
			cell.setPhrase(new Phrase("IRB Account Manager", contentGrayFontSmall));
			paymentTable.addCell(cell);
			chunk2 = new Chunk(bid.getAccountManager() , contentBlackFontSmall);
			phrase = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);				
			cell.setPhrase(phrase);
			paymentTable.addCell(cell);
			
			paymentTable.addCell("");
			paymentTable.addCell("");
			cell.setPhrase(new Phrase("Client PO#", contentGrayFontSmall));
			paymentTable.addCell(cell);
			chunk2 = new Chunk(invoiceReceivable.getClientPO() , contentBlackFontSmall);
			phrase = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);				
			cell.setPhrase(phrase);
			paymentTable.addCell(cell);
		}
		
		paymentTable.completeRow();
		return paymentTable;
	}
	
	protected PdfPTable getPricingInfo(List<RequirementBean> deductionList) throws DocumentException{
		Font headerGreenFont = FontFactory.getFont("Calibri", 12.0f, Font.BOLDITALIC, headerBgColor);
		boolean deduction =false;
		String tempST="";
		
		if(deductionList != null && deductionList.size()>0){
			deduction =true;
		}
		
		PdfPTable pricingTable = new PdfPTable(8);
		pricingTable.setWidthPercentage(100.0f);
		pricingTable.setSpacingBefore(20);
		pricingTable.setWidths(new float[]{0.5f, 2.0f, 1.5f, 2.0f, 1.0f, 1.0f, 1.0f, 1.5f});
		pricingTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		pricingTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);		
		pricingTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(8);
		cell.setPhrase(new Phrase("Pricing Info", sectionHeaderFont));
		cell.setBorderWidthBottom(1.25f);
		cell.setBorderColorBottom(headerBgColor);
		cell.setPaddingBottom(5.0f);
		pricingTable.addCell(cell);
		
		//add empty row
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(8);
		cell.setPhrase(new Phrase());
		cell.setFixedHeight(7.0f);
		pricingTable.addCell(cell);
		
		/*
		 * Changes for deduction - Maneet 21 May'14
		 * Add Sub heading
		 */
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(8);
		cell.setPhrase(new Phrase("Cost", headerGreenFont));
		//cell.setBorderWidthBottom(1.25f);
		//cell.setBorderColorBottom(headerBgColor);
		cell.setPaddingBottom(5.0f);
		pricingTable.addCell(cell);
		
		//add empty row
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(8);
		cell.setPhrase(new Phrase());
		cell.setFixedHeight(3.0f);
		pricingTable.addCell(cell);
		
		//Changes end
		
		pricingTable.getDefaultCell().setColspan(1);
		pricingTable.getDefaultCell().setBackgroundColor(headerBgColor);
		pricingTable.getDefaultCell().setPaddingBottom(7.0f);
		pricingTable.addCell(new Phrase("S.No.", headerWhiteFont));
		pricingTable.addCell(new Phrase("Country", headerWhiteFont));
		pricingTable.addCell(new Phrase("Services", headerWhiteFont));
		pricingTable.addCell(new Phrase("Description", headerWhiteFont));
		pricingTable.addCell(new Phrase("Quantity", headerWhiteFont));
		pricingTable.addCell(new Phrase("Unit Price", headerWhiteFont));
		pricingTable.addCell(new Phrase("Setup Fee", headerWhiteFont));
		pricingTable.addCell(new Phrase("Amount", headerWhiteFont));
		
		pricingTable.getDefaultCell().setPaddingBottom(0.0f);
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(1);
		cell.setPaddingBottom(6.0f);
		PdfPCell altCell = new PdfPCell();
		altCell.setBorder(Rectangle.NO_BORDER);
		altCell.setBackgroundColor(grayBgColor);
		altCell.setPaddingBottom(6.0f);
		RequirementBean requirement;
		float quantity, unitPrice, setupFee, amount=0.0f, subtotal=0.0f, serviceTax, stAmount, subtotalDed=0.0f;
		for(int i =0; i<projectInfo.getProposalRequirements().size(); i++){
			requirement = projectInfo.getProposalRequirements().get(i);
			if(i%2==0){
				cell.setPhrase(new Phrase(String.valueOf(i+1), contentGrayFont));
				pricingTable.addCell(cell);
				cell.setPhrase(new Phrase(requirement.getCountry(), contentGrayFont));
				pricingTable.addCell(cell);
				cell.setPhrase(new Phrase(requirement.getServices(), contentGrayFont));
				pricingTable.addCell(cell);
				cell.setPhrase(new Phrase(requirement.getDescription(), contentGrayFont));
				pricingTable.addCell(cell);
				cell.setPhrase(new Phrase(requirement.getSampleSize(), contentGrayFont));
				pricingTable.addCell(cell);
								
				unitPrice = Float.parseFloat(requirement.getQuotedCpi());
				setupFee = Float.parseFloat(requirement.getSetupFee());
				cell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(unitPrice), contentGrayFont));
				pricingTable.addCell(cell);
				cell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(setupFee), contentGrayFont));
				pricingTable.addCell(cell);
				if(requirement.getServices().equals("Sample")){	
					quantity = Float.parseFloat(requirement.getSampleSize());
					amount = quantity * unitPrice + setupFee;
				}
				else{
					amount = unitPrice + setupFee;
				}
				subtotal = subtotal + amount;
				cell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(amount), contentGrayFont));
				pricingTable.addCell(cell);
			}
			else{
				altCell.setPhrase(new Phrase(String.valueOf(i+1), contentGrayFont));
				pricingTable.addCell(altCell);
				altCell.setPhrase(new Phrase(requirement.getCountry(), contentGrayFont));
				pricingTable.addCell(altCell);
				altCell.setPhrase(new Phrase(requirement.getServices(), contentGrayFont));
				pricingTable.addCell(altCell);
				altCell.setPhrase(new Phrase(requirement.getDescription(), contentGrayFont));
				pricingTable.addCell(altCell);
				altCell.setPhrase(new Phrase(requirement.getSampleSize(), contentGrayFont));
				pricingTable.addCell(altCell);
				
				unitPrice = Float.parseFloat(requirement.getQuotedCpi());
				setupFee = Float.parseFloat(requirement.getSetupFee());
				altCell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(unitPrice), contentGrayFont));
				pricingTable.addCell(altCell);
				altCell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(setupFee), contentGrayFont));
				pricingTable.addCell(altCell);
				if(requirement.getServices().equals("Sample")){
					quantity = Float.parseFloat(requirement.getSampleSize());
					amount = quantity * unitPrice + setupFee;
				}
				else{
					amount = unitPrice + setupFee;
				}
				subtotal = subtotal + amount;
				altCell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(amount), contentGrayFont));
				pricingTable.addCell(altCell);
			}
		}
		
		//add empty row and line on top
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(8);
		cell.setPhrase(new Phrase());
		cell.setFixedHeight(8.0f);
		cell.setBorderWidthTop(2.0f);
		pricingTable.addCell(cell);
		
		pricingTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
		pricingTable.getDefaultCell().setBorderWidthBottom(0.25f);
		pricingTable.getDefaultCell().setPaddingBottom(8.0f);
		
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(5);
		cell.setPhrase(new Phrase());
		pricingTable.addCell(cell);
		pricingTable.addCell(cell);
		
		pricingTable.addCell(cell);
				
		altCell = new PdfPCell();
		altCell.setBorder(Rectangle.NO_BORDER);
		altCell.setColspan(2);
		altCell.setBackgroundColor(BaseColor.WHITE);
		altCell.setPhrase(new Phrase("Sub Total", contentBlackFont));
		altCell.setBorderWidthBottom(0.25f);
		pricingTable.addCell(altCell);
		//pricingTable.addCell("");
		pricingTable.addCell(new Phrase(billingCurrency + " " + formatCurrency(subtotal), contentGreenFont));
			
		if(deduction == false){
			pricingTable.addCell(cell);		
			altCell.setPhrase(new Phrase("Project Minimum", contentBlackFont));
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(Double.parseDouble(projectInfo.getProjectMinimum())), contentGreenFont));
			
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Final Project Cost", contentBlackFont));
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(Double.parseDouble(projectInfo.getProjectValue())), contentGreenFont));
			
			tempST="";
			if(invoiceReceivable.getServiceTaxApplicable().equals("1")){
				serviceTax = Float.parseFloat(invoiceReceivable.getServiceTax());
				tempST = "@  " + invoiceReceivable.getServiceTax();
			}
			else{
				serviceTax = 0.0f;
				tempST = "";
			}
			amount = Float.parseFloat(projectInfo.getProjectValue());
			stAmount = amount * serviceTax /100;		
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Service Tax" + tempST, 
					new Font(FontFactory.getFont("Calibri", 11.0f, Font.ITALIC, BaseColor.BLACK))));
			pricingTable.addCell(altCell);
			tempST=null;
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + formatCurrency(stAmount), 
					new Font(FontFactory.getFont("Calibri", 11.0f, Font.ITALIC, grayColor))));
			
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Grand Total", contentRedFont));
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(Double.parseDouble(invoiceReceivable.getAmount())), contentRedFont));
		}
		
		
		/*
		 * Changes for deduction - Maneet
		 */
		else{
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setColspan(8);
			cell.setPhrase(new Phrase("Deduction", headerGreenFont));
			//cell.setBorderWidthBottom(1.25f);
			//cell.setBorderColorBottom(headerBgColor);
			cell.setPaddingBottom(5.0f);
			pricingTable.addCell(cell);
			
			//add empty row
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setColspan(8);
			cell.setPhrase(new Phrase());
			cell.setFixedHeight(3.0f);
			pricingTable.addCell(cell);
			
			pricingTable.getDefaultCell().setColspan(1);
			pricingTable.getDefaultCell().setBackgroundColor(headerBgColor);
			pricingTable.getDefaultCell().setPaddingBottom(7.0f);
			pricingTable.addCell(new Phrase("S.No.", headerWhiteFont));
			pricingTable.addCell(new Phrase("Country", headerWhiteFont));
			pricingTable.addCell(new Phrase("Services", headerWhiteFont));
			pricingTable.addCell(new Phrase("Description", headerWhiteFont));
			pricingTable.addCell(new Phrase("Reason", headerWhiteFont));
			pricingTable.addCell(new Phrase("Quantity", headerWhiteFont));
			pricingTable.addCell(new Phrase("Unit Price", headerWhiteFont));		
			pricingTable.addCell(new Phrase("Amount", headerWhiteFont));
			
			pricingTable.getDefaultCell().setPaddingBottom(0.0f);
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setColspan(1);
			cell.setPaddingBottom(6.0f);
			altCell = new PdfPCell();
			altCell.setBorder(Rectangle.NO_BORDER);
			altCell.setBackgroundColor(grayBgColor);
			altCell.setPaddingBottom(6.0f);
			
			subtotalDed=0.0f;
			for(int i =0; i<deductionList.size(); i++){
				requirement = deductionList.get(i);
				if(i%2==0){
					cell.setPhrase(new Phrase(String.valueOf(i+1), contentGrayFont));
					pricingTable.addCell(cell);
					cell.setPhrase(new Phrase(requirement.getCountry(), contentGrayFont));
					pricingTable.addCell(cell);
					cell.setPhrase(new Phrase(requirement.getServices(), contentGrayFont));
					pricingTable.addCell(cell);
					cell.setPhrase(new Phrase(requirement.getDescription(), contentGrayFont));
					pricingTable.addCell(cell);
					cell.setPhrase(new Phrase(requirement.getReason(), contentGrayFont));
					pricingTable.addCell(cell);
					cell.setPhrase(new Phrase(requirement.getSampleSize(), contentGrayFont));
					pricingTable.addCell(cell);
					
					amount=0.0f;					
					unitPrice = Float.parseFloat(requirement.getQuotedCpi());
					quantity = Float.parseFloat(requirement.getSampleSize());
					amount = quantity * unitPrice;
					
					cell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(unitPrice), contentGrayFont));
					pricingTable.addCell(cell);					
					subtotalDed = subtotalDed + amount;
					cell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(amount), contentGrayFont));
					pricingTable.addCell(cell);
				}
				else{
					altCell.setPhrase(new Phrase(String.valueOf(i+1), contentGrayFont));
					pricingTable.addCell(altCell);
					altCell.setPhrase(new Phrase(requirement.getCountry(), contentGrayFont));
					pricingTable.addCell(altCell);
					altCell.setPhrase(new Phrase(requirement.getServices(), contentGrayFont));
					pricingTable.addCell(altCell);
					altCell.setPhrase(new Phrase(requirement.getDescription(), contentGrayFont));
					pricingTable.addCell(altCell);
					altCell.setPhrase(new Phrase(requirement.getReason(), contentGrayFont));
					pricingTable.addCell(altCell);
					altCell.setPhrase(new Phrase(requirement.getSampleSize(), contentGrayFont));
					pricingTable.addCell(altCell);
					
					amount=0.0f;
					unitPrice = Float.parseFloat(requirement.getQuotedCpi());
					quantity = Float.parseFloat(requirement.getSampleSize());
					amount = quantity * unitPrice;
					
					altCell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(unitPrice), 
							contentGrayFont));
					pricingTable.addCell(altCell);					
					
					subtotalDed = subtotalDed + amount;
					altCell.setPhrase(new Phrase(billingCurrency + " " + formatCurrency(amount), 
							contentGrayFont));
					pricingTable.addCell(altCell);
				}
			}
			
			//add empty row and line on top
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setColspan(8);
			cell.setPhrase(new Phrase());
			cell.setFixedHeight(8.0f);
			cell.setBorderWidthTop(2.0f);
			pricingTable.addCell(cell);
			
			pricingTable.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
			pricingTable.getDefaultCell().setBorderWidthBottom(0.25f);
			pricingTable.getDefaultCell().setPaddingBottom(8.0f);
			
			cell = new PdfPCell();
			cell.setBorder(Rectangle.NO_BORDER);
			cell.setColspan(5);
			cell.setPhrase(new Phrase());
			pricingTable.addCell(cell);
			pricingTable.addCell(cell);
			
			pricingTable.addCell(cell);
					
			altCell = new PdfPCell();
			altCell.setBorder(Rectangle.NO_BORDER);
			altCell.setColspan(2);
			altCell.setBackgroundColor(BaseColor.WHITE);
			altCell.setPhrase(new Phrase("Sub Total", contentBlackFont));
			altCell.setBorderWidthBottom(0.25f);
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + formatCurrency(subtotalDed), 
					contentGreenFont));
			
			pricingTable.addCell(cell);
			Chunk chunk1 = new Chunk();
			chunk1 = new Chunk("Gross Cost", contentBlackFont);
			Chunk chunk2 = new Chunk(" (Cost - Deduction)", contentBlackFontSmall);
			Phrase phrase  = new Phrase();
			phrase.add(chunk1);
			phrase.add(chunk2);
			altCell.setPhrase(phrase);
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			amount = subtotal-subtotalDed;
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(amount), contentGreenFont));
			
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Project Minimum", contentBlackFont));
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(Double.parseDouble(projectInfo.getProjectMinimum())), contentGreenFont));
			
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Final Project Cost", contentBlackFont));
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(Double.parseDouble(projectInfo.getProjectValue())), contentGreenFont));
			
			tempST="";
			if(invoiceReceivable.getServiceTaxApplicable().equals("1")){
				serviceTax = Float.parseFloat(invoiceReceivable.getServiceTax());
				tempST = "@  " + invoiceReceivable.getServiceTax();
			}
			else{
				serviceTax = 0.0f;
				tempST = "";
			}
			amount = Float.parseFloat(projectInfo.getProjectValue());
			stAmount = amount * serviceTax /100;		
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Service Tax" + tempST, 
					new Font(FontFactory.getFont("Calibri", 11.0f, Font.ITALIC, BaseColor.BLACK))));
			pricingTable.addCell(altCell);
			tempST=null;
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + formatCurrency(stAmount), 
					new Font(FontFactory.getFont("Calibri", 11.0f, Font.ITALIC, grayColor))));
			
			pricingTable.addCell(cell);
			altCell.setPhrase(new Phrase("Grand Total", contentRedFont));
			pricingTable.addCell(altCell);
			//pricingTable.addCell("");
			pricingTable.addCell(new Phrase(billingCurrency + " " + 
					formatCurrency(Double.parseDouble(invoiceReceivable.getAmount())), contentRedFont));
		}
		
		
		/*
		 * Changes end
		 */
		return pricingTable;
	}
	
	protected PdfPTable getRemittanceInstructions(BankingBean bank, String transactionType) throws DocumentException{
		Font headerFont = new Font(FontFactory.getFont("Calibri", 13.0f, Font.BOLD, BaseColor.BLACK));
		PdfPTable remittanceTable = new PdfPTable(2);
		remittanceTable.setWidthPercentage(100.0f);
		remittanceTable.setSpacingBefore(20);
		remittanceTable.setWidths(new float[]{3.0f,7.0f	});
		remittanceTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);		
		remittanceTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);		
		remittanceTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell headerCell = new PdfPCell();
		headerCell.setBorder(Rectangle.NO_BORDER);
		remittanceTable.addCell("");
		remittanceTable.addCell("");
		headerCell.setColspan(2);
		
		Chunk underline = new Chunk("Bank Details", headerFont);
	    underline.setUnderline(0.1f, -3f); //0.1 thick, -2 y-location
	    Phrase phrase = new Phrase();
	    phrase.add(underline);
	    remittanceTable.addCell(phrase);
	    headerCell.setPhrase(phrase);
		
		PdfPCell cell= new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		
		//add empty row
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setColspan(2);
		cell.setPhrase(new Phrase());
		cell.setFixedHeight(7.0f);
		remittanceTable.addCell(cell);
		
		remittanceTable.addCell(new Phrase("Bank Name", contentBlackFont));
		remittanceTable.addCell(new Phrase(": " + bank.getBankName(), contentBlackFont));
		remittanceTable.addCell(new Phrase("Beneficiary Name", contentBlackFont));
		remittanceTable.addCell(new Phrase(": " + bank.getBenefName(), contentBlackFont));
		remittanceTable.addCell(new Phrase("A/c No.", contentBlackFont));
		remittanceTable.addCell(new Phrase(": " + bank.getAccountNo(), contentBlackFont));
		
		if(transactionType.equals("1")){
			remittanceTable.addCell(new Phrase("IFSC Code", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + bank.getIfsccode(), contentBlackFont));
		}
		else{
			remittanceTable.addCell(new Phrase("Swift Code", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + bank.getSwiftCode(), contentBlackFont));
			if(bank.getRoutingNO() != null && !(bank.getRoutingNO().equals("") || bank.getRoutingNO().equals("NA"))){
				remittanceTable.addCell(new Phrase("IBAN Routing No.", contentBlackFont));
				remittanceTable.addCell(new Phrase(": " + bank.getRoutingNO(), contentBlackFont));
			}
			if(bank.getSqrtCode() != null && !(bank.getSqrtCode().equals("") || bank.getSqrtCode().equals("NA"))){
				remittanceTable.addCell(new Phrase("SORT Code", contentBlackFont));
				remittanceTable.addCell(new Phrase(": " + bank.getSqrtCode(), contentBlackFont));
			}					
		}
		remittanceTable.addCell(new Phrase("Bank Physical Address", contentBlackFont));
		remittanceTable.addCell(new Phrase(": " + bank.getPhysicalAdd(), contentBlackFont));
		remittanceTable.addCell(cell);
		remittanceTable.addCell(cell);
		
		//Additional Info
		if(transactionType.equals("1")){
			underline = new Chunk("Additional Info", headerFont);
		    underline.setUnderline(0.1f, -3f); //0.1 thick, -2 y-location
		    phrase = new Phrase();
		    phrase.add(underline);
		    remittanceTable.addCell(phrase);
		    headerCell.setPhrase(phrase);
			//add empty row
			remittanceTable.addCell(cell);
			remittanceTable.addCell(new Phrase("PAN No.", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + bank.getPanNo(), contentBlackFont));
			remittanceTable.addCell(new Phrase("ST No.", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + bank.getServiceTaxNo(), contentBlackFont));
			if(bank.getTan() != null && !(bank.getTan().equals("") || bank.getTan().equals("NA"))){
				remittanceTable.addCell(new Phrase("TAN No.", contentBlackFont));
				remittanceTable.addCell(new Phrase(": " + bank.getTan(), contentBlackFont));
			}
			if(bank.getVatNo() != null && !(bank.getVatNo().equals("") || bank.getVatNo().equals("NA"))){
				remittanceTable.addCell(new Phrase("VAT No.", contentBlackFont));
				remittanceTable.addCell(new Phrase(": " + bank.getVatNo(), contentBlackFont));
			}
			remittanceTable.addCell(cell);
			remittanceTable.addCell(cell);
		}
		else if(transactionType.equals("2") && !bank.getInterBankListSize().equals("0")){
			IntermediaryBankingBean intBank = bank.getIntermediayBankList().get(0);
			
			underline = new Chunk("Intermediary Bank Details", headerFont);
		    underline.setUnderline(0.1f, -3f); //0.1 thick, -2 y-location
		    phrase = new Phrase();
		    phrase.add(underline);
		    remittanceTable.addCell(phrase);
		    headerCell.setPhrase(phrase);
			//add empty row
			remittanceTable.addCell(cell);
			remittanceTable.addCell(new Phrase("Bank Name", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + intBank.getInterBankName(), contentBlackFont));
			remittanceTable.addCell(new Phrase("Beneficiary Name", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + intBank.getInterBenefName(), contentBlackFont));
			remittanceTable.addCell(new Phrase("A/c No.", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + intBank.getInterAccountNo(), contentBlackFont));
						
			remittanceTable.addCell(new Phrase("Swift Code", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + intBank.getInterSwiftCode(), contentBlackFont));
			if(intBank.getInterRoutingNo() != null && 
					!(intBank.getInterRoutingNo().equals("") || intBank.getInterRoutingNo().equals("NA"))){
				remittanceTable.addCell(new Phrase("IBAN Routing No.", contentBlackFont));
				remittanceTable.addCell(new Phrase(": " + intBank.getInterRoutingNo(), contentBlackFont));
			}
			if(intBank.getInterSqrtCode() != null && 
					!(intBank.getInterSqrtCode().equals("") || intBank.getInterSqrtCode().equals("NA"))){
				remittanceTable.addCell(new Phrase("SORT Code", contentBlackFont));
				remittanceTable.addCell(new Phrase(": " + intBank.getInterSqrtCode(), contentBlackFont));
			}					
			remittanceTable.addCell(new Phrase("Bank Physical Address", contentBlackFont));
			remittanceTable.addCell(new Phrase(": " + intBank.getInterPhysicalAdd(), contentBlackFont));
			remittanceTable.addCell(cell);
			remittanceTable.addCell(cell);
		}
		
		//Add terms and conditions
		underline = new Chunk("Terms & Conditions", headerFont);
	    underline.setUnderline(0.1f, -3f); //0.1 thick, -2 y-location
	    phrase = new Phrase();
	    phrase.add(underline);
	    remittanceTable.addCell(phrase);
	    headerCell.setPhrase(phrase);
		
		//add empty row
		remittanceTable.addCell(cell);
		remittanceTable.getDefaultCell().setColspan(2);
		remittanceTable.addCell(new Phrase("�   Any Bank Charges (towards the remittance) need to be borne by" +
			" remitter.\n�   If invoice exceeds 30 days of due date, customer can be charged additional finance" +
			" charge of 2% per month on the past due balance from the date of the invoice.\n�   If any account" +
			" is placed for collection with an attorney or collection service, there shall be charged to the" +
			" account a reasonable attorney fee and other costs of the \n     collection, which shall be paid by the" +
			" customer/client.", contentBlackFont));
		remittanceTable.addCell(cell);
		remittanceTable.addCell(cell);
		remittanceTable.addCell(cell);
		remittanceTable.addCell(cell);
		
		remittanceTable.addCell(new Phrase("We appreciate the opportunity to be of service and look forward" +
				" to continuing this successful business relationship!", sectionHeaderFont));
		return remittanceTable;
	}

	private String formatCurrency(double value){
		if(invoiceReceivable.getTransactionType().equals("1")){
			return CommonFunction.inrCurrencyformat(value);
		}
		else{			
			return currencyFormat.format(value);
		}
		
	}
}
