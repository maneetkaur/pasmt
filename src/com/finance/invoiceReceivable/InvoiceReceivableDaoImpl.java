package com.finance.invoiceReceivable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;
import com.finance.banking.BankingBean;
import com.finance.banking.IntermediaryBankingBean;
import com.finance.projectSummary.DeductionListMapper;
import com.finance.projectSummary.IncomeListMapper;
import com.sales.bid.BidBean;
import com.sales.bid.RequirementBean;
import com.sales.client.ContactBean;
import com.sales.proposal.ProposalBean;
import com.sales.proposal.ProposalDao;

@Repository
@Transactional
public class InvoiceReceivableDaoImpl implements InvoiceReceivableDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	ProposalDao proposalDao;

	@Override
	public List<InvoiceReceivableBean> invoiceReceivableList(InvoiceReceivableBean invoiceReceivable,
			HttpServletRequest request, int startIndex, int range) {
		try{
			StringBuffer where_clause, where_clause2, qryTemp;
			String temp, sql_count;
			boolean invoiceNumberSearch=false;
			String sql = "SELECT pir.invoice_receivable_id as ir_id, lp.proposal_id, pir.month, pir.year, pir.total_amount, " +
					"date_format(pir.raise_on, '%b %e, %Y'), date_format(pir.due_on, '%b %e, %Y'), pir.invoice_number" +
					", DATEDIFF(now(),pir.due_on) AS overDueBy, pir.payment_status, pps.wave, pir.transaction_type FROM" +
					" pasmt_invoice_receivable pir" +
					" JOIN pasmt_project_summary pps ON pir.project_summary_id=pps.project_summary_id JOIN lu_project" +
					" lp ON pps.project_id=lp.project_id ";
			where_clause = new StringBuffer("WHERE pir.status=1 AND ");
			where_clause2 = new StringBuffer("WHERE pir.status=1 AND ");
			
			if(invoiceReceivable.getInvoiceCode() != null && !invoiceReceivable.getInvoiceCode().equals("")){
				invoiceNumberSearch = true;
				where_clause.append("pir.invoice_number like '%" + invoiceReceivable.getInvoiceCode() + "%' AND ");
				where_clause2.append("pps.wave like '%" + invoiceReceivable.getInvoiceCode() + "%' AND ");
			}
			
			if(invoiceReceivable.getMonth()!=null && !invoiceReceivable.getMonth().equals("")){				
				String[] monthNYear = invoiceReceivable.getMonth().split("-");
				temp = commonFunction.changeMonthToNumber(monthNYear[0]);
				where_clause.append("pir.month like '%" + temp + "%' AND " + 
						"pir.year like '%" + monthNYear[1] + "%' AND ");
				where_clause2.append("pir.month like '%" + temp + "%' AND " + 
						"pir.year like '%" + monthNYear[1] + "%' AND ");
			}
			
			String where=where_clause.toString().trim();
			String where2=where_clause2.toString().trim();
			where_clause = null;
			where_clause2 = null;			
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			where2=where2.substring(0, where2.length()-4);
			if(invoiceNumberSearch){
				qryTemp = new StringBuffer("(" + sql + where + ") UNION (" + sql + where2 +")");				
				sql_count = "select count(1) from (" + qryTemp.toString() + ") b";
			}
			else{
				qryTemp = new StringBuffer(sql + where);				
				sql_count = "SELECT count(1) FROM pasmt_invoice_receivable pir JOIN pasmt_project_summary pps ON " +
						"pir.project_summary_id=pps.project_summary_id JOIN lu_project lp ON pps.project_id = " +
						"lp.project_id " + where;
			}			
			sql = "select b.* from (" + qryTemp.toString() + ") b ORDER BY b.ir_id DESC LIMIT " + 
					startIndex + ", " + range;
			
			//Calculate count for pagination
			int total_rows = jdbcTemplate.queryForInt(sql_count);		
			request.setAttribute("total_rows", total_rows);
			List<InvoiceReceivableBean> invoiceReceivableList = jdbcTemplate.query(sql, 
					new InvoiceReceivableListMapper());
			
			Map<String, String> billingCurrencyMap = 
					commonFunction.createMapFromList(commonFunction.getBillingCurrency());
			Map<String, Object> map = new HashMap<String, Object>();
			for(InvoiceReceivableBean invoiceReceivableTemp: invoiceReceivableList){
				//get month from number and concatenate and month and year into one
				if(invoiceReceivableTemp.getMonth() != null && invoiceReceivableTemp.getYear() != null){
					temp = commonFunction.changeMonthToWords(invoiceReceivableTemp.getMonth());
					invoiceReceivableTemp.setMonth(temp + "-" + invoiceReceivableTemp.getYear());
				}
				
				sql = "SELECT pc.client_code, pb.billing_currency_id, pb.project_type FROM pasmt_proposal pp JOIN" +
						" pasmt_bid pb ON pp.bid_number = " +
						"pb.bid_id JOIN pasmt_client pc ON pb.client_id=pc.client_id WHERE pp.proposal_id=" + 
						invoiceReceivableTemp.getClientCode();
				map = jdbcTemplate.queryForMap(sql);
				invoiceReceivableTemp.setClientCode(map.get("client_code").toString());
				temp = map.get("billing_currency_id").toString();
				invoiceReceivableTemp.setBillingCurrency(billingCurrencyMap.get(temp));
				temp = map.get("project_type").toString();
				//If project type is tracker, append wave
				if(temp.equals("2")){
					temp = invoiceReceivableTemp.getWaveMonth();
					if(temp != null && !temp.equals("")){
						invoiceReceivableTemp.setInvoiceCode(invoiceReceivableTemp.getInvoiceCode() + "-" + temp);
					}
				}
			}
			return invoiceReceivableList;
		}
		catch(Exception e){
			System.out.println("Exception in invoiceReceivableList of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ProposalBean getProjectInfo(String projectSummaryId) {
		try{
			String sql = "SELECT pp.bid_number, pp.project_minimum, pps.project_value, pp.proposal_number, pps.wave" +
					" FROM pasmt_project_summary pps JOIN lu_project" +
					" lp ON pps.project_id = lp.project_id JOIN pasmt_proposal pp ON lp.proposal_id = " +
					"pp.proposal_id WHERE pps.project_summary_id =" + projectSummaryId;
			ProposalBean proposal = new ProposalBean();
			BidBean bid = new BidBean();
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap = jdbcTemplate.queryForMap(sql);
			bid.setBidId(dataMap.get("bid_number").toString());
			proposal.setProjectMinimum(dataMap.get("project_minimum").toString());
			proposal.setProjectValue(dataMap.get("project_value").toString());
			proposal.setProposalNumber(dataMap.get("proposal_number").toString());
			//notesOnTargeting will store wave/month value
			if(dataMap.get("wave") != null){
				proposal.setNotesOnTargeting(dataMap.get("wave").toString());
			}
			
			//added client_id and address - Maneet 24 March'14 for PDF
			sql = "SELECT pc.client_name, pc.client_id, pc.address, pb.bid_name, pb.project_type, pe.employee_name," +
					" pb.billing_currency_id" +
					" FROM pasmt_bid pb JOIN pasmt_client pc ON pb.client_id = pc.client_id JOIN pasmt_employee pe" +
					" ON pb.account_manager_id = pe.employee_id WHERE pb.bid_id =" + bid.getBidId();
			bid = (BidBean) jdbcTemplate.queryForObject(sql, new RowMapper<BidBean>(){
				@Override
				public BidBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					BidBean bid = new BidBean();
					bid.setBidClient(rs.getString("pc.client_id"));
					bid.setClientAddress(rs.getString("pc.address"));
					bid.setClientCode(rs.getString("pc.client_name"));
					bid.setBidName(rs.getString("pb.bid_name"));
					bid.setAccountManager(rs.getString("pe.employee_name"));
					bid.setBillingCurrency(rs.getString("pb.billing_currency_id"));
					if(rs.getString("pb.project_type")!= null){
						String temp = rs.getString("pb.project_type");
						if(temp.equals("1")){
							bid.setProjectType("Adhoc");
						}
						else if(temp.equals("2")){
							bid.setProjectType("Tracker");
						}
					}
					return bid;
				}
			});
			
			//set bid id again as the object has been reset
			bid.setBidId(dataMap.get("bid_number").toString());
			
			sql = "SELECT lgv.generic_value FROM lu_generic_values lgv WHERE lgv.generic_value_id =" + 
					bid.getBillingCurrency();
			bid.setBillingCurrency(jdbcTemplate.queryForObject(sql, String.class));
			
			//Get the income list
			sql = "SELECT bid_requirement_id, country_id, services_id, target_audience, description, sample_size" +
					", quoted_cpi, setup_fee, survey_id FROM pasmt_sales_requirement WHERE source=3 AND status=1" +
					" AND source_id=" + projectSummaryId;
			List<RequirementBean> incomeList = jdbcTemplate.query(sql, new IncomeListMapper());
			proposal.setProposalRequirements(proposalDao.getRequirementsForPDF(incomeList));
			proposal.setBid(bid);
			bid = null;
			sql = null;
			return proposal;
		}
		catch(Exception e){
			System.out.println("Exception in getProjectInfo of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}		
		return null;
	}

	@Override
	public List<CommonBean> getProjectSummaryList() {
		try{
			String projectNumber, wave;
			String sql = "SELECT pps.project_summary_id, pps.project_id, pps.wave FROM pasmt_project_summary pps" +
					" LEFT JOIN pasmt_invoice_receivable pir ON pps.project_summary_id=pir.project_summary_id" +
					" WHERE pir.project_summary_id is null AND pps.status =1";
			List<CommonBean> projectSummaryList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){
				@Override
				public CommonBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CommonBean projectSummary = new CommonBean();				
					projectSummary.setId(rs.getString("pps.project_summary_id"));
					projectSummary.setName(rs.getString("pps.project_id"));
					projectSummary.setCode(rs.getString("pps.wave"));
					return projectSummary;
				}
			});
			
			for(CommonBean projectSummary: projectSummaryList){
				if(projectSummary != null){
					sql = "SELECT pp.proposal_number FROM lu_project lp JOIN pasmt_proposal pp ON lp.proposal_id" +
							" = pp.proposal_id WHERE lp.project_id=" + projectSummary.getName();
					projectNumber = jdbcTemplate.queryForObject(sql, String.class);
					if(projectNumber != null){
						wave = projectSummary.getCode();
						if(wave != null && !wave.equals("")){
							projectSummary.setName(projectNumber + " - " + wave);
						}
						else{
							projectSummary.setName(projectNumber);
						}
					}
					projectSummary.setCode(null);
				}
			}
			projectNumber = wave = null;
			return projectSummaryList;
		}
		catch(Exception e){
			System.out.println("Exception in getProjectSummaryList of InvoiceReceivableDaoImpl.java: " + e);
		}
		return null;
	}
	
	public String createInvoiceNumber(String projectNumber){
		Date date = new Date();  
		DateFormat formatter = new SimpleDateFormat("yyMM");
		String invoiceNum="", temp, sql;
		try{
			sql = "SELECT count(*) FROM pasmt_invoice_receivable";
			int count = jdbcTemplate.queryForInt(sql);
			String curYear, curMonth;
			temp = formatter.format(date);
			curYear= temp.substring(0,2);
			curMonth = temp.substring(2,4);
			temp = "IRB-" + curMonth + "-" + curYear + "-" + projectNumber + "-";
			//if count is zero, this is the first entry. So, a fresh bid code has to be generated.
			if(count==0){
				invoiceNum = temp + "001";		
			}
			//there are existing rows in bid table in database
			else{
				String dbYear, dbMonth, dbCode;				
				
				//Find the last entry in bid table
				sql = "SELECT invoice_number FROM pasmt_invoice_receivable ORDER BY invoice_receivable_id " +
						"DESC LIMIT 1";
				
				dbCode = jdbcTemplate.queryForObject(sql, String.class);
				
				dbMonth = dbCode.substring(4,6);
				dbYear = dbCode.substring(7,9);
				//System.out.println("dbyear: " + dbYear + " dbmonth: " + dbMonth);
				//if current month and year match the latest entry
				if(curYear.equals(dbYear) && curMonth.equals(dbMonth)){
					int tempCount = Integer.parseInt(dbCode.substring(dbCode.length()-3));
					tempCount++;
					if(tempCount < 10){
						invoiceNum = temp + "00" + Integer.toString(tempCount);
					}
					else if(tempCount>=10 && tempCount<100){
						invoiceNum = temp + "0" + Integer.toString(tempCount);
					}
					else {
						invoiceNum = temp + Integer.toString(tempCount);
					}
					//assign all variables to null
					curMonth = curYear = dbCode = dbYear = dbMonth = null;
				}
				else{
					invoiceNum = temp + "001";
				}
			}			
		}
		catch(Exception e){
			System.out.println("Exception in createInvoiceNumber of InvoiceReceivableDaoImpl.java");
			e.printStackTrace();
		}
		finally{
			date = null;
			formatter = null;
			temp = sql = null;
		}
		return invoiceNum;
	}

	@Override
	public void addInvoiceReceivable(InvoiceReceivableBean invoiceReceivable) {
		try{
			//If service tax is not applicable, then service tax rate is 0
			if(!invoiceReceivable.getServiceTaxApplicable().equals("1")){
				invoiceReceivable.setServiceTax("0");
			}
			
			//Split month and year into different number fields
			if(invoiceReceivable.getMonth()!=null){
				if(invoiceReceivable.getMonth().equals("")){
					
				}
				else{
					String temp[] = invoiceReceivable.getMonth().split("-");
					invoiceReceivable.setMonth(commonFunction.changeMonthToNumber(temp[0]));
					invoiceReceivable.setYear(temp[1]);
				}				
			}
			invoiceReceivable.setInvoiceCode(createInvoiceNumber(invoiceReceivable.getProjectNumber()));
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int paymentTerms =0;			
			    try {
			        paymentTerms = Integer.parseInt(invoiceReceivable.getPaymentTerms());			        
			    } catch (NumberFormatException nfe) {
			        System.out.println("Payment terms is not a number for invoice number: " + 
			        	invoiceReceivable.getInvoiceCode());
			        
			    }			
			cal.add(Calendar.DATE, paymentTerms);
			
			//If wave is null in string then set it to null
			if(invoiceReceivable.getWaveMonth()!=null && invoiceReceivable.getWaveMonth().equals("null")){
				invoiceReceivable.setWaveMonth(null);
			}
			
			String sql = "INSERT INTO pasmt_invoice_receivable (project_summary_id, client_contact_id, banking_id, " +
					"month, year, client_po_number, transaction_type, service_description, service_tax_applicable," +
					" service_tax_rate, total_amount, eq_usd_total_amount, payment_mode, payment_term, invoice_number," +
					" raise_on, due_on, payment_status, status, created_on) VALUES (?,?,?, ?,?,?,?,?,?,?, ?,?,?,?,?,?," +
					"?,?,?,?)";
			jdbcTemplate.update(sql, new Object[]{invoiceReceivable.getProjectSummaryId(), 
				invoiceReceivable.getInvoiceTo(), invoiceReceivable.getBank_detail_id(), invoiceReceivable.getMonth(),
				invoiceReceivable.getYear(), invoiceReceivable.getClientPO(), 
				invoiceReceivable.getTransactionType(), invoiceReceivable.getDescription(), 
				invoiceReceivable.getServiceTaxApplicable(), invoiceReceivable.getServiceTax(), 
				invoiceReceivable.getAmount().replace(",", ""), invoiceReceivable.getAmountUsd().replace(",", ""), 
				invoiceReceivable.getPaymentMode(),
				invoiceReceivable.getPaymentTerms(), invoiceReceivable.getInvoiceCode(), new Date(), cal.getTime(),
				1, 1, new Date()});
		}
		catch(Exception e){
			System.out.println("Exception in addInvoiceReceivable of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	@Override
	public InvoiceReceivableBean getInvoiceReceivableInformation(String invoiceReceivableId) {
		try{
			String sql="SELECT pir.project_summary_id, client_contact_id, pir.banking_id, month, year, client_po_number," +
					" pps.wave, transaction_type, service_description, service_tax_applicable, service_tax_rate," +
					" total_amount, eq_usd_total_amount, payment_mode, payment_term, invoice_number," +
					" date_format(raise_on, '%b %e, %Y'), date_format(due_on, '%b %e, %Y'), payment_status," +
					" invoice_receivable_id, DATEDIFF(now(),raise_on) AS aging, DATEDIFF(now(),due_on) AS" +
					" overDueBy, DATE(payment_made_on), received_payment_mode, received_banking_id, transaction_id, " +
					"net_banking_number, cheque_number, pps.project_id, payment_remarks, pb.bank_name FROM" +
					" pasmt_invoice_receivable pir JOIN pasmt_project_summary" +
					" pps ON pps.project_summary_id = pir.project_summary_id JOIN pasmt_banking pb ON" +
					" pb.banking_id = pir.banking_id WHERE invoice_receivable_id=" + 
					invoiceReceivableId;
			InvoiceReceivableBean invoiceReceivable = jdbcTemplate.queryForObject(sql, new EditIRMapper());
			if(invoiceReceivable.getMonth() != null){
				invoiceReceivable.setMonth(commonFunction.changeMonthToWords(invoiceReceivable.getMonth()) + "-" +
						invoiceReceivable.getYear());
			}
			sql = "SELECT pp.proposal_number FROM lu_project lp JOIN pasmt_proposal pp ON lp.proposal_id = " +
					"pp.proposal_id WHERE lp.project_id=" + invoiceReceivable.getProjectNumber();
			String temp = jdbcTemplate.queryForObject(sql, String.class);
			invoiceReceivable.setProjectNumber(temp);
			
			if(invoiceReceivable.getReceivedBankId() != null && !invoiceReceivable.getReceivedBankId().equals("") &&
					!invoiceReceivable.getReceivedBankId().equals("0")){
				sql = "SELECT bank_name FROM pasmt_banking WHERE banking_id=" + invoiceReceivable.getReceivedBankId();
				temp = jdbcTemplate.queryForObject(sql, String.class);
				invoiceReceivable.setReceivedBankName(temp);
			}
			
			
			temp = null;
			return invoiceReceivable;			
		}
		catch(Exception e){
			System.out.println("Exception in getInvoiceReceivableInformation of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void editInvoiceReceivable(InvoiceReceivableBean invoiceReceivable) {
		try{
			//If service tax is not applicable, then service tax rate is 0
			if(!invoiceReceivable.getServiceTaxApplicable().equals("1")){
				invoiceReceivable.setServiceTax("0");
			}
			
			if(invoiceReceivable.getMonth()!=null){
				if(invoiceReceivable.getMonth().equals("")){
					
				}
				else{
					String temp[] = invoiceReceivable.getMonth().split("-");
					invoiceReceivable.setMonth(commonFunction.changeMonthToNumber(temp[0]));
					invoiceReceivable.setYear(temp[1]);
				}				
			}
			String sql="";
			if(invoiceReceivable.getPaymentMadeOn().equals("")){
				invoiceReceivable.setPaymentMadeOn(null);
				sql = "UPDATE pasmt_invoice_receivable SET client_contact_id=" + invoiceReceivable.getInvoiceTo() + 
				", banking_id=" + invoiceReceivable.getBank_detail_id() + ", month=" + invoiceReceivable.getMonth()
				+ ", year=" + invoiceReceivable.getYear() + ", client_po_number='" + invoiceReceivable.getClientPO()
				+ "', transaction_type=" + 
				invoiceReceivable.getTransactionType() + ", service_description='" + invoiceReceivable.getDescription()
				+ "', service_tax_applicable=" + invoiceReceivable.getServiceTaxApplicable() + ", service_tax_rate='"
				+ invoiceReceivable.getServiceTax() + "', total_amount='" + 
				invoiceReceivable.getAmount().replace(",", "") + 
				"', eq_usd_total_amount='" + invoiceReceivable.getAmountUsd().replace(",", "") + "', payment_mode=" + 
				invoiceReceivable.getPaymentMode() + ", payment_term='" + invoiceReceivable.getPaymentTerms() + 
				"', due_on=DATE_ADD(`raise_on` , INTERVAL " + invoiceReceivable.getPaymentTerms() + " DAY), " +
				"payment_status=" + invoiceReceivable.getPaymentStatus() + ", received_payment_mode=" + 
				invoiceReceivable.getPaymentModeReceived() + ", received_banking_id=" + 
				invoiceReceivable.getReceivedBankId() + ", transaction_id='" + invoiceReceivable.getTransactionId()
				+ "', net_banking_number='" + invoiceReceivable.getNetBankingNo() + "', cheque_number='" + 
				invoiceReceivable.getChequeNo() + "', payment_remarks='" + invoiceReceivable.getPaymentRemarks() + 
				"' WHERE invoice_receivable_id=" + invoiceReceivable.getInvoiceReceivableId();
			}
			else{
				sql = "UPDATE pasmt_invoice_receivable SET client_contact_id=" + invoiceReceivable.getInvoiceTo() + 
				", banking_id=" + invoiceReceivable.getBank_detail_id() + ", month=" + invoiceReceivable.getMonth()
				+ ", year=" + invoiceReceivable.getYear() + ", client_po_number='" + invoiceReceivable.getClientPO()
				+ "', transaction_type=" + 
				invoiceReceivable.getTransactionType() + ", service_description='" + invoiceReceivable.getDescription()
				+ "', service_tax_applicable=" + invoiceReceivable.getServiceTaxApplicable() + ", service_tax_rate='"
				+ invoiceReceivable.getServiceTax() + "', total_amount='" + 
				invoiceReceivable.getAmount().replace(",", "") + 
				"', eq_usd_total_amount='" + invoiceReceivable.getAmountUsd().replace(",", "") + "', payment_mode=" + 
				invoiceReceivable.getPaymentMode() + ", payment_term='" + invoiceReceivable.getPaymentTerms() + 
				"', due_on=DATE_ADD(`raise_on` , INTERVAL " + invoiceReceivable.getPaymentTerms() + " DAY), " +
				"payment_status=" + invoiceReceivable.getPaymentStatus() + ", payment_made_on='" + 
				invoiceReceivable.getPaymentMadeOn() + "', received_payment_mode=" + 
				invoiceReceivable.getPaymentModeReceived() + ", received_banking_id=" + 
				invoiceReceivable.getReceivedBankId() + ", transaction_id='" + invoiceReceivable.getTransactionId()
				+ "', net_banking_number='" + invoiceReceivable.getNetBankingNo() + "', cheque_number='" + 
				invoiceReceivable.getChequeNo() + "', payment_remarks='" + invoiceReceivable.getPaymentRemarks() +
				"' WHERE invoice_receivable_id=" + invoiceReceivable.getInvoiceReceivableId();
			}
			
			//DATE_ADD(`date` , INTERVAL 2 DAY)
			jdbcTemplate.update(sql);
			
		}
		catch(Exception e){
			System.out.println("Exception in editInvoiceReceivable of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
	}

	@Override
	public ContactBean getClientContact(String contactId, int contactType) {
		ContactBean contact = new ContactBean();
		try{
			String sql="";
			if(contactType==1){
				sql = "SELECT contact_name, email_id FROM pasmt_contacts WHERE status=1 AND contacts_id=" + contactId;
			}
			else if(contactType==2){
				sql = "SELECT contact_name, email_id FROM pasmt_contacts WHERE source_type=1 AND contact_type=1 AND " +
						"status=1 AND source_id=" + contactId;
			}
			Map<String, Object> contactMap = jdbcTemplate.queryForMap(sql);
			if(contactMap!= null){
				contact.setName(contactMap.get("contact_name").toString());
				contact.setEmail(contactMap.get("email_id").toString());
			}
		}
		catch(Exception e){
			System.out.println("Exception in getClientContact of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}		
		return contact;
	}

	@Override
	public BankingBean getRemittanceInstruction(String bankId, String clientId, String transactionType) {
		BankingBean bank = new BankingBean();
		try{
			//If local transaction, then get only bank details
			String sql = "SELECT bank_name, beneficiary_name, bank_physical_address, account_number, swift_code, " +
					"iban_number, ifsc_code, sort_code, pan, service_tax_number, tan, vat_number FROM pasmt_banking" +
					" WHERE banking_id=" + bankId;
			bank = jdbcTemplate.queryForObject(sql, new RowMapper<BankingBean>(){

				@Override
				public BankingBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					BankingBean tempBank = new BankingBean();
					tempBank.setBankName(rs.getString("bank_name"));
					tempBank.setBenefName(rs.getString("beneficiary_name"));
					tempBank.setPhysicalAdd(rs.getString("bank_physical_address"));
					tempBank.setAccountNo(rs.getString("account_number"));
					tempBank.setSwiftCode(rs.getString("swift_code"));
					tempBank.setRoutingNO(rs.getString("iban_number"));
					tempBank.setIfsccode(rs.getString("ifsc_code"));
					tempBank.setSqrtCode(rs.getString("sort_code"));
					tempBank.setPanNo(rs.getString("pan"));
					tempBank.setServiceTaxNo(rs.getString("service_tax_number"));
					tempBank.setTan(rs.getString("tan"));
					tempBank.setVatNo(rs.getString("vat_number"));
					return tempBank;
				}
				
			});
			//If international transaction, then get intermediary bank details, if any
			if(transactionType.equals("2")){
				sql = "SELECT pib.bank_name, pib.beneficiary_name, pib.account_number, pib.iban_number, " +
						"pib.swift_code, pib.bank_physical_address, pib.sort_code FROM pasmt_intermediary_banking pib JOIN " +
						"pasmt_client pc ON pib.country_id = pc.bill_country_id WHERE pib.status=1 AND " +
						"pib.banking_id=" + bankId + " AND pc.client_id=" + clientId + " LIMIT 1";
				List<IntermediaryBankingBean> intBankList = jdbcTemplate.query(sql, 
						new RowMapper<IntermediaryBankingBean>(){

							@Override
							public IntermediaryBankingBean mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								IntermediaryBankingBean intermediaryBank = new IntermediaryBankingBean();
								intermediaryBank.setInterBankName(rs.getString("pib.bank_name"));
								intermediaryBank.setInterBenefName(rs.getString("pib.beneficiary_name"));
								intermediaryBank.setInterAccountNo(rs.getString("pib.account_number"));
								intermediaryBank.setInterRoutingNo(rs.getString("pib.iban_number"));
								intermediaryBank.setInterSwiftCode(rs.getString("pib.swift_code"));
								intermediaryBank.setInterPhysicalAdd(rs.getString("pib.bank_physical_address"));
								intermediaryBank.setInterSqrtCode(rs.getString("pib.sort_code"));
								return intermediaryBank;
							}					
				});
				
				if(intBankList!= null && intBankList.size()>0){
					bank.setInterBankListSize("1");
				}
				else{
					bank.setInterBankListSize("0");
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in getRemittanceInstruction of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}
		
		return bank;
	}
	
	@Override
	public List<CommonBean> getBankList()
	{
		List<CommonBean> bankList=new ArrayList<CommonBean>();
		String sql="select distinct(bank_name) from pasmt_banking";
		bankList=jdbcTemplate.query(sql,new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean bank = new CommonBean();
				bank.setId(rs.getString("bank_name"));
				bank.setName(rs.getString("bank_name"));
				return bank;
			}
		});
		return bankList;
	}
	
	/*
	 * This method returns the list of bank account number and currency based on the name of the bank selected
	 * If the account type is current, then the currency is INR, and if it is EEFC, then the selected currency 
	 * will be displayed 
	 * Added by Maneet - 22 Apr'14
	 */
	@Override
	public List<CommonBean> getBankAccNoList(String bankName) {
		String sql = "SELECT pb.banking_id, pb.account_number, lgv.generic_value, pb.account_type FROM" +
				" pasmt_banking pb LEFT JOIN lu_generic_values lgv ON pb.eefc_currency_id = lgv.generic_value_id" +
				" WHERE pb.bank_name='" + bankName + "'";
		List<CommonBean> bankAccNoList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean bankAccNo = new CommonBean();
				bankAccNo.setId(rs.getString("pb.banking_id"));
				String accountType = rs.getString("pb.account_type"), currency="";
				if(accountType.equals("1")){
					currency = "INR";
				}
				else if(accountType.equals("2")){
					currency = rs.getString("lgv.generic_value");
				}
				bankAccNo.setName(rs.getString("pb.account_number") + " - " + currency);
				return bankAccNo;
			}
			
		});
		return bankAccNoList;
	}

	@Override
	public List<RequirementBean> getDeductionDetails(String projectSummaryId) {
		try{
			String sql = "SELECT project_deduction_id, survey_id, country_id, services_id, description, reason," +
						" quantity, unit_price FROM pasmt_project_deduction WHERE status=1 AND source_id=" + 
						projectSummaryId;
			List<RequirementBean> deductionList = jdbcTemplate.query(sql, new DeductionListMapper());
			if(deductionList != null && deductionList.size()>0){
				deductionList = convertDeductionList(deductionList);
			}
			
			return deductionList;
		}
		catch(Exception e){
			System.out.println("Exception in getDeductionDetails of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}
	
	public List<RequirementBean> convertDeductionList(List<RequirementBean> deductionList){
		String sql, temp;
		try{					
			for(RequirementBean requirement : deductionList){
				//get country
				if(requirement.getCountry()!= null && !requirement.getCountry().equals("-1")){
					temp = requirement.getCountry();
					if(temp.equals("0")){
						requirement.setCountry("All");
					}
					else{
						sql = "SELECT country_name FROM lu_country WHERE country_id=" + requirement.getCountry();
						temp = jdbcTemplate.queryForObject(sql, String.class);
						requirement.setCountry(temp);
					}
				}
				
				//get services
				if(requirement.getServices() != null){
					temp = requirement.getServices();
					if(temp.equals("1")){
						temp = "Data Processing";
					}
					else if(temp.equals("2")){
						temp = "Sample";
					}
					else if(temp.equals("3")){
						temp = "Survey Programming";
					}
					else if(temp.equals("4")){
						temp = "Translation";
					}
					requirement.setServices(temp);
				}
				
				//Get reason
				if(requirement.getReason() != null){
					temp = requirement.getReason();
					if(temp.equals("1")){
						requirement.setReason("Adjustment");
					}
					else if(temp.equals("2")){
						requirement.setReason("Discount");
					}
					else if(temp.equals("3")){
						requirement.setReason("Rejections");
					}
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in convertDeductionList of InvoiceReceivableDaoImpl.java: " + e);
			e.printStackTrace();
		}
		finally{
			sql = temp = null;
		}		
		return deductionList;
	}
}
