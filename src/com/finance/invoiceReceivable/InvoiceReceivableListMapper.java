package com.finance.invoiceReceivable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import org.springframework.jdbc.core.RowMapper;

public class InvoiceReceivableListMapper implements
		RowMapper<InvoiceReceivableBean> {

	@Override
	public InvoiceReceivableBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		DecimalFormat currencyFormat = new DecimalFormat("###,##0.00");
		InvoiceReceivableBean invoiceReceivable = new InvoiceReceivableBean();
		invoiceReceivable.setInvoiceReceivableId(rs.getString("ir_id"));
		invoiceReceivable.setClientCode(rs.getString("proposal_id"));
		invoiceReceivable.setMonth(rs.getString("month"));
		invoiceReceivable.setYear(rs.getString("year"));
		double amount =Double.parseDouble(rs.getString("total_amount"));
		invoiceReceivable.setAmount(currencyFormat.format(amount));
		invoiceReceivable.setRaisedOn(rs.getString("date_format(pir.raise_on, '%b %e, %Y')"));
		invoiceReceivable.setDueOn(rs.getString("date_format(pir.due_on, '%b %e, %Y')"));
		invoiceReceivable.setInvoiceCode(rs.getString("invoice_number"));
		String temp = rs.getString("payment_status");		
		int overDueBy = 0;
		if(temp != null && temp.equals("1")){
			if(rs.getString("overDueBy")!= null){
				overDueBy = Integer.parseInt(rs.getString("overDueBy"));
				if(overDueBy<0){
					overDueBy = 0;
				}
			}			
		}
		invoiceReceivable.setOverDueBy(String.valueOf(overDueBy));
		invoiceReceivable.setWaveMonth(rs.getString("wave"));
		//Set status string 1 - Pending, 2 - Paid, 3 - Declined
		if(temp != null){
			if(temp.equals("1")){
				invoiceReceivable.setPaymentStatus("Pending");
			}
			else if(temp.equals("2")){
				invoiceReceivable.setPaymentStatus("Paid");
			}
			else if(temp.equals("3")){
				invoiceReceivable.setPaymentStatus("Declined");
			}
			
		}
		//Set transaction type 1 - Local, 2 - International
		temp = "";
		if(rs.getString("transaction_type") != null){
			temp = rs.getString("transaction_type");
			if(temp.equals("1")){
				invoiceReceivable.setTransactionType("Local");
			}
			else if(temp.equals("2")){
				invoiceReceivable.setTransactionType("International");
			}
		}
		return invoiceReceivable;
	}

}
