package com.finance.invoiceReceivable;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.sales.proposal.ProposalBean;
import com.sales.proposal.ProposalDao;

@Controller
public class InvoiceReceivableController {
	
	@Autowired
	InvoiceReceivableDao invoiceReceivableDao;
	
	@Autowired
	ProposalDao proposalDao;
	
	@RequestMapping(value="/sAdminInvoiceReceivableList")
	public ModelAndView invoiceReceivableList(HttpServletRequest request,
			@ModelAttribute("searchInvoiceReceivable")InvoiceReceivableBean searchInvoiceReceivable){
		ModelAndView invoiceReceivable = new ModelAndView("finance/invoiceReceivable/invoiceReceivableList");
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		List<InvoiceReceivableBean> invoiceReceivableList = invoiceReceivableDao.invoiceReceivableList(
				searchInvoiceReceivable, request, startIndex, 15);
		invoiceReceivable.addObject("invoiceReceivableList", invoiceReceivableList);
		invoiceReceivableList = null;
		return invoiceReceivable;
	}
	
	@RequestMapping(value="/sAdminAddInvoiceReceivableForm")
	public ModelAndView addInvoiceReceivableForm(HttpServletRequest request){
		ModelAndView addInvoiceReceivable = new ModelAndView("finance/invoiceReceivable/addInvoiceReceivable");
		addInvoiceReceivable.addObject("projectSummaryList", invoiceReceivableDao.getProjectSummaryList());
		addInvoiceReceivable.addObject("bankList", invoiceReceivableDao.getBankList());
		InvoiceReceivableBean invoiceReceivable = new InvoiceReceivableBean();
		
		if(request.getParameter("projectSummaryId")!=null && !(request.getParameter("projectSummaryId").equals("0"))){
			String projectSummaryId = request.getParameter("projectSummaryId");
			invoiceReceivable.setProjectSummaryId(projectSummaryId);
			ProposalBean proposal = invoiceReceivableDao.getProjectInfo(projectSummaryId);
			addInvoiceReceivable.addObject("proposal", proposal);
			addInvoiceReceivable.addObject("clientContactList", 
					proposalDao.getContactList(proposal.getBid().getBidId(), 1));
			addInvoiceReceivable.addObject("deductionList", 
						invoiceReceivableDao.getDeductionDetails(projectSummaryId));
			proposal = null;		
		}
		else{
			addInvoiceReceivable.addObject("proposal", new ProposalBean());
		}
		addInvoiceReceivable.addObject("addInvoiceReceivable", invoiceReceivable);
		return addInvoiceReceivable;		
	}
	
	@RequestMapping(value="/sAdminAddInvoiceReceivable", method=RequestMethod.POST)
	public String addInvoiceReceivable(@ModelAttribute("addInvoiceReceivable") InvoiceReceivableBean invoiceReceivable,
			ModelMap model) {
		invoiceReceivableDao.addInvoiceReceivable(invoiceReceivable);
		return "redirect:/sAdminInvoiceReceivableList";
	}
	
	@RequestMapping(value="/sAdminEditInvoiceRecievableForm")
	public ModelAndView editInvoiceRecievableForm(HttpServletRequest request){
		ModelAndView editInvoiceReceivable = new ModelAndView("finance/invoiceReceivable/editInvoiceReceivable");
		editInvoiceReceivable.addObject("projectSummaryList", invoiceReceivableDao.getProjectSummaryList());
		editInvoiceReceivable.addObject("bankList", invoiceReceivableDao.getBankList());		
		InvoiceReceivableBean invoiceReceivable = 
				invoiceReceivableDao.getInvoiceReceivableInformation(request.getParameter("invoiceReceivableId"));
		editInvoiceReceivable.addObject("editInvoiceReceivable", invoiceReceivable);
		if(invoiceReceivable.getBankName() != null){
			editInvoiceReceivable.addObject("bankAccList", 
					invoiceReceivableDao.getBankAccNoList(invoiceReceivable.getBankName()));
		}
		if(invoiceReceivable.getReceivedBankName() != null){
			editInvoiceReceivable.addObject("receivedBankAccList", 
					invoiceReceivableDao.getBankAccNoList(invoiceReceivable.getReceivedBankName()));
		}
		ProposalBean proposal = invoiceReceivableDao.getProjectInfo(invoiceReceivable.getProjectSummaryId());
		editInvoiceReceivable.addObject("proposal", proposal);
		editInvoiceReceivable.addObject("clientContactList", 
				proposalDao.getContactList(proposal.getBid().getBidId(), 1));
		/*
		 * Add deduction details
		 * Added by Maneet Kaur - 16 June'14
		 */
		editInvoiceReceivable.addObject("deductionList", 
				invoiceReceivableDao.getDeductionDetails(invoiceReceivable.getProjectSummaryId()));
		return editInvoiceReceivable;
	}
	
	@RequestMapping(value="/sAdminEditInvoiceReceivable", method=RequestMethod.POST)
	public String editInvoiceReceivable(@ModelAttribute("editInvoiceReceivable") InvoiceReceivableBean invoiceReceivable,
			ModelMap model) {
		invoiceReceivableDao.editInvoiceReceivable(invoiceReceivable);
		return "redirect:/sAdminInvoiceReceivableList";
	}
	
	@RequestMapping(value="/sAdminDownloadInvoiceReceivable", method=RequestMethod.GET)
	public ModelAndView downloadInvoiceReceivable(HttpServletRequest request){
		ModelAndView downloadInvoiceReceivable = new ModelAndView("invoiceReceivablePdfView");
		String invoiceReceivableId = request.getParameter("invoiceReceivableId");
		InvoiceReceivableBean invoiceReceivable = 
				invoiceReceivableDao.getInvoiceReceivableInformation(invoiceReceivableId);
		downloadInvoiceReceivable.addObject("editInvoiceReceivable", invoiceReceivable);
		ProposalBean proposal = invoiceReceivableDao.getProjectInfo(invoiceReceivable.getProjectSummaryId());
		downloadInvoiceReceivable.addObject("proposal", proposal);
		//get the details of the invoiced to client's contact
		downloadInvoiceReceivable.addObject("clientContact", 
				invoiceReceivableDao.getClientContact(invoiceReceivable.getInvoiceTo(), 1));
		String clientId = proposal.getBid().getBidClient();
		downloadInvoiceReceivable.addObject("clientSalesContact", 
				invoiceReceivableDao.getClientContact(clientId, 2));
		//get the banking information
		downloadInvoiceReceivable.addObject("remittanceInstruction", 
				invoiceReceivableDao.getRemittanceInstruction(invoiceReceivable.getBank_detail_id(), clientId, 
					invoiceReceivable.getTransactionType()));
		
		//Deduction Details
		downloadInvoiceReceivable.addObject("deductionList", 
				invoiceReceivableDao.getDeductionDetails(invoiceReceivable.getProjectSummaryId()));
		clientId = null;
		return downloadInvoiceReceivable;
	}
	
	@RequestMapping(value="/getBankAccList", method=RequestMethod.GET)
	public String getBankAccList(HttpServletRequest request, ModelMap model){
		List<CommonBean> bankAccList = invoiceReceivableDao.getBankAccNoList(request.getParameter("bankName"));
		model.addAttribute("bankAccList", bankAccList);
		return "ajaxOperation/ajaxOperationFinance";
	}
}
