package com.finance.invoiceReceivable;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EditIRMapper implements RowMapper<InvoiceReceivableBean> {

	@Override
	public InvoiceReceivableBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {		
		InvoiceReceivableBean invoiceReceivable = new InvoiceReceivableBean();
		invoiceReceivable.setProjectSummaryId(rs.getString("pir.project_summary_id"));
		invoiceReceivable.setInvoiceTo(rs.getString("client_contact_id"));
		invoiceReceivable.setBank_detail_id(rs.getString("pir.banking_id"));
		invoiceReceivable.setMonth(rs.getString("month"));
		invoiceReceivable.setYear(rs.getString("year"));
		invoiceReceivable.setClientPO(rs.getString("client_po_number"));
		invoiceReceivable.setWaveMonth(rs.getString("pps.wave"));
		invoiceReceivable.setTransactionType(rs.getString("transaction_type"));
		invoiceReceivable.setDescription(rs.getString("service_description"));
		invoiceReceivable.setServiceTaxApplicable(rs.getString("service_tax_applicable"));
		invoiceReceivable.setServiceTax(rs.getString("service_tax_rate"));
		invoiceReceivable.setAmount(rs.getString("total_amount"));
		invoiceReceivable.setAmountUsd(rs.getString("eq_usd_total_amount"));
		invoiceReceivable.setPaymentMode(rs.getString("payment_mode"));
		invoiceReceivable.setPaymentTerms(rs.getString("payment_term"));
		invoiceReceivable.setInvoiceCode(rs.getString("invoice_number"));
		invoiceReceivable.setRaisedOn(rs.getString("date_format(raise_on, '%b %e, %Y')"));
		invoiceReceivable.setDueOn(rs.getString("date_format(due_on, '%b %e, %Y')"));
		invoiceReceivable.setPaymentStatus(rs.getString("payment_status"));
		invoiceReceivable.setInvoiceReceivableId(rs.getString("invoice_receivable_id"));
		invoiceReceivable.setAgingFor(rs.getString("aging"));
		int overDueBy = 0;
		if(invoiceReceivable.getPaymentStatus() != null && invoiceReceivable.getPaymentStatus().equals("1")){
			if(rs.getString("overDueBy")!= null){
				overDueBy = Integer.parseInt(rs.getString("overDueBy"));
				if(overDueBy<0){
					overDueBy = 0;
				}
			}			
		}
		invoiceReceivable.setOverDueBy(String.valueOf(overDueBy));
		invoiceReceivable.setPaymentMadeOn(rs.getString("DATE(payment_made_on)"));
		invoiceReceivable.setPaymentModeReceived(rs.getString("received_payment_mode"));
		invoiceReceivable.setReceivedBankId(rs.getString("received_banking_id"));
		invoiceReceivable.setTransactionId(rs.getString("transaction_id"));
		invoiceReceivable.setNetBankingNo(rs.getString("net_banking_number"));
		invoiceReceivable.setChequeNo(rs.getString("cheque_number"));
		invoiceReceivable.setProjectNumber(rs.getString("pps.project_id"));
		invoiceReceivable.setPaymentRemarks(rs.getString("payment_remarks"));
		invoiceReceivable.setBankName(rs.getString("pb.bank_name"));
		return invoiceReceivable;
	}

}
