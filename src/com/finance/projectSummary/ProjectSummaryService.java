package com.finance.projectSummary;

import java.util.ArrayList;
import java.util.List;

import com.sales.bid.RequirementBean;

public class ProjectSummaryService {
	public ProjectSummaryBean removeEmptyRows(ProjectSummaryBean projectSummary) {
		//Remove empty income rows
		if(projectSummary.getIncomeList().size()==1){
			RequirementBean tempReqmt = projectSummary.getIncomeList().get(0);
			//if all the fields are empty
			if(tempReqmt.getSurveyId().equals("") && tempReqmt.getCountry().equals("0") &&
				tempReqmt.getServices().equals("0") && tempReqmt.getTargetAudience().equals("0") &&
				tempReqmt.getDescription().equals("") && tempReqmt.getSampleSize().equals("") &&
				tempReqmt.getQuotedCpi().equals("") && tempReqmt.getSetupFee().equals("")){
				projectSummary.setIncomeList(null);
			}
		}
		else{
			List<RequirementBean> tempIncomes = new ArrayList<RequirementBean>();
			for(RequirementBean tempReqmt: projectSummary.getIncomeList()){
				/*
				 * if the row has been removed, every element of the row becomes null. 
				 * So, check if the first element is not null
				 */
				if(tempReqmt.getSurveyId() != null){
					if(!(tempReqmt.getSurveyId().equals("") && tempReqmt.getCountry().equals("0") &&
						tempReqmt.getServices().equals("0") && tempReqmt.getTargetAudience().equals("0") &&
						tempReqmt.getDescription().equals("") && tempReqmt.getSampleSize().equals("") &&
						tempReqmt.getQuotedCpi().equals("") && tempReqmt.getSetupFee().equals(""))){
						tempIncomes.add(tempReqmt);
					}
				}
			}
			projectSummary.setIncomeList(tempIncomes);
		}
		
		//Remove empty expense rows
		if(projectSummary.getExpenseList().size()==1){
			ExpenseDetailBean expenseDetail = projectSummary.getExpenseList().get(0);
			//if all the fields are empty
			if(expenseDetail.getExpenseOn().equals("0") && expenseDetail.getExpenseType().equals("0") &&
				expenseDetail.getVendorName().equals("-1") && expenseDetail.getQuantity().equals("") &&
				expenseDetail.getUnitPrice().equals("") && expenseDetail.getProjectMinimum().equals("") &&
				expenseDetail.getSetupFee().equals("") && expenseDetail.getSurveyId().equals("") &&
				expenseDetail.getCountry().equals("0")){
					projectSummary.setExpenseList(null);
			}
		}
		else{
			List<ExpenseDetailBean> expenseDetails = new ArrayList<ExpenseDetailBean>();
			for(ExpenseDetailBean expenseDetail: projectSummary.getExpenseList()){
				if(expenseDetail.getExpenseOn()!=null){
					if(!(expenseDetail.getExpenseOn().equals("0") && expenseDetail.getExpenseType().equals("0") &&
						expenseDetail.getVendorName().equals("-1") && expenseDetail.getQuantity().equals("") &&
						expenseDetail.getUnitPrice().equals("") && expenseDetail.getProjectMinimum().equals("") &&
						expenseDetail.getSetupFee().equals("") && expenseDetail.getSurveyId().equals("") &&
						expenseDetail.getCountry().equals("0"))){
							expenseDetails.add(expenseDetail);
					}
				}
			}
			projectSummary.setExpenseList(expenseDetails);
		}
		
		/*
		 * Remove empty deduction rows
		 * Added by Maneet - 19 May'14
		 */
		if(projectSummary.getDeductionList().size()==1){
			RequirementBean tempReqmt = projectSummary.getDeductionList().get(0);
			//if all the fields are empty
			if(tempReqmt.getSurveyId().equals("") && tempReqmt.getCountry().equals("-1") &&
				tempReqmt.getServices().equals("0") && tempReqmt.getDescription().equals("") && 
				tempReqmt.getReason().equals("0") && tempReqmt.getSampleSize().equals("") &&
				tempReqmt.getQuotedCpi().equals("")){
				projectSummary.setDeductionList(null);
			}
		}
		else{
			List<RequirementBean> tempDeductions = new ArrayList<RequirementBean>();
			for(RequirementBean tempReqmt: projectSummary.getDeductionList()){
				/*
				 * if the row has been removed, every element of the row becomes null. 
				 * So, check if the first element is not null
				 */
				if(tempReqmt.getSurveyId() != null){
					if(!(tempReqmt.getSurveyId().equals("") && tempReqmt.getCountry().equals("-1") &&
						tempReqmt.getServices().equals("0") && tempReqmt.getDescription().equals("") && 
						tempReqmt.getReason().equals("0") && tempReqmt.getSampleSize().equals("") &&
						tempReqmt.getQuotedCpi().equals(""))){
						tempDeductions.add(tempReqmt);
					}
				}
			}
			projectSummary.setDeductionList(tempDeductions);
		}
		return projectSummary;
	}
}
