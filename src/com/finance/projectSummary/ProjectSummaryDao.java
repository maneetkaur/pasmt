package com.finance.projectSummary;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;

public interface ProjectSummaryDao {
	public List<ProjectSummaryBean> projectSummaryList(ProjectSummaryBean projectSummary, 
			HttpServletRequest request, int startIndex, int range);
	
	public List<CommonBean> getProjectList();
	
	public void addProjectSummary(ProjectSummaryBean projectSummary);
	
	public String getProjectDetails(String sourceId, int from);
	
	public ProjectSummaryBean getProjectSummaryInformation(String projectSummaryId);
	
	public void editProjectSummary(ProjectSummaryBean projectSummary);
}
