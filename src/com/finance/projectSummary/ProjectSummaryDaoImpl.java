package com.finance.projectSummary;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;
import com.sales.bid.RequirementBean;

@Repository
@Transactional
public class ProjectSummaryDaoImpl implements ProjectSummaryDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	CommonFunction commonFunction;

	@Override
	public List<ProjectSummaryBean> projectSummaryList(
			ProjectSummaryBean projectSummary, HttpServletRequest request,
			int startIndex, int range) {
		try{
			StringBuffer where_clause;
			String temp;
			String sql = "SELECT pps.project_summary_id, pp.proposal_number, pp.bid_number, pps.closing_month, " +
					"pps.closing_year, pps.project_value, pps.project_expence, pps.wave FROM pasmt_project_summary pps "
					+ "JOIN lu_project lp ON pps.project_id=lp.project_id JOIN pasmt_proposal pp ON lp.proposal_id" +
					"=pp.proposal_id ";
			where_clause = new StringBuffer("WHERE pps.status=1 AND ");
			if(projectSummary.getProjectId() != null && !projectSummary.getProjectId().equals("")){
				where_clause.append("pp.proposal_number like '%" + projectSummary.getProjectId() + "%' AND ");
			}
			
			if(projectSummary.getClosingMonth()!=null && !projectSummary.getClosingMonth().equals("")){
				String[] monthNYear = projectSummary.getClosingMonth().split("-");
				temp = commonFunction.changeMonthToNumber(monthNYear[0]);
				where_clause.append("pps.closing_month like '%" + temp + "%' AND " + 
						"pps.closing_year like '%" + monthNYear[1] + "%' AND ");
			}
			
			String where=where_clause.toString().trim();
			where_clause = null;
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			
			//Calculate count for pagination
			String sql_count = "SELECT count(1) FROM pasmt_project_summary pps " +
					"JOIN lu_project lp ON pps.project_id=lp.project_id JOIN pasmt_proposal pp ON lp.proposal_id" +
					"=pp.proposal_id ";
			sql_count = sql_count + where;
			int total_rows = jdbcTemplate.queryForInt(sql_count);		
			request.setAttribute("total_rows", total_rows);
			
			String limit = " ORDER BY pps.project_summary_id DESC LIMIT " + startIndex + ", " + range;
			sql = sql + where + limit;	
			List<ProjectSummaryBean> projectSummaryList = jdbcTemplate.query(sql, new ProjectSummaryListMapper());
			
			Map<String, String> billingCurrencyMap = 
					commonFunction.createMapFromList(commonFunction.getBillingCurrency());
			for(ProjectSummaryBean projectSummaryTemp : projectSummaryList){
				//get month from number and concatenate and month and year into one
				if(projectSummaryTemp.getClosingMonth()!=null && projectSummaryTemp.getClosingYear() != null){
					temp = commonFunction.changeMonthToWords(projectSummaryTemp.getClosingMonth());
					projectSummaryTemp.setClosingMonth(temp+"-"+projectSummaryTemp.getClosingYear());
				}
				
				sql = "SELECT pc.client_code, pe.employee_name, pb.billing_currency_id FROM pasmt_bid pb JOIN" +
						" pasmt_client pc ON " +
						"pb.client_id=pc.client_id JOIN pasmt_employee pe ON pb.account_manager_id=pe.employee_id " +
						"WHERE pb.bid_id=" + projectSummaryTemp.getClientCode();
				Map<String, Object> map = new HashMap<String, Object>();
				map = jdbcTemplate.queryForMap(sql);
				projectSummaryTemp.setClientCode(map.get("client_code").toString());
				projectSummaryTemp.setAccountManager(map.get("employee_name").toString());
				temp = map.get("billing_currency_id").toString();
				projectSummaryTemp.setBillingCurrency(billingCurrencyMap.get(temp));
			}
			return projectSummaryList;
		}
		catch(Exception e){
			System.out.println("Exception in projectSummaryList of ProjectSummaryDaoImpl.java: " + e);
		}		
		return null;
	}

	@Override
	public void addProjectSummary(ProjectSummaryBean projectSummary) {
		try{
			StringBuffer sb;
			String sql = "INSERT INTO pasmt_project_summary (project_id, closing_month, closing_year, wave, " +
					"project_value, project_expence, status, created_on) VALUES (?,?,?,?, ?,?,?,?)";
			if(projectSummary.getClosingMonth() != null){
				String temp[] = projectSummary.getClosingMonth().split("-");
				projectSummary.setClosingMonth(commonFunction.changeMonthToNumber(temp[0]));
				projectSummary.setClosingYear(temp[1]);
			}
			//Remove comma from project value and expense
			String temp = projectSummary.getProjectValue();
			temp = temp.replace(",","");
			projectSummary.setProjectValue(temp);
			
			temp = projectSummary.getProjectExpense();
			temp = temp.replace(",","");
			projectSummary.setProjectExpense(temp);
			jdbcTemplate.update(sql, new Object[]{projectSummary.getProjectId(), projectSummary.getClosingMonth(),
				projectSummary.getClosingYear(), projectSummary.getWaveMonth(), projectSummary.getProjectValue(),
				projectSummary.getProjectExpense(), 1, new Date()});
			
			//Get the project summary id of the latest entry
			sql = "SELECT project_summary_id FROM pasmt_project_summary WHERE project_id=" + 
					projectSummary.getProjectId() + " ORDER BY project_summary_id DESC LIMIT 1";
			String projectSummaryId = jdbcTemplate.queryForObject(sql, String.class);
			
			//Insert the income details, if any
			if(projectSummary.getIncomeList() != null){
				List<RequirementBean> incomeList = projectSummary.getIncomeList();
				if(incomeList.size()>0){
					sb = new StringBuffer("INSERT INTO pasmt_sales_requirement (source_id, country_id, services_id," +
							" target_audience, description, sample_size, quoted_cpi, setup_fee, survey_id, source," +
							" status, created_on) VALUES ");
					for(RequirementBean income: incomeList){
						/*change sample size to 0 if service is 2*/
						if(!income.getServices().equals("2")){
							income.setSampleSize("0");
						}
						sb = sb.append("(" + projectSummaryId + "," + income.getCountry() + "," + income.getServices()
								+ "," + income.getTargetAudience() + ",'" + income.getDescription() + "', '" +
								income.getSampleSize() + "', '" + income.getQuotedCpi() + "', '" + income.getSetupFee()
								+"', '" + income.getSurveyId() + "',3,1, now()), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
					incomeList = null;
				}
			}
			
			//Insert the expense details, if any
			if(projectSummary.getExpenseList() != null){
				List<ExpenseDetailBean> expenseList = projectSummary.getExpenseList();
				if(expenseList.size()>0){
					sb = new StringBuffer("INSERT INTO pasmt_project_expence (project_summary_id, vendor_id, " +
							"expence_on, expence_type, quantity, unit_price, project_minimum, setup_fee, " +
							"survey_id, country_id, status, created_on) VALUES ");
					for(ExpenseDetailBean expense : expenseList){
						sb = sb.append("(" + projectSummaryId + "," + expense.getVendorName() + "," + 
								expense.getExpenseOn() + "," + expense.getExpenseType() + ",'" + expense.getQuantity()
								+ "', '" + expense.getUnitPrice() + "', '" + expense.getProjectMinimum() + "', '" + 
								expense.getSetupFee() + "', '"  + expense.getSurveyId() + "'," + expense.getCountry()
								+ ",1, now()), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
					expenseList = null;
				}
			}
			
			/*
			 * Added by Maneet for deduction - 19 May'14
			 * Insert the deduction details, if any
			 */
			if(projectSummary.getDeductionList() != null){
				List<RequirementBean> deductionList = projectSummary.getDeductionList();
				if(deductionList.size() > 0){
					sb = new StringBuffer("INSERT INTO pasmt_project_deduction (source_id, survey_id, country_id," +
							" services_id, description, reason, quantity, unit_price, status, created_on) VALUES ");
					for(RequirementBean deduction: deductionList){
						sb = sb.append("(" + projectSummaryId + ", '" + deduction.getSurveyId() + "'," + 
								deduction.getCountry() + "," + deduction.getServices() + ", '" + 
								deduction.getDescription() + "'," + deduction.getReason() + "," + 
								deduction.getSampleSize() + ", '" + deduction.getQuotedCpi() +"', 1, now()), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
					deductionList = null;
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in addProjectSummary of ProjectSummaryDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
				
	}

	@Override
	public String getProjectDetails(String sourceId, int from) {
		try{
			String temp="",billingCurrency="", projectMinimum="0", sql;
			Map<String, Object> projectMap;
			if(from==1){
				sql = "SELECT pp.bid_number, pp.project_minimum FROM lu_project lp JOIN pasmt_proposal pp ON" +
						" lp.proposal_id=pp.proposal_id WHERE lp.project_id=" + sourceId;
				projectMap = jdbcTemplate.queryForMap(sql);
				temp = projectMap.get("bid_number").toString();
				projectMinimum = projectMap.get("project_minimum").toString();
			}
			else if(from==2){
				String tempArray[] = sourceId.split(";");
				temp = tempArray[0];
				projectMinimum = tempArray[1];
			}
			if(projectMinimum != null && !projectMinimum.equals("")){
				DecimalFormat currencyFormat = new DecimalFormat("###,##0.00");
				float pm = Float.parseFloat(projectMinimum);
				projectMinimum = currencyFormat.format(pm);
				currencyFormat = null;
			}
			
			sql = "SELECT pb.bid_name, pb.project_type, pe.employee_name, pb.billing_currency_id, pc.client_name" +
					" FROM pasmt_bid pb JOIN pasmt_client pc ON pb.client_id=pc.client_id JOIN pasmt_employee pe" +
					" ON pb.account_manager_id=pe.employee_id WHERE pb.bid_id=" + temp;			
			projectMap = jdbcTemplate.queryForMap(sql);
			if(projectMap.get("billing_currency_id")!=null){
				temp = projectMap.get("billing_currency_id").toString();
				
				sql = "SELECT generic_value FROM lu_generic_values WHERE generic_value_id=" + temp;
				billingCurrency= jdbcTemplate.queryForObject(sql, String.class);				
			}
			if(projectMap.get("project_type")!=null){
				temp = projectMap.get("project_type").toString();
				if(temp.equals("1")){
					temp = "AdHoc";
				}
				else if(temp.equals("2")){
					temp = "Tracker";
				}
			}
			temp = projectMap.get("client_name").toString() + ";" + projectMap.get("bid_name").toString() + ";" + 
					temp + ";" + projectMap.get("employee_name").toString() + ";" + billingCurrency + ";" + 
					projectMinimum;
			billingCurrency = projectMinimum = sql = null;
			
			return temp;
			
		}
		catch(Exception e){
			System.out.println("Exception in getProjectDetails of ProjectSummaryDaoImpl.java: " + e);
		}
		return null;
	}

	@Override
	public ProjectSummaryBean getProjectSummaryInformation(String projectSummaryId) {
		try{
			String sql = "SELECT pps.project_summary_id, pp.proposal_number, pps.closing_month, pps.closing_year," +
					" pps.wave, pps.project_value, pps.project_expence, pp.bid_number, pp.project_minimum FROM" +
					" pasmt_project_summary pps JOIN lu_project lp ON lp.project_id=pps.project_id JOIN" +
					" pasmt_proposal pp ON lp.proposal_id=pp.proposal_id WHERE pps.project_summary_id=" + 
					projectSummaryId;
			ProjectSummaryBean projectSummary = jdbcTemplate.queryForObject(sql, new EditProjectSummaryMapper());
			
			//get closing month as month-year
			projectSummary.setClosingMonth(commonFunction.changeMonthToWords(projectSummary.getClosingMonth()) + "-"
					+ projectSummary.getClosingYear());
			sql = "SELECT bid_requirement_id, country_id, services_id, target_audience, description, sample_size," +
					" quoted_cpi, setup_fee, survey_id FROM pasmt_sales_requirement WHERE status=1 AND source=3 AND" +
					" source_id=" + projectSummaryId;
			List<RequirementBean> requirementList = jdbcTemplate.query(sql, new IncomeListMapper());
			projectSummary.setIncomeList(requirementList);
			
			sql = "SELECT project_expence_id, vendor_id, expence_on, expence_type, quantity, unit_price," +
					" project_minimum, setup_fee, survey_id, country_id FROM pasmt_project_expence WHERE status=1" +
					" AND project_summary_id=" + projectSummaryId;
			List<ExpenseDetailBean> expenseList = jdbcTemplate.query(sql, new ExpenseListMapper());
			projectSummary.setExpenseList(expenseList);
			projectSummary.setIncomeSize(String.valueOf(requirementList.size()));
			projectSummary.setExpenseSize(String.valueOf(expenseList.size()));
		
			 /* Changes for deduction list - Added by Maneet -19 May'14
			 */
			sql = "SELECT project_deduction_id, survey_id, country_id, services_id, description, reason, quantity," +
					" unit_price FROM pasmt_project_deduction WHERE status=1 AND source_id=" + projectSummaryId;
			requirementList = jdbcTemplate.query(sql, new DeductionListMapper());
			projectSummary.setDeductionList(requirementList);
			projectSummary.setDeductionSize(String.valueOf(requirementList.size()));
			
			 /* Changes for deduction list ends*/
			requirementList = null;
			expenseList = null;
			return projectSummary;
		}
		catch(Exception e){
			System.out.println("Exception in getProjectSummaryInformation of ProjectSummaryDaoImpl.java: " + e);
		}
		return null;
	}

	@Override
	public void editProjectSummary(ProjectSummaryBean projectSummary) {
		try{
			StringBuffer sb;
			String sql = "UPDATE pasmt_project_summary SET closing_month=?, closing_year=?, wave=?, project_value=?" +
					", project_expence=? WHERE project_summary_id=?";
			if(projectSummary.getClosingMonth() != null){
				String temp[] = projectSummary.getClosingMonth().split("-");
				projectSummary.setClosingMonth(commonFunction.changeMonthToNumber(temp[0]));
				projectSummary.setClosingYear(temp[1]);
			}
			//Remove comma from project value and expense
			String temp = projectSummary.getProjectValue();
			temp = temp.replace(",","");
			projectSummary.setProjectValue(temp);
			
			temp = projectSummary.getProjectExpense();
			temp = temp.replace(",","");
			projectSummary.setProjectExpense(temp);
			jdbcTemplate.update(sql, new Object[]{projectSummary.getClosingMonth(), projectSummary.getClosingYear(),
				projectSummary.getWaveMonth(), projectSummary.getProjectValue(), projectSummary.getProjectExpense(),
				projectSummary.getProjectSummaryId()});
			
			if(projectSummary.getIncomeList() != null){
				int currentSize = Integer.parseInt(projectSummary.getIncomeSize());
				List<RequirementBean> incomeList = projectSummary.getIncomeList();
				RequirementBean newIncome;
				
				//Update the existing rows if there are changes in any
				if(projectSummary.getIncomeIdList().length()>0){
					String incomeArray[] = projectSummary.getIncomeIdList().split(",");
					if(currentSize>0 && incomeArray.length>0){
						for(int i=0; i<incomeArray.length; i++){
							newIncome = incomeList.get(Integer.parseInt(incomeArray[i]));
							//Change quantity to 0 if service is 2
							if(!newIncome.getServices().equals("2")){
								newIncome.setSampleSize("0");
							}
							sql = "UPDATE pasmt_sales_requirement SET country_id=?, services_id=?, target_audience=?" +
									", description=?, sample_size=?, quoted_cpi=?, setup_fee=?, survey_id=? WHERE " +
									"bid_requirement_id=?";
							jdbcTemplate.update(sql, new Object[]{newIncome.getCountry(), newIncome.getServices(),
								newIncome.getTargetAudience(), newIncome.getDescription(), newIncome.getSampleSize(),
								newIncome.getQuotedCpi(), newIncome.getSetupFee(), newIncome.getSurveyId(),
								newIncome.getRequirementId()});
						}
					}
				}
				//Insert the newly added income rows
				if(incomeList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_sales_requirement (source_id, country_id, services_id," +
							" target_audience, description, sample_size, quoted_cpi, setup_fee, survey_id, source," +
							" status, created_on) VALUES ");
					for(int i=currentSize; i<incomeList.size(); i++){
						RequirementBean income = incomeList.get(i);
						/*change sample size to 0 if service is 2*/
						if(!income.getServices().equals("2")){
							income.setSampleSize("0");
						}
						sb = sb.append("(" + projectSummary.getProjectSummaryId() + "," + income.getCountry() + ","
								+ income.getServices()
								+ "," + income.getTargetAudience() + ",'" + income.getDescription() + "', '" +
								income.getSampleSize() + "', '" + income.getQuotedCpi() + "', '" + income.getSetupFee()
								+"', '" + income.getSurveyId() + "',3,1, now()), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
					incomeList = null;
				}
			}
			
			//Expense
			if(projectSummary.getExpenseList() != null){
				int currentSize = Integer.parseInt(projectSummary.getExpenseSize());
				List<ExpenseDetailBean> expenseList = projectSummary.getExpenseList();
				ExpenseDetailBean newExpense;
				
				//Update the existing rows if there are changes in any
				if(projectSummary.getExpenseIdList().length()>0){
					String expenseArray[] = projectSummary.getExpenseIdList().split(",");
					if(currentSize>0 && expenseArray.length>0){
						for(int i=0; i<expenseArray.length; i++){
							newExpense = expenseList.get(Integer.parseInt(expenseArray[i]));
							
							sql = "UPDATE pasmt_project_expence SET vendor_id=?, expence_on=?, expence_type=?, " +
								"quantity=?, unit_price=?, project_minimum=?, setup_fee=?, survey_id=?, " +
								"country_id=? WHERE project_expence_id=?";
							jdbcTemplate.update(sql, new Object[]{newExpense.getVendorName(), newExpense.getExpenseOn()
								, newExpense.getExpenseType(), newExpense.getQuantity(), newExpense.getUnitPrice(),
								newExpense.getProjectMinimum(), newExpense.getSetupFee(), newExpense.getSurveyId(),
								newExpense.getCountry(), newExpense.getExpenseId()});
						}
					}
				}
				//Insert the newly added expense rows
				if(expenseList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_project_expence (project_summary_id, vendor_id, " +
							"expence_on, expence_type, quantity, unit_price, project_minimum, setup_fee, " +
							"survey_id, country_id, status, created_on) VALUES ");
					for(int i=currentSize; i<expenseList.size(); i++){
						ExpenseDetailBean expense = expenseList.get(i);
						sb = sb.append("(" + projectSummary.getProjectSummaryId() + "," + expense.getVendorName() +
								"," + expense.getExpenseOn() + "," + expense.getExpenseType() + ",'" + 
								expense.getQuantity() + "', '" + expense.getUnitPrice() + "', '" + 
								expense.getProjectMinimum() + "', '" + expense.getSetupFee() + "', '" +
								expense.getSurveyId() + "'," + expense.getCountry() + ",1, now()), ");
					}
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
					expenseList = null;
				}
			}
			
			/*
			 * Changed for Deduction Details - Maneet 20 May'14
			 */
			if(projectSummary.getDeductionList() != null){
				int currentSize = Integer.parseInt(projectSummary.getDeductionSize());
				List<RequirementBean> deductionList = projectSummary.getDeductionList();
				RequirementBean newDeduction;
				
				//Update the existing rows if there are changes in any
				if(projectSummary.getDeductionIdList().length() > 0){
					String deductionArray[] = projectSummary.getDeductionIdList().split(",");
					if(currentSize > 0 && deductionArray.length >0){
						for(int i=0; i<deductionArray.length; i++){
							newDeduction = deductionList.get(Integer.parseInt(deductionArray[i]));
														
							sql = "UPDATE pasmt_project_deduction SET survey_id=?, country_id=?, services_id=?, " +
									"description=?, reason=?, quantity=?, unit_price=? WHERE project_deduction_id=?";
							jdbcTemplate.update(sql, new Object[]{newDeduction.getSurveyId(), newDeduction.getCountry(),
									newDeduction.getServices(), newDeduction.getDescription(), newDeduction.getReason(),
									newDeduction.getSampleSize(), newDeduction.getQuotedCpi(), 
									newDeduction.getRequirementId()});
						}
					}
				}
				
				//Insert the newly added deduction rows
				if(deductionList.size()>currentSize){
					sb = new StringBuffer("INSERT INTO pasmt_project_deduction (source_id, survey_id, country_id," +
							" services_id, description, reason, quantity, unit_price, status, created_on) VALUES ");
					for(int i=currentSize; i<deductionList.size(); i++){
						RequirementBean deduction = deductionList.get(i);
						sb = sb.append("(" + projectSummary.getProjectSummaryId() + ", '" + deduction.getSurveyId() + 
								"'," + deduction.getCountry() + "," + deduction.getServices() + ", '" + 
								deduction.getDescription() + "'," + deduction.getReason() + "," + 
								deduction.getSampleSize() + ", '" + deduction.getQuotedCpi() +"', 1, now()), ");
					}					
					sql = sb.toString();
					sb=null;
					if(sql.charAt(sql.length()-2)==','){
						sql = sql.substring(0, sql.length()-2);
					}
					jdbcTemplate.update(sql);
					deductionList = null;
				}
			}			
			sql = temp = null;
		}
		catch(Exception e){
			System.out.println("Exception in editProjectSummary of ProjectSummaryDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			e.printStackTrace();
		}
	}

	@Override
	public List<CommonBean> getProjectList() {
		try{
			String sql = "(select pp.project_id, pps.proposal_number " +
					"from lu_project pp, pasmt_proposal pps, pasmt_bid pb " +
					"where pp.proposal_id = pps.proposal_id and pb.bid_id = pps.bid_number and project_type = 2 " +
					"and (pp.project_status = 48 or pp.project_status = 45)) " +
					"union (select pp.project_id, pps.proposal_number " +
					"from pasmt_proposal pps, lu_project pp left join pasmt_project_summary ppps " +
					"on pp.project_id = ppps.project_id " +
					"where pp.proposal_id = pps.proposal_id and ppps.project_id is null " +
					"and pp.project_status = 48)";
			List<CommonBean> projectList =  jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

				@Override
				public CommonBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CommonBean project = new CommonBean();					
					project.setId(rs.getString("project_id"));
					project.setName(rs.getString("proposal_number"));
					return project;
				}				
			});
			return projectList;
		}
		catch(Exception e){
			System.out.println("Exception in getProjectList of ProjectSummaryDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}

}
