package com.finance.projectSummary;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import org.springframework.jdbc.core.RowMapper;

public class ProjectSummaryListMapper implements RowMapper<ProjectSummaryBean> {

	@Override
	public ProjectSummaryBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		DecimalFormat currencyFormat = new DecimalFormat("###,##0.00");
		ProjectSummaryBean projectSummaryBean = new ProjectSummaryBean();		
		projectSummaryBean.setProjectSummaryId(rs.getString("pps.project_summary_id"));
		projectSummaryBean.setProjectId(rs.getString("pp.proposal_number"));
		projectSummaryBean.setClientCode(rs.getString("pp.bid_number"));
		projectSummaryBean.setClosingMonth(rs.getString("pps.closing_month"));
		projectSummaryBean.setClosingYear(rs.getString("pps.closing_year"));
		projectSummaryBean.setProjectValue(rs.getString("pps.project_value"));
		projectSummaryBean.setProjectExpense(rs.getString("pps.project_expence"));
		String temp = "";
		if(projectSummaryBean.getProjectValue()!=null && projectSummaryBean.getProjectExpense()!= null){			
			float projectValue = Float.parseFloat(projectSummaryBean.getProjectValue());
			float projectExpense = Float.parseFloat(projectSummaryBean.getProjectExpense());
			float margin = (projectValue-projectExpense)/projectValue*100;
			DecimalFormat df=new DecimalFormat("#.##");
			temp = df.format(margin);
			projectSummaryBean.setProjectValue(currencyFormat.format(projectValue));
			projectSummaryBean.setProjectExpense(currencyFormat.format(projectExpense));
		}
		projectSummaryBean.setMargin(temp + " %");
		if(rs.getString("pps.wave")!= null){
			temp = rs.getString("pps.wave");
			if(temp.equals("null")){
				temp="";
			}
			projectSummaryBean.setWaveMonth(temp);
			
		}
		return projectSummaryBean;
	}

}
