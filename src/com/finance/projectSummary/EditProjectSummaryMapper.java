package com.finance.projectSummary;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

import org.springframework.jdbc.core.RowMapper;

public class EditProjectSummaryMapper implements RowMapper<ProjectSummaryBean> {

	@Override
	public ProjectSummaryBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		DecimalFormat currencyFormat = new DecimalFormat("###,##0.00");
		ProjectSummaryBean projectSummary = new ProjectSummaryBean();
		projectSummary.setProjectSummaryId(rs.getString("pps.project_summary_id"));
		projectSummary.setProjectId(rs.getString("pp.proposal_number"));
		projectSummary.setClosingMonth(rs.getString("pps.closing_month"));
		projectSummary.setClosingYear(rs.getString("pps.closing_year"));
		projectSummary.setWaveMonth(rs.getString("pps.wave"));
		projectSummary.setProjectValue(rs.getString("pps.project_value"));
		projectSummary.setProjectExpense(rs.getString("pps.project_expence"));
		if(projectSummary.getProjectValue() != null && projectSummary.getProjectExpense() != null){
			float projectValue = Float.parseFloat(projectSummary.getProjectValue());
			float projectExpense = Float.parseFloat(projectSummary.getProjectExpense());
			float margin = (projectValue-projectExpense)/projectValue*100;
			DecimalFormat df=new DecimalFormat("#.##");
			projectSummary.setMargin(df.format(margin));
			projectSummary.setProjectValue(currencyFormat.format(projectValue));
			projectSummary.setProjectExpense(currencyFormat.format(projectExpense));
		}
		
		String temp;
		//, 
		if(rs.getString("pp.project_minimum") != null){
			temp = rs.getString("pp.project_minimum");
		}
		else{
			temp="0.00";
		}
		projectSummary.setClientCode(rs.getString("pp.bid_number") + ";" + temp);
		return projectSummary;
	}

}
