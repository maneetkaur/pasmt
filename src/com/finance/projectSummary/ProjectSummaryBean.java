package com.finance.projectSummary;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

import com.sales.bid.RequirementBean;

public class ProjectSummaryBean {
	private String projectSummaryId, projectId, closingMonth, closingYear, projectValue, projectExpense, margin, 
	waveMonth;
	
	/*	fields for checking changes in income and expense list
	*	fields for checking changes in deduction list - Added by Maneet - 19 May'14
	*/
	private String incomeIdList, expenseIdList, expenseSize, incomeSize, deductionIdList, deductionSize;
	
	//fields required only for listing
	private String clientCode, accountManager, billingCurrency;
	
	private List<RequirementBean> incomeList;
	private List<ExpenseDetailBean> expenseList;
	
	/*
	 * Added by Maneet on 19 May'14, to add deduction details
	 */
	private List<RequirementBean> deductionList;
	
	public ProjectSummaryBean(){
		incomeList = LazyList.decorate(new ArrayList<RequirementBean>(), 
						new InstantiateFactory(RequirementBean.class));
		
		expenseList = LazyList.decorate(new ArrayList<ExpenseDetailBean>(), 
						new InstantiateFactory(ExpenseDetailBean.class));
		
		//added for deduction
		deductionList = LazyList.decorate(new ArrayList<RequirementBean>(), 
				new InstantiateFactory(RequirementBean.class));
	}
	
	public String getProjectSummaryId() {
		return projectSummaryId;
	}
	public void setProjectSummaryId(String projectSummaryId) {
		this.projectSummaryId = projectSummaryId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getClosingMonth() {
		return closingMonth;
	}
	public void setClosingMonth(String closingMonth) {
		this.closingMonth = closingMonth;
	}
	public String getClosingYear() {
		return closingYear;
	}
	public void setClosingYear(String closingYear) {
		this.closingYear = closingYear;
	}
	public String getProjectValue() {
		return projectValue;
	}
	public void setProjectValue(String projectValue) {
		this.projectValue = projectValue;
	}
	public String getProjectExpense() {
		return projectExpense;
	}
	public void setProjectExpense(String projectExpense) {
		this.projectExpense = projectExpense;
	}
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getAccountManager() {
		return accountManager;
	}
	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}
	public List<RequirementBean> getIncomeList() {
		return incomeList;
	}
	public void setIncomeList(List<RequirementBean> incomeList) {
		this.incomeList = incomeList;
	}
	public List<ExpenseDetailBean> getExpenseList() {
		return expenseList;
	}
	public void setExpenseList(List<ExpenseDetailBean> expenseList) {
		this.expenseList = expenseList;
	}

	public String getWaveMonth() {
		return waveMonth;
	}

	public void setWaveMonth(String waveMonth) {
		this.waveMonth = waveMonth;
	}

	public String getIncomeIdList() {
		return incomeIdList;
	}

	public void setIncomeIdList(String incomeIdList) {
		this.incomeIdList = incomeIdList;
	}

	public String getExpenseIdList() {
		return expenseIdList;
	}

	public void setExpenseIdList(String expenseIdList) {
		this.expenseIdList = expenseIdList;
	}

	public String getExpenseSize() {
		return expenseSize;
	}

	public void setExpenseSize(String expenseSize) {
		this.expenseSize = expenseSize;
	}

	public String getIncomeSize() {
		return incomeSize;
	}

	public void setIncomeSize(String incomeSize) {
		this.incomeSize = incomeSize;
	}

	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}

	public List<RequirementBean> getDeductionList() {
		return deductionList;
	}

	public void setDeductionList(List<RequirementBean> deductionList) {
		this.deductionList = deductionList;
	}

	public String getDeductionIdList() {
		return deductionIdList;
	}

	public void setDeductionIdList(String deductionIdList) {
		this.deductionIdList = deductionIdList;
	}

	public String getDeductionSize() {
		return deductionSize;
	}

	public void setDeductionSize(String deductionSize) {
		this.deductionSize = deductionSize;
	}
}
