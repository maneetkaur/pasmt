package com.finance.projectSummary;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.bid.RequirementBean;

public class DeductionListMapper implements RowMapper<RequirementBean>{

	@Override
	public RequirementBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		RequirementBean deduction = new RequirementBean();
		deduction.setRequirementId(rs.getString("project_deduction_id"));
		deduction.setSurveyId(rs.getString("survey_id"));
		deduction.setCountry(rs.getString("country_id"));
		deduction.setServices(rs.getString("services_id"));
		deduction.setDescription(rs.getString("description"));
		deduction.setReason(rs.getString("reason"));
		deduction.setSampleSize(rs.getString("quantity"));
		deduction.setQuotedCpi(rs.getString("unit_price"));
		return deduction;
	}
	
}
