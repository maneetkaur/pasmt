package com.finance.projectSummary;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sales.bid.RequirementBean;

public class IncomeListMapper implements RowMapper<RequirementBean>{

	@Override
	public RequirementBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		RequirementBean income = new RequirementBean();
		income.setRequirementId(rs.getString("bid_requirement_id"));
		income.setCountry(rs.getString("country_id"));
		income.setServices(rs.getString("services_id"));
		income.setTargetAudience(rs.getString("target_audience"));
		income.setDescription(rs.getString("description"));
		income.setSampleSize(rs.getString("sample_size"));
		income.setQuotedCpi(rs.getString("quoted_cpi"));
		income.setSetupFee(rs.getString("setup_fee"));
		income.setSurveyId(rs.getString("survey_id"));
		return income;
	}
	
}
