package com.finance.projectSummary;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class ProjectSummaryController {
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	ProjectSummaryDao projectSummaryDao;
	
	ProjectSummaryService projectSummaryService = new ProjectSummaryService();
	
	@RequestMapping(value="/projectSummaryList")
	public ModelAndView projectSummaryList(HttpServletRequest request, 
				@ModelAttribute("searchProjectSummary") ProjectSummaryBean searchProjectSummary){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView projectSummary = new ModelAndView("finance/projectSummary/projectSummaryList");
		List<ProjectSummaryBean> projectSummaryList = 
				projectSummaryDao.projectSummaryList(searchProjectSummary, request, startIndex, 15);
		projectSummary.addObject("projectSummaryList", projectSummaryList);
		projectSummary.addObject("managerList", commonFunction.getManagerList());
		projectSummaryList = null;
		return projectSummary;
	}
	
	@RequestMapping(value="/addProjectSummaryForm")
	public ModelAndView addProjectSummaryForm(){
		ModelAndView addProjectSummary = new ModelAndView("finance/projectSummary/addProjectSummary");
		addProjectSummary.addObject("addProjectSummary", new ProjectSummaryBean());
		addProjectSummary.addObject("projectList", projectSummaryDao.getProjectList());
		addProjectSummary.addObject("countryList", commonFunction.getAllCountryList());
		addProjectSummary.addObject("vendorList", commonFunction.getActiveVendorList());
		return addProjectSummary;
	}
	
	@RequestMapping(value="/getProjectDetails", method=RequestMethod.GET)
	public String getProjectDetails(HttpServletRequest request, ModelMap model) {
		String projectInfo = projectSummaryDao.getProjectDetails(request.getParameter("projectId"), 1);
		model.addAttribute("projectInfo", projectInfo);
		return "ajaxOperation/ajaxOperationFinance";
	}
	
	@RequestMapping(value="/addProjectSummary", method=RequestMethod.POST)
	public String addProjectSummary(@ModelAttribute("addProjectSummary")ProjectSummaryBean projectSummary, 
			ModelMap model) {
		projectSummaryService.removeEmptyRows(projectSummary);
		projectSummaryDao.addProjectSummary(projectSummary);
		return "redirect:/projectSummaryList";
	}
	
	@RequestMapping(value="/editProjectSummaryForm")
	public ModelAndView editProjectSummaryForm(HttpServletRequest request){
		ModelAndView addProjectSummary = new ModelAndView("finance/projectSummary/editprojectSummary");
		ProjectSummaryBean editProjectSummary = 
				projectSummaryDao.getProjectSummaryInformation(request.getParameter("projectSummaryId"));
		addProjectSummary.addObject("editProjectSummary", editProjectSummary);
		addProjectSummary.addObject("countryList", commonFunction.getAllCountryList());
		addProjectSummary.addObject("vendorList", commonFunction.getActiveVendorList());
		addProjectSummary.addObject("projectInfo", 
				projectSummaryDao.getProjectDetails(editProjectSummary.getClientCode(), 2));
		return addProjectSummary;
	}
	
	@RequestMapping(value="/editProjectSummary", method=RequestMethod.POST)
	public String editProjectSummary(@ModelAttribute("editProjectSummary")ProjectSummaryBean projectSummary, 
			ModelMap model) {
		projectSummaryService.removeEmptyRows(projectSummary);
		projectSummaryDao.editProjectSummary(projectSummary);
		return "redirect:/projectSummaryList";
	}
}
