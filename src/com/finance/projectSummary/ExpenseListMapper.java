package com.finance.projectSummary;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ExpenseListMapper implements RowMapper<ExpenseDetailBean> {

	@Override
	public ExpenseDetailBean mapRow(ResultSet rs, int rowNum)
			throws SQLException {
		ExpenseDetailBean expense = new ExpenseDetailBean();
		expense.setExpenseId(rs.getString("project_expence_id"));
		expense.setVendorName(rs.getString("vendor_id"));
		expense.setExpenseOn(rs.getString("expence_on"));
		expense.setExpenseType(rs.getString("expence_type"));
		expense.setQuantity(rs.getString("quantity"));
		expense.setUnitPrice(rs.getString("unit_price"));
		expense.setProjectMinimum(rs.getString("project_minimum"));
		expense.setSetupFee(rs.getString("setup_fee"));
		expense.setSurveyId(rs.getString("survey_id"));
		expense.setCountry(rs.getString("country_id"));
		return expense;
	}

}
