package com.finance.projectSummary;

public class ExpenseDetailBean {
	
	private String expenseId, expenseOn, expenseType, vendorName, currency, quantity, unitPrice, projectMinimum, 
		setupFee, surveyId, country;

	public String getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}

	public String getExpenseOn() {
		return expenseOn;
	}

	public void setExpenseOn(String expenseOn) {
		this.expenseOn = expenseOn;
	}

	public String getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getProjectMinimum() {
		return projectMinimum;
	}

	public void setProjectMinimum(String projectMinimum) {
		this.projectMinimum = projectMinimum;
	}

	public String getSetupFee() {
		return setupFee;
	}

	public void setSetupFee(String setupFee) {
		this.setupFee = setupFee;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
