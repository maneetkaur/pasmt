package com.finance.banking;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface BankigDetailDAO {
	
	public void addBanking(BankingBean bankingBean, HttpServletRequest request);
	public List bankList(HttpServletRequest request,BankingBean banking,int startIndex,int range);
	public BankingBean getBankingDetail(HttpServletRequest request);
	public void editBankingInfo(String bankId,BankingBean bankingBean);
	public String checkAccountNo(HttpServletRequest request);

}
