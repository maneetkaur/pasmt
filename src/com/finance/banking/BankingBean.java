package com.finance.banking;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

import com.sales.bid.RequirementBean;

public class BankingBean { 
	
	private String bankId, bankName,benefName,accountNo,ifsccode,accountType,accTypeCuurency;
	private String routingNO,swiftCode,sqrtCode,createdOn,status;
	private String physicalAdd,panNo,serviceTaxNo,tan,vatNo,interBankListSize,interBankIdList;
	private List<IntermediaryBankingBean> intermediayBankList;
	
	public BankingBean()
	{
		intermediayBankList= LazyList.decorate(new ArrayList<IntermediaryBankingBean>(), 
				new InstantiateFactory(IntermediaryBankingBean.class));
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBenefName() {
		return benefName;
	}

	public void setBenefName(String benefName) {
		this.benefName = benefName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfsccode() {
		return ifsccode;
	}

	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}

	public String getRoutingNO() {
		return routingNO;
	}

	public void setRoutingNO(String routingNO) {
		this.routingNO = routingNO;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getSqrtCode() {
		return sqrtCode;
	}

	public void setSqrtCode(String sqrtCode) {
		this.sqrtCode = sqrtCode;
	}

	public String getPhysicalAdd() {
		return physicalAdd;
	}

	public void setPhysicalAdd(String physicalAdd) {
		this.physicalAdd = physicalAdd;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getServiceTaxNo() {
		return serviceTaxNo;
	}

	public void setServiceTaxNo(String serviceTaxNo) {
		this.serviceTaxNo = serviceTaxNo;
	}

	public String getTan() {
		return tan;
	}

	public void setTan(String tan) {
		this.tan = tan;
	}

	public String getVatNo() {
		return vatNo;
	}

	public void setVatNo(String vatNo) {
		this.vatNo = vatNo;
	}

	public List<IntermediaryBankingBean> getIntermediayBankList() {
		return intermediayBankList;
	}

	public void setIntermediayBankList(
			List<IntermediaryBankingBean> intermediayBankList) {
		this.intermediayBankList = intermediayBankList;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInterBankListSize() {
		return interBankListSize;
	}

	public void setInterBankListSize(String interBankListSize) {
		this.interBankListSize = interBankListSize;
	}

	public String getInterBankIdList() {
		return interBankIdList;
	}

	public void setInterBankIdList(String interBankIdList) {
		this.interBankIdList = interBankIdList;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getAccTypeCuurency() {
		return accTypeCuurency;
	}

	public void setAccTypeCuurency(String accTypeCuurency) {
		this.accTypeCuurency = accTypeCuurency;
	}
	
	 
	

}
