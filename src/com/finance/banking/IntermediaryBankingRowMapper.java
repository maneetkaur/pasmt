package com.finance.banking;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class IntermediaryBankingRowMapper implements RowMapper<IntermediaryBankingBean>{
	
	@Override
	public IntermediaryBankingBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		IntermediaryBankingBean bean=new IntermediaryBankingBean();
		bean.setInterbankId(rs.getString("intermediary_banking_id"));
		bean.setInterBankName(rs.getString("bank_name"));
		bean.setInterBenefName(rs.getString("beneficiary_name"));
		bean.setInterAccountNo(rs.getString("account_number"));
		bean.setInterBankAdd(rs.getString("bank_addess"));
		bean.setInterRoutingNo(rs.getString("iban_number"));
		bean.setInterSwiftCode(rs.getString("swift_code"));
		bean.setInterSqrtCode(rs.getString("sort_code"));
		//bean.setInterPhysicalAdd(rs.getString("bank_physical_address"));
		bean.setCountryId(rs.getString("country_id"));
		
		return bean;
	}

}
