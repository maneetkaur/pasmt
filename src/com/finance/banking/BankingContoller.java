package com.finance.banking;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Controller
public class BankingContoller {
	
	@Autowired
	BankigDetailDAO bankingDao;
	
	@Autowired
	CommonFunction commonFumc;

	@RequestMapping(value="/sAdminBankingDetailList")
	public ModelAndView bankList(HttpServletRequest request, 
					@ModelAttribute("searchBank") BankingBean banking){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView bankModel = new ModelAndView("finance/banking/bankdetailList");		
		List<BankingBean> bankList = bankingDao.bankList(request, banking, startIndex, 15);
		bankModel.addObject("bankList", bankList);
		bankList=null;		
		return bankModel;
	}
	
	@RequestMapping(value="/sAdminAddBankingDetailForm")
	public ModelAndView addBankingDetail(){
		List<CommonBean> countryList=commonFumc.getAllCountryList();
		List<CommonBean> currencyList=commonFumc.getBillingCurrency();
		ModelAndView addBankingDetail = new ModelAndView("finance/banking/addBankingDetail");
		addBankingDetail.addObject("countryList", countryList);
		addBankingDetail.addObject("currencyList", currencyList);
		addBankingDetail.addObject("addBankingDetail", new BankingBean());
		countryList=null;
		currencyList=null;
		return addBankingDetail;
	}
	
	@RequestMapping(value="/sAdminSubmitBankingDetail",method=RequestMethod.POST)
	public String AddBanking(@ModelAttribute("addBankingDetail") BankingBean bankingBean,ModelMap model,HttpServletRequest request)
	{		
		bankingDao.addBanking(bankingBean,request);
		model.addAttribute("searchBank", new BankingBean());
		return "redirect:/sAdminBankingDetailList";
	}
	
	@RequestMapping(value="/sAdminEditBankForm",method=RequestMethod.GET)
	public ModelAndView updateCategory(HttpServletRequest request)
	{
		BankingBean bankingBean=bankingDao.getBankingDetail(request);
		List<CommonBean> countryList=commonFumc.getAllCountryList();
		List<CommonBean> currencyList=commonFumc.getBillingCurrency();
		ModelAndView model=new ModelAndView("finance/banking/editBankingDetail");
		model.addObject("editBankingDetail",bankingBean);
		model.addObject("countryList", countryList);
		model.addObject("currencyList", currencyList);
		currencyList=null;
		countryList=null;
		return model;
	}
	
	@RequestMapping(value="/sAdminEditBankingDetail",method=RequestMethod.POST)
	public String EditBankingDetail(@ModelAttribute("editBankingDetail")BankingBean bankingBean,HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String id=(String)session.getAttribute("bankId");
		bankingDao.editBankingInfo(id,bankingBean);
		return "redirect:/sAdminBankingDetailList";
	}
	
	@RequestMapping(value="/findAccountNoList",method=RequestMethod.GET)
	public String getAccountNo(HttpServletRequest request,ModelMap model)
	{
		String Target=bankingDao.checkAccountNo(request);
		model.addAttribute("checkAccountNo", Target);
		return "ajaxOperation/ajaxOperationFinance";
	}
}
