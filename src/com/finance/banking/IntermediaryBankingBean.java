package com.finance.banking;

public class IntermediaryBankingBean {

	private String interbankId,interBankName,interBenefName,interAccountNo;
	private String interBankAdd,interRoutingNo,interSwiftCode,interSqrtCode;
	private String interPhysicalAdd,countryId;
	
	public String getInterbankId() {
		return interbankId;
	}
	public void setInterbankId(String interbankId) {
		this.interbankId = interbankId;
	}
	public String getInterBankName() {
		return interBankName;
	}
	public void setInterBankName(String interBankName) {
		this.interBankName = interBankName;
	}
	public String getInterBenefName() {
		return interBenefName;
	}
	public void setInterBenefName(String interBenefName) {
		this.interBenefName = interBenefName;
	}
	public String getInterAccountNo() {
		return interAccountNo;
	}
	public void setInterAccountNo(String interAccountNo) {
		this.interAccountNo = interAccountNo;
	}
	public String getInterBankAdd() {
		return interBankAdd;
	}
	public void setInterBankAdd(String interBankAdd) {
		this.interBankAdd = interBankAdd;
	}
	public String getInterRoutingNo() {
		return interRoutingNo;
	}
	public void setInterRoutingNo(String interRoutingNo) {
		this.interRoutingNo = interRoutingNo;
	}
	public String getInterSwiftCode() {
		return interSwiftCode;
	}
	public void setInterSwiftCode(String interSwiftCode) {
		this.interSwiftCode = interSwiftCode;
	}
	public String getInterSqrtCode() {
		return interSqrtCode;
	}
	public void setInterSqrtCode(String interSqrtCode) {
		this.interSqrtCode = interSqrtCode;
	}
	public String getInterPhysicalAdd() {
		return interPhysicalAdd;
	}
	public void setInterPhysicalAdd(String interPhysicalAdd) {
		this.interPhysicalAdd = interPhysicalAdd;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	
	

	
}
