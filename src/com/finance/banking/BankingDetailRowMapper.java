package com.finance.banking;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BankingDetailRowMapper implements RowMapper<BankingBean>{
	
	@Override
	public BankingBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		BankingBean bean=new BankingBean();;
		bean.setBankId(rs.getString("banking_id"));
		bean.setBankName(rs.getString("bank_name"));
		bean.setBenefName(rs.getString("beneficiary_name"));
		bean.setAccountType(rs.getString("account_type"));
		bean.setAccTypeCuurency(rs.getString("eefc_currency_id"));
		bean.setAccountNo(rs.getString("account_number"));
		bean.setIfsccode(rs.getString("ifsc_code"));
		bean.setRoutingNO(rs.getString("iban_number"));
		bean.setSwiftCode(rs.getString("swift_code"));
		bean.setSqrtCode(rs.getString("sort_code"));
		bean.setPhysicalAdd(rs.getString("bank_physical_address"));
		bean.setPanNo(rs.getString("pan"));
		bean.setServiceTaxNo(rs.getString("service_tax_number"));
		bean.setTan(rs.getString("tan"));
		bean.setVatNo(rs.getString("vat_number"));
		//bean.setCreatedOn(rs.getString("date_format(created_on, '%b %e, %Y')"));
		//bean.setStatus(rs.getString("status"));
		return bean;
	}

}
