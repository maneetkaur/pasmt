package com.finance.banking;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;



@Repository
@Transactional
public class BankingDetailDAOImpl implements BankigDetailDAO {
	
	private JdbcTemplate jdbcTemplate;
	@Autowired		
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		//System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	@Override
	
	public List bankList(HttpServletRequest request,BankingBean banking,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		List<BankingBean> bankList=new ArrayList<BankingBean>();
		String sql="";
		try
			{
			sb.append(" where ");
			if(banking.getBankName()!=null)
	 		   	{
		 			if(!banking.getBankName().equals(""))
		 			{
		 				sb.append("bank_name like '%"+banking.getBankName()+"%'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(banking.getAccountNo()!=null)
	 		   {
		 			if(!banking.getAccountNo().equals(""))
		 			{
		 				sb.append(" account_number like '%"+banking.getAccountNo()+"%'").append(" and ");
		 				
		 			}
	 		   } 
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			
	 			qryStartPart="select banking_id,bank_name,account_type,account_number,date_format(created_on, '%b %e, %Y'),status from pasmt_banking ";
	 			
				sql=qryStartPart+qryPart; 
				bankList=jdbcTemplate.query(sql,new RowMapper() {
					public BankingBean mapRow(ResultSet rs,int rownumber) throws SQLException
					{
						BankingBean bean=new BankingBean();
						bean.setBankId(rs.getString("banking_id"));
						bean.setBankName(rs.getString("bank_name"));
						if(rs.getString("account_type").equals("2"))
						{
							bean.setAccountType("EEFC");
						}
						else
						{
							bean.setAccountType("Current");
						}
						bean.setAccountNo(rs.getString("account_number"));
						bean.setCreatedOn(rs.getString("date_format(created_on, '%b %e, %Y')"));
						bean.setStatus(rs.getString("status"));
						return bean;
					}
				});
			/*
			 * count for pagination 
			 */
			qryStartPart="select count(1) from pasmt_banking ";
			sql=qryStartPart+qryPart;
			int total_rows = jdbcTemplate.queryForInt(sql);
			request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in bankList() in banking "+e.toString());
				//TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		finally
		{
			sql=null;
			qry=null;
			qryEndPart=null;
			qrylast=null;
			qryPart=null;
			qryStartPart=null;
		}
		return bankList;
	}
	
	@Override
	public void addBanking(BankingBean bankingBean, HttpServletRequest request)
	{
		
		List<IntermediaryBankingBean> bankingList=new ArrayList<>();
		String sql="",sql1="",sql2="",bankId;
		StringBuffer sb=new StringBuffer("insert into pasmt_intermediary_banking(banking_id,country_id,bank_name,beneficiary_name,account_number,bank_addess,iban_number,swift_code,sort_code,status,created_on) values");
		 
			//System.out.println("query "+sql);	
		try{
			//System.out.println("1");
			if(bankingBean.getAccTypeCuurency()!=null && !bankingBean.getAccTypeCuurency().equals(""))
			{
				sql="insert into pasmt_banking(bank_name,beneficiary_name,account_type,eefc_currency_id,account_number,ifsc_code,iban_number,swift_code,sort_code,bank_physical_address,pan,service_tax_number,tan,vat_number,status,created_on) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				jdbcTemplate.update(sql, new Object[]{
						bankingBean.getBankName(),bankingBean.getBenefName(),bankingBean.getAccountType(),bankingBean.getAccTypeCuurency(),bankingBean.getAccountNo(),bankingBean.getIfsccode(),bankingBean.getRoutingNO()
						,bankingBean.getSwiftCode(),bankingBean.getSqrtCode(),bankingBean.getPhysicalAdd(),bankingBean.getPanNo(),
						bankingBean.getServiceTaxNo(),bankingBean.getTan(),bankingBean.getVatNo(),"1",new Date()});
			}
			else
			{
				sql="insert into pasmt_banking(bank_name,beneficiary_name,account_type,account_number,ifsc_code,iban_number,swift_code,sort_code,bank_physical_address,pan,service_tax_number,tan,vat_number,status,created_on) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				jdbcTemplate.update(sql, new Object[]{
						bankingBean.getBankName(),bankingBean.getBenefName(),bankingBean.getAccountType(),bankingBean.getAccountNo(),bankingBean.getIfsccode(),bankingBean.getRoutingNO()
						,bankingBean.getSwiftCode(),bankingBean.getSqrtCode(),bankingBean.getPhysicalAdd(),bankingBean.getPanNo(),
						bankingBean.getServiceTaxNo(),bankingBean.getTan(),bankingBean.getVatNo(),"1",new Date()});
			}
			
			//System.out.println("2");
			sql1="select max(banking_id) from pasmt_banking where bank_name='"+bankingBean.getBankName()+"'";
			bankId=jdbcTemplate.queryForObject(sql1, String.class);
			if(bankingBean.getIntermediayBankList()!=null && bankingBean.getIntermediayBankList().size()>0)
			{
				bankingList=removeRow(bankingBean);
				if(bankingList.size()>0 && bankingList!=null)
				{
						//System.out.println("size-->"+answerList.size());
						if(bankingList.size()>0 && bankingList!=null)
						{
							for(int i=0;i<bankingList.size();i++)	
					    	{
					    		System.out.println("i");
								sb.append("('"+bankId+"','"+bankingList.get(i).getCountryId()+"','"+bankingList.get(i).getInterBankName()+"','"+bankingList.get(i).getInterBenefName()+"','"+bankingList.get(i).getInterAccountNo()+"','"+bankingList.get(i).getInterBankAdd()+"','"+bankingList.get(i).getInterRoutingNo()+"','"+bankingList.get(i).getInterSwiftCode()+"','"+bankingList.get(i).getInterSqrtCode()+"',1,now()), ");		
					    	}  
					    	sql2=sb.toString();
							  if(sql2.charAt(sql2.length()-2)==','){
								   sql2 = sql2.substring(0, sql2.length()-2);
								  }
							  jdbcTemplate.update(sql2);
						}
				}
			}
			
		}
		catch(Exception e)
		{
			System.out.println("exception in addBanking() in banking--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			//throw e;
		}
		finally
		{
			sql=null;
			sql1=null;
			sql2=null;
		}
		
	}
	
	public BankingBean getBankingDetail(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String sql="",sql1="";
		BankingBean bankingBean=new BankingBean();
		List<BankingBean> blist=new ArrayList<>();
		List<IntermediaryBankingBean> bnklist=new ArrayList<>();
		String id=request.getParameter("bankId");
		sql="select banking_id,bank_name,beneficiary_name,account_type,eefc_currency_id,account_number,ifsc_code,iban_number,swift_code,sort_code,bank_physical_address,pan,service_tax_number,tan,vat_number from pasmt_banking where banking_id='"+id+"'";
		sql1="select intermediary_banking_id,country_id,bank_name,beneficiary_name,account_number,bank_addess,iban_number,swift_code,sort_code from pasmt_intermediary_banking where banking_id='"+id+"'";
		blist=jdbcTemplate.query(sql, new BankingDetailRowMapper());
		bankingBean=blist.get(0);
		bnklist=jdbcTemplate.query(sql1,new IntermediaryBankingRowMapper());
		bankingBean.setIntermediayBankList(bnklist);
		session.setAttribute("bankId",id);
		//System.out.println(list);
		return bankingBean;
	}
	
	public void editBankingInfo(String bankId,BankingBean bankingBean)
	{
		String sql="",sql1="",sql2="";
		String[] interBankIds;
		StringBuffer sb;	
		List<IntermediaryBankingBean> interBanklist=new ArrayList<>();	
		//System.out.println("before update");
		sql="update pasmt_banking set bank_name='"+bankingBean.getBankName()+"',beneficiary_name='"+bankingBean.getBenefName()+"',account_type='"+bankingBean.getAccountType()+"',eefc_currency_id='"+bankingBean.getAccTypeCuurency()+"',account_number='"+bankingBean.getAccountNo()+"',ifsc_code='"+bankingBean.getIfsccode()+"',iban_number='"+bankingBean.getRoutingNO()+"',swift_code='"+bankingBean.getSwiftCode()+"',sort_code='"+bankingBean.getSqrtCode()+"',bank_physical_address='"+bankingBean.getPhysicalAdd()+"',pan='"+bankingBean.getPanNo()+"',service_tax_number='"+bankingBean.getServiceTaxNo()+"',tan='"+bankingBean.getTan()+"',vat_number='"+bankingBean.getVatNo()+"' where banking_id='"+bankId+"'";
		jdbcTemplate.update(sql);
		//System.out.println("after updatebnmbn");
		interBanklist=removeRow(bankingBean);
		//System.out.println("after remove row");
		if(interBanklist!=null && interBanklist.size()>0)
		{
			//System.out.println("in if block");
			Integer currentSize=0;
			if(!bankingBean.getInterBankListSize().equals("") && bankingBean.getInterBankListSize()!=null && bankingBean.getInterBankListSize().length()>0)
			{
				//System.out.println("check current sizebbbjbj");
				currentSize=Integer.parseInt(bankingBean.getInterBankListSize());
				//System.out.println("after check");
			}
			
			if(bankingBean.getInterBankIdList().length()>0)
			{
				interBankIds=bankingBean.getInterBankIdList().split(",");
				IntermediaryBankingBean newInterBank;
				
				/********
				 *  for update already exists rows *
				 *******/
				if(currentSize>0 && interBankIds.length>0)
				{
					for(int i=0;i<interBankIds.length;i++)
					{
						newInterBank=interBanklist.get(Integer.parseInt(interBankIds[i]));
						String query="update pasmt_intermediary_banking set country_id='"+newInterBank.getCountryId()+"',bank_name='"+newInterBank.getInterBankName()+"',beneficiary_name='"+newInterBank.getInterBenefName()+"',account_number='"+newInterBank.getInterAccountNo()+"',bank_addess='"+newInterBank.getInterBankAdd()+"',iban_number='"+newInterBank.getInterRoutingNo()+"',sort_code='"+newInterBank.getInterSqrtCode()+"',swift_code='"+newInterBank.getInterSwiftCode()+"' where intermediary_banking_id='"+newInterBank.getInterbankId()+"'";
						jdbcTemplate.update(query);
					}
				}
			}
			/*****
			 ** for insert newly added rows *
			 *********/
			if(interBanklist.size()>currentSize)
			{
				sb=new StringBuffer("insert into pasmt_intermediary_banking(banking_id,country_id,bank_name,beneficiary_name,account_number,bank_addess,iban_number,swift_code,sort_code,status,created_on) values");
				for(int i=currentSize;i<interBanklist.size();i++)
				{
					sb.append("('"+bankId+"','"+interBanklist.get(i).getCountryId()+"','"+interBanklist.get(i).getInterBankName()+"','"+interBanklist.get(i).getInterBenefName()+"','"+interBanklist.get(i).getInterAccountNo()+"','"+interBanklist.get(i).getInterBankAdd()+"','"+interBanklist.get(i).getInterRoutingNo()+"','"+interBanklist.get(i).getInterSwiftCode()+"','"+interBanklist.get(i).getInterSqrtCode()+"',1,now())");
				}
				sql2=sb.toString();
				/**** to remove , from query *****/
				if(sql2.charAt(sql2.length()-1)==','){
				   sql2 = sql2.substring(0, sql2.length()-1);
				  }		 
				jdbcTemplate.update(sql2);
			}
		}
		
	}
	
	
	/***** 
	 * To Remove Empty Rows *
	 *****/
	
	public List<IntermediaryBankingBean> removeRow(BankingBean bankingBean)
	{
		List<IntermediaryBankingBean> bankingList=new ArrayList<>();
		for(IntermediaryBankingBean blist:bankingBean.getIntermediayBankList())
		{
			if(blist.getInterBankName()!=null && !blist.getInterBankName().equals("") && blist.getInterBenefName()!=null)
			{
				bankingList.add(blist);
			}
		}
		return bankingList;
	}
	
	public String checkAccountNo(HttpServletRequest request)
	{
		String target="notExist",sql;
		List<String> bankingList=new ArrayList<>();
		try
		{
			sql="select banking_id from pasmt_banking where account_number='"+request.getParameter("acuntNo")+"'";
			//System.out.println("sql-->"+sql);
			bankingList=jdbcTemplate.query(sql, new RowMapper() {
				public String mapRow(ResultSet rs,int rowNumber) throws SQLException {
					return rs.getString("banking_id");
				}
			});
			//System.out.println("size->"+bankingList.size());
			if(bankingList!=null && bankingList.size()>0)
			{
				target="Exists";
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in checkAccountNo()--->"+e);
		}
		return target;
	}
}
