package com.finance.invoicePayable;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class InvoicePayableBean {

	private String transType,invoiceNO,vendorName,currency,serviceCate,irbproject,invoiceDate,invoicePayId;
	private String Payment,dueOn,amount,serviceTax,totalAmount,advancePayment,balAmount,dateFrom,dateTo;
	private String eqUsdAmount,eqUsdServTax,eqUsdTotalAmt,eqUsdAdvPay,eqUsdBalAmt,overdue;
	CommonsMultipartFile invoiceFile;
	
	/****** additional fields for edit page *************/
	private String entryNo,invoiceStatus,paymentdate,bankPayment,paidAmt,payTDS,transId,netbankingID,chequeNo,eqUsdpaidAmt,eqUsdpayTDS,docId,vendorCode;

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getInvoiceNO() {
		return invoiceNO;
	}

	public void setInvoiceNO(String invoiceNO) {
		this.invoiceNO = invoiceNO;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getServiceCate() {
		return serviceCate;
	}

	public void setServiceCate(String serviceCate) {
		this.serviceCate = serviceCate;
	}

	public String getIrbproject() {
		return irbproject;
	}

	public void setIrbproject(String irbproject) {
		this.irbproject = irbproject;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getPayment() {
		return Payment;
	}

	public void setPayment(String payment) {
		Payment = payment;
	}

	public String getDueOn() {
		return dueOn;
	}

	public void setDueOn(String dueOn) {
		this.dueOn = dueOn;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getServiceTax() {
		return serviceTax;
	}

	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAdvancePayment() {
		return advancePayment;
	}

	public void setAdvancePayment(String advancePayment) {
		this.advancePayment = advancePayment;
	}

	public String getBalAmount() {
		return balAmount;
	}

	public void setBalAmount(String balAmount) {
		this.balAmount = balAmount;
	}

	public CommonsMultipartFile getInvoiceFile() {
		return invoiceFile;
	}

	public void setInvoiceFile(CommonsMultipartFile invoiceFile) {
		this.invoiceFile = invoiceFile;
	}

	public String getEntryNo() {
		return entryNo;
	}

	public void setEntryNo(String entryNo) {
		this.entryNo = entryNo;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getPaymentdate() {
		return paymentdate;
	}

	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}

	public String getBankPayment() {
		return bankPayment;
	}

	public void setBankPayment(String bankPayment) {
		this.bankPayment = bankPayment;
	}

	public String getPaidAmt() {
		return paidAmt;
	}

	public void setPaidAmt(String paidAmt) {
		this.paidAmt = paidAmt;
	}

	public String getPayTDS() {
		return payTDS;
	}

	public void setPayTDS(String payTDS) {
		this.payTDS = payTDS;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getNetbankingID() {
		return netbankingID;
	}

	public void setNetbankingID(String netbankingID) {
		this.netbankingID = netbankingID;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public String getEqUsdAmount() {
		return eqUsdAmount;
	}

	public void setEqUsdAmount(String eqUsdAmount) {
		this.eqUsdAmount = eqUsdAmount;
	}

	public String getEqUsdServTax() {
		return eqUsdServTax;
	}

	public void setEqUsdServTax(String eqUsdServTax) {
		this.eqUsdServTax = eqUsdServTax;
	}

	public String getEqUsdTotalAmt() {
		return eqUsdTotalAmt;
	}

	public void setEqUsdTotalAmt(String eqUsdTotalAmt) {
		this.eqUsdTotalAmt = eqUsdTotalAmt;
	}

	public String getEqUsdAdvPay() {
		return eqUsdAdvPay;
	}

	public void setEqUsdAdvPay(String eqUsdAdvPay) {
		this.eqUsdAdvPay = eqUsdAdvPay;
	}

	public String getEqUsdBalAmt() {
		return eqUsdBalAmt;
	}

	public void setEqUsdBalAmt(String eqUsdBalAmt) {
		this.eqUsdBalAmt = eqUsdBalAmt;
	}

	public String getEqUsdpaidAmt() {
		return eqUsdpaidAmt;
	}

	public void setEqUsdpaidAmt(String eqUsdpaidAmt) {
		this.eqUsdpaidAmt = eqUsdpaidAmt;
	}

	public String getEqUsdpayTDS() {
		return eqUsdpayTDS;
	}

	public void setEqUsdpayTDS(String eqUsdpayTDS) {
		this.eqUsdpayTDS = eqUsdpayTDS;
	}

	public String getInvoicePayId() {
		return invoicePayId;
	}

	public void setInvoicePayId(String invoicePayId) {
		this.invoicePayId = invoicePayId;
	}

	public String getOverdue() {
		return overdue;
	}

	public void setOverdue(String overdue) {
		this.overdue = overdue;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	 
}
