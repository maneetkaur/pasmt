package com.finance.invoicePayable;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;


@Repository
@Transactional
public class InvoicePayableDAOImpl implements InvoicePayableDAO{
	
	private JdbcTemplate jdbcTemplate;
	@Autowired		
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource)
	{
		//System.out.println("set");
		this.jdbcTemplate=new JdbcTemplate(dataSource);
	}
	
	@Autowired
	CommonFunction commonFunction;
	
@Override
	
	public List getInvoicePayableList(HttpServletRequest request,InvoicePayableBean invoice,int startIndex,int range)
	{
		HttpSession session=request.getSession(true);
		String qry="";
		String qrylast="";
		String qryPart="",qryStartPart="",qryEndPart="",query2="";
		StringBuffer sb=new StringBuffer();
		List<InvoicePayableBean> invoiceList=new ArrayList<InvoicePayableBean>();
		String sql="";
		try
			{
			sb.append(" and ");
			if(invoice.getInvoiceStatus()!=null)
	 		   	{
		 			if(!invoice.getInvoiceStatus().equals("NONE"))
		 			{
		 				sb.append("invoice_status='"+invoice.getInvoiceStatus()+"'").append(" and ");
		 				
		 			}
	 		   }	
	 		   if(invoice.getDateFrom()!=null && invoice.getDateTo()!=null)
	 		   {
		 			if(!invoice.getDateFrom().equals("") && !invoice.getDateTo().equals(""))
		 			{
		 				sb.append("invoice_date between '"+invoice.getDateFrom()+"' and '"+invoice.getDateTo()+"'").append(" and ");
		 				
		 			}
	 		   } 
	 			qryPart=(sb.toString()).trim();
	 			int qryPartLength=qryPart.length();
	 			
	 			if((qryPart.substring(qryPartLength-3,qryPartLength)).equals("and"))
	 			{
	 				qryPart=(qryPart.substring(0,qryPartLength-3)).trim();
	 			}
	 			
	 			if(qryPartLength==5)
	 			{
	 				qryPart="";
	 			}
	 			if(invoice.getInvoiceStatus()!=null || invoice.getDateFrom()!=null || invoice.getDateTo()!=null)
	 			{
	 				if(!invoice.getInvoiceStatus().equals("NONE") || !invoice.getDateFrom().equals("") || !invoice.getDateTo().equals(""))
	 				{
	 					qryStartPart="select pip.invoice_payable_id,date_format(pip.invoice_date, '%b %e, %Y'),date_format(pip.due_on, '%b %e, %Y'),pip.amount,pip.invoice_no,pv.vendor_code,pv.billing_currency_id,DATEDIFF(now(),pip.due_on),pip.invoice_date,pip.transaction_type,pip.invoice_status from pasmt_invoice_payble pip,pasmt_vendor pv where pip.vendor_id=pv.vendor_id ";
	 				}
	 			}
	 			else
	 			{
	 				//qryStartPart="select pip.invoice_payable_id,date_format(pip.invoice_date, '%b %e, %Y'),date_format(pip.due_on, '%b %e, %Y'),pip.amount,pip.invoice_no,pv.vendor_code,pv.billing_currency_id,DATEDIFF(now(),pip.due_on),pip.invoice_date,pip.transaction_type,pip.invoice_status from pasmt_invoice_payble pip,pasmt_vendor pv where pip.vendor_id=pv.vendor_id and MONTH(invoice_date)=MONTH(now()) ";
	 				qryStartPart="select pip.invoice_payable_id,date_format(pip.invoice_date, '%b %e, %Y'),date_format(pip.due_on, '%b %e, %Y'),pip.amount,pip.invoice_no,pv.vendor_code,pv.billing_currency_id,DATEDIFF(now(),pip.due_on),pip.invoice_date,pip.transaction_type,pip.invoice_status from pasmt_invoice_payble pip,pasmt_vendor pv where pip.vendor_id=pv.vendor_id ";
	 			}
				sql=qryStartPart+qryPart; 
				//System.out.println("listing sql-->"+sql);
				
				invoiceList=jdbcTemplate.query(sql,new RowMapper() {
					public InvoicePayableBean mapRow(ResultSet rs,int rownumber) throws SQLException
					{
						InvoicePayableBean bean=new InvoicePayableBean();
						int date,t_type,i_status;
						Map<String, String> billingCurrencyMap = 
								commonFunction.createMapFromList(commonFunction.getBillingCurrency());
						bean.setInvoicePayId(rs.getString("pip.invoice_payable_id"));
						bean.setInvoiceNO(rs.getString("pip.invoice_no"));
						bean.setInvoiceDate(rs.getString("date_format(pip.invoice_date, '%b %e, %Y')"));
						bean.setDueOn(rs.getString("date_format(pip.due_on, '%b %e, %Y')"));
						bean.setAmount(rs.getString("pip.amount"));
						bean.setVendorName(rs.getString("pv.vendor_code"));
						bean.setCurrency(billingCurrencyMap.get(rs.getString("pv.billing_currency_id")));
						date=Integer.parseInt(rs.getString("DATEDIFF(now(),pip.due_on)"));
						if(date>0 && rs.getString("pip.invoice_date").equals("1"))
						{
							bean.setOverdue(rs.getString("DATEDIFF(now(),pip.due_on)"));
						}
						else
						{
							bean.setOverdue("0");
						}
						t_type=Integer.parseInt(rs.getString("pip.transaction_type"));
						if(t_type==1)
						{
							bean.setTransType("Local");
						}
						else
						{
							bean.setTransType("International");
						}
						i_status=Integer.parseInt(rs.getString("pip.invoice_status"));
						if(i_status==1)
						{
							bean.setInvoiceStatus("Pending");
						}
						else if(i_status==2)
						{
							bean.setInvoiceStatus("Paid");
						}
						else
						{
							bean.setInvoiceStatus("Rejected");
						}
						return bean;
					}
				});
			/*
			 * count for pagination 
			 */
			qryStartPart="select count(1) from pasmt_invoice_payble pip,pasmt_vendor pv where pip.vendor_id=pv.vendor_id and MONTH(invoice_date)=MONTH(now())";
			sql=qryStartPart+qryPart;
			int total_rows = jdbcTemplate.queryForInt(sql);
			request.setAttribute("total_rows", total_rows);
				
			}
			catch(Exception e)
			{
				System.out.println("exception in getInvoicePayableList() in invoice payable "+e.toString());
				//TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			}
		finally
		{
			sql=null;
			qry=null;
			qryEndPart=null;
			qrylast=null;
			qryPart=null;
			qryStartPart=null;
		}
		return invoiceList;
	}
	
	
	@Override
	public void addInvoicePayable(InvoicePayableBean invoicePayableBean,String filePath)
	{
		String sql="",sql1="",path;
		sql="insert into pasmt_invoice_payble(vendor_id,transaction_type,invoice_no,service_category,project_number,invoice_date,payment_term,due_on,amount,eq_usd_amount,service_tax,eq_usd_service_tax,total_amount,eq_usd_total_amount,advance_payment,eq_usd_advance_payment,balance_amount,eq_balance_amount,invoice_status,status,created_on) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			//System.out.println("query "+sql);	
		try{
			//System.out.println("1");
			jdbcTemplate.update(sql, new Object[]{
					invoicePayableBean.getVendorName(),invoicePayableBean.getTransType(),invoicePayableBean.getInvoiceNO(),invoicePayableBean.getServiceCate()
					,invoicePayableBean.getIrbproject(),invoicePayableBean.getInvoiceDate(),invoicePayableBean.getPayment(),invoicePayableBean.getDueOn(),
					invoicePayableBean.getAmount().replace(",",""),invoicePayableBean.getEqUsdAmount().replace(",",""),invoicePayableBean.getServiceTax().replace(",",""),invoicePayableBean.getEqUsdServTax().replace(",",""),
					invoicePayableBean.getTotalAmount().replace(",",""),invoicePayableBean.getEqUsdTotalAmt().replace(",",""),invoicePayableBean.getAdvancePayment().replace(",",""),invoicePayableBean.getEqUsdAdvPay().replace(",",""),
					invoicePayableBean.getBalAmount().replace(",",""),invoicePayableBean.getEqUsdBalAmt().replace(",",""),"1","1",new Date()});
			//System.out.println("2");
			
			// get vendor code for save invoice
			sql1="select vendor_code from pasmt_vendor where vendor_id='"+invoicePayableBean.getVendorName()+"'";
			String vendorCode=jdbcTemplate.queryForObject(sql1, String.class);
						
			//Get the invoice payable id of the latest entry
			sql = "SELECT MAX(invoice_payable_id) FROM pasmt_invoice_payble WHERE vendor_id ='" + invoicePayableBean.getVendorName()+ "'";
			String invoicePayableId = jdbcTemplate.queryForObject(sql, String.class);
			
			path= filePath + "/Document/Finance/InvoicePayable/" +invoicePayableBean.getVendorName();
			addDocuments(invoicePayableBean.getInvoiceFile(), path,invoicePayableId,vendorCode,invoicePayableBean.getInvoiceNO() , 0);
			
			
			//Get the document id of the latest entry
			sql1 = "SELECT MAX(document_id) FROM pasmt_document WHERE document_type ='61' and document_source_id='"+invoicePayableId+"'";
			String docId = jdbcTemplate.queryForObject(sql1, String.class);
			
			sql1="update pasmt_invoice_payble set document_id='"+docId+"' where invoice_payable_id='"+invoicePayableId+"'";
			jdbcTemplate.update(sql1);
			
		}
		catch(Exception e)
		{
			System.out.println("exception in addInvoicePayable() in invoicePayable--> "+e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			//throw e;
		}
		finally
		{
			sql=null;
			sql1=null;
		}
		
	}
	public void addDocuments(CommonsMultipartFile file, String path, String invoicePayableId,String vendorCode,String invoiceNo, int from){
		try{
			//Check if there is any existing file
			if(file.getOriginalFilename()!=""){
				CommonFunction commFunc=new CommonFunction();
				commFunc.createDirectory(path);
				String extension, name, sql="";
				int dotIndex;
				File destination;
				name = file.getOriginalFilename();						
				dotIndex = name.lastIndexOf(".");
				extension = name.substring(dotIndex+1, name.length());			
				name = vendorCode+"_"+invoiceNo;
					
				destination = new File(path + "/" + name + "." + extension);			
				try {
					file.transferTo(destination);
					if(from==0){
						sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, " +
							"document_source_type, created_on)" +
							" VALUES(" + invoicePayableId + ", '" + extension + "', 61, '" + name + "',3, now())";
					}
					else if(from==1){
						sql = "SELECT count(1) FROM pasmt_document WHERE document_source_id =" + invoicePayableId + 
								"  AND document_source_type=4";
						int count = jdbcTemplate.queryForInt(sql);
						if(count==0){
							sql = "INSERT INTO pasmt_document(document_source_id, extension, document_type, document_name, "
									+ "document_source_type, created_on) VALUES(" + invoicePayableId + ", '" + extension + "',61, '" + name + "',3, now())";
						}
						else{
							sql = "UPDATE pasmt_document SET extension ='" + extension + "',document_name='" + name +
									"' WHERE document_source_id =" + invoicePayableId + " AND document_source_type=4";
						}
					}
					jdbcTemplate.update(sql);
				} catch (IllegalStateException e) {
					System.out.println("Illegal state Exception in addDocuments of InvoicePayableDaoImpl.java"+e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("IO Exception in addDocuments of InvoicePayableDaoImpl.java" + e.toString());
					e.printStackTrace();
				}
			}
		}
		catch(Exception e){
			System.out.println("Exception in addDocuments of VendorDaoImpl.java: " + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		
	}

	
	public String getCurrencyForVendor(HttpServletRequest request)
	{
		String currency="";
		try
		{
			String sql="select gv.generic_value from lu_generic_values gv,pasmt_vendor pv where gv.generic_value_id=pv.billing_currency_id and vendor_id='"+request.getParameter("vendorId")+"'";
			currency=jdbcTemplate.queryForObject(sql, String.class);

		}
		catch(Exception e)
		{
			System.out.println("Exception in getCurrencyForVendor() "+e.toString());
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}		
		return currency;
	}
	
	public InvoicePayableBean getInvoicePayabledetail(HttpServletRequest request)
	{
		HttpSession session=request.getSession(true);
		String sql="",sql1="",currency,sql2="",docName="";
		InvoicePayableBean invoicePayable=new InvoicePayableBean();
		List<InvoicePayableBean> invoicelist=new ArrayList<>();
		List<CommonBean> document=new ArrayList<>();
		String id=request.getParameter("invoicePayId");
		sql="select vendor_id,transaction_type,invoice_no,service_category,project_number,DATE(invoice_date),payment_term,DATE(due_on),amount,eq_usd_amount,service_tax,eq_usd_service_tax," +
				"total_amount,eq_usd_total_amount,advance_payment,eq_usd_advance_payment,balance_amount,eq_balance_amount,invoice_status,document_id,DATE(payment_date),payment_from_bank," +
				"amount_paid,eq_usd_amount_paid,tds_to_pay,eq_usd_tds_to_pay,transaction_id,net_banking_number,cheque_number from pasmt_invoice_payble where invoice_payable_id='"+id+"'";
		invoicelist=jdbcTemplate.query(sql, new InvoicePayableRowMapper());
		invoicePayable=invoicelist.get(0);
		
		/***** get currency for vendor *******/
		sql1="select gv.generic_value from lu_generic_values gv,pasmt_vendor pv where gv.generic_value_id=pv.billing_currency_id and vendor_id='"+invoicePayable.getVendorName()+"'";
		currency=jdbcTemplate.queryForObject(sql1, String.class);
		sql2="select vendor_code from pasmt_vendor where vendor_id='"+invoicePayable.getVendorName()+"'";
		String vendorCode=jdbcTemplate.queryForObject(sql2, String.class);
		sql2="select document_name,extension from pasmt_document where document_id='"+invoicePayable.getDocId()+"'";
		//System.out.println("sql-->"+sql2);
		document=jdbcTemplate.query(sql2, new RowMapper() {
			@Override
			public CommonBean mapRow(ResultSet rs,int rownumber) throws SQLException
			{
				CommonBean bean=new CommonBean();
				bean.setId(rs.getNString("extension"));
				bean.setName(rs.getString("document_name"));
				//System.out.println("doc-->"+rs.getString("document_name")+"."+rs.getNString("extension"));
				return bean;
			}
		});
		if(document!=null && document.size()>0)
		{
			docName=document.get(0).getName()+"."+document.get(0).getId();
		}
		invoicePayable.setCurrency(currency);
		invoicePayable.setVendorCode(vendorCode);
		//System.out.println("doc name-->"+docName);
		invoicePayable.setDocId(docName);
		session.setAttribute("invoicePayId",id);
		//System.out.println(list);
		sql=null;
		sql1=null;
		sql2=null;
		docName=null;
		document=null;
		invoicelist=null;
		return invoicePayable;
	}
	
	public void editInvoicePayableInfo(String invoicePayId,InvoicePayableBean invoicePayable,String filePath)
	{
		String sql="",sql1="",sql2="",path;
		if(invoicePayable.getIrbproject()!=null && !invoicePayable.getIrbproject().equals("NONE"))
		{
			if(invoicePayable.getPaymentdate()!=null && !invoicePayable.getPaymentdate().equals("") && invoicePayable.getBankPayment()!=null && !invoicePayable.getBankPayment().equals(""))
			{
				sql="update pasmt_invoice_payble set vendor_id=?,transaction_type=?,invoice_no=?,service_category=?,project_number=?," +
						"invoice_date=?,payment_term=?,due_on=?,amount=?,service_tax=?,eq_usd_amount=?,eq_usd_service_tax=?,total_amount=?," +
						"eq_usd_total_amount=?,advance_payment=?,eq_usd_advance_payment=?,balance_amount=?,eq_balance_amount=?,invoice_status=?," +
						"payment_date=?,payment_from_bank=?,amount_paid=?,eq_usd_amount_paid=?,tds_to_pay=?,eq_usd_tds_to_pay=?," +
						"transaction_id=?,net_banking_number=?,cheque_number=? where invoice_payable_id=?";
				jdbcTemplate.update(sql,new Object[]{
						invoicePayable.getVendorName(),invoicePayable.getTransType(),invoicePayable.getInvoiceNO(),invoicePayable.getServiceCate(),
						invoicePayable.getIrbproject(),invoicePayable.getInvoiceDate(),invoicePayable.getPayment(),invoicePayable.getDueOn(),
						invoicePayable.getAmount().replace(",",""),invoicePayable.getServiceTax().replace(",",""),invoicePayable.getEqUsdAmount().replace(",",""),invoicePayable.getEqUsdServTax().replace(",",""),
						invoicePayable.getTotalAmount().replace(",",""),invoicePayable.getEqUsdTotalAmt().replace(",",""),invoicePayable.getAdvancePayment().replace(",",""),invoicePayable.getEqUsdAdvPay().replace(",",""),
						invoicePayable.getBalAmount().replace(",",""),invoicePayable.getEqUsdBalAmt().replace(",",""),invoicePayable.getInvoiceStatus(),invoicePayable.getPaymentdate(),
						invoicePayable.getBankPayment(),invoicePayable.getPaidAmt().replace(",",""),invoicePayable.getEqUsdpaidAmt().replace(",",""),invoicePayable.getPayTDS().replace(",",""),
						invoicePayable.getEqUsdpayTDS().replace(",",""),invoicePayable.getTransId(),invoicePayable.getNetbankingID(),invoicePayable.getChequeNo(),
						invoicePayId});
				
			}
			else
			{
				sql="update pasmt_invoice_payble set vendor_id=?,transaction_type=?,invoice_no=?,service_category=?,project_number=?," +
						"invoice_date=?,payment_term=?,due_on=?,amount=?,service_tax=?,eq_usd_amount=?,eq_usd_service_tax=?,total_amount=?," +
						"eq_usd_total_amount=?,advance_payment=?,eq_usd_advance_payment=?,balance_amount=?,eq_balance_amount=?,invoice_status=? where invoice_payable_id=?";
				jdbcTemplate.update(sql,new Object[]{
						invoicePayable.getVendorName(),invoicePayable.getTransType(),invoicePayable.getInvoiceNO(),invoicePayable.getServiceCate(),
						invoicePayable.getIrbproject(),invoicePayable.getInvoiceDate(),invoicePayable.getPayment(),invoicePayable.getDueOn(),
						invoicePayable.getAmount().replace(",",""),invoicePayable.getServiceTax().replace(",",""),invoicePayable.getEqUsdAmount().replace(",",""),
						invoicePayable.getEqUsdServTax().replace(",",""),invoicePayable.getTotalAmount().replace(",",""),invoicePayable.getEqUsdTotalAmt().replace(",",""),
						invoicePayable.getAdvancePayment().replace(",",""),invoicePayable.getEqUsdAdvPay().replace(",",""),invoicePayable.getBalAmount().replace(",",""),
						invoicePayable.getEqUsdBalAmt().replace(",",""),invoicePayable.getInvoiceStatus(),invoicePayId});
				
			}
			
		}
		else
		{
			if(invoicePayable.getPaymentdate()!=null && !invoicePayable.getPaymentdate().equals("") && invoicePayable.getBankPayment()!=null && !invoicePayable.getBankPayment().equals(""))
			{
				sql="update pasmt_invoice_payble set vendor_id=?,transaction_type=?,invoice_no=?,service_category=?,invoice_date=?," +
						"payment_term=?,due_on=?,amount=?,service_tax=?,eq_usd_amount=?,eq_usd_service_tax=?,total_amount=?," +
						"eq_usd_total_amount=?,advance_payment=?,eq_usd_advance_payment=?,balance_amount=?,eq_balance_amount=?,invoice_status=?," +
						"payment_date=?,payment_from_bank=?,amount_paid=?,eq_usd_amount_paid=?,tds_to_pay=?,eq_usd_tds_to_pay=?," +
						"transaction_id=?,net_banking_number=?,cheque_number=? where invoice_payable_id=?";
				jdbcTemplate.update(sql,new Object[]{
						invoicePayable.getVendorName(),invoicePayable.getTransType(),invoicePayable.getInvoiceNO(),invoicePayable.getServiceCate(),
						invoicePayable.getInvoiceDate(),invoicePayable.getPayment(),invoicePayable.getDueOn(),
						invoicePayable.getAmount().replace(",",""),invoicePayable.getServiceTax().replace(",",""),invoicePayable.getEqUsdAmount().replace(",",""),
						invoicePayable.getEqUsdServTax().replace(",",""),invoicePayable.getTotalAmount().replace(",",""),invoicePayable.getEqUsdTotalAmt().replace(",",""),
						invoicePayable.getAdvancePayment().replace(",",""),invoicePayable.getEqUsdAdvPay().replace(",",""),invoicePayable.getBalAmount().replace(",",""),
						invoicePayable.getEqUsdBalAmt().replace(",",""),invoicePayable.getInvoiceStatus(),invoicePayable.getPaymentdate(),invoicePayable.getBankPayment(),
						invoicePayable.getPaidAmt().replace(",",""),invoicePayable.getEqUsdpaidAmt().replace(",",""),invoicePayable.getPayTDS().replace(",",""),
						invoicePayable.getEqUsdpayTDS().replace(",",""),invoicePayable.getTransId(),invoicePayable.getNetbankingID(),invoicePayable.getChequeNo(),
						invoicePayId});
				
			}
			else
			{
				sql="update pasmt_invoice_payble set vendor_id=?,transaction_type=?,invoice_no=?,service_category=?,invoice_date=?," +
						"payment_term=?,due_on=?,amount=?,service_tax=?,eq_usd_amount=?,eq_usd_service_tax=?,total_amount=?," +
						"eq_usd_total_amount=?,advance_payment=?,eq_usd_advance_payment=?,balance_amount=?,eq_balance_amount=?,invoice_status=?" +
						" where invoice_payable_id=?";
				jdbcTemplate.update(sql,new Object[]{
						invoicePayable.getVendorName(),invoicePayable.getTransType(),invoicePayable.getInvoiceNO(),invoicePayable.getServiceCate(),
						invoicePayable.getInvoiceDate(),invoicePayable.getPayment(),invoicePayable.getDueOn(),invoicePayable.getAmount().replace(",",""),
						invoicePayable.getServiceTax().replace(",",""),invoicePayable.getEqUsdAmount().replace(",",""),invoicePayable.getEqUsdServTax().replace(",",""),
						invoicePayable.getTotalAmount().replace(",",""),invoicePayable.getEqUsdTotalAmt().replace(",",""),invoicePayable.getAdvancePayment().replace(",",""),
						invoicePayable.getEqUsdAdvPay().replace(",",""),invoicePayable.getBalAmount().replace(",",""),invoicePayable.getEqUsdBalAmt().replace(",",""),
						invoicePayable.getInvoiceStatus(),invoicePayId});
				
			}
			
		}
		
				if(invoicePayable.getInvoiceFile()!=null && !invoicePayable.getInvoiceFile().getOriginalFilename().equals(""))
				{
					// get vendor code for save invoice
					sql1="select vendor_code from pasmt_vendor where vendor_id='"+invoicePayable.getVendorName()+"'";
					String vendorCode=jdbcTemplate.queryForObject(sql1, String.class);
					
					path= filePath + "/Document/Finance/InvoicePayable/" +invoicePayable.getVendorName();
					addDocuments(invoicePayable.getInvoiceFile(), path,invoicePayId,vendorCode,invoicePayable.getInvoiceNO() , 1);
					
					//Get the document id of the latest entry
					sql1 = "SELECT MAX(document_id) FROM pasmt_document WHERE document_type ='61' and document_source_id='"+invoicePayId+"'";
					String docId = jdbcTemplate.queryForObject(sql1, String.class);
					
					sql1="update pasmt_invoice_payble set document_id='"+docId+"' where invoice_payable_id='"+invoicePayId+"'";
					jdbcTemplate.update(sql1);
		
				}
				sql=null;
				sql1=null;
				path=null;
		}
	public String checkInvoiceNo(HttpServletRequest request)
	{
		String target="notExist",sql;
		List<String> invoiceList=new ArrayList<>();
		try
		{
			sql="select invoice_payable_id from pasmt_invoice_payble where invoice_no='"+request.getParameter("invoiceNo")+"' and vendor_id='"+request.getParameter("vendorId")+"'";
			//System.out.println("sql-->"+sql);
			invoiceList=jdbcTemplate.query(sql, new RowMapper() {
				public String mapRow(ResultSet rs,int rowNumber) throws SQLException {
					return rs.getString("invoice_payable_id");
				}
			});
			//System.out.println("size->"+bankingList.size());
			if(invoiceList!=null && invoiceList.size()>0)
			{
				target="Exists";
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception in checkInvoiceNo()--->"+e);
		}
		return target;
	}
	
}
