package com.finance.invoicePayable;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;
import com.finance.banking.BankingBean;



@Controller
public class InvoicePayableController {
	
	@Autowired
	CommonFunction commFunc;
	
	@Autowired
	InvoicePayableDAO invoiceDao;
	
	@Autowired
	ServletContext servletContext;
	
	
	@RequestMapping(value="/sAdminInvoicePayableList")
	public ModelAndView getInvoiceList(HttpServletRequest request, 
					@ModelAttribute("searchInvoicePayable") InvoicePayableBean invoicePayableBean){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView invoiceModel = new ModelAndView("finance/invoicePayable/invoicePayableList");		
		List<InvoicePayableBean> getInvoiceList = invoiceDao.getInvoicePayableList(request, invoicePayableBean, startIndex, 15);
		invoiceModel.addObject("getInvoiceList", getInvoiceList);
		getInvoiceList=null;		
		return invoiceModel;
	}
	
	@RequestMapping(value="/sAdminAddInvoicePayableForm")
	public ModelAndView addInvoicePayable(){
		List<CommonBean> vendorList=commFunc.getActiveVendorList();
		List<CommonBean> projectList=commFunc.getProjectList();
		//List<CommonBean> currencyList=commFunc.getBillingCurrency();
		ModelAndView addinvoicePayable = new ModelAndView("finance/invoicePayable/addInvoicePayable");
		addinvoicePayable.addObject("vendorList", vendorList);
		addinvoicePayable.addObject("projectList", projectList);
		//addinvoicePayable.addObject("currencyList", currencyList);
		addinvoicePayable.addObject("addInvoicePayable", new InvoicePayableBean());
		vendorList=null;
		projectList=null;
		return addinvoicePayable;
	}
	
	@RequestMapping(value="/sAdminSubmitInvoicePayable",method=RequestMethod.POST)
	public String AddInvoicePayable(@ModelAttribute("addInvoicePayable") InvoicePayableBean invoicePayableBean,ModelMap model)
	{		
		String filepath = servletContext.getRealPath("");
		invoiceDao.addInvoicePayable(invoicePayableBean,filepath);
		model.addAttribute("searchInvoicePayable", new InvoicePayableBean());
		return "redirect:/sAdminInvoicePayableList";
	}
	@RequestMapping(value="/getCurrency",method=RequestMethod.GET)
	public String getCurrency(HttpServletRequest request,ModelMap model)
	{
		String currency=invoiceDao.getCurrencyForVendor(request);
		model.addAttribute("currency", currency);
		return "ajaxOperation/ajaxOperationFinance";
		
	}
	
	@RequestMapping(value="/sAdminEditInvoicePayableForm",method=RequestMethod.GET)
	public ModelAndView updateCategory(HttpServletRequest request)
	{
		InvoicePayableBean invoiceBean=invoiceDao.getInvoicePayabledetail(request);
		List<CommonBean> vendorList=commFunc.getActiveVendorList();
		List<CommonBean> projectList=commFunc.getProjectList();
		List<CommonBean> bankList=commFunc.getBankList();
		ModelAndView addinvoicePayable = new ModelAndView("finance/invoicePayable/editInvoicePayable");
		addinvoicePayable.addObject("editInvoicePayable",invoiceBean);
		addinvoicePayable.addObject("vendorList", vendorList);
		addinvoicePayable.addObject("projectList", projectList);
		addinvoicePayable.addObject("bankList", bankList);
		bankList=null;
		vendorList=null;
		projectList=null;
		return addinvoicePayable;
	}
	
	@RequestMapping(value="/downloadInvoiceFile", method=RequestMethod.GET)
	public void downloadFile(HttpServletRequest request, HttpServletResponse response){
		String fullpath=servletContext.getRealPath("") + "/Document/Finance/InvoicePayable/" +request.getParameter("vendorId")+"/"+request.getParameter("fileName");
		commFunc.downloadFile(servletContext, response, fullpath);
	}
	
	@RequestMapping(value="/sAdminEditInvoicePayable",method=RequestMethod.POST)
	public String EditBankingDetail(@ModelAttribute("editInvoicePayable")InvoicePayableBean invoicePayable,HttpServletRequest request)
	{
		String path,id;
		HttpSession session=request.getSession(true);
		id=(String)session.getAttribute("invoicePayId");
		path=servletContext.getRealPath("");
		invoiceDao.editInvoicePayableInfo(id,invoicePayable,path);
		return "redirect:/sAdminInvoicePayableList";
	}
	
	@RequestMapping(value="/findInvoiceNoList",method=RequestMethod.GET)
	public String getInvoiceNo(HttpServletRequest request,ModelMap model)
	{
		String Target=invoiceDao.checkInvoiceNo(request);
		model.addAttribute("checkinvoiceNo", Target);
		return "ajaxOperation/ajaxOperationFinance";
	}
}
