package com.finance.invoicePayable;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface InvoicePayableDAO {
	
	public void addInvoicePayable(InvoicePayableBean invoicePayableBean, String filePath);
	public String getCurrencyForVendor(HttpServletRequest request);
	public InvoicePayableBean getInvoicePayabledetail(HttpServletRequest request);
	public List getInvoicePayableList(HttpServletRequest request,InvoicePayableBean invoice,int startIndex,int range);
	public void editInvoicePayableInfo(String invoicePayId,InvoicePayableBean invoicePayable,String filePath);
	public String checkInvoiceNo(HttpServletRequest request);

}
