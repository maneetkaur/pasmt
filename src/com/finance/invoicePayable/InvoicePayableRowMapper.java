package com.finance.invoicePayable;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.finance.banking.BankingBean;

public class InvoicePayableRowMapper implements RowMapper<InvoicePayableBean>{
	
	@Override
	public InvoicePayableBean mapRow(ResultSet rs,int rownumber) throws SQLException
	{
		InvoicePayableBean bean=new InvoicePayableBean();;
		bean.setTransType(rs.getString("transaction_type"));
		bean.setVendorName(rs.getString("vendor_id"));
		bean.setInvoiceNO(rs.getString("invoice_no"));
		bean.setServiceCate(rs.getString("service_category"));
		bean.setIrbproject(rs.getString("project_number"));
		bean.setInvoiceDate(rs.getString("DATE(invoice_date)"));
		bean.setPayment(rs.getString("payment_term"));
		bean.setDueOn(rs.getString("DATE(due_on)"));
		bean.setAmount(rs.getString("amount"));
		bean.setEqUsdAmount(rs.getString("eq_usd_amount"));
		bean.setServiceTax(rs.getString("service_tax"));
		bean.setEqUsdServTax(rs.getString("eq_usd_service_tax"));
		bean.setTotalAmount(rs.getString("total_amount"));
		bean.setEqUsdTotalAmt(rs.getString("eq_usd_total_amount"));
		bean.setAdvancePayment(rs.getString("advance_payment"));
		bean.setEqUsdAdvPay(rs.getString("eq_usd_advance_payment"));
		bean.setBalAmount(rs.getString("balance_amount"));
		bean.setEqUsdBalAmt(rs.getString("eq_balance_amount"));
		bean.setInvoiceStatus(rs.getString("invoice_status"));
		bean.setDocId(rs.getString("document_id"));
		bean.setPaymentdate(rs.getString("DATE(payment_date)"));
		System.out.println("payent date-->"+rs.getString("DATE(payment_date)"));
		bean.setBankPayment(rs.getString("payment_from_bank"));
		System.out.println("bank payamne-->"+rs.getString("payment_from_bank"));
		bean.setPaidAmt(rs.getString("amount_paid"));
		bean.setEqUsdpaidAmt(rs.getString("eq_usd_amount_paid"));
		bean.setPayTDS(rs.getString("tds_to_pay"));
		bean.setEqUsdpayTDS(rs.getString("eq_usd_tds_to_pay"));
		bean.setTransId(rs.getString("transaction_id"));
		bean.setNetbankingID(rs.getString("net_banking_number"));
		bean.setChequeNo(rs.getString("cheque_number"));
		//bean.setCreatedOn(rs.getString("date_format(created_on, '%b %e, %Y')"));
		//bean.setStatus(rs.getString("status"));
		return bean;
	}


}
