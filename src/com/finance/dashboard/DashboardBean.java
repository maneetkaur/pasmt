package com.finance.dashboard;

public class DashboardBean {
	private String bsStartDate, bsEndDate, bsMonth, cfStartDate, cfEndDate, cfMonth, paymentStatus;

	public String getBsStartDate() {
		return bsStartDate;
	}

	public void setBsStartDate(String bsStartDate) {
		this.bsStartDate = bsStartDate;
	}

	public String getBsEndDate() {
		return bsEndDate;
	}

	public void setBsEndDate(String bsEndDate) {
		this.bsEndDate = bsEndDate;
	}

	public String getBsMonth() {
		return bsMonth;
	}

	public void setBsMonth(String bsMonth) {
		this.bsMonth = bsMonth;
	}

	public String getCfStartDate() {
		return cfStartDate;
	}

	public void setCfStartDate(String cfStartDate) {
		this.cfStartDate = cfStartDate;
	}

	public String getCfEndDate() {
		return cfEndDate;
	}

	public void setCfEndDate(String cfEndDate) {
		this.cfEndDate = cfEndDate;
	}

	public String getCfMonth() {
		return cfMonth;
	}

	public void setCfMonth(String cfMonth) {
		this.cfMonth = cfMonth;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	
}
