package com.finance.dashboard;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Repository
@Transactional
public class DashboardDaoImpl implements DashboardDao{
	private float totalBSExpense = 0.0f, totalCFExpense = 0.0f;
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	CommonFunction commonFunction;

	@Override
	public List<CommonBean> getBalanceSheetExpense(DashboardBean balanceSheet) {
		try{
			StringBuffer where_clause;
			String temp, where;
			String sql = "SELECT ifnull(SUM(ifnull(eq_usd_total_amount,0)),0) as categorySum, service_category FROM " +
					"pasmt_invoice_payble ";
			where_clause = new StringBuffer("WHERE status=1 AND ");
			
			if(balanceSheet.getBsMonth() != null && !balanceSheet.getBsMonth().equals("")){
				String[] monthNYear = balanceSheet.getBsMonth().split("-");
				temp = commonFunction.changeMonthToNumber(monthNYear[0]);							
				where_clause.append("MONTH(invoice_date)='" + temp  + "' AND YEAR(invoice_date)='" + 
						monthNYear[1] + "' AND ");
			}
			
			if(balanceSheet.getBsStartDate()!= null && !balanceSheet.getBsStartDate().equals("") &&
					balanceSheet.getBsEndDate()!=null && !balanceSheet.getBsEndDate().equals("")){
				where_clause.append("DATE(invoice_date) >= '" + balanceSheet.getBsStartDate() + 
						"' AND DATE(invoice_date)<='" + balanceSheet.getBsEndDate() + "' AND ");
			}
			
			where=where_clause.toString().trim();
			where_clause = null;			
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			sql = sql + where + " GROUP BY service_category";
			//System.out.println("\nsql: " + sql);
						
			List<CommonBean> bsExpenseList = jdbcTemplate.query(sql, new ExpenseMapper());
			float total=0.0f, temp_expense;
			if(bsExpenseList != null && bsExpenseList.size()>0){
				for(CommonBean expense: bsExpenseList){
					temp_expense = Float.parseFloat(expense.getName());
					total = total + temp_expense;
				}				
			}
			totalBSExpense = total;
			//System.out.println("BS Expense -> " + totalCFExpense);
			CommonBean totalExpense = new CommonBean();
			totalExpense.setId("totalBSExpense");
			totalExpense.setName(String.valueOf(total));
			bsExpenseList.add(totalExpense);
			return bsExpenseList;
		}
		catch(Exception e){
			System.out.println("Exception in getBalanceSheetExpense of DashboardDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<CommonBean> getBalanceSheetIncome(DashboardBean balanceSheet) {
		try{
			StringBuffer where_clause;
			String temp, where;
			String sql = "SELECT ifnull(SUM(ifnull(eq_usd_total_amount,0)),0) as totalIncome FROM " +
					"pasmt_invoice_receivable ";			
			where_clause = new StringBuffer("WHERE status=1 AND ");
			
			if(balanceSheet.getBsMonth() != null && !balanceSheet.getBsMonth().equals("")){
				String[] monthNYear = balanceSheet.getBsMonth().split("-");
				temp = commonFunction.changeMonthToNumber(monthNYear[0]);							
				where_clause.append("MONTH(raise_on)='" + temp  + "' AND YEAR(raise_on)='" + 
						monthNYear[1] + "' AND ");
			}
			
			if(balanceSheet.getBsStartDate()!= null && !balanceSheet.getBsStartDate().equals("") &&
					balanceSheet.getBsEndDate()!=null && !balanceSheet.getBsEndDate().equals("")){
				where_clause.append("DATE(raise_on) >= '" + balanceSheet.getBsStartDate() + 
						"' AND DATE(raise_on)<='" + balanceSheet.getBsEndDate() + "' AND ");
			}
			
			where=where_clause.toString().trim();
			where_clause = null;			
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			sql = sql + where;
			//System.out.println("\nsql: " + sql);
			
			List<CommonBean> incomeList = new ArrayList<CommonBean>();
			CommonBean income = new CommonBean();
			income.setId("Projects");
			income.setName(jdbcTemplate.queryForObject(sql, String.class));
			incomeList.add(income);
			float incomeTotal = Float.parseFloat(income.getName());
			float grossProfit = incomeTotal - totalBSExpense;
			//System.out.println("BS Income -> " + incomeTotal);
			//System.out.println("Gross Profit -> " + grossProfit);
			income = new CommonBean();
			income.setId("Gross Profit");
			income.setName(String.valueOf(grossProfit));
			incomeList.add(income);
			income = null;
			return incomeList;
		}
		catch(Exception e){
			System.out.println("Exception in getBalanceSheetIncome of DashboardDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<CommonBean> getCashflowIncome(DashboardBean cashflow) {
		try{
			StringBuffer where_clause;
			String temp, where;
			String sql = "SELECT ifnull(SUM(ifnull(eq_usd_total_amount,0)),0) as incomeSum, payment_status FROM" +
					" pasmt_invoice_receivable ";			
			where_clause = new StringBuffer("WHERE status=1 AND ");
			
			if(cashflow.getCfMonth() != null && !cashflow.getCfMonth().equals("")){
				String[] monthNYear = cashflow.getCfMonth().split("-");
				temp = commonFunction.changeMonthToNumber(monthNYear[0]);							
				where_clause.append("MONTH(due_on)='" + temp  + "' AND YEAR(due_on)='" + 
						monthNYear[1] + "' AND ");
			}
			
			if(cashflow.getCfStartDate()!= null && !cashflow.getCfStartDate().equals("") &&
					cashflow.getCfEndDate()!=null && !cashflow.getCfEndDate().equals("")){
				where_clause.append("DATE(due_on) >= '" + cashflow.getCfStartDate() + 
						"' AND DATE(due_on)<='" + cashflow.getCfEndDate() + "' AND ");
			}
			
			where=where_clause.toString().trim();
			where_clause = null;			
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			sql = sql + where + " GROUP BY payment_status";
			//System.out.println("\nsql: " + sql);
			
			List<CommonBean> cfIncomeList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

				@Override
				public CommonBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					CommonBean income = new CommonBean();
					if(rs.getString("payment_status")!= null){
						String temp = rs.getString("payment_status");
						//1 - Pending, 2 - Paid, 3 - Declined
						if(temp.equals("1")){
							income.setId("Payment Due");
						}
						else if(temp.equals("2")){
							income.setId("Payment Received");
						}
						else if(temp.equals("3")){
							income.setId("Payment Declined");
						}
					}
					income.setName(rs.getString("incomeSum"));
					return income;
				}
				
			});
			
			float total=0.0f, temp_income;
			if(cfIncomeList != null && cfIncomeList.size()>0){
				for(CommonBean income: cfIncomeList){
					temp_income = Float.parseFloat(income.getName());
					total = total + temp_income;
				}				
			}
			CommonBean totalIncome = new CommonBean();
			totalIncome.setId("totalCfIncome");
			totalIncome.setName(String.valueOf(total));
			cfIncomeList.add(totalIncome);
			//System.out.println("CF income -> " + total);
			float cashInHand = total - totalCFExpense;
			//System.out.println("Cash in hand -> " + cashInHand);
			totalIncome = new CommonBean();
			totalIncome.setId("Cash in hand");
			totalIncome.setName(String.valueOf(cashInHand));
			cfIncomeList.add(totalIncome);
			totalIncome = null;
			return cfIncomeList;			
		}
		catch(Exception e){
			System.out.println("Exception in getCashflowIncome of DashboardDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<CommonBean> getCashflowExpense(DashboardBean cashflow) {
		try{
			StringBuffer where_clause;
			String temp, where;
			String sql = "SELECT ifnull(SUM(ifnull(eq_usd_total_amount,0)),0) as categorySum, " +
					"service_category FROM pasmt_invoice_payble ";			
			where_clause = new StringBuffer("WHERE status=1 AND ");
			
			if(cashflow.getCfMonth() != null && !cashflow.getCfMonth().equals("")){
				String[] monthNYear = cashflow.getCfMonth().split("-");
				temp = commonFunction.changeMonthToNumber(monthNYear[0]);							
				where_clause.append("MONTH(due_on)='" + temp  + "' AND YEAR(due_on)='" + 
						monthNYear[1] + "' AND ");
			}
			
			if(cashflow.getCfStartDate()!= null && !cashflow.getCfStartDate().equals("") &&
					cashflow.getCfEndDate()!=null && !cashflow.getCfEndDate().equals("")){
				where_clause.append("DATE(due_on) >= '" + cashflow.getCfStartDate() + 
						"' AND DATE(due_on)<='" + cashflow.getCfEndDate() + "' AND ");
			}
			
			if(cashflow.getPaymentStatus() != null && !cashflow.getPaymentStatus().equals("0")){								
				if(cashflow.getPaymentStatus().equals("4")){
					where_clause.append("(invoice_status=1 OR invoice_status=2) AND ");
				}
				else{
					where_clause.append("invoice_status=" + cashflow.getPaymentStatus() + " AND ");
				}
			}
			else{
				where_clause.append("(invoice_status=1 OR invoice_status=2) AND ");
			}
			
			where=where_clause.toString().trim();
			where_clause = null;			
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);								
			
			sql = sql + where + " GROUP BY service_category";
			//System.out.println("\nsql: " + sql);
			
			List<CommonBean> cfExpenseList = jdbcTemplate.query(sql, new ExpenseMapper());
			
			float total=0.0f, temp_expense;
			if(cfExpenseList != null && cfExpenseList.size()>0){
				for(CommonBean expense: cfExpenseList){
					temp_expense = Float.parseFloat(expense.getName());
					total = total + temp_expense;
					//System.out.println(expense.getId() + " -> " + expense.getName());
				}
			}
			totalCFExpense = total;
			//System.out.println("CF Expense -> " + totalCFExpense);
			CommonBean totalExpense = new CommonBean();
			totalExpense.setId("totalCfExpense");
			totalExpense.setName(String.valueOf(total));
			cfExpenseList.add(totalExpense);			
			return cfExpenseList;			
		}
		catch(Exception e){
			System.out.println("Exception in getCashflowExpense of DashboardDaoImpl.java: " + e);
			e.printStackTrace();
		}
		return null;
	}
}
