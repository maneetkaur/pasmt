package com.finance.dashboard;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.commonFunc.CommonBean;

public class ExpenseMapper implements RowMapper<CommonBean> {

	@Override
	public CommonBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		CommonBean expense = new CommonBean();
		String temp = rs.getString("service_category");
		if(temp.equals("1")){
			expense.setId("Sample");
		}
		else if(temp.equals("2")){
			expense.setId("Advertising");
		}
		else if(temp.equals("3")){
			expense.setId("Technology");
		}
		else if(temp.equals("4")){
			expense.setId("Certification");
		}
		else if(temp.equals("5")){
			expense.setId("Accounting");
		}
		else if(temp.equals("6")){
			expense.setId("Programming");
		}
		else if(temp.equals("7")){
			expense.setId("Panel Recruitment");
		}
		else if(temp.equals("8")){
			expense.setId("Incentive");
		}
		else if(temp.equals("9")){
			expense.setId("OSBT");
		}
		expense.setName(rs.getString("categorySum"));		
		return expense;
	}

}
