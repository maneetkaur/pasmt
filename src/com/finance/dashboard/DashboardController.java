package com.finance.dashboard;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DashboardController {
	
	@Autowired
	DashboardDao dashboardDao;
	
	@RequestMapping(value="/sAdminFinanceDashboard")
	public ModelAndView displayFinanceDashboard(HttpServletRequest request, 
			@ModelAttribute("balanceSheet") DashboardBean balanceSheet,
			@ModelAttribute("cashflow") DashboardBean cashflow){
		ModelAndView financeDashboard = new ModelAndView("finance/dashboard/financeDashboard");
		financeDashboard.addObject("bsExpenseList", dashboardDao.getBalanceSheetExpense(balanceSheet));
		financeDashboard.addObject("bsIncomeList", dashboardDao.getBalanceSheetIncome(balanceSheet));
		financeDashboard.addObject("cfExpenseList", dashboardDao.getCashflowExpense(cashflow));
		financeDashboard.addObject("cfIncomeList", dashboardDao.getCashflowIncome(cashflow));		
		return financeDashboard;
	}

}

