package com.finance.dashboard;

import java.util.List;

import com.commonFunc.CommonBean;

public interface DashboardDao {
	public List<CommonBean> getBalanceSheetIncome(DashboardBean balanceSheet);
	
	public List<CommonBean> getBalanceSheetExpense(DashboardBean balanceSheet);
	
	public List<CommonBean> getCashflowIncome(DashboardBean cashflow);
	
	public List<CommonBean> getCashflowExpense(DashboardBean cashflow);

}
