package com.projects.surveys;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SurveyListMapper implements RowMapper<SurveyBean>{

	@Override
	public SurveyBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		SurveyBean survey = new SurveyBean();
		survey.setSurveyId(rs.getString("ls.survey_id"));
		survey.setProjectId(rs.getString("lp.proposal_id"));
		survey.setProjectManager(rs.getString("pe.employee_name"));
		survey.setSurveyCode(rs.getString("ls.survey_code"));
		survey.setCulture(rs.getString("ls.culture_id"));
		survey.setStartDate(rs.getString("date_format(ls.start_date, '%b %e, %Y')"));
		survey.setLoi(rs.getString("ls.loi"));
		survey.setCloseDate(rs.getString("date_format(ls.end_date, '%b %e, %Y')"));
		String temp = "";
		if(rs.getString("ls.survey_status") != null){
			int status = Integer.parseInt(rs.getString("ls.survey_status"));
			if(status==1){
				temp = "Pending";
			}
			else if(status==2){
				temp="On Hold";
			}
			else if(status==3){
				temp="Testing";
			}
			else if(status==4){
				temp="Live";
			}
			else if(status==5){
				temp="Closed";
			}
		}
		survey.setSurveyStatus(temp);
		return survey;
	}

}
