package com.projects.surveys;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface SurveyDao {
	public List<SurveyBean> surveyList(HttpServletRequest request, SurveyBean survey, int startIndex, int range);

}
