package com.projects.surveys;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.functors.InstantiateFactory;
import org.apache.commons.collections.list.LazyList;

public class SurveyBean {
	
	private String surveyId, projectId, surveyCode, wave, culture, surveyCategory, completesQuota, softLaunchQuota,
		incidenceRate, loi, cpi, maxIncentiveBudget, pastParticipationDays, deDupeRespondents, startDate, closeDate,
		surveyType, urlType, reportingUrl, userId, password, surveyNumber, surveyDescription, surveyStatus, createdBy,
		statusChangedOn, statusChangedBy, status, createdOn;
	
	//for listing
	private String projectManager, clientCode, projectType;
		
	private String[] panel, approvedChannels, sensitiveIndustry, pastParticipationcategory;
	
	private List<UrlBean> urlList;
	
	public SurveyBean(){
		urlList = LazyList.decorate(new ArrayList<UrlBean>(),
				new InstantiateFactory(UrlBean.class));
	}

	public String getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getSurveyCode() {
		return surveyCode;
	}
	public void setSurveyCode(String surveyCode) {
		this.surveyCode = surveyCode;
	}
	public String getWave() {
		return wave;
	}
	public void setWave(String wave) {
		this.wave = wave;
	}
	public String getCulture() {
		return culture;
	}
	public void setCulture(String culture) {
		this.culture = culture;
	}
	public String getSurveyCategory() {
		return surveyCategory;
	}
	public void setSurveyCategory(String surveyCategory) {
		this.surveyCategory = surveyCategory;
	}
	public String getCompletesQuota() {
		return completesQuota;
	}
	public void setCompletesQuota(String completesQuota) {
		this.completesQuota = completesQuota;
	}
	public String getSoftLaunchQuota() {
		return softLaunchQuota;
	}
	public void setSoftLaunchQuota(String softLaunchQuota) {
		this.softLaunchQuota = softLaunchQuota;
	}
	public String getIncidenceRate() {
		return incidenceRate;
	}
	public void setIncidenceRate(String incidenceRate) {
		this.incidenceRate = incidenceRate;
	}
	public String getLoi() {
		return loi;
	}
	public void setLoi(String loi) {
		this.loi = loi;
	}
	public String getCpi() {
		return cpi;
	}
	public void setCpi(String cpi) {
		this.cpi = cpi;
	}
	public String getMaxIncentiveBudget() {
		return maxIncentiveBudget;
	}
	public void setMaxIncentiveBudget(String maxIncentiveBudget) {
		this.maxIncentiveBudget = maxIncentiveBudget;
	}
	public String getPastParticipationDays() {
		return pastParticipationDays;
	}
	public void setPastParticipationDays(String pastParticipationDays) {
		this.pastParticipationDays = pastParticipationDays;
	}
	public String getDeDupeRespondents() {
		return deDupeRespondents;
	}
	public void setDeDupeRespondents(String deDupeRespondents) {
		this.deDupeRespondents = deDupeRespondents;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(String closeDate) {
		this.closeDate = closeDate;
	}
	public String getSurveyType() {
		return surveyType;
	}
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	public String getUrlType() {
		return urlType;
	}
	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}
	public String getReportingUrl() {
		return reportingUrl;
	}
	public void setReportingUrl(String reportingUrl) {
		this.reportingUrl = reportingUrl;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSurveyNumber() {
		return surveyNumber;
	}
	public void setSurveyNumber(String surveyNumber) {
		this.surveyNumber = surveyNumber;
	}
	public String getSurveyDescription() {
		return surveyDescription;
	}
	public void setSurveyDescription(String surveyDescription) {
		this.surveyDescription = surveyDescription;
	}
	public String getSurveyStatus() {
		return surveyStatus;
	}
	public void setSurveyStatus(String surveyStatus) {
		this.surveyStatus = surveyStatus;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getStatusChangedOn() {
		return statusChangedOn;
	}
	public void setStatusChangedOn(String statusChangedOn) {
		this.statusChangedOn = statusChangedOn;
	}
	public String getStatusChangedBy() {
		return statusChangedBy;
	}
	public void setStatusChangedBy(String statusChangedBy) {
		this.statusChangedBy = statusChangedBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String[] getPanel() {
		return panel;
	}
	public void setPanel(String[] panel) {
		this.panel = panel;
	}
	public String[] getApprovedChannels() {
		return approvedChannels;
	}
	public void setApprovedChannels(String[] approvedChannels) {
		this.approvedChannels = approvedChannels;
	}
	public String[] getSensitiveIndustry() {
		return sensitiveIndustry;
	}
	public void setSensitiveIndustry(String[] sensitiveIndustry) {
		this.sensitiveIndustry = sensitiveIndustry;
	}
	public String[] getPastParticipationcategory() {
		return pastParticipationcategory;
	}
	public void setPastParticipationcategory(String[] pastParticipationcategory) {
		this.pastParticipationcategory = pastParticipationcategory;
	}
	public List<UrlBean> getUrlList() {
		return urlList;
	}
	public void setUrlList(List<UrlBean> urlList) {
		this.urlList = urlList;
	}
	public String getProjectManager() {
		return projectManager;
	}
	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
}
