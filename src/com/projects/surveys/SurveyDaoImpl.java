package com.projects.surveys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;

@Repository
@Transactional
public class SurveyDaoImpl implements SurveyDao {
	
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	CommonFunction commonFunction;

	@Override
	public List<SurveyBean> surveyList(HttpServletRequest request,
			SurveyBean survey, int startIndex, int range) {
		StringBuffer where_clause;
		String temp;
		String sql = "SELECT ls.survey_id, lp.proposal_id, pe.employee_name, ls.survey_code, ls.culture_id, " +
				"date_format(ls.start_date, '%b %e, %Y'), ls.loi, date_format(ls.end_date, '%b %e, %Y'), " +
				"ls.survey_status FROM lu_survey ls JOIN lu_project lp ON ls.project_id=lp.project_id JOIN " +
				"pasmt_employee pe ON lp.project_manager=pe.employee_id ";
		where_clause = new StringBuffer("WHERE ls.status=1 AND ");

		if(survey.getSurveyCode()!=null && !survey.getSurveyCode().equals("")){
			where_clause.append("ls.survey_code like '%" + survey.getSurveyCode() + "%' AND ");
		}
		
		if(survey.getCulture()!=null){
			temp = survey.getCulture();
			if(!temp.equals("") && !temp.equals("0")){
				where_clause.append("ls.culture_id=" + temp + " AND ");
			}
		}
		
		if(survey.getSurveyStatus() != null){
			temp = survey.getSurveyStatus();
			if(!temp.equals("") && !temp.equals("0")){
				where_clause.append("ls.survey_status=" + temp + " AND ");
			}
		}
		
		String where=where_clause.toString().trim();
		where_clause = null;
		
		//Remove the extra 'AND' in the where clause
		where=where.substring(0, where.length()-4);
		
		//Calculate count for pagination
		String sql_count = "SELECT count(1) FROM lu_survey ls JOIN lu_project lp ON ls.project_id=lp.project_id JOIN "
			+ "pasmt_employee pe ON lp.project_manager=pe.employee_id ";
		sql_count = sql_count + where;
		int total_rows = jdbcTemplate.queryForInt(sql_count);		
		request.setAttribute("total_rows", total_rows);
		
		String limit = " ORDER BY ls.survey_id DESC LIMIT " + startIndex + ", " + range;
		sql = sql + where + limit;		
		List<SurveyBean> surveyList = jdbcTemplate.query(sql, new SurveyListMapper());
		
		Map<String, String> cultureMap = new HashMap<String, String>();
		for(CommonBean commonBean: commonFunction.getCultureList()){
			cultureMap.put(commonBean.getId(), commonBean.getCode());
		}
		
		for(SurveyBean tempSurvey: surveyList){
			tempSurvey.setCulture(cultureMap.get(tempSurvey.getCulture().toString()));
			sql = "SELECT pc.client_code, pb.project_type FROM pasmt_proposal pp JOIN pasmt_bid pb ON " +
					"pp.bid_number = pb.bid_id JOIN pasmt_client pc ON pb.client_id = pc.client_id WHERE " +
					"pp.proposal_id=" + tempSurvey.getProjectId();
			Map map = new HashMap();
			map = jdbcTemplate.queryForMap(sql);
			tempSurvey.setClientCode(map.get("client_code").toString());
			temp = map.get("project_type").toString();
			//1=AdHoc, 2=Tracker
			if(temp.equals("1")){
				temp = "AdHoc";
			}
			else if(temp.equals("2")){
				temp = "Tracker";
			}
			tempSurvey.setProjectType(temp);
		}
		return surveyList;
	}

}
