package com.projects.surveys;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;

@Controller
public class SurveyController {
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	SurveyDao surveyDao;
	
	@RequestMapping(value="/surveyList")
	public ModelAndView surveyList(HttpServletRequest request, @ModelAttribute("searchSurvey") SurveyBean searchSurvey){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView survey = new ModelAndView("projects/surveys/surveyList");
		List<SurveyBean> surveyList = surveyDao.surveyList(request, searchSurvey, startIndex, 15);
		survey.addObject("surveyList", surveyList);
		survey.addObject("cultureList", commonFunction.getCultureList());
		surveyList=null;
		return survey;
	}
	
	@RequestMapping(value="/addSurveyForm")
	public ModelAndView addSurveyForm(){
		ModelAndView addSurvey = new ModelAndView("projects/surveys/addSurvey");
		addSurvey.addObject("addSurvey", new SurveyBean());
		addSurvey.addObject("surveyCategoryList", commonFunction.getSurveyCategories());
		return addSurvey;
	}

}
