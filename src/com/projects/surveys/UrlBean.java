package com.projects.surveys;

import java.util.List;

public class UrlBean {
	private String liveUrl, testUrl, maskClientUrl, countPages, redirectType;
	
	private List<VariableBean> variables;

	public String getLiveUrl() {
		return liveUrl;
	}
	public void setLiveUrl(String liveUrl) {
		this.liveUrl = liveUrl;
	}
	public String getTestUrl() {
		return testUrl;
	}
	public void setTestUrl(String testUrl) {
		this.testUrl = testUrl;
	}
	public String getMaskClientUrl() {
		return maskClientUrl;
	}
	public void setMaskClientUrl(String maskClientUrl) {
		this.maskClientUrl = maskClientUrl;
	}
	public String getCountPages() {
		return countPages;
	}
	public void setCountPages(String countPages) {
		this.countPages = countPages;
	}
	public String getRedirectType() {
		return redirectType;
	}
	public void setRedirectType(String redirectType) {
		this.redirectType = redirectType;
	}
	public List<VariableBean> getVariables() {
		return variables;
	}
	public void setVariables(List<VariableBean> variables) {
		this.variables = variables;
	}
}
