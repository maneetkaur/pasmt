package com.projects.surveys;

public class VariableBean {
	private String rIdSequence, rIdType, rIdLabel, rIdTestRangeStart, rIdTestRangeEnd, rIdLiveRangeStart, 
		rIdLiveRangeEnd, sId, source, channel, time, ip;
	//List of test and live client ids

	public String getrIdSequence() {
		return rIdSequence;
	}
	public void setrIdSequence(String rIdSequence) {
		this.rIdSequence = rIdSequence;
	}
	public String getrIdType() {
		return rIdType;
	}
	public void setrIdType(String rIdType) {
		this.rIdType = rIdType;
	}
	public String getrIdLabel() {
		return rIdLabel;
	}
	public void setrIdLabel(String rIdLabel) {
		this.rIdLabel = rIdLabel;
	}
	public String getrIdTestRangeStart() {
		return rIdTestRangeStart;
	}
	public void setrIdTestRangeStart(String rIdTestRangeStart) {
		this.rIdTestRangeStart = rIdTestRangeStart;
	}
	public String getrIdTestRangeEnd() {
		return rIdTestRangeEnd;
	}
	public void setrIdTestRangeEnd(String rIdTestRangeEnd) {
		this.rIdTestRangeEnd = rIdTestRangeEnd;
	}
	public String getrIdLiveRangeStart() {
		return rIdLiveRangeStart;
	}
	public void setrIdLiveRangeStart(String rIdLiveRangeStart) {
		this.rIdLiveRangeStart = rIdLiveRangeStart;
	}
	public String getrIdLiveRangeEnd() {
		return rIdLiveRangeEnd;
	}
	public void setrIdLiveRangeEnd(String rIdLiveRangeEnd) {
		this.rIdLiveRangeEnd = rIdLiveRangeEnd;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
}
