package com.projects.projects;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.commonFunc.CommonFunction;
import com.sales.proposal.ProposalBean;
import com.sales.proposal.ProposalDao;

@Controller
public class ProjectController {
	
	@Autowired
	CommonFunction commonFunction;
	
	@Autowired
	ProjectDao projectDao;
	
	@Autowired
	ProposalDao proposalDao;
	
	@RequestMapping(value="/projectList")
	public ModelAndView projectList(HttpServletRequest request, 
					@ModelAttribute("searchProject") ProjectBean project){
		int startIndex=0;
		if(request.getParameter("pager.offset")!=null){
			startIndex = Integer.parseInt(request.getParameter("pager.offset"));
		}
		ModelAndView projectModel = new ModelAndView("projects/projects/projectList");		
		List<ProjectBean> projectList = projectDao.projectList(request, project, startIndex, 15);
		projectModel.addObject("projectStatusList", commonFunction.getProjectStatusList());
		projectModel.addObject("projectList", projectList);
		projectList=null;		
		return projectModel;
	}
	
	@RequestMapping(value="/addProjectForm")
	public ModelAndView addProjectForm(HttpServletRequest request){
		ModelAndView addProject = new ModelAndView("projects/projects/addProject");
		addProject.addObject("proposalList", projectDao.getProposalList());
		addProject.addObject("managerList", commonFunction.getManagerList());
		addProject.addObject("projectStatusList", commonFunction.getProjectStatusList());
		addProject.addObject("cultureList", commonFunction.getCultureList());
		ProjectBean project = new ProjectBean();
		
		if(request.getParameter("proposalId")!=null && !(request.getParameter("proposalId").equals("0"))){
			String proposalId = request.getParameter("proposalId");
			project.setProposalId(proposalId);
			ProposalBean proposal = projectDao.getProposalInformation(proposalId);
			addProject.addObject("proposal", proposal);
			proposal = null;			
		}
		else{
			addProject.addObject("proposal", new ProposalBean());
		}
		addProject.addObject("addProject", project);
		return addProject;
		
	}
	
	@RequestMapping(value="/getContactInfo", method=RequestMethod.GET)
	public String getContactInfo(HttpServletRequest request, ModelMap model){
		String contactInfo = projectDao.getContactInfo(request.getParameter("contactId"));
		model.addAttribute("contactInfo", contactInfo);
		return "ajaxOperation/ajaxOperationProjects";
	}
	
	@RequestMapping(value="/addProject", method=RequestMethod.POST)
	public String addProject(@ModelAttribute("addProject") ProjectBean project, ModelMap model){
		projectDao.addProject(project);
		return "redirect:/projectList";
	}
	
	@RequestMapping(value="/editProjectForm")
	public ModelAndView editProjectForm(HttpServletRequest request){
		ModelAndView editProject = new ModelAndView("projects/projects/editProject");
		editProject.addObject("managerList", commonFunction.getManagerList());
		editProject.addObject("projectStatusList", commonFunction.getProjectStatusList());
		editProject.addObject("cultureList", commonFunction.getCultureList());
		ProjectBean project = projectDao.getProjectInformation(request.getParameter("projectId"));
		editProject.addObject("editProject", project);
		editProject.addObject("proposal", projectDao.getProposalInformation(project.getProposalId()));
		editProject.addObject("pmContactInfo", projectDao.getContactInfo(project.getProjectManager()));
		editProject.addObject("tlContactInfo", projectDao.getContactInfo(project.getTeamLeader()));
		return editProject;
	}
	
	@RequestMapping(value="/editProject", method=RequestMethod.POST)
	public String editProject(@ModelAttribute("editProject") ProjectBean project, ModelMap model){
		projectDao.editProject(project);		
		return "redirect:/projectList";
	}
}
