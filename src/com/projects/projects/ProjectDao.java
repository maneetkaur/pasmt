package com.projects.projects;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.commonFunc.CommonBean;
import com.sales.proposal.ProposalBean;

public interface ProjectDao {	
	public List<ProjectBean> projectList(HttpServletRequest request, ProjectBean project, int startIndex, int range);
	
	public List<CommonBean> getProposalList();
	
	public ProposalBean getProposalInformation(String proposalId);
	
	public void addProject(ProjectBean project);
	
	public String getContactInfo(String employeeId);
	
	public ProjectBean getProjectInformation(String projectId);

	void editProject(ProjectBean project);
}
