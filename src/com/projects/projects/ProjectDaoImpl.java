package com.projects.projects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.commonFunc.CommonBean;
import com.commonFunc.CommonFunction;
import com.sales.client.ContactBean;
import com.sales.proposal.ProposalBean;
import com.sales.proposal.ProposalDao;

@Repository
@Transactional
public class ProjectDaoImpl implements ProjectDao{
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	@Qualifier("dataSource")
	public void setJdbcTemplate(DataSource dataSource){
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	ProposalDao proposalDao;
	
	@Autowired
	CommonFunction commonFunction;

	@Override
	public List<ProjectBean> projectList(HttpServletRequest request, ProjectBean project, int startIndex,
			int range) {
		List<ProjectBean> projectList = new ArrayList<ProjectBean>();
		String sql, sql_count, temp;
		StringBuffer where_clause;		
		List<CommonBean> projectStatusList = commonFunction.getProjectStatusList();
		Map<String, String> projectStatusMap = commonFunction.createMapFromList(projectStatusList);
		projectStatusList = null;
		/*
		 * Project Name comes from bid table. If it is present in search criteria, then join the project, proposal
		 * and bid table. Query separately to get project status, client and employee details
		 * If project name is not present in search criteria, then join the project, proposal and employee table.
		 * Query separately to get project status, bid and client information 
		 */
		//Project name in search criteria
		if(project!=null && project.getProjectName()!=null && !project.getProjectName().equals("")){
			sql = "SELECT lp.project_id, pp.proposal_number, lp.project_manager, lp.project_status, pb.bid_name, " +
					"pb.project_type, pb.client_id FROM lu_project lp JOIN pasmt_proposal pp ON lp.proposal_id = " +
					"pp.proposal_id JOIN pasmt_bid pb ON pp.bid_number=pb.bid_id ";
			where_clause = new StringBuffer("WHERE lp.status=1 AND ");
			
			if(project.getProjectStatus() != null){
				String projectStatus = project.getProjectStatus();
				if(!projectStatus.equals("") && !projectStatus.equals("0")){
					where_clause.append("lp.project_status like '%" + projectStatus + "%' AND ");
				}
			}
			
			if(project.getProjectNumber() != null && !(project.getProjectNumber().equals(""))){
				where_clause.append("pp.proposal_number like '%" + project.getProjectNumber() + "%' AND ");
			}
						
			//project name is not empty
			where_clause.append("pb.bid_name like '%" + project.getProjectName() + "%' AND ");
			
			String where=where_clause.toString().trim();
			where_clause = null;
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			
			//Calculate count for pagination
			sql_count = "SELECT count(1) FROM lu_project lp JOIN pasmt_proposal pp ON lp.proposal_id = " +
					"pp.proposal_id JOIN pasmt_bid pb ON pp.bid_number=pb.bid_id ";
			sql_count = sql_count + where;
			int total_rows = jdbcTemplate.queryForInt(sql_count);		
			request.setAttribute("total_rows", total_rows);
			
			String limit = " ORDER BY lp.project_id DESC LIMIT " + startIndex + ", " + range;
			sql = sql + where + limit;
			
			projectList = jdbcTemplate.query(sql, new RowMapper<ProjectBean>(){

				@Override
				public ProjectBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					ProjectBean tempProject = new ProjectBean();
					tempProject.setProjectId(rs.getString("lp.project_id"));
					tempProject.setProjectNumber(rs.getString("pp.proposal_number"));
					tempProject.setProjectManager(rs.getString("lp.project_manager"));
					tempProject.setProjectStatus(rs.getString("lp.project_status"));
					tempProject.setProjectName(rs.getString("pb.bid_name"));
					tempProject.setProjectType(rs.getString("pb.project_type"));
					tempProject.setClientCode(rs.getString("pb.client_id"));
					return tempProject;
				}				
			});
			
			Map<String, String> managerMap = commonFunction.createMapFromList(commonFunction.getManagerList());
			if(projectList!= null && projectList.size()>0){
				for(ProjectBean tempProject: projectList){
					tempProject.setProjectManager(managerMap.get(tempProject.getProjectManager()).toString());
					temp = tempProject.getProjectType();
					if(temp.equals("1")){
						tempProject.setProjectType("AdHoc");
					}
					else if(temp.equals("2")){
						tempProject.setProjectType("Tracker");
					}
					tempProject.setProjectStatus(projectStatusMap.get(tempProject.getProjectStatus()).toString());
					
					//get client code
					sql = "SELECT pc.client_code FROM pasmt_client pc WHERE pc.client_id=" + 
							tempProject.getClientCode();
					tempProject.setClientCode(jdbcTemplate.queryForObject(sql, String.class));
				}
			}
		}
		//Project name not in search criteria
		else{
			sql = "SELECT lp.project_id, pp.proposal_number, pe.employee_name, lp.project_status, pp.bid_number " +
					"FROM lu_project lp JOIN pasmt_proposal pp ON lp.proposal_id = pp.proposal_id JOIN " +
					"pasmt_employee pe ON lp.project_manager = pe.employee_id ";
			where_clause = new StringBuffer("WHERE lp.status=1 AND ");
			
			if(project.getProjectStatus() != null){
				String projectStatus = project.getProjectStatus();
				if(!projectStatus.equals("") && !projectStatus.equals("0")){
					where_clause.append("lp.project_status like '%" + projectStatus + "%' AND ");
				}
			}
			
			if(project.getProjectNumber() != null && !(project.getProjectNumber().equals(""))){
				where_clause.append("pp.proposal_number like '%" + project.getProjectNumber() + "%' AND ");
			}
			
			String where=where_clause.toString().trim();
			where_clause = null;
			
			//Remove the extra 'AND' in the where clause
			where=where.substring(0, where.length()-4);
			
			//Calculate count for pagination
			sql_count = "SELECT count(1) FROM lu_project lp JOIN pasmt_proposal pp ON lp.proposal_id = pp.proposal_id" +
				" JOIN pasmt_employee pe ON lp.project_manager = pe.employee_id ";
			sql_count = sql_count + where;
			int total_rows = jdbcTemplate.queryForInt(sql_count);		
			request.setAttribute("total_rows", total_rows);
			
			String limit = " ORDER BY lp.project_id DESC LIMIT " + startIndex + ", " + range;
			sql = sql + where + limit;
			
			projectList = jdbcTemplate.query(sql, new RowMapper<ProjectBean>(){

				@Override
				public ProjectBean mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					ProjectBean projectTemp = new ProjectBean();
					projectTemp.setProjectId(rs.getString("lp.project_id"));
					projectTemp.setProjectNumber(rs.getString("pp.proposal_number"));
					projectTemp.setProjectManager(rs.getString("pe.employee_name"));
					projectTemp.setProjectStatus(rs.getString("lp.project_status"));
					projectTemp.setBidId(rs.getString("pp.bid_number"));
					return projectTemp;
				}			
			});
			if(projectList != null && projectList.size()>0){								
				for(ProjectBean tempProject: projectList){
					sql = "SELECT pb.bid_name, pb.project_type, pc.client_code FROM pasmt_bid pb JOIN pasmt_client" +
							" pc ON pb.client_id = pc.client_id WHERE pb.bid_id=" + tempProject.getBidId();					
					Map map = new HashMap();
					map = jdbcTemplate.queryForMap(sql);					
					tempProject.setProjectName(map.get("bid_name").toString());
					tempProject.setClientCode(map.get("client_code").toString());
					temp = map.get("project_type").toString();
					if(temp.equals("1")){
						tempProject.setProjectType("AdHoc");
					}
					else if(temp.equals("2")){
						tempProject.setProjectType("Tracker");
					}
					
					//get project status
					temp = projectStatusMap.get(tempProject.getProjectStatus()).toString();
					tempProject.setProjectStatus(temp);
				}				
			}
		}
				
		return projectList;
	}

	@Override
	public List<CommonBean> getProposalList() {
		List<CommonBean> proposalList;
		String sql = "SELECT proposal_id, proposal_number FROM pasmt_proposal WHERE proposal_id NOT IN " +
				"(SELECT proposal_id FROM lu_project)";
		proposalList = jdbcTemplate.query(sql, new RowMapper<CommonBean>(){

			@Override
			public CommonBean mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				CommonBean proposal = new CommonBean();
				proposal.setId(rs.getString("proposal_id"));
				proposal.setName(rs.getString("proposal_number"));
				return proposal;
			}			
		});
		return proposalList;
	}

	@Override
	public ProposalBean getProposalInformation(String proposalId) {
		ProposalBean proposal = proposalDao.getProposalInformation(proposalId);
		//set contact names
		ContactBean contact = proposalDao.getContactInfo(proposal.getClientContact(), 0);
		proposal.setClientContact(contact.getName());
		contact = proposalDao.getContactInfo(proposal.getClientProjectManager(), 0);
		proposal.setClientProjectManager(contact.getName());
		//change requirements values
		proposal.setProposalRequirements(proposalDao.getRequirementsForPDF(proposal.getProposalRequirements()));
		//change date format		
		proposal.setStartDate(commonFunction.changeDateFormat(proposal.getStartDate()));
		proposal.setEndDate(commonFunction.changeDateFormat(proposal.getEndDate()));
		return proposal;
	}
	
	@Override
	public void addProject(ProjectBean project) {
		String sql = "INSERT lu_project (proposal_id, project_manager, team_leader, team_name, culture_id, " +
				"project_status, pm_remark, status, created_on) VALUES (?,?,?,?,?, ?,?,?,?)";
		String cultureString = ("" + 
				Arrays.asList(project.getCulture())).replaceAll("(^.|.$)", "").replace(", ", "," );
		jdbcTemplate.update(sql, new Object[]{project.getProposalId(), project.getProjectManager(), 
				project.getTeamLeader(), project.getTeamName(), cultureString, project.getProjectStatus(),
				project.getPmRemark(), 1, new Date()});
		
	}

	@Override
	public String getContactInfo(String employeeId) {
		if(employeeId.equals("0")){
			return ":";
		}
		String sql = "SELECT email_id, phone FROM pasmt_employee WHERE employee_id=" + employeeId;		
		return jdbcTemplate.queryForObject(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("email_id") + ":" + rs.getString("phone");
			}			
		});
	}

	@Override
	public ProjectBean getProjectInformation(String projectId) {
		String sql = "SELECT project_id, proposal_id, project_manager, team_leader, team_name, culture_id, " +
				"project_status, pm_remark FROM lu_project WHERE project_id=" + projectId;
		ProjectBean project = jdbcTemplate.queryForObject(sql, new EditProjectMapper());
		//set last project status as current for reference
		project.setLastProjectStatus(project.getProjectStatus());
		
		sql = "SELECT distinct(culture_id) FROM lu_survey where project_id=2";
		List<String> selectedCultures = jdbcTemplate.query(sql, new RowMapper<String>(){

			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {				
				return rs.getString("culture_id");
			}			
		});
				
		if(selectedCultures!=null && selectedCultures.size()>0){
			Map<String, String> tempMap = new HashMap<String, String>();
			for(String culture: selectedCultures){
				tempMap.put(culture, culture);
			}
			project.setCultureMap(tempMap);
		}
		
		return project;
	}
	
	@Override
	public void editProject(ProjectBean project) {
		String sql = "UPDATE lu_project SET project_manager=?, team_leader=?, team_name=?, culture_id=?, " +
				"project_status=?, pm_remark=? WHERE project_id=?";
		String cultureString = ("" + 
				Arrays.asList(project.getCulture())).replaceAll("(^.|.$)", "").replace(", ", "," );
		jdbcTemplate.update(sql, new Object[]{project.getProjectManager(), project.getTeamLeader(), 
				project.getTeamName(), cultureString, project.getProjectStatus(),project.getPmRemark(), 
				project.getProjectId()});
		
		/*
		 * Change by Maneet
		 * Date- 05 March'14
		 * if project status has changed and is closure, then send an email to billing@irbureau.com to remind
		 *  to raise an invoice
		 */
		if(!project.getProjectStatus().equals(project.getLastProjectStatus())){
			if(project.getProjectStatus().equals("48")){
				//add code to send an email
			}
		}
	}

}
