package com.projects.projects;

import java.util.Map;


public class ProjectBean {
	private String projectId, projectManager, clientPM, teamLeader, teamName, projectStatus, pmRemark,
		status, createdOn;
	private String[] culture;
	//Added lastProjectStatus on 5 March'14 by Maneet
	private String proposalId, bidId, lastProjectStatus;
	
	//Map for getting selected cultures from survey
	private Map<String, String> cultureMap;
	
	/*Fields needed only for listing
	 * projectNumber, projectName come from proposal
	 * clientCode, projectType come from bid through proposal
	 */
	private String projectNumber, projectName, clientCode, projectType;
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectManager() {
		return projectManager;
	}
	public void setProjectManager(String projectManager) {
		this.projectManager = projectManager;
	}
	public String getClientPM() {
		return clientPM;
	}
	public void setClientPM(String clientPM) {
		this.clientPM = clientPM;
	}
	public String getTeamLeader() {
		return teamLeader;
	}
	public void setTeamLeader(String teamLeader) {
		this.teamLeader = teamLeader;
	}
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public String getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}
	public String getPmRemark() {
		return pmRemark;
	}
	public void setPmRemark(String pmRemark) {
		this.pmRemark = pmRemark;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getProjectNumber() {
		return projectNumber;
	}
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getProposalId() {
		return proposalId;
	}
	public void setProposalId(String proposalId) {
		this.proposalId = proposalId;
	}
	public String getBidId() {
		return bidId;
	}
	public void setBidId(String bidId) {
		this.bidId = bidId;
	}
	public String[] getCulture() {
		return culture;
	}
	public void setCulture(String[] culture) {
		this.culture = culture;
	}
	public Map<String, String> getCultureMap() {
		return cultureMap;
	}
	public void setCultureMap(Map<String, String> cultureMap) {
		this.cultureMap = cultureMap;
	}
	public String getLastProjectStatus() {
		return lastProjectStatus;
	}
	public void setLastProjectStatus(String lastProjectStatus) {
		this.lastProjectStatus = lastProjectStatus;
	}
}
