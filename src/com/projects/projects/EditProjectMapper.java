package com.projects.projects;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EditProjectMapper implements RowMapper<ProjectBean>{

	@Override
	public ProjectBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProjectBean project = new ProjectBean();
		project.setProjectId(rs.getString("project_id"));
		project.setProposalId(rs.getString("proposal_id"));
		project.setProjectManager(rs.getString("project_manager"));
		project.setTeamLeader(rs.getString("team_leader"));
		project.setTeamName(rs.getString("team_name"));
		String temp = rs.getString("culture_id");
		if(temp != null && !temp.equals("")){
			project.setCulture(temp.split(","));
		}		
		project.setProjectStatus(rs.getString("project_status"));
		project.setPmRemark(rs.getString("pm_remark"));
		return project;
	}

}
