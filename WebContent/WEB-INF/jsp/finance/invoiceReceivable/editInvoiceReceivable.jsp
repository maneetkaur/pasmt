<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit Invoice Receivable - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- month picker starts -->
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery-1.8.2-ui.css">
<!-- month picker ends -->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<style>
.calendar-off table.ui-datepicker-calendar {display:none !important;}
</style>
</head>
<body onLoad="irInitialize(1);">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit Invoice Receivable</h2>
      <form:form method="POST" action="sAdminEditInvoiceReceivable" modelAttribute="editInvoiceReceivable">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span>
              	<span style="float: right;">
              	<a href="sAdminDownloadInvoiceReceivable?invoiceReceivableId=${editInvoiceReceivable.invoiceReceivableId}">
                			<img alt="Download PDF" src="images/btndownload-pdf.png">
                </a>
              </span>
              </td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
           <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">        
            <tr>
              <td width="0" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td height="31" align="left" valign="top">
                  	  <table width="1080" border="0" cellspacing="0" cellpadding="0">                        
                        <tr>
                          <td align="left" valign="top">
                           <table style="margin-left:10px; " width="1041" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                              <td height="31" align="left" valign="middle" width="235">
                              	<form:hidden path="invoiceReceivableId"/>
                              	<label>Project Number</label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${editInvoiceReceivable.projectNumber}"
                              	 		class="inptform">                                                            	                              
                              </td>
                              <td height="31" align="left" valign="middle" width="250">&nbsp;
                              	</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;
                              	                             	
                              </td>
                            </tr>                            
                            <tr>
                              <td height="31" align="left" valign="middle"><label>Client Name</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.bid.clientCode}"
                              	 class="inptform">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Project Name</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.bid.bidName}" class="inptform">                             	
                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label>Project Type</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.bid.projectType}"
                                 class="inptform">
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Account Manager</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.bid.accountManager}"
                                 class="inptform">
                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="waveMonth">Wave/Month</form:label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<form:input path="waveMonth" class="inptform" disabled="true" 
                              		value="${proposal.notesOnTargeting}"/>                                                                 
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label for="month">Month<span>*</span></label>
                              </td>                              
                              <td colspan="2" align="left" valign="middle">                              	
                              	 <input type="text" id="month" name="month" class="monthyearpicker inptform"
                              	 value="${editInvoiceReceivable.month}" >
                              </td>                                                           
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="invoiceTo">Invoice To<span>*</span></form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                                 
                                 <form:select path="invoiceTo" class="inptformSelect" style="width:243px;">
                                 	<form:option value="0" label="--Select--"/>
                                  	<form:options items="${clientContactList}" itemLabel="name" itemValue="id"/>
                                 </form:select>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="clientPO">Client PO#<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle">                              	                                 
                                 <form:input path="clientPO" class="inptform"/>
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="transactionType">Transaction Type<span>*</span></form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                                 
                                 <form:select path="transactionType" class="inptformSelect" style="width:243px;">
                                 	<form:option value="0" label="--Select--"/>
                                  	<form:option value="1" label="Local"/>
                                  	<form:option value="2" label="International"/>
                                 </form:select>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="description">Description of Services<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle">                              	                                 
                                 <form:input path="description" class="inptform"/>
                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="serviceTaxApplicable">Service Tax Applicable<span>*</span></form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                                 
                                 <form:select path="serviceTaxApplicable" class="inptformSelect" style="width:243px;"
                                 	onchange="displayServiceTax(this.value);">
                                 	<form:option value="0" label="--Select--"/>
                                  	<form:option value="1" label="Yes"/>
                                  	<form:option value="2" label="No"/>
                                 </form:select>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="serviceTax">Service Tax<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle">                              	                                 
                                 <form:input path="serviceTax" class="inptform" disabled="true" 
                                 	onchange="applyServiceTax(this.value);"/>
                              </td>
                            </tr>                            
                            <tr>
                              <td height="31" align="left" valign="middle">&nbsp;
                              	<input type="hidden" id="projectValue" value="${proposal.projectValue}">
                              	<form:hidden path="projectNumber" value="${proposal.proposalNumber}"/>
                              </td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                  	</td>
                  </tr>                
                <tr>
                  <td colspan="10" align="left" valign="top">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Pricing Info</h2></td>
        </tr>
        <%-- cost starts here --%>
        <tr>
          <td height="30" align="left" valign="middle" style="padding-left: 10px; color: black; font-size: 18px;">
          	<strong>Cost</strong>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" >
          <table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td width="82" align="left" valign="middle">Item No.</td>
              <td style="padding-left:0px;" width="152" align="left" valign="middle">Country</td>
              <td width="126" align="left" valign="middle">Services</td>              
              <td width="169" align="left" valign="middle">Description</td>
              <td width="166" align="left" valign="middle">Quantity</td>
              <td width="127" align="left" valign="middle">Unit Price</td>
              <td width="133" align="left" valign="middle">Setup Fee</td>
              <td width="168" align="left" valign="middle">Total Cost</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:10px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                               
                <!-- requirements go here -->
               <c:choose>
               <c:when test="${proposal.proposalRequirements.size()>0}">
               <tr>                
                <td colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >             
                   <tbody id="requirement">
                   <c:forEach items="${proposal.proposalRequirements}" var="req" varStatus="status">
                    <tr>
                      <td width="84" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${status.index+1}"
                      		id="r_no${status.index}" class="inptform" style="width:40px;">                      </td>
                      <td width="152" height="38" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.country}"
                                 class="inptform" style="width:80px;">                      </td>
                      <td width="126" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.services}"
                                 class="inptform" style="width:80px;" id="r_service${status.index}">                      </td>                      
                      <td width="170" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.description}" id="r_desc${status.index}"
                                 class="inptform" style="width:110px;">                      </td>
                      <td width="165" align="left" valign="middle">                      	
                      	   <input type="text" disabled="disabled" value="${req.sampleSize}" id="r_sample${status.index}"
                                 class="inptform" style="width:84px;"></td>
                      <td width="128" align="left" valign="middle">
                      	<input id="currencyValF${status.index}" type="text" class="inptform2" style="width:25px;
                      	 	font-size:11px;" disabled="disabled" value="${proposal.bid.billingCurrency}"/>                     
                      	<input type="text" disabled="disabled" value="${req.quotedCpi}" id="r_cpi${status.index}"
                                 class="inptform" style="width:43px;">                      </td>
                      <td width="133" align="left" valign="middle">
                        <input id="currencyVal${status.index}" type="text" class="inptform2" style="width:25px;
                         font-size:11px;" disabled="disabled" value="${proposal.bid.billingCurrency}"/>                      	
                      	<input type="text" disabled="disabled" value="${req.setupFee}" id="r_setup${status.index}"
                                 class="inptform" style="width:43px;">                      </td>
                      <td width="100" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="r_total${status.index}" size="10"
                      	 style="font-weight: bold; height: 30px;" readonly />                      </td>
                      <td width="63" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                    </c:forEach>
                   </tbody>
                  </table></td>                                                               
                </tr>				
                <tr>
                  <td colspan="11" align="left" valign="top"><table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >                      
                      <tr class="amboseff">
                        <td height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;">Sub Total</td>
                        <td width="116" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield14" type="text" class="inptformdis" id="totalSum" size="10" readonly/>						</td>
                        <td width="47" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>                      
                      <tr class="amboseff">
                        <td height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;">Project Minimum</td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input type="text" class="inptformdis" id="projectMinimum" size="10" readonly 
                        		value="${proposal.bid.billingCurrency} ${proposal.projectMinimum}"/>                        </td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                      <tr class="amboseff">
                        <td height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;">Final Project Cost</td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input type="text" class="inptformdis" id="finalProjectCost" size="10" readonly 
                        	value="${proposal.bid.billingCurrency} ${proposal.projectValue}"/>                        </td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                      <tr class="amboseff">
                        <td height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;">Service Tax</td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input type="text" class="inptformdis" id="stAmount" size="10" readonly/>                        </td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                      <tr class="amboseff">
                        <td height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;"><strong>Grand Total</strong></td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input type="text" class="inptformdis" id="grandTotal" size="10" readonly 
                        	style="font-weight: bold;"/>                        </td>
                        <td align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
                </c:when>                  
               </c:choose>
                <!-- requirements end here -->
       <tr>
          <td colspan="11" align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="11" align="left" valign="top">&nbsp;</td>
        </tr>
       <%--cost ends here --%>
                                                       
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
     	<td height="30" colspan="11" align="left" valign="top">
     	 <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-left: 10px;">
           <tr>
             <td width="241">
               <form:label path="amount">Total Amount</form:label>
             </td>
             <td width="274">
             	<input type="text" value="${proposal.bid.billingCurrency}" style="width: 40px;"
             		class="inptform">
             	<form:input path="amount" class="inptform" style="width:180px;" readonly="true"/>
             </td>
             <td width="246">
               <form:label path="amountUsd">Amount in USD<span>*</span></form:label>
             </td>
             <td width="342">
             	<input type="text" value="USD" style="width: 40px;" class="inptform">
             	<form:input path="amountUsd" class="inptform" style="width:180px;"
             		onchange="currencyFormat(this.value,this)"/>
             </td>
           </tr>
           <tr>
             <td>
               <form:label path="paymentMode">Payment Mode<span>*</span></form:label>
             </td>
             <td>
            	  <form:select path="paymentMode" class="inptformSelect" style="width:243px;">
                	<form:option value="0" label="--Select--"/>
           		<form:option value="1" label="Wire Transfer"/>
           		<form:option value="2" label="Paypal"/>
           <form:option value="3" label="Cheque"/>
           <form:option value="4" label="NetBanking"/>
                </form:select>
             </td>
             <td><form:label path="paymentTerms">Payment Terms<span>*</span></form:label></td>
             <td>                        	
             	<form:input path="paymentTerms" class="inptform" style="width:180px;" 
             		onchange="calculateDueDate(this.value);"/>
             	<input type="text" value="days" style="width:40px;" class="inptform">
             </td>
           </tr>
           <tr>
           <td>
           	<form:label path="bank_detail_id">Instruction for Remittance<span>*</span></form:label>
           </td>
           <td>
           	<form:select path="bankName" class="inptformSelect" style="width:243px;"
           		onchange="getBankAccList(this.value, 1);">
                	<form:option value="0" label="--Select--"/>
           		<form:options items="${bankList}" itemLabel="name" itemValue="id"/>
             </form:select>
           </td>
           <td><label for="bank_detail_id">Account Number<span>*</span></label></td>
           <td>
           	<select id="bank_detail_id" name="bank_detail_id" class="inptformSelect"
           		style="width:243px;">
              <option value="0" label="--Select--"/>
              <c:forEach items="${bankAccList}" var="bankAccVar">
                <c:choose>
              	<c:when test="${editInvoiceReceivable.bank_detail_id == bankAccVar.id}">
              	  <option value="${bankAccVar.id}" label="${bankAccVar.name}" selected/>
              	</c:when>
              	<c:otherwise>
              	  <option value="${bankAccVar.id}" label="${bankAccVar.name}"/>
              	</c:otherwise>
                </c:choose>	                          
              </c:forEach>                  
     		</select>
           </td>                      
           </tr>
           <tr>
             <td>
            	<label>Invoice Number</label>
            </td>
            <td>
            	<c:choose>
            	  <c:when test="${proposal.bid.projectType=='Tracker'}">
            	  	<input type="text" disabled="disabled" 
            	  		value="${editInvoiceReceivable.invoiceCode}-${editInvoiceReceivable.waveMonth}"
            	 		class="inptform">
            	  </c:when>
            	  <c:otherwise>
            	  	<input type="text" disabled="disabled" 
            	  		value="${editInvoiceReceivable.invoiceCode}" class="inptform">
            	  </c:otherwise>
            	</c:choose>                      	
            </td>
           	<td><form:label path="raisedOn">Raise On</form:label></td>
           	<td>
           	  <form:input path="raisedOn" readonly="true" class="inptform" 
           	  	style="background-color:#ebebe4;"/>
           	</td>                      	
           </tr>
           <tr>
             <td><form:label path="dueOn">Due On</form:label></td>
           	<td><form:input path="dueOn" disabled="true" class="inptform"/></td>
           	<td><form:label path="agingFor">Aging For</form:label></td>
           	<td><form:input path="agingFor" disabled="true" class="inptform"/></td>                      	
           </tr>
           <tr>
             <td><form:label path="overDueBy">Overdue By</form:label></td>
           	<td><form:input path="overDueBy" disabled="true" class="inptform"/></td>
           	<td colspan="2">&nbsp;</td>
           </tr>
           <tr>                       
           	<td colspan="4">&nbsp;
           	
           	</td>
           </tr>
           <tr>
           	<td colspan="4">&nbsp;
           	
           	</td>
           </tr>
           <tr>
           	<td colspan="4" style="font-size: 16px;">
           	<strong>Payment Details</strong>
           	</td>
           </tr>
           <tr>
           	<td colspan="4">&nbsp;
           	
           	</td>
           </tr>
           <tr>
           	<td><form:label path="paymentStatus">Payment Status<span>*</span></form:label></td>
           	<td>
           	  <form:select path="paymentStatus" class="inptformSelect" style="width:243px;">
                	<form:option value="0" label="--Select--"/>
           		<form:option value="1" label="Pending"/>
           		<form:option value="2" label="Paid"/>
           <form:option value="3" label="Declined"/>
               </form:select>
           	</td>
           	<td><form:label path="paymentRemarks">Payment Remarks</form:label></td>
           	<td><form:input path="paymentRemarks" class="inptform"/></td>                      	
           </tr>
           <tr>
           	<td><form:label path="paymentMadeOn">Payment Made On</form:label></td>
           	<td>                      		
           		 <input name="paymentMadeOn" id="paymentMadeOn" class="date-picker inptform" 
                   	 value="${editInvoiceReceivable.paymentMadeOn}"/>
           	</td>
           	<td><form:label path="paymentModeReceived">Payment Mode (Received)</form:label></td>
           	<td>
           	  <form:select path="paymentModeReceived" class="inptformSelect" style="width:243px;">
                	<form:option value="0" label="--Select--"/>
           		<form:option value="1" label="Wire Transfer"/>
           		<form:option value="2" label="Paypal"/>
           <form:option value="3" label="Cheque"/>
           <form:option value="4" label="NetBanking"/>
               </form:select>
           	</td>                      	
           </tr>
           <tr>
           	<td><form:label path="receivedBankName">Payment in Bank</form:label></td>
           	<td>
           	  <form:select path="receivedBankName" class="inptformSelect" style="width:243px;"
           	  	onchange="getBankAccList(this.value, 2);">
                	<form:option value="0" label="--Select--"/>
           		<form:options items="${bankList}" itemLabel="name" itemValue="id"/>
               </form:select>
           	</td>
           	<td><label for="receivedBankId">Account Number</label></td>
           	<td>
           		<select id="receivedBankId" name="receivedBankId" class="inptformSelect"
           		style="width:243px;">
              	<option value="0" label="--Select--"/>
              	<c:forEach items="${bankAccList}" var="bankAccVar">
                 <c:choose>
               	<c:when test="${editInvoiceReceivable.receivedBankId == bankAccVar.id}">
               	  <option value="${bankAccVar.id}" label="${bankAccVar.name}" selected/>
               	</c:when>
               	<c:otherwise>
               	  <option value="${bankAccVar.id}" label="${bankAccVar.name}"/>
               	</c:otherwise>
                 </c:choose>	                          
               </c:forEach>                       
     			</select>	              
             </td>                      	                      	
           </tr>
           <tr>
             <td><form:label path="transactionId">TransactionID</form:label></td>
           	<td><form:input path="transactionId" class="inptform"/></td>
           	<td><form:label path="netBankingNo">Net Banking No.</form:label></td>
           	<td><form:input path="netBankingNo" class="inptform"/></td>                      	                      	
           </tr>
           <tr>
             <td><form:label path="chequeNo">Cheque No.</form:label></td>
           	<td><form:input path="chequeNo" class="inptform"/></td>
             <td colspan="2">&nbsp;</td>
           </tr>
         </table>
        </td>
     </tr>                 
     <tr>
       <td colspan="11" align="left" valign="top">&nbsp;</td>
     </tr>            
          </table></td>
        </tr>        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit"
              		onclick="return invoiceReceivableValidation(1);"></td>
              <td width="118" align="center" valign="middle">
              	<a href="sAdminInvoiceReceivableList" class="btnsubmitLink" >Cancel</a>
              </td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
<!-- javascript starts here -->
<script src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common/commonScript.js"></script>
<script type="text/javascript" src="js/finance/invoiceReceivableScript.js"></script>
<script type="text/javascript">
	var reqSize = '${proposal.proposalRequirements.size()}';
	var currencyVal = '${proposal.bid.billingCurrency}';
</script>
  <script type="text/javascript">
  //for month picker
	$(function() {
	    $('#month').datepicker( {
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'MM-yy',
    beforeShow: function(input, inst) {
      $(inst.dpDiv).addClass('calendar-off');
      if((selDate = $(this).val()).length>0){
  		year = selDate.substring(selDate.length - 4, selDate.length);
          month = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
                   $(this).datepicker('option', 'monthNames'));
          $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
          $(this).datepicker('setDate', new Date(year, month, 1));
  		}
	  },
	    onClose: function(dateText, inst) { 
	      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	      $(this).datepicker('setDate', new Date(year, month, 1));
	    }
	});
	    
	    $( "#paymentMadeOn" ).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        beforeShow: function(input, inst) {
	          $(inst.dpDiv).removeClass('calendar-off');     
	        }
	      });
	});
  
  </script>
  <!-- javascript ends here -->
</body>
</html>
	