<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Invoice Receivable List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- month picker starts -->
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery-1.8.2-ui.css">
<!-- month picker ends -->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<!-- ui-datepicker-calendar for month picker -->
<style>
.ui-datepicker-calendar {
    display: none;
 }
 .searchbar label{
 	padding-left:10px;
 }
</style>
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddinvoicereceivable">
      <h3><a href="sAdminAddInvoiceReceivableForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:15px; width: 160px; padding-right: 5px;">Search Invoice Receivable</h2>
      	<form:form method="POST" modelAttribute="searchInvoiceReceivable" action="sAdminInvoiceReceivableList">
      		<form:label path="invoiceCode">Invoice #</form:label>
      		<form:input path="invoiceCode" style=" width:120px;"/>
			<form:label path="month">Month</form:label>
			<input name="month" id="month" class="date-picker" style="width: 120px;" 
				value="${searchInvoiceReceivable.month}"/>
			<form:label path="clientCode">Client Code</form:label>
			<form:input path="clientCode" style=" width:120px;"/>
       		<input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>       		
      	</form:form>      
      </div>
      </div>
      <div class="searchresult2">
      <h2>Invoice Receivable Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="226" align="left" valign="middle">Invoice Number</td>
                    		<td width="112" align="left" valign="middle">Client Code</td>
                    		<td width="133" align="left" valign="middle">Transaction Type</td>
                    		<td width="131" align="left" valign="middle">Amount</td>
		                    <td width="131" align="left" valign="middle">Raised On</td>
		                    <td width="136" align="left" valign="middle">Due On</td>
                  		    <td width="110" align="left" valign="middle">Overdue By</td>
               		        <td width="108" align="left" valign="middle">Payment Status</td>
                  		  </tr>
              			</table>
              		  </td>
            		</tr>            		
            	<c:if test="${not empty invoiceReceivableList}">
      			  <c:set var="pageUrl" value="sAdminInvoiceReceivableList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}"
      	  			 isOffset="true">            		
            	<%--start --%>
            	<pg:param name="invoiceCode" value="${param.invoiceCode}"/>
		        <pg:param name="month" value="${param.month}"/>
		        <pg:param name="clientCode" value="${param.clientCode}"/>
		       	<c:forEach items="${invoiceReceivableList}" var="inVar" varStatus="iStatus">
		       	
					<pg:item>       	
				      <c:if test="${iStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="sAdminEditInvoiceRecievableForm?invoiceReceivableId=${inVar.invoiceReceivableId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  	<td width="226" align="left" valign="middle">${inVar.invoiceCode}</td>
		                    		<td width="112" align="left" valign="middle">${inVar.clientCode}</td>
		                    		<td width="133" align="left" valign="middle">${inVar.transactionType}</td>
		                    		<td width="131" align="left" valign="middle">
		                    			${inVar.billingCurrency} ${inVar.amount}</td>
				                    <td width="131" align="left" valign="middle">${inVar.raisedOn}</td>
				                    <td width="136" align="left" valign="middle">${inVar.dueOn}</td>
		                  		    <td width="110" align="left" valign="middle">${inVar.overDueBy}</td>
		                  		    <td width="108" align="left" valign="middle">${inVar.paymentStatus}</td>
	                  		      </tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${iStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="sAdminEditInvoiceRecievableForm?invoiceReceivableId=${inVar.invoiceReceivableId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  	<td width="226" align="left" valign="middle">${inVar.invoiceCode}</td>
		                    		<td width="112" align="left" valign="middle">${inVar.clientCode}</td>
		                    		<td width="133" align="left" valign="middle">${inVar.transactionType}</td>
		                    		<td width="131" align="left" valign="middle">
		                    			${inVar.billingCurrency} ${inVar.amount}</td>
				                    <td width="131" align="left" valign="middle">${inVar.raisedOn}</td>
				                    <td width="136" align="left" valign="middle">${inVar.dueOn}</td>
		                  		    <td width="110" align="left" valign="middle">${inVar.overDueBy}</td>
		                  		    <td width="108" align="left" valign="middle">${inVar.paymentStatus}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
  
  <!-- javascript starts here -->
  <script src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript">
  //for month picker
	$(function() {
	    $('.date-picker').datepicker( {
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'MM-yy',
	        onClose: function(dateText, inst) { 
	            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            $(this).datepicker('setDate', new Date(year, month, 1));
	        },
	        beforeShow: function(){
	        	if((selDate = $(this).val()).length>0){
	        		year = selDate.substring(selDate.length - 4, selDate.length);
	                month = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
	                         $(this).datepicker('option', 'monthNames'));
	                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
	                $(this).datepicker('setDate', new Date(year, month, 1));
	        	}
	        }
	    });
	});
  </script>
  <!-- javascript ends here -->
</body>
</html>
    