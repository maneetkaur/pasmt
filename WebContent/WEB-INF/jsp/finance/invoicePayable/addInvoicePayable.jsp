<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Add Invoice Payable - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!-- date picker starts -->
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.date.js"></script>
		
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.date.js"></script>
        <script type="text/javascript" src="resources/datepicker/scripts/datepicker.js"></script>
        <link rel="stylesheet" href="resources/datepicker/css/datepicker.css" type="text/css" media="screen" /> 
<!--  date picker ends -->
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/common/commonScript.js"></script>

</head>
<body onLoad="initialize();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Invoice Payable</h2>
      <form:form method="POST" action="sAdminSubmitInvoicePayable" modelAttribute="addInvoicePayable" enctype="multipart/form-data">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
            <tr>
              <td height="31" colspan="6" align="left" valign="top"><table style="margin-left:20px;" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="transType">Transaction Type<span>*</span></form:label></td>
                  <td align="left" valign="middle"><form:select path="transType" class="inptformSelect" style="width:243px;" >
                              <form:option value="NONE">--select--</form:option>
                              <form:option value="1">Local</form:option>
                              <form:option value="2">International</form:option>
                              </form:select></td>
                </tr>
                 <tr>
                  <td height="31" align="left" valign="middle"><form:label path="vendorName">Vendor Name<span>*</span></form:label></td>
                  <td align="left" valign="middle"><form:select path="vendorName" class="inptformSelect" style="width:243px;" onchange="getCurrency(this.value);" >
                              <form:option value="NONE">--select--</form:option>
                              <form:options items="${vendorList}" itemLabel="name" itemValue="id"/>
                              </form:select> 
                              </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="invoiceNO">Invoice No.<span>*</span></form:label></td>
                  <td align="left" valign="middle"><form:input path="invoiceNO" class="inptform" onchange="checkInvoiceNoDuplicay(this.value)" />
                  <span id="invoice_validation"></span></td>
                </tr>
               
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="currency">Currency<span>*</span></form:label></td>
                  <td align="left" valign="middle"><form:input path="currency" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="serviceCate">Service Category<span>*</span></form:label></td>
                  <td align="left" valign="middle"><form:select path="serviceCate" class="inptformSelect" style="width:243px;" onchange="isShowProjectList(this.value);">
                              <form:option value="NONE">--select--</form:option>
                              <form:option value="1">Sample</form:option>
                              <form:option value="2">Advertising</form:option>
                              <form:option value="3">Technology</form:option>
                              <form:option value="4">Certification</form:option>
                              <form:option value="5">Accounting</form:option>
                              <form:option value="6">Programming</form:option>
                              <form:option value="7">Panel Recruitment</form:option>
                              <form:option value="8">Incentive</form:option>
                              <form:option value="9">OSBT</form:option>
                              </form:select> </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="irbproject">IRB Project No.<span>*</span></form:label> </td>
                  <td align="left" valign="middle"><form:select path="irbproject" class="inptformSelect" style="width:243px;" disabled="true" id="projectSel">
                              <form:option value="NONE">--select--</form:option>
                              <form:options items="${projectList }" itemLabel="name" itemValue="id"/>	
                              </form:select> </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="invoiceDate">Date of Invoice <span>*</span></form:label></td>
                  <td align="left" valign="middle"><input class="format-y-m-d divider-dash inptform" type="text" readonly="readonly" name="invoiceDate" 
              			id="invoiceDate" onchange="calculateDueOn()"/>  </td>
                </tr>
                 <tr>
                  <td width="158" height="31" align="left" valign="middle"><form:label path="Payment">Payment Term<span>*</span></form:label> </td>
                  <td width="557" align="left" valign="middle"><form:input path="Payment" class="inptform" style="width:190px;" onchange="calculateDueOn();" /><input type="text" id="days" value="days" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/> </td>
                </tr>
                <tr>
			        <td height="31" width="158" align="left" valign="middle"><form:label path="dueOn">Due On<span>*</span></form:label></td>
			        <td width="557" align="left" valign="middle"><input class="inptform" type="text" readonly="readonly" name="dueOn" 
              			id="dueOn"/>  </td>
		        </tr>
               
              </table></td>
            </tr>
            <tr>
              <td width="0" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td height="31" align="left" valign="top">
                  	  <table width="466" border="0" cellspacing="0" cellpadding="0">
                        
                        <tr>
                          <td align="left" valign="top"><table style="margin-left:10px; " width="1051" border="0" align="left" cellpadding="0" cellspacing="0">
                            
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="amount">Amount<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="currency1" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true" />
                              <form:input path="amount" id="amount" class="inptform" style="width:190px;" onchange="calculateTotalAmt();currencyFormat(this.value,this);"/></td>
                              <td width="576" colspan="2" align="left" valign="middle"><input type="text" id="usd1" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true" value="USD"/>
                              <form:input path="eqUsdAmount" class="inptform" style="width:190px;" onchange="currencyFormat(this.value,this)"/></td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="serviceTax">Service Tax<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="currency2" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/>
                              <form:input path="serviceTax" class="inptform" style="width:190px;" onchange="calculateTotalAmt();currencyFormat(this.value,this);"/>                              </td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="usd2" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true" value="USD"/>
                              <form:input path="eqUsdServTax" class="inptform" style="width:190px;" onchange="currencyFormat(this.value,this)"/></td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="totalAmount">Total Amount<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="currency3" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/>                             
                              <form:input path="totalAmount" class="inptform" style="width:190px;" />                              </td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="usd3" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true" value="USD"/>
                              <form:input path="eqUsdTotalAmt" class="inptform" style="width:190px;" onchange="currencyFormat(this.value,this)"/>                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="advancePayment">Advance Payment<span>*</span></form:label></td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="currency4"  class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/>                             
                              <form:input path="advancePayment" class="inptform" style="width:190px;" onchange="calculateBalanceAmt();currencyFormat(this.value,this)"/>                              </td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="usd4" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true" value="USD"/>
                              <form:input path="eqUsdAdvPay" class="inptform" style="width:190px;" onchange="currencyFormat(this.value,this)"/>                              </td>
                            </tr>
                            <tr>
                              <td width="158" height="31" align="left" valign="middle"><form:label path="balAmount">Balance Amount<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="currency5" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/>                              
                              <form:input path="balAmount" class="inptform" style="width:190px;" onchange="currencyFormat(this.value,this)" />                              </td>
                              <td colspan="2" align="left" valign="middle"><input type="text" id="usd5" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true" value="USD"/>
                              <form:input path="eqUsdBalAmt" class="inptform" style="width:190px;" onchange="currencyFormat(this.value,this)"/>                              </td>
                            </tr>
                            
                            <tr>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                            
                          </table></td>
                        </tr>
                      </table>                  	</td>
                  </tr>                
                <tr>
                  <td colspan="10" align="left" valign="top">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table style="margin-left:20px;" width="479" border="0" align="left" cellpadding="0" cellspacing="0">
            <tr>
              <td width="33%" height="31" align="left" valign="middle"><form:label path="invoiceFile">Upload Invoice<span>*</span></form:label></td>
              <td width="66%" align="left" valign="middle"><form:input type="file" path="invoiceFile" onchange="extensionValidation(this.value)"/></td>
              <td  align="left" valign="middle"><span id="invoiceUpload" style="font-size:16px;"></span></td>
            </tr>
            
          </table></td>
        </tr>
        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return invoicePayableValidation()"></td>
              <td width="118" align="center" valign="middle"><input type="reset" name="Submit3" id="Submit3" value="Clear" class="btnsubmit"></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
<!-- javascript starts here -->
<script type="text/javascript">
function isShowProjectList(servCateId)
{
	if(servCateId=='1')
		{
			document.getElementById('projectSel').disabled=false;
		}
	else
		{
			document.getElementById('projectSel').disabled=true;
		}
}
function getCurrency(vendorId){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		var a=request.open("GET","getCurrency?vendorId="+vendorId+"&action=currencyValue",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			//document.getElementById('msg_category').innerHTML = request.responseText;
			var text=request.responseText.trim("");
			document.getElementById('currency').value = text;
			for(var i=1;i<=5;i++)
			{
				document.getElementById('currency'+i).value = text;
			}
			if(text=='USD')
			{
				for(var i=1;i<=5;i++)
				{
				document.getElementById('usd'+i).style.display = "none";
				}
				document.getElementById('eqUsdAmount').style.display = "none";
				document.getElementById('eqUsdServTax').style.display = "none";
				document.getElementById('eqUsdTotalAmt').style.display = "none";
				document.getElementById('eqUsdAdvPay').style.display = "none";
				document.getElementById('eqUsdBalAmt').style.display = "none";	
			}
			else
			{
				for(var i=1;i<=5;i++)
				{
				document.getElementById('usd'+i).style.display = "block";
				}
				document.getElementById('eqUsdAmount').style.display = "block";
				document.getElementById('eqUsdServTax').style.display = "block";
				document.getElementById('eqUsdTotalAmt').style.display = "block";
				document.getElementById('eqUsdAdvPay').style.display = "block";
				document.getElementById('eqUsdBalAmt').style.display = "block";
			}
			
			request.abort();
			request = null;
}

function extensionValidation(fileName){
    var extIndex=fileName.lastIndexOf('\\');
    fileName = fileName.substring(extIndex+1);
    
    extIndex=fileName.lastIndexOf(".");
    var extension=fileName.substring(extIndex+1);
   // id = id.substring(8); //to get the number after the id
    
    if(extension.length>4){
          document.getElementById('invoiceUpload').innerHTML = "File extension cannot be longer than 4 characters";
          document.getElementById('invoiceFile').value="";
          return;
    }
    if(extension!="xls" && extension!="xlsx" && extension!="doc" && extension!="docx" && extension!="pdf"){
          document.getElementById('invoiceUpload').innerHTML = "You can only upload a pdf/doc/xls files";         
          document.getElementById('invoiceFile').value="";
          return;
    }
    document.getElementById('invoiceUpload').innerHTML = "";
}
function invoicePayableValidation()
{
	//var status=true;
	//alert(document.getElementById("eqUsdBalAmt").value);
	if(document.getElementById('currency').value=='USD')
	{
		document.getElementById('eqUsdAmount').value = document.getElementById("amount").value;
		document.getElementById('eqUsdServTax').value =document.getElementById("serviceTax").value ;
		document.getElementById('eqUsdTotalAmt').value = document.getElementById("totalAmount").value;
		document.getElementById('eqUsdAdvPay').value = document.getElementById("advancePayment").value;
		document.getElementById('eqUsdBalAmt').value = document.getElementById("balAmount").value;	
	}
	
	if(document.getElementById("transType").value=="NONE" || document.getElementById("invoiceNO").value=="" || 
	document.getElementById("vendorName").value=="NONE" || document.getElementById("currency").value==""|| 
	document.getElementById("serviceCate").value=="NONE"||
	document.getElementById("dueOn").value==""|| document.getElementById("amount").value==""||
	document.getElementById("serviceTax").value==""|| document.getElementById("totalAmount").value==""||
	document.getElementById("advancePayment").value==""|| document.getElementById("balAmount").value==""||
	document.getElementById("eqUsdAmount").value==""|| document.getElementById("eqUsdServTax").value==""||
	document.getElementById("eqUsdTotalAmt").value==""|| document.getElementById("eqUsdAdvPay").value==""||
	document.getElementById("eqUsdBalAmt").value==""|| document.getElementById("invoiceFile").value==""||
	document.getElementById("invoiceDate").value==""|| document.getElementById("Payment").value=="")
	{
		document.getElementById("validation").innerHTML="Please enter all mandatory fields";
		return false;	
	}
	if(document.getElementById("irbproject"))
	{
		if(document.getElementById("irbproject").value=="") 
			document.getElementById("validation").innerHTML="Please enter all mandatory fields";
		return false;
			
	}
	if(!((document.getElementById('amount').value).replace(/,/g,"")>=0 || (document.getElementById('eqUsdAmount').value).replace(/,/g,"")>=0 )){
		document.getElementById('validation').innerHTML="The amount can only be in numbers";
     return false;
    }
    if(!((document.getElementById('serviceTax').value).replace(/,/g,"")>=0 || (document.getElementById('eqUsdServTax').value).replace(/,/g,"")>=0)){
     document.getElementById('validation').innerHTML="The service tax can only be in numbers";
     return false;
    }
    if(!((document.getElementById('totalAmount').value).replace(/,/g,"")>=0 || (document.getElementById('eqUsdTotalAmt').value).replace(/,/g,"")>=0)){
     document.getElementById('validation').innerHTML="The total amount can only be in numbers";
     return false;
    }
    if(!((document.getElementById('advancePayment').value).replace(/,/g,"")>=0 || (document.getElementById('eqUsdAdvPay').value).replace(/,/g,"")>=0)){
     document.getElementById('validation').innerHTML="The advance payment can only be in numbers";
     return false;
    }
    if(!((document.getElementById('balAmount').value).replace(/,/g,"")>=0 || (document.getElementById('eqUsdBalAmt').value).replace(/,/g,"")>=0)){
     document.getElementById('validation').innerHTML="The balance amount can only be in numbers";
     return false;
    }
    return true;
}
function calculateDueOn()
{
	var invaoiceDate,noOfDays,total='';
	if(document.getElementById("invoiceDate").value=='')
	{
		invaoiceDate=0000-00-00;
	}
	else
	{
		invaoiceDate=document.getElementById("invoiceDate").value;
	}
	if(document.getElementById("Payment").value=='')
	{
		noOfDays=0;
	}
	else
	{
		noOfDays=document.getElementById("Payment").value;
	}

		//var invaoiceDate=document.getElementById("invoiceDate").value;
		
		document.getElementById("dueOn").value=AddDaysInDate(invaoiceDate,noOfDays);
	
}
function calculateTotalAmt()
{
	var amt,tax,total='';
	if(document.getElementById("amount").value=='')
	{
		amt=0.00;
	}
	else
	{
		amt=parseFloat((document.getElementById("amount").value).replace(/,/g,""));
	}
	if(document.getElementById("serviceTax").value=='')
	{
		tax=0.00;
	}
	else
	{
		tax=parseFloat((document.getElementById("serviceTax").value).replace(/,/g,""));
	}
	total=((parseFloat(amt)+parseFloat(tax)).toFixed(2)); 
	currencyFormat(total,document.getElementById("totalAmount"));
	document.getElementById("balAmount").onchange=calculateBalanceAmt();
	currencyFormat(total,document.getElementById("balAmount"));
	document.getElementById("balAmount").readOnly=true;
	document.getElementById("totalAmount").readOnly=true;
	//document.getElementById("totalAmount").value=total;
	
}
function calculateBalanceAmt()
{
	var amt,tax,total='';
	if(document.getElementById("totalAmount").value=='')
	{
		amt=0.00;
	}
	else
	{
		amt=parseFloat((document.getElementById("totalAmount").value).replace(/,/g,""));
	}
	if(document.getElementById("advancePayment").value=='')
	{
		tax=0.00;
	}
	else
	{
		tax=parseFloat((document.getElementById("advancePayment").value).replace(/,/g,""));
	}
	total=((parseFloat(amt)+parseFloat(tax)).toFixed(2)); 
	currencyFormat(total,document.getElementById("balAmount"));
	document.getElementById("balAmount").readOnly=true;
	//document.getElementById("totalAmount").value=total;
	
}
function checkInvoiceNoDuplicay(invoiceNo)
{
	vendorId=document.getElementById("vendorName").value;
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange2; 
		request.open("GET","findInvoiceNoList?invoiceNo="+invoiceNo+"&vendorId="+vendorId+"&action=getInvoiceNo",true);
		request.send(true);	
 	}
}
function handleStateChange2()
{
	if (request.readyState != 4) return;
	if (request.status != 200) return;
	var requestres = request.responseText;
	document.getElementById("invoice_validation").innerHTML=requestres;
	var list = requestres.replace(/^\s+|\s+$/g, "");
	if(list.length>0)
		{
			document.getElementById("invoiceNO").value="";
		}
	
	 
	 
}
</script>

<!-- javascript ends here -->
</body>
</html>
