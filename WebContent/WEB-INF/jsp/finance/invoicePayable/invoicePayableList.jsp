<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Invoice Payable List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!-- date picker starts -->
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.date.js"></script>
		
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.date.js"></script>
        <script type="text/javascript" src="resources/datepicker/scripts/datepicker.js"></script>
        <link rel="stylesheet" href="resources/datepicker/css/datepicker.css" type="text/css" media="screen" /> 
<!--  date picker ends -->
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<style type="text/css">
.searchbar label {
padding-left: 12px;
}
.btnsearch{
margin-left: 10px;}
</style>
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddInvoicePayable">
      <h3><a href="sAdminAddInvoicePayableForm"></a></h3>
      </div>
      <div class="searchbar">
      
      	<form:form method="POST" modelAttribute="searchInvoicePayable" action="sAdminInvoicePayableList">
      		<form:label path="invoiceStatus"></form:label>
      	</form:form>
      	<table width="1346" border="0" cellpadding="0" cellspacing="0">
        <form:form method="POST" modelAttribute="searchInvoicePayable" action="sAdminInvoicePayableList">
          <tr>
            <td width="115"><h2 style="font-size:15px; width: 112px;">Search Invoice</h2></td>
            <td width="115">Invoice Status</td>
            <td width="158"><form:select path="invoiceStatus"  class="selectdeco" style="width:140px;margin-top:4px;">
      		<form:option value="NONE">-- select --</form:option>
              <form:option value="1">Pending</form:option>
              <form:option value="2">Paid</form:option>
              <form:option value="3">Rejected</form:option>
              </form:select></td>
            <td width="95">Invoice from</td>
            <td width="147"><input class="format-y-m-d divider-dash " type="text" readonly="readonly" name="dateFrom" id="dateFrom" style="width:95px;margin-top:4px;"/></td>
            <td width="25">To</td>
            <td width="175"><input class="format-y-m-d divider-dash  " type="text" readonly="readonly" name="dateTo" id="dateTo" style="width:95px;margin-top:4px;" /></td>
            <td width="516"><input type="submit" name="button" id="button" value="Submit" class="btnsearch"/> </td>
          </tr>
          </form:form>
        </table>
      </div>
      </div>
      
      <div class="searchresult2">
      <h2>Invoice Payable Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1127" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="113" align="left" valign="middle">Invoice Number</td>
                    		<td width="142" align="left" valign="middle">Vendor Code</td>
                    		<td width="142" align="left" valign="middle">Transaction Type</td>
                    		<td width="167" align="left" valign="middle">Amount</td>
		                    <td width="189" align="left" valign="middle">Received On</td>
		                    <td width="150" align="left" valign="middle">Due On</td>
		                    <td width="112" align="left" valign="middle">Overdue By</td>
                  		    <td width="112" align="left" valign="middle">Payment Status</td>
                  		  </tr>
              			</table>
              		  </td>
            		</tr>
            	<c:if test="${not empty getInvoiceList}">
      			  <c:set var="pageUrl" value="getInvoiceList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">            		
            	<%--start --%>
            	<pg:param name="invoiceStatus" value="${param.invoiceStatus}"/>
            	<pg:param name="" value="${param}"/>
            	<pg:param name="" value="${param}"/>
		       	<c:forEach items="${getInvoiceList}" var="proVar" varStatus="pStatus">
		       	
					<pg:item>       	
				      <c:if test="${pStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="sAdminEditInvoicePayableForm?invoicePayId=${proVar.invoicePayId}">
                			<div class="blacktxt">
                  			  <table width="1127" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${proVar.invoiceNO}</td>
                      			  <td width="142" align="left" valign="middle">${proVar.vendorName}</td>
			                      <td width="142" align="left" valign="middle">${proVar.transType}</td>
			                      <td width="167" align="left" valign="middle">${proVar.currency} ${proVar.amount}</td>
			                      <td width="189" align="left" valign="middle">${proVar.invoiceDate}</td>
			                      <td width="150" align="left" valign="middle">${proVar.dueOn}</td>
			                      <td width="112" align="left" valign="middle">${proVar.overdue}</td>
                    			  <td width="112" align="left" valign="middle">${proVar.invoiceStatus}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${pStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="sAdminEditInvoicePayableForm?invoicePayId=${proVar.invoicePayId}">
                			<div class="blacktxt2">
                  			  <table width="1127" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${proVar.invoiceNO}</td>
                      			  <td width="142" align="left" valign="middle">${proVar.vendorName}</td>
			                      <td width="142" align="left" valign="middle">${proVar.transType}</td>
			                      <td width="167" align="left" valign="middle">${proVar.currency} ${proVar.amount}</td>
			                      <td width="189" align="left" valign="middle">${proVar.invoiceDate}</td>
			                      <td width="150" align="left" valign="middle">${proVar.dueOn}</td>
			                      <td width="112" align="left" valign="middle">${proVar.overdue}</td>
                    			  <td width="112" align="left" valign="middle">${proVar.invoiceStatus}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    