<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit Bank Detail - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
</head>
<body onLoad="getRowValuesForBanking();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit Bank Detail</h2>
      <form:form method="POST" action="sAdminEditBankingDetail" modelAttribute="editBankingDetail" enctype="multipart/form-data">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
            
            <tr>
              <td width="0" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td height="31" align="left" valign="top">
                  	  <table width="466" border="0" cellspacing="0" cellpadding="0">
                        
                        <tr>
                          <td align="left" valign="top"><table style="margin-left:10px; " width="1051" border="0" align="left" cellpadding="0" cellspacing="0">
                            
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bankName">Bank Name<span>*</span></form:label>                              </td>
                              <td width="285" colspan="2" align="left" valign="middle"><form:input path="bankName" class="inptform"/>                            </td>
                               
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="benefName">Beneficiary Name<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="benefName" class="inptform" />                              </td>
                              </td>
                            </tr>
                            <tr>
                  				<td height="31" align="left" valign="middle"><form:label path="accountType">Account Type<span>*</span></form:label></td>
                  				<td align="left" valign="middle"><form:select path="accountType" class="inptformSelect" style="width:243px;" onchange="getCurrencyValue(this.value);" >
                              	<form:option value="NONE">--select--</form:option>
                              	<form:option value="1">Current</form:option>
                              	<form:option value="2">EEFC</form:option>
                              	</form:select> 
                              	</td>
                            </tr>
                			<tr>
                			<td colspan="2">
                			<div id="currencySelect" style="display: none;">
                			<table border="0" align="left" cellpadding="0" cellspacing="0">
                			 <tr>
                  				<td height="31" width="157px;" align="left" valign="middle"><form:label path="accTypeCuurency">Currency</form:label></td>
                  				<td colspan="2" align="left" valign="middle"><form:select path="accTypeCuurency" class="inptformSelect" style="width:243px;">
                              	<form:option value="0">--select--</form:option>
                              	<form:options items="${currencyList}" itemLabel="name" itemValue="id"/>
                              	</form:select> 
                             	 </td>
                			</tr>
                			</table></div>
                			</td>
                			</tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="accountNo">A/C No.<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="accountNo" class="inptform" onchange="checkAccountNoAvailability(this.value)" />       
                                <span id="account_validation"></span></td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="ifsccode">IFSC Code<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="ifsccode" class="inptform" />                              </td>
                                                        
                            </tr>
                            <tr>
                              <td width="157" height="31" align="left" valign="middle"><form:label path="routingNO">IBAN/Routing No.</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="routingNO" class="inptform"/>                              </td>
                                                      
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="swiftCode">Swift Code<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="swiftCode" class="inptform" />                              </td>
                             
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="sqrtCode">SORT Code</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="sqrtCode" class="inptform" />                              </td>
                             
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="physicalAdd">Bank Physical Address<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="physicalAdd" class="inptform" />                              </td>
                             
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2>Intermediary Bank details</h2></td>
        </tr>
        <tr>
         <td height="37" align="left" valign="middle" bgcolor="#778D4C" ><table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:0px;" width="88" align="left" valign="middle">Country</td>
              <td width="120" align="left" valign="middle">Bank Name</td>
              <td width="119" align="left" valign="middle">Beneficiary Name</td>
              <td width="120" align="left" valign="middle">A/C No</td>
              <td width="135" align="left" valign="middle">Bank Address</td>
              <td width="125" align="left" valign="middle">IBAN/Routing No</td>
              <td width="118" align="left" valign="middle">Swift Code</td>
              <td width="137" align="left" valign="middle">SORT Code</td>              
              <td width="63" align="left" valign="middle"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:0px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                <tr>
               <td width="1123" colspan="11" align="left" valign="top">            
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                   <tbody id="IntermediaryBank">
                   <c:choose>
                    <c:when test="${editBankingDetail.intermediayBankList.size()>0}">
                   <c:forEach items="${editBankingDetail.intermediayBankList}" varStatus="bnkst">
                    <tr>
                      <td width="100" height="38" align="left" valign="middle">
                      	<form:select path="intermediayBankList[${bnkst.index}].countryId" id="i_country${bnkst.index}" class="inptform2" style="width:97px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interBankName" id="i_name${bnkst.index}" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interBenefName" id="i_bname${bnkst.index}" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interAccountNo" id="i_account${bnkst.index}" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="150" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interBankAdd" id="i_add${bnkst.index}" class="inptform2" style="width:140px;"/>
                      </td>
                      <td width="135" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interRoutingNo" id="i_rounting${bnkst.index}" class="inptform2"  style="width:125px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interSwiftCode" id="i_swift${bnkst.index}" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[${bnkst.index}].interSqrtCode" id="i_sqrt${bnkst.index}" class="inptform2"  style="width:120px;" />
                      	<form:hidden path="intermediayBankList[${bnkst.index}].interbankId" id="ids${bnkst.index}"/>
                      </td>                      
                     <td width="63" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                    </c:forEach>
                    </c:when>
                    <c:otherwise>
                    <tr>
                      <td width="100" height="38" align="left" valign="middle">
                      	<form:select path="intermediayBankList[0].countryId" id="i_country0" class="inptform2" style="width:97px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interBankName" id="i_name0" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interBenefName" id="i_bname0" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interAccountNo" id="i_account0" class="inptform2" style="width:120px;"/>
                      </td>
                      <td width="150" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interBankAdd" id="i_add0" class="inptform2" style="width:140px;"/>
                      </td>
                      
                      <td width="135" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interRoutingNo" id="i_rounting0" class="inptform2"  style="width:125px;"/>
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interSwiftCode" id="i_swift0" class="inptform2" style="width:120px;"/>
                      	
                      </td>
                      <td width="130" align="left" valign="middle">
                      	<form:input path="intermediayBankList[0].interSqrtCode" id="i_sqrt0" class="inptform2"  style="width:120px;" />
                      </td>                      
                                           
                     
                      <td width="63" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                    </c:otherwise>
                    </c:choose>
                    <form:hidden path="interBankListSize"/>
                  	<form:hidden path="interBankIdList"/>
                    </tbody>
                  </table></td>               
                  
                </tr>
                <tr>
                  <td height="38" colspan="11" align="left" valign="top" ><input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit" /></td>
                </tr>
                
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </td>
        </tr>
        	 <tr>
                        <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2>Additional Info</h2></td>
         				</tr>
                        <tr>
                        <td>
                        <table>
                       <tr>
					        <td height="31" width="158" align="left" valign="middle"><form:label path="panNo">PAN No.<span>*</span></form:label></td>
					        <td width="285" align="left" valign="middle"><form:input path="panNo" class="inptform"/></td>
				        </tr>
				        <tr>
					        <td height="31" align="left" valign="middle"><form:label path="serviceTaxNo">Service Tax. No.<span>*</span></form:label></td>
					        <td colspan="2" align="left" valign="middle"><form:input path="serviceTaxNo" class="inptform"/></td>
				        </tr>
				        <tr>
					        <td height="31" align="left" valign="middle"><form:label path="tan">TAN</form:label></td>
					        <td colspan="2" align="left" valign="middle"><form:input path="tan" class="inptform"/></td>
				        </tr>
				        <tr>
					        <td height="31" align="left" valign="middle"><form:label path="vatNo">VAT No.</form:label></td>
					        <td colspan="2" align="left" valign="middle"><form:input path="vatNo" class="inptform"/></td>
				        </tr>
				        </table>
				        </td>
				        </tr>
                        </table>
         					 
                      </table>
                  	</td>
                  </tr>                
                <tr>
                  <td colspan="10" align="left" valign="top">&nbsp;</td>
                </tr>
       <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="checkRowUpdate();return checkBankingValidation();"></td>
              <td width="118" align="center" valign="middle"><a href="bankingDetailList" class="btnsubmitLink" >Cancel</a></td></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
<!-- javascript starts here -->

<script type="text/javascript">
var accountNumber;
var btn_close='<td width="57" align="left" valign="middle" id="btn-close" class="btn-remove">' + 
	'<a href="javascript:void(0)"><img src="images/btncross.png" width="18" height="17" border="0" />' + 
	'</a></td>';
var interBankListSize='${editBankingDetail.intermediayBankList.size()}';
var i=interBankListSize;
if(i==0)
	{
	i=1; 
	}
var row=1;

$(document).ready(function(){
	
	accountNumber=document.getElementById("accountNo").value;
	getCurrencyValue(accountType.value);
	//Add more functionality for additional contacts
		$("#addMore").click(function() {	
			 var tr_temp=$("#IntermediaryBank tr:first").clone();
			 tr_temp.find("#i_country0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_country' + i},
	    				'name': function(_, name) { return 'intermediayBankList['+ i +'].countryId'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#i_name0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_name' + i},
	    				'name': function(_, name) { return 'intermediayBankList['+ i +'].interBankName'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#i_bname0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_bname' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].interBenefName'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#i_account0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_account' + i},
	    				'name': function(_, name) { return 'intermediayBankList[' + i + '].interAccountNo'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#i_add0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_add' + i},
	    				'name': function(_, name) { return 'intermediayBankList[' + i + '].interBankAdd	'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#i_rounting0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_rounting' + i},
	    				'name': function(_, name) { return 'intermediayBankList[' + i + '].interRoutingNo'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#i_swift0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_swift' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].interSwiftCode'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#i_sqrt0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'i_sqrt' + i},
	    				'name': function(_, name) { return 'intermediayBankList[' + i + '].interSqrtCode'},
	    				'value': ''
	  			});
				}).end();
			tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close);
			 tr_temp.appendTo("#IntermediaryBank");
			 i++;			 			
		});
			
		$("#btn-close").live('click',function(){
			$(this).parent().remove();
			row--;
		});
			
			
});
var values=new Array();
function getRowValuesForBanking()
{
	for(var j=0;j<interBankListSize;j++)
	{
		values[j]=new Array();
		values[j][0]=document.getElementById('i_country'+j).value;
		values[j][1]=document.getElementById('i_name'+j).value;
		values[j][2]=document.getElementById('i_bname'+j).value;
		values[j][3]=document.getElementById('i_account'+j).value;
		values[j][4]=document.getElementById('i_add'+j).value;
		values[j][5]=document.getElementById('i_rounting'+j).value;
		values[j][6]=document.getElementById('i_swift'+j).value;
		values[j][7]=document.getElementById('i_sqrt'+j).value;
		values[j][8]=document.getElementById('i_padd'+j).value;
	}
	document.getElementById("interBankListSize").value=interBankListSize;
}
var interBankIdList;
function checkRowUpdate(){
	interBankIdList="";  
	for(var k=0;k<reqSize;k++)
      {
            if(values[k][0]!=document.getElementById('i_country'+k).value || values[k][1]!=document.getElementById('i_name'+k).value || 
				values[k][2]!=document.getElementById('i_bname'+k).value || values[k][3]!=document.getElementById('i_account'+k).value || 
				values[k][4]!=document.getElementById('i_add'+k).value  || values[k][5]!=document.getElementById('i_rounting'+k).value ||
				values[k][6]!=document.getElementById('i_swift'+k).value  || values[k][7]!=document.getElementById('i_sqrt'+k).value ||
				values[k][8]!=document.getElementById('i_padd'+k).value);  
            {  
            	interBankIdList+=k+",";
            }         
      }
	  document.getElementById("interBankIdList").value = interBankIdList;
}
function checkBankingValidation()
{
	//var status=true;
	var bankList=new Array();
	if(document.getElementById("bankName").value=="" || document.getElementById("benefName").value==""|| 
	document.getElementById("physicalAdd").value=="" || document.getElementById("accountNo").value==""||
	document.getElementById("accountType").value=="" || document.getElementById("swiftCode").value==""||
	document.getElementById("ifsccode").value==""||document.getElementById("serviceTaxNo").value==""||
	document.getElementById("panNo").value=="")
	{
		document.getElementById("validation").innerHTML="Please enter all mandatory fields";
		return false;	
	}
	for(var j=0; j<i; j++){  
		  if(document.getElementById('i_country' + j)){
		    if(document.getElementById('i_country' + j).value!=""){
			//alert('languge-->'+document.getElementById('language' + j).value);
		    	bankList[j]=document.getElementById('i_country' + j).value;
		    }
		  }
	}
	for(var k=0;k<bankList.length;k++)
	{
		for(var l=k+1;l<bankList.length;l++)
		{
			
			document.getElementById("validation").innerHTML="Please don't select a country more than once.";
			return false;
		}
	}
	return true;
}
function getCurrencyValue(accountType)
{
	if(accountType=="2")
		{
			document.getElementById("currencySelect").style.display='block';
			
		}
	else
		{
		document.getElementById("currencySelect").style.display='none';
		}
}

function checkAccountNoAvailability(accNo)
{
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange2; 
		request.open("GET","findAccountNoList?acuntNo="+accNo+"&action=getAccountNo",true);
		request.send(true);	
 	}
}
function handleStateChange2()
{
	if (request.readyState != 4) return;
	if (request.status != 200) return;
	var requestres = request.responseText;
	//alert(request.responseText);
	document.getElementById("account_validation").innerHTML=requestres;
	var list = requestres.replace(/^\s+|\s+$/g, "");
	if(list!=0)
		{
		
		
		document.getElementById("accountNo").value=accountNumber;
		}
	
	 
	 
}

</script>

<!-- javascript ends here -->
</body>
</html>
