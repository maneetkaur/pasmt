<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Bank List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddBankingDetail">
      <h3><a href="sAdminAddBankingDetailForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:15px;">Search Bank</h2>
      	<form:form method="POST" modelAttribute="searchBank" action="sAdminBankingDetailList">
      		<form:label path="bankName">Bank Name</form:label>
      		<form:input path="bankName" />
			<form:label path="accountNo">Account No</form:label>
			<form:input path="accountNo" />
			
       		<input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>       		
      	</form:form>
      
      </div>
      </div>
      <div class="searchresult2">
      <h2>Bank Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1100" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c"><table  style="margin-left:10px;"width="1125" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="271" align="left" valign="middle">Bank Name</td>
                    		<td width="261" align="left" valign="middle">Acount Type</td>
                    		<td width="290" align="left" valign="middle">Account No</td>
                    		<td width="303" align="left" valign="middle">Added On</td>
	                    </tr>
              			</table>
              		  </td>
            		</tr>            		
            	<c:if test="${not empty bankList}">
      			  <c:set var="pageUrl" value="bankList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}"
      	  			 isOffset="true">            		
            	<%--start --%>
            	<pg:param name="bankName" value="${param.bankName}"/>
		        <pg:param name="accountNo" value="${param.accountNo}"/>
		        <c:forEach items="${bankList}" var="bnkVar" varStatus="bStatus">
		       	
				<pg:item>       	
				      <c:if test="${bStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="sAdminEditBankForm?bankId=${bnkVar.bankId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			   <td width="280" align="left" valign="middle">${bnkVar.bankName}</td>
	                    			<td width="263" align="left" valign="middle">${bnkVar.accountType}</td>
	                    			<td width="290" align="left" valign="middle">${bnkVar.accountNo}</td>
                    			  <td width="254" align="left" valign="middle">${bnkVar.createdOn}</td>                    			
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${bStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="sAdminEditBankForm?bankId=${bnkVar.bankId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			 <td width="279" align="left" valign="middle">${bnkVar.bankName}</td>
                    			 <td width="264" align="left" valign="middle">${bnkVar.accountType}</td>
                    			 <td width="292" align="left" valign="middle">${bnkVar.accountNo}</td>
                    			<td width="252" align="left" valign="middle">${bnkVar.createdOn}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    