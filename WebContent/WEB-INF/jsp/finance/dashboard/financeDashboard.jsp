<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "Finance";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Finance Dashboard - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- month picker starts -->
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery-1.8.2-ui.css">
<!-- month picker ends -->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<!-- ui-datepicker-calendar for month picker -->
<style>
.calendar-off table.ui-datepicker-calendar {display:none !important;}
.dLabelClass{display: block;}
</style>
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchboxcontainer">
      </div>
      <div class="searchresult2">
      <h3 class="uppercase">Balance Sheet</h3>
        <section class="fin-bid fin-dash">
          <form:form method="POST" modelAttribute="balanceSheet" action="sAdminFinanceDashboard">
            <div>
            <p>
            <form:label path="bsMonth">Month:</form:label>            
			<input type="text" id="bsMonth" name="bsMonth" class="monthyearpicker" value="${balanceSheet.bsMonth}" >
             </p>
             </div>
             
             <div>
            <p> <label for="bsStartDate">Start Date</label>
            <input name="bsStartDate" id="bsStartDate" class="date-picker" value="${balanceSheet.bsStartDate}"/>
            </p>
            </div>
            <div>
            <p> <label for="bsEndDate">End Date</label>
            <input name="bsEndDate" id="bsEndDate" class="date-picker" value="${balanceSheet.bsEndDate}"/>
            </p>
            </div>
            <div>
            <p> <label for="" class="dLabelClass">&nbsp;</label>
            <input class="search-fin" type="submit" value="Search"/>
            </p>
            </div>
          </form:form>
        </section>
        
        <table width="94%" border="0" cellspacing="0" cellpadding="0" class="table-striped fin-table-dash">
          <thead>
            <tr>
              <th width="25%">Income</th>
              <th width="25%">Amount</th>
              <th width="25%">Expense</th>
              <th width="25%">Amount</th>
            </tr>
          </thead>
          <tr>
          	<%-- Checking for income list starts --%>
          	 <c:set var="iLNotEmpty" value="0"/>
          	 <c:if test="${not empty bsIncomeList && bsIncomeList.size()>0 && bsIncomeList.size()>=2}">
          	 	<c:set var="iLNotEmpty" value="1"/>        	 	
          	 </c:if>
          	 <%-- Checking for income list ends --%>
          	 
          	 <%-- Check for expense list starts --%>
          	 <c:set var="eLSize" value="0"/>
           	 <c:if test="${not empty bsExpenseList && bsExpenseList.size()>0}">
           	  <c:choose>
           	  	<c:when test="${bsExpenseList.size()==1}">
           	  	  <c:set var="eLSize" value="1"/>
           	  	</c:when>
           	  	<c:otherwise>
           	  	  <c:set var="eLSize" value="2"/>
           	  	</c:otherwise>
           	  </c:choose>
           	 </c:if>
           	 <%-- Check for expense list starts --%>
            <td>Projects</td>
            <td class="solid_border">            	
              <c:if test="${iLNotEmpty==1}">
              	$ ${bsIncomeList[0].name}
              </c:if>            	
            </td>
            <c:choose>
              <c:when test="${eLSize==2}">
              	<td>${bsExpenseList[0].id}</td>
            	<td>$ ${bsExpenseList[0].name}</td>
              </c:when>
              <c:otherwise>
              	<td>Expenses</td>
            	<td>$ 0.00</td>
              </c:otherwise>
            </c:choose>           
          </tr>
          <c:if test="${eLSize==2}">          
          <c:forEach items="${bsExpenseList}" begin="1" end="${bsExpenseList.size()-2}" var="bsExpense" 
          	varStatus="stEx">    		
   		  	<tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>${bsExpense.id}</td>
           	  <td>$ ${bsExpense.name}</td>
           	</tr>          	
          </c:forEach>                   
          </c:if>
          <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
            <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
            <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
            <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
          <tr>
            <td class="make-">Total</td>
            <td class="make-">$ ${bsIncomeList[bsIncomeList.size()-2].name}</td>
            <td class="make-"></td>
            <td class="make-">$ ${bsExpenseList[bsExpenseList.size()-1].name}</td>
          </tr>
          <tr class="white_height"></tr>
          <tr>
            <td colspan="3" class="three-col-gap make-">Gross Profits</td>
            <td class="three-col-nxt make-">$ ${bsIncomeList[bsIncomeList.size()-1].name}</td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
       
       <h3 class="uppercase">Cash Flow</h3>
        <section class="fin-bid fin-dash">
          <form:form method="POST" modelAttribute="cashflow" action="sAdminFinanceDashboard">
            <div>
            <p>
            <form:label path="cfMonth">Month:</form:label>            
			<input type="text" id="cfMonth" name="cfMonth" class="monthyearpicker" value="${cashflow.cfMonth}" >
             </p>
             </div>
             
             <div>
            <p> <label for="cfStartDate">Start Date</label>
            <input name="cfStartDate" id="cfStartDate" class="date-picker" value="${cashflow.cfStartDate}"/>
            </p>
            </div>
            <div>
            <p> <label for="cfEndDate">End Date</label>
            <input name="cfEndDate" id="cfEndDate" class="date-picker" value="${balanceSheet.cfEndDate}"/>
            </p>
            </div>
            <div>
            <p> <label for="paymentStatus">Payment Status</label>
              <form:select path="paymentStatus">
            	<form:option value="4" label="All"/>
            	<form:option value="1" label="Pending"/>
            	<form:option value="2" label="Paid"/>
            	<form:option value="3" label="Declined/Rejected"/>
              </form:select>
            </p>
            </div>
            <div>
            <p> <label for="" class="dLabelClass">&nbsp;</label>
            <input class="search-fin" type="submit" value="Search"/>
            </p>
            </div>
          </form:form>                     
        </section>
       
        <table width="94%" border="0" cellspacing="0" cellpadding="0" class="table-striped fin-table-dash">
          <thead>
            <tr>
              <th width="25%">Income</th>
              <th width="25%">Amount</th>
              <th width="25%">Expense</th>
              <th width="25%">Amount</th>
            </tr>
          </thead>          
          	<%-- Checking for income list starts --%>
          	 <c:set var="cfILNotEmpty" value="0"/>
          	 <c:if test="${not empty cfIncomeList && cfIncomeList.size()>0 && cfIncomeList.size()>2}">
          	 	<c:set var="cfILNotEmpty" value="1"/>        	 	
          	 </c:if>
          	 <%-- Checking for income list ends --%>
          	 
          	 <%-- Check for expense list starts --%>
          	 <c:set var="cfELSize" value="0"/>
           	 <c:if test="${not empty cfExpenseList && cfExpenseList.size()>0}">
           	  <c:choose>
           	  	<c:when test="${cfExpenseList.size()==1}">
           	  	  <c:set var="cfELSize" value="1"/>
           	  	</c:when>
           	  	<c:otherwise>
           	  	  <c:set var="cfELSize" value="2"/>
           	  	</c:otherwise>
           	  </c:choose>
           	 </c:if>
           	 <%-- Check for expense list starts --%>
           	<c:choose>
           	  <c:when test="${cfILNotEmpty==1}">
           	  	<c:forEach items="${cfIncomeList}" end="${cfIncomeList.size()-3}" var="cfIncome" 
          			varStatus="stIn">
		          	<tr>
		            <td>${cfIncome.id} </td>
		            <td class="solid_border">            	              
		              	$ ${cfIncome.name}                          	
		            </td>
		            <c:choose>
		              <c:when test="${cfELSize==2}">
		              	<td>${cfExpenseList[stIn.index].id}</td>
		            	<td>$ ${cfExpenseList[stIn.index].name}</td>
		              </c:when>
		              <c:otherwise>
		              	<c:choose>
			              	<c:when test="${stIn.index==0}">
			              	  <td>Expenses</td>
		            		  <td>$ 0.00</td>
			              	</c:when>
			              	<c:otherwise>
			              	  <td>&nbsp;</td>
		            		  <td>&nbsp;</td>
			              	</c:otherwise>
		              	</c:choose>
		              	
		              </c:otherwise>
		            </c:choose>           
		          </tr>
	          	</c:forEach>
           	  </c:when>           	  
           	</c:choose>           	
          <c:if test="${cfELSize==2}">          
          <c:forEach items="${cfExpenseList}" begin="${cfIncomeList.size()-2}" end="${cfExpenseList.size()-2}"
           	var="cfExpense" varStatus="cfEx">    		
   		  	<tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>${cfExpense.id}</td>
           	  <td>$ ${cfExpense.name}</td>
           	</tr>          	
          </c:forEach>                   
          </c:if>
          <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           	</tr>
          <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
            <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
            <tr>
   		  	  <td>&nbsp;</td>
           	  <td class="solid_border">&nbsp;</td>
           	  <td>&nbsp;</td>
           	  <td>&nbsp;</td>
           </tr>
          <tr>
            <td class="make-">Total</td>
            <td class="make-">$ ${cfIncomeList[cfIncomeList.size()-2].name}</td>
            <td class="make-">&nbsp;</td>
            <td class="make-">$ ${cfExpenseList[cfExpenseList.size()-1].name}</td>
          </tr>
          <tr class="white_height"></tr>
          <tr>
            <td colspan="3" class="three-col-gap  make-">Gross Profits</td>
            <td class="three-col-nxt make-">$ ${cfIncomeList[cfIncomeList.size()-1].name}</td>
          </tr>
        </table>
        <p>&nbsp;</p>
      
      </div>
    </div>
  </div>
</div>
 <!-- javascript starts here -->
  <script src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript">
  //for month picker
	$(function() {
	    $('#bsMonth').datepicker( {
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'MM-yy',
    beforeShow: function(input, inst) {
      $(inst.dpDiv).addClass('calendar-off');
      if((selDate = $(this).val()).length>0){
  		year = selDate.substring(selDate.length - 4, selDate.length);
          month = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
                   $(this).datepicker('option', 'monthNames'));
          $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
          $(this).datepicker('setDate', new Date(year, month, 1));
  		}
	  },
	    onClose: function(dateText, inst) { 
	      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	      $(this).datepicker('setDate', new Date(year, month, 1));
	    }
	});
	    
	    $( "#bsStartDate" ).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        beforeShow: function(input, inst) {
	          $(inst.dpDiv).removeClass('calendar-off');     
	        }
	      });
	    
	    $( "#bsEndDate" ).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        beforeShow: function(input, inst) {
	          $(inst.dpDiv).removeClass('calendar-off');     
	        }
	      });
	    
	    //Cashflow
	    $('#cfMonth').datepicker( {
	    changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
	    dateFormat: 'MM-yy',
	    beforeShow: function(input, inst) {
	      $(inst.dpDiv).addClass('calendar-off');
	      if((selDate = $(this).val()).length>0){
	  		year = selDate.substring(selDate.length - 4, selDate.length);
	          month = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
	                   $(this).datepicker('option', 'monthNames'));
	          $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
	          $(this).datepicker('setDate', new Date(year, month, 1));
	  		}
		  },
		    onClose: function(dateText, inst) { 
		      var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		      var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		      $(this).datepicker('setDate', new Date(year, month, 1));
		    }
		});
	    
	    $( "#cfStartDate" ).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        beforeShow: function(input, inst) {
	          $(inst.dpDiv).removeClass('calendar-off');     
	        }
	      });
	    
	    $( "#cfEndDate" ).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        beforeShow: function(input, inst) {
	          $(inst.dpDiv).removeClass('calendar-off');     
	        }
	      });
	});
  
  </script>
  <!-- javascript ends here -->
</body>
</html>
