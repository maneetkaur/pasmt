<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Projects";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit Project Summary - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<!-- month picker starts -->
<link rel="stylesheet" type="text/css" media="screen" href="css/jquery-1.8.2-ui.css">
<!-- month picker ends -->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<style>
.ui-datepicker-calendar {
    display: none;
 }
</style>

</head>
<body onLoad="editInitialize();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit Project Summary</h2>
      <form:form method="POST" action="editProjectSummary" modelAttribute="editProjectSummary">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
           <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td width="1100" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">              	              	
                <tr>
                  <td width="200" height="31" align="left" valign="middle">
                  	<form:hidden path="projectSummaryId"/>
                  	<form:label path="projectId">Project Number<span>*</span></form:label>
                  </td>
                  <td width="887" align="left" valign="middle">
                  	<form:input path="projectId" disabled="true" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<label for="clientName">Client Name</label>
                  </td>
                  <td align="left" valign="middle">                	
                  	<input type="text" id="clientName" class="inptform" disabled="disabled">                
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<label for="projectName">Project Name</label>
                  </td>
                  <td align="left" valign="middle">                  	
                  	<input type="text" id="projectName" class="inptform" disabled="disabled">                
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<label for="projectType">Project Type</label>
                  </td>
                  <td align="left" valign="middle">                  	
                  	<input type="text" id="projectType" class="inptform" disabled="disabled">                
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<label for="accountManager">Account Manager</label>
                  </td>
                  <td align="left" valign="middle">                  	
                  	<input type="text" id="accountManager" class="inptform" disabled="disabled">                
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="closingMonth">Closing Month<span>*</span></form:label>
                  </td>
                  <td align="left" valign="middle">                 	
                  	<input name="closingMonth" id="closingMonth" class="date-picker inptform"
                  	 	value="${editProjectSummary.closingMonth}"/>               
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="waveMonth">Wave/Month<span>*</span></form:label>
                  </td>
                  <td align="left" valign="middle">                  	 
                  	<form:input path="waveMonth" class="inptform" maxlength="5"/>              
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<label for="projectMinimum">Project Minimum</label>
                  </td>
                  <td align="left" valign="middle">
                  	<input type="text" id="currPM" style="width:40px;" class="inptform" disabled="disabled"/>                	
                  	<input type="text" id="projectMinimum" class="inptform" disabled="disabled" style="width: 180px;">                
                </tr>            
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="projectValue">Project Value</form:label>
                  </td>
                  <td align="left" valign="middle">
                  	<input type="text" id="currPV" style="width:40px;" class="inptform" disabled="disabled"/>
                  	<form:input path="projectValue" class="inptform" disabled="true" style="width:180px;"/> 
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="projectExpense">Project Expense</form:label>
                  </td>
                  <td align="left" valign="middle">
                  	<input type="text" id="currPE" style="width:40px;" class="inptform" disabled="disabled"/>
                  	<form:input path="projectExpense" class="inptform" disabled="true" style="width:180px;"/> 
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="margin">Margin %</form:label>
                  </td>
                  <td align="left" valign="middle">                  	
                  	<form:input path="margin" class="inptform" disabled="true"/> 
                  </td>
                </tr>                
                <tr>
                  <td height="31" align="left" valign="middle">&nbsp;</td>
                  <td align="left" valign="middle">&nbsp;</td>
                </tr>
                
              </table></td>
            </tr>
            
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Income Details</h2></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" >
           <table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:2px;" width="91" align="left" valign="middle">SurveyID</td>
              <td width="95" align="left" valign="middle">Country</td>
              <td width="91" align="left" valign="middle">Services</td>
              <td width="102" align="left" valign="middle">Audience Type</td>
              <td width="143" align="left" valign="middle">Description</td>
              <td width="90" align="left" valign="middle">Currency</td>
              <td width="85" align="left" valign="middle">Quantity</td>
              <td width="92" align="left" valign="middle">Unit Price</td>              
              <td width="89" align="left" valign="middle">Setup Fee</td>
              <td width="159" align="left" valign="middle">Amount</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:10px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                <tr>
                <!-- income list starts here -->
                <c:choose>
                  <c:when test="${editProjectSummary.incomeList.size()>0}">
                  	<td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                   <tbody id="income">
                   <c:forEach items="${editProjectSummary.incomeList}" var="inc" varStatus="st">
                    <tr>                                                         
                      <td width="99" height="38" align="left" valign="middle">
                      	<form:hidden path="incomeList[${st.index}].requirementId" id="r_id${st.index}"/>
                        <form:input path="incomeList[${st.index}].surveyId" id="r_survey${st.index}" class="inptform2"
                         size="10"/>                      
                      </td>
                      <td width="103" align="left" valign="middle">
                      	<form:select path="incomeList[${st.index}].country" id="r_country${st.index}" class="inptform2"
                      	 style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<form:select path="incomeList[${st.index}].services" id="r_service${st.index}" 
                      		class="inptform2" style="width:80px;" onchange="populateValues(this.value, this.id);">
                      		<form:option value="0" label="--Select--"/>                      		
                      		<form:option value="1" label="Data Processing"/>
                      		<form:option value="2" label="Sample"/>
                      		<form:option value="3" label="Survey Programming"/>                    
                      		<form:option value="4" label="Translation"/>                      		
                      	</form:select>
                      </td>
                      <td width="110" align="left" valign="middle">
                      	<form:select path="incomeList[${st.index}].targetAudience" id="r_audience${st.index}" 
                      		class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="B2B"/>
                      		<form:option value="2" label="B2C"/>
                      		<form:option value="3" label="HC"/>
                      	</form:select>
                      </td>
                      <td width="154" align="left" valign="middle">
                      	<form:input path="incomeList[${st.index}].description" id="r_desc${st.index}" class="inptform2"
                      		style="width:120px;"/>
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<input id="currencyVal${st.index}" type="text" class="inptform2" readonly/>                     	
                      </td>
                      <td width="91" align="left" valign="middle">
                      	<form:input path="incomeList[${st.index}].sampleSize" id="r_sample${st.index}" 
                      		class="inptform2" size="10" onchange="addQuantity();rowCostFun(this.id,1)"/>
                      </td>
                      <td width="99" align="left" valign="middle">
                      	<input id="currUnit${st.index}" type="text" class="inptform2" style="width:25px;
                      	 font-size:11px;" readonly/>
                      	<form:input path="incomeList[${st.index}].quotedCpi" class="inptform2" id="r_cpi${st.index}"
                      	 size="10" style="width:43px;" onchange="rowCostFun(this.id,2);"/>
                      </td>
                      <td width="96" align="left" valign="middle">
                        <input id="currSetup${st.index}" type="text" class="inptform2" style="width:25px;
                         font-size:11px;" readonly/>
                      	<form:input path="incomeList[${st.index}].setupFee" class="inptform2" id="r_setup${st.index}"
                      	 	size="10" style="width:43px;" onchange="addSetup();rowCostFun(this.id,3);"/>
                      </td>
                      <td width="102" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="r_total${st.index}" size="10" 
                      		style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="71" align="left" valign="middle" style="display: block;"></td>                     
                    </tr>
                    </c:forEach>
                   </tbody>
                  </table></td>
                  </c:when>
                  <c:otherwise>
                  	<td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                   <tbody id="income">
                    <tr>
                      <td width="99" height="38" align="left" valign="middle">
                        <form:input path="incomeList[0].surveyId" id="r_survey0" class="inptform2" size="10"/>                      
                      </td>
                      <td width="103" align="left" valign="middle">
                      	<form:select path="incomeList[0].country" id="r_country0" class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<form:select path="incomeList[0].services" id="r_service0" class="inptform2" 
                      		style="width:80px;" onchange="populateValues(this.value, this.id);">
                      		<form:option value="0" label="--Select--"/>                      		
                      		<form:option value="1" label="Data Processing"/>
                      		<form:option value="2" label="Sample"/>
                      		<form:option value="3" label="Survey Programming"/>                    
                      		<form:option value="4" label="Translation"/>                      		
                      	</form:select>
                      </td>
                      <td width="110" align="left" valign="middle">
                      	<form:select path="incomeList[0].targetAudience" id="r_audience0" class="inptform2" 
                      		style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="B2B"/>
                      		<form:option value="2" label="B2C"/>
                      		<form:option value="3" label="HC"/>
                      	</form:select>
                      </td>
                      <td width="154" align="left" valign="middle">
                      	<form:input path="incomeList[0].description" id="r_desc0" class="inptform2"
                      		style="width:120px;"/>
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<input id="currencyVal0" type="text" class="inptform2" readonly/>                     	
                      </td>
                      <td width="91" align="left" valign="middle">
                      	<form:input path="incomeList[0].sampleSize" id="r_sample0" class="inptform2" size="10"
                      		onchange="addQuantity();rowCostFun(this.id,1)"/>
                      </td>
                      <td width="99" align="left" valign="middle">
                      	<input id="currUnit0" type="text" class="inptform2" style="width:25px; font-size:11px;" 
                      		readonly/>
                      	<form:input path="incomeList[0].quotedCpi" class="inptform2" id="r_cpi0" size="10" 
                      		style="width:43px;" onchange="rowCostFun(this.id,2);"/>
                      </td>
                      <td width="96" align="left" valign="middle">
                        <input id="currSetup0" type="text" class="inptform2" style="width:25px; font-size:11px;"
                        	 readonly/>
                      	<form:input path="incomeList[0].setupFee" class="inptform2" id="r_setup0" size="10" 
                      		style="width:43px;" onchange="addSetup();rowCostFun(this.id,3);"/>
                      </td>
                      <td width="102" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="r_total0" size="10" 
                      		style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="71" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                   </tbody>
                  </table></td>
                  </c:otherwise>
                </c:choose>
                <!-- income list ends here -->
                <td>
                	<form:hidden path="incomeSize"/>
                	<form:hidden path="incomeIdList"/>
                </td>
                </tr>
                                
                <tr>
                  <td height="38" colspan="11" align="left" valign="top" ><input type="button" name="Submit"
                   	id="addMore" value="Add More +" class="btnsubmit" /></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">
                    <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                      <tr class="amboseff">
                        <td width="429" height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;"><strong>Grand Total</strong></td>
                        <td width="90" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">                        	
                        </td>
                        <td width="143" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="190" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield10" type="text" class="inptformdis" id="quantitySum" size="10"
                           	readonly/>
                        </td>
                        <td width="96" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield11" type="text" class="inptformdis" id="setupSum" size="10" 
                          	readonly />
                        </td>
                        <td width="126" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield14" type="text" class="inptformdis" id="totalSum" size="10" readonly />
                        </td>
                        <td width="47" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>                                               
              </table></td>
            </tr>
          </table></td>
        </tr>
        
        <%-- Deduction Details start - Added By Maneet 19 May'14 --%>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Deduction Details</h2></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" >
           <table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:2px;" width="91" align="left" valign="middle">SurveyID</td>
              <td width="95" align="left" valign="middle">Country</td>
              <td width="91" align="left" valign="middle">Services</td>
              <td width="143" align="left" valign="middle">Description</td>
              <td width="90" align="left" valign="middle">Reason</td>
              <td width="85" align="left" valign="middle">Quantity</td>
              <td width="92" align="left" valign="middle">Unit Price</td>              
              <td width="159" align="left" valign="middle">Amount</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:10px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                <tr>
                <!-- deduction list starts here -->
                <c:choose>
                  <c:when test="${editProjectSummary.deductionList.size()>0}">
                  	<td width="1123" colspan="11" align="left" valign="top">
	                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
	                   <tbody id="deduction">
	                   <c:forEach items="${editProjectSummary.deductionList}" var="ded" varStatus="st">	                   
	                    <tr>
	                      <td width="122" height="38" align="left" valign="middle">
	                      	<form:hidden path="deductionList[${st.index}].requirementId" id="d_id${st.index}"/>
	                        <form:input path="deductionList[${st.index}].surveyId" id="d_survey${st.index}"
	                         class="inptform2"/>                      </td>
	                      <td width="127" align="left" valign="middle">
	                      	<form:select path="deductionList[${st.index}].country" id="d_country${st.index}" 
	                      	class="inptform2" style="width:80px;">
	                      		<form:option value="-1" label="--Select--"/>
	                      		<form:option value="0" label="All"/>
	                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
	                      	</form:select>                      </td>
	                      <td width="119" align="left" valign="middle">
	                      	<form:select path="deductionList[${st.index}].services" id="d_service${st.index}"
	                      	 class="inptform2" style="width:80px;">
	                      		<form:option value="0" label="--Select--"/>                      		
	                      		<form:option value="1" label="Data Processing"/>
	                      		<form:option value="2" label="Sample"/>
	                      		<form:option value="3" label="Survey Programming"/>                    
	                      		<form:option value="4" label="Translation"/>                      		
	                      	</form:select>                      </td>
	                      <td width="183" align="left" valign="middle">
	                      	<form:input path="deductionList[${st.index}].description" id="d_desc${st.index}" class="inptform2"
	                      		style="width:120px;"/>                      </td>
	                      <td width="126" align="left" valign="middle">
	                      	<form:select path="deductionList[${st.index}].reason" id="d_reason${st.index}" class="inptform2" 
	                      		style="width:80px;" onchange="populateValues(this.value, this.id);">
	                      		<form:option value="0" label="--Select--"/>                      		
	                      		<form:option value="1" label="Adjustment"/>
	                      		<form:option value="2" label="Discount"/>
	                      		<form:option value="3" label="Rejections"/>                     		
	                      	</form:select>
	                      </td>
	                      <td width="112" align="left" valign="middle">
	                      	<form:input path="deductionList[${st.index}].sampleSize" id="d_sample${st.index}" class="inptform2" size="10"
	                      		onchange="addDedQuantity();dRowCostFun(this.id,1)"/>                      </td>
	                      <td width="121" align="left" valign="middle">
	                      	<input id="dCurrUnit${st.index}" type="text" class="inptform2" style="width:25px; font-size:11px;" 
	                      		readonly/>
	                      	<form:input path="deductionList[${st.index}].quotedCpi" class="inptform2" id="d_cpi${st.index}" size="10" 
	                      		style="width:43px;" onchange="dRowCostFun(this.id,2);"/>                      </td>
	                      <td width="124" align="left" valign="middle">
	                      	<input name="textfield10" type="text" class="inptformdis" id="d_total${st.index}" size="10" 
	                      		style="font-weight: bold; height: 30px;" readonly />                      </td>
	                      <td width="87" align="left" valign="middle" style="display: block;"></td>
	                    </tr>
	                    </c:forEach>
	                   </tbody>
	                  </table></td>
                  </c:when>
                  <c:otherwise>
                  	<td width="1123" colspan="11" align="left" valign="top">
	                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
	                   <tbody id="deduction">
	                    <tr>
	                      <td width="122" height="38" align="left" valign="middle">
	                        <form:input path="deductionList[0].surveyId" id="d_survey0" class="inptform2"/>
	                      </td>
	                      <td width="127" align="left" valign="middle">
	                      	<form:select path="deductionList[0].country" id="d_country0" class="inptform2" style="width:80px;">
	                      		<form:option value="-1" label="--Select--"/>
	                      		<form:option value="0" label="All"/>
	                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
	                      	</form:select>                      </td>
	                      <td width="119" align="left" valign="middle">
	                      	<form:select path="deductionList[0].services" id="d_service0" class="inptform2" 
	                      		style="width:80px;">
	                      		<form:option value="0" label="--Select--"/>                      		
	                      		<form:option value="1" label="Data Processing"/>
	                      		<form:option value="2" label="Sample"/>
	                      		<form:option value="3" label="Survey Programming"/>                    
	                      		<form:option value="4" label="Translation"/>                      		
	                      	</form:select>                      </td>
	                      <td width="183" align="left" valign="middle">
	                      	<form:input path="deductionList[0].description" id="d_desc0" class="inptform2"
	                      		style="width:120px;"/>                      </td>
	                      <td width="126" align="left" valign="middle">
	                      	<form:select path="deductionList[0].reason" id="d_reason0" class="inptform2" 
	                      		style="width:80px;" onchange="populateValues(this.value, this.id);">
	                      		<form:option value="0" label="--Select--"/>                      		
	                      		<form:option value="1" label="Adjustment"/>
	                      		<form:option value="2" label="Discount"/>
	                      		<form:option value="3" label="Rejections"/>                     		
	                      	</form:select>
	                      </td>
	                      <td width="112" align="left" valign="middle">
	                      	<form:input path="deductionList[0].sampleSize" id="d_sample0" class="inptform2" size="10"
	                      		onchange="addDedQuantity();dRowCostFun(this.id,1)"/>                      </td>
	                      <td width="121" align="left" valign="middle">
	                      	<input id="dCurrUnit0" type="text" class="inptform2" style="width:25px; font-size:11px;" 
	                      		readonly/>
	                      	<form:input path="deductionList[0].quotedCpi" class="inptform2" id="d_cpi0" size="10" 
	                      		style="width:43px;" onchange="dRowCostFun(this.id,2);"/>                      </td>
	                      <td width="124" align="left" valign="middle">
	                      	<input name="textfield10" type="text" class="inptformdis" id="d_total0" size="10" 
	                      		style="font-weight: bold; height: 30px;" readonly />                      </td>
	                      <td width="87" align="left" valign="middle" style="display: block;"></td>
	                    </tr>
	                   </tbody>
	                  </table></td>
                  </c:otherwise>
                </c:choose>
                <!-- deduction list ends here -->
                <td>
                	<form:hidden path="deductionSize"/>
                	<form:hidden path="deductionIdList"/>
                </td>
                </tr>
                                
                <tr>
                  <td height="38" colspan="11" align="left" valign="top" ><input type="button" name="Submit"
                   	id="addMoreDeduction" value="Add More +" class="btnsubmit" /></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">
                    <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                      <tr class="amboseff">
                        <td width="429" height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;"><strong>Grand Total</strong></td>
                        <td width="90" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">                        	
                        </td>
                        <td width="158" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="113" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield10" type="text" class="inptformdis" id="dedQuantitySum" size="10"
                           	readonly/>
                        </td>
                        <td width="120" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                        <td width="164" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield14" type="text" class="inptformdis" id="dedTotalSum" size="10" readonly />
                        </td>
                        <td width="47" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>                                               
              </table></td>
            </tr>
          </table></td>
        </tr>
        
        <%-- Deduction Details end --%>
        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Expense Detail</h2></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" >
           <table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:2px;" width="100" align="left" valign="middle">Survey Id</td>
              <td width="96" align="left" valign="middle">Country</td>
              <td width="96" align="left" valign="middle">Expense On</td>
              <td width="93" align="left" valign="middle">Expense Type</td>
              <td width="94" align="left" valign="middle">Vendor Name</td>
              <td width="98" align="left" valign="middle">Currency</td>
              <td width="98" align="left" valign="middle">Quantity</td>
              <td width="95" align="left" valign="middle">Unit Price</td>
              <td width="91" align="left" valign="middle">Proj Min</td>
              <td width="103" align="left" valign="middle">Setup Fee</td>              
              <td width="159" align="left" valign="middle">Amount</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:10px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                <tr>
                <!-- expense details start here -->
                <c:choose>
                  <c:when test="${editProjectSummary.expenseList.size()>0}">
                  	<td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                   <tbody id="expense">
                   <c:forEach items="${editProjectSummary.expenseList}" varStatus="st">
                   	 <tr>
                   	 <td width="100" height="38" align="left" valign="middle">                        
                        <form:input path="expenseList[${st.index}].surveyId" id="e_survey${st.index}"
                         class="inptform2"/>  
                      </td>
                      <td width="96" height="38" align="left" valign="middle">                        
                        <form:select path="expenseList[${st.index}].country" id="e_country${st.index}"
                         class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="96" height="38" align="left" valign="middle">
                      	<form:hidden path="expenseList[${st.index}].expenseId" id="e_id${st.index}"/>                       
                        <form:select path="expenseList[${st.index}].expenseOn" id="e_expenseOn${st.index}"
                         class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="Panel"/>
                      		<form:option value="2" label="OSBT"/>
                      		<form:option value="3" label="Vendor"/>
                      		<form:option value="4" label="Survey Programming"/>
                      		<form:option value="5" label="Data Processing"/>
                      		<form:option value="6" label="Translation"/>
                      	</form:select>
                      </td>
                      <td width="93" align="left" valign="middle">
                      	<form:select path="expenseList[${st.index}].expenseType" id="e_expenseType${st.index}" 
                      		class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="Sample Buying"/>
                      		<form:option value="2" label="Affiliate"/>
                      		<form:option value="3" label="Panelist Incentive"/>                      		
                      	</form:select>
                      </td>
                      <td width="94" align="left" valign="middle">
                      	<form:select path="expenseList[${st.index}].vendorName" id="e_vendor${st.index}" 
                      		class="inptform2" style="width:80px;">
                      		<form:option value="-1" label="--Select--"/>
                      		<form:option value="0" label="NA"/>
                      		<form:options items="${vendorList}" itemLabel="name" itemValue="id"/>                      		                      		
                      	</form:select>
                      </td>
                      <td width="98" align="left" valign="middle">                      	
                      	<input id="e_currency${st.index}" type="text" class="inptform2" readonly/>
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<form:input path="expenseList[${st.index}].quantity" id="e_quantity${st.index}" 
                      		class="inptform2" onchange="addEQuantity();eRowCostFun(this.id,1);"/>
                      </td>
                      <td width="95" align="left" valign="middle">
                      	<input id="currEUnit${st.index}" type="text" class="inptform2" style="width:25px; 
                      		font-size:11px;" readonly/>
                      	<form:input path="expenseList[${st.index}].unitPrice" class="inptform2" 
                      		id="e_unitPrice${st.index}" size="10" 
                      		style="width:43px;" onchange="eRowCostFun(this.id,2);"/>                    	
                      </td>
                      <td width="91" align="left" valign="middle">
                        <input id="currEPM${st.index}" type="text" class="inptform2" style="width:25px; 
                        	font-size:11px;" readonly/>
                      	<form:input path="expenseList[${st.index}].projectMinimum" id="e_projMin${st.index}" 
                      		class="inptform2" size="10" style="width:43px;" onchange="eRowCostFun(this.id,4);"/>
                      </td>
                      <td width="103" align="left" valign="middle">
                      	<input id="currESetup${st.index}" type="text" class="inptform2" style="width:25px; 
                      		font-size:11px;" readonly/>
                      	<form:input path="expenseList[${st.index}].setupFee" id="e_setup${st.index}" 
                      		class="inptform2" size="10" style="width:43px;" 
                      		onchange="addESetup();eRowCostFun(this.id,3);"/>                      	                      
                      </td>
                      <td width="88" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="e_total${st.index}" size="10" 
                      		style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="71" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                   </c:forEach>                    
                   </tbody>
                  </table></td>
                  </c:when>
                  <c:otherwise>
                  	<td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                   <tbody id="expense">
                    <tr>
                      <td width="100" height="38" align="left" valign="middle">                        
                        <form:input path="expenseList[0].surveyId" id="e_survey0" class="inptform2"/>  
                      </td>
                      <td width="96" height="38" align="left" valign="middle">                        
                        <form:select path="expenseList[0].country" id="e_country0" class="inptform2"
                        	 style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="96" height="38" align="left" valign="middle">                        
                        <form:select path="expenseList[0].expenseOn" id="e_expenseOn0" class="inptform2"
                      	 	style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="Panel"/>
                      		<form:option value="2" label="OSBT"/>
                      		<form:option value="3" label="Vendor"/>
                      	</form:select>
                      </td>
                      <td width="93" align="left" valign="middle">
                      	<form:select path="expenseList[0].expenseType" id="e_expenseType0" class="inptform2"
                      	 	style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="Sample Buying"/>
                      		<form:option value="2" label="Affiliate"/>
                      		<form:option value="3" label="Panelist Incentive"/>
                      		<form:option value="4" label="Others"/>                    		
                      	</form:select>
                      </td>
                      <td width="94" align="left" valign="middle">
                      	<form:select path="expenseList[0].vendorName" id="e_vendor0" class="inptform2" 
                      		style="width:80px;">
                      		<form:option value="-1" label="--Select--"/>
                      		<form:option value="0" label="NA"/>
                      		<form:options items="${vendorList}" itemLabel="name" itemValue="id"/>                      		                      		
                      	</form:select>
                      </td>
                      <td width="98" align="left" valign="middle">                      	
                      	<input id="e_currency0" type="text" class="inptform2" readonly/>
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<form:input path="expenseList[0].quantity" id="e_quantity0" class="inptform2"
                      		onchange="addEQuantity();eRowCostFun(this.id,1);"/>
                      </td>
                      <td width="95" align="left" valign="middle">
                      	<input id="currEUnit0" type="text" class="inptform2" style="width:25px; font-size:11px;"
                      	 	readonly/>
                      	<form:input path="expenseList[0].unitPrice" class="inptform2" id="e_unitPrice0" size="10" 
                      		style="width:43px;" onchange="eRowCostFun(this.id,2);"/>                    	
                      </td>
                      <td width="91" align="left" valign="middle">
                        <input id="currEPM0" type="text" class="inptform2" style="width:25px; font-size:11px;"
                      	 	readonly/>
                      	<form:input path="expenseList[0].projectMinimum" id="e_projMin0" class="inptform2" size="10"
                      		style="width:43px;" onchange="eRowCostFun(this.id,4);"/>
                      </td>
                      <td width="103" align="left" valign="middle">
                      	<input id="currESetup0" type="text" class="inptform2" style="width:25px; font-size:11px;"
                      	 	readonly/>
                      	<form:input path="expenseList[0].setupFee" id="e_setup0" class="inptform2" size="10"
                      		style="width:43px;" onchange="addESetup();eRowCostFun(this.id,3);"/>                      	                      
                      </td>
                      <td width="88" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="e_total0" size="10" 
                      		style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="71" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                   </tbody>
                  </table></td>
                  </c:otherwise>
                </c:choose>
                <!-- expense details end here -->
                <td>
                	<form:hidden path="expenseSize"/>
                	<form:hidden path="expenseIdList"/>
                </td>                  
                </tr>                                
                <tr>
                  <td height="38" colspan="11" align="left" valign="top" >
                  	<input type="button" name="Submit" id="addMoreExpense" value="Add More +" class="btnsubmit" /></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">
                  	<table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                      <tr class="amboseff">
                        <td width="314" height="38" align="left" valign="middle" bgcolor="#bbcb9b" 
                        	style="padding-left:5px;"><strong>Grand Total</strong></td>
                        <td width="61" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="201" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="285" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input name="eQuantitySum" type="text" class="inptformdis" id="eQuantitySum" size="10"
                           	readonly /></td>
                        <td width="104" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield11" type="text" class="inptformdis" id="eSetupSum" size="10" 
                          	readonly />                        </td>
                        <td width="109" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                          <input name="textfield14" type="text" class="inptformdis" id="eTotalSum" size="10" readonly/>                        </td>
                        <td width="47" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>                
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>                
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
           <table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit"
              	 onClick="checkRowUpdate();return projectSummaryValidation(1);">
              </td>
              <td width="118" align="center" valign="middle">
              	<a href="projectSummaryList" class="btnsubmitLink" >Cancel</a>
              </td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
  <!-- javascript starts here -->
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/common/commonScript.js"></script>
  <script type="text/javascript" src="js/finance/projectSummaryScript.js"></script>
  <script type="text/javascript">
var btn_close='<td width="71" align="left" valign="middle" id="btn-close" class="btn-remove">' + 
	'<a href="javascript:void(0)"><img src="images/btncross.png" width="18" height="17" border="0" />' + 
	'</a></td>';
var btn_close2='<td width="71" align="left" valign="middle" id="btn-close2" class="btn-remove">' + 
	'<a href="javascript:void(0)"><img src="images/btncross.png" width="18" height="17" border="0" ></a></td>';
//Added for deduction details
var btn_close_deduction='<td width="71" align="left" valign="middle" id="btn-close-ded" class="btn-remove">' + 
	'<a href="javascript:void(0)"><img src="images/btncross.png" width="18" height="17" border="0" />' + 
	'</a></td>';
var ded_i=1;
var deductionRow = '${editProjectSummary.deductionList.size()}';
//Changes end
var i=1;
var exp_i=1;
var doNotDel=false;
var count=0;
var incomeRow = '${editProjectSummary.incomeList.size()}';
var expenseRow = '${editProjectSummary.expenseList.size()}';

var projectInfo = '${requestScope.projectInfo}';

$(document).ready(function(){
	
	/*Add more functionality for income detail*/
		$("#addMore").click(function() {	
			 var tr_temp=$("#income tr:first").clone();
			 tr_temp.find("#r_survey0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_survey' + i},
	    				'name': function(_, name) { return 'incomeList['+ i +'].surveyId'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_country0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_country' + i},
	    				'name': function(_, name) { return 'incomeList['+ i +'].country'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_service0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_service' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].services'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_audience0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_audience' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].targetAudience'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_desc0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_desc' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].description'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_sample0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_sample' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].sampleSize'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_cpi0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_cpi' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].quotedCpi'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_setup0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_setup' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].setupFee'},
	    				'value': ''
	  			});
				}).end();			 			 
			 tr_temp.find("#r_total0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_total' + i},	    			
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#currencyVal0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currencyVal' + i}
	  			});
				}).end();
			 tr_temp.find("#currUnit0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currUnit' + i}
	  			});
				}).end();
			 tr_temp.find("#currSetup0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currSetup' + i}
	  			});
				}).end();
			 tr_temp.find("#r_id0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_id' + i},
	    				'name': function(_, name) { return 'incomeList[' + i + '].requirementId'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close);
			 tr_temp.appendTo("#income");
			 i++;			 			
		});
			
		/*Add more functionality for expense details*/
		$("#addMoreExpense").click(function() {	
			 var tr_temp=$("#expense tr:first").clone();
			 tr_temp.find("#e_survey0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_survey' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].surveyId'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#e_country0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_country' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].country'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#e_expenseOn0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_expenseOn' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].expenseOn'},
	    				'value': ''
	  			});
				}).end();
			tr_temp.find("#e_expenseType0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_expenseType' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].expenseType'},
	    				'value': ''
	  			});
				}).end();
			tr_temp.find("#e_vendor0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_vendor' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].vendorName'},
	    				'value': ''
	  			});
				}).end();			
			tr_temp.find("#e_currency0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_currency' + exp_i}
	  			});
			}).end();
			tr_temp.find("#e_quantity0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_quantity' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].quantity'},
	    				'value': ''
	  			});
				}).end();
			tr_temp.find("#e_unitPrice0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_unitPrice' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].unitPrice'},
	    				'value': ''
	  			});
				}).end();
			tr_temp.find("#e_projMin0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_projMin' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].projectMinimum'},
	    				'value': ''
	  			});
				}).end();
			tr_temp.find("#e_setup0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_setup' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].setupFee'},
	    				'value': ''
	  			});
				}).end();	 			 			 
			 tr_temp.find("#e_total0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_total' + exp_i},	    			
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#currESetup0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currESetup' + exp_i}
	  			});
				}).end();
			tr_temp.find("#currEPM0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currEPM' + exp_i}
	  			});
				}).end();
			tr_temp.find("#currEUnit0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currEUnit' + exp_i}
	  			});
				}).end();
			tr_temp.find("#e_id0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'e_id' + exp_i},
	    				'name': function(_, name) { return 'expenseList['+ exp_i +'].expenseId'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close2);
			 tr_temp.appendTo("#expense");
			 exp_i++;			 			
		});
		
		//close button for income
		$("#btn-close").live('click',function(){
			doNotDel=false;
			count=0;
			
			//To fetch the values of the Select Fields
			$(this).parent().find("select").each(function(){
				var value = $(this).val();
				var id=$(this).attr('id');
				if(id.indexOf("r_incidence")==-1){
					if(value!="0"){
						doNotDel=true;
						return false;		//breaks the .each loop
					}
				}
				else{
					if(value!="-1"){
						doNotDel=true;
						return false;		//breaks the .each loop
					}
				}
			});
			
			//To fetch the values of the Input Fields
			$(this).parent().find("input").each(function(){
				count++;
				var value = $(this).val();
				var id=$(this).attr('id');
				//check if count is 9, then the total column is being accessed
				if(id.indexOf("r_total")==-1){
					//Skip the minutes and currency column
					if(id.indexOf("minutes")==-1 && id.indexOf("currencyVal")==-1 && id.indexOf("currUnit")==-1 && 
						id.indexOf("currSetup")==-1){
						if(value!=""){
							doNotDel=true;
							return false;		//breaks the .each loop
						}						
					}
				}
			});

			if(doNotDel==false){
				$(this).parent().remove();	
			}				
		});
		
		//close button for expense
		$("#btn-close2").live('click',function(){
			doNotDel=false;
			
			//To fetch the values of the Select Fields
			$(this).parent().find("select").each(function(){
				var value = $(this).val();
				var id=$(this).attr('id');
				if(id.indexOf("e_vendor")==-1){
					if(value!="0"){
						doNotDel=true;
						return false;		//breaks the .each loop
					}
				}
				else{
					if(value!="-1"){
						doNotDel=true;
						return false;		//breaks the .each loop
					}
				}
			});
			
			//To fetch the values of the Input Fields
			$(this).parent().find("input").each(function(){
				count++;
				var value = $(this).val();
				var id=$(this).attr('id');				
				if(id.indexOf("e_total")==-1){
					//Skip the currency columns
					if(id.indexOf("currESetup")==-1 && id.indexOf("currEPM")==-1 && id.indexOf("currEUnit")==-1
						&& id.indexOf("e_currency")==-1){
						if(value!=""){
							doNotDel=true;
							return false;		//breaks the .each loop
						}						
					}
				}
			});

			if(doNotDel==false){
				$(this).parent().remove();	
			}				
		});
		
		/*	Changes for deduction start
		Add more functionality for deduction details
		Added by Maneet - 19 May'14*/
	$("#addMoreDeduction").click(function() {	
		 var tr_temp=$("#deduction tr:first").clone();
		 tr_temp.find("#d_survey0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_survey' + ded_i},
    				'name': function(_, name) { return 'deductionList['+ ded_i +'].surveyId'},
    				'value': ''
  			});
			}).end();
		 tr_temp.find("#d_country0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_country' + ded_i},
    				'name': function(_, name) { return 'deductionList['+ ded_i +'].country'},
    				'value': ''
  			});
			}).end();			 
		 tr_temp.find("#d_service0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_service' + ded_i},
    				'name': function(_, name) { return 'deductionList[' + ded_i + '].services'},
    				'value': ''
  			});
			}).end();			 			 
		 tr_temp.find("#d_desc0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_desc' + ded_i},
    				'name': function(_, name) { return 'deductionList[' + ded_i + '].description'},
    				'value': ''
  			});
			}).end();
		 tr_temp.find("#d_reason0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_reason' + ded_i},
    				'name': function(_, name) { return 'deductionList[' + ded_i + '].reason'},
    				'value': ''
  			});
			}).end();
		 tr_temp.find("#d_sample0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_sample' + ded_i},
    				'name': function(_, name) { return 'deductionList[' + ded_i + '].sampleSize'},
    				'value': ''
  			});
			}).end();			 
		 tr_temp.find("#d_cpi0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_cpi' + ded_i},
    				'name': function(_, name) { return 'deductionList[' + ded_i + '].quotedCpi'},
    				'value': ''
  			});
			}).end();						 			 
		 tr_temp.find("#d_total0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_total' + ded_i},	    			
    				'value': ''
  			});
			}).end();			 
		 tr_temp.find("#dCurrUnit0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'dCurrUnit' + ded_i}
  			});
			}).end();
		 tr_temp.find("#d_id0").each(function() {
			 $(this).attr({
    				'id': function(_, id) { return 'd_id' + ded_i},
    				'name': function(_, name) { return 'deductionList['+ ded_i +'].requirementId'},
    				'value': ''
  			});
			}).end();
		 tr_temp.find("td:last").remove();
		 tr_temp.append(btn_close_deduction);
		 tr_temp.appendTo("#deduction");
		 ded_i++;			 			
	});
	
	//close button for deduction
	$("#btn-close-ded").live('click',function(){
		doNotDel=false;
		count=0;
		
		//To fetch the values of the Select Fields
		$(this).parent().find("select").each(function(){
			var value = $(this).val();
			var id=$(this).attr('id');
			if(id.indexOf("d_country")==-1){
				if(value!="0"){
					doNotDel=true;
					return false;		//breaks the .each loop
				}
			}
			else{
				if(value!="-1"){
					doNotDel=true;
					return false;		//breaks the .each loop
				}
			}
		});
		
		//To fetch the values of the Input Fields
		$(this).parent().find("input").each(function(){
			var value = $(this).val();
			var id=$(this).attr('id');
			if(id.indexOf("d_total")==-1){
				//Skip the minutes and currency column
				if(id.indexOf("dCurrUnit")==-1){
					if(value!=""){
						doNotDel=true;
						return false;		//breaks the .each loop
					}						
				}
			}
		});

		if(doNotDel==false){
			$(this).parent().remove();	
		}				
	});
	/*	Changes for deduction end */
		
});
//for month picker
$(function() {
    $('.date-picker').datepicker( {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM-yy',
        onClose: function(dateText, inst) { 
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        beforeShow: function(){
        	if((selDate = $(this).val()).length>0){
        		year = selDate.substring(selDate.length - 4, selDate.length);
                month = jQuery.inArray(selDate.substring(0, selDate.length - 5), 
                         $(this).datepicker('option', 'monthNames'));
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
        	}
        }
    });
});
</script>
  <!-- javascript ends here -->
</body>
</html>
