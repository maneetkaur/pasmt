<%@page import="com.admin.cultureQuestion.CultureQuestion"%>
<%@page import="com.admin.globalQuestion.GlobalQuestionBean"%>
<%@page import="com.admin.cultureQuestion.CultureQuestionBean"%>
<%@page import="com.commonFunc.CommonBean"%>
<%@page import="com.admin.globalQuestion.GlobalQuestion"%>
<%@page import="com.admin.questionType.QuestionTypeBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.admin.cultureQuestion.*"%> 

<%
  	String action=request.getParameter("action");
      if(action.equals("validateLang") )
      {
      	//System.out.println("chkCliAl->");
      	if(request.getAttribute("checkLang")!=null)
      	{
      		if(((String)request.getAttribute("checkLang")).equals("AlreadyExist"))
      		{
  %>
        	<span style="color:#ee8285;font-weight:bold;" >'<%=request.getParameter("lang")%>'  Language already exists</span>
		<%
			}		
			}
			else
			{
				out.print("Communication failed");
			}
		}
				
      else if(action.equals("CheckLangCode") )
		{
			if(request.getAttribute("checkCode")!=null)
			{
				if(((String)request.getAttribute("checkCode")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;"> '<%=request.getParameter("langCode")%>'  Language code already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}
		else if(action.equals("validateCountry"))
		{
			if(request.getAttribute("checkCountry")!=null)
			{
				if(((String)request.getAttribute("checkCountry")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;"> Country name already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}
		else if(action.equals("ValidateCountryCode"))
		{
			if(request.getAttribute("checkCountryCode")!=null)
			{
				if(((String)request.getAttribute("checkCountryCode")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;"> Country code already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}
		else if(action.equals("validateRegion"))
		{
			if(request.getAttribute("checkRegion")!=null)
			{
				if(((String)request.getAttribute("checkRegion")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;"> Region for this country already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}
		else if(action.equals("validateCompany"))
		{
			if(request.getAttribute("checkCompany")!=null)
			{
				if(((String)request.getAttribute("checkCompany")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;"> '<%=request.getParameter("Company")%>'  Company already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}
		else if(action.equals("validateWebsite"))
		{
			if(request.getAttribute("checkWebsite")!=null)
			{
				if(((String)request.getAttribute("checkWebsite")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;">'<%=request.getParameter("Website")%>'  Website url already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}
		else if(action.equals("validateType"))
		{
			if(request.getAttribute("checkQuestionType")!=null)
			{
				if(((String)request.getAttribute("checkQuestionType")).equals("AlreadyExist"))
				{
					%>
					<span style="color:#ee8285;font-weight:bold;">'<%=request.getParameter("QuestionType")%>'  Question type already exists</span>
					<%
				}
			}
			else
			{
				out.print("Communication failed");
			}
		}

		else if(action.equals("getQuestionDetail"))
		{
			String[] showDiv=((String)request.getAttribute("chkQuestion")).split(",");
			System.out.println(showDiv);
			if(request.getAttribute("chkQuestion")!=null)
			{
				if(showDiv[0].equals("grid"))
				{
					%>0<%
					if(showDiv.length > 1 && showDiv[1].equals("inputBox"))
					{
						%>3<%	
					}
				}
				else if(showDiv[0].equals("child"))
				{
					%>1<%
					if(showDiv.length > 1 && showDiv[1].equals("inputBox"))
					{
						%>5<%
					}
				}
				else if(showDiv[0].equals("both"))
				{
					%>2<%
					if( showDiv.length > 1 && showDiv[1].equals("inputBox"))
					{
						%>4<%
					}
				}
				else if(showDiv[0].equals("multipleFields"))
				{
					%>6<%
				}
				else if(showDiv[0].equals("multipleChild"))
				{
					%>7<%
				}
				else if(showDiv[0].equals("ParentChild"))
				{
					%>9<%
				}
			}
			else
			{
				out.print("Communication failed");
			}

		}
		else if(action.equals("getQuestionList"))
		{		
			if(request.getAttribute("QuestionTypeList")!=null)
			{
				%>{selectoption0:[
				{
					name:'typeId',
					id:'typeId',
					classname:'inptformSelect',
					style:'width:243px;',
					onchange:'findQuestionType(this.value)',
					option:
						[
						{value:'NONE',param:'--Question Type--'}
						<%
					List<QuestionTypeBean> question=(ArrayList)request.getAttribute("QuestionTypeList");
						for(int i=0;i<question.size();i++)
						{
					%>,{value:'<%=question.get(i).getQuestionTypeId()%>',param:'<%=question.get(i).getQuestionTypeHeading()%>'}<%
					}
					%>
					]
				}		
				]
				
			}

			<%
			}
			else
			{
				out.print("Communication failed");
			}

		}
	else if(action.equals("getAnswerList"))
	{		
		if(request.getAttribute("answerList")!=null)
		{System.out.println("in ajax");
			%>{selectoption0:[
				{
					name:'answerTypeId',
					id:'answerTypeId',
					classname:'inptformSelect',
					style:'width:243px;',
					onchange:'findQuestionType(this.value)',
					option:
					[
					
					<%
					List<GlobalQuestion> question=(ArrayList)request.getAttribute("answerList");
					System.out.println("hjfbvf--"+question);
					for(int i=0;i<question.size();i++)
					{
					%>,{value:'<%=question.get(i).getAnswerId()%>',param:'<%=question.get(i).getAnswer()%>'}<%
					}
					%>
				]
			}		
			]
			}
		<%
		}
		else
		{
			out.print("communication failed");
		}
}
	else if(action.equals("getCategory"))
	{		
		if(request.getAttribute("CategoryList")!=null)
		{
			%>{selectoption0:[
				{
					option:
					[
					{value:'NONE',param:'--Subcategory--'}
					<%
					List<CommonBean> category=(ArrayList)request.getAttribute("CategoryList");
					for(int i=0;i<category.size();i++)
					{
					%>,{value:'<%=category.get(i).getId()%>',param:'<%=category.get(i).getName()%>'}<%
					}
					%>
				]
			}		
			]
			}
		<%
		}
		else
		{
			out.print("communication failed");
		}
}
else if(action.equals("validatePanel"))
{
	if(request.getAttribute("checkPanelName")!=null)
	{
		if(((String)request.getAttribute("checkPanelName")).equals("AlreadyExist"))
		{
		%>
			<span style="color:#ee8285;font-weight:bold;"> Panel name already exists</span>
		<%
		}
	}
	else
	{
		out.print("Communication failed");
	}
}
else if(action.equals("validateCatgeory"))
{
	if(request.getAttribute("checkCategory")!=null)
	{
		if(((String)request.getAttribute("checkCategory")).equals("AlreadyExist"))
		{
		%>
			<span style="">'<%=request.getParameter("Category")%>' Category already exists</span>
		<%
		}
	}
	else
	{
		out.print("Communication failed");
	}
} 
else if(action.equals("validateRegQuestion"))
{
	if(request.getAttribute("checkRegistration")!=null)
	{
		if(((String)request.getAttribute("checkRegistration")).equals("AlreadyExist"))
		{
		%>
			<span style="">'<%=request.getParameter("question")%>' question already exist</span>
		<%
		}
	}
	else
	{
		out.print("Communication failed");
	}
}    
else if(action.equals("validateSubCatgeory"))
{
	if(request.getAttribute("checkSubCategory")!=null)
	{
		if(((String)request.getAttribute("checkSubCategory")).equals("AlreadyExist"))
		{
		%>
			<span style="">'<%=request.getParameter("subCategory")%>' Sub Category already exists</span>
		<%
		}
	}
	else
	{
		out.print("Communication failed");
	}
}    
else if(action.equals("getQuestion"))
{	
	if(request.getAttribute("globalQuestionText")!=null)
	{
		CultureQuestion questiontext=(CultureQuestion)request.getAttribute("globalQuestionText");
		%>{questiontext:[ 
				{
					option:
					[
					{value:'<%=questiontext.getGlobalQuestionText()%>',id:'<%=questiontext.getGlobalQuestionId()%>',subQuestion:'<%=questiontext.getGlobalSubQuesText()%>'}
					]
				}		
				],
			
			answer:[ 
				{
					option:
					[
					{value:'NONE',id:''}
					<%
					if(questiontext.getGlobalAnswerList().size()!=0)
					{
					
						for(int i=0;i<questiontext.getGlobalAnswerList().size();i++)
						{
						%>,{value:'<%=questiontext.getGlobalAnswerList().get(i).getName()%>',id:'<%=questiontext.getGlobalAnswerList().get(i).getId()%>'}<%
						}
					
					}
					%>
				]
			}		
			],
			subquestion:[ 
				{
					option:
					[
					{value:'NONE',id:''}
					<%
					if(questiontext.getSubQuestionList().size()!=0)
					{
					
						for(int i=0;i<questiontext.getSubQuestionList().size();i++)
						{
						%>,{value:'<%=questiontext.getSubQuestionList().get(i).getName()%>',id:'<%=questiontext.getSubQuestionList().get(i).getId()%>'}<%
						}
					
					}
					%>
				]
			}		
			],
			parentQuestion:[ 
				{
					option:
					[
					<%
					if(questiontext.getQuesType()!=null && questiontext.getQuesType().equals("child"))
					{
						if(questiontext.getParentQuestionId()!=null && questiontext.getParentAnswerId()!=null)
						{
							%>{value:'<%=questiontext.getParentAnswerId()%>',id:'<%=questiontext.getParentQuestionId()%>'}<%
						}
						else
						{
							%>{value:'NONE',id:'NONE'}<%
						}
						
					}
					else
					{
						%>{value:'',id:''}<%
					}
					%>
					]
				}		
			]
	
	
			}
		<%
	}
	else
	{
		out.print("Communication failed");
	}
}
%>