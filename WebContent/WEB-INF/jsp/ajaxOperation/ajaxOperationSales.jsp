<%
	String action=request.getParameter("action");
	//Client Name validation
	if(action.equals("validateName")){
		if(request.getAttribute("checkName")!=null)
		{
			int target = (Integer)request.getAttribute("checkName");
			if(target == 1){
%>
				<span style="color:#ee8285;font-weight:bold;" >'<%=request.getParameter("clientName")%>'- This client name seems to be already added.</span>
<%
			}
		}
		else{
			out.print("Communication Failed");
		}
	}
	
	//Client code validation
	else if(action.equals("validateCode")){
		if(request.getAttribute("checkCode")!=null)
		{
			int target = (Integer)request.getAttribute("checkCode");
			if(target == 1){
%>
				<span style="color:#ee8285;font-weight:bold;" >'<%=request.getParameter("clientCode")%>'- This client code seems to be already added.</span>
<%
			}
		}
		else{
			out.print("Communication Failed");
		}
	}
	
	//Client Website url validation
	else if(action.equals("validateWebUrl")){
		if(request.getAttribute("checkWeb")!=null)
		{
			int target = (Integer)request.getAttribute("checkWeb");
			if(target == 1){
%>
				<span style="color:#ee8285;font-weight:bold;" >'<%=request.getParameter("webUrl")%>'- This client URL seems to be already added.</span>
<%
			}
		}
		else{
			out.print("Communication Failed");
		}
	}
	
	//Get account manager id on the basis of the selected clientId
	else if(action.equals("getAccountManager")){
		if(request.getAttribute("acctManagerId")!=null){
			String acctMgrId = (String)request.getAttribute("acctManagerId");
				out.print(acctMgrId);
		}
	}
%>