<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Profile";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Change Password - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
</head>
<body>
<!--Header Start-->
<%@include file="../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Change Password</h2>
      <form:form method="POST" action="changePassword" modelAttribute="changePassword">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle">
              	<span id="validation" style="font-size:16px;"></span>
              	<span id="message" style="font-size: 16px;">
              	<%
              		if(request.getParameter("success")!= null){
              			int success = Integer.parseInt(request.getParameter("success"));
              			if(success==2){
              				out.print("The password does not match the stored password.");
              			}
              			else if(success==0){
              				out.print("Your password could not be updated.");
              			}
              		}
              	%>          
              	</span>              
              	</td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td width="5018" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:25px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td width="152" height="31" align="left" valign="middle">
                  	&nbsp;
                  </td>
                  <td width="935" align="left" valign="middle">
                  	&nbsp;
                  </td>
                </tr>              	              	
                <tr>
                  <td width="152" height="31" align="left" valign="middle">
                  	<form:label path="oldPassword">Old Password<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input type="password" path="oldPassword" class="inptform"/>               
                  </td>
                </tr>
                <tr>
                  <td width="152" height="31" align="left" valign="middle">
                  	<form:label path="password">New Password<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="password" class="inptform" type="password"/>               
                  </td>
                </tr>
                <tr>
                  <td width="152" height="31" align="left" valign="middle">
                  	<label>Confirm Password<span>*</span></label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<input type="password" id="confirmPassword" class="inptform">              
                  </td>
                </tr>                                                                                                             
                <tr>
                  <td height="31" align="left" valign="middle">&nbsp;</td>
                  <td align="left" valign="middle">&nbsp;</td>
                </tr>
                
              </table></td>
            </tr>
            
          </table></td>
        </tr>      
        
        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit"
              	 onClick="return passwordValidation();">
              </td>
              <td width="118" align="center" valign="middle">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  <!-- javascript starts -->
  <script type="text/javascript" src="js/profile/profileScript.js"></script>
  <!-- javascript ends -->
</body>
</html>
