<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Projects";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Add Project - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
</head>
<body onLoad="initialize();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Project</h2>
      <form:form method="POST" action="addProject" modelAttribute="addProject">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
           <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">        
            <tr>
              <td width="0" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td height="31" align="left" valign="top">
                  	  <table width="1080" border="0" cellspacing="0" cellpadding="0">                        
                        <tr>
                          <td align="left" valign="top">
                           <table style="margin-left:10px; " width="1041" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                              <td height="31" align="left" valign="middle" width="235">
                              	<form:label path="proposalId">Proposal Number<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<form:select path="proposalId" class="inptformSelect"
			                   		style="width:243px;" onchange="getProposalInformation(this.value);">
			                      <form:option value="0" label="--Select--"/>
			                      <form:options items="${proposalList}" itemLabel="name" itemValue="id"/>
			                    </form:select>                              </td>
                              <td height="31" align="left" valign="middle" width="250">
                              	<label>Bid Number</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                                <input type="text" disabled="disabled" value="${proposal.bid.bidCode}" 
                                class="inptform">                              </td>
                            </tr>                            
                            <tr>
                              <td height="31" align="left" valign="middle"><label>Client Name</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.bid.bidClient}"
                              	 class="inptform">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Client Sales Contact</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                                <input type="text" disabled="disabled" value="${proposal.clientContact}" 
                                class="inptform">                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><label>Bid Name</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.bid.bidName}" class="inptform">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Client Project Manager</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                                <input type="text" disabled="disabled" value="${proposal.clientProjectManager}"
                                 class="inptform">                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label>Project Type</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.bid.projectType}"
                                 class="inptform">                              </td>
                              <td height="31" align="left" valign="middle"><label>Purchase Order No.</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.purchaseOrderNo}"
                                 class="inptform">                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label>Product Type</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.bid.productType}"
                                 class="inptform">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Target Audience</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.targetAudience}"
                                 class="inptform">                              </td>
                            </tr>
                            <tr>
                              <td width="78" height="31" align="left" valign="middle">
                              	<label>Billing Currency</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.bid.billingCurrency}"
                                 class="inptform" id="bid.billingCurrency">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Notes on Targeting</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.notesOnTargeting}"
                                 class="inptform">                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label>Account Manager</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.bid.accountManager}"
                                 class="inptform">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Start Date</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
              					<input type="text" disabled="disabled" value="${proposal.startDate}"
                                 class="inptform">              				  </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label>Bidding Date</label></td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" value="${proposal.bid.bidDate}"
                                 class="inptform">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>End Date</label></td>
                              <td colspan="2" align="left" valign="middle">                              	
              					<input type="text" disabled="disabled" value="${proposal.endDate}"
                                 class="inptform">              				</td>
                            </tr>
                            <tr>                              
                              <td height="31" align="left" valign="middle">
                              	<label>Project Value</label></td>
                              <td align="left" width="40" valign="middle">
                              	<input type="text" id="currPV" style="width:40px;" class="inptform" disabled="true"/>                              </td>
                              <td width="300" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.projectValue}"
                                 class="inptform"  style="width:180px; text-align:left;" id="projectValue">                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label>Project Minimum</label>                              </td>
                              <td align="left" width="40" valign="middle">
                              	<input type="text" id="currPM" style="width:40px;" class="inptform" disabled="true"/>                              </td>
                              <td width="346" align="left" valign="middle">                              	
                              	<input type="text" disabled="disabled" value="${proposal.projectMinimum}"
                                 class="inptform" style="width:180px; text-align:left;" id="projectMinimum">                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="projectStatus">Project Status<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<form:select path="projectStatus" class="inptformSelect" style="width:243px;">
                                 	<form:option value="0" label="--Select--"/>
                                  	<form:options items="${projectStatusList}" itemLabel="name" itemValue="id"/>
                                 </form:select>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="teamName">Team Name<span>*</span></form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                                 
                                 <form:input path="teamName" class="inptform"/>
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="projectManager">Project Manager<span>*</span></form:label>                              	</td>
                              <td colspan="2" align="left" valign="middle">                              	                                 
                                 <form:select path="projectManager" class="inptformSelect" style="width:243px;"
                                 	onchange="getContactInfo(this.value, 1);">
                                 	<form:option value="0" label="--Select--"/>
                                  	<form:options items="${managerList}" itemLabel="name" itemValue="id"/>
                                 </form:select>                              </td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label for="pmContact">Contact No.</label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	                                 
                                <input type="text" disabled="disabled" class="inptform" id="pmContact"> 
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label for="pmEmail">Email Address</label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" class="inptform" id="pmEmail">
                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="teamLeader">Team Leader<span>*</span></form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                                 <form:select path="teamLeader" class="inptformSelect" style="width:243px;"
                                 	onchange="getContactInfo(this.value, 2);">
                                 	<form:option value="0" label="--Select--"/>
                                  	<form:options items="${managerList}" itemLabel="name" itemValue="id"/>
                                 </form:select>                              </td>
                              <td height="31" align="left" valign="middle">                              </td>
                              <td colspan="2" align="left" valign="middle">              				  </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<label for="tlContact">Contact No.</label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	                                 
                                <input type="text" disabled="disabled" class="inptform" id="tlContact"> 
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<label for="tlEmail">Email Address</label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" disabled="disabled" class="inptform" id="tlEmail">
                              </td>
                            </tr>                            
                            <tr>
                              <td height="31" align="left" valign="top" style="padding-top: 14px;">
                              	<form:label path="culture">Culture<span>*</span></form:label>                              	</td>
                              <td colspan="2" align="left" valign="top" style="padding-top: 10px;">
                              <div style="width: 260px; height: 78px; overflow-y: auto; overflow-x: hidden; ">
                              	<table style="width: 240px; border: 0px;">
	                              <tr>
	                              	<c:forEach items="${cultureList}" var="culture" varStatus="status">
	                              	  <c:choose>
	                              	  	<c:when test="${(status.index)%3==0}">
	                              	  		<c:choose>
	                              	  		  <c:when test="${status.index!=0}">
	                              	  		  </tr>
	                              	  		  <tr>
	                              	  		  	<td>
	                                 	  		<form:checkbox path="culture" label="${culture.code}" value="${culture.id}"
	                                 	  			class="checkboxform"/>
	                                 	  		</td>                                	  		
	                                 	  	  </c:when>
	                                 	  	  <c:otherwise>
	                                 	  	  	<td>
	                                 	  		<form:checkbox path="culture" label="${culture.code}" value="${culture.id}"
	                                 	  			class="checkboxform"/>
	                                 	  		</td>                                	  		                                 	  		
	                                 	  	  </c:otherwise>
	                              	  		</c:choose>
	                                 	  	                                 	  	
	                               	  	</c:when>
	                               	  	<c:otherwise>
	                               	  		<td>               	  	
	                               	  		<form:checkbox path="culture" label="${culture.code}" value="${culture.id}"
	                               		  	class="checkboxform"/>
	                               		  	</td>
	                               	  	</c:otherwise>
	                              	  </c:choose>
	                              	</c:forEach>
	                              </tr>
	                              </table>	                              
                              </div>                             
                                                      	                               
                              </td>
                              <td height="31" align="left" valign="top" style="padding-top: 10px;">
                              	<form:label path="pmRemark">PM Remark</form:label>                              	</td>
                              <td colspan="2" align="left" valign="middle">                              	
                                 <form:textarea path="pmRemark" class="inptformmesg" style="width:236px;"/>
                              </td>                              
                            </tr>                            
                            <tr>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                  	</td>
                  </tr>                
                <tr>
                  <td colspan="10" align="left" valign="top">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Requirement</h2></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" ><table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:0px;" width="91" align="left" valign="middle">Country</td>
              <td width="91" align="left" valign="middle">Services</td>
              <td width="93" align="left" valign="middle">Audience Type</td>
              <td width="130" align="left" valign="middle">Description</td>
              <td width="84" align="left" valign="middle">Sample Size</td>
              <td width="94" align="left" valign="middle">Incidence</td>
              <td width="85" align="left" valign="middle"># Questions</td>
              <td width="104" align="left" valign="middle">LOI</td>              
              <td width="96" align="left" valign="middle">Unit Price</td>
              <td width="96" align="left" valign="middle">Setup Fee</td>
              <td width="159" align="left" valign="middle">Total Cost</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:10px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                               
                <!-- requirements go here -->
               <c:choose>
               <c:when test="${proposal.proposalRequirements.size()>0}">
               <tr>                
                <td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >             
                   <tbody id="requirement">
                   <c:forEach items="${proposal.proposalRequirements}" var="req" varStatus="status">
                    <tr>
                      <td width="93" height="38" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.country}"
                                 class="inptform" style="width:80px;">
                      </td>
                      <td width="93" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.services}"
                                 class="inptform" style="width:80px;" id="r_service${status.index}">
                      </td>
                      <td width="94" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.targetAudience}"
                      		id="r_audience${status.index}" class="inptform" style="width:80px;">
                      </td>
                      <td width="131" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.description}" id="r_desc${status.index}"
                                 class="inptform" style="width:110px;">
                      </td>
                      <td width="86" align="left" valign="middle">                      	
                      	   <input type="text" disabled="disabled" value="${req.sampleSize}" id="r_sample${status.index}"
                                 class="inptform" style="width:65px;">
                      </td>
                      <td width="94" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.incidence}" id="r_incidence${status.index}"
                                 class="inptform" style="width:80px;">
                      </td>
                      <td width="86" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.noOfQuestions}" id="r_question${status.index}"
                              class="inptform" style="width:65px;">
                      </td>
                      <td width="108" align="left" valign="middle">                      	
                      	<input type="text" disabled="disabled" value="${req.loi}" id="r_loi${status.index}"
                              class="inptform" style="width:43px;">
                      	<input type="text" id="minutes" value="minutes" class="inptform2" style="width:38px;
                      	 font-size: 11px; padding-left: 1px;" disabled="disabled"/>                      
                      </td>
                      <td width="98" align="left" valign="middle">
                      	<input id="currencyValF${status.index}" type="text" class="inptform2" style="width:25px;
                      	 	font-size:11px;" disabled="disabled"/>                     
                      	<input type="text" disabled="disabled" value="${req.quotedCpi}" id="r_feas${status.index}"
                                 class="inptform" style="width:43px;">
                      </td>
                      <td width="100" align="left" valign="middle">
                        <input id="currencyVal${status.index}" type="text" class="inptform2" style="width:25px;
                         font-size:11px;" disabled="disabled"/>                      	
                      	<input type="text" disabled="disabled" value="${req.setupFee}" id="r_cpi${status.index}"
                                 class="inptform" style="width:43px;">
                      </td>
                      <td width="93" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="r_total${status.index}" size="10" style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="63" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                    </c:forEach>
                   </tbody>
                  </table></td>                                                               
                  
                </tr>				
                <tr>
                  <td colspan="11" align="left" valign="top"><table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                      <tr class="amboseff">
                        <td width="409" height="38" align="left" valign="middle" bgcolor="#bbcb9b" style="padding-left:5px;"><strong>Grand Total</strong></td>
                        <td width="110" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input name="textfield10" type="text" class="inptformdis" id="sampleSum"
                        	 size="10" style="width: 65px;" readonly />
                        </td>
                        <td width="178" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="92" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="177" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="108" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"><input name="textfield14" type="text" class="inptformdis" id="totalSum" size="10" readonly /></td>
                        <td width="47" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
                </c:when>                  
               </c:choose>
                <!-- requirements end here -->
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                	<td height="30" colspan="19" align="left" valign="top"><strong>Project Specs<span>*</span></strong></td>
                </tr>
                <tr>
                  <td colspan="10" align="left" valign="top">                  	
                  	<textarea class="inptformmesg2" disabled="disabled">${proposal.bid.projectSpecs}</textarea>
                  </td>
                </tr> 
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>                
                <tr>
                  <td colspan="11" align="left" valign="top">
                  	<table width="500" border="0" cellspacing="0" cellpadding="0">
                   <c:forEach items="${proposal.bid.documentNames}" var="dName">
                    <tr>
                      <td height="29" colspan="2" align="left" valign="middle" style="font-size:13px; color:#006600;
                      	 padding-right:3px;">
                      	<a href="downloadBidFile?bidId=${proposal.bid.bidId}&fileName=${dName}" class="grytxt">
                      		${dName}
                      	</a>
                      </td>                      
                    </tr>
                   </c:forEach>
                   <c:forEach items="${proposal.documentNames}" var="dName">
                    <tr>
                      <td height="29" colspan="2" align="left" valign="middle" style="font-size:13px; color:#006600; 
                      	 padding-right:3px;">
                      	<a href="downloadProposalFile?proposalId=${proposal.proposalId}&fileName=${dName}" class="grytxt">
                      		${dName}
                      	</a>
                      </td>                      
                    </tr>
                   </c:forEach>                  
                  </table>
                  </td>
                </tr>
                
              </table></td>
            </tr>
          </table></td>
        </tr>
            
          </table></td>
        </tr>        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return projectValidation(0);"></td>
              <td width="118" align="center" valign="middle"><input type="reset" name="Submit3" id="Submit3" value="Clear" class="btnsubmit"></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
<!-- javascript starts here -->
<script type="text/javascript" src="js/sales/proposalScript.js"></script>
<script type="text/javascript" src="js/projects/projectScript.js"></script>
<script type="text/javascript">
	var reqSize = '${proposal.proposalRequirements.size()}';
</script>
<!-- javascript ends here -->
</body>
</html>
