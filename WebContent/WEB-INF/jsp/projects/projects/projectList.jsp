<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Projects";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Project List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddproject">
      <h3><a href="addProjectForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:15px;">Search Project</h2>
      	<form:form method="POST" modelAttribute="searchProject" action="projectList">
      		<form:label path="projectNumber">Project Number</form:label>
      		<form:input path="projectNumber" style=" width:120px;"/>
			<form:label path="projectName">Project Name</form:label>
			<form:input path="projectName" style=" width:120px;"/>
			<form:label path="projectStatus">Status</form:label>
			<form:select path="projectStatus" style=" width:120px;" class="selectdeco">
		        <form:option value="0" label="--Select--"></form:option>
		      	<form:options items="${projectStatusList}" itemValue="id" itemLabel="name"/>
		      </form:select>
       		<input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>       		
      	</form:form>
      
      </div>
      </div>
      <div class="searchresult2">
      <h2>Project Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="113" align="left" valign="middle">Project Number</td>
                    		<td width="284" align="left" valign="middle">Project Name</td>
                    		<td width="167" align="left" valign="middle">Client Code</td>
		                    <td width="160" align="left" valign="middle">Project Type</td>
		                    <td width="202" align="left" valign="middle">Status</td>
		                    <td width="161" align="left" valign="middle">Project Manager</td>
                  		  </tr>
              			</table>
              		  </td>
            		</tr>            		
            	<c:if test="${not empty projectList}">
      			  <c:set var="pageUrl" value="projectList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}"
      	  			 isOffset="true">            		
            	<%--start --%>
            	<pg:param name="projectNumber" value="${param.projectNumber}"/>
		        <pg:param name="projectName" value="${param.projectName}"/>
		        <pg:param name="projectStatus" value="${param.projectStatus}"/>
		       	<c:forEach items="${projectList}" var="proVar" varStatus="pStatus">
		       	
					<pg:item>       	
				      <c:if test="${pStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editProjectForm?projectId=${proVar.projectId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${proVar.projectNumber}</td>
                      			  <td width="284" align="left" valign="middle">${proVar.projectName}</td>
			                      <td width="167" align="left" valign="middle">${proVar.clientCode}</td>
			                      <td width="160" align="left" valign="middle">${proVar.projectType}</td>
			                      <td width="202" align="left" valign="middle">${proVar.projectStatus}</td>
			                      <td width="161" align="left" valign="middle">${proVar.projectManager}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${pStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editProjectForm?projectId=${proVar.projectId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${proVar.projectNumber}</td>
                      			  <td width="284" align="left" valign="middle">${proVar.projectName}</td>
			                      <td width="167" align="left" valign="middle">${proVar.clientCode}</td>
			                      <td width="160" align="left" valign="middle">${proVar.projectType}</td>
			                      <td width="202" align="left" valign="middle">${proVar.projectStatus}</td>
			                      <td width="161" align="left" valign="middle">${proVar.projectManager}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    