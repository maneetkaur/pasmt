<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Projects";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Add Survey - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Survey</h2>
      <form:form method="POST" action="addSurvey" modelAttribute="addSurvey">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
           <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">        
            <tr>
              <td width="0" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td height="31" align="left" valign="top">
                  	  <table width="1080" border="0" cellspacing="0" cellpadding="0">                        
                        <tr>
                          <td align="left" valign="top">
                           <table style="margin-left:10px; " width="1041" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                              <td height="31" align="left" valign="middle" width="213">
                              	<form:label path="projectId">Project #<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<form:select path="projectId" class="inptformSelect"
			                   		style="width:243px;" onchange="getProjectInformation(this.value);">
			                      <form:option value="0" label="--Select--"/>
			                      <form:options items="${projectList}" itemLabel="name" itemValue="id"/>
			                    </form:select>
			                   </td>
			                   <td height="31" align="left" valign="middle" width="206">
                              	&nbsp;
                               </td>
                               <td width="200" colspan="2" align="left" valign="middle">                              	
                              	&nbsp;
                               </td>                            
                            </tr>                            
                            <tr>
                              <td height="31" align="left" valign="middle" width="213">
                              	<form:label path="">Survey ID</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<form:input path="surveyCode" class="inptform"/>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="wave">Wave</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<form:input path="wave" class="inptform"/>
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="culture">Culture</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<select id="culture" name="culture" class="inptformSelect" style="width:243px;">
                              		<option id="0" value="--Select--">
                              	</select>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="surveyCategory">Category</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              	<form:select path="surveyCategory" class="inptformSelect" style="width:243px;">
                              	  <form:option value="0" label="--Select--"/>
                              	  <form:options items="${surveyCategoryList}" itemValue="categoryTypeId" 
                              	  	itemLabel="categoryType"/>
                              	</form:select>
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="completesQuota">Completes Quota</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<form:input path="completesQuota" class="inptform"/>                              	
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="softLaunchQuota">Soft Launch Quota</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<form:input path="softLaunchQuota" class="inptform"/>
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="incidenceRate">Incidence Rate</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<form:select path="incidenceRate" class="inptformSelect" style="width:243px;" >                      		
		                      		<form:option value="-1" label="--Select--"/>
		                      		<form:option value="101" label="NA"/>
		                      		<form:option value="0" label="<1%"/>
		                      		<c:forEach var="index" begin="1" end="100">
		                      			<form:option value="${index}" label="${index}%"/>
		                      		</c:forEach>
		                      	</form:select>
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="loi">LOI</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<form:input path="loi" class="inptform" style="width:180px;"/>
                              	<input type="text" id="loiMin" style="width:40px;" class="inptform" readonly="readonly"
                              		value="minutes"/>
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="cpi">CPI</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" id="currCpi" style="width:40px;" class="inptform" 
                              		readonly="readonly"/>
                              	<form:input path="cpi" class="inptform" style="width:180px;"/>
                              </td>
                              <td width="206" height="31" align="left" valign="middle">
                              	<form:label path="maxIncentiveBudget">Max. Incentive Budget</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              	<input type="text" id="currCpi" style="width:40px;" class="inptform" 
                              		readonly="readonly"/>
                              	<form:input path="maxIncentiveBudget" class="inptform" style="width:180px;"/>
                              </td>
                            </tr>
                            <tr>                              
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Panel</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                              </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="approvedChannels">Approved Channels</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle" width="180px;">
                              	<form:checkbox path="approvedChannels" value="1" label="Direct Invites"/>
                              	<form:checkbox path="approvedChannels" value="2" label="Router"/>
                              	<form:checkbox path="approvedChannels" value="3" label="Portal"/><br/>
                              	<form:checkbox path="approvedChannels" value="4" label="OSBT"/>
                              	<form:checkbox path="approvedChannels" value="5" label="Vendor"/>                         	
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Sensitive Industry</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
              				  </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Past Participation Category</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">
                               
                              </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Past Participation Days</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
              				  </td>                              
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">De-dupe Respondents</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
              				  </td>                              
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Custom De-dupe</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
              				  </td>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Start Date</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                              	
                              </td>                                                           
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">
                              	<form:label path="">Close Date</form:label>
                              </td>
                              <td colspan="2" align="left" valign="middle">                                 
                              </td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>                                                                                   
                            <tr>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                  	</td>
                  </tr>                
                <tr>
                  <td colspan="10" align="left" valign="top">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            
            <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Requirement</h2></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" ><table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:0px;" width="91" align="left" valign="middle">Country</td>
              <td width="91" align="left" valign="middle">Services</td>
              <td width="93" align="left" valign="middle">Audience Type</td>
              <td width="130" align="left" valign="middle">Description</td>
              <td width="84" align="left" valign="middle">Sample Size</td>
              <td width="94" align="left" valign="middle">Incidence</td>
              <td width="85" align="left" valign="middle"># Questions</td>
              <td width="104" align="left" valign="middle">LOI</td>              
              <td width="96" align="left" valign="middle">Unit Price</td>
              <td width="96" align="left" valign="middle">Setup Fee</td>
              <td width="159" align="left" valign="middle">Total Cost</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <!-- Change begins -->
          
          <!-- change ends here -->
          </td>
        </tr>
            
          </table></td>
        </tr>        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return projectValidation()"></td>
              <td width="118" align="center" valign="middle"><input type="reset" name="Submit3" id="Submit3" value="Clear" class="btnsubmit"></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
<!-- javascript starts here -->
<script type="text/javascript" src="js/sales/proposalScript.js"></script>
<script type="text/javascript" src="js/projects/projectScript.js"></script>
<script type="text/javascript">
	var reqSize = '${proposal.proposalRequirements.size()}';
</script>
<!-- javascript ends here -->
</body>
</html>
