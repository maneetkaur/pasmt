<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Projects";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Survey List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddsurvey">
      <h3><a href="addSurveyForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:15px;">Search Survey</h2>
      	<form:form method="POST" modelAttribute="searchSurvey" action="surveyList">
      		<form:label path="surveyCode">Survey Id</form:label>
      		<form:input path="surveyCode" style=" width:120px;"/>
			<form:label path="culture">Culture</form:label>
			<form:select path="culture" style=" width:120px;" class="selectdeco">
		        <form:option value="0" label="--Select--"></form:option>
		      	<form:options items="${cultureList}" itemValue="id" itemLabel="code"/>
		    </form:select>
			<form:label path="surveyStatus">Status</form:label>
			<form:select path="surveyStatus" style=" width:120px;" class="selectdeco">
		        <form:option value="0" label="--Select--"></form:option>
		      	<form:option value="1" label="Pending"/>
		      	<form:option value="2" label="On Hold"/>
		      	<form:option value="3" label="Testing"/>
		      	<form:option value="4" label="Live"/>
		      	<form:option value="5" label="Closed"/>
		    </form:select>
       		<input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>       		
      	</form:form>
      
      </div>
      </div>
      <div class="searchresult2">
      <h2>Survey Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="82" align="left" valign="middle">Survey Id</td>
                    		<td width="65" align="left" valign="middle">Culture</td>
                    		<td width="91" align="left" valign="middle">Client Code</td>
		                    <td width="121" align="left" valign="middle">Project Type</td>
		                    <td width="88" align="left" valign="middle">Start Date</td>
		                    <td width="71" align="left" valign="middle">Starts</td>
                  		    <td width="81" align="left" valign="middle">Completes</td>
                  		    <td width="97" align="left" valign="middle">Conversion</td>
                  		    <td width="76" align="left" valign="middle">LOI</td>
                  		    <td width="111" align="left" valign="middle">Close Date</td>
                  		    <td width="89" align="left" valign="middle">Status</td>
                  		    <td width="115" align="left" valign="middle">Project Manager</td>
                  		  </tr>
              			</table>
              		  </td>
            		</tr>            		
            	<c:if test="${not empty surveyList}">
      			  <c:set var="pageUrl" value="surveyList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}"
      	  			 isOffset="true">            		
            	<%--start --%>
            	<pg:param name="surveyCode" value="${param.surveyCode}"/>
		        <pg:param name="culture" value="${param.culture}"/>
		        <pg:param name="surveyStatus" value="${param.surveyStatus}"/>
		       	<c:forEach items="${surveyList}" var="surVar" varStatus="sStatus">
		       	
					<pg:item>       	
				      <c:if test="${sStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editSurveyForm?surveyId=${surVar.surveyId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  	<td width="82" align="left" valign="middle">${surVar.surveyCode}</td>
		                    		<td width="65" align="left" valign="middle">${surVar.culture}</td>
		                    		<td width="91" align="left" valign="middle">${surVar.clientCode}</td>
				                    <td width="121" align="left" valign="middle">${surVar.projectType}</td>
				                    <td width="88" align="left" valign="middle">${surVar.startDate}</td>
				                    <td width="71" align="left" valign="middle">TBD</td>
		                  		    <td width="81" align="left" valign="middle">TBD</td>
		                  		    <td width="97" align="left" valign="middle">TBD</td>
		                  		    <td width="76" align="left" valign="middle">${surVar.loi}</td>
		                  		    <td width="111" align="left" valign="middle">${surVar.closeDate}</td>
		                  		    <td width="89" align="left" valign="middle">${surVar.surveyStatus}</td>
		                  		    <td width="115" align="left" valign="middle">${surVar.projectManager}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${sStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editSurveyForm?surveyId=${surVar.surveyId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  	<td width="82" align="left" valign="middle">${surVar.surveyCode}</td>
		                    		<td width="65" align="left" valign="middle">${surVar.culture}</td>
		                    		<td width="91" align="left" valign="middle">${surVar.clientCode}</td>
				                    <td width="121" align="left" valign="middle">${surVar.projectType}</td>
				                    <td width="88" align="left" valign="middle">${surVar.startDate}</td>
				                    <td width="71" align="left" valign="middle">TBD</td>
		                  		    <td width="81" align="left" valign="middle">TBD</td>
		                  		    <td width="97" align="left" valign="middle">TBD</td>
		                  		    <td width="76" align="left" valign="middle">${surVar.loi}</td>
		                  		    <td width="111" align="left" valign="middle">${surVar.closeDate}</td>
		                  		    <td width="89" align="left" valign="middle">${surVar.surveyStatus}</td>
		                  		    <td width="115" align="left" valign="middle">${surVar.projectManager}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    