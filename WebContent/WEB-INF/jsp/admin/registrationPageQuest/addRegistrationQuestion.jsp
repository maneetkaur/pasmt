<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "Admin";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Add Registration Page Question- PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript">

var i = 1;
var rowCount=1;
var btn_close='<td width="113" align="left" valign="middle" id="remove" class="btn-remove">'+
    		  '<img src="images/btncross.png" width="18" height="17" border="0"></td>';
$(document).ready(function(){
	//alert('2');
		
		$("#addMore").click(function() {	
			//alert('3');
			 var tr_temp=$("#categoryTable tr:first").clone();
			 tr_temp.find("#category0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'category'+i},
	    				'name': function(_, name) { return 'categoryList['+ i +'].categoryName'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#language0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'language'+i},
	    				'name': function(_, name) { return 'categoryList['+ i +'].languageId'},
	    				'value': ''
	  			});
				}).end();
				tr_temp.find("td:last").remove();
			 	tr_temp.append(btn_close);
				tr_temp.appendTo("#categoryTable");
			 i++;
			 
			
		});
		$("#remove").live('click',function(){
			//alert('4');
			$(this).parent().remove();
			rowCount--;
		});
		
});
</script>
</head>
<body>
<!--Header Start-->
<div class="headerbox">
  <%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Category</h2>
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
		    <form:form modelAttribute="add_registraionQuesthujh" action="submitCategory" method="post" enctype="multipart/form-data">
            <table width="1087" border="0" align="right" cellpadding="0" cellspacing="0">
          
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="977" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              <tr>
                <td width="110" height="31" align="left" valign="middle">Registraion Page Question Label <span>*</span></td>
                <td colspan="5" align="left" valign="middle">
                <form:input path="regPageQuestionGlobal" class="inptform" />
                <br/><span  id="msg_category" style="color:red;padding-left: 20px;"></span></td>
              </tr>
           
              <tr>
                <td height="31" align="left" valign="middle">
                <input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit"></td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
               <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
             
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
                         
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return checkCategoryValidation();"></td>
                    <td width="118" align="center" valign="middle"><a href="categoryList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
             
            </table>    </form:form>       </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    
    