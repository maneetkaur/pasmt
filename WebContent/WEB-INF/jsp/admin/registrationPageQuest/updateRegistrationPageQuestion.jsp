<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%> 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Registration Page Question- PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript">


var sizeOfList='${update_category.categoryList.size()}';
var i = sizeOfList;
var btn_close='<td width="40" align="left" valign="middle" id="remove" class="btn-remove">'+
			  '<img src="images/btncross.png" width="18" height="17" border="0"></td>';
$(document).ready(function(){
	//alert('2');
		
		$("#addMore").click(function() {	
			//alert('3');
			 var tr_temp=$("#categoryTable tr:first").clone();
			 tr_temp.find("#category0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'category'+i},
	    				'name': function(_, name) { return 'categoryList['+ i +'].categoryName'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#language0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'language'+i},
	    				'name': function(_, name) { return 'categoryList['+ i +'].languageId'},
	    				'value': ''
	  			});
				}).end();
			 	tr_temp.find("td:last").remove();
			 	tr_temp.append(btn_close);
				tr_temp.appendTo("#categoryTable");
			 i++;
			 
			
		});
		$("#remove").live('click',function(){
			//alert('4');
			$(this).parent().remove();
			rowCount--;
		});
		
});

var values=new Array();
function getRowValues()
{
	for(var j=0;j<sizeOfList;j++)
	{
		values[j]=new Array();
		values[j][0]=document.getElementById('language'+j).value;
		values[j][1]=document.getElementById('category'+j).value;
	}
	document.getElementById("sizeOfList").value=sizeOfList;
}
var categoryIdList;
function checkRowUpdation()
{
	categoryIdList="";
	for(var i=0;i<sizeOfList;i++)
	{
		if(values[i][0]!=document.getElementById('language'+i).value || values[i][1]!=document.getElementById('category'+i).value)
		{
			categoryIdList+=i+",";		
		}
	}
	document.getElementById("categoryIdList").value=categoryIdList;
}

</script>
</head>
<body onLoad="return getRowValues()">
<!--Header Start-->
<div class="headerbox">
  <%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Update Category</h2>
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        
       <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
		    <form:form modelAttribute="update_category" action="editCategory" method="post" enctype="multipart/form-data">
            <table style="margin-left:10px;" width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
          
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="1015" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              <tr>
                <td width="115" height="31" align="left" valign="middle">Category <span>*</span></td>
                <td colspan="5" align="left" valign="middle" width="1015">
                <form:input path="categoryName" class="inptform" onblur="checkCategoryNameAvail(this.value)"/>
                <br/><span  id="msg_category" style="color:red"></span></td>
              </tr>
              <tr>
                <td height="31" colspan="6"  valign="middle"><table width="1046" border="0" cellspacing="0" cellpadding="0">
                  <tbody id="categoryTable">
                  
                  <c:forEach items="${update_category.categoryList}" varStatus="st">
                 <tr>
                  <td width="115" align="left" valign="middle">Language <span>*</span></td>
                   <td width="265" align="left" valign="middle">
                    <form:select path="categoryList[${st.index}].languageId" class="inptformSelect" id="language${st.index}" style="width:243px;"  >
                    	<form:option value="NONE" label="--Language--"/>
                        <form:options items="${languageList}" itemLabel="name" itemValue="id"/>                        
                    </form:select></td>
                    <td width="146" align="left" valign="middle">Category name <span>*</span></td>
                    <td width="265" align="left" valign="middle">
                   <form:input path="categoryList[${st.index}].categoryName" class="inptform" id="category${st.index}"/>
                      <form:hidden path="categoryList[${st.index}].categoryId" id="ids${st.index}"/>           
                    </td>
                  <td  width="255" align="left" valign="middle" >&nbsp;</td>
                  </tr>                  
                  </c:forEach>
                  <form:hidden path="sizeOfList"/>
                  <form:hidden path="categoryIdList"/>
                  </tbody>
                </table></td>
                </tr>
            
              <tr>
                <td height="31" align="left" valign="middle">
                <input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit"></td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>  
               <tr>
                <td width="110" height="31" align="left" valign="middle">Uploaded Image</td>
                <td width="50"  align="left" valign="middle"><img src="${update_category.imagePath}" alt="image"/>
               </td>
              </tr>  
             <tr>
                <td width="110" height="31" align="left" valign="middle">Category Image<span>*</span></td>
                <td width="50"  align="left" valign="middle">
                <form:input path="categoryImage"  type="file" accept="image/*" onchange="imageValidation(this.value)"/></td>
               <td><span  id="msg_category_image" style="color:red;padding-left: 20px;"></span></td>
              </tr>  
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit" onClick="checkRowUpdation();return checkCategoryValidation()"></td>
                    <td width="118" align="center" valign="middle"><a href="categoryList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
             
            </table>    </form:form>       </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
function checkCategoryValidation()
{
	//var status=true;
	var languageList=new Array();
	if(document.getElementById("categoryName").value=="" || document.getElementById("category1").value=="" || document.getElementById("language1").value=="NONE" )
	{
		document.getElementById("validation").innerHTML="Please enter all mandatory fields";
		return false;	
	}
	for(var j=0; j<i; j++){  
		  if(document.getElementById('language' + j)){
		    if(document.getElementById('language' + j).value!=""){
		    	languageList[j]=document.getElementById('language' + j).value;
		    }
		  }
	}
	for(var k=0;k<languageList.length;k++)
	{
		for(var l=k+1;l<languageList.length;l++)
		if(languageList[k]==languageList[l])
		{
			document.getElementById("validation").innerHTML="Please don't select a language more than once."
			return false;
		}
	}
	return true;
}
function checkCategoryNameAvail(category){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		var a=request.open("GET","ValidateCategoryName?Category="+category+"&action=validateCatgeory",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msg_category').innerHTML = request.responseText;
			var text=request.responseText.trim("");
			if(text.length != 0)
			{
				document.getElementById('categoryName').value = '';
			}
			request.abort();
			request = null;
}
function imageValidation(fileName){
    var extIndex=fileName.lastIndexOf('\\');
    fileName = fileName.substring(extIndex+1);
    
    extIndex=fileName.lastIndexOf(".");
    var extension=fileName.substring(extIndex+1);
   // id = id.substring(8); //to get the number after the id
    
    if(extension.length>4){
          document.getElementById('msg_category_image').innerHTML = "File extension cannot be longer than 4 characters";
          document.getElementById('categoryImage').value="";
          return;
    }
    if(extension!="jpeg" && extension!="jpg" && extension != "png" && extension != "gif"){
          document.getElementById('msg_category_image').innerHTML = "You can only upload a JPEG/PNG image";         
          document.getElementById('categoryImage').value="";
          return;
    }
    document.getElementById('msg_category_image').innerHTML = "";
}
</script>
    