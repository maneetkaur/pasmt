  <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%>    
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Company - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
 <%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Update Company</h2>
      <form:form action="editCompany" modelAttribute="edit" method="post">
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
      <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;"><span>*</span>indicates mandatory field</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" > 
            <table style="margin-left:10px; "width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
            <div id="Error_msg" style="color: red" align="center"></div>
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="972" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Company Name<span>*</span> </td>
                <td colspan="5" align="left" valign="middle"><form:input path="companyName"  onfocus="hideMessage()" class="inptform" onblur="checkCompanyNameAvail(this.value)"/>
              <br/><span  id="msg_company" style="color:red; padding-left: 20px;"></span></td>
              </tr>
              <tr>
                <td width="158" height="31" align="left" valign="middle">Website </td>
                <td colspan="5" align="left" valign="middle"><form:input path="website" class="inptform" onchange="checkURLexist(this.value)"/>
                <br/><span  id="msg_website" style="color:red; padding-left: 20px;" ></span></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Country<span>*</span> </td>
                <td colspan="5" align="left" valign="middle">
                 <form:select path="countryId"   class="inptformSelect">
                     <form:option value="NONE" label="--Country--"/>
                     <c:forEach var="coun" items="${allCountryList}">
                     <form:option value="${coun.id}">${coun.name}</form:option>
                     </c:forEach>
                     </form:select></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Contact Person</td>
                <td colspan="5" align="left" valign="middle"><form:input path="contactPerson" class="inptform"/>
                </td>
              </tr>
              <tr>
                 <td height="31" align="left" valign="middle">Email Address</td>
                <td colspan="5" align="left" valign="middle"><form:input path="emailId" class="inptform" onchange="checkEmailIdFormat(this)" />
                 <br/><span  id="msg_email" style="color:#ee8285;font-weight:bold;padding-left: 20px;"></span></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Phone No.</td>
                <td colspan="5" align="left" valign="middle"><form:input path="phoneNo" class="inptform" />
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:8px;">Address</td>
                <td colspan="5" align="left" valign="middle"><form:textarea path="address" class="inptformmesg"/>
               </td>
              </tr> 
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit" onClick="return checkCompanyValidation();"></td>
                    <td width="118" align="center" valign="middle"><a href="companyList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
            </table>          </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
var company=document.getElementById("companyName").value;
var website=document.getElementById("website").value;
var email_valid=true;
function checkCompanyValidation()
{
	var status=true;
	if(document.getElementById("countryId").value=="NONE" && document.getElementById("companyName").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Company and Country";
		status=false;	
	}
	else if(!document.getElementById("countryId").value=="NONE" && document.getElementById("companyName").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Company name";
		status=false;	
	}
	else if(document.getElementById("countryId").value=="NONE" && ! document.getElementById("companyName").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please select Country";
		status=false;	
	}
	else if(email_valid==false)
	{
		status=false;
	}
	
	return status;
}
function checkCompanyNameAvail(companyName){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		var a=request.open("GET","ValidateCompany?Company="+companyName+"&action=validateCompany",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msg_company').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
				document.getElementById('companyName').value = company;
			}
			request.abort();
			request = null;
}
function validateEmail(email){
	var email_value = document.getElementById(email).value;
	var atpos=email_value.indexOf("@");
	var dotpos=email_value.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_value.length)
	{
	  return false;
	}
	return true;
}
function checkEmailIdFormat(email)
{
	email_valid=true;
	if(email.value != ""){
		if(!validateEmail('emailId')){
			document.getElementById('msg_email').innerHTML="The email id format is not valid.";
			email_valid=false;
		}
	}
}
function checkURLexist(url){
	if (window.ActiveXObject) 
    {          
     	request1 = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request1 = new XMLHttpRequest();          
    } 
	if (request1) 
	{
		request1.onreadystatechange = handleStateChange1; 
		var a=request1.open("GET","ValidateWebsiteURL?Website="+url+"&action=validateWebsite",true);
		request1.send(true);	
 	}
	
}
function handleStateChange1()
{	
			if (request1.readyState != 4) return;
			if (request1.status != 200) return;
			document.getElementById('msg_website').innerHTML = request1.responseText;
			var text=request1.responseText.trim("");
			if(text.length != 0)
			{
				document.getElementById('website').value = website;
			}
			request1.abort();
			request1 = null;
}
</script>

