<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
      <%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%
	String topMenu = "Admin";
%>  
<%!
    Integer pagerPageNumber=new Integer(0);
%>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View Region - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddregion">
      <h3><a href=#	></a></h3>
      </div>
      <div class="searchbar">
      <h2>Search Region</h2>
      <form:form action="searchRegion" modelAttribute="search" method="post"> 
      <label>Country Name</label>
     <form:input path="country"/>
       <label>Country Code </label>
       <form:input path="countryCode"/>
      <input type="submit" name="button" id="button" value="Search" class="btnsearch"/>
      </form:form>
      <h3><a href="#">Advanced Search</a></h3>
      </div>
      </div>
      <div class="searchresult2">
        <h2> Region List</h2>
        <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="1087" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" ><table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="left" valign="top"><table width="1135" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
                      <tr>
                        <td height="37" align="left" valign="middle" bgcolor="#778d4c"><table width="1130" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
                            <tr>
                              <td width="310" align="left" valign="middle">Sr.no</td>
                              <td width="309" align="left" valign="middle">Country </td>
                              <td width="282" align="left" valign="middle">Country Code</td>
                              <td width="229" align="left" valign="middle">Download Zips</td>
                            </tr>
                        </table></td>
                      </tr>
               <c:if test="${not empty regionList}">
              <c:set var="pageUrl" value="regionList"/>
           <c:set var="totalRows" value="${requestScope.total_rows}"/>
           <pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">
           <c:forEach items="${regionList}" var="ll" varStatus="counter">  
			<pg:item> 
			<c:choose>
			
            	<c:when test="${counter.count%2==0}">
                       <tr>
                        <td height="37" align="left" valign="middle" ><a href="#">
                          <div class="blacktxt">
                            <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
                              <tr>
                              <form:form action="uploadZips" modelAttribute="ZipCode" method="post" enctype="multipart/form-data">
                                <td width="223" align="left" valign="middle">${counter.count}</td>
                                <td width="223" align="left" valign="middle">${ll.country}</td>
                                <td width="203" align="left" valign="middle"> ${ll.countryCode}</td>
                                 
                                
                                </form:form>
                               <td width="164" align="left" valign="middle"><a href="downloadFile?country=${ll.country}"><input name="button3" type="submit" class="btnsearch2" id="button3" value="Download Zips" ></a></td>
                              </tr>
                            </table>
                          </div>
                        </a> </td>
                      </tr>
                       </c:when>
            <c:otherwise>
                      <tr>
                        <td height="37" align="left" valign="middle" ><a href="#">
                          <div class="blacktxt2">
                            <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
                              <tr>
                              <form:form action="uploadZips" modelAttribute="ZipCode" method="post" enctype="multipart/form-data">
                              <td width="223" align="left" valign="middle">${counter.count}</td>	
                                <td width="223" align="left" valign="middle">${ll.country}</td>
                                <td width="203" align="left" valign="middle">${ll.countryCode}</td>
                                <form:hidden path="countryId" value="${ll.countryId}"/> 
                                
                                 </form:form>
                                <td width="164" align="left" valign="middle"><a href="downloadFile?country=${ll.country}"><input name="button3" type="submit" class="btnsearch2" id="button3" value="Download Zips"></a></td>
                              </tr>
                            </table>
                          </div>
                        </a></td>
                      </tr>
                     </c:otherwise>
             </c:choose>
            </pg:item>
           </c:forEach>
           <%@include file="../../include/paging_bar.jsp"%>
           </pg:pager>
          </c:if>
                            </table>
                          
                        </a></td>
                      </tr>
                      
                      <tr>
                        <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td height="2" colspan="2" align="left" valign="middle" ></td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
