<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Culture - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Update Culture</h2>
      <table width="820" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn"><table width="456" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td height="210" align="left" valign="top" bgcolor="#A6B881" class="pageupdatebg"><table width="416" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="18" align="left" valign="top" class="whitedeco">Country</td>
                    </tr>
                    <br/>
                    <c:if test="${message=='Already Exist'}">
                      <div id="Error_msg" style="color: red" align="center">This Culture is already exist</div>
                    </c:if>
                  <form:form action="editCulture" method="post" modelAttribute="edit">
                  
                  <tr>
                    <td align="left" valign="top"><form:select path="countryId"  class="forminpt">
                      <c:forEach var="coun" items="${countryList}">
                        <form:option value="${coun.id}">${coun.name}</form:option>
                      </c:forEach>
                    </form:select>
                    <br/>
                      <span  id="msg_country" style="color:red"></span> </td>
                  </tr>
                  <tr>
                    <td height="32" align="left" valign="middle" class="whitedeco">Language</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top"><form:select path="languageId" class="forminpt" >
                      <c:forEach var="item" items="${languageList}">
                        <form:option value="${item.id}">${item.name}</form:option>
                      </c:forEach>
                    </form:select>
                    <br/>
                      <span  id="msg_countryCode" style="color:red"></span> </td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="64" align="left" valign="top"><table width="272" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="180" align="right" valign="middle"><input type="submit" value="Update" class="btnsubmit2" onClick="return checkCultureValidation();"></td>
                        <td width="103" align="right" valign="middle"><a href=cultureList class="btnsubmit2">Cancel</a></td>
                      </tr>
                      </form:form>
                      
                    </table></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
function  checkCultureValidation()
{
	var status=true;
	if(document.getElementById("country").value=="" && document.getElementById("countryCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please select all fields";
		status=false;	
	}
	else if(!document.getElementById("country").value=="" && document.getElementById("countryCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please select Language";
		status=false;	
	}
	else if(document.getElementById("country").value=="" && ! document.getElementById("countryCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please select Country";
		status=false;	
	}
	
	return status;
}

</script>

    