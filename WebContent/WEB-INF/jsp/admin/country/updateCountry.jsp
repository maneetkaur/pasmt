<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "Admin";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Country - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Country</h2>
      <table width="820" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn"><table width="456" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td height="210" align="left" valign="top" bgcolor="#A6B881" class="pageupdatebg"><table width="416" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="18" align="left" valign="top" class="whitedeco">Country</td>
                    </tr>
                    <br>
                    <div id="Error_msg" style="color: red" align="center"></div>
                  <form:form action="editCountry" method="post" modelAttribute="edit">
                  
                    <tr>
                      <td align="left" valign="top"><form:input path="country" class="forminpt"  onblur="checkCountryNameAvail(this.value)" />
                          <br/>
                        <span  id="msg_country" style="color:red"></span> </td>
                    </tr>
                    <tr>
                      <td height="32" align="left" valign="middle" class="whitedeco">Country Code</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><form:input path="countryCode" class="forminpt"  onfocus="hideMessage()" onblur="checkCountryCodeAvail(this.value)" />
                          <br/>
                        <span  id="msg_countryCode" style="color:red"></span> </td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="64" align="left" valign="top"><table width="272" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" align="right" valign="middle"><input type="submit" value="Submit" class="btnsubmit2" onClick="return checkCountryValidation();"></td>
                            <td width="103" align="right" valign="middle"><a href=countryList class="btnsubmit2">Cancel</a></td>
                          </tr>
                        </form:form>
                        
                      </table></td>
                    </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="112" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
function checkCountryValidation()
{
	var status=true;
	if(document.getElementById("country").value=="" && document.getElementById("countryCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Country and Country code";
		status=false;	
	}
	else if(!document.getElementById("country").value=="" && document.getElementById("countryCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Country Code";
		status=false;	
	}
	else if(document.getElementById("country").value=="" && ! document.getElementById("countryCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Country";
		status=false;	
	}
	
	return status;
}
function checkCountryNameAvail(country){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		var a=request.open("GET","ValidateCountry?Country="+country+"&action=validateCountry",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msg_country').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
				document.getElementById('country').value = '';
			}
			request.abort();
			request = null;
}
function checkCountryCodeAvail(countryCode){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange1; 
		var a=request.open("GET","ValidateCountryCode?CountryCode="+countryCode+"&action=ValidateCountryCode",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange1()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;						
			document.getElementById('msg_countryCode').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
				document.getElementById('countryCode').value = '';
			}
			request.abort();
			request = null;
}
		
</script>

    