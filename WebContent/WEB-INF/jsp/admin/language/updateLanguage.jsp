<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "Admin";
%> 
 <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Language - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body onload="getlangDetail()">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Update Language</h2>
      <table width="820" border="0" align="right" cellpadding="0" cellspacing="0">
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn"><table width="456" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td height="210" align="left" valign="top" bgcolor="#A6B881" class="pageupdatebg"><table width="416" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="18" align="left" valign="top" class="whitedeco">Language</td>
                    </tr>
                    <br>
                    <div id="Error_msg" style="color: red" align="center"></div>
                  <form:form action="editLanguage" method="post" modelAttribute="edit">
                  
                    <tr>
                      <td align="left" valign="top"><form:input path="lang" class="forminpt" onchange="checklanguageNameAvail(this.value)"  />
                          <br/>
                        <span  id="msg_lang" style="color:red"></span></td>
                    </tr>
                    <tr>
                      <td height="32" align="left" valign="middle" class="whitedeco">Language Code</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><form:input path="langCode" class="forminpt" onchange="checklanguageCodeAvail(this.value)" maxlength="2"/>
                      <input type="hidden" id="preCodee"/>
                          <br/>
                        <span  id="msg_langCode" style="color:red"></span></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td height="64" align="left" valign="top"><table width="272" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="180" align="right" valign="middle"><input type="submit" value="Update" class="btnsubmit2" onClick="return checkLanguageValidation();"></td>
                            <td width="103" align="right" valign="middle"><a href= "languagelist" class="btnsubmit2">Cancel</a></td>
                          </tr>
                        </form:form>
                        
                      </table></td>
                    </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td height="70" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
        <tr>
          <td height="42" align="left" valign="middle" class="pagelistinggrn">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
var Prelang="";
var PrelangCode="";
function getlangDetail()
{
	Prelang=document.getElementById("lang").value;
	PrelangCode=document.getElementById("langCode").value;
}
function checkLanguageValidation()
{
	var codeLength=document.getElementById("langCode").value;
	var status=true;
	if(document.getElementById("lang").value=="" && document.getElementById("langCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter language and language code";
		status=false;	
	}
	else if(!document.getElementById("lang").value=="" && document.getElementById("langCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Language Code";
		status=false;	
	}
	else if(document.getElementById("lang").value=="" && ! document.getElementById("langCode").value=="" )
	{
		document.getElementById("Error_msg").innerHTML="Please enter Language";
		status=false;	
	}
	if(document.getElementById("langCode").value!="" && codeLength.length!=2)
	{
		document.getElementById("Error_msg").innerHTML="Language code should contain two characters";
		status=false;
	}
	
	return status;
}
function checklanguageNameAvail(lang){
	//alert('before if'+Prelang);
	if(Prelang!=lang)
	{//alert('in if'+Prelang);
		if (window.ActiveXObject) 
	    {          
	     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
	    } 
	    else if (window.XMLHttpRequest) 
	    {         
	        request = new XMLHttpRequest();          
	    } 
		if (request) 
		{
			request.onreadystatechange = handleStateChange; 
			var a=request.open("GET","ValidateLanguage?lang="+lang+"&action=validateLang",true);
			request.send(true);	
	 	}
	}
	
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;						
			document.getElementById('msg_lang').innerHTML = request.responseText;
			var text=request.responseText.trim('');
			if(text.length != 0)
			{
				document.getElementById('lang').value = Prelang;
			}
			request.abort();
			request = null;
}
function checklanguageCodeAvail(langcode){
	if(PrelangCode!=langcode)
	{
		if (window.ActiveXObject) 
	    {          
	     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
	    } 
	    else if (window.XMLHttpRequest) 
	    {         
	        request = new XMLHttpRequest();          
	    } 
		if (request) 
		{
			request.onreadystatechange = handleStateChange1; 
			var a=request.open("GET","ValidateLanguageCode?langCode="+langcode+"&action=CheckLangCode",true);
			request.send(true);	
	 	}
	}
	
	//document.getElementById('preCode').value=langcode;
	return val;
}
function handleStateChange1()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;						
			document.getElementById('msg_langCode').innerHTML = request.responseText;
			var text=request.responseText.trim('');
			if(text.length != 0)
			{
				document.getElementById('langCode').value =PrelangCode;
			}
			request.abort();
			request = null;
}
		
</script>

