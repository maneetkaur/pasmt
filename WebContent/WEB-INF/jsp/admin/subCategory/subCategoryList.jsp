	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     <%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%
	String topMenu = "Admin";
%>   
<%!
    Integer pagerPageNumber=new Integer(0);
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>view Subcategory - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddsurveysubcat">
      <h3><a href="addSubCategory"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:13px">Search Subcategory</h2>
      <form:form modelAttribute="search_sub_category" action="searchSubCategory" method="post">
      <label>Category</label>
      <form:input path="category" style="width:120px;"/>
      <label>SubCategory</label>
      <form:input path="subCategoryType" style="width:120px;"/>
       <label>Language</label>
       <form:select path="languageId"  class="selectdeco" style="width:120px;" >
      <form:option value="NONE" label="--Language--"></form:option>
      <form:options items="${languagelist}" itemLabel="name" itemValue="id"/>
      </form:select>
       <input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>
       </form:form>
       <h3><a href="#">Advanced Search</a></h3>
      </div>
      </div>
      <div class="searchresult"> 
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1135" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td height="42" colspan="5" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="383" align="left" valign="middle"><h2>SubCategory Listing</h2></td>
                  <td width="753">&nbsp;</td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td width="147" height="42" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Sr No </td>
              <td width="233" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Category</td>
              <td width="262" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">SubCategory</td>
              <td width="207" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Status</td>
              <td width="286" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Action</td>
			  </tr>
			  <c:if test="${not empty subCategoryList}">
              <c:set var="pageUrl" value="subCategoryList"/>
           <c:set var="totalRows" value="${requestScope.total_rows}"/>
           <pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">
            <c:forEach items="${subCategoryList}" var="ll" varStatus="counter">  
            <pg:item> 
			<c:choose>
            	<c:when test="${counter.count%2==0}">
            <tr>
              <td align="left" valign="middle" class="listingitem">${counter.count}</td>
              <td align="left" valign="middle" class="listingitem">${ll.category}</td>
              <td align="left" valign="middle" class="listingitem">${ll.subCategoryType}</td>
              <td align="left" valign="middle" class="listingitem">
				<c:choose>
             		<c:when test="${ll.status==1}">
              			<a href="updateSubCategoryStatus?subCategoryTypeId=${ll.subCategoryTypeId}">Active</a>              		</c:when>
               		<c:otherwise>
               			<a href="updateSubCategoryStatus?subCategoryTypeId=${ll.subCategoryTypeId}">Inactive</a>               		</c:otherwise>
               	</c:choose>			</td>
              <td align="left" valign="middle" class="listingitem"><a href="updateSubCategory?subCategoryTypeId=${ll.subCategoryTypeId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
              </tr>
            </c:when>
            <c:otherwise>
            	 <tr>
              <td align="left" valign="middle" class="listingitem2">${counter.count}</td>
              <td align="left" valign="middle" class="listingitem2">${ll.category}</td>
              <td align="left" valign="middle" class="listingitem2">${ll.subCategoryType}</td>
              <td align="left" valign="middle" class="listingitem2">
              <c:choose>
             		<c:when test="${ll.status==1}">
              			<a href="updateSubCategoryStatus?subCategoryTypeId=${ll.subCategoryTypeId}">Active</a>              		</c:when>
               		<c:otherwise>
               			<a href="updateSubCategoryStatus?subCategoryTypeId=${ll.subCategoryTypeId}">Inactive</a>               		</c:otherwise>
               	</c:choose>               	</td>
              <td align="left" valign="middle" class="listingitem2"><a href="updateSubCategory?subCategoryTypeId=${ll.subCategoryTypeId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
              </tr>
            </c:otherwise>
           </c:choose>
            </pg:item>
           </c:forEach>
           <%@include file="../../include/paging_bar.jsp"%>
           </pg:pager>
          </c:if>
             <tr>
              <td colspan="5" align="left" valign="middle" bgcolor="#FFFFFF" class="pagelistinggrn">&nbsp;</td>
            </tr>
           
          </table></td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" ></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>