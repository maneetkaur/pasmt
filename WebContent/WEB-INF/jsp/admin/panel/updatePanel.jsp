<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%> 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Panel - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
          <table style="margin-top:0px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table style="margin-left:10px;" width="1127" border="0" align="left" cellpadding="0" cellspacing="0">
  <td width="1111"><form:form action="editPanel" method="post" modelAttribute="edit">
  <tr>
    <td height="31" align="left" valign="top"><table width="1120" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td height="42" colspan="6" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn"><table width="1116" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="356" align="left" valign="middle"><h2>Update Panel</h2></td>
              <td width="760">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td width="271" align="left" valign="middle" bgcolor="#D2DCBF" class="pagelistinggrn">Panel Name</td>
        <td width="355" align="left" valign="middle" bgcolor="#D2DCBF" class="pagelistinggrn">Company</td>
        <td width="172" align="left" valign="middle" bgcolor="#D2DCBF" class="pagelistinggrn">Culture</td>
        <td width="147" align="left" valign="middle" bgcolor="#D2DCBF" class="pagelistinggrn">Panel Size</td>
        <td width="175" align="left" valign="middle" bgcolor="#D2DCBF" class="pagelistinggrn">Active Panel Size</td>
      </tr>
      <tr>
        <td align="left" valign="middle" class="listingitem"><c:out value="${edit.panelName}" /></td>
        <td align="left" valign="middle" class="listingitem"><c:out value="${edit.company}" /></td>
        <td align="left" valign="middle" class="listingitem"><c:out value="${edit.cultureCode}" /></td>
        <td align="left" valign="middle" class="listingitem">0</td>
        <td align="left" valign="middle" class="listingitem">0</td>
      </tr>
    </table>
      <br/>
        <span  id="msg_lang" style="color:red"></span> </td>
  </tr>
  <tr>
    <td height="31" align="left" valign="middle">&nbsp;</td>
    <br/>
    <div id="Error_msg" style="color: red" align="center"></div>
  </tr>
  <tr>
    <td height="31" align="left" valign="middle"><table width="1120" border="0" align="left" cellpadding="0" cellspacing="0">
      <tr>
        <td width="1142" height="42" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn"><table width="1087" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="1156" align="left" valign="middle" bgcolor="#494c41"><h2>Modules</h2></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="42" align="left" valign="middle" bgcolor="#D2DCBF" class="pagelistinggrn"><table width="200" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="30" align="left" valign="middle">Profile</td>
            <td width="52" height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="1"/></td>
          </tr>
          <tr>
            <td height="30" align="left" valign="middle">Survey</td>
            <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="2"/></td>
          </tr>
          <tr>
            <td height="30" align="left" valign="middle">Reward</td>
            <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="3"/></td>
          </tr>
          <tr>
            <td height="30" align="left" valign="middle">Referral</td>
            <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="4"/></td>
          </tr>
          <tr>
            <td height="30" align="left" valign="middle">Network</td>
            <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="5"/></td>
          </tr>
          <tr>
            <td height="30" align="left" valign="middle">Routing</td>
            <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="6"/></td>
          </tr>
          <tr>
            <td height="30" align="left" valign="middle">Polls</td>
            <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="7"/></td>
          </tr>
        </table></td>
      </tr>
      <c:forEach items="${panelList}" var="ll" varStatus="counter">
        <c:choose>
          <c:when test="${counter.count%2==0}"> </c:when>
          <c:otherwise> </c:otherwise>
        </c:choose>
      </c:forEach>
    </table></td>
    </tr>
  <tr>
    <td height="31" align="left" valign="top" style="padding-top:3px;">&nbsp;</td>
    </tr>
  <tr>
    <td height="88" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit"></td>
        <td width="118" align="center" valign="middle"><a href="panelList" class="btnsubmitLink" >Cancel</a></td>
      </tr>
      </form:form>
      
    </table></td>
  </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
function checkPanelValidation()
{
	var status=true;
	if(document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" && document.getElementById("cultureId").value=="NONE" )
	{
		document.getElementById("Error_msg").innerHTML="Please fill all fields";
		status=false;	
	}	
	else if(document.getElementById("panelName").value=="" && !document.getElementById("companyId").value=="" && document.getElementById("cultureId").value=="NONE" )
	{
		document.getElementById("Error_msg").innerHTML="Please fill Panel name and Country";
		status=false;	
	}
	else if(document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" && !document.getElementById("cultureId").value=="" )
	{	
		document.getElementById("Error_msg").innerHTML="Please fill Panel name and Company";
		status=false;	
	}
	else if(document.getElementById("panelName").value=="" && !document.getElementById("companyId").value=="" && !document.getElementById("cultureId").value=="" )
	{	
		document.getElementById("Error_msg").innerHTML="Please fill Panel name ";
		status=false;	
	}
	else if(!document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" &&  document.getElementById("cultureId").value=="NONE" )
	{	
		document.getElementById("Error_msg").innerHTML="Please select Company and Culture";
		status=false;	
	}
	else if(!document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" && !document.getElementById("cultureId").value=="" )
	{	
		document.getElementById("Error_msg").innerHTML="Please select Company";
		status=false;	
	}
	else if(!document.getElementById("panelName").value=="" && !document.getElementById("companyId").value=="" &&  document.getElementById("cultureId").value=="NONE" )
	{	
		document.getElementById("Error_msg").innerHTML="Please select Culture";
		status=false;	
	}	
	return status;
}


</script>