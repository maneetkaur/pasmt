<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     <%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%
	String topMenu = "Admin";
%>    
<%!
    Integer pagerPageNumber=new Integer(0);
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View Panel - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddpanel">
      <h3><a href="addPanel"></a></h3>
      </div>
      <div class="searchbar"><h2>Search Panel</h2>
      <form:form modelAttribute="search" action="searchPanel" method="post">
      <label>Company </label>
      <form:input path="company"/>
       <label>Country </label>
      <form:input path="countryName"/>
       <input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>
       </form:form>
       <h3><a href="#">Advanced Search</a></h3>
      </div>
      </div>
      <div class="searchresult">
        <table width="1135" border="0" align="left" cellpadding="0" cellspacing="0">
          <tr>
            <td height="42" colspan="8" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn"><table width="1044" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="383" align="left" valign="middle"><h2>Panel Listing</h2></td>
                <td width="753">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="105" height="42" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Sr No </td>
            <td width="230" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Panel Name</td>
            <td width="259" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Company</td>
            <td width="124" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Culture</td>
            <td width="100" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Panel Size</td>
            <td width="145" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Active Panel Size</td>
            <td width="111" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn"> Status </td>
            <td width="61" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Edit</td>
          </tr>
           <c:if test="${not empty panelList}">
              <c:set var="pageUrl" value="panelList"/>
           <c:set var="totalRows" value="${requestScope.total_rows}"/>
           <pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">
          <c:forEach items="${panelList}" var="ll" varStatus="counter">
            <pg:item> 
            <c:choose>
              <c:when test="${counter.count%2==0}">
                <tr>
                  <td align="left" valign="middle" class="listingitem">${counter.count}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.panelName}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.company}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.cultureCode}</td>
                  <td align="left" valign="middle" class="listingitem">0</td>
                  <td align="left" valign="middle" class="listingitem">0</td>
                  <td align="left" valign="middle" class="listingitem"><c:choose>
                      <c:when test="${ll.status==1}"> <a href="updatePanelStatus?panelId=${ll.panelId}">Active</a> </c:when>
                      <c:otherwise> <a href="updatePanelStatus?panelId=${ll.panelId}">Inactive</a> </c:otherwise>
                  </c:choose></td>
                  <td align="left" valign="middle" class="listingitem"><a href="updatePanel?panelId=${ll.panelId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
                </tr>
              </c:when>
              <c:otherwise>
                <tr>
                  <td align="left" valign="middle" class="listingitem2">${counter.count}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.panelName}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.company}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.cultureCode} </td>
                  <td align="left" valign="middle" class="listingitem2">0</td>
                  <td align="left" valign="middle" class="listingitem2">0</td>
                  <td align="left" valign="middle" class="listingitem2"><c:choose>
                      <c:when test="${ll.status==1}"> <a href="updatePanelStatus?panelId=${ll.panelId}">Active</a> </c:when>
                      <c:otherwise> <a href="updatePanelStatus?panelId=${ll.panelId}">Inactive</a> </c:otherwise>
                  </c:choose></td>
                  <td align="left" valign="middle" class="listingitem2"><a href="updatePanel?panelId=${ll.panelId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
              </c:otherwise>
            </c:choose>
          </pg:item>
           </c:forEach>
           <%@include file="../../include/paging_bar.jsp"%>
           </pg:pager>
          </c:if>
          <tr>
            <td colspan="8" align="left" valign="middle" bgcolor="#FFFFFF" class="pagelistinggrn">&nbsp;</td>
          </tr>
          
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
