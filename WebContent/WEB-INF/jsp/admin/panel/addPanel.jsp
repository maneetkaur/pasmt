<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%> 
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Add Panel - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Panel</h2>
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
            <table style="margin-left:10px;" width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="972" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
               <form:form action="AddPanel" method="post" modelAttribute="add">
              <tr>
                <td height="31" align="left" valign="middle">Panel Name <span>*</span></td>
                <td colspan="5" align="left" valign="middle"><form:input path="panelName" class="inptform"  onblur="checkPanelNameAvail(this.value)" />
                            <br/>
                          <span  id="msg_panel" style="color:red"></span> </td>
              </tr>
              <tr>
                <td width="158" height="31" align="left" valign="middle">Company Name<span>*</span></td>
                <br/>
                      <div id="Error_msg" style="color: red" align="center"></div>   
                <td colspan="5" align="left" valign="middle"><form:select path="companyId"  class="inptformSelect" style="width:243px;">
                     <form:option value="NONE" label="--Company--"/>
                     <c:forEach var="coun" items="${companyList}">
                     <form:option value="${coun.id}">${coun.name}</form:option>
                     </c:forEach>
                     </form:select>
                        <br/><span  id="msg_country" style="color:red"></span>    </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Culture <span>*</span></td>
                <td colspan="5" align="left" valign="middle"><form:select path="cultureId" class="inptformSelect" style="width:243px;">
                        <form:option value="NONE" label="--Culture--" />
                        <c:forEach var="item" items="${cultureList}"> 
                        <form:option value="${item.id}">${item.code}</form:option>
                        </c:forEach>
                        </form:select>
                        <br/><span  id="msg_countryCode" style="color:red"></span>  </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:3px;">Modules <span>*</span></td>
                <td colspan="5" align="left" valign="middle"><table width="200" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                   <td height="30" align="left" valign="middle">Profile</td>
                    <td width="52" height="30" align="left" valign="middle">
                    <form:checkbox path="selectModule" value="1"/>
                    </td>
                  </tr>
                  <tr>
                    <td height="30" align="left" valign="middle">Survey</td>
                    <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="2"/></td>
                  </tr>
                  <tr>
                    <td height="30" align="left" valign="middle">Reward</td>
                    <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="3"/></td>
                  </tr>
                  <tr>
                    <td height="30" align="left" valign="middle">Referral</td>
                    <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="4"/></td>
                  </tr>
                  <tr>
                    <td height="30" align="left" valign="middle">Network</td>
                    <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="5"/></td>
                  </tr>
                  <tr>
                    <td height="30" align="left" valign="middle">Routing</td>
                    <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="6"/></td>
                  </tr>
                  <tr>
                    <td height="30" align="left" valign="middle">Polls</td>
                    <td height="30" align="left" valign="middle"><form:checkbox path="selectModule" value="7"/></td>
                  </tr>
                </table></td>
              </tr>              
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onclick="return checkPanelValidation()"></td>
                    <td width="118" align="center" valign="middle"><a href="panelList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                   </form:form>
                </table></td>
                </tr>
            </table>          </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
function checkPanelValidation()
{
	var status=true;
	if(document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" && document.getElementById("cultureId").value=="NONE" )
	{
		document.getElementById("Error_msg").innerHTML="Please fill all fields";
		status=false;	
	}	
	else if(document.getElementById("panelName").value=="" && !document.getElementById("companyId").value=="" && document.getElementById("cultureId").value=="NONE" )
	{
		document.getElementById("Error_msg").innerHTML="Please fill Panel name and Country";
		status=false;	
	}
	else if(document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" && !document.getElementById("cultureId").value=="" )
	{	
		document.getElementById("Error_msg").innerHTML="Please fill Panel name and Company";
		status=false;	
	}
	else if(document.getElementById("panelName").value=="" && !document.getElementById("companyId").value=="" && !document.getElementById("cultureId").value=="" )
	{	
		document.getElementById("Error_msg").innerHTML="Please fill Panel name ";
		status=false;	
	}
	else if(!document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" &&  document.getElementById("cultureId").value=="NONE" )
	{	
		document.getElementById("Error_msg").innerHTML="Please select Company and Culture";
		status=false;	
	}
	else if(!document.getElementById("panelName").value=="" && document.getElementById("companyId").value=="NONE" && !document.getElementById("cultureId").value=="" )
	{	
		document.getElementById("Error_msg").innerHTML="Please select Company";
		status=false;	
	}
	else if(!document.getElementById("panelName").value=="" && !document.getElementById("companyId").value=="" &&  document.getElementById("cultureId").value=="NONE" )
	{	
		document.getElementById("Error_msg").innerHTML="Please select Culture";
		status=false;	
	}	
	return status;
}
function checkPanelNameAvail(panel){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		var a=request.open("GET","ValidatePanelName?panel="+panel+"&action=validatePanel",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;						
			document.getElementById('msg_panel').innerHTML = request.responseText;
			var text=request.responseText.trim('');
			//alert(text.length);
			if(text.length != 0)
			{
				document.getElementById('panelName').value = '';
			}
			request.abort();
			request = null;
}

</script>