<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
 <%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>   
<%
	String topMenu = "Admin";
%> 
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View Culture Question - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddCultureQuestion">
      <h3><a href="addCultureQuestion"></a></h3>
      </div>
      <div class="btncopyCultureQuestion">
      <h3><a href="copyCultureQuestion"></a></h3>
      </div>
      <div class="searchbar" style="width:835px;">
      <h2 style=" width:80px;font-size:12px">Search Question</h2>
      <form:form modelAttribute="search_culture_question" action="searchCultureQuestion" method="post">
      <label>Culture</label>
       <form:select path="cultureId"  class="selectdeco" style=" width:120px;">
      <form:option value="NONE" label="--Culture--"></form:option>
      <form:options items="${cultureList}" itemLabel="code" itemValue="id"/>
      </form:select>
       <label>Subcategory</label>
       <form:select path="categoryId"  class="selectdeco" style=" width:120px;">
      <form:option value="NONE" label="--Subcategory--"></form:option>
      <form:options items="${categoryList}" itemLabel="name" itemValue="id"/>
      </form:select>
      <label>Question Type</label>
      <form:select path="questionTypeId"  class="selectdeco" style=" width:120px;">
      <form:option value="NONE" label="--Question type--"></form:option>
      <form:options items="${questionTypeList}" itemLabel="name" itemValue="id"/>
      </form:select>
       <input type="submit" name="button" id="button" value="Submit" class="btnsearchBar"/>
       </form:form>
      </div>
      </div>
      <div class="searchresult">
        <table width="1135" border="0" align="left" cellpadding="0" cellspacing="0">
          <tr>
            <td height="42" colspan="7" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn"><table width="1044" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="383" align="left" valign="middle"><h2>Question Listing</h2></td>
                <td width="753">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="164" height="42" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Sr No </td>
            <td width="219" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Culture</td>
            <td width="237" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Subcategory</td>
            <td width="207" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Question Type</td>
            <td width="107" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn"> Status </td>
            <td width="107" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Preview</td>
            <td width="94" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Edit</td>
          </tr>
           <c:if test="${not empty cultureQuestionList}">
              <c:set var="pageUrl" value="cultureQuestionView"/>
           <c:set var="totalRows" value="${requestScope.total_rows}"/>
           <pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">
          <c:forEach items="${cultureQuestionList}" var="ll" varStatus="counter">
          <pg:item>
            <c:choose>
              <c:when test="${counter.count%2==0}">
                <tr>
                  <td align="left" valign="middle" class="listingitem">${counter.count}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.cultureCode}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.categoryType}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.questionType}</td>
                  <td align="left" valign="middle" class="listingitem"><c:choose>
                      <c:when test="${ll.status==1}"> <a href="updateCultureQuestionStatus?questionId=${ll.questionId}">Active</a> </c:when>
                      <c:otherwise> <a href="updateCultureQuestionStatus?questionId=${ll.questionId}">Inactive</a> </c:otherwise>
                  </c:choose></td>
                  <td align="left" valign="middle" class="listingitem"><a href="previewCultureQuestion?cultureQuestionId=${ll.questionId}"><img src="images/icon-preview.png" alt="preview" width="20" height="23" border="0" title="preview" /></a></td>
                  <td align="left" valign="middle" class="listingitem"><a href="updateCultureQuestion?cultureQuestionId=${ll.questionId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
                </tr>
              </c:when>
              <c:otherwise>
                <tr> 
                  <td align="left" valign="middle" class="listingitem2">${counter.count}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.cultureCode}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.categoryType}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.questionType} </td>
                  <td align="left" valign="middle" class="listingitem2"><c:choose>
                      <c:when test="${ll.status==1}"> <a href="updateCultureQuestionStatus?questionId=${ll.questionId}">Active</a> </c:when>
                      <c:otherwise> <a href="updateCultureQuestionStatus?questionId=${ll.questionId}">Inactive</a> </c:otherwise>
                  </c:choose></td>
                  <td align="left" valign="middle" class="listingitem2"><a href="previewCultureQuestion?cultureQuestionId=${ll.questionId}"><img src="images/icon-preview.png" alt="preview" width="20" height="23" border="0" title="preview" /></a></td>
                  <td align="left" valign="middle" class="listingitem2"><a href="updateCultureQuestion?cultureQuestionId=${ll.questionId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
              </c:otherwise>
            </c:choose>
          </pg:item>
           </c:forEach>
           <%@include file="../../include/paging_bar.jsp"%>
           </pg:pager>
          </c:if>
          <tr>
            <td colspan="7" align="left" valign="middle" bgcolor="#FFFFFF" class="pagelistinggrn">&nbsp;</td>
          </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
