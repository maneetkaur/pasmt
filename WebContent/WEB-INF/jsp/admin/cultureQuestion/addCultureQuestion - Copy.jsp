<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.admin.cultureQuestion.*"%>
<%
	String topMenu = "Admin";
	CultureQuestion cultureQuestion=new CultureQuestion();
	 if(request.getAttribute("globalQuestionText")!=null)
 	{
 		cultureQuestion=(CultureQuestion)request.getAttribute("globalQuestionText");
 	}
	
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Pasmt</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/admin/cultureQuestionValidation.js"></script>
<script type="text/javascript">

</script>
</head>
<body> 
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Question</h2>
      <table style="margin-top:1px;" width="1138" border="0" cellspacing="0" cellpadding="0">
        
       <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
            <table width="1125" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="872" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <form:form action="submitCultureQuestion" modelAttribute="add_culture_question">
              <tr>
                <td width="253" height="31" align="left" valign="middle">Culture <span>*</span></td>
                <td colspan="5" align="left" valign="middle">
               <form:select path="cultureId" class="inptformSelect" style="width:243px;" onchange="getCategoryList(this.value)">
                        <form:option value="NONE" label="--Culture--" />
                        <form:options items="${cultureList}" itemLabel="code" itemValue="id"/>
               </form:select>
               </td>
              </tr>
              <tr>
                <td  colspan="6" align="left" valign="middle"><div id="category_div" >
                <table width="1125" cellpadding="0" cellspacing="0">
                <tr>
                <td width="253" height="31" align="left" valign="middle">Category <span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
               <!--<form:select path="categoryId" class="inptformSelect" style="width:243px;" onchange="findQuestionType(this.value)"> -->
               <select id="categoryId" name="categoryId" class="inptformSelect" style="width:243px;" onchange= "getQuestiontype(this.value)">
                        <option value="NONE">--Category--</option>
                       <!-- <form:options items="${categoryTypeList}" itemLabel="name" itemValue="id"/> -->
               <!-- </form:select> -->
               </select>
               </td>
              </tr>
                </table>
                </div></td>
                </tr>
              
             
              <tr>
                <td  colspan="6" align="left" valign="middle"><div style="display:block">
                <table width="1125">
              <tr>
                <td width="249" height="31" align="left" valign="middle">Question Type <span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
                <form:select path="questionTypeId" class="inptformSelect" style="width:243px;" onchange="findQuestionText(this.value);">
                	<form:option value="NONE" label="--Question Type--"></form:option>
                	<!-- <form:options items="${questionTypeList}" itemLabel="name" itemValue="id"/> -->
                </form:select>               </td>
              </tr>
                </table>
                </div></td>
                </tr>
              
              <tr>
                <td height="31" colspan="6" align="left" valign="middle">
                <div style="display: none;" id="question_div">
		   <input type="hidden" id="questionId" value="14" name="questionId"/>
                    </div>
                    
                    </td>
                    </tr>
                    <tr>
                      <td height="31" colspan="2">
                      <div style="display: NONE" id="answer_div">
                      <table width="1125" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="253" height="31">Global Answer</td>
                          <td width="872">Culture Answer</td>
                          <td width="4">&nbsp;</td>
                        </tr>
                        <tr>
                        <td colspan="2">
                          <div id="global_answer" style="display: block">
                          
                          </div>                          </td>
                        </tr>
                  </table>
                </div>                </td>
                </tr>
               
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><div id="sub_Question" style="display: none;" >
                
                  <table width="801" border="0" cellspacing="0" cellpadding="0">
                 
                  <tr>
                      <td colspan="3"><strong>Add Sub Questions <span>*</span></strong></td>
                      </tr> 
                    
                   
                    <tr>
                <td height="19" colspan="6" align="left" valign="middle"><table width="794" border="0" cellspacing="0" cellpadding="0">
                 <tbody id="subQuestionTable">
                  <tr>
                    <td width="187" align="left" valign="middle">Question <span>*</span></td>
                    <td width="264" align="left" valign="middle">  <form:input path="" class="inptform" id="question0"/>     </td>
                    <td width="343" align="left" valign="baseline">&nbsp;</td>
                  </tr>     
                  </tbody>
                </table></td>
                </tr>
                    <tr>
                      <td width="441" height="19" align="left" valign="middle"><input type="button" name="addMore" id="addMore" value="Add More +" class="btnsubmit"></td>
                      <td width="360" colspan="5" align="left" valign="middle">&nbsp;</td>
                    </tr>
                  </table>
                </div></td>
                </tr>
              
               <tr>
                <td height="31" colspan="6" align="left" valign="middle"><div id="parent_question" style="display: none; padding-top:12px;">
                  <table width="645" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="3"><strong>Select Parent Question </strong></td>
                    </tr>
                    <tr>
                      <td width="190">Select Question <span>*</span> </td>
                      <td width="455" colspan="2"><div id="question_list"></div></td>
                    </tr>
                    <tr>
                      <td>Select Answer <span>*</span></td>
                      <td colspan="2"><div id="answer_list"></div></td>
                    </tr>
                    <tr>
                <td height="19" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
                  </table>
                </div></td>
                </tr>
              
              
                   
               
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return checkQuestionValidation()"></td>
                    <td width="118" align="center" valign="middle"><input type="submit" name="Submit3" id="Submit3" value="Clear" class="btnsubmit"></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              </form:form>
            </table>          </td>
        </tr>
        
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">

</script>
