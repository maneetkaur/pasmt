<%@ page contentType="text/html; charset=UTF-8" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.admin.cultureQuestion.*"%>
<%
	String topMenu = "Admin";
	
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Copy Culture Info - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/admin/cultureQuestionValidation.js"></script>
<!-- checkbox tree include starts -->
  <script src="resources/checkBoxTree/js/jquery.js" type="text/javascript"></script>
  <script src="resources/checkBoxTree/js/ui.core.js" type="text/javascript"></script>
  <script src="resources/checkBoxTree/js/jquery.cookie.js" type="text/javascript"></script>
  <link href="resources/checkBoxTree/css/ui.dynatree.css" rel="stylesheet" type="text/css">
  <script src="resources/checkBoxTree/js/jquery.dynatree.min.js" type="text/javascript"></script>
<!-- checkbox tree include ends -->
<script type="text/javascript">
var questionConflict = jQuery.noConflict();
    jQuery(function(){
      questionConflict("#categoryTree").dynatree({
        //Tree parameters
        persist: false,
        checkbox: true,
        selectMode: 3,
        activeVisible: true,
  
        //Un/check real checkboxes recursively after selection
        onSelect: function(select, dtnode) {
		
          dtnode.visit(function(dtnode){
            questionConflict("#chb-"+dtnode.data.key).attr("checked",select);
            },null,true);
        },
        //Hack to prevent appearing of checkbox when node is expanded/collapsed
        onExpand: function(select, dtnode) {
          questionConflict("#chb-"+dtnode.data.key).attr("checked",dtnode.isSelected()).addClass("hidden");
        }
      });
      //Hide real checkboxes
      questionConflict("#categoryTree :checkbox").addClass("hidden");
      //Update real checkboxes according to selections
      questionConflict.map(questionConflict("#categoryTree").dynatree("getTree").getSelectedNodes(),
        function(dtnode){
          questionConflict("#chb-"+dtnode.data.key).attr("checked",true);
          dtnode.activate();
        });
    });
  var selectedCategories="", selectedsubCategories="",temp;
  function getSelectedCategories(divNameCat){
	  selectedCategories="";
	  selectedsubCategories="";
	  questionConflict.map(questionConflict("#" + divNameCat).dynatree("getTree").getSelectedNodes(),
		function(dtnode){		  
			temp = dtnode.data.key;		  		  
		  	if(temp.substring(0,4)=="cat-"){		  		 
		  		 selectedCategories = selectedCategories + temp.substring(4) + ",";
		  	}
		  	else{
		  		selectedsubCategories = selectedsubCategories + temp + ",";
		  	}
		});
	  if(divNameCat == "categoryTree"){
		  document.getElementById("subCategory").value = selectedsubCategories;
		  //document.getElementById("completeCategory").value = selectedCategories;
	  }
	}
  </script>
  
  <link rel="stylesheet" href="css/style.css" type="text/css" />
  <style type="text/css">
  	div.categoryTreeDiv{
  		max-height: 105px; overflow:auto; width: 242px; overflow-x: hidden;
  	}
  </style>

</head>
<body> 
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Question</h2>
      <form:form  id="myForm" action="saveCultureInfo" modelAttribute="copy_culture_question" >
      <table style="margin-top:1px;" width="1138" border="0" cellspacing="0" cellpadding="0">
       <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
            <table width="1125" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td width="253" align="left" valign="middle">&nbsp;</td>
                <td width="872" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
               
             <tr>
                <td width="253" height="31" align="left" valign="middle">From Culture<span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
               <form:select path="cultureId" class="inptformSelect" style="width:243px;" id="fromCulture" onchange="getCategoryValues(this.value)">
              		<form:option value="NONE">--Culture--</form:option>
              		<form:options items="${cultureList}" itemLabel="code" itemValue="id"/>
               </form:select>
               </td>
              </tr>
               <tr>
                <td height="20" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="subCategory">Category</form:label>
                	<form:hidden path="subCategory" id="subCategory"/>
                	
                </td>
                <td colspan="5" align="left" valign="middle">                  
                  <div id="categoryTree" class="categoryTreeDiv">
                  	<ul>
                  		<c:forEach items="${categoryList}" var="category">                  						    
					      <li id="cat-${category.categoryTypeId}">${category.categoryName}
					        <ul>
					       	  <c:forEach items="${category.subCategoryList}" var="subCategory">					       	  
					       	  	<li id="${subCategory.subCategoryId}">${subCategory.subCategory}	
					       	  	</li>					       	  
					       	  </c:forEach>
					        </ul>
					      </li>					      					      					      					    					  
                  		</c:forEach>
                  	</ul>
                  </div>
                 </td>
              </tr>
                <tr></tr>
              <tr></tr><tr>
                <td height="20" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>

              <tr>
                <td height="31" align="left" valign="middle">To Culture<span>*</span></td>
                <td colspan="5" align="left" valign="middle"><form:select path="destinationCultureId" class="inptformSelect" style="width:243px;" id="toCulture">
              		<form:option value="NONE">--Culture--</form:option>
              		<form:options items="${cultureList}" itemLabel="code" itemValue="id"/>
               </form:select>
                </td>
              </tr>
             
              
              
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">
                <table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="getSelectedCategories('categoryTree'); return checkCultureValue();"></td>
                    <td width="118" align="center" valign="middle"><a href="cultureQuestionView" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
            </table>          </td>
        </tr>
        
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
       </form:form>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
function getCategoryValues(cultureId){
	window.location.href="copyCultureQuestion?cultureId="+cultureId;
}
</script>