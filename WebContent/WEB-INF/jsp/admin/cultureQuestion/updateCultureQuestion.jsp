<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.admin.cultureQuestion.*"%>
<%
	String topMenu = "Admin";
	
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Culture Question - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/admin/cultureQuestionValidation.js"></script>
<script type="text/javascript">
var sizeOfList='${edit_culture_question.cultureAnswer.size()}';	;
var subquesListSize='${edit_culture_question.subQuestionList.size()}';
</script>
</head>
<body> 
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Question</h2>
      <form:form  id="myForm" action="editCultureQuestion" modelAttribute="edit_culture_question">
      <table style="margin-top:1px;" width="1138" border="0" cellspacing="0" cellpadding="0">
       <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
            <table width="1125" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="872" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
               <tr>
                <td width="253" height="31" align="left" valign="middle">Culture <span>*</span></td>
                <td colspan="5" align="left" valign="middle">
                <form:select path="cultureId" class="inptformSelect" style="width:243px;" disabled="true">
                        <form:options items="${cultureList}" itemLabel="code" itemValue="id"/>
               </form:select>
               </td>
              </tr>
              <tr>
                <td  colspan="6" align="left" valign="middle"><div id="category_div" >
                <table width="1125" cellpadding="0" cellspacing="0">
                <tr>
                <td width="253" height="31" align="left" valign="middle">Subcategory <span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
                <form:input path="categoryType" class="inptform" disabled="true"/>
               </td>
              </tr>
                </table>
                </div></td>
                </tr>
              <tr>
                <td  colspan="6" align="left" valign="middle"><div style="display:block">
                <table width="1125">
              <tr>
                <td width="249" height="31" align="left" valign="middle">Question Type <span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
                 <form:input path="questionType" class="inptform" disabled="true"/>
                </td>
              </tr>
                </table>
                </div></td>
                </tr>
                <tr>
  <td width="249" height="31" align="left" valign="middle">Registration Page Status <span>*</span>
              </td>             
                <td width="864" colspan="5" align="left" valign="middle">
                <form:select path="regEnable" class="inptformSelect" style="width:243px;"  >
                <form:option value="NONE" label="Select Status"></form:option>
              
                
                <form:option value="1" label="OB"></form:option>
                <form:option value="2" label="OSBT"></form:option>
				<form:option value="3" label="OB & OSBT"></form:option>                
                </form:select>
                </td></tr>                
              <tr>
                <td height="31" colspan="6" align="left" valign="middle">
                <div style="display: block;margin-top:10px;" id="question_div">
                <table width="1000">
                 <tr><td width="253" style="color:#494949;"><strong>Global Question Text</strong></td>
                     <td width="747" style="color:#494949;"><strong>Culture Question Text</strong></td>
                 </tr>
                <tr><td width="248" height="31" align="left" valign="middle">
                <input type="hidden" id="questionId" name="QuestionId" class="inptform"/>
                 <form:input path="question" class="inptform" disabled="true" /> 
		  		</td>
                 <td width="742" height="31" align="left" valign="middle">
                 <form:input path="cultureQuestion" class="inptform" />
                </td>
                 </tr>
                 </table>
                    </div>
                    </td>
                    </tr>
                  <c:if test="${!fn:contains(edit_culture_question.cultureQues.quesType,'inputBox')}">
                    <tr>
                      <td height="31" colspan="2">
                      <div style="display: block;margin-top:10px;" id="answer_div">
                      <table width="1000" border="0" cellspacing="0" cellpadding="0">
                        <tr><td width="253" style="color:#494949;"><strong>Global Answer</strong></td>
                        <td width="747" style="color:#494949;"><strong>Culture Answer</strong></td>
                        </tr>
                        <c:forEach items="${edit_culture_question.cultureAnswer}" varStatus="ans">	
                       <tr><td height="31" width="248">
                       <form:input path="cultureAnswer[${ans.index}].name" class="inptform" disabled="true" />
                       <form:hidden path="cultureAnswer[${ans.index}].id" class="inptform"  id="answer${ans.index}"/>
                       </td>
						<td height="31" width="742">
                        <form:input path="cultureAnswer[${ans.index}].code" class="inptform" id="cultureAnswer${ans.index}" />
                        </td>
						</tr>
						</c:forEach>
                        <form:hidden path="cultureAnsIdList"/>
                  </table>
                </div>                </td>
                </tr>
              </c:if>
             
              <c:if test="$!fn:contains(edit_culture_question.cultureQues.quesType,'grid') || fn:contains(edit_culture_question.cultureQues.quesType,'both')}">
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><div id="sub_Question" style="display: block;margin-top:10px;" >
                
                  <table width="1000" border="0" cellspacing="0" cellpadding="0">
                 <tr><td width="252" style="color:#494949;"><strong>Global Sub Question Text </strong></td>
                 <td width="748" style="color:#494949;"><strong>Culture Sub Question Text </strong></td>
                 </tr>		
                  <c:forEach items="${edit_culture_question.subQuestionList}" varStatus="ans">	
                       <tr><td height="31" td width="248">
                       <form:input path="subQuestionList[${ans.index}].name" class="inptform" disabled="true" />
                       <form:hidden path="subQuestionList[${ans.index}].id" class="inptform" id="subques${ans.index}"/>
                       </td>
						<td height="31" width="742" >
                        <form:input path="subQuestionList[${ans.index}].code" class="inptform" id="subQuestion${ans.index}" /></td>
						</tr>
						</c:forEach>
                        <form:hidden path="cultureSubQuesIdList"/>
                  </table>
                </div></td>
                </tr>
              </c:if>
              <c:if test="${!fn:contains(edit_culture_question.cultureQues.quesType,'multipleChild') || !fn:contains(edit_culture_question.cultureQues.quesType,'both') || !fn:contains(edit_culture_question.cultureQues.quesType,'child')}">
               <tr>
                <td height="31" colspan="6" align="left" valign="middle"><div id="parent_question" style="display: none; padding-top:12px;">
                  <table width="645" border="0" cellspacing="0" cellpadding="0">
                    <tr><td>
                    <input type="hidden" id="parentQuestionId" name="parentQuestionId" class="inptform"/>
                    <input type="hidden" id="parentAnswerId" name="parentAnswerId" class="inptform"/>
                    </td></tr>
                  </table>
                </div></td>
                </tr>
                </c:if>
              	<tr>
                <td height="88" colspan="6" align="left" valign="middle">
                <table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="checkRowUpdation();checksubQuesRowUpdation();return checkQuestionValidation()"></td>
                    <td width="118" align="center" valign="middle"><a href="cultureQuestionView" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
             
             
            </table>          </td>
        </tr>
        
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
       </form:form>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
</script>