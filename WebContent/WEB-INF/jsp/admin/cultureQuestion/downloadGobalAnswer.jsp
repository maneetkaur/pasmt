<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-Disposition", "attachment; filename=\"Global_Answer.xls\"");%>
<table>
<c:forEach items="${globalAnswers}" var="list" varStatus="status">
<tr>
<td>${list.name}</td>
</tr>
</c:forEach>
</table>
