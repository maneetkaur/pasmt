<%@ page contentType="text/html; charset=UTF-8" %>	
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.admin.cultureQuestion.*"%>
<%
	String topMenu = "Admin";
	
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Add Culture Question - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/admin/cultureQuestionValidation.js"></script>

</head>
<body> 
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Question</h2>
      <form:form  id="myForm" action="submitCultureQuestion" modelAttribute="add_culture_question" enctype="multipart/form-data">
      <table style="margin-top:1px;" width="1138" border="0" cellspacing="0" cellpadding="0">
       <tr>
           <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
            <table width="1125" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="872" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
               <tr>
                <td width="253" height="31" align="left" valign="middle">Culture <span>*</span></td>
                <td colspan="5" align="left" valign="middle">
               <form:select path="cultureId" class="inptformSelect" style="width:243px;" onchange="getCategoryList(this.value)">
                        <form:option value="NONE" label="--Culture--" />
                        <form:options items="${cultureList}" itemLabel="code" itemValue="id"/>
               </form:select>
               </td>
              </tr>
              <tr>
                <td  colspan="6" align="left" valign="middle"><div id="category_div" >
                <table width="1125" cellpadding="0" cellspacing="0">
                <tr>
                <td width="253" height="31" align="left" valign="middle">Subcategory <span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
               <!--<form:select path="categoryId" class="inptformSelect" style="width:243px;" onchange="findQuestionType(this.value)"> -->
               <select id="categoryId" name="categoryId" class="inptformSelect" style="width:243px;" onchange= "getQuestiontype(this.value)">
                        <option value="NONE">--Subcategory--</option>
                       <!-- <form:options items="${categoryTypeList}" itemLabel="name" itemValue="id"/> -->
               <!-- </form:select> -->
               </select>
               </td>
              </tr>
                </table>
                </div></td>
                </tr>
              <tr>
                <td  colspan="6" align="left" valign="middle"><div style="display:block">
                <table width="1125">
              <tr>
                <td width="249" height="31" align="left" valign="middle">Question Type <span>*</span></td>
                <td width="864" colspan="5" align="left" valign="middle">
                <form:select path="questionTypeId" class="inptformSelect" style="width:243px;" onchange="findQuestionText(this.value);">
                	<form:option value="NONE" label="--Question Type--"></form:option>
                	<!-- <form:options items="${questionTypeList}" itemLabel="name" itemValue="id"/> -->
                </form:select>               </td>
              </tr>
              <tr>
              <td width="249" height="31" align="left" valign="middle">Registration Page Status <span>*</span>
              </td>             
                <td width="864" colspan="5" align="left" valign="middle">
                <form:select path="regEnable" class="inptformSelect" style="width:243px;"  >
                <form:option value="NONE" label="Select Status"></form:option>
                <form:option value="1" label="OB"></form:option>
                <form:option value="2" label="OSBT"></form:option>
				<form:option value="3" label="OB & OSBT"></form:option>                
                </form:select>
                </td></tr>
                </table>
                </div></td>
                </tr>
              
              <tr>
                <td height="31" colspan="6" align="left" valign="middle">
                <div style="display: none;margin-top:10px;" id="question_div">
                <table width="1000">
                 <tr><td width="253" style="color:#494949;"><strong>Global Question Text</strong></td>
                     <td width="747" style="color:#494949;"><strong>Culture Question Text</strong></td>
                 </tr>
                <tr><td width="248" height="31" align="left" valign="middle">
                <!-- <input type="hidden" id="questionId" name="QuestionId" class="inptform"/>-->
                <form:hidden path="questionId" class="inptform" id="questionId"/> 
                 <input type="text" id="question"  name="question" class="inptform" disabled="disabled"/></td>
                 <td width="742" height="31" align="left" valign="middle"><input type="text" id="cultureQuestion" name="cultureQuestion" class="inptform"/></td>
                 </tr>
                 </table>
                    </div>
                    </td>
                    </tr>
                    <tr>
                <td height="31" colspan="6" align="left" valign="middle">
                <div style="display: none;margin-top:10px;" id="subquestion_div">
                <table width="1000">
                 <tr><td width="253" style="color:#494949;"><strong>Global Vertical Child Text</strong></td>
                     <td width="747" style="color:#494949;"><strong>Culture Vertical Child Text</strong></td>
                 </tr>
                <tr><td width="248" height="31" align="left" valign="middle">
                <input type="text" id="globalSubQuesText"  name="globalSubQuesText" class="inptform" disabled="disabled"/></td>
                 <td width="742" height="31" align="left" valign="middle"><input type="text" id="ciltureSubQuesText" name="ciltureSubQuesText" class="inptform"/></td>
                 </tr>
                 </table>
                    </div>
                    </td>
                    </tr>
                    <tr>
                <td height="31" colspan="6" align="left" valign="middle"><div id="sub_Question" style="display: none;margin-top:10px;" >
                
                  <table width="1000" border="0" cellspacing="0" cellpadding="0">
                 <tr><td width="252" style="color:#494949;"><strong>Global Sub Question Text </strong></td>
                 <td width="748" style="color:#494949;"><strong>Culture Sub Question Text </strong></td>
                 </tr>		
                  <tr>
                <td height="19" colspan="2" align="left" valign="middle">
                <table  border="0" cellspacing="0" cellpadding="0">
                 <tbody id="subQuestionTable">
                  </tbody>
                </table>
                </td>
                </tr>
                  </table>
                </div></td>
                </tr>
                     <tr>
						<td height="31" colspan="2">
                        <div id="answer_type" style="display:none;">
                         <table width="1000" border="0" cellspacing="0" cellpadding="0">
                        <tr><td width="254">Answer Upload Method</td>
						<td width="746">
						  <form:select path="answerTypeId" class="inptformSelect" style="width:243px;" onchange="getAnswerType()">
							<form:option value="NONE" label="--Answer type--"></form:option>
							<form:option value="1">Import Answer</form:option>
							<form:option value="2">Custom</form:option>
						  </form:select>						</td>
					  </tr>
                      </table>
                      </div>
                      </td>
                      </tr>
                    <tr>
                      <td height="31" colspan="2">
                      <div style="display: none;margin-top:10px;" id="answer_div">
                      <table width="1000" border="0" cellspacing="0" cellpadding="0">
                        <tr><td width="253" style="color:#494949;"><strong>Global Answer</strong></td>
                        <td width="747" style="color:#494949;"><strong>Culture Answer</strong></td>
                        </tr>	
                        <tr>
                        <td colspan="2">
                          <div id="global_answer" style="display: block">
                          <table>
                          <tbody id="culture_answer">
						</tbody>
                          </table>
                          </div>    
                            </td>
                        </tr>
                  </table>
                </div>                </td>
                </tr>
                 <tr>
                      <td height="31" colspan="2">
                      <div style="display: none;" id="import_answer">
                      <table width="1000" border="0" cellspacing="0" cellpadding="0">
                        <tr><td width="253" style="color:#494949;"><strong>Global Answer</strong></td>
                        </tr>	
                        <tr>
                        <td colspan="2">
                         <table>
                          <tbody id="culture_import">
						</tbody>
                          </table>
                         </td>
                        </tr>
                        <tr><td height="31" td width="248">Download global answer</td>
	   					<td width="742"><input type="button" id="download" value="Download" class="btnsubmitLink" onClick="findGlobalAnswer();"/>
                       	</td></td></tr>
                        <tr><td height="31" td width="248">Upload Only CSV File</td>
						<td width="742"><input type="file" id="Answerfile" name="Answerfile"/></td></tr>
                  </table>
                </div>                </td>
                </tr>
               <tr>
                <td height="31" colspan="6" align="left" valign="middle"><div id="parent_question" style="display: none; padding-top:12px;">
                  <table width="645" border="0" cellspacing="0" cellpadding="0">
                    <tr><td>
                    <input type="hidden" id="parentQuestionId" name="parentQuestionId" class="inptform"/>
                    <input type="hidden" id="parentAnswerId" name="parentAnswerId" class="inptform"/>
                    </td></tr>
                  </table>
                </div></td>
                </tr>
               
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">
                <table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return checkQuestionValidation()"></td>
                    <td width="118" align="center" valign="middle"><a href="cultureQuestionView" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
             
             
          </tr>
        
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        
      </table>
      </td></table>
       </form:form>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
</script>