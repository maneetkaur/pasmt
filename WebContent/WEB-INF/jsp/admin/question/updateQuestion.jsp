 	<%@page import="com.admin.globalQuestion.GlobalQuestion"%>
	<%@page import="java.util.List"%>
	<%@page import="java.util.ArrayList"%>
	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	    pageEncoding="ISO-8859-1"%>
	<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%
		String topMenu = "Admin";
		
	%>  
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Edit Global Question - PASMT v2.0</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noindex, nofollow" />
	<meta name="pasmt" content="Codrops" />
	<!--[if IE]>
	<script src="js/html5.js"></script>
	<![endif]-->
	 <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.13.custom.css">
<link rel="stylesheet" type="text/css" href="css/ui.dropdownchecklist.themeroller.css">

<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript">
	var sizeOfList='${edit_global_question.answerList.size()}';
	var i = listSize;
	var btn_close='<td width="113" align="left" valign="middle" id="remove" class="btn-remove">'+
	    		  '<img src="images/btncross.png" width="18" height="17" border="0"></td>';
	var btn_close_child='<tr><td width="198" align="left" valign="middle" id="removeChild" class="btn-remove">'+
    		  '<img src="images/btncross.png" width="18" height="17" border="0"></td></tr>';
	var listSize='${edit_global_question.subQuestionList.size()}';
	var j=sizeOfList;
	var row=1;
	var sizeOfChildList='${edit_global_question.childQuestionList.size()}';
	var k=sizeOfChildList;
	</script>
	</head>
	<body>
	<!--Header Start-->
	<%@include file="../../include/header.jsp"%>
	<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
	<!--Header End-->
	<!--Middle Box Start-->
	<div class="middleboxcontainer">
	  <div class="middlebox">
	    <!--left Navigation Start-->
	     <%@include file="../../include/left.jsp"%>
	    <!--left Navigation End-->
	    <div class="rightbox">
	      <div class="searchresult">
	      <h2> Update Question</h2>
	      <form:form action="editQuestion" method="post" modelAttribute="edit_global_question" enctype="multipart/form-data">
		  <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
	       <tr>
	          <td height="37" al	ign="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
	           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
	              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
	            </tr>
	           </table>          </td>
	        </tr>
	        <tr>
	          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >           
	            <table style="margin-left:10px;" width="1087" border="0" align="right" cellpadding="0" cellspacing="0">
	              <tr>
	                <td width="189" align="left" valign="middle">&nbsp;</td>
	                <td width="941" colspan="5" align="left" valign="middle">&nbsp;</td>
	              </tr>   
	              <tr>
	                <td height="31" align="left" valign="middle">Question Name <span>*</span></td>
	                <td colspan="5" align="left" valign="middle">
					  <div id="question_type_list" >
						
						<form:select path="typeId" class="inptformSelect" style="width:243px;"  disabled="true">
						<form:option value="NONE" label="--QuestionType--"></form:option>
						<form:options items="${QuestionTypeList}" itemLabel="questionTypeHeading" itemValue="questionTypeId"/>
						</form:select>
					
					  </div>               </td>
	              </tr>
	           
	              <tr>
	                <td height="31" align="left" valign="middle">Question Text<span>*</span></td>
	                <td colspan="5" align="left" valign="middle">
	                <form:input path="question" class="inptform" />               </td>
	              </tr>
	             
	              <tr>
	                <td height="2" colspan="6" align="left" valign="middle">
					  <div id="sub_Question" style="display: none;" >                
						<table width="801" border="0" cellspacing="0" cellpadding="0">                 
						  <tr>
							<td colspan="3"><strong>Add Sub Questions <span>*</span></strong></td>
	                      </tr>                     
	                   
	                      <tr>
							<td height="19" colspan="6" align="left" valign="middle">
							  <table width="794" border="0" cellspacing="0" cellpadding="0">
								<tbody id="subQuestionTable">
								<c:forEach items="${edit_global_question.subQuestionList }" varStatus="st">
								  <tr>
									<td width="187" align="left" valign="middle">Question <span>*</span></td>
									<td width="264" align="left" valign="middle"><form:input path="subQuestionList[${st.index}].subQuestion" class="inptform" id="question0"/>
	                                 <form:hidden path="subQuestionList[${st.index}].subquestionId" id="ids${st.index}"/> 		     </td>
									<td width="343" align="left" valign="baseline">&nbsp;</td>
								  </tr>  
								  </c:forEach> 
	                               <form:hidden path="listSize"/>
	                  			   <form:hidden path="subquestionIdList"/>  
								</tbody>
							  </table>						</td>
						  </tr>
	                      <tr>
							<td width="441" height="19" align="left" valign="middle"><input type="button" name="addMore" id="addMore" value="Add More +" class="btnsubmit"></td>
							<td width="360" colspan="5" align="left" valign="middle">&nbsp;</td>
	                      </tr>
						</table>
	                  </div>				</td>
	              </tr>
	              
	              <tr>
	                <td height="2" colspan="6" align="left" valign="middle">
					  <div id="parent_question" style="display: none; padding-top:12px;">
						<table width="645" border="0" cellspacing="0" cellpadding="0">
	                      <tr>
							<td colspan="3"><strong>Select Parent Question </strong></td>
	                      </tr>
						  <tr>
							<td width="189">Select Question <span>*</span> </td>
							<td width="455" colspan="2">
							  <div id="question_list">
							  <form:select path="parent_question_id" class="inptformSelect" style="width:243px;" onChange="findAnswerList(this.value)">
							  	<form:option value="NONE" label="--Parent Question--"></form:option>
							  	<form:options items="${parentQuestionList}" itemLabel="questionTypeHeading" itemValue="questionTypeId"/>
							  </form:select>
							  </div>						</td>
	                      </tr>
						  <tr>
							<td>Select Answer <span>*</span></td>
							<td colspan="2"><div id="answer_list">
							<select name="parentAnswersList" id="parentAnswersList" multiple="true" style="width:243px;">
							<option value="NONE">--Answer--</option>
							<c:forEach items="${parentGlobalAnswer}" var="list">
							<option value="${list.id}" <c:if test="${not empty parentAnswerMap[list.id]}">selected</c:if> >${list.code}</option>
							</c:forEach>
							</select>
							</div></td>
	                      </tr>
	                      <tr>
							<td height="19" align="left" valign="middle">&nbsp;</td>
							<td colspan="5" align="left" valign="middle">&nbsp;</td>
						  </tr>
						</table>
					  </div>				</td>
	              </tr>
	               <tr>
	                <td height="31" colspan="6" align="left" valign="top">
						<table  width="1046" border="0" cellspacing="0" cellpadding="0" id="answer_div">
						  <tr>
							<td height="19" colspan="6" align="left" valign="middle">
							  <table width="900" border="0" cellspacing="0" cellpadding="0" id="answerTable">
								<tbody >
	                            <tr>
	               	 		<td width="190">Sort Answer by <span>*</span></td>
	               			 <td width="364"><form:select path="sortingId" class="inptformSelect" style="width:243px;">
	                			<form:option value="NONE" label="--Sorting type--"></form:option>
	               			    <form:option value="0">Alphabetical order</form:option>
	               			    <form:option value="1">Numeric order(Ascending)</form:option>
               			    	<form:option value="2">Numeric order(Descending)</form:option>
                            	<form:option value="3">Insertion order</form:option>
	                			</form:select></td>
	                </tr>         
	                              <c:forEach items="${edit_global_question.answerList}" varStatus="st">
								  <tr>
									<td width="181" align="left" valign="middle">Answer ${st.index+1}</td>
									<td width="364" align="left" valign="middle">
										
										<form:input path="answerList[${st.index}].answer" class="inptform" id="anwser${st.index}"/>	
	                                    <form:hidden path="answerList[${st.index}].answerId" id="ids${st.index}"/> 						</td>
									<td width="355" align="left" valign="middle">&nbsp;</td>
								  </tr>
	                               </c:forEach>
	                               <form:hidden path="sizeOfList"/>
	                  			   <form:hidden path="anwserIdList"/>
								</tbody>
							  </table>						</td>
						  </tr>
	                       <tr>
							<td colspan="3"><input type="button" name="Submit" id="Add" value="Add More +" class="btnsubmit"></td>
	                      </tr>
	                      <tr>
	                        <td colspan="2" align="left" valign="middle"><table width="670" border="0" cellspacing="0" cellpadding="0">
	                          <tr>
	                            <td colspan="7" align="left" valign="middle">&nbsp;</td>
	                            </tr>
	                          <tr>
	                            <td width="187" height="19" align="left" valign="middle">Additional Answer Option</td>
	                            <td width="31" align="left" valign="middle"><form:checkbox path="extra_ans_option" value="0"/></td>
	                            <td width="148" align="left" valign="middle">None of the Above</td>
	                            <td width="30" align="left" valign="middle"><form:checkbox path="extra_ans_option" value="1"/></td>
	                            <td width="72" align="left" valign="middle">All </td>
	                            <td width="34" align="left" valign="middle"><form:checkbox path="extra_ans_option" value="2"/></td>
	                            <td width="168" align="left" valign="middle">Others</td>
	                          </tr>
	                        </table></td>
	                        </tr>
	                     
	                      <tr>
							<td colspan="3">&nbsp;</td>
	                      </tr>
						</table>
	                				</td>
	              </tr>
	             <tr>
				<td  height="31" colspan="6" align="left" valign="middle">
				  <div id="anwser_div2" style="display: none;">
					<table width="700" border="0" cellspacing="0" cellpadding="0">
					  <tr>	
               	 		<td width="197">Parent Question <span>*</span></td>
               			 <td width="844">
               			  <form:select path="parent_question_id" class="inptformSelect" style="width:243px;" onChange="findAnswerList(this.value)">
							  	<form:option value="NONE" label="--Parent Question--"></form:option>
							  	<form:options items="${parentQuestionList}" itemLabel="questionTypeHeading" itemValue="questionTypeId"/>
							  </form:select>
                          </td>
                </tr>
                <tr>
                <td width="197">Max rows for answer <span>*</span></td>
                 <td width="844">
                 <form:input path="marRowForChild" class="inptform"/>                 </td>
                 </tr>
                  <tr>
                <td width="197">Child question<span>*</span></td>
                 <td width="844">
                 <form:input path="childVerticalQuestion" class="inptform"/>                 </td>
                 </tr>
               
                 <tr>
					<td colspan="3"><strong>Add child Answers <span>*</span></strong></td>
                  </tr>                     
				 
                    <tr>
                      <td height="19" colspan="6" align="left" valign="middle"><table width="1041" border="0" cellspacing="0" cellpadding="0">
                        <tbody id="childTable">
                        
             <tr>
                          <td>
                          <c:forEach items="${edit_global_question.childQuestionList}" varStatus="st" var="childQuestion">
                          
                          <div id="${st.index}"><table>	
                           
                            <tr>
                              <td width="191" align="left" valign="middle">Child answer${st.index+1}<span>*</span></td>
                              <td width="12" colspan="2" align="left" valign="middle"><form:input path="childQuestionList[${st.index}].childQuestion" class="inptform" id="childQuestion${st.index}"/></td>
                            </tr>
                            <c:forEach items="${childQuestion.childAnswerList}" varStatus="stChild">
                            <tr>
                              <td width="191" align="left" valign="middle">Child answer options${st.index+1}-${stChild.index+1}<span>*</span></td>
                              <td width="12" colspan="2" align="left" valign="middle"><form:input path="childQuestionList[${st.index}].childAnswerList[${stChild.index}].name" class="inptform" id="childQuesAnswer${stChild.index+1}" />
                              <form:hidden path="childQuestionList[${st.index}].childAnswerList[${stChild.index}].id" class="inptform" /></td>
                     	       </tr>
                            </c:forEach>
                            <form:hidden path="childQuestionList[${st.index}].childQuestionId"/>
                           <tr>
                                      <td>Upload only CSV file</td>
                                      <td><form:input type="file" path="childQuestionList[${st.index}].answerFile" id="answerfile${st.index}"/></td>
                             </tr>
                            
                           </table>
                           </div>  
                            </c:forEach>
                           </td></tr>  
                           <form:hidden path="childQuestionIdList"/>
                           <form:hidden path="sizeOfChildList"/>
                           
                            </tbody></table>
                          </td>
                        </tr>
                      <tr>
						<td width="197" height="19" align="left" valign="middle"><input type="button" onClick="addNewChildRows();" name="addMore" id="add_child" value="Add More +" class="btnsubmit"></td>
						<td width="844" colspan="5" align="left" valign="middle">
                        <input type="hidden" id="admorecount" name="admorecount" value="0">                        </td>
                      </tr>					
					</table>
					 </div>	</td>
				</tr>
	            </table>
	          </td>
	        </tr> 
	        
	        <tr>
	          <td height="31" align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="checkRowUpdation();RowUpdation();return checkQuestionValidation();return RowUpdationForChild()"></td>
	              <td width="118" align="center" valign="middle"><a href="questionList" class="btnsubmitLink" >Cancel</a></td>
	            </tr>
	          </table></td>
	          <td colspan="5" align="left" valign="middle">&nbsp;</td>
	        </tr>
	        <tr>
	          <td height="31" align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
	          <td colspan="5" align="left" valign="middle">&nbsp;</td>
	        </tr>            
	        <tr>
	          <td height="2" align="left" valign="middle" >              </td>
	        </tr>
	        <tr>
	          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
	        </tr> 
          </table>
		  </form:form>
          <div id="child_ques_div" style="display:none;">
          <table id="child_table">
          <tr>
              <td width="191" align="left" valign="middle">Child answer</td>
              <td width="12" colspan="2" align="left" valign="middle"><input  type="text" name="childQuestionList[0].childQuestion" class="inptform" id="childQuestion"/></td>
           </tr>
           <tr>
               <td>Upload Only CSV File</td>
               <td><input type="file" name="childQuestionList[0].answerFile" id="answerfile" /></td>
           </tr> 
           <tr>
           		<td width="198" align="left" valign="middle" id="removeChild" class="btn-remove"><img src="images/btncross.png" width="18" height="17" border="0">
                </td>
           </tr>
           </table>
               </div>  
	      </div>
	    </div>
	  </div>
	  </div>
	</body>
	</html>
	<script type="text/javascript">
	var answers=new Array();
	var subquestion=new Array();
	var childquestion=new Array();
	function checkQuestionValidation()
	{	
		var status=true;
		if( document.getElementById("typeId").value=="NONE" || document.getElementById("question").value=="" )		
		{
			status=false;	
		}
		else if(parentQues==true)
		{
			if(document.getElementById("parent_question_id").value=="NONE" || document.getElementById("parentAnswersList").value=="NONE")
			{
				status=false;
			}
		}
		else if(subQues==true)
		{
			if(document.getElementById("question0").value=="")
			{
				status=false;
			}
		}
		else if(answer==true)
		{
			if(document.getElementById('answerTypeId').value=="NONE")
			{
				status=false;
			}
		}
		if(status==false)
		{
			document.getElementById('validation').innerHTML="Please enter all mandatory fields."		
		}
		return status;
	}
	
	
	$(document).ready(function(){

		/**** multiselect drop down ***********/
		//$("#parentAnswersList").dropdownchecklist({ emptyText:"--Answer--" ,width: 238 });
		$("#parent_question_id").change(function()
				{
			//$("#ddcl-parent_answer_id")doc.style.display='none';
			document.getElementById('ddcl-parentAnswersList').style.display='none';
		
		});
	
		/************ ADD ANWERS *************/
	
		$("#Add").click(function() {	
		
			 var tr_temp=$("#answerTable tr:eq(1)").clone();
			 tr_temp.find("#anwser0").each(function() {
				 $(this).attr({
						'id': function(_, id) { return 'anwser'+j},
						'name': function(_, name) { return 'answerList['+ j +'].answer'},
						'value': ''
				});
				}).end();
			 /*tr_temp.find("#sequence0").each(function() {
			 $(this).attr({
				'id': function(_, id) { return 'sequence'+j},
				'name': function(_, name) { return 'List['+ j +'].sequence'},
				'value': ''
			});
			}).end();*/
				tr_temp.find("td:last").remove();
				tr_temp.append(btn_close);
				tr_temp.appendTo("#answerTable");
			 j++;
		});
		
		/************** ADD MORE ANSWERS ***************/
		$("#addMore").click(function() {	
			 var tr_temp=$("#subQuestionTable tr:first").clone();
			 tr_temp.find("#question0").each(function() {
				 $(this).attr({
						'id': function(_, id) { return 'question'+i},
						'name': function(_, name) { return 'subQuestionList['+ i +'].subQuestion'},
						'value': ''
				});
				}).end();
				tr_temp.find("td:last").remove();
				tr_temp.append(btn_close);
				tr_temp.appendTo("#subQuestionTable");
			 i++;		
		});
			
		/**************** REMOVING ROWS ***************/	
		$("#remove").live('click',function(){
			$(this).parent().remove();
			row--;
		});
		
		
		/*************** FINDING QUES TYPE DETAIL ***************/
		var TypeId=document.getElementById("typeId").value;
		
		var subQues=false;
		var parentQues=false;
		var answer=true;
		
		function findQuestionType(typeId){
			if (window.ActiveXObject) 
			{          
				request = new ActiveXObject("Microsoft.XMLHTTP"); 
			} 
			else
			{         
				request = new XMLHttpRequest();          
			} 
			if (request) 
			{
				request.onreadystatechange = handleStateChange; 
				request.open("GET","getQuestionTypeInfo?questionTypeId="+typeId+"&action=getQuestionDetail",true);
				request.send(true);	
			}
		}
	
	
		function handleStateChange()
		{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			//alert(request.responseText);
			var requestres = request.responseText;
			//alert(requestres);
			var resText = requestres.replace(/^\s+|\s+$/g, "");
			if(resText==9)
			{
				document.getElementById('anwser_div2').style.display='block';
				document.getElementById('parent_question').style.display='none';
				document.getElementById('sub_Question').style.display='none';
				document.getElementById("answer_div").style.display='none';
				//document.getElementById('parent_question').style.display='none';
				parenChildQues=true;
			}
			else if(resText==0 || resText==03)
			{
				document.getElementById('sub_Question').style.display='block';
				document.getElementById('parent_question').style.display='none';
				document.getElementById('anwser_div2').style.display='none';
				subQues=true;
			}
			else if(resText==1 || resText==15)
			{
				document.getElementById('parent_question').style.display='block';
				document.getElementById('sub_Question').style.display='none';
				document.getElementById('anwser_div2').style.display='none';
				parentQues=true;
			}
			else if(resText==2 || resText==24)
			{
				document.getElementById('sub_Question').style.display='block';
				document.getElementById('parent_question').style.display='block';
				document.getElementById('anwser_div2').style.display='none';
				subQues=true;
				parentQues=true;
			}
			else if(resText==6)
			{
				document.getElementById('sub_Question').style.display='none';
				document.getElementById('parent_question').style.display='none';
				document.getElementById("answer_div").style.display='block';
				document.getElementById('anwser_div2').style.display='none';
			}
			else if(resText==7)
			{
				document.getElementById('sub_Question').style.display='none';
				document.getElementById('parent_question').style.display='block';
				document.getElementById("answer_div").style.display='block';
				document.getElementById('anwser_div2').style.display='none';
			}
			
			if(resText==03 || resText==24 ||resText==15)
			{
				document.getElementById("answer_div").style.display='none';
				document.getElementById('anwser_div2').style.display='none';
				answer=false;
			}
			
			request.abort();
			request = null;
		}
		
	
		/*********** FINIDNG ANSWERS *************/
		
		
		/************ GETTING ROW VALUES FOR ANSWERS ***************/
		
		function getRowValues()
		{
			for(var j=0;j<sizeOfList;j++)
			{
				answers[j]=document.getElementById('anwser'+j).value;
			}
			document.getElementById("sizeOfList").value=sizeOfList;
		}
		
		/************ GETTING ROW VALUES FOR CHILDQUESTION ***************/
		document.getElementById("sizeOfChildList").value=sizeOfChildList;
		function getRowValuesForChild()
		{
			
			for(var j=0;j<sizeOfChildList;j++)
			{
				childquestion[j]=document.getElementById('childQuestion'+j).value;
			}
			document.getElementById("sizeOfChildList").value=sizeOfChildList;
		}
		
		var childQuestionIdList;
		function RowUpdationForChild()
		{
			
			childQuestionIdList="";
			for(var i=0;i<sizeOfChildList;i++)
			{
				if(childquestion[i]!=document.getElementById('childQuestion'+i).value )
				{
					childQuestionIdList+=i+",";		
				}
			}
			document.getElementById("childQuestionIdList").value=childQuestionIdList;
		}
		
		/************ GETTING ROW VALUES FOR CHILDQUESTION OPTION ***************/
		
		/*function getRowValuesForChildOption()
		{
			for(var j=0;j<sizeOfChildOptionList;j++)
			{
				childquestion[j]=document.getElementById('childQuestion'+j).value;
			}
			document.getElementById("sizeOfChildList").value=sizeOfChildList;
		}	
		
		var childQuesOptionIdList;
		function RowUpdationForChildOption()
		{
			childQuesOptionIdList="";
			for(var i=0;i<sizeOfChildOptionList;i++)
			{
				if(childquestion[i]!=document.getElementById('childQuestion'+i).value )
				{
					childQuesOptionIdList+=i+",";		
				}
			}
			document.getElementById("childQuesOptionIdList").value=childQuesOptionIdList;
		}*/
		
		
		/**************** GETTING ROW VALUES *****************/
		
		function getSubquesionRowValues()
		{
			for(var j=0;j<listSize;j++)
			{
				subquestion[j]=document.getElementById('question'+j).value;
			}
			document.getElementById("listSize").value=listSize;
		}
		var subquestionIdList;
		function RowUpdation()
		{
			subquestionIdList="";
			for(var i=0;i<listSize;i++)
			{
				if(subquestion[i]!=document.getElementById('question'+i).value )
				{
					subquestionIdList+=i+",";		
				}	
			}
			document.getElementById("subquestionIdList").value=subquestionIdList;
		}
		
		
		//var TypeId=document.getElementById("typeId").value;
		findQuestionType(TypeId);
		var subquestionIdList;
		
		//Calling getRowValues() 
		getRowValues();
		
		//Calling getSubquesionRowValues()
		getSubquesionRowValues();
		
		//calling getRowValuesForChild()
		getRowValuesForChild();
		
		//calling 
		findAnswerList($('#parent_question_id option:selected').val());
		$("#removeChild").live('click',function(){
				
			$(this).parent().parent().remove();
			
		});
		
	});//Ready ends

/**** add more functionality for childquestion*********/
		
		function addNewChildRows()
		{
			//alert('k-->'+k);
			//alert('1');
			document.getElementById('child_ques_div').style.display='none';
			var tr_temp=$("#child_table tr").clone();
			document.getElementById('child_ques_div').style.display='none';
			//alert(tr_temp.val());
			
			tr_temp.find("#childQuestion").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'childQuestion'+k},
	    				'name': function(_, name) { return 'childQuestionList['+ k +'].childQuestion'},
	    				'value': ''
	  			});
			}).end();
			
			tr_temp.find("#answerfile").each(function() {
			 $(this).attr({
 				'id': function(_, id) { return 'answerfile'+k},
 				'name': function(_, name) { return 'childQuestionList['+ k +'].answerFile'},
 				'value': ''
			});
			}).end();
			tr_temp.find("#child_table").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'child_table'+k},
	    		});
			}).end();
			//alert('2');
			//tr_temp.append(btn_close_child);					
				$('#childTable').append("<tr><td colspan='2'><div id='"+k+"'>");
				
				/*alert('before append-->'+document.getElementById('childQuestionTable').innerHTML);*/				
				//alert('3');
				tr_temp.appendTo("#"+k);
				
			k++;
			
		}
	function findAnswerList(typeId)
	{
		if (window.ActiveXObject) 
	    {          
	     	request2 = new ActiveXObject("Microsoft.XMLHTTP"); 
	    } 
	    else
	    {         
	    	request2 = new XMLHttpRequest();          
	    } 
		if (request2) 
		{
			request2.onreadystatechange = handleStateChange2;
			request2.open("GET","findAnswerList?questionTypeId="+typeId+"&action=getAnswerList",true);
			request2.send(true);	
	 	}
	}
	
	function handleStateChange2()
	{   
		if (request2.readyState != 4) return;
		if (request2.status != 200) return;
		
		var requestres = request2.responseText;
		var list = requestres.replace(/^\s+|\s+$/g, "");
		var jsondata = eval("("+list+")");
		var option = '';
		for(var i=1; i<(jsondata.selectoption0[0].option).length; i++){
			option = option+'<option value="'+jsondata.selectoption0[0].option[i].value+'"->'+jsondata.selectoption0[0].option[i].param+'</option>';
		}

		document.getElementById("parentAnswersList").innerHTML=option;
		 $("#parentAnswersList").dropdownchecklist({ emptyText:"--Answer--" ,width: 238,maxDropHeight: 140 });
		
		
	}
	var answerIdList;
	function checkRowUpdation()
	{
		answerIdList="";
		for(var i=0;i<sizeOfList;i++)
		{
			
			if(answers[i]!=document.getElementById('anwser'+i).value)
			{
				answerIdList+=i+",";		
			}
			
		}
		document.getElementById("anwserIdList").value=answerIdList;
		
	}
	</script>
	