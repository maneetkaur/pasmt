<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%>  
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Add Global Question - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<!-- multiple select box css -->
 <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.13.custom.css">
<link rel="stylesheet" type="text/css" href="css/ui.dropdownchecklist.themeroller.css">

<link rel="stylesheet" href="css/style.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>

<script type="text/javascript">
var i = 1;
var rowCount=1;
var btn_close='<td width="113" align="left" valign="middle" id="remove" class="btn-remove">'+
    		  '<img src="images/btncross.png" width="18" height="17" border="0"></td>';
var btn_close_child='<tr><td width="198" align="left" valign="middle" id="removeChild" class="btn-remove">'+
    		  '<img src="images/btncross.png" width="18" height="17" border="0"></td></tr>';
var j=1;
var row=1;
$(document).ready(function(){
	/***** code for multiselect drop down box ********/
	//$returnS5 = $('#returnS5');
 $("#parentAnswersList").dropdownchecklist({ emptyText:"--Answer--" ,width: 238,maxDropHeight: 140 });
	$("#parent_question_id").change(function()
			{
		//$("#ddcl-parent_answer_id")doc.style.display='none';
		document.getElementById('ddcl-parentAnswersList').style.display='none';
	
	});
	

/**** add more functionality for subquestion*********/
		$("#addMore").click(function() {	
			 var tr_temp=$("#subQuestionTable tr:first").clone();
			 tr_temp.find("#question0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'question'+i},
	    				'name': function(_, name) { return 'subQuestionList['+ i +'].subQuestion'},
	    				'value': ''
	  			});
				}).end();
			 	tr_temp.find("td:last").remove();
			 	tr_temp.append(btn_close);
				tr_temp.appendTo("#subQuestionTable");
			 i++;		
		});
		
		$("#Add").click(function() {	
			 var tr_temp=$("#answerTable tr:first").clone();
			 tr_temp.find("#anwser0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'anwser'+j},
	    				'name': function(_, name) { return 'answerList['+ j +'].answer'},
	    				'value': ''
	  			});
				}).end();
			 /*tr_temp.find("#sequence0").each(function() {
			 $(this).attr({
 				'id': function(_, id) { return 'sequence'+j},
 				'name': function(_, name) { return 'List['+ j +'].sequence'},
 				'value': ''
			});
			}).end();*/
			 	tr_temp.find("td:last").remove();
			 	tr_temp.append(btn_close);
				tr_temp.appendTo("#answerTable");
			 j++;
		});
		$("#remove").live('click',function(){
			$(this).parent().remove();
			row--;
		});
		
	
		/****** add more for internal childanswer in childquestion ********/
		$("#Add_child_question").click(function() {
			alert('in add_child_more');
			 var tr_temp=$("#answerTable2 tr:first").clone();
			 tr_temp.find("#childAnwser0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'childAnwser'+j},
	    				'name': function(_, name) { return 'childQuestionList['+ j +'].childAnswerList[].name'},
	    				'value': ''
	  			});
				}).end();
			 	tr_temp.find("td:last").remove();
			 	tr_temp.append(btn_close);
				tr_temp.appendTo("#answerAddedRows");
			 j++;
		});
		$("#remove").live('click',function(){
			$(this).parent().remove();
			row--;
		});
		$("#removeChild").live('click',function(){
				
			$(this).parent().parent().remove();
			
		});
		
		
		
	
	
});
/**** add more functionality for childquestion*********/
		function addNewChildRows(){
			//alert('i-->'+i);
			var tr_temp=$("#0").clone();
			tr_temp.find("#childQuestion0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'childQuestion'+i},
	    				'name': function(_, name) { return 'childQuestionList['+ i +'].childQuestion'},
	    				'value': ''
	  			});
			}).end();
			tr_temp.find("#answerfile0").each(function() {
			 $(this).attr({
 				'id': function(_, id) { return 'answerfile'+i},
 				'name': function(_, name) { return 'childQuestionList['+ i +'].answerFile'},
 				'value': ''
			});
			}).end();
			
			tr_temp.append(btn_close_child);	
				
				$('#childTable').append("<tr><td colspan='2'><div id='"+i+"'>");
				/*alert('before append-->'+document.getElementById('childQuestionTable').innerHTML);*/				
				
				tr_temp.appendTo("#"+i);
				
			 i++;		
		}
		
</script>

</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!-- multiple select box js -->
<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
<script type="text/javascript" src="js/ui.dropdownchecklist.js"></script>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Question</h2>
      <form:form action="AddQuestion" method="post" modelAttribute="add_global_question" enctype="multipart/form-data">
	  <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >           
            <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td colspan="2" align="left" valign="middle">&nbsp;</td>
                <td width="6" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
                            <tr>
                <td width="199" height="31" align="left" valign="middle">Question Name <span>*</span></td>
                <td width="909" align="left" valign="middle"><div id="question_type_list" >
                  <form:select path="typeId" class="inptformSelect" style="width:243px;" onchange="findQuestionType(this.value)">
                    <form:option value="NONE" label="--QuestionType--"></form:option>
                    <form:options items="${QuestionTypeList}" itemLabel="questionTypeHeading" itemValue="questionTypeId"/>
                  </form:select>
                </div></td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
           
              <tr>
                <td height="31" align="left" width="201" valign="middle">Question Text<span>*</span> </td>
                <td height="31" align="left" width="890" valign="middle"> <form:input path="question" class="inptform" />   </td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
             
              <tr>
                <td height="5" colspan="7" align="left" valign="middle">
				  <div id="sub_Question" style="display: none;" >                
					<table width="801" border="0" cellspacing="0" cellpadding="0">                 
					  <tr>
						<td colspan="3"><strong>Add Sub Questions <span>*</span></strong></td>
                      </tr>                     
                   
                      <tr>
						<td height="19" colspan="6" align="left" valign="middle">
						  <table width="794" border="0" cellspacing="0" cellpadding="0">
							<tbody id="subQuestionTable">
							  <tr>
								<td width="187" align="left" valign="middle">Question <span>*</span></td>
								<td width="264" align="left" valign="middle">  <form:input path="subQuestionList[0].subQuestion" class="inptform" id="question0"/>     </td>
								<td width="343" align="left" valign="baseline">&nbsp;</td>
							  </tr>     
							</tbody>
						  </table>						</td>
					  </tr>
                      <tr>
						<td width="441" height="19" align="left" valign="middle"><input type="button" name="addMore" id="addMore" value="Add More +" class="btnsubmit"></td>
						<td width="360" colspan="5" align="left" valign="middle">&nbsp;</td>
                      </tr>
					</table>
                  </div>				</td>
              </tr>
              
              <tr>
                <td height="3" colspan="7" align="left" valign="middle">
				  <div id="parent_question" style="display: none; padding-top:8px;">
					<table width="645" border="0" cellspacing="0" cellpadding="0">
                      <tr>
						<td colspan="3"><strong>Select Parent Question </strong></td>
                      </tr>
					  <tr>
						<td width="196">Select Question <span>*</span> </td>
						<td width="449" colspan="2">
						  <div id="question_list">
						  <form:select path="parent_question_id" class="inptformSelect" style="width:243px;" onChange="findAnswerList(this.value); ">
						  	<form:option value="NONE" label="--Parent Question--"></form:option>
						  	<form:options items="${parentQuestionList}" itemLabel="questionTypeHeading" itemValue="questionTypeId"/>
						  </form:select>
						  </div></td>
                      </tr>
					  <tr>
						<td>Select Answer <span>*</span></td>
						<td colspan="2"><div id="answer_list" >
						<select name="parentAnswersList" id="parentAnswersList" style="width:243px;"  multiple="true">
							<option value="NONE">--Answer--</option>
						</select></div></td>
                      </tr>
					</table>
				  </div>				</td>
              </tr>
              <tr>
				<td  height="31" colspan="7" align="left" valign="middle">
				  <div id="anwser_div">
					<table width="673" border="0" cellspacing="0" cellpadding="0">
					  <tr>	
               	 		<td width="195">Sort Answer by <span>*</span></td>
               			 <td width="478"><form:select path="sortingId" class="inptformSelect" style="width:243px;">
                			<form:option value="NONE" label="--Sorting type--"></form:option>
               			    <form:option value="0">Alphabetical order</form:option>
               			    <form:option value="1">Numeric order(Ascending)</form:option>
               			    <form:option value="2">Numeric order(Descending)</form:option>
                            <form:option value="3">Insertion order</form:option>
                			</form:select></td>
                </tr>
                      <tr>
						<td width="199" height="29">Answer Upload Method<span>*</span></td>
						<td width="478">
						  <form:select path="answerTypeId" class="inptformSelect" style="width:243px;" onchange="getAnswerType()">
							<form:option value="NONE" label="--Answer type--"></form:option>
							<form:option value="1">Import Answer</form:option>
							<form:option value="2">Custom</form:option>
						  </form:select>						</td>
					  </tr>
					</table>
                  </div>				</td>
              </tr>
            
            
              <tr>
                <td height="8" colspan="7" align="left" valign="top">
				  <div id="custom_answer" style="display: none;">
					<table  width="1046" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td height="19" colspan="6" align="left" valign="middle">
						  <table width="900" border="0" cellspacing="0" cellpadding="0">
							<tbody id="answerTable">
							  <tr>
								<td width="188" align="left" valign="middle">Answer</td>
								<td width="357" align="left" valign="middle">
									<form:input path="answerList[0].answer" class="inptform" id="anwser0"/>								</td>
								<td width="355" align="left" valign="middle">&nbsp;</td>
							  </tr>
							</tbody>
						  </table>						</td>
					  </tr>
                      <tr>
						<td colspan="3"><input type="button" name="Submit" id="Add" value="Add More +" class="btnsubmit"></td>
                      </tr>
                     
					</table>
                  </div>				</td>
              </tr>
              <tr>
                <td height="31" colspan="7" align="left" valign="middle">
				  <div id="import_answer" style="display: none;">
					<table width="502" border="0" cellspacing="0" cellpadding="0">
                      <tr>
						<td>Upload Only CSV File</td>
						<td><form:input type="file" path="CSvFile" /></td>
                      </tr>
                     
					</table>
                  </div>                  </td>
                  </tr>
                  <tr>
                  <td colspan="2">
                  <table width="900">
                  <tr>
				<td  height="31" colspan="6" align="left" valign="middle">
				  <div id="anwser_div2" style="display: none;">
					<table width="700" border="0" cellspacing="0" cellpadding="0">
					  <tr>	
               	 		<td width="197">Parent Question <span>*</span></td>
               			 <td width="844">
               			 <form:select path="parent_question_id" class="inptformSelect" style="width:243px;">
                			<form:option value="NONE" label="--Parent Question--"></form:option>
						  	<form:options items="${parentQuestionList}" itemLabel="questionTypeHeading" itemValue="questionTypeId"/>
						  </form:select>
                          </td>
                </tr>
                <tr>
                <td width="197">Max rows for answer <span>*</span></td>
                 <td width="844">
                 <form:input path="marRowForChild" class="inptform"/>                 </td>
                 </tr>
                  <tr>
                <td width="197">Child question<span>*</span></td>
                 <td width="844">
                 <form:input path="childVerticalQuestion" class="inptform"/>                 </td>
                 </tr>
               
                 <tr>
					<td colspan="3"><strong>Add child Answers <span>*</span></strong></td>
                  </tr>                     
				 
                    <tr>
                      <td height="19" colspan="6" align="left" valign="middle"><table width="1041" border="0" cellspacing="0" cellpadding="0">
                        <tbody id="childTable">
                          <tr>
                          <td>
                          <div id="0"><table>
                            <tr>
                              <td width="191" align="left" valign="middle">Child answer<span>*</span></td>
                              <td width="12" colspan="2" align="left" valign="middle"><form:input path="childQuestionList[0].childQuestion" class="inptform" id="childQuestion0"/></td>
                            </tr>
                           <tr>
                                      <td>Upload Only CSV File</td>
                                      <td><form:input type="file" path="childQuestionList[0].answerFile" id="answerfile0"/></td>
                             </tr>
                             
                           </table>
                            
                           </div>  
                           </td></tr>         
                            </tbody>                    </table></td>
                        </tr>
                      <tr>
						<td width="197" height="19" align="left" valign="middle"><input type="button" onClick="addNewChildRows();" name="addMore" id="add_child" value="Add More +" class="btnsubmit"></td>
						<td width="844" colspan="5" align="left" valign="middle">
                        <input type="hidden" id="admorecount" name="admorecount" value="0">                        </td>
                      </tr>					
					</table>
					 </div>	</td>
				</tr>
                  <tr>
                  <td colspan="2">				
				  <table width="367" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return checkQuestionValidation()"></td>
                      <td width="118" align="center" valign="middle"><a href="questionList" class="btnsubmitLink" >Cancel</a></td>
                    </tr>
                  </table></td>
              </tr>
              
              <tr>
                <td height="31" colspan="7" align="left" valign="middle">&nbsp;</td>
              </tr>
            </table>          </td>
        </tr> 
        
        <tr>
          <td height="31" align="left" valign="middle">&nbsp;</td>
          <td colspan="5" align="left" valign="middle">&nbsp;</td>
        </tr>
        <tr>
          <td height="31" align="left" valign="middle">&nbsp;</td>
          <td colspan="5" align="left" valign="middle">&nbsp;</td>
        </tr>            
        <tr>
          <td height="2" align="left" valign="middle" >&nbsp;</td>
        </tr>
                
      </table>
      </td>
      </tr>
      </table>
	  </form:form>
     
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
//alert('in js');
function findQuestionType(typeId){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		request.open("GET","getQuestionTypeInfo?questionTypeId="+typeId+"&action=getQuestionDetail",true);
		request.send(true);	
 	}
	//return val;
}
var subQues=false;
var parentQues=false;
var parenChildQues=false;
var answer=true;
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			//alert(request.responseText);
			var requestres = request.responseText;
			//alert(requestres);
			var resText = requestres.replace(/^\s+|\s+$/g, "");
			//alert(resText);
			
			if(resText==9)
			{
				document.getElementById('anwser_div2').style.display='block';
				document.getElementById('parent_question').style.display='none';
				document.getElementById('sub_Question').style.display='none';
				document.getElementById("anwser_div").style.display='none';
				//document.getElementById('parent_question').style.display='none';
				parenChildQues=true;
			}
			else if(resText==0 || resText==03)
			{
				document.getElementById('sub_Question').style.display='block';
				document.getElementById('parent_question').style.display='none';
				document.getElementById('anwser_div2').style.display='none';
				subQues=true;
			}
			else if(resText==1 || resText==15)
			{
				document.getElementById('parent_question').style.display='block';
				document.getElementById('sub_Question').style.display='none';
				document.getElementById('anwser_div2').style.display='none';
				parentQues=true;
			}
			else if(resText==2 || resText==24)
			{
				document.getElementById('sub_Question').style.display='block';
				document.getElementById('parent_question').style.display='block';
				document.getElementById('anwser_div2').style.display='none';
				subQues=true;
				parentQues=true;
			}
			else if(resText==6)
			{
				document.getElementById('sub_Question').style.display='none';
				document.getElementById('parent_question').style.display='none';
				document.getElementById("anwser_div").style.display='block';
				document.getElementById('anwser_div2').style.display='none';
			}
			else if(resText==7)
			{
				document.getElementById('sub_Question').style.display='none';
				document.getElementById('parent_question').style.display='block';
				document.getElementById("anwser_div").style.display='block';
				document.getElementById('anwser_div2').style.display='none';
			}
			
			if(resText==03 || resText==24 ||resText==15)
			{
				document.getElementById("anwser_div").style.display='none';
				document.getElementById('anwser_div2').style.display='none';
				answer=false;
			}
			
			request.abort();
			request = null;
}
var k=0;
function getAnswerType()
{

	var option=document.getElementById("answerTypeId").value;
	//k++;
	//alert('option value-->'+option);
	if(option==1)
	{
		//alert('in option 1 if');
		document.getElementById("import_answer").style.display ='block';	
	}
	else
	{
		//alert('in option 1 else');
		document.getElementById("import_answer").style.display ='none';
	}
	if(option==2)
	{
		//alert('in option 2 if');
		document.getElementById("custom_answer").style.display ='block';		
	}
	else
	{
		//alert('in option 2 else');
		document.getElementById("custom_answer").style.display ='none';
	}
	k++;
}
function checkQuestionValidation()
{	
	var status=true;
	//alert(document.getElementById('parent_question_answer_id').value);
	if(document.getElementById("typeId").value=="NONE" || document.getElementById("question").value=="" ||  document.getElementById("sortingId").value=="" )		
	{
		status=false;	
	}
	else if(parentQues==true)
	{
		if(document.getElementById("parent_question_id").value=="NONE" || document.getElementById("parentAnswersList").value=="NONE")
		{
			status=false;
		}
	}
	else if(subQues==true)
	{
		if(document.getElementById("question0").value=="")
		{
			status=false;
		}
	}
	else if(answer==true)
	{
		/*if(document.getElementById('answerTypeId').value=="NONE")
		{
			status=false;
		}*/
	}
	if(status==false)
	{
		document.getElementById('validation').innerHTML="Please enter all mandatory fields."		
	}
	return status;
}
/*
function findCultureId(cultureId)
{
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange1; 
		request.open("GET","getQuestionTypeList?cultureId="+cultureId+"&action=getQuestionList",true);
		request.send(true);	
 	}
}
function handleStateChange1()
{
	if (request.readyState != 4) return;
	if (request.status != 200) return;
	var requestres = request.responseText;
	var list = requestres.replace(/^\s+|\s+$/g, "");
	//alert(list);
	var jsondata = eval("("+list+")");
	//alert(jsondata);
	var option ;
	for(var i=0; i<(jsondata.selectoption0[0].option).length; i++){
		option = option+'<option value="'+jsondata.selectoption0[0].option[i].value+'" >'+jsondata.selectoption0[0].option[i].param+'</option>';
	}
	//alert(option);
	document.getElementById("typeId").innerHTML = option;
	
	var options ; 
	for(var i=0; i<(jsondata.selectoption1[0].option).length; i++){
		options = options+'<option value="'+jsondata.selectoption1[0].option[i].value+'" >'+jsondata.selectoption1[0].option[i].param+'</option>';
	}
	document.getElementById("parent_question_id").innerHTML = options;	
	
	
}
*/

function findAnswerList(typeId)
{
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange2; 
		request.open("GET","findAnswerList?questionTypeId="+typeId+"&action=getAnswerList",true);
		request.send(true);	
 	}
}
function handleStateChange2()
{
	if (request.readyState != 4) return;
	if (request.status != 200) return;
	var requestres = request.responseText;
	//alert(request.responseText);
	var list = requestres.replace(/^\s+|\s+$/g, "");
	//alert(list);
	var jsondata = eval("("+list+")");
	//alert(jsondata.selectoption0[0].option[3].param);
	var option = '';
	for(var i=1; i<(jsondata.selectoption0[0].option).length; i++){
		option = option+'<option value="'+jsondata.selectoption0[0].option[i].value+'" >'+jsondata.selectoption0[0].option[i].param+'</option>';
	}
	//document.getElementById("parent_answer_id").innerHTML="";

	document.getElementById("parentAnswersList").innerHTML=option;
	 $("#parentAnswersList").dropdownchecklist({ emptyText:"--Answer--" ,width: 238 ,maxDropHeight: 140});
	 
}

</script>
