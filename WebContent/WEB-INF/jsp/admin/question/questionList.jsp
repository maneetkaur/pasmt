<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%> 
<%@page import="com.admin.globalQuestion.GlobalQuestionBean"%>  
<%
	String topMenu = "Admin";
%> 
<%!
    Integer pagerPageNumber=new Integer(0);
	GlobalQuestionBean previewQuestion= new GlobalQuestionBean();
	List<GlobalQuestionBean> previewQuestions= new ArrayList();
	int questionType;
	
%>
<!DOCTYPE html>       
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View Global Question - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
 
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ulightbox.css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->

<!-- Lightbox Start  -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/jquery.ulightbox.js"></script>
<!-- Lightbox End -->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddGlobalQuetion">
      <h3><a href="addQuestion"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style=" width:120px;">Search Question</h2>
      <form:form modelAttribute="search_global_question" action="searchQuestion" method="post">
       <label>Category</label>
       <form:select path="subCategoryId"  class="selectdeco" style=" width:120px;">
      <form:option value="NONE" label="--Category--"></form:option>
      <form:options items="${subCategoryList}" itemLabel="name" itemValue="id"/>
      </form:select>
       <label>Subcategory</label>
       <form:select path="categoryId"  class="selectdeco" style=" width:120px;">
      <form:option value="NONE" label="--Subcategory--"></form:option>
      <form:options items="${categoryList}" itemLabel="name" itemValue="id"/>
      </form:select>
      <label>Question Type</label>
      <form:select path="typeId"  class="selectdeco" style=" width:120px;">
      <form:option value="NONE" label="--Question type--"></form:option>
      <form:options items="${questionTypeList}" itemLabel="name" itemValue="id"/>
      </form:select>
       <input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>
       </form:form>
       
      </div>
      </div>
      <div class="searchresult">
        <table width="1135" border="0" align="left" cellpadding="0" cellspacing="0">
          <tr>
            <td height="42" colspan="7" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn"><table width="1044" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="383" align="left" valign="middle"><h2>Question Listing</h2></td>
                <td width="753">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="134" height="42" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Sr No </td>
            <td width="181" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Category</td>
            <td width="233" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Subcategory</td>
            <td width="241" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Question Type</td>
            <td width="147" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn"> Status </td>
            <td width="104" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Preview</td>
            <td width="95" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Edit</td>
          </tr>
           <c:if test="${not empty questionList}">
              <c:set var="pageUrl" value="questionList"/>
           <c:set var="totalRows" value="${requestScope.total_rows}"/>
           <pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">
          <c:forEach items="${questionList}" var="ll" varStatus="counter">
          <pg:item>
            <c:choose>
              <c:when test="${counter.count%2==0}">
                <tr>
                  <td align="left" valign="middle" class="listingitem">${counter.count}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.categoryType}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.subCtaegory}</td>
                  <td align="left" valign="middle" class="listingitem">${ll.questionType}</td>
                  <td align="left" valign="middle" class="listingitem"><c:choose>
                      <c:when test="${ll.status==1}"> <a href="updateQuestionStatus?questionId=${ll.questionId}">Active</a> </c:when>
                      <c:otherwise> <a href="updateQuestionStatus?questionId=${ll.questionId}">Inactive</a> </c:otherwise>
                  </c:choose></td>
                  <td align="left" valign="middle" class="listingitem"><a href="previewQuestion?questionId=${ll.questionId}"><img src="images/icon-preview.png" alt="preview" width="20" height="23" border="0" title="preview" /></a></td>
                  <td align="left" valign="middle" class="listingitem"><a href="updateQuestion?questionId=${ll.questionId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
                </tr>
              </c:when>
              <c:otherwise>
                <tr> 
                  <td align="left" valign="middle" class="listingitem2">${counter.count}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.categoryType}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.subCtaegory}</td>
                  <td align="left" valign="middle" class="listingitem2">${ll.questionType} </td>
                  <td align="left" valign="middle" class="listingitem2"><c:choose>
                      <c:when test="${ll.status==1}"> <a href="updateQuestionStatus?questionId=${ll.questionId}">Active</a> </c:when>
                      <c:otherwise> <a href="updateQuestionStatus?questionId=${ll.questionId}">Inactive</a> </c:otherwise>
                  </c:choose></td>
                  
                  <td align="left" valign="middle" class="listingitem2"><a href="previewQuestion?questionId=${ll.questionId}"><img src="images/icon-preview.png" alt="preview" width="20" height="23" border="0" title="preview" /></a></td>
                  <td align="left" valign="middle" class="listingitem2"><a href="updateQuestion?questionId=${ll.questionId}"><img src="images/iconedit.png" alt="edit" width="20" height="23" border="0" title="edit" /></a></td>
              </c:otherwise>
            </c:choose>
          </pg:item>
           </c:forEach>
           <%@include file="../../include/paging_bar.jsp"%>
           </pg:pager>
          </c:if>
          <tr>
            <td colspan="7" align="left" valign="middle" bgcolor="#FFFFFF" class="pagelistinggrn">&nbsp;</td>
          </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>

<script type="text/javascript">

$(document).ready(function(){
	 var isPreview=<%=request.getParameter("checkPreview")%>;
	
		if(isPreview == true){
		uLightBox.init({
			override:true,
			background: 'white',
			centerOnResize: true,
			fade: true
		});	
	 		 uLightBox.alert({
	 			width: '700px',
				title: 'Question Preview',
				//rightButtons: ['Submit'],
				rightButtons: ['Close'],
				 opened: function() {
					$('<span />').html('<div style="height:230px;border: 2px solid rgb(238, 238, 238);background: none repeat scroll 0% 0% rgb(239, 239, 239); color: rgb(0, 0, 0); overflow: auto; margin-bottom: 1.5em;">'+
					'<form action="UserAction" name="agreeform" method="post" id="form2" style="margin-top:0px;" >'+
			                   '<input type="hidden" name="action" value="addLoginQuestionInfo">'+
								'<table width="90%;">'+
								'<%
								if(session.getAttribute("previewQuestions")!=null)
			                      {
									previewQuestions=(List<GlobalQuestionBean>)session.getAttribute("previewQuestions");
									System.out.println("in preview");
									for(int m=0;m<previewQuestions.size();m++)
									{
										previewQuestion=previewQuestions.get(m);
										System.out.println("in preview values-->"+previewQuestion);
				                             questionType=Integer.parseInt(previewQuestion.getQuestionType());
				                             if(m==0)
				                             {
				                            	 %><tr><td style="font-size:14px;color:green;"><strong style="margin-top:0px;">Global Culture</strong></td></tr>'+
													'<% 
				                             }
				                             else
				                             {
				                            	 %><tr><td style="font-size:14px;padding-top:6px;color:green;"><strong style="margin-top:0px;"><%=previewQuestion.getCultureCode()%></strong></td></tr>'+
												'<% 
				                             }
				                             %><tr><td style="font-size:12px;padding-top:8px;padding-bottom:8px;color:black;"><strong><%=previewQuestion.getQuestion()%></strong></td></tr><tr>'+
											 '<input type="hidden" name="question1" id="question1" value="<%=previewQuestion.getQuestionId()%>"/>'+
											
											'<% 
				                		if(questionType==1)
										{
											System.out.println("in preview 1");
											 %><td align="left" valign="top">'+
											 '<table width="400px;">'+	 
											 '<%
											 for(int d=0;d<previewQuestion.getAnswerList().size();d++)
											 {
												 System.out.println("valuse--->"+previewQuestion.getAnswerList().get(d).getAnswer());
													%><tr><td width="20px;"><input type="radio" name="" value="" </td><td><font size="2"><%=previewQuestion.getAnswerList().get(d).getAnswer()%></font></td></tr>'+
											'<%	
				                            }
											 %></table></td>'+
											 '<%
										}
										 else if(questionType==2)
					                        {System.out.println("in preview 2");
						                         %><td width="108" align="left" valign="top">'+
												'<select name="" style="width:230px;" id=""  class="inptforregis" >'+
						                         '<option selected="selected" value="">Select</option>'+
						                         '<%					
						                         for(int d=0;d<previewQuestion.getAnswerList().size();d++)
												 {
													%><option value=""><%=previewQuestion.getAnswerList().get(d).getAnswer()%></option>'+
													'<%
												}
												%></select></td>'+
						                        '<%
					                          }
					                      else if(questionType==3)
					                      {System.out.println("in preview 3");
					                      %><td align="left" valign="top">'+
											 '<table width="600px;">'+	 
											 '<%
											 for(int d=0;d<previewQuestion.getAnswerList().size();d++)
											 {
												 System.out.println("valuse--->"+previewQuestion.getAnswerList().get(d).getAnswer());
													%><tr><td width="20px;"><input type="checkbox" name="" value=""></td><td><font size="2.5px;"><%=previewQuestion.getAnswerList().get(d).getAnswer()%></font></td></tr>'+
											'<%	
				                         }
											 %></table></td>'+
											 '<%
					                      }
					                      else if(questionType==4)
					                      {System.out.println("in preview 4");
					                    	  %><td width="108" align="left" valign="top">'+
												'<select name="" style="width:230px;" id="" class="inptforregis" multiple="true">'+
						                         '<option selected="selected" value="">Select</option>'+
						                         '<%					
						                         for(int d=0;d<previewQuestion.getAnswerList().size();d++)
												 {
													%><option value=""><%=previewQuestion.getAnswerList().get(d).getAnswer()%></option>'+
													'<%
												}
												%></select></td>'+
						                        '<%
					                      }
					                      else if(questionType==5)
					                      {
					                      %><td width="108" align="left" valign="top">'+
											'<input name="" type="text" class="inptforregis"  value="" id="" /></td>'+
					                      '<%
					                      }
					                      else if(questionType==6)
					                      {
					                      %><td width="108" align="left" valign="top">'+
											'<table border="1">'+
											'<%
	
											int k=0;
											for(int i=0;i<previewQuestion.getSubQuestionList().size();)
											{
												if(k==0)
												{
													%><tr><td style="border:none">&nbsp;</td></tr><%
													for(int j=0;j<previewQuestion.getAnswerList().size();j++)
													{
														%><td style="font-size:12px;color:black;padding:2px;text-align:center;"><strong<%=previewQuestion.getAnswerList().get(j).getAnswer()%></strong></td><%
													}
													%></tr><%
												}
												else
												{
													%><tr><td style="font-size:12px;color:black;padding:2px;"><strong<%=previewQuestion.getSubQuestionList().get(i).getSubQuestion()%></strong></td><%
													for(int j=0;j<previewQuestion.getAnswerList().size();j++)
													{
														%><td style="text-align:center;"><input type="radio" name="" id="" value="<%=previewQuestion.getAnswerList().get(j).getAnswer()%>"/></td><%
													}
													%></tr><%
												}
												
												if(k==1)
												{
													i++;
												}
												if(i==0)
												{
													k=1;
												}
											}
											%></table></td><%
											
					                      }
					                      else if(questionType==7)
					                      {
					                    	  %><td width="108" align="left" valign="top">'+
					  						'<table border="1">'+
					  						'<%
	
					  						int k=0;
					  						for(int i=0;i<previewQuestion.getSubQuestionList().size();)
					  						{
					  							if(k==0)
					  							{
					  								%><tr><td style="border:none">&nbsp;</td><%
					  								for(int j=0;j<previewQuestion.getAnswerList().size();j++)
					  								{
					  									%><td style="font-size:12px;color:black;padding:4px;text-align:center;"><strong><%=previewQuestion.getAnswerList().get(j).getAnswer()%></strong></td><%
					  								}
					  								%></tr><%
					  							}
					  							else
					  							{
					  								%><tr><td style="font-size:12px;color:black;padding:4px;"><strong><%=previewQuestion.getSubQuestionList().get(i).getSubQuestion()%></strong</td><%
					  								for(int j=0;j<previewQuestion.getAnswerList().size();j++)
					  								{
					  									%><td style="text-align:center;"><input type="checkbox" name="" id="" value="<%=previewQuestion.getAnswerList().get(j).getAnswer()%>"/></td><%
					  								}
					  								%></tr><%
					  							}
					  							
					  							if(k==1)
					  							{
					  								i++;
					  							}
					  							if(i==0)
					  							{
					  								k=1;
					  							}
					  						}
					  						%></table></td><%
					                      }
					                      else if(questionType==8)
					                      {
					                      %><td width="108" align="left" valign="top">'+
											'<table border="1">'+
											'<%
	
											int k=0;
											for(int i=0;i<previewQuestion.getSubQuestionList().size();)
											{
												if(k==0)
												{
													%><tr><td style="border:none">&nbsp;</td></tr><%
													for(int j=0;j<previewQuestion.getAnswerList().size();j++)
													{
														%><td style="font-size:12px;color:black;padding:2px;text-align:center;"><strong><%=previewQuestion.getAnswerList().get(j).getAnswer()%></strong></td><%
													}
													%></tr><%
												}
												else
												{
													%><tr><td style="font-size:12px;color:black;padding:2px;"><strong><%=previewQuestion.getSubQuestionList().get(i).getSubQuestion()%></strong></td><%
													for(int j=0;j<previewQuestion.getAnswerList().size();j++)
													{
														%><td style="text-align:center;"><input type="text" name="" id="" /></td><%
													}
													%></tr><%
												}
												
												if(k==1)
												{
													i++;
												}
												if(i==0)
												{
													k=1;
												}
											}
											%></table></td><%
					                      } 
										%></tr><%
									}
			                    }
			                    %><tr><td colspan="2">&nbsp;</td></tr>'+
								
								 '</table>'+
								 '</form></div>').appendTo('#lbContent');
				}
		    });
			 $(document).keyup(function(e) { 
        if (e.keyCode == 27) { // esc keycode
			uLightBox.clear();

        }
    });
	      }
		});

</script>