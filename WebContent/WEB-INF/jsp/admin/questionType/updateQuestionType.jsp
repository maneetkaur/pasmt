<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String topMenu = "Admin";
%> 

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Question Type - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
     <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Update Question Type</h2>
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="311" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
            <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <c:if test="${message=='Already Exist'}">
                      <div  style="color: red" align="center">This Culture is already exist</div>
                      </c:if>
                <td width="921" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
               <form:form action="editQuestionType" method="post" modelAttribute="edit_question_type">
                <tr>
                <td height="31" align="left" valign="middle">Subategory <span>*</span></td>
<td colspan="5" align="left" valign="middle"><form:select path="categoryTypeId" class="inptformSelect" style="width:243px;">
                        <form:option value="NONE" label="--Subategory--" />
                      <form:options items="${categoryTypeList}" itemLabel="name" itemValue="id"/>
                        </form:select>
                        <br/><span  id="msg_countryCode" style="color:red"></span>  </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Question Name <span>*</span></td>
<td colspan="5" align="left" valign="middle"><form:input path="questionTypeHeading" class="inptform" onchange="checkQuestionTypeAvail(this.value)"/>
                            <br/>
                          <span  id="msg_type" style="color:red; padding-left: 20px;" ></span></td>
              </tr>
              <tr>
                <td width="209" height="31" align="left" valign="middle">Answer Type<span>*</span></td>
        <br/>
                      <div id="Error_msg" style="color: red" align="center"></div>   
                <td colspan="5" align="left" valign="middle"><form:select path="questionType"  class="inptformSelect" style="width:243px;" disabled="true">
                     <form:option value="NONE" label="--Type--"/>
                     <form:option value="1">Radio Button</form:option>
                     <form:option value="2">Single Select drop down list</form:option>
                     <form:option value="3">Checkboxes</form:option>
                     <form:option value="4">Multi Select drop down list</form:option>
                     <form:option value="5">Input box</form:option>
                     <form:option value="6">Single Select grid</form:option>
                     <form:option value="7">Multi Select grid</form:option>
                     <form:option value="8">Input box grid</form:option>
                     </form:select>
                        <br/><span  id="msg_country" style="color:red"></span>    </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">Question Nature<span>*</span></td>
  <td colspan="5" align="left" valign="middle"><form:select path="questionNature" class="inptformSelect" style="width:243px;" disabled="true">
                        <form:option value="NONE" label="--Nature--" />
                       <form:option value="0">Parent</form:option>
                       <form:option value="1">Child</form:option>
                       <form:option value="2">Parent-Child</form:option>
                        </form:select>
                        <br/><span  id="msg_countryCode" style="color:red"></span>  </td>
              </tr>
                            
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit" onClick="return checkQuestionTypeValidation()"></td>
                    <td width="118" align="center" valign="middle"><a href="questionTypeList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                   </form:form>
                </table></td>
                </tr>
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
            </table>          </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>
<script type="text/javascript">
var questionType=document.getElementById("questionTypeHeading").value;
function checkQuestionTypeValidation()
{
	var status=true;
	if( document.getElementById("categoryTypeId").value=="NONE" || document.getElementById("questionTypeHeading").value=="" ||
		document.getElementById("questionType").value=="NONE" || document.getElementById("questionNature").value=="NONE"  )
	{
		status=false;	
	}
	if(status==false)
	{
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields."
	}
	return status;
}
function checkQuestionTypeAvail(type){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChange; 
		var a=request.open("GET","ValidateQuestionType?QuestionType="+type+"&action=validateType",true);
		request.send(true);	
 	}
	return val;
}
function handleStateChange()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msg_type').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
				document.getElementById('questionTypeHeading').value = questionType;
			}
			request.abort();
			request = null;
}
</script>