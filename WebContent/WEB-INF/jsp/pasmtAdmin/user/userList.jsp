<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
     <%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
	<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%
	String topMenu = "PasmtAdmin";
%>   
<%!
    Integer pagerPageNumber=new Integer(0);
%>   
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>User List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnadduser">
      <h3><a href="sAdminAddUserForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="width:120px;">Search User</h2>
      <form:form modelAttribute="searchUser" action="sAdminUserList" method="post">
	      <label>User Name </label>
	      <form:input path="employeeName" style=" width:120px;"/>
	       <label>User Code</label>
	      <form:input path="userCode" style=" width:120px;"/>
	      <form:label path="status">Status</form:label>
		  <form:select path="status" style=" width:120px;" class="selectdeco">
			<form:option value="NONE" label="--Select--"></form:option>
			<form:option value="2" label="All"/>
			<form:option value="1" label="Active"/>
			<form:option value="0" label="Inactive"/>
		  </form:select>
	      <input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>
       </form:form>
       <h3><a href="#">Advanced Search</a></h3>
      </div>
      </div>
      <div class="searchresult"> 
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1135" border="0" align="center" 
          	cellpadding="0" cellspacing="0">
            <tr>
              <td height="42" colspan="8" align="left" valign="middle" bgcolor="#494c41" class="pagelistinggrn">
              	<table width="1087" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="383" align="left" valign="middle"><h2>User Listing</h2></td>
                  <td width="753">&nbsp;</td>
                  </tr>
                </table></td>
            </tr>
        
            <tr>
              <td width="175" height="42" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">
              	Employee Name </td>
              <td width="125" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Employee Code</td>
              <td width="162" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Department</td>
              <td width="177" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Team</td>
			  <td width="170" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Role</td>
              <td width="133" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Access Role</td>
              <td width="106" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">User Status</td>
              <td width="87" align="left" valign="middle" bgcolor="#778d4c" class="pagelistinggrn">Action</td>
            </tr>
			  <c:if test="${not empty userList}">
              <c:set var="pageUrl" value="sAdminUserList"/>
           	  <c:set var="totalRows" value="${requestScope.total_rows}"/>
           <pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" 
           	isOffset="true">
            <c:forEach items="${userList}" var="ll" varStatus="counter">  
            <pg:item> 
			<c:choose>
             <c:when test="${counter.count%2==0}">
              <tr>
              	<td align="left" valign="middle" class="listingitem">${ll.employeeName}</td>
              	<td align="left" valign="middle" class="listingitem">${ll.userCode}</td>
              	<td align="left" valign="middle" class="listingitem">${ll.department}</td>
                <td align="left" valign="middle" class="listingitem">${ll.team}</td>
                <td align="left" valign="middle" class="listingitem">${ll.role}</td>
              	<td align="left" valign="middle" class="listingitem">${ll.accessRole}</td>
              	<td align="left" valign="middle" class="listingitem">
				  <c:choose>
             		<c:when test="${ll.status==1}">
              			<a href="sAdminUpdateUserStatus?userId=${ll.userId}&status=${ll.status}">
              				Active              			</a>              		</c:when>
               		<c:otherwise>
               			<a href="sAdminUpdateUserStatus?userId=${ll.userId}&status=${ll.status}">
               				Inactive               			</a>               		</c:otherwise>
               	  </c:choose>               	</td>
              	<td align="left" valign="middle" class="listingitem">
              	  <a href="sAdminEditUserForm?userId=${ll.userId}">
              	  	<img src="images/iconedit.png" alt="Edit" width="20" height="23" border="0" title="Edit" />              	  </a>              	</td>                              
              </tr>
             </c:when>
             <c:otherwise>
              <tr>
              	<td align="left" valign="middle" class="listingitem">${ll.employeeName}</td>
              	<td align="left" valign="middle" class="listingitem">${ll.userCode}</td>
              	<td align="left" valign="middle" class="listingitem">${ll.department}</td>
                <td align="left" valign="middle" class="listingitem">${ll.team}</td>
                <td align="left" valign="middle" class="listingitem">${ll.role}</td>
              	<td align="left" valign="middle" class="listingitem">${ll.accessRole}</td>
              	<td align="left" valign="middle" class="listingitem">
				  <c:choose>
             		<c:when test="${ll.status==1}">
              			<a href="sAdminUpdateUserStatus?userId=${ll.userId}&status=${ll.status}">
              				Active              			</a>              		</c:when>
               		<c:otherwise>
               			<a href="sAdminUpdateUserStatus?userId=${ll.userId}&status=${ll.status}">
               				Inactive               			</a>               		</c:otherwise>
               	  </c:choose>               	</td>
              	<td align="left" valign="middle" class="listingitem">
              	  <a href="sAdminEditUserForm?userId=${ll.userId}">
              	  	<img src="images/iconedit.png" alt="Edit" width="20" height="23" border="0" title="Edit" />              	  </a>              	</td>                              
              </tr>
            </c:otherwise>
           </c:choose>
            </pg:item>
           </c:forEach>
           <%@include file="../../include/paging_bar.jsp"%>
           </pg:pager>
          </c:if>
             <tr>
              <td colspan="8" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
           
          </table></td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" ></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
</body>
</html>