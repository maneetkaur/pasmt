<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "PasmtAdmin";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit User - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/pasmtAdmin/userScript.js"></script>
</head>
<body onload="javascript:getInitialValues();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit User</h2>
      <form:form method="POST" action="sAdminEditUser" modelAttribute="editUser" enctype="multipart/form-data">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td width="5018" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:25px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td width="152" height="31" align="left" valign="middle">
                  	&nbsp;
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:hidden path="userId"/>
                  </td>
                </tr>              	              	
                <tr>
                  <td width="152" height="31" align="left" valign="middle">
                  	<form:label path="username">Username<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="username" class="inptform" onchange="checkUsername(this.value);"/>
                  	<br/> 
                  	<span id="msgUsername" style="padding-left: 20px;"></span>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="password">Password<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="password" class="inptform" type="password"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="employeeName">Name<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="employeeName" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="userCode">Code</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="userCode" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="company">Company</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="company" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="department">Department</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:select path="department" class="inptformSelect" style="width:243px;">
                  	  <form:option value="0" label="--Select--"/>
                  	  <form:options items="${departmentList}" itemLabel="name" itemValue="id"/>
                  	</form:select>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="team">Team</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="team" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="role">Role<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:select path="role" class="inptformSelect" style="width:243px;">
                  	  <form:option value="0" label="--Select--"/>
                  	  <form:options items="${roleList}" itemLabel="name" itemValue="id"/>
                  	</form:select>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="accessRole">Access Role<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle" class="radiogroup">
                  	<form:radiobuttons path="accessRole" items="${accessRoleList}" itemLabel="name" itemValue="id"
                  		/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="emailId">Email<span>*</span></form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="emailId" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="phone">Phone</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:input path="phone" class="inptform"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="address">Address</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:textarea path="address" class="inptformmesg"/>
                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle">
                  	<form:label path="status">Status</form:label>
                  </td>
                  <td width="935" align="left" valign="middle">
                  	<form:select path="status" class="inptformSelect" style="width:243px;">
                  	  <form:option value="NONE" label="--Select--"/>
                  	  <form:option value="1" label="Active"/>
                  	  <form:option value="0" label="Inactive"/>                  	
                  	</form:select>
                  </td>
                </tr>                                                                                  
                <tr>
                  <td height="31" align="left" valign="middle">&nbsp;</td>
                  <td align="left" valign="middle">&nbsp;</td>
                </tr>
                
              </table></td>
            </tr>
            
          </table></td>
        </tr>      
        
        
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return userValidation();"></td>
              <td width="118" align="center" valign="middle"><a href="sAdminUserList" class="btnsubmitLink" >Cancel</a></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  <!-- javascript starts -->
  <script type="text/javascript" src="js/common/commonScript.js"></script>
  <script type="text/javascript" src="js/pasmtAdmin/userScript.js"></script>
  <script type="text/javascript">
  	function getInitialValues(){
  		preName = document.getElementById('username').value;
  	}
  	
  	function checkUsername(username){
  		if(preName != username){
  			validateUsername(username);
  		}
  	}
  </script>
  <!-- javascript ends -->
</body>
</html>
