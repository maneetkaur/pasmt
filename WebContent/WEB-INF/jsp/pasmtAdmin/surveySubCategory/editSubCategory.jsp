<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "PasmtAdmin";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Edit Survey Sub Category - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex, nofollow" />
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />

</head>
<body onload="initialize();">
<!--Header Start-->
<div class="headerbox">
  <%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Survey Category</h2>
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > 
		    <form:form modelAttribute="editSurveySubCategory" action="editSurveySubCategory" method="post">
            <table width="1087" border="0" align="right" cellpadding="0" cellspacing="0">
          
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="947" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              <tr>
                <td width="155" height="31" align="left" valign="middle">Category Name<span>*</span></td>
                <td colspan="5" align="left" valign="middle">
                <form:input path="categoryType" class="inptform" disabled="true"/>
                <form:hidden path="subCategoryTypeId"/>
                <form:hidden path="categoryTypeId"/> </td>
              </tr>
              <tr>
                <td width="155" height="31" align="left" valign="middle">Sub Category Name<span>*</span></td>
                <td colspan="5" align="left" valign="middle">
                <form:input path="subCategoryType" class="inptform" onchange="checkName(this.value)"/>
                <br/><span  id="msg_category" style="color:red;padding-left: 20px;"></span></td>
              </tr>
               <!--  display the already existing rows -->
               <tr>
                <td height="31" colspan="6" align="left" valign="middle">
                <table width="1015" border="0" cellspacing="0" cellpadding="0">
                <c:forEach items="${editSurveySubCategory.subCategoryList}" var="lang" varStatus="lStatus">              
                  <tr>
                    <td width="158" align="left" valign="middle">Language</td>                 
                	<td width="302" align="left" valign="middle">
                		<form:input path="subCategoryList[${lStatus.index}].language" class="inptform" disabled="true"/>
                		<form:hidden path="subCategoryList[${lStatus.index}].subCategoryId"/></td>
                    <td width="190" align="left" valign="middle" style="padding-left:10px;">Sub Category Translation</td>
                    <td width="365" align="left" valign="middle">
                    	<form:input path="subCategoryList[${lStatus.index}].subCategoryName" class="inptform"/>                    </td>
                    </tr>
                  </c:forEach>
                </table>
                <form:hidden path="subCategorySize"/>
                <form:hidden path="subCategoryIdList"/>
                </td>
               </tr>
               
               <!-- display the languages for which there is no entry yet -->
               <tr>
                <td height="31" colspan="6" align="left" valign="middle">
                <table width="1015" border="0" cellspacing="0" cellpadding="0">
                <c:forEach items="${editSurveySubCategory.languageList}" var="lang" varStatus="lStatus">
                <c:set var="counter" value="${lStatus.index + editSurveySubCategory.subCategorySize}"></c:set>              
                  <tr>
                    <td width="158" align="left" valign="middle">Language</td>                 
                	<td width="302" align="left" valign="middle">
                		<input type="text" value="${lang.name}" class="inptform" disabled="true">
                		<form:hidden path="subCategoryList[${counter}].language" value="${lang.id}"/>                    </td>
                    <td width="190" align="left" valign="middle" style="padding-left:10px;">Sub Category Translation</td>
                    <td width="365" align="left" valign="middle">
                    	<form:input path="subCategoryList[${counter}].subCategoryName" class="inptform"/>                    </td>
                    </tr>
                  </c:forEach>
                </table></td>
               </tr>             
               <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>             
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
                         
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="checkRowUpdation();"></td>
                    <td width="118" align="center" valign="middle"><a href="surveyCategoryList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="88" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>           
            </table>    
            </form:form>       </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </div>
    </div>
  </div>
  </div>
  <script type="text/javascript" src="js/pasmtAdmin/surveySubCategoryScript.js"></script>
 <script type="text/javascript">
var rowNumber='${editSurveySubCategory.subCategorySize}';
var values=new Array();
function initialize(){
	preType = document.getElementById('subCategoryType').value;
	for(var i=0; i<rowNumber; i++){
		values[i] = document.getElementById('subCategoryList'+i+'.subCategoryName').value;
	}
}
function checkName(categoryType){
	if(preType!=categoryType){
		checkSSubCategoryName(categoryType);
	}
}
var subCategoryIdList;
function checkRowUpdation()
{
	subCategoryIdList=""; 
	for(var i=0;i<rowNumber;i++)
      {
            if(values[i]!=document.getElementById('subCategoryList'+i+'.subCategoryName').value)
            {  
            	subCategoryIdList+=i+",";
            }         
      }
	  document.getElementById("subCategoryIdList").value = subCategoryIdList;
}
</script>
</body>
</html>    