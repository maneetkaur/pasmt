<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Sales";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit Proposal - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<!-- date picker starts -->
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.date.js"></script>
		
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="resources/datepicker/scripts/jquery.date.js"></script>
        <script type="text/javascript" src="resources/datepicker/scripts/datepicker.js"></script>
        <link rel="stylesheet" href="resources/datepicker/css/datepicker.css" type="text/css" media="screen" /> 
<!--  date picker ends -->
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
</head>
<body onLoad="editInitialize(); getRowValues();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit Proposal</h2>
      <form:form method="POST" action="editProposal" modelAttribute="editProposal" enctype="multipart/form-data">      
      <table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
          	<table style="margin-left:5px;" width="1100" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span>
              <span style="float: right;">
              	<a href="downloadPDFProposal?proposalId=${editProposal.proposalId}">
                			<img alt="Download PDF" src="images/btndownload-pdf.png">
                </a>
              </span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
            <tr>
              <td height="31" colspan="6" align="left" valign="top"><table style="margin-left:20px;" width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
              		<td><form:hidden path="proposalId"/>
              		</td>
              	</tr>
              	<tr>
              	  
              	</tr>
                <tr>   
                  <td width="157" height="31" align="left" valign="middle"><form:label path="proposalNumber">Proposal Number<span>*</span></form:label>
                  </td>
                  <td width="285" align="left" valign="middle">
                  	<form:input path="proposalNumber"  class="inptform" disabled="true"/>          
                  </td>               
                  <td width="284" height="31" align="left" valign="middle"><form:label path="bid.bidCode">Bid Number<span>*</span></form:label>
                  </td>                  
                  <td width="361" align="left" valign="middle">
                  	<form:input path="bid.bidCode"  class="inptform" disabled="true"/>          
                  </td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td width="0" height="31" colspan="6" align="left" valign="top">           
              <table style="margin-left:10px; " width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              	<tr>
                  <td height="31" align="left" valign="top">
                  	  <table width="466" border="0" cellspacing="0" cellpadding="0">
                        
                        <tr>
                          <td align="left" valign="top"><table style="margin-left:10px; " width="1051" border="0" align="left" cellpadding="0" cellspacing="0">
                            
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bid.bidClient">Client Name</form:label>                              </td>
                              <td width="285" colspan="2" align="left" valign="middle"><form:input path="bid.bidClient" class="inptform" disabled="true"/>                            </td>
                              <td width="281" height="31" align="left" valign="middle"><form:label path="clientContact">Client Sales Contact<span>*</span></form:label>                              </td>
                              <td width="328" colspan="2" align="left" valign="middle"><form:select path="clientContact" class="inptformSelect" style="width:243px;">
                                  <form:option value="0" label="--Select--"/>
                                  <form:options items="${contactList}" itemLabel="name" itemValue="id"/>
                                </form:select>                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bid.bidName">Bid Name</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="bid.bidName" class="inptform" disabled="true"/>                              </td>
                              <td height="31" align="left" valign="middle"><form:label path="clientProjectManager">Client Project Manager<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:select path="clientProjectManager" class="inptformSelect" style="width:243px;">
                                  <form:option value="0" label="--Select--"/>
                                  <form:options items="${contactList}" itemLabel="name" itemValue="id"/>
                                </form:select>                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bid.projectType">Project Type</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="bid.projectType" class="inptform" disabled="true"/>                              </td>
                              <td height="31" align="left" valign="middle"><form:label path="purchaseOrderNo">Purchase Order No.<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="purchaseOrderNo" class="inptform"/>                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bid.productType">Product Type</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="bid.productType" class="inptform" disabled="true"/>                              </td>
                              <td height="31" align="left" valign="middle"><form:label path="targetAudience">Target Audience<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="targetAudience" class="inptform"/>                              </td>
                            </tr>
                            <tr>
                              <td width="157" height="31" align="left" valign="middle"><form:label path="bid.billingCurrency">Billing Currency</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="bid.billingCurrency" class="inptform" disabled="true"/>                              </td>
                              <td height="31" align="left" valign="middle"><form:label path="notesOnTargeting">Notes on Targeting<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="notesOnTargeting" class="inptform"/>                              </td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bid.accountManager">Account Manager</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="bid.accountManager" class="inptform" disabled="true"/>                              </td>
                              <td height="31" align="left" valign="middle"><form:label path="startDate">Start Date<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><input class="format-y-m-d divider-dash inptform" type="text" readonly="readonly" name="startDate" 
              			id="startDate"/></td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle"><form:label path="bid.bidDate">Bidding Date</form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><form:input path="bid.bidDate" class="inptform" disabled="true"/>                              </td>
                              <td height="31" align="left" valign="middle"><form:label path="endDate">End Date<span>*</span></form:label>                              </td>
                              <td colspan="2" align="left" valign="middle"><input class="format-y-m-d divider-dash inptform" type="text" readonly="readonly" name="endDate" 
              			id="endDate"/>                              </td>
                            </tr>
                            <tr>                              
                              <td height="31" align="left" valign="middle"><form:label path="projectValue">Project Value<span>*</span></form:label>                              </td>
                              <td align="left" width="40" valign="middle"><input type="text" id="currPV" style="width:40px;" class="inptform" disabled="true"/>                              </td>
                              <td align="left" valign="middle"><form:input path="projectValue" style="width:180px; text-align:left;"  class="inptform" disabled="true"/></td>
                              <td height="31" align="left" valign="middle"><form:label path="projectMinimum">Project Minimum<span>*</span></form:label>                              </td>
                              <td align="left" width="40" valign="middle"><input type="text" id="currPM" style="width:40px;" class="inptform" readonly="true"/>                              </td>
                              <td align="left" valign="middle"><form:input path="projectMinimum" style="width:180px; text-align:left;" class="inptform" onchange="totalCost();"/></td>
                            </tr>
                            <tr>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                              <td height="31" align="left" valign="middle">&nbsp;</td>
                              <td colspan="2" align="left" valign="middle">&nbsp;</td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                  	</td>
                  </tr>                
                <tr>
                  <td colspan="10" align="left" valign="top">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
            
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" > <h2> Requirement</h2></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#778D4C" ><table width="1123" border="0" align="right" cellpadding="0" cellspacing="0" class="whitetxt">
            <tr>
              <td style="padding-left:0px;" width="91" align="left" valign="middle">Country</td>
              <td width="91" align="left" valign="middle">Services</td>
              <td width="93" align="left" valign="middle">Audience Type</td>
              <td width="130" align="left" valign="middle">Description</td>
              <td width="84" align="left" valign="middle">Sample Size</td>
              <td width="94" align="left" valign="middle">Incidence</td>
              <td width="85" align="left" valign="middle"># Questions</td>
              <td width="104" align="left" valign="middle">LOI</td>              
              <td width="96" align="left" valign="middle">Unit Price</td>
              <td width="96" align="left" valign="middle">Setup Fee</td>
              <td width="159" align="left" valign="middle">Total Cost</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >
          <table width="1130" border="0" align="right" cellpadding="0" cellspacing="0">
            <tr>
              <td align="left" valign="top">
              <table style="margin-left:10px; " width="1125" border="0" cellspacing="0" cellpadding="0">                               
                <tr>
                <c:choose>
                <c:when test="${editProposal.proposalRequirements.size()>0}">
                <td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >             
                   <tbody id="requirement">
                   <c:forEach items="${editProposal.proposalRequirements}" var="req" varStatus="status">
                    <tr>
                      <td><form:hidden path="proposalRequirements[${status.index}].requirementId" id="r_id${status.index}"/> </td>
                      <td width="93" height="38" align="left" valign="middle">
                      	<form:select path="proposalRequirements[${status.index}].country" id="r_country${status.index}" class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="93" align="left" valign="middle">
                      	<form:select path="proposalRequirements[${status.index}].services" id="r_service${status.index}" 
                      		class="inptform2" style="width:80px;" onchange="populateValues(this.value, this.id);">
                      		<form:option value="0" label="--Select--"/>                      		
                      		<form:option value="1" label="Data Processing"/>
                      		<form:option value="2" label="Sample"/>
                      		<form:option value="3" label="Survey Programming"/>                    
                      		<form:option value="4" label="Translation"/>                      		
                      	</form:select>
                      </td>
                      <td width="94" align="left" valign="middle">
                      	<form:select path="proposalRequirements[${status.index}].targetAudience" id="r_audience${status.index}" class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="B2B"/>
                      		<form:option value="2" label="B2C"/>
                      		<form:option value="3" label="HC"/>
                      		<form:option value="4" label="NA"/>
                      	</form:select>
                      </td>
                      <td width="131" align="left" valign="middle">
                      	<form:input path="proposalRequirements[${status.index}].description" id="r_desc${status.index}" class="inptform2" style="width:110px;"/>
                      </td>
                      <td width="86" align="left" valign="middle">
                      	<form:input path="proposalRequirements[${status.index}].sampleSize" id="r_sample${status.index}" class="inptform2" size="10" style="width:65px;" onchange="addSample();rowCostFun(this.id,2);"/>
                      </td>
                      <td width="94" align="left" valign="middle">                      	
                      	<form:select path="proposalRequirements[${status.index}].incidence" id="r_incidence${status.index}" class="inptform2" style="width:80px;" >
                      		<form:option value="-1" label="--Select--"></form:option>
                      		<form:option value="101" label="NA"/>
                      		<form:option value="0" label="<1%"></form:option>
                      		<c:forEach var="index" begin="1" end="100">
                      			<form:option value="${index}" label="${index}%"></form:option>
                      		</c:forEach>
                      	</form:select>
                      </td>
                      <td width="86" align="left" valign="middle">
                      	<form:input path="proposalRequirements[${status.index}].noOfQuestions" id="r_question${status.index}" class="inptform2" style="width:65px;" size="10"/>
                      </td>
                      <td width="108" align="left" valign="middle">
                      	<form:input path="proposalRequirements[${status.index}].loi" id="r_loi${status.index}" class="inptform2" size="10" style="width:43px;"/>
                      	<input type="text" id="minutes" value="minutes" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/>                        
                      </td>
                      <td width="98" align="left" valign="middle">
                      <input id="currencyValF${status.index}" type="text" class="inptform2" style="width:25px; font-size:11px;" readonly="true"/>
                      	<form:input path="proposalRequirements[${status.index}].quotedCpi" id="r_feas${status.index}" class="inptform2" style="width:43px;" size="10" onchange="rowCostFun(this.id,0);"/>
                      </td>
                      <td width="100" align="left" valign="middle">
                        <input id="currencyVal${status.index}" type="text" class="inptform2" style="width:25px; font-size:11px;" readonly="true"/>
                      	<form:input path="proposalRequirements[${status.index}].setupFee" class="inptform2" id="r_cpi${status.index}" size="10" style="width:43px;" onchange="rowCostFun(this.id,1);"/>
                      </td>
                      <td width="93" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="r_total${status.index}" size="10" style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="63" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                    </c:forEach>
                   </tbody>
                  </table></td>                                                               
                  </c:when>
                  <c:otherwise>                 
                  	<td width="1123" colspan="11" align="left" valign="top">
                  <table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                   <tbody id="requirement">
                    <tr>
                      <td width="93" height="38" align="left" valign="middle">
                      	<form:select path="proposalRequirements[0].country" id="r_country0" class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:options items="${countryList}" itemLabel="name" itemValue="id"/>
                      	</form:select>
                      </td>
                      <td width="93" align="left" valign="middle">
                      	<form:select path="proposalRequirements[0].services" id="r_service0" class="inptform2" 
                      		style="width:80px;" onchange="populateValues(this.value, this.id);">
                      		<form:option value="0" label="--Select--"/>                      		
                      		<form:option value="1" label="Data Processing"/>
                      		<form:option value="2" label="Sample"/>
                      		<form:option value="3" label="Survey Programming"/>                    
                      		<form:option value="4" label="Translation"/>                      		
                      	</form:select>
                      </td>
                      <td width="94" align="left" valign="middle">
                      	<form:select path="proposalRequirements[0].targetAudience" id="r_audience0" class="inptform2" style="width:80px;">
                      		<form:option value="0" label="--Select--"/>
                      		<form:option value="1" label="B2B"/>
                      		<form:option value="2" label="B2C"/>
                      		<form:option value="3" label="HC"/>
                      		<form:option value="4" label="NA"/>
                      	</form:select>
                      </td>
                      <td width="131" align="left" valign="middle">
                      	<form:input path="proposalRequirements[0].description" id="r_desc0" class="inptform2" style="width:110px;"/>
                      </td>
                      <td width="86" align="left" valign="middle">
                      	<form:input path="proposalRequirements[0].sampleSize" id="r_sample0" class="inptform2" style="width:65px;" size="10" onchange="addSample();rowCostFun(this.id,2);"/>
                      </td>
                      <td width="94" align="left" valign="middle">                      	
                      	<form:select path="proposalRequirements[0].incidence" id="r_incidence0" class="inptform2" style="width:80px;" >
                      		<form:option value="-1" label="--Select--"></form:option>
                      		<form:option value="101" label="NA"/>
                      		<form:option value="0" label="<1%"></form:option>
                      		<c:forEach var="index" begin="1" end="100">
                      			<form:option value="${index}" label="${index}%"></form:option>
                      		</c:forEach>
                      	</form:select>
                      </td>
                      <td width="86" align="left" valign="middle">
                      	<form:input path="proposalRequirements[0].noOfQuestions" id="r_question0" class="inptform2" style="width:65px;" size="10"/>
                      </td>
                      <td width="108" align="left" valign="middle">
                      	<form:input path="proposalRequirements[0].loi" id="r_loi0" class="inptform2" size="10" style="width:43px;"/>
                      	<input type="text" id="minutes" value="minutes" class="inptform2" style="width:38px; font-size: 11px; padding-left: 1px;" readonly="true"/>                     
                      </td> 
                      <td width="98" align="left" valign="middle">
                      	<input id="currencyValF0" type="text" class="inptform2" style="width:25px; font-size:11px;" readonly="true"/>
                      	<form:input path="proposalRequirements[0].quotedCpi" id="r_feas0" class="inptform2" size="10" style="width:43px;" onchange="addFeasibility();rowCostFun(this.id,0);"/>
                      </td>                     
                      <td width="100" align="left" valign="middle">
                        <input id="currencyVal0" type="text" class="inptform2" style="width:25px; font-size:11px;" readonly="true"/>
                      	<form:input path="proposalRequirements[0].setupFee" class="inptform2" id="r_cpi0" size="10" style="width:43px;" onchange="rowCostFun(this.id,1);"/>
                      </td>                      
                      <td width="93" align="left" valign="middle">
                      	<input name="textfield10" type="text" class="inptformdis" id="r_total0" size="10" style="font-weight: bold; height: 30px;" readonly />
                      </td>
                      <td width="63" align="left" valign="middle" style="display: block;"></td>
                    </tr>
                   </tbody>
                  </table></td>               
                  </c:otherwise>
                  </c:choose>
                  <td>
                  	<form:hidden path="propReqmtSize"/>
                  	<form:hidden path="reqmtIdList"/>
                  </td>
                </tr>
                                
                <tr>
                  <td height="38" colspan="11" align="left" valign="top" ><input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit" /></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top"><table width="1121" border="0" align="left" cellpadding="0" cellspacing="0"  >
                      <tr class="amboseff">
                        <td width="409" height="38" align="left" valign="middle" bgcolor="#bbcb9b" style="padding-left:5px;"><strong>Grand Total</strong></td>
                        <td width="110" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input name="textfield10" type="text" class="inptformdis" style="width: 65px;" 
                        	id="sampleSum" size="10" readonly />
                        </td>
                        <td width="178" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="92" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="177" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco"></td>
                        <td width="108" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">
                        	<input name="textfield14" type="text" class="inptformdis" id="totalSum" size="10" readonly/>
                        </td>
                        <td width="47" align="left" valign="middle" bgcolor="#bbcb9b" class="sumdeco">&nbsp;</td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                	<td height="30" colspan="19" align="left" valign="top"><strong>Project Specs<span>*</span></strong></td>
                </tr>
                <tr>
                  <td colspan="10" align="left" valign="top">
                  	<form:textarea path="bid.projectSpecs" class="inptformmesg2"/>                  </td>
                </tr> 
                <tr>
                  <td colspan="11" align="left" valign="top">&nbsp;</td>
                </tr>                     
                <tr>
                  <td colspan="11" align="left" valign="top"><strong>Document </strong></td>
                </tr>
                <tr>
                  <td colspan="11" align="left" valign="top">
                  	<table width="500" border="0" cellspacing="0" cellpadding="0">
                  	<c:forEach items="${editProposal.bid.documentNames}" var="dName">
                    <tr>
                      <td height="29" colspan="2" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                      	<a href="downloadBidFile?bidId=${editProposal.bid.bidId}&fileName=${dName}" class="grytxt">
                      		${dName}
                      	</a>
                      </td>                      
                    </tr>
                   </c:forEach>
                   <c:forEach items="${editProposal.documentNames}" var="dName">
                    <tr>
                      <td height="29" colspan="2" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                      	<a href="downloadProposalFile?proposalId=${editProposal.proposalId}&fileName=${dName}" class="grytxt">
                      		${dName}
                      	</a>
                      </td>                      
                    </tr>
                   </c:forEach>                   
                  </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" align="left" valign="top">
                  <table id="bid_doc" width="267" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="29" colspan="2" align="left" valign="middle">
                      	<form:input path="documentList[0].file" type="file" id="document0"
                      		accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                      		onchange="documentValidation(this.value, this.id);"/>                      
                      </td>
                    </tr>                   
                  </table>
                 </td>
                 <td>
                	<span id="validation_document" style="font-size:16px;"></span>
                </td>
                </tr>               
                <tr>
                	<td height="38" colspan="11" align="left" valign="top" >
                		<input type="button" name="Submit" id="addDocs" value="Add More +" class="btnsubmit" />
                	</td>
                </tr>
                <tr>
                	<td>
                		&nbsp;
                	</td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" >&nbsp;</td>
        </tr>
        <tr>
          <td height="37" align="left" valign="top" bgcolor="#D2DCBF" ><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="checkRowUpdate();return proposalValidation()"></td>
              <td width="118" align="center" valign="middle"><input type="reset" name="Submit3" id="Submit3" value="Clear" class="btnsubmit"></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>
      </form:form>
      </div>
    </div>
  </div>
  </div>
  
<!-- javascript starts here -->
<script type="text/javascript" src="js/sales/proposalScript.js"></script>
<script type="text/javascript">
var btn_close='<td width="57" align="left" valign="middle" id="btn-close" class="btn-remove">' + 
	'<a href="javascript:void(0)"><img src="images/btncross.png" width="18" height="17" border="0" />' + 
	'</a></td>';
var btn_close2='<td width="38" align="left" valign="middle" id="btn-close2" class="btn-remove">' + 
	'<a href="javascript:void(0)"><img src="images/btncross.png" width="18" height="17"></a></td>';
	
var reqSize = '${editProposal.proposalRequirements.size()}';
var startDateVal = '${editProposal.startDate}';
var endDateVal = '${editProposal.endDate}';
var i;
var doc_i=1;
var doNotDel=false;
var count=0;

$(document).ready(function(){
	
	//Add more functionality for additional contacts
		$("#addMore").click(function() {	
			 var tr_temp=$("#requirement tr:first").clone();
			 tr_temp.find("#r_country0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_country' + i},
	    				'name': function(_, name) { return 'proposalRequirements['+ i +'].country'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_service0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_service' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].services'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_audience0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_audience' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].targetAudience'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#r_desc0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_desc' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].description'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_sample0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_sample' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].sampleSize'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_incidence0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_incidence' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].incidence'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_question0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_question' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].noOfQuestions'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_loi0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_loi' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].loi'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_feas0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_feas' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].quotedCpi'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#r_cpi0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_cpi' + i},
	    				'name': function(_, name) { return 'proposalRequirements[' + i + '].setupFee'},
	    				'value': ''
	  			});
				}).end();			 			 
			 tr_temp.find("#r_total0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_total' + i},	    			
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#currencyVal0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'currencyVal' + i}
	  			});
				}).end();
			 tr_temp.find("#r_id0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'r_id' + i},
	    				'name': '',
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close);
			 tr_temp.appendTo("#requirement");
			 i++;			 			
		});
			
		//add more functionality for signedIO files
		$("#addDocs").click(function() {						
			 var tr_sign_temp=$("#bid_doc tr:first").clone();			
			 tr_sign_temp.find("#document0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'document' + doc_i},
	    				'name': function(_, name) { return 'documentList['+ doc_i +'].file'},
	    				'value': ''
	  			});
				}).end();
			 
			 tr_sign_temp.append(btn_close2);
			 tr_sign_temp.appendTo("#bid_doc");
			 doc_i++;			 			
		});
		
		$("#btn-close").live('click',function(){
			doNotDel=false;
			count=0;
			
			//To fetch the values of the Select Fields
			$(this).parent().find("select").each(function(){
				var value = $(this).val();
				var id=$(this).attr('id');
				if(id.indexOf("r_incidence")==-1){
					if(value!="0"){
						doNotDel=true;
						return false;		//breaks the .each loop
					}
				}
				else{
					if(value!="-1"){
						doNotDel=true;
						return false;		//breaks the .each loop
					}
				}
			});
			
			//To fetch the values of the Input Fields
			$(this).parent().find("input").each(function(){
				count++;
				var value = $(this).val();
				var id=$(this).attr('id');
				//check if count is 9, then the total column is being accessed
				if(id.indexOf("r_total")==-1){
					//Skip the minutes and currency column
					if(id.indexOf("minutes")==-1 && id.indexOf("currencyVal")==-1 && id.indexOf("currencyValF")==-1){
						if(value!=""){
							doNotDel=true;
							return false;		//breaks the .each loop
						}						
					}
				}
			});

			if(doNotDel==false){
				$(this).parent().remove();	
			}				
		});
		
		$("#btn-close2").live('click',function(){
			$(this).parent().remove();			
		});
		
});

var values=new Array();
function getRowValues(){		
	for(var j=0;j<reqSize;j++)
    {
        values[j]=new Array();
        values[j][0]=document.getElementById('r_country'+j).value;
        values[j][1]=document.getElementById('r_service'+j).value;			
		values[j][2]=document.getElementById('r_audience'+j).value;
		values[j][3]=document.getElementById('r_desc'+j).value;
		values[j][4]=document.getElementById('r_sample'+j).value;
		values[j][5]=document.getElementById('r_incidence'+j).value;
		values[j][6]=document.getElementById('r_question'+j).value;
		values[j][7]=document.getElementById('r_loi'+j).value;
		values[j][8]=document.getElementById('r_feas'+j).value;
		values[j][9]=document.getElementById('r_cpi'+j).value;			
    }
}

var reqmtIdList;
function checkRowUpdate(){
	reqmtIdList="";  
	for(var k=0;k<reqSize;k++)
      {
            if(values[k][0]!=document.getElementById('r_country'+k).value || values[k][1]!=document.getElementById('r_service'+k).value || 
				values[k][2]!=document.getElementById('r_audience'+k).value || values[k][3]!=document.getElementById('r_desc'+k).value || 
				values[k][4]!=document.getElementById('r_sample'+k).value  || values[k][5]!=document.getElementById('r_incidence'+k).value ||
				values[k][6]!=document.getElementById('r_question'+k).value  || values[k][7]!=document.getElementById('r_loi'+k).value ||
				values[k][8]!=document.getElementById('r_feas'+k).value  || values[k][9]!=document.getElementById('r_cpi'+k).value)
            {  
            	reqmtIdList+=k+",";
            }         
      }
	  document.getElementById("reqmtIdList").value = reqmtIdList;
}
</script>

<!-- javascript ends here -->
</body>
</html>
