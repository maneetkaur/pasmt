<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Sales";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Proposal List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<style type="text/css">
.searchbar label {
padding-left: 12px;
}
.btnsearch{
margin-left: 10px;}
</style>
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaaddproposal">
      <h3><a href="addProposalForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:15px; width: 112px;">Search Proposal</h2>
      	<form:form method="POST" modelAttribute="searchProposal" action="proposalList">
      		<form:label path="bid.bidName">Proposal Name</form:label>
      		<form:input path="bid.bidName" style="width:120px;"/>
			<form:label path="proposalNumber">Proposal Number  </form:label>
			<form:input path="proposalNumber" style="width:120px;"/>
			<form:label path="bid.accountManager">Account Manager</form:label>
        	<form:select path="bid.accountManager" style=" width:120px;" class="selectdeco">
		        <form:option value="0" label="--Select--"/>
		        <form:options items="${managerList}" itemLabel="name" itemValue="id"/>                
		    </form:select>
       		<input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>       		     		
      	</form:form>
      
      </div>
      </div>
      <div class="searchresult2">
      <h2>Proposal Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="113" align="left" valign="middle">Proposal Number</td>
                    		<td width="284" align="left" valign="middle">Proposal Name</td>
                    		<td width="280" align="left" valign="middle">Client Name</td>
		                    <td width="110" align="left" valign="middle">Project Type</td>
		                    <td width="190" align="left" valign="middle">Account Manager</td>
		                    <td width="110" align="left" valign="middle">Created On</td>
                  		  </tr>
              			</table>
              		  </td>
            		</tr>
            	<c:if test="${not empty proposalList}">
      			  <c:set var="pageUrl" value="proposalList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">            		
            	<%--start --%>
            	<pg:param name="bid.bidName" value="${param['bid.bidName']}"/>
            	<pg:param name="proposalNumber" value="${param.proposalNumber}"/>
            	<pg:param name="bid.accountManager" value="${param['bid.accountManager']}"/>
		       	<c:forEach items="${proposalList}" var="proVar" varStatus="pStatus">
		       	
					<pg:item>       	
				      <c:if test="${pStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editProposalForm?proposal_id=${proVar.proposalId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${proVar.proposalNumber}</td>
                      			  <td width="284" align="left" valign="middle">${proVar.bid.bidName}</td>
			                      <td width="280" align="left" valign="middle">${proVar.bid.bidClient}</td>
			                      <td width="110" align="left" valign="middle">${proVar.bid.projectType}</td>
			                      <td width="190" align="left" valign="middle">${proVar.bid.accountManager}</td>
			                      <td width="110" align="left" valign="middle">${proVar.createdOn}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${pStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editProposalForm?proposal_id=${proVar.proposalId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${proVar.proposalNumber}</td>
                      			  <td width="284" align="left" valign="middle">${proVar.bid.bidName}</td>
			                      <td width="280" align="left" valign="middle">${proVar.bid.bidClient}</td>
			                      <td width="110" align="left" valign="middle">${proVar.bid.projectType}</td>
			                      <td width="190" align="left" valign="middle">${proVar.bid.accountManager}</td>
			                      <td width="110" align="left" valign="middle">${proVar.createdOn}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    