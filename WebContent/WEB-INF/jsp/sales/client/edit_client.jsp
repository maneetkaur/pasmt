<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String topMenu = "Sales";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit Client - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<!-- tabbed page -->
<link type="text/css" rel="stylesheet" href="resources/tabs/css/responsive-tabs.css" />
<link type="text/css" rel="stylesheet" href="resources/tabs/css/style.css" />
<!-- tabbed page include ends -->
</head>
<body onLoad="javascript:getRowValues();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit Client</h2>
        <div id="horizontalTab">
        <ul>
            <li><a href="#tab-1">Add Client Details</a></li>
            <li><a href="#tab-2">Terms and Conditions</a></li>
             
        </ul>
		<form:form method="POST" action="editClient" modelAttribute="edit_client" enctype="multipart/form-data">
        <div id="tab-1" style="background-color:#D2DCBF;">
            <p style="margin-left:0px;">
      <table style="margin-top:1px; " width="1087" border="0" cellspacing="0" cellpadding="0">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table style="margin-left:10px;" width="1087" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="972" colspan="5" align="left" valign="middle">&nbsp;                	
                	<form:hidden path="client_id"/>
                	</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="client_name">Client Name <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="client_name" class="inptform" onchange="checkName(this.value);"/>
                  <br/> 
                  <span id="msgClientName" style="padding-left: 20px;"></span>
                </td>
              </tr>
              <tr>
                <td width="158" height="31" align="left" valign="middle">
                <form:label path="client_code">Client Code <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:input path="client_code" class="inptform" onchange="checkCode(this.value);"/>
                	<br/> 
                  	<span id="msgClientCode" style="padding-left: 20px;"></span>                	
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="region">Region <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
	                <form:select path="region" class="inptformSelect" style="width:243px;">
	                	<form:option value="0" label="--Select--"/>
	                	<form:option value="1" label="North America"/>
	                	<form:option value="2" label="APAC"/>
	                	<form:option value="3" label="Europe"/>
	                	<form:option value="4" label="LatAM"/>
	                	<form:option value="5" label="Africa"/>
	                </form:select>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="client_type">Client Type <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="client_type" class="inptformSelect" style="width:243px;">
	                	<form:option value="0" label="--Select--"/>
	                	<form:option value="1" label="Research Clients"/>
	                	<form:option value="2" label="End Clients"/>
	                	<form:option value="3" label="Panel Companies"/>
	                	<form:option value="4" label="Consulting Firms"/>
	                	<form:option value="5" label="Panel Aggregators"/>
	                	<form:option value="6" label="Other"/>
	                </form:select> 
				</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="acct_manager_name">IRB Account Manager<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="acct_manager_name" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${managerList}" itemValue="id" itemLabel="name"/>
                	</form:select>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Primary Contact</strong></td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="53" height="57" align="left" valign="middle">
                    <form:label path="primaryContact.name">Name<span>*</span></form:label></td>
                    <td width="150" align="left" valign="middle">
                    	<form:input path="primaryContact.name" class="inptform" style="width:130px;"/>                    
                    </td>
                    <td width="88" align="center" valign="middle"><form:label path="primaryContact.jobTitle">Job Title<span>*</span></form:label></td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="primaryContact.jobTitle" class="inptform" style="width:130px;"/>                    	
                    </td>
                    <td width="73" align="center" valign="middle"><form:label path="primaryContact.email">Email<span>*</span></form:label></td>
                    <td width="142" align="left" valign="middle">
                    	<form:input path="primaryContact.email" class="inptform" style="width:130px;"/>                    	
                   	</td>
                    <td width="96" align="center" valign="middle"><form:label path="primaryContact.phoneNo">Phone No.<span>*</span></form:label></td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="primaryContact.phoneNo" class="inptform" style="width:130px;"/>                    	
                    </td>
                    <td width="88" align="center" valign="middle"><form:label path="primaryContact.extno">Ext No.</form:label></td>
                    <td width="101" align="left" valign="middle">
                    	<form:input path="primaryContact.extno" class="inptform" style="width:65px;"/>          	
                    </td>
                    <td width="60" align="left" valign="middle">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td height="19" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Additional Contacts</strong></td>
                </tr>
              <tr>
               <c:if test="${edit_client.additionalContacts.size()>0}">
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">                
                 <tbody id="addContacts">                 
                  <c:forEach items="${edit_client.additionalContacts}" var="ac" varStatus="status">                              
                  <tr>
                    <td width="0"><form:hidden path="additionalContacts[${status.index}].id" id="add_id${status.index}"/></td>
                    <td width="53" height="57" align="left" valign="middle">Name  </td>
                    <td width="153" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].name" id="add_name${status.index}" class="inptform" style="width:130px;"/>                    	
                    </td>
                    <td width="89" align="center" valign="middle">Job Title  </td>
                    <td width="121" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].jobTitle" class="inptform" id="add_job${status.index}" style="width:130px;"/>
                    </td>
                    <td width="78" align="center" valign="middle">Email  </td>
                    <td width="130" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].email" class="inptform" id="add_email${status.index}" style="width:130px;"/>
                    </td>
                    <td width="98" align="center" valign="middle">Phone No.  </td>
                    <td width="120" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].phoneNo" class="inptform" id="add_phone${status.index}" style="width:130px;"/>
                    </td>
                    <td width="92" align="center" valign="middle">Ext No. </td>
                    <td width="90" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].extno" class="inptform" id="add_ext${status.index}" style="width:65px;"/>                    	
                    </td>
                    <td width="60" align="left" valign="middle" style="display: block;"></td>                    
                  </tr>
                  </c:forEach>                  
                </tbody>                  
                </table></td>
                </c:if>
                <c:if test="${edit_client.additionalContacts.size()==0}">
	               <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">                
	                 <tbody id="addContacts">                 	                                                
	                  <tr>
	                  <td>
	                  </td>	                  
	                    <td width="53" height="57" align="left" valign="middle">Name  </td>
	                    <td width="153" align="left" valign="middle">
	                    	<form:input path="additionalContacts[0].name" id="add_name0" class="inptform" style="width:130px;"/>                    	
	                    </td>
	                    <td width="89" align="center" valign="middle">Job Title  </td>
	                    <td width="121" align="left" valign="middle">
	                    	<form:input path="additionalContacts[0].jobTitle" class="inptform" id="add_job0" style="width:130px;"/>
	                    </td>
	                    <td width="78" align="center" valign="middle">Email  </td>
	                    <td width="130" align="left" valign="middle">
	                    	<form:input path="additionalContacts[0].email" class="inptform" id="add_email0" style="width:130px;"/>
	                    </td>
	                    <td width="98" align="center" valign="middle">Phone No.  </td>
	                    <td width="120" align="left" valign="middle">
	                    	<form:input path="additionalContacts[0].phoneNo" class="inptform" id="add_phone0" style="width:130px;"/>
	                    </td>
	                    <td width="92" align="center" valign="middle">Ext No. </td>
	                    <td width="90" align="left" valign="middle">
	                    	<form:input path="additionalContacts[0].extno" class="inptform" id="add_ext0" style="width:65px;"/>                    	
	                    </td>
	                    <td width="60" align="left" valign="middle" style="display: block;"></td>                    
	                  </tr>	                                
	                </tbody>                  
	                </table></td>
                </c:if>
                <td>
                	<form:hidden path="addContactsSize"/>
                	<form:hidden path="contactIdList"/>
                </td>
                </tr>
                          
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit"></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:8px;"><form:label path="address">Address<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:textarea path="address" class="inptformmesg"/>                	
                </td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="city">City<span>*</span></form:label></td>
                <td align="left" valign="middle"><form:input path="city" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="state">State<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="state" class="inptform"/></td>
              </tr>
              <tr>
               <td height="31" align="left" valign="middle"><form:label path="postal_code">Postal Code<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="postal_code" class="inptform"/></td>
              </tr>
              <tr>
              <td height="31" align="left" valign="middle"><form:label path="country_name">Country<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="country_name" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${countryList}" itemValue="id" itemLabel="name"/>
                	</form:select>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="websiteUrl">Website URL<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="websiteUrl" class="inptform" onchange="checkUrl(this.value);"/>
                	<br/> 
                  	<span id="msgWebUrl" style="padding-left: 20px;"></span>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="phoneNo">Phone No.</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="phoneNo" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="faxNo">Fax No.</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="faxNo" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="rfqEmail">RFQ Email</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="rfqEmail" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              <tr>
                <td height="25" align="left" valign="top">Signed IO<font size="2px;">(Image/PDF)</font></td>
                <td colspan="3" align="left" valign="top">
                <table width="270" border="0" cellpadding="0" cellspacing="0">
                	<c:forEach items="${edit_client.signedIoNames}" var="sName">                	                	
                		<tr>
                			<td width="270" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadClientFile?client_id=${edit_client.client_id}&fileName=${sName}&documentType=0" class="grytxt">
                					${sName}
                				</a>
                			</td>                			
               			  </tr>                	                	
                	</c:forEach>
                	<tr>
                		<td colspan="2" align="left" valign="top" style="padding-top: 5px;">
                		<table width="266" border="0" cellspacing="0" cellpadding="0">
		                 <tbody id="addSigned">
		                  <tr>
		                    <td width="218">                    	                    
		                    	<form:input path="signedIOs[0].file" type="file" id="signedIO0"
		                    		accept="image/*,application/pdf" onchange="signedValidation(this.value, this.id);"/></td>                                        
		                  </tr>
		                 </tbody>                                                    
                		</table>                		
                		</td>
                		
                	</tr>
                </table>
                </td>
                <td>
		          <span id="validation_signed" style="font-size:16px;"></span>
		        </td>
              </tr>
               <tr>
                    <td height="39" align="center" valign="top">&nbsp;</td>
                    <td align="left" valign="top" style="padding-top: 3px;"><a href="javascript:void(0)" id="addSignedBtn" class="grytxt">+Add More</a></td>                    
                  </tr>
              <tr>
                <td height="25" align="left" valign="top">Guideline Documents</td>
                <td align="left" valign="top">
                <table width="270" border="0" cellpadding="0" cellspacing="0">
                	<c:forEach items="${edit_client.guidelineNames}" var="gName">                	                	
                		<tr>
                			<td width="270" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadClientFile?client_id=${edit_client.client_id}&fileName=${gName}&documentType=1" class="grytxt">
                					${gName}
                				</a>
                			</td>                			
               			  </tr>                	                	
                	</c:forEach>
                	<tr>
                	  <td colspan="2" align="left" valign="top" style="padding-top: 5px;">
                		<table width="266" border="0" cellspacing="0" cellpadding="0">
		                 <tbody id="addGuideline">
		                  <tr>
		                    <td width="218">                    	                    
		                    	<form:input path="guidelineDocs[0].file" type="file" id="guideline0" 
		                    	accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
		                    	onchange="guidelineValidation(this.value, this.id);"/>
		                    </td>                                        
		                  </tr>
		                 </tbody>                                                    
                		</table>
                	  </td>                	  
                	</tr>
                </table>
                </td>
                <td>
                	<span id="validation_guideline" style="font-size:16px;"></span>
                </td>                
              </tr>
              <tr>
					<td height="39" align="center" valign="top">&nbsp;</td>
                    <td align="left" valign="top" style="padding-top: 3px;"><a href="javascript:void(0)" id="addGuidelineBtn" class="grytxt">+Add More</a></td>
                  </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="sendCsat">Send CSAT</form:label></td>
                <td colspan="5" align="left" valign="middle"><table width="143" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="22" align="left" valign="middle"><form:radiobutton path="sendCsat" value="1" onclick="activateCsat(this.value)"/></td>
                    <td width="30" align="left" valign="middle">Yes</td>
                    <td width="21" align="left" valign="middle"><form:radiobutton path="sendCsat" value="0" onclick="activateCsat(this.value)"/></td>
                    <td width="70" align="left" valign="middle">No</td>
                  </tr>
                  
                </table></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="csatOptions">CSAT Options</form:label></td>
                <td colspan="5" align="left" valign="middle">
                <form:select path="csatOptions" class="inptformSelect" style="width:243px;">
                	<form:option value="0" label="--Select--"/>
                	<form:option value="1" label="Per Project"/>
                	<form:option value="2" label="Weekly"/>
                	<form:option value="3" label="Monthly"/>
                	<form:option value="4" label="Quaterly"/>
                	<form:option value="5" label="Annually"/>
                </form:select>
               	</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="status">Client Status</form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="status" class="inptformSelect" style="width:243px;">
                		<form:option value="-1" label="--Select--"/>
                		<form:option value="0" label="Inactive"/>
                		<form:option value="1" label="Active"/>
                	</form:select>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="addedOn">Client Added On</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="addedOn" class="inptform" disabled="true"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Billing Detail</strong></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_name">Name<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_name" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_job_title">Job Title<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_job_title" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_email">Email<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_email" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_phone_no">Phone No.<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_phone_no" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_ext_no">Ext No.</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_ext_no" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_fax_no">Fax No.</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_fax_no" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:8px;"><form:label path="bill_address">Address <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:textarea path="bill_address" class="inptformmesg"/></td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_city">City<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_city" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_state">State<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_state" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_postal_code">Postal Code<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_postal_code" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_country">Country<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="bill_country" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${countryList}" itemValue="id" itemLabel="name"/>
                	</form:select>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="bill_acc_payable_email">Account Payable Email<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="bill_acc_payable_email" class="inptform"/></td>
              </tr>                                        
              <tr>
                <td height="88" colspan="6" align="left" valign="middle"><table width="367" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="219" align="right" valign="middle">
                    	<input type="submit" name="Submit2" id="Submit2" value="Update" class="btnsubmit" onClick="checkRowUpdation(); return clientValidation(1);">
                    </td>
                    <td width="118" align="center" valign="middle"><a href="viewClientList" class="btnsubmitLink" >Cancel</a></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table>
      </p>
      </div>
      <div id="tab-2" style="background-color:#D2DCBF;">
       	  <p>
       	  <table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation_tandc" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="886" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="tAndC.termsOfAgreement">Terms Of Agreement </form:label></td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.termsOfAgreement" class="inptformmesgt"/>
                </td>                
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="tAndC.definitions">Definitions </form:label></td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.definitions" class="inptformmesgt"/>
                </td>                
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="tAndC.serviceLevelAgreement">Service Level Agreement </form:label></td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.serviceLevelAgreement" class="inptformmesgt"/>
                </td>                
              </tr>
              <tr>
                <td width="201" height="31" align="left" valign="middle">
                <form:label path="tAndC.projectSetupAndTesting">Project Setup and Testing</form:label></td>
                <td align="left" valign="middle">
                	<form:textarea path="tAndC.projectSetupAndTesting" class="inptformmesgt"/>
                </td>
              </tr>          
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="tAndC.qualityMeasures">Quality Measures </form:label></td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.qualityMeasures" class="inptformmesgt"/>
	            </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="tAndC.proposalRevision">Proposal Revision </form:label>                	
                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.proposalRevision" class="inptformmesgt"/>
	            </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="tAndC.projectClosure">Project Closure </form:label>                	
                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.projectClosure" class="inptformmesgt"/>
	            </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="tAndC.costAndBilling">Cost And Billing </form:label>                	
                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.costAndBilling" class="inptformmesgt"/>
	            </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="tAndC.paymentTerms">Payment Terms </form:label>                	
                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.paymentTerms" class="inptformmesgt"/>
	            </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="tAndC.governingLawAndJurisdiction">Governing Law </form:label>                	
                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC.governingLawAndJurisdiction" class="inptformmesgt"/>
	            </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td align="left" valign="middle">&nbsp;</td>
              </tr>
            </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle"><input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" onClick="return tAndCValidation(0);">
              </td>
              <td width="118" align="center" valign="middle"><a href="#tab-1" class="btnsubmitLink" >Cancel</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table>
		  </p>
        </div>
      </form:form>
      </div>
    </div>
  </div>
  </div>
<!-- javascript starts -->
<script type="text/javascript" src="js/sales/clientValidation.js"></script>
<script type="text/javascript">
var pass=true;
var rowNumber='${edit_client.addContactsSize}';
var i;
var sign_i=1;
var guide_i=1;
var btn_close='<td width="60" align="left" valign="middle" id="btn-close" class="btn-remove">' + 
	'<img src="images/btncross.png" width="18" height="17" border="0">' + 
	'</td>';
var btn_close2 = '<td width="48" align="center" valign="middle" id="btn-close" class="btn-remove">' +
	'<img src="images/btncross.png" width="18" height="17" border="0">' +
	'</td>';
$(document).ready(function(){
	//Code for tabbed pages
	 $('#horizontalTab').responsiveTabs({
           startCollapsed: 'accordion',
           collapsible: true,
           rotate: false,
           setHash: true
       });
	
	//Add more functionality for additional contacts
		$("#addMore").click(function() {
			 var tr_temp=$("#addContacts tr:first").clone();
			 tr_temp.find("#add_name0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_name' + i},
	    				'name': function(_, name) { return 'additionalContacts['+ i +'].name'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_job0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_job' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].jobTitle'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_email0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_email' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].email'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_phone0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_phone' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].phoneNo'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#add_ext0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_ext' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].extno'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close);			
			 tr_temp.appendTo("#addContacts");
			 i++;			 			
		});
		
	//add more functionality for signedIO files
		$("#addSignedBtn").click(function() {						
			 var tr_sign_temp=$("#addSigned tr:first").clone();			
			 tr_sign_temp.find("#signedIO0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'signedIO' + sign_i},
	    				'name': function(_, name) { return 'signedIOs['+ sign_i +'].file'},
	    				'value': ''
	  			});
				}).end();
			 
			 tr_sign_temp.append(btn_close2);
			 tr_sign_temp.appendTo("#addSigned");
			 sign_i++;			 			
		});
		
	//add more functionality for guideline documents
		$("#addGuidelineBtn").click(function() {						
			 var tr_sign_temp=$("#addGuideline tr:first").clone();			 
			 tr_sign_temp.find("#guideline0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'guideline' + guide_i},
	    				'name': function(_, name) { return 'guidelineDocs['+ guide_i +'].file'},
	    				'value': ''
	  			});
				}).end();
			 
			 tr_sign_temp.append(btn_close2);
			 tr_sign_temp.appendTo("#addGuideline");
			 guide_i++;			 			
		});
		
		$("#btn-close").live('click',function(){
			$(this).parent().remove();			
		});
		
});

var values=new Array();
function getRowValues()
{
	//initialize i
	if(rowNumber==0){
		i=1;
	}
	else{
		i=rowNumber;
	}
	
	var csatOption='${edit_client.sendCsat}';
	if(csatOption=="1"){
		document.getElementById("csatOptions").disabled=false;
	}
	else{
		document.getElementById("csatOptions").disabled=true;
	}  
	for(var j=0;j<rowNumber;j++)
      {
            values[j]=new Array();
            values[j][0]=document.getElementById('add_name'+j).value;
            values[j][1]=document.getElementById('add_job'+j).value;			
			values[j][2]=document.getElementById('add_email'+j).value;
			values[j][3]=document.getElementById('add_phone'+j).value;
			values[j][4]=document.getElementById('add_ext'+j).value;			
      }
	preName = document.getElementById('client_name').value;
	preCode = document.getElementById('client_code').value;
	preUrl = document.getElementById('websiteUrl').value;
}

var contactIdList;
function checkRowUpdation()
{
    contactIdList="";  
	for(var i=0;i<rowNumber;i++)
      {
            if(values[i][0]!=document.getElementById('add_name'+i).value || values[i][1]!=document.getElementById('add_job'+i).value || 
				values[i][2]!=document.getElementById('add_email'+i).value || values[i][3]!=document.getElementById('add_phone'+i).value || 
				values[i][4]!=document.getElementById('add_ext'+i).value)
            {  
            	contactIdList+=i+",";
            }         
      }
	  document.getElementById("contactIdList").value = contactIdList;
}
function checkName(clientName){
	if(preName != clientName){
		validateClientName(clientName);
	}
}

function checkCode(clientCode) {
	if(preCode != clientCode){
		validateClientCode(clientCode);	
	}
}

function checkUrl(clientUrl) {
	if(preUrl != clientUrl){
		validateWebUrl(clientUrl);
	}
}

</script>
<script src="resources/tabs/js/jquery.responsiveTabs.js" type="text/javascript"></script>
<!-- javascript ends -->
</body>
</html>    