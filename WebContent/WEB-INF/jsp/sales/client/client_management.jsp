<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Sales";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Client List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />

<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchboxcontainer">
	      <div class="btnaddclient">
	      	<h3><a href="addClientForm"></a></h3>
	      </div>
	      <div class="searchbar"><h2 style=" width:120px;">Search Client</h2>
	      	<form:form method="POST" action="viewClientList" modelAttribute="search_client">
		      <form:label path="client_name">Client Name</form:label>
		      <form:input path="client_name" style=" width:120px;"/>
		      <form:label path="client_code">Client Code</form:label>
		      <form:input path="client_code" style=" width:120px;"/>
		      <form:label path="status">Status</form:label>
		      <form:select path="status" style=" width:120px;" class="selectdeco">
		        <form:option value="NONE" label="--Select--"></form:option>
		      	<form:option value="2" label="All"/>
		      	<form:option value="1" label="Active"/>
		      	<form:option value="0" label="Inactive"/>
		      </form:select>
		      <input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>
		  	</form:form>
	      </div>
      </div>
      
      <div class="searchresult">
      <h2> Client List</h2>      
      		<table style="margin-top:1px;" width="1087" border="0" cellspacing="0" cellpadding="0">
		        <tr>
		          <td height="37" align="left" valign="middle" bgcolor="#778d4c"><table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
		            <tr>
		              <td width="254" align="left" valign="middle" style="padding-left:10px;">Client Name</td>
		              <td width="140" align="left" valign="middle">Client Code</td>
		              <td width="134" align="left" valign="middle">Region</td>
		              <td width="165" align="left" valign="middle">Country</td>
		              <td width="155" align="left" valign="middle">Primary Contact</td>
		              <td width="194" align="left" valign="middle">Account Manager</td>
		              <td width="45" align="left" valign="middle">Status</td>
		            </tr>
		          </table></td>
		        </tr>
		        <c:if test="${not empty clientList}">
      			  <c:set var="pageUrl" value="viewClientList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>
      			<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">		      
		        <%--start --%>
		        <pg:param name="client_name" value="${param.client_name}"/>
		        <pg:param name="client_code" value="${param.client_code}"/>
		        <pg:param name="status" value="${param.status}"/>
		       	<c:forEach items="${clientList}" var="clientVar" varStatus="clientStatus">
		       	<%--use ${clientStatus.count} to alternate the row css
		       	use divide by 2--%>
					<pg:item>       	
				       	<c:if test="${clientStatus.count%2!=0}">
					        <tr>
					          <td height="37" align="left" valign="middle" >
					         	<a href="editClientForm?client_id=${clientVar.client_id}">
					         	<div class="blacktxt">
						            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
						             <tr>
						               <td width="252" align="left" valign="middle"><c:out value="${clientVar.client_name}"/></td>
						               <td width="137" align="left" valign="middle"><c:out value="${clientVar.client_code}"/></td>
						               <td width="134" align="left" valign="middle"><c:out value="${clientVar.region}"/></td>
						               <td width="163" align="left" valign="middle"><c:out value="${clientVar.country_name}"/></td>
						               <td width="153" align="left" valign="middle"><c:out value="${clientVar.primaryContact.name}"/></td>
						               <td width="192" align="left" valign="middle"><c:out value="${clientVar.acct_manager_name}"/></td>
						               <td width="56" align="left" valign="middle"><c:out value="${clientVar.status}"/></td>
						             </tr>
						            </table>
					         	</div>
					         	</a>
					         </td>
					        </tr>
				        </c:if>
				        <c:if test="${clientStatus.count%2==0}">
					        <tr>
					          <td height="37" align="left" valign="middle" >
					         <a href="editClientForm?client_id=${clientVar.client_id}"> <div class="blacktxt2">
					           <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
					             <tr>
					               <td width="252" align="left" valign="middle"><c:out value="${clientVar.client_name}"/></td>
					               <td width="137" align="left" valign="middle"><c:out value="${clientVar.client_code}"/></td>
					               <td width="134" align="left" valign="middle"><c:out value="${clientVar.region}"/></td>
					               <td width="163" align="left" valign="middle"><c:out value="${clientVar.country_name}"/></td>
					               <td width="153" align="left" valign="middle"><c:out value="${clientVar.primaryContact.name}"/></td>
					               <td width="192" align="left" valign="middle"><c:out value="${clientVar.acct_manager_name}"/></td>
					               <td width="56" align="left" valign="middle"><c:out value="${clientVar.status}"/></td>
					             </tr>
					           </table>
					         </div></a>          </td>
					        </tr>
				        </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		         </c:if>        
		        <tr>
		          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
		        </tr>
		        <tr>
		          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
		        </tr>
		        <tr>
		          <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
		        </tr>
		      </table>	     
      </div>
    </div>
  </div>
  </div>
</body>
</html>
