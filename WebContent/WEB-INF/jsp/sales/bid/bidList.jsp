<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Sales";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Bid List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<style type="text/css">
.searchbar label {
padding-left: 10px;
}
.btnsearch{
margin-left: 10px;}
</style>
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaaddbid">
      <h3><a href="addBidForm"></a></h3>
      </div>
      <div class="searchbar">
        <h2 style="width: 85px;">Search Bid</h2>
        <form:form method="POST" action="bidList" modelAttribute="searchBid">
        	<form:label path="bidName">Bid Name</form:label>
        	<form:input path="bidName" style=" width:110px;"/>    
        	<form:label path="bidCode">Bid ID</form:label>
        	<form:input path="bidCode" style=" width:110px;"/>
        	<form:label path="biddingStatus">Bid Status</form:label>
        	<form:select path="biddingStatus" style=" width:110px;" class="selectdeco">
		        <form:option value="0" label="--Select--"/>
		        <form:option value="5" label="All"/>
                <form:option value="1" label="Bidding"/>
                <form:option value="2" label="Lost"/>
                <form:option value="3" label="Won"/>
                <form:option value="4" label="Pass"/>                
		    </form:select>
		    <form:label path="accountManager">A/C Manager</form:label>
        	<form:select path="accountManager" style=" width:110px;" class="selectdeco">
		        <form:option value="0" label="--Select--"/>
		        <form:options items="${managerList}" itemLabel="name" itemValue="id"/>                
		    </form:select>
       		<input type="submit" name="button" id="button" value="Search" class="btnsearch"/>
       </form:form>
      </div>
      </div>
      <div class="searchresult2">
      <h2> Bids List</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="113" align="left" valign="middle">Bid ID</td>
                    		<td width="284" align="left" valign="middle">Bid Name</td>
                    		<td width="280" align="left" valign="middle">Client Name</td>
		                    <td width="110" align="left" valign="middle">Bid Status</td>
		                    <td width="190" align="left" valign="middle">Account Manager</td>
		                    <td width="110" align="left" valign="middle">Updated On</td>
                  		  </tr>
              			</table>
              		  </td>
            		</tr>
            	<c:if test="${not empty bidList}">
      			  <c:set var="pageUrl" value="bidList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">            		
            	<%--start --%>
            	<pg:param name="bidName" value="${param.bidName}"/>
            	<pg:param name="bidCode" value="${param.bidCode}"/>
            	<pg:param name="biddingStatus" value="${param.biddingStatus}"/>
            	<pg:param name="accountManager" value="${param.accountManager}"/>
		       	<c:forEach items="${bidList}" var="bidVar" varStatus="bStatus">
		       	
					<pg:item>       	
				      <c:if test="${bStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editBidForm?bid_id=${bidVar.bidId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${bidVar.bidCode}</td>
                      			  <td width="284" align="left" valign="middle">${bidVar.bidName}</td>
			                      <td width="280" align="left" valign="middle">${bidVar.bidClient}</td>
			                      <td width="110" align="left" valign="middle"><strong>${bidVar.biddingStatus}</strong></td>
			                      <td width="190" align="left" valign="middle">${bidVar.accountManager}</td>
			                      <td width="110" align="left" valign="middle">${bidVar.updateDate}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${bStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editBidForm?bid_id=${bidVar.bidId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="113" align="left" valign="middle">${bidVar.bidCode}</td>
                      			  <td width="284" align="left" valign="middle">${bidVar.bidName}</td>
			                      <td width="280" align="left" valign="middle">${bidVar.bidClient}</td>
			                      <td width="110" align="left" valign="middle"><strong>${bidVar.biddingStatus}</strong></td>
			                      <td width="190" align="left" valign="middle">${bidVar.accountManager}</td>
			                      <td width="110" align="left" valign="middle">${bidVar.updateDate}</td>
                    			</tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    