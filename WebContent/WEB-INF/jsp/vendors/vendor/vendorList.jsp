<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tlds/pager-taglib.tld" prefix="pg"%>
<%@ taglib uri="/WEB-INF/tlds/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tlds/struts-logic.tld" prefix="logic"%>
<%!
    Integer pagerPageNumber=new Integer(0);
%>
<%
	String topMenu = "Vendors";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Vendor List - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Parallax Content Slider with CSS3 and jQuery" />
<meta name="pasmt" content="Codrops" />
<meta name="robots" content="noindex, nofollow" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>

<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
    <div class="searchboxcontainer">
      <div class="btnaddvendor">
      <h3><a href="addVendorForm"></a></h3>
      </div>
      <div class="searchbar">
      <h2 style="font-size:15px;">Search Proposal</h2>
      	<form:form method="POST" modelAttribute="searchVendor" action="vendorList">
      		<form:label path="vendorName">Vendor Name</form:label>
      		<form:input path="vendorName" style=" width:120px;"/>
			<form:label path="vendorCode">Vendor Code</form:label>
			<form:input path="vendorCode" style=" width:120px;"/>
			<form:label path="vendorStatus">Status</form:label>
			<form:select path="vendorStatus" style=" width:120px;" class="selectdeco">
		        <form:option value="NONE" label="--Select--"></form:option>
		      	<form:option value="0" label="All"/>
		      	<form:option value="1" label="Active"/>
		      	<form:option value="2" label="Hold"/>
		      	<form:option value="3" label="Inactive"/>
		      </form:select>
       		<input type="submit" name="button" id="button" value="Submit" class="btnsearch"/>
      	</form:form>
      
      </div>
      </div>
      <div class="searchresult2">
      <h2>Vendor Listing</h2>      
      	<table style="margin-top:1px;" width="1135" border="0" cellspacing="0" cellpadding="0">
       		  <tr>
          		<td width="1158" height="37" colspan="2" align="left" valign="top" bgcolor="#FFFFFF" >
          		  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0" style="margin-top:1px;">
            		<tr>
              		  <td height="37" align="left" valign="middle" bgcolor="#778d4c">
              		  	<table  style="margin-left:10px;"width="1087" border="0" align="left" cellpadding="0" cellspacing="0" class="whitetxt">
                  		  <tr>
                    		<td width="280" align="left" valign="middle">Vendor Name</td>
                    		<td width="268" align="left" valign="middle">Vendor Code</td>
                    		<td width="351" align="left" valign="middle">Country</td>
		                    <td width="188" align="left" valign="middle">Contact Person</td>
	                      </tr>
              			</table>
              		  </td>
            		</tr>
            	<c:if test="${not empty vendorList}">
      			  <c:set var="pageUrl" value="vendorList"/>
      			  <c:set var="totalRows" value="${requestScope.total_rows}"/>	
      	  		<pg:pager url="${pageUrl}" maxIndexPages="5" maxPageItems="15" index="half-full" items="${totalRows}" isOffset="true">            		
            	<%--start --%>
		       	<c:forEach items="${vendorList}" var="venVar" varStatus="vStatus">
		       	
					<pg:item>       	
				      <c:if test="${vStatus.count%2!=0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editVendorForm?vendorId=${venVar.vendorId}">
                			<div class="blacktxt">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="280" align="left" valign="middle">${venVar.vendorName}</td>
                      			  <td width="268" align="left" valign="middle">${venVar.vendorCode}</td>
			                      <td width="351" align="left" valign="middle">${venVar.country}</td>
			                      <td width="188" align="left" valign="middle">${venVar.primaryContact.name}</td>
			                    </tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>    
				      </c:if>
				      <c:if test="${vStatus.count%2==0}">
					  	<tr>
              			  <td height="37" align="left" valign="middle" >
              			  	<a href="editVendorForm?vendorId=${venVar.vendorId}">
                			<div class="blacktxt2">
                  			  <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
                    			<tr>
                      			  <td width="280" align="left" valign="middle">${venVar.vendorName}</td>
                      			  <td width="268" align="left" valign="middle">${venVar.vendorCode}</td>
			                      <td width="351" align="left" valign="middle">${venVar.country}</td>
			                      <td width="188" align="left" valign="middle">${venVar.primaryContact.name}</td>
			                    </tr>
                  			  </table>
                			</div>
              				</a>
              			  </td>
            			</tr>
				     </c:if>
					</pg:item>
		        </c:forEach>
		        <%@include file="../../include/paging_bar.jsp"%>
		        </pg:pager>
		        <%--end --%>
		        </c:if>       
            <tr>
              <td align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td height="2" colspan="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
      </table>      	  
      </div>
    </div>
  </div>
  </div>
</body>
</html>
    