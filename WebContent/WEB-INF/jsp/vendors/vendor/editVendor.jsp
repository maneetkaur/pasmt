<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "Vendors";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Edit Vendor - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- tabbed page -->
<link type="text/css" rel="stylesheet" href="resources/tabs/css/responsive-tabs.css" />
<link type="text/css" rel="stylesheet" href="resources/tabs/css/style.css" />
<!-- tabbed page include ends -->
<!-- checkbox tree include starts -->
  <script src="resources/checkBoxTree/js/jquery.js" type="text/javascript"></script>
  <script src="resources/checkBoxTree/js/ui.core.js" type="text/javascript"></script>
  <script src="resources/checkBoxTree/js/jquery.cookie.js" type="text/javascript"></script>
  <link href="resources/checkBoxTree/css/ui.dynatree.css" rel="stylesheet" type="text/css">
  <script src="resources/checkBoxTree/js/jquery.dynatree.min.js" type="text/javascript"></script>
<!-- checkbox tree include ends -->
<script type="text/javascript">
var vendorConflict = jQuery.noConflict();
    jQuery(function(){
      vendorConflict("#categoryTree").dynatree({
        //Tree parameters
        persist: false,
        checkbox: true,
        selectMode: 3,
        activeVisible: true,
  
        //Un/check real checkboxes recursively after selection
        onSelect: function(select, dtnode) {
		
          dtnode.visit(function(dtnode){
            vendorConflict("#chb-"+dtnode.data.key).attr("checked",select);
            },null,true);
        },
        //Hack to prevent appearing of checkbox when node is expanded/collapsed
        onExpand: function(select, dtnode) {
          vendorConflict("#chb-"+dtnode.data.key).attr("checked",dtnode.isSelected()).addClass("hidden");
        }
      });
      //Hide real checkboxes
      vendorConflict("#categoryTree :checkbox").addClass("hidden");
      //Update real checkboxes according to selections
      vendorConflict.map(vendorConflict("#categoryTree").dynatree("getTree").getSelectedNodes(),
        function(dtnode){
          vendorConflict("#chb-"+dtnode.data.key).attr("checked",true);
          dtnode.activate();
        });
      
      vendorConflict("#categoryTreeSurvey").dynatree({
          //Tree parameters
          persist: false,
          checkbox: true,
          selectMode: 3,
          activeVisible: true,
    
          //Un/check real checkboxes recursively after selection
          onSelect: function(select, dtnode) {
  		
            dtnode.visit(function(dtnode){
              vendorConflict("#chb-"+dtnode.data.key).attr("checked",select);
              },null,true);
          },
          //Hack to prevent appearing of checkbox when node is expanded/collapsed
          onExpand: function(select, dtnode) {
            vendorConflict("#chb-"+dtnode.data.key).attr("checked",dtnode.isSelected()).addClass("hidden");
          }
        });
        //Hide real checkboxes
        vendorConflict("#categoryTreeSurvey :checkbox").addClass("hidden");
        //Update real checkboxes according to selections
        vendorConflict.map(vendorConflict("#categoryTreeSurvey").dynatree("getTree").getSelectedNodes(),
          function(dtnode){
            vendorConflict("#chb-"+dtnode.data.key).attr("checked",true);
            dtnode.activate();
          });
      });
    
  var selectedCategories="", selectedsubCategories="",temp;
  function getSelectedCategories(divNameCat){
	  selectedCategories="";
	  selectedsubCategories="";
	  vendorConflict.map(vendorConflict("#" + divNameCat).dynatree("getTree").getSelectedNodes(),
		function(dtnode){		  
			temp = dtnode.data.key;		  		  
		  	if(temp.substring(0,4)=="cat-"){		  		 
		  		 selectedCategories = selectedCategories + temp.substring(4) + ",";
		  	}
		  	else{
		  		selectedsubCategories = selectedsubCategories + temp + ",";
		  	}
		});
	  if(divNameCat == "categoryTree"){
		  document.getElementById("surveySubCategoryOsbt").value = selectedsubCategories;
		  document.getElementById("completeCategoryOsbt").value = selectedCategories;
	  }
	  else{
		  document.getElementById("surveySubCategorySurvey").value = selectedsubCategories;
		  document.getElementById("completeCategorySurvey").value = selectedCategories;
	  }
  }
  </script>
  
  <link rel="stylesheet" href="css/style.css" type="text/css" />
  <style type="text/css">
  	span.trackingSpans{
  		color: #000000; font-family: Calibri; font-size: 16px;
  	}
  	div.categoryTreeDiv{
  		max-height: 105px; overflow:auto; width: 242px; overflow-x: hidden;
  	}
  </style>
</head>
<body onLoad="javascript:getRowValues(); initialize();">
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Edit Vendor</h2>
           <div id="horizontalTab">
        <ul>
        	<c:set var="vTypeVal" value="24"/>
            <li><a href="#tab-1">Edit Vendor Details</a></li>
            <li><a href="#tab-2" id="tab-bill">Billing Information</a></li>                      
            <li <c:if test="${empty vendorTypeMap['24']}">style="display:none;"</c:if>><a href="#tab-3">Tracking- Recruitment</a></li>
            <li <c:if test="${empty vendorTypeMap['25']}">style="display:none;"</c:if>><a href="#tab-4">Tracking- OSBT</a></li>
            <li <c:if test="${empty vendorTypeMap['23']}">style="display:none;"</c:if>><a href="#tab-5">Tracking- Surveys</a></li>             
        </ul>
		<form:form method="POST" action="editVendor" modelAttribute="editVendor" enctype="multipart/form-data">
        <div id="tab-1" style="background-color:#D2DCBF;">
            <p style="margin-left:0px;"><table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="874" colspan="5" align="left" valign="middle">&nbsp;<form:hidden path="vendorId"/></td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="vendorName">Vendor Name <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="vendorName" class="inptform" onchange="checkName(this.value);"/>
                   <br/> 
                  <span id="msgVendorName" style="padding-left: 20px;"></span>                </td>                
              </tr>
              <tr>
                <td width="213" height="31" align="left" valign="middle">
                <form:label path="vendorCode">Vendor Code <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:input path="vendorCode" class="inptform" onchange="checkCode(this.value);"/>
                	<br/>
                  	<span id="msgVendorCode" style="padding-left: 20px;"></span>                </td>
              </tr>                        
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="vendorType">Vendor Type<span>*</span></form:label>
                </td>
                <td colspan="5" align="left" valign="middle">
                  <div id="vendorTypeDiv" class="vtypediv">
                  	 <c:forEach items="${vendorTypeList}" var="vType">
                  	 	<form:checkbox path="vendorType" value="${vType.id}" label="${vType.name}"
                  	 	 style="margin-right:3px;"/><br/>
                  	 </c:forEach>
                  </div>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="accountManager">IRB Account Manager<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="accountManager" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${managerList}" itemValue="id" itemLabel="name"/>
                	</form:select>                </td>
              </tr>             
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Primary Contact</strong></td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="53" height="57" align="left" valign="middle">
                    <form:label path="primaryContact.name">Name<span>*</span></form:label></td>
                    <td width="150" align="left" valign="middle">
                    	<form:input path="primaryContact.name" class="inptform" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle"><form:label path="primaryContact.jobTitle">Job Title<span>*</span></form:label></td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="primaryContact.jobTitle" class="inptform" style="width:130px;"/>                    </td>
                    <td width="73" align="center" valign="middle"><form:label path="primaryContact.email">Email<span>*</span></form:label></td>
                    <td width="142" align="left" valign="middle">
                    	<form:input path="primaryContact.email" class="inptform" style="width:130px;"/>                   	</td>
                    <td width="96" align="center" valign="middle"><form:label path="primaryContact.phoneNo">Phone No.<span>*</span></form:label></td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="primaryContact.phoneNo" class="inptform" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle"><form:label path="primaryContact.extno">Ext No.</form:label></td>
                    <td width="101" align="left" valign="middle">
                    	<form:input path="primaryContact.extno" class="inptform" style="width:65px;"/>                    </td>
                    <td width="60" align="left" valign="middle"></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td height="19" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Additional Contacts</strong></td>
                </tr>
                <c:choose>
                <c:when test="${editVendor.additionalContacts.size()>0}">
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                 <tbody id="addContacts">
                 <c:forEach items="${editVendor.additionalContacts}" var="ac" varStatus="status">                              
                  <tr>
                    <td width="0"><form:hidden path="additionalContacts[${status.index}].id" id="add_id${status.index}"/></td>
                    <td width="53" height="57" align="left" valign="middle">Name  </td>
                    <td width="153" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].name" id="add_name${status.index}" class="inptform" style="width:130px;"/>                    	
                    </td>
                    <td width="89" align="center" valign="middle">Job Title  </td>
                    <td width="121" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].jobTitle" class="inptform" id="add_job${status.index}" style="width:130px;"/>
                    </td>
                    <td width="78" align="center" valign="middle">Email  </td>
                    <td width="130" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].email" class="inptform" id="add_email${status.index}" style="width:130px;"/>
                    </td>
                    <td width="98" align="center" valign="middle">Phone No.  </td>
                    <td width="120" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].phoneNo" class="inptform" id="add_phone${status.index}" style="width:130px;"/>
                    </td>
                    <td width="92" align="center" valign="middle">Ext No. </td>
                    <td width="90" align="left" valign="middle">
                    	<form:input path="additionalContacts[${status.index}].extno" class="inptform" id="add_ext${status.index}" style="width:65px;"/>                    	
                    </td>
                    <td width="60" align="left" valign="middle" style="display: block;"></td>                    
                  </tr>
                  </c:forEach>
                </tbody>  
                </table></td>
                </tr>
                </c:when>
                <c:otherwise>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                 <tbody id="addContacts">
                  <tr>
                    <td width="53" height="57" align="left" valign="middle">Name  </td>
                    <td width="150" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].name" id="add_name0" class="inptform" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle">Job Title  </td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].jobTitle" class="inptform" id="add_job0" style="width:130px;"/>                    </td>
                    <td width="73" align="center" valign="middle">Email  </td>
                    <td width="142" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].email" class="inptform" id="add_email0" style="width:130px;"/>                    </td>
                    <td width="96" align="center" valign="middle">Phone No.  </td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].phoneNo" class="inptform" id="add_phone0" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle">Ext No. </td>
                    <td width="101" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].extno" class="inptform" id="add_ext0" style="width:65px;"/>                    </td>
                    <td width="60" align="left" valign="middle" style="display: block;"></td>                      
                  </tr>
                </tbody>  
                </table></td>
                </tr>
                </c:otherwise>
               </c:choose>      
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit"></td>
                <td>
                	<form:hidden path="addContactsSize"/>
                	<form:hidden path="contactIdList"/>
                </td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:8px;"><form:label path="address">Address<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:textarea path="address" class="inptformmesg"/>                </td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="city">City<span>*</span></form:label></td>
                <td align="left" valign="middle"><form:input path="city" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="state">State<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="state" class="inptform"/></td>
              </tr>
              <tr>
               <td height="31" align="left" valign="middle"><form:label path="postalCode">Postal Code<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="postalCode" class="inptform"/></td>
              </tr>
              <tr>
              <td height="31" align="left" valign="middle"><form:label path="country">Country<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="country" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${countryList}" itemValue="id" itemLabel="name"/>
                	</form:select>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="websiteUrl">Website URL<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="websiteUrl" class="inptform" onchange="checkUrl(this.value);"/>
                	<br/> 
                  	<span id="msgWebUrl" style="padding-left: 20px;"></span>                </td>
              </tr>            
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="phoneNo">Phone No.<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="phoneNo" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="faxNo">Fax No.</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="faxNo" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="rfqEmail">RFQ Email</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="rfqEmail" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="vendorForm">Vendor Form<span>*</span></form:label></td>
                <td colspan="3" align="left" valign="top">
                <div id="vendorFormDiv">
                  <c:choose>
                	<c:when test="${not empty editVendor.vendorFormName}">
                	  <table width="255" border="0" cellpadding="0" cellspacing="0">                         	                
                		<tr>
                			<td width="210" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadVendorFile?vendorId=${editVendor.vendorId}&fileName=${editVendor.vendorFormName}&documentType=36" class="grytxt">
                					${editVendor.vendorFormName}
                				</a>
                			</td>                			
                			<td width="45" align="center" valign="middle" id="btn-close2" class="btn-remove">
                				<a href="javascript:void(0)" onClick="javascript:replace('vendorForm');"><img src="images/btncross.png" width="18" height="17" border="0"></a>
                			</td>
               			</tr>
               		  </table>
                	</c:when>
                	<c:otherwise>
                		<form:input path="vendorForm" type="file"
                    	 	accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    	 	onchange="documentValidation(this.value, this.id);"/>
                    </c:otherwise>
                  </c:choose> 
                  </div>               	                                 
                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="guidelineDocs">Guideline Documents</form:label></td>
                <td colspan="3" align="left" valign="top">
                <div id="guidelineDocsDiv">
                	<c:choose>
                	<c:when test="${not empty editVendor.guidelineName}">
                	  <table width="255" border="0" cellpadding="0" cellspacing="0">                         	                
                		<tr>
                			<td width="210" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                			<a href="downloadVendorFile?vendorId=${editVendor.vendorId}&fileName=${editVendor.guidelineName}&documentType=37" class="grytxt">
                				${editVendor.guidelineName}
                			</a>
                			</td>                			
                			<td width="45" align="center" valign="middle" id="btn-close2" class="btn-remove">
                				<a href="javascript:void(0)" onClick="javascript:replace('guidelineDocs');">
                					<img src="images/btncross.png" width="18" height="17" border="0">
                				</a>
                			</td>
               			</tr>
               		  </table>
                	</c:when>
                	<c:otherwise>
                		<form:input path="guidelineDocs" type="file"
                    	 	accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 	onchange="documentValidation(this.value, this.id);"/>
                    </c:otherwise>
                  </c:choose>
                  </div>   
                	                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="signedIO">Signed IO</form:label></td>
                <td colspan="3" align="left" valign="top" style="width: 50px;">
                <div id="signedIODiv">
                <c:choose>
                	<c:when test="${not empty editVendor.signedIOName}">
                	  <table width="255" border="0" cellpadding="0" cellspacing="0">                         	                
                		<tr>
                			<td width="210" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadVendorFile?vendorId=${editVendor.vendorId}&fileName=${editVendor.signedIOName}&documentType=41" class="grytxt">
                					${editVendor.signedIOName}
                				</a>
                			</td>                			
                			<td width="45" align="center" valign="middle" id="btn-close2" class="btn-remove">
                				<a href="javascript:void(0)" onClick="javascript:replace('signedIO');">
                					<img src="images/btncross.png" width="18" height="17" border="0">
                				</a>
                			</td>
               			</tr>
               		  </table>
                	</c:when>
                	<c:otherwise>
                		<form:input path="signedIO" type="file"
                    		accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    		onchange="documentValidation(this.value, this.id);"/>
                    </c:otherwise>
                  </c:choose>   
                  </div>              	
                    	<span id="validation_document" style="padding-left: 20px;"></span>               </td> 
                   <td></td>               
              </tr>                                         
              <tr>
                <td height="31" align="left" valign="top"><form:label path="panelBook">Panel Book</form:label></td>
                <td colspan="3" align="left" valign="top">
                <div id="panelBookDiv">
                <c:choose>
                	<c:when test="${not empty editVendor.panelBookName}">
                	  <table width="255" border="0" cellpadding="0" cellspacing="0">                         	                
                		<tr>
                			<td width="210" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadVendorFile?vendorId=${editVendor.vendorId}&fileName=${editVendor.panelBookName}&documentType=38" class="grytxt">${editVendor.panelBookName}</a>
                			</td>                			
                			<td width="45" align="center" valign="middle" id="btn-close2" class="btn-remove">
                				<a href="javascript:void(0)" onClick="javascript:replace('panelBook');">
                					<img src="images/btncross.png" width="18" height="17" border="0">
                				</a>
                			</td>
               			</tr>
               		  </table>
                	</c:when>
                	<c:otherwise>
                		<form:input path="panelBook" type="file"
                    	 	accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 	onchange="documentValidation(this.value, this.id);"/>
                    </c:otherwise>
                  </c:choose> 
                  </div>
                	                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="companyBrochure">Company Brochure</form:label></td>
                <td colspan="3" align="left" valign="top">
                <div id="companyBrochureDiv">
                <c:choose>
                	<c:when test="${not empty editVendor.brochureName}">
                	  <table width="255" border="0" cellpadding="0" cellspacing="0">                         	                
                		<tr>
                			<td width="210" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadVendorFile?vendorId=${editVendor.vendorId}&fileName=${editVendor.brochureName}&documentType=39" class="grytxt">
                					${editVendor.brochureName}
                				</a>
                			</td>                			
                			<td width="45" align="center" valign="middle" id="btn-close2" class="btn-remove">
                				<a href="javascript:void(0)" onClick="javascript:replace('companyBrochure');">
                					<img src="images/btncross.png" width="18" height="17" border="0">
                				</a>
                			</td>
               			</tr>
               		  </table>
                	</c:when>
                	<c:otherwise>
                		<form:input path="companyBrochure" type="file"
                    	 	accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 	onchange="documentValidation(this.value, this.id);"/> 
                    </c:otherwise>
                  </c:choose>
                  </div> 
                	               </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="esomar">Esomar 26/28</form:label></td>
                <td colspan="3" align="left" valign="top">
                <div id="esomarDiv">
                <c:choose>
                	<c:when test="${not empty editVendor.esomarName}">
                	  <table width="255" border="0" cellpadding="0" cellspacing="0">                         	                
                		<tr>
                			<td width="210" align="left" valign="middle" style="font-size:13px; color:#006600; padding-right:3px;">
                				<a href="downloadVendorFile?vendorId=${editVendor.vendorId}&fileName=${editVendor.esomarName}&documentType=40" class="grytxt">
                					${editVendor.esomarName}
                				</a>
                			</td>                			
                			<td width="45" align="center" valign="middle" id="btn-close2" class="btn-remove">
                				<a href="javascript:void(0)" onClick="javascript:replace('esomar');">
                					<img src="images/btncross.png" width="18" height="17" border="0">
                				</a>
                			</td>
               			</tr>
               		  </table>
                	</c:when>
                	<c:otherwise>
                		<form:input path="esomar" type="file"
                    	 	accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 	onchange="documentValidation(this.value, this.id);"/> 
                    </c:otherwise>
                  </c:choose> 
                	               </td>      
              </tr>                                      
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="vendorStatus">Vendor Status<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="vendorStatus" class="inptformSelect" style="width:243px;">        
                		<form:option value="0" label="--Select--"/>
                		<form:option value="1" label="Active"/>
                		<form:option value="2" label="Hold"/>
                		<form:option value="3" label="Inactive"/>
                	</form:select>                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:10px;"> 
                	<form:label path="tAndC">Terms and Conditions </form:label>                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC" class="inptformmesgt"/>	            </td>
              </tr>                                      
              <tr>
                <td height="30" colspan="6" align="left" valign="middle"></td>
                </tr>
            </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit2" id="Submit2" value="Save" class="btnsubmit"
              		 onClick="checkRowUpdation();return vendorValidation(1);">              
              </td>
              <td width="118" align="center" valign="middle"><a href="#tab-2" class="btnsubmitLink" >Continue</a></td>            
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table></p>
        </div>
        </form:form>
        <form:form method="POST" action="editVendorBill" modelAttribute="editVendor">
        <div id="tab-2" style="background-color:#D2DCBF;">
       	  <p>
       	  <table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation_billing" style="font-size:16px;"></span>
              	<form:hidden path="vendorId"/></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1084" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="540" align="left" valign="top"><table width="512" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="31" colspan="6" align="left" valign="middle"><strong>Billing Information</strong></td>
                </tr>
                <tr>
                  <td width="221" height="31" align="left" valign="middle"><form:label path="paymentType">Payment Type<span>*</span></form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:select path="paymentType" class="inptformSelect" style="width:243px;">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="Local"/>
                      <form:option value="2" label="International"/>
                    </form:select>                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="billingCurrency">Billing Currency<span>*</span></form:label></td>
                  <td colspan="5" align="left" valign="middle">
                    <form:select path="billingCurrency" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${billCurrencyList}" itemValue="id" itemLabel="name"/>
                	</form:select> 
                 </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="paymentMode">Payment Mode<span>*</span></form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:select path="paymentMode" class="inptformSelect" style="width:243px;">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="Wire Transfer"/>
                      <form:option value="2" label="Paypal"/>
                      <form:option value="3" label="Cheque"/>
                      <form:option value="4" label="NetBanking"/>
                    </form:select>                  </td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="bankName">Bank Name</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="bankName" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="beneficiaryName">Beneficiary Name</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="beneficiaryName" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="accountNo">Account No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="accountNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="ifscCode">IFSC Code</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="ifscCode" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="ibanNo">IBAN/Routing No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="ibanNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="swiftCode">Swift Code</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="swiftCode" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="sortCode">Sort Code</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="sortCode" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="bankAddress">Bank Physical Address</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="bankAddress" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="intBankName">Intermediary Bank Name</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intBankName" class="inptform"/></td>
                </tr>

                <tr>
                  <td height="31" align="left" valign="middle">&nbsp;</td>
                  <td width="291" align="left" valign="middle">&nbsp;</td>
                </tr>
              </table></td>
              <td width="544" align="left" valign="top"><table width="535" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                  <td height="31" colspan="6" align="left" valign="middle">&nbsp;</td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle" nowrap><form:label path="intBeneficiaryName">Intermediary Beneficiary Name</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intBeneficiaryName" class="inptform"/></td>
                </tr>
                <tr>
                  <td width="245" height="31" align="left" valign="middle"><form:label path="intBankAddress">Intermediary Bank Address</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intBankAddress" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="intAccountNo">Intermediary Bank A/c No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intAccountNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle" nowrap><form:label path="intIbanNo">Intermediary Bank IBAN/Routing No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intIbanNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="intSwiftCode">Intermediary Bank Swift Code</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intSwiftCode" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="intSortCode">Intermediary Bank SORT Code</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="intSortCode" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="paypalEmail">Paypal Email</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="paypalEmail" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="panNo">PAN No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="panNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="serviceTaxNo">Service Tax No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="serviceTaxNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="tan">TAN</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="tan" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="vatNo">VAT No.</form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="vatNo" class="inptform"/></td>
                </tr>
                <tr>
                  <td height="31" align="left" valign="middle"><form:label path="accountRecEmail">Account Receivable Email<span>*</span></form:label></td>
                  <td colspan="5" align="left" valign="middle"><form:input path="accountRecEmail" class="inptform"/></td>
                </tr>
                
              </table></td>
            </tr>
          </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table style="margin-left:230px;" width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" 
              		onClick="return billingValidation();">
              </td>
              <td width="118" align="center" valign="middle"><a href="#tab-1" class="btnsubmitLink" >Cancel</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table>
		  </p>
        </div>
        </form:form>
        <form:form name="recruitForm" method="POST" action="editVendorRecruit" modelAttribute="editVendorRecruit">
        <div id="tab-3" style="background-color:#D2DCBF;">
            <p>
			<table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation_recruitment" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
                  <td height="31" colspan="6" align="left" valign="middle"><strong>Tracking Method</strong></td>
              </tr>
              <tr>
                <td align="left" valign="middle">
                	<form:hidden path="vendorId"/>
                	<form:hidden path="trackingMethodId"/>
                </td>
                <td width="874" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="trackingType">Tracking Type<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="trackingType" class="inptformSelect" style="width:243px;" 
                  	onchange="openDivRecruit(this.value, '');">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="No Tracking"/>
                      <form:option value="3" label="Pixel Tracking"/>
                      <form:option value="4" label="Server to Server"/>
                  </form:select>
                </td>                
              </tr>
              <tr>
                <td width="213" height="31" align="left" valign="middle">
                <form:label path="cpa">CPA<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<input type="text" id="currRecCPA" style="width:40px;" class="inptform" readonly="true"
                	 	value="${selectedCurrency}"/>
                	<form:input path="cpa" class="inptform" style="width:180px; text-align:left;"/>
                </td>
              </tr>                        
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="variables[0].variableName">Variable Name<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="variables[0].variableName" class="inptform"/>
                  <form:hidden path="variables[0].variableId"/>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                <div id="trackingDiv" style="display: none;">
                 <!-- <div id="trackingDiv">-->                  
                  <table>
                  	<tr>
                  	  <td width="209px;" height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="urlCode"><span id="trackingSpan" class="trackingSpans"></span><span>*</span></form:label>
                  	  </td>
                  	  <td align="left" valign="middle">
                  		<form:textarea path="urlCode" class="inptformmesg" style="width:468px;"/>
                	  </td>
                	  <td>
                	  	<div id="codeInstruction" class="vendorinstruction" style="width: 350px; height: 70px;">                  	  	  
				  		</div>
				  	  </td>
                  	</tr>
                  </table>                
                </div>
                </td>
              </tr>
			  <tr>
                <td height="31" align="left" valign="middle"><form:label path="successPage">Success Page for Conversion<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="successPage" class="inptformSelect" style="width:243px;">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="Form Submit"/>
                      <form:option value="2" label="Email Confirmation"/>
                      <form:option value="3" label="Validation Page"/>
                      <form:option value="4" label="First Survey Completion"/>
                  </form:select>                  
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="generateCoReg">
                Generate Co-Reg URL</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="generateCoReg" class="inptformSelect" style="width:243px;">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                  
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top: 10px;">
                	<label for="registrationUrl">Co-Reg URL</label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:textarea path="coRegUrl" disabled="true" class="inptformmesg" style="width:468px;"/>
                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="top" style="padding-top: 10px;">
                	<label for="registrationUrl">Registration URL</label></td>
                <td colspan="5" align="left" valign="middle">                  
                  <textarea id="registrationUrl" disabled="true" class="inptformmesg" style="width:468px;"></textarea>                  
                </td>
              </tr>                        
              <tr>
                <td height="30" colspan="6" align="left" valign="middle"></td>
                </tr>
            </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit3" id="Submit3" value="Save" class="btnsubmit" 
              		onclick="return recruitValidation();">
              </td>
              <td width="118" align="center" valign="middle"></td>            
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table></p>
        </div>
        </form:form>
        <form:form name="osbtForm" method="POST" action="editVendorOsbt" modelAttribute="editVendorOsbt">
        <div id="tab-4" style="background-color:#D2DCBF;">
            <p>
			<table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation_osbt" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
                  <td height="31" colspan="6" align="left" valign="middle"><strong>Tracking Method</strong></td>
              </tr>
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="770" colspan="5" align="left" valign="middle">
                	<form:hidden path="vendorId"/>
                	<form:hidden path="trackingMethodId"/></td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="trackingType">Tracking Type<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="trackingType" class="inptformSelect" style="width:243px;"
                  	onchange="openDivRecruit(this.value, 'Osbt');">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="No Tracking"/>
                      <form:option value="2" label="URL Tracking"/>
                      <form:option value="3" label="Pixel Tracking"/>
                      <form:option value="4" label="Server to Server"/>
                  </form:select>                </td>                
              </tr>
              <tr>
                <td width="317" height="31" align="left" valign="middle">
                <form:label path="cpa">CPA<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<input type="text" id="currOsbtCPA" style="width:40px;" class="inptform" readonly="true"
                	 	value="${selectedCurrency}"/>
                	<form:input path="cpa" class="inptform" style="width:180px; text-align:left;"/>
                </td>
              </tr>
			  <tr>
                <td height="31" align="left" valign="middle"><form:label path="signUpAllowed">
                Is OSBT Respondent Allowed for Signup</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="signUpAllowed" class="inptformSelect" style="width:243px;">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="variables[0].variableName">Variable Name<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="variables[0].variableName" class="inptform"/>
                  <form:hidden path="variables[0].variableId"/>
                </td>
              </tr>
			  
			  <tr>
                <td colspan="2">
                <div id="trackingOsbtDiv" style="display: none;">
                 <!-- <div id="trackingOsbtDiv">-->                  
                  <table>
                  	<tr>
                  	  <td width="313px;" height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="urlCode"><span id="trackingOsbtSpan" class="trackingSpans"></span><span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">
                  		<form:textarea path="urlCode" class="inptformmesg" style="width:468px;"/>
                  	  </td>
                  	  <td>
                	  	<div id="codeInstructionOsbt" class="vendorinstruction" style="width: 266px; height: 70px; padding-top: 7px;">                  	  	  
				  		</div>
				  	  </td>
                  	</tr>
                  </table>                
                </div>                </td>
              </tr>
              <tr>
                <td colspan="2">
                <div id="trackingOsbtUrlDiv" style="display: none;">
                 <!-- <div id="trackingOsbtUrlDiv">-->                  
                  <table>
                  	<tr>
                  	  <td width="313px;" height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="completeUrl">Complete URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="completeUrl" class="inptformvendorurl"/>
                  	  </td>
                  	  <td rowspan="5">
                	  	<div id="urlInstructionOsbt" class="vendorinstruction" style="width: 266px;">
                	  		<b>Note:</b><br>
                	  		Use #id1# to pass the panelist id in the URL. Refer the example below:<br>
							http://survey.irbureau.com/vendorEndPages/vendor.jsp?vid1=#id1#
				  		</div>
				  	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="terminateUrl">Terminate URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="terminateUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="quotaFullUrl">Quota Full URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="quotaFullUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="securityTerminateUrl">Security Terminate URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="securityTerminateUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="errorsUrl">Errors URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="errorsUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  </table>                
                </div>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><label for="osbtLandingUrl">Landing Page URL</label></td>
                <td colspan="5" align="left" valign="middle">
                  <input id="osbtLandingUrl" disabled="disabled" class="inptformvendorurl"/>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><label for="osbtSurveyLandingUrl">Survey Specific Landing Page URL</label></td>
                <td colspan="5" align="left" valign="middle">
                  <input id="osbtSurveyLandingUrl" disabled="disabled" class="inptformvendorurl"/>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><strong>Routing Preferences</strong>
                	<form:hidden path="routingId"/>
                </td>               
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="routingAllowed">Routing Allowed</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="routingAllowed" class="inptformSelect" style="width:243px;" 
                  	onchange="openRoutingFields(this.value, '');">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                </td>
              </tr>
              <tr>
              	<td colspan="2">
              		<div id="maxRoutedRow" style="display: none;">
              		<table>
              		  <tr>
		                <td width="313px;" height="31" align="left" valign="middle">
		                	<form:label path="maxRouted">Max no. of routed surveys per respondent</form:label></td>
		                <td colspan="5" align="left" valign="middle">
		                  <form:input path="maxRouted" class="inptform"/>                </td>
		              </tr>
              		</table>
              		</div>
              	</td>
              </tr>
              <tr>
              	<td colspan="2">
              		<div id="maxQualifiedRow" style="display: none;">
              		<table>
              		  <tr>
		                <td width="313px;" height="31" align="left" valign="middle">
		                	<form:label path="maxQualified">Max no. of qualified surveys per respondent</form:label>
		                </td>
		                <td colspan="5" align="left" valign="middle">
		                  <form:input path="maxQualified" class="inptform"/>                </td>
		              </tr>
              		</table>
              		</div>
              	</td>
              </tr>            
              <tr>
                <td height="31" align="left" valign="middle"><label for="audienceType">Audience Type</label></td>
                <td colspan="5" align="left" valign="middle"><table width="250" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="left" valign="middle"  style="padding-top:2px">
                    	<form:checkbox path="audienceType" value="1"/>
                    </td>
                    <td align="left" valign="top">B2B</td>
                    <td align="left" valign="middle" style="padding-top:2px">
                    	<form:checkbox path="audienceType" value="2"/>
                    </td>
                    <td align="left" valign="top">B2C</td>
                    <td align="left" valign="middle" style="padding-top:2px">
                    	<form:checkbox path="audienceType" value="3"/>
                    </td>
                    <td align="left" valign="top">HC</td>
                  </tr>
                  
                </table></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="surveySubCategory">Survey Category for Routing</form:label>
                	<form:hidden path="surveySubCategory" id="surveySubCategoryOsbt"/>
                	<form:hidden path="completeCategory" id="completeCategoryOsbt"/>
                </td>
                <td colspan="5" align="left" valign="middle">                  
                  <div id="categoryTree" class="categoryTreeDiv">
                  	<ul>
                  		<c:forEach items="${categoryList}" var="category">                  						    
					      <li id="cat-${category.categoryTypeId}"
					      	<c:if test="${not empty editVendorOsbt.completeCategoryMap[category.categoryTypeId]}">class="selected"</c:if>>
					        ${category.categoryType}
					        <ul>
					       	  <c:forEach items="${category.subCategoryList}" var="subCategory">
					       	  <c:set var="subCatId" value="${subCategory.subCategoryTypeId}"></c:set>					       	  
					       	  	<li id="${subCategory.subCategoryTypeId}"					 
					       	  		<c:if test="${not empty editVendorOsbt.subCategoryMap[subCategory.subCategoryTypeId]}">class="selected"</c:if> >
					       	  		${subCategory.subCategoryType}	
					       	  	</li>					       	  
					       	  </c:forEach>
					        </ul>
					      </li>					      					      					      					    					  
                  		</c:forEach>
                  	</ul>
                  </div>
                 </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="minConversion">Minimum Conversion for Routing</form:label></td>
                <td colspan="5" align="left" valign="middle">                  
                  <form:select path="minConversion" class="inptformSelect" style="width:243px;" >
                     <form:option value="101" label="--Select--"></form:option>                     
                     <form:option value="0" label="<1%"></form:option>
                     <c:forEach var="index" begin="1" end="100">
                     	<form:option value="${index}" label="${index}%"></form:option>
                     </c:forEach>
                   </form:select>
                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="maxLoi">Maximum LOI for Routing</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="maxLoi" class="inptform" style="width:180px; text-align:left;"/>
                  <input type="text" style="width:40px;" class="inptform" readonly="true" value="min"/>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><strong>Branding Preferences</strong></td>               
              </tr>    
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="brandingType">Select Branding</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="brandingType" class="inptformSelect" style="width:243px;">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="IRB Branding"/>
                      <form:option value="2" label="Vendor Branding"/> 
                      <form:option value="3" label="No Branding"/>                    
                  </form:select>                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="helpLink">Help Link</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="helpLink" class="inptform"/>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="sendUrl">Send UserID in URL or Pixel or S2S</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="sendUrl" class="inptformSelect" style="width:243px;">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="emailCollection">Email Collection</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="emailCollection" class="inptformSelect" style="width:243px;">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes" selected="true"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                </td>
              </tr>
                           
              <tr>
                <td height="30" colspan="6" align="left" valign="middle"></td>
                </tr>
            </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit4" id="Submit4" value="Save" class="btnsubmit"
              		 onclick="getSelectedCategories('categoryTree'); return osbtValidation();">
              </td>
              <td width="118" align="center" valign="middle"></td>            
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table></p>
        </div>
        </form:form>
        <form:form name="surveyForm" method="POST" action="editVendorSurvey" modelAttribute="editVendorSurvey">
        <div id="tab-5" style="background-color:#D2DCBF;">
            <p>
			<table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation_surveys" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
			  <tr>
                  <td height="31" colspan="6" align="left" valign="middle"><strong>Tracking Method</strong></td>
              </tr>
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="770" colspan="5" align="left" valign="middle">
                	<form:hidden path="vendorId"/>
                	<form:hidden path="trackingMethodId"/>
                </td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="trackingType">Tracking Type<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="trackingType" class="inptformSelect" style="width:243px;"
                  	onchange="openDivRecruit(this.value, 'Survey');">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="No Tracking"/>
                      <form:option value="2" label="URL Tracking"/>
                      <form:option value="3" label="Pixel Tracking"/>
                      <form:option value="4" label="Server to Server"/>
                  </form:select>
                </td>                
              </tr>
              <tr>
                <td width="317" height="31" align="left" valign="middle">
                <form:label path="variables[0].variableName">1st Variable<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:input path="variables[0].variableName" class="inptform"/>
                	<form:hidden path="variables[0].variableId"/>
                </td>
              </tr>
              <tr>
              	<td colspan="2">
              	  <div id="variableDiv" style="display: none;">
              		<table>
              		  <tr>
		                <td width="313" height="31" align="left" valign="middle">
		                <form:label path="variables[1].variableName">2nd Variable</form:label></td>
		                <td colspan="5" align="left" valign="middle">
		                	<form:input path="variables[1].variableName" class="inptform"/>
		                	<form:hidden path="variables[1].variableId"/>
		                </td>
		              </tr>
		              <tr>
		                <td width="313" height="31" align="left" valign="middle">
		                <form:label path="variables[2].variableName">3rd Variable</form:label></td>
		                <td colspan="5" align="left" valign="middle">
		                	<form:input path="variables[2].variableName" class="inptform"/>
		                	<form:hidden path="variables[2].variableId"/>
		                </td>
		              </tr>
              		</table>
              	  </div>
              	</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="sendUrl">Send UserID in URL or Pixel or S2S</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="sendUrl" class="inptformSelect" style="width:243px;">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                </td>
              </tr>                                                                
			  <tr>
                <td colspan="2">
                <div id="trackingSurveyDiv" style="display: none;">
                 <!-- <div id="trackingSurveyDiv">-->                  
                  <table>
                  	<tr>
                  	  <td width="313px;" height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="urlCode"><span id="trackingSurveySpan" class="trackingSpans"></span><span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">
                  		<form:textarea path="urlCode" class="inptformmesg" style="width:468px;"/>
                  	  </td>
                  	  <td>
                	  	<div id="codeInstructionSurvey" class="vendorinstruction" style="width: 266px; height: 70px;">                  	  	  
				  		</div>
				  	  </td>
                  	  <td>
                  	  	
                  	  </td>
                  	</tr>
                  	<!-- display static IRB pages -->
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<label for="completeUrlStatic">Complete URL</label>    	  </td>
                  	  <td align="left" valign="middle"> 
                  	  	<input type="text" id="completeUrlStatic" class="inptformvendorurl" disabled="true">                 		
                  	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<label for="terminateUrlStatic">Terminate URL</label>    	  </td>
                  	  <td align="left" valign="middle"> 
                  	  	<input type="text" id="terminateUrlStatic" class="inptformvendorurl" disabled="true">                 		
                  	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<label for="quotaFullUrlStatic">Quota Full URL</label>    	  </td>
                  	  <td align="left" valign="middle"> 
                  	  	<input type="text" id="quotaFullUrlStatic" class="inptformvendorurl" disabled="true">                 		
                  	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<label for="securityTerminateUrlStatic">Security Terminate URL</label>    	  </td>
                  	  <td align="left" valign="middle"> 
                  	  	<input type="text" id="securityTerminateUrlStatic" class="inptformvendorurl" disabled="true">                 		
                  	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<label for="errorsUrlStatic">Errors URL</label>    	  </td>
                  	  <td align="left" valign="middle"> 
                  	  	<input type="text" id="errorsUrlStatic" class="inptformvendorurl" disabled="true">                 		
                  	  </td>
                  	</tr>                  
                  </table>                
                </div>                </td>
              </tr>
              <tr>
                <td colspan="2">
                <div id="trackingSurveyUrlDiv" style="display: none;">
                 <!-- <div id="trackingSurveyUrlDiv">-->                  
                  <table>
                  	<tr>
                  	  <td width="313px;" height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="completeUrl">Complete URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="completeUrl" class="inptformvendorurl"/>
                  	  </td>
                  	  <td rowspan="5">
                	  	<div id="urlInstructionSurvey" class="vendorinstruction" style="width: 266px;">
                	  		<b>Note:</b><br>
                	  		Use #id1# to pass the panelist id in the URL. Refer the example below:<br>
							http://survey.irbureau.com/vendorEndPages/vendor.jsp?vid1=#id1#
				  		</div>
				  	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="terminateUrl">Terminate URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="terminateUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="quotaFullUrl">Quota Full URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="quotaFullUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="securityTerminateUrl">Security Terminate URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="securityTerminateUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  	<tr>
                  	  <td height="31" align="left" valign="top" style="padding-top: 10px;">
                  	  	<form:label path="errorsUrl">Errors URL<span>*</span></form:label>                  	  </td>
                  	  <td align="left" valign="middle">                  		
                  		<form:input path="errorsUrl" class="inptformvendorurl"/>                	  </td>
                  	</tr>
                  </table>                
                </div>                </td>
              </tr>
			  <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><strong>Routing Preferences</strong>
                	<form:hidden path="routingId"/>
                </td>               
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="routingAllowed">Routing Allowed</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="routingAllowed" class="inptformSelect" style="width:243px;"
                  	onchange="openRoutingFields(this.value, 'Survey');">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No"/>                    
                  </form:select>                </td>
              </tr>
              <tr>
              	<td colspan="2">
              		<div id="maxRoutedSurveyRow" style="display: none;">
              		<table>
              		  <tr>
		                <td width="313px;" height="31" align="left" valign="middle">
		                	<form:label path="maxRouted">Max no. of routed surveys per respondent</form:label></td>
		                <td colspan="5" align="left" valign="middle">
		                  <form:input path="maxRouted" class="inptform"/>                </td>
		              </tr>
              		</table>
              		</div>
              	</td>
              </tr>
              <tr>
              	<td colspan="2">
              		<div id="maxQualifiedSurveyRow" style="display: none;">
              		<table>
              		  <tr>
		                <td width="313px;" height="31" align="left" valign="middle">
		                	<form:label path="maxQualified">Max no. of qualified surveys per respondent</form:label>
		                </td>
		                <td colspan="5" align="left" valign="middle">
		                  <form:input path="maxQualified" class="inptform"/>                </td>
		              </tr>
		              <tr>
		                <td height="31" align="left" valign="middle">
		                	<form:label path="cpc">CPC for routed completes</form:label></td>
		                <td colspan="5" align="left" valign="middle">
		                  <input type="text" id="currCPC" style="width:40px;" class="inptform" readonly="true"
		                  	value="${selectedCurrency}"/>
		                  <form:input path="cpc" class="inptform" style="width:180px; text-align:left;"/>                </td>
		              </tr>
              		</table>
              		</div>
              	</td>
              </tr>              			  
              <tr>
                <td height="31" align="left" valign="middle"><label for="audienceType">Audience Type</label></td>
                <td colspan="5" align="left" valign="middle"><table width="250" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="left" valign="middle"  style="padding-top:2px">
                    	<form:checkbox path="audienceType" value="1"/>
                    </td>
                    <td align="left" valign="top">B2B</td>
                    <td align="left" valign="middle" style="padding-top:2px">
                    	<form:checkbox path="audienceType" value="2"/>
                    </td>
                    <td align="left" valign="top">B2C</td>
                    <td align="left" valign="middle" style="padding-top:2px">
                    	<form:checkbox path="audienceType" value="3"/>         
                    </td>
                    <td align="left" valign="top">HC</td>
                  </tr>
              
                </table></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="surveySubCategory">Survey Category for Routing</form:label>
                	<form:hidden path="surveySubCategory" id="surveySubCategorySurvey"/>
                	<form:hidden path="completeCategory" id="completeCategorySurvey"/>
                </td>
                <td colspan="5" align="left" valign="middle">
                  <div id="categoryTreeSurvey" class="categoryTreeDiv">
                  	<ul>
                  		<c:forEach items="${categoryList}" var="category">                  						    
					      <li id="cat-${category.categoryTypeId}"
					      	<c:if test="${not empty editVendorSurvey.completeCategoryMap[category.categoryTypeId]}">class="selected"</c:if>>
					        ${category.categoryType}
					        <ul>
					       	  <c:forEach items="${category.subCategoryList}" var="subCategory">
					       	  	<li id="${subCategory.subCategoryTypeId}" 
					       	  		<c:if test="${not empty editVendorSurvey.subCategoryMap[subCategory.subCategoryTypeId]}">class="selected"</c:if> >
					       	  		${subCategory.subCategoryType}
					       	  	</li>					       	  
					       	  </c:forEach>
					        </ul>
					      </li>					      					      					      					    					  
                  		</c:forEach>
                  	</ul>
                  </div></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="minConversion">Minimum Conversion for Routing</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="minConversion" class="inptformSelect" style="width:243px;" >
                     <form:option value="101" label="--Select--"></form:option>                     
                     <form:option value="0" label="<1%"></form:option>
                     <c:forEach var="index" begin="1" end="100">
                     	<form:option value="${index}" label="${index}%"></form:option>
                     </c:forEach>
                   </form:select>
                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="maxLoi">Maximum LOI for Routing</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="maxLoi" class="inptform" style="width:180px; text-align:left;"/>
                  <input type="text" style="width:40px;" class="inptform" readonly="true" value="min"/>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><strong>Branding Preferences</strong></td>               
              </tr>    
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="brandingType">Select Branding</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="brandingType" class="inptformSelect" style="width:243px;">
                      <form:option value="0" label="--Select--"/>
                      <form:option value="1" label="IRB Branding"/>
                      <form:option value="2" label="Vendor Branding"/> 
                      <form:option value="3" label="No Branding"/>                    
                  </form:select>                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="helpLink">Help Link</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="helpLink" class="inptform"/>                </td>
              </tr>              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="emailCollection">Email Collection</form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:select path="emailCollection" class="inptformSelect" style="width:243px;">
                      <form:option value="-1" label="--Select--"/>
                      <form:option value="1" label="Yes"/>
                      <form:option value="0" label="No" selected="true"/>                    
                  </form:select>                </td>
              </tr>                       
              <tr>
                <td height="30" colspan="6" align="left" valign="middle"></td>
                </tr>
            </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit5" id="Submit5" value="Save" class="btnsubmit"
              	 	onclick="getSelectedCategories('categoryTreeSurvey');return surveyValidation();">
              </td>
              <td width="118" align="center" valign="middle"></td>            
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table></p>
        </div>
        </form:form>

    </div>
      
      </div>
    </div>
  </div>
  </div>
<!-- Javascript -->
<script type="text/javascript" src="js/vendors/vendorScript.js"></script>
<script type="text/javascript">
var pass=true;
var rowNumber='${editVendor.addContactsSize}';
var i;
var btn_close='<td width="60" align="left" valign="middle" id="btn-close" class="btn-remove">' + 
	'<img src="images/btncross.png" width="18" height="17" border="0">' + 
	'</td>';
$(document).ready(function(){
	//Code for tabbed pages
	 $('#horizontalTab').responsiveTabs({
            startCollapsed: 'accordion',
            collapsible: true,
            rotate: false,
            setHash: true
        });
	
	//Add more functionality for additional contacts
		$("#addMore").click(function() {	
			 var tr_temp=$("#addContacts tr:first").clone();
			 tr_temp.find("#add_name0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_name' + i},
	    				'name': function(_, name) { return 'additionalContacts['+ i +'].name'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_job0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_job' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].jobTitle'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_email0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_email' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].email'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_phone0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_phone' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].phoneNo'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#add_ext0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_ext' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].extno'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close);
			 tr_temp.appendTo("#addContacts");
			 i++;			 			
		});				
		
		$("#btn-close").live('click',function(){
			$(this).parent().remove();			
		});
		
});

var values=new Array();
function getRowValues()
{
	//initialize i
	if(rowNumber==0){
		i=1;
	}
	else{
		i=rowNumber;
	} 
	for(var j=0;j<rowNumber;j++)
      {
            values[j]=new Array();
            values[j][0]=document.getElementById('add_name'+j).value;
            values[j][1]=document.getElementById('add_job'+j).value;			
			values[j][2]=document.getElementById('add_email'+j).value;
			values[j][3]=document.getElementById('add_phone'+j).value;
			values[j][4]=document.getElementById('add_ext'+j).value;			
      }
	preName = document.getElementById('vendorName').value;
	preCode = document.getElementById('vendorCode').value;
	preUrl = document.getElementById('websiteUrl').value;
}

var contactIdList;
function checkRowUpdation()
{
    contactIdList="";  
	for(var i=0;i<rowNumber;i++)
      {
            if(values[i][0]!=document.getElementById('add_name'+i).value || values[i][1]!=document.getElementById('add_job'+i).value || 
				values[i][2]!=document.getElementById('add_email'+i).value || values[i][3]!=document.getElementById('add_phone'+i).value || 
				values[i][4]!=document.getElementById('add_ext'+i).value)
            {  
            	contactIdList+=i+",";
            }         
      }
	  document.getElementById("contactIdList").value = contactIdList;
}

function checkName(vendorName){
	if(preName != vendorName){
		validateVendorName(vendorName);
	}
}

function checkCode(vendorCode) {
	if(preCode != vendorCode){
		validateVendorCode(vendorCode);	
	}
}

function checkUrl(vendorUrl) {
	if(preUrl != vendorUrl){
		validateWebUrl(vendorUrl);
	}
}

function replace(documentType){
	var cStatus = confirm("Are you sure you want to replace the file?");
	var html;
	var accept = 'accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"';
	if(cStatus==true){
		if(documentType == "vendorForm"){
			accept = 'accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"';
		}
		html = '<input type="file" name="' + documentType + '" id="' + documentType + '"' + accept + 
		'onchange="documentValidation(this.value, this.id);">';
		document.getElementById(documentType + 'Div').innerHTML = html;
	}
}
</script>
<script src="resources/tabs/js/jquery.responsiveTabs.js" type="text/javascript"></script>
<!-- Javascript ends -->
</body>
</html>    