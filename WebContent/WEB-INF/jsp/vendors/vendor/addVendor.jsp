<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%
	String topMenu = "Vendors";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex, nofollow" />
<title>Add Vendor - PASMT v2.0</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<!-- tabbed page -->
<link type="text/css" rel="stylesheet" href="resources/tabs/css/responsive-tabs.css" />
<link type="text/css" rel="stylesheet" href="resources/tabs/css/style.css" />
<!-- tabbed page include ends -->
</head>
<body>
<!--Header Start-->
<%@include file="../../include/header.jsp"%>
<!--Header End-->
<!--Middle Box Start-->
<div class="middleboxcontainer">
  <div class="middlebox">
    <!--left Navigation Start-->
    <%@include file="../../include/left.jsp"%>
    <!--left Navigation End-->
    <div class="rightbox">
      <div class="searchresult">
      <h2> Add Vendor</h2>
           <div id="horizontalTab">
        <ul>
            <li><a href="#tab-1">Add Vendor Details</a></li>          
             
        </ul>
		<form:form method="POST" action="addVendor" modelAttribute="addVendor" enctype="multipart/form-data">
        <div id="tab-1" style="background-color:#D2DCBF;">
            <p style="margin-left:0px;"><table width="1087" border="0" cellpadding="0" cellspacing="0" style="margin-top:1px;">
        
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" style="font-size:12px;">
           <table style="margin-left:5px;" width="1000" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="189" align="left" valign="middle"><span>*</span>indicates mandatory field </td>
              <td width="811" align="left" valign="middle"><span id="validation" style="font-size:16px;"></span></td>
            </tr>
           </table>
          </td>
        </tr>
        <tr>
          <td height="37" align="left" valign="middle" bgcolor="#D2DCBF" >
          
            <table width="1087" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="middle">&nbsp;</td>
                <td width="874" colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle">
                <form:label path="vendorName">Vendor Name <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                  <form:input path="vendorName" class="inptform" onchange="validateVendorName(this.value);"/>
                   <br/> 
                  <span id="msgVendorName" style="padding-left: 20px;"></span>                </td>                
              </tr>
              <tr>
                <td width="213" height="31" align="left" valign="middle">
                <form:label path="vendorCode">Vendor Code <span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:input path="vendorCode" class="inptform" onchange="validateVendorCode(this.value);"/>
                	<br/>
                  	<span id="msgVendorCode" style="padding-left: 20px;"></span>                </td>
              </tr>                        
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="vendorType">Vendor Type<span>*</span></form:label>
                </td>
                <td colspan="5" align="left" valign="middle">
                  <div id="vendorTypeDiv" class="vtypediv">
                  	 <c:forEach items="${vendorTypeList}" var="vType">
                  	 	<form:checkbox path="vendorType" value="${vType.id}" label="${vType.name}"
                  	 	 style="margin-right:3px;"/><br/>
                  	 </c:forEach>
                  </div>
                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">
                	<form:label path="accountManager">IRB Account Manager<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="accountManager" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${managerList}" itemValue="id" itemLabel="name"/>
                	</form:select>                </td>
              </tr>              
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Primary Contact</strong></td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="53" height="57" align="left" valign="middle">
                    <form:label path="primaryContact.name">Name<span>*</span></form:label></td>
                    <td width="150" align="left" valign="middle">
                    	<form:input path="primaryContact.name" class="inptform" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle"><form:label path="primaryContact.jobTitle">Job Title<span>*</span></form:label></td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="primaryContact.jobTitle" class="inptform" style="width:130px;"/>                    </td>
                    <td width="73" align="center" valign="middle"><form:label path="primaryContact.email">Email<span>*</span></form:label></td>
                    <td width="142" align="left" valign="middle">
                    	<form:input path="primaryContact.email" class="inptform" style="width:130px;"/>                   	</td>
                    <td width="96" align="center" valign="middle"><form:label path="primaryContact.phoneNo">Phone No.<span>*</span></form:label></td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="primaryContact.phoneNo" class="inptform" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle"><form:label path="primaryContact.extno">Ext No.</form:label></td>
                    <td width="101" align="left" valign="middle">
                    	<form:input path="primaryContact.extno" class="inptform" style="width:65px;"/>                    </td>
                    <td width="60" align="left" valign="middle"></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td height="19" colspan="6" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><strong>Additional Contacts</strong></td>
                </tr>
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><table width="1087" border="0" cellspacing="0" cellpadding="0">
                 <tbody id="addContacts">
                  <tr>
                    <td width="53" height="57" align="left" valign="middle">Name  </td>
                    <td width="150" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].name" id="add_name0" class="inptform" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle">Job Title  </td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].jobTitle" class="inptform" id="add_job0" style="width:130px;"/>                    </td>
                    <td width="73" align="center" valign="middle">Email  </td>
                    <td width="142" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].email" class="inptform" id="add_email0" style="width:130px;"/>                    </td>
                    <td width="96" align="center" valign="middle">Phone No.  </td>
                    <td width="137" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].phoneNo" class="inptform" id="add_phone0" style="width:130px;"/>                    </td>
                    <td width="88" align="center" valign="middle">Ext No. </td>
                    <td width="101" align="left" valign="middle">
                    	<form:input path="additionalContacts[0].extno" class="inptform" id="add_ext0" style="width:65px;"/>                    </td>
                    <td width="60" align="left" valign="middle" style="display: block;"></td>                      
                  </tr>
                </tbody>  
                </table></td>
                </tr>
                          
              <tr>
                <td height="31" colspan="6" align="left" valign="middle"><input type="button" name="Submit" id="addMore" value="Add More +" class="btnsubmit"></td>
                </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:8px;"><form:label path="address">Address<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:textarea path="address" class="inptformmesg"/>                </td>
              </tr>
              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="city">City<span>*</span></form:label></td>
                <td align="left" valign="middle"><form:input path="city" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="state">State<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="state" class="inptform"/></td>
              </tr>
              <tr>
               <td height="31" align="left" valign="middle"><form:label path="postalCode">Postal Code<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="postalCode" class="inptform"/></td>
              </tr>
              <tr>
              <td height="31" align="left" valign="middle"><form:label path="country">Country<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="country" class="inptformSelect" style="width:243px;">
                		<form:option value="0" label="--Select--"/>
                		<form:options items="${countryList}" itemValue="id" itemLabel="name"/>
                	</form:select>                </td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="websiteUrl">Website URL<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="websiteUrl" class="inptform" onchange="validateWebUrl(this.value);"/>
                	<br/> 
                  	<span id="msgWebUrl" style="padding-left: 20px;"></span>                </td>
              </tr>            
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="phoneNo">Phone No.<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="phoneNo" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="faxNo">Fax No.</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="faxNo" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="rfqEmail">RFQ Email</form:label></td>
                <td colspan="5" align="left" valign="middle"><form:input path="rfqEmail" class="inptform"/></td>
              </tr>
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="vendorForm">Vendor Form<span>*</span></form:label></td>
                <td colspan="3" align="left" valign="top">
                	<form:input path="vendorForm" type="file"
                    	 accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    	 onchange="documentValidation(this.value, this.id);"/>                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="guidelineDocs">Guideline Documents</form:label></td>
                <td colspan="3" align="left" valign="top">
                	<form:input path="guidelineDocs" type="file"
                    	 accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 onchange="documentValidation(this.value, this.id);"/>                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="signedIO">Signed IO</form:label></td>
                <td colspan="3" align="left" valign="top" style="width: 50px;">
                	<form:input path="signedIO" type="file"
                    	accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	onchange="documentValidation(this.value, this.id);"/>
                    	<span id="validation_document" style="padding-left: 20px;"></span>               </td> 
                   <td></td>               
              </tr>                                         
              <tr>
                <td height="31" align="left" valign="top"><form:label path="panelBook">Panel Book</form:label></td>
                <td colspan="3" align="left" valign="top">
                	<form:input path="panelBook" type="file"
                    	 accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 onchange="documentValidation(this.value, this.id);"/>                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="companyBrochure">Company Brochure</form:label></td>
                <td colspan="3" align="left" valign="top">
                	<form:input path="companyBrochure" type="file"
                    	 accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 onchange="documentValidation(this.value, this.id);"/>                </td>      
              </tr>
              <tr>
                <td height="31" align="left" valign="top"><form:label path="esomar">Esomar 26/28</form:label></td>
                <td colspan="3" align="left" valign="top">
                	<form:input path="esomar" type="file"
                    	 accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                    	 onchange="documentValidation(this.value, this.id);"/>                </td>      
              </tr>                                      
              <tr>
                <td height="31" align="left" valign="middle">&nbsp;</td>
                <td colspan="5" align="left" valign="middle">&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td height="31" align="left" valign="middle"><form:label path="vendorStatus">Vendor Status<span>*</span></form:label></td>
                <td colspan="5" align="left" valign="middle">
                	<form:select path="vendorStatus" class="inptformSelect" style="width:243px;">        
                		<form:option value="0" label="--Select--"/>
                		<form:option value="1" label="Active"/>
                		<form:option value="2" label="Hold"/>
                		<form:option value="3" label="Inactive"/>
                	</form:select>                </td>
              </tr> 
              <tr>
                <td height="31" align="left" valign="top" style="padding-top:10px;"> 
                	<form:label path="tAndC">Terms and Conditions </form:label>                </td>
                <td align="left" valign="middle">
                  <form:textarea path="tAndC" class="inptformmesgt"/>	            </td>
              </tr>                                      
              <tr>
                <td height="30" colspan="6" align="left" valign="middle"></td>
                </tr>
            </table>
                  </td>
        </tr>
        <tr>
          <td height="2" align="left" valign="middle" >              </td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF"><table width="367" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="219" align="right" valign="middle">
              	<input type="submit" name="Submit2" id="Submit2" value="Submit" class="btnsubmit" 
              		onClick="return vendorValidation(0);">
              </td>
              <td width="118" align="center" valign="middle">
              	<input type="reset" name="Submit3" id="Submit3" value="Clear" class="btnsubmit">
              </td>            
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="middle" bgcolor="#D2DCBF">&nbsp;</td>
        </tr>
      </table></p>
        </div>
     </form:form>
	
    </div>
      
      </div>
    </div>
  </div>
  </div>
<!-- Javascript -->
<script type="text/javascript" src="js/vendors/vendorScript.js"></script>
<script type="text/javascript">
var pass=true;
var i = 1;
var btn_close='<td width="60" align="left" valign="middle" id="btn-close" class="btn-remove">' + 
	'<img src="images/btncross.png" width="18" height="17" border="0">' + 
	'</td>';
$(document).ready(function(){
	//Code for tabbed pages
	 $('#horizontalTab').responsiveTabs({
            startCollapsed: 'accordion',
            collapsible: true,
            rotate: false,
            setHash: true
        });
		
	//Add more functionality for additional contacts
		$("#addMore").click(function() {	
			 var tr_temp=$("#addContacts tr:first").clone();
			 tr_temp.find("#add_name0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_name' + i},
	    				'name': function(_, name) { return 'additionalContacts['+ i +'].name'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_job0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_job' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].jobTitle'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_email0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_email' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].email'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("#add_phone0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_phone' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].phoneNo'},
	    				'value': ''
	  			});
				}).end();
			 tr_temp.find("#add_ext0").each(function() {
				 $(this).attr({
	    				'id': function(_, id) { return 'add_ext' + i},
	    				'name': function(_, name) { return 'additionalContacts[' + i + '].extno'},
	    				'value': ''
	  			});
				}).end();			 
			 tr_temp.find("td:last").remove();
			 tr_temp.append(btn_close);
			 tr_temp.appendTo("#addContacts");
			 i++;			 			
		});				
		
		$("#btn-close").live('click',function(){
			$(this).parent().remove();			
		});
		
});


</script>
<script src="resources/tabs/js/jquery.responsiveTabs.js" type="text/javascript"></script>
<!-- Javascript ends -->
</body>
</html>    