				<tr class="paginglink">
                    <td colspan="7" style="text-align:right; padding-top: 8px;" ><pg:index>
                        <pg:prev><a href="${pageUrl}" style="text-decoration: none;"> [&lt;&lt; Prev]</a> </pg:prev>
                        <pg:pages>
                            <bean:define id="pageNo" value="<%=pagerPageNumber.toString()%>"/>
                            <logic:equal name="pageNo" value="<%=pageNumber.toString()%>"> <%=pageNumber%> </logic:equal>
                            <logic:notEqual name="pageNo" value="<%=pageNumber.toString()%>">
                                <%
                                    if(request.getParameter("pager.offset")!=null)
                                    {
                                        if((Integer.parseInt(request.getParameter("pager.offset"))/15 + 1)==Integer.parseInt(pageNumber.toString()))
                                        {
                                %>
                                <span><b><%= pageNumber %></b></span>
                                <%
                                }
                                else
                                {
                                %>
                                <a href="${pageUrl}" ><%= pageNumber %></a>
                                <%
                                    }
                                }
                                else
                                {
                                    if(pageNumber.toString()=="1")
                                    {
                                %>
                                <span><b><%= pageNumber %></b></span>
                                <%
                                }
                                else
                                {
                                %>
                                <a href="${pageUrl}" ><%= pageNumber %></a>
                                <%
                                        }
                                    }
                                %>
                            </logic:notEqual>
                        </pg:pages>
                        <pg:next> <a href="${pageUrl}" style="text-decoration: none"> [Next &gt;&gt;]</a> </pg:next>
                    </pg:index></td>
                </tr>