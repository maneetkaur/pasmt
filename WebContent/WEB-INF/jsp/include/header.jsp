<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="com.login.LoginBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="headerbox">
  <div class="logobox">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="79%" align="left" valign="middle"><table  style="margin-left:30px; margin-top:10px;"width="431" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="107" height="55" align="left" valign="middle"><img src="images/logo.png" width="86" height="44"></td>
            <td width="324" align="left" valign="middle"><strong>PANEL AND SURVEY MANAGEMENT TOOL</strong><br>
                <span class="grytxt" style="text-decoration:none;">PASMT Version 2.0 <span style="padding-left:25px;">Powered by IRB</span></span></td>
          </tr>
        </table></td>
        <%
        	LoginBean login = (LoginBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        %>
        <c:set var="empName" value="<%=login.getEmployeeName()%>"></c:set>
        <%
        	login = null;
        %>
        <td width="21%" align="left" valign="middle" style="font-size:11px;">
        	<a href="editProfileForm" class="grytxt">${empName}</a> |
        	<a href="#" class="grytxt">Help Desk</a> | 
        	<a class="grytxt" href="<c:url value="/j_spring_security_logout"/>">Logout</a></td>
      </tr>
    </table>
  </div>
  <div class="nav">
    <ul id="nav" name="nav">
      <li class="<%if(topMenu.equals("Sales")) out.println("selected"); %>"><a href="viewClientList">Sales</a></li>
      <li class="<%if(topMenu.equals("Projects")) out.println("selected"); %>"><a href="projectList">Projects</a></li>
      <li><a href="#">Screeners</a></li>
      <li><a href="#">Mailing</a></li>
      <li><a class="<%if(topMenu.equals("Vendors")) out.println("selected"); %>" href="vendorList">Vendors</a></li>
      <li><a href="#">Reports</a></li>
      <li><a href="#">CSAT</a></li>
      <li><a href="#">Settings</a></li>
      <li><a href="#">Quality</a></li>
      <li class="<%if(topMenu.equals("Finance")) out.println("selected"); %>">
      	<a href="sAdminFinanceDashboard">Finance</a>
      </li>
      <li><a class="<%if(topMenu.equals("Admin")) out.println("selected"); %>" href="languagelist">Panel Admin</a></li>
      <li><a class="<%if(topMenu.equals("PasmtAdmin")) out.println("selected"); %>" href="surveyCategoryList">Admin</a></li>
    </ul>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript"></script>-->
    
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.spasticNav.js"></script>
    
    
    <script type="text/javascript">
    $('#nav').spasticNav();
</script>
  </div>
</div>