<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="leftnavi">
	<%
		if(topMenu.equals("Sales")){
	%>
      <h1>Sales</h1>
      <ul>
        <li><a href="inProgress">Dashboard</a></li>
        <!-- <li ><a href="viewClientList" class="active">Clients</a></li> -->
        <li><a href="viewClientList">Clients</a></li>
        <li><a href="inProgress">Feasibility</a></li>
        <li><a href="inProgress">Price Grid</a></li>
        <li><a href="bidList">Bids</a></li>
        <li><a href="proposalList">Proposal</a></li>
      </ul>
	<%
		}
		else if(topMenu.equals("Projects")){
	%>
			<h1>Projects</h1>
		      <ul>
		        <li><a href="#">Dashboard</a></li>
		        <li><a href="projectList">Projects</a></li>		     
		        <li><a href="surveyList">Surveys</a></li>
		        <li><a href="#">Screener</a></li>
		        <li><a href="#">Sampling</a></li>		       
		        <li><a href="#">Routing</a></li>
		        <li><a href="#">Vendor</a></li>
		        <li><a href="#">Invitation</a></li>
		        <li><a href="#">Tracking</a></li>
		        <li><a href="#">Survey Reports</a></li>
		        <li><a href="#">Quality Control</a></li>
		        <li><a href="#">Respondent Lookup</a></li>
		        <li><a href="#">Re-Allow Panelists</a></li>
		        <li><a href="#">Client Final Ids</a></li>
		        <li><a href="#">Survey Priority</a></li>
		        <li><a href="projectSummaryList">Project Summary</a></li>
		      </ul>
	<%
		}
		else if(topMenu.equals("Finance")){
	%>
			<h1>Finance</h1>
		      <ul>
		        <li><a href="sAdminFinanceDashboard">Dashboard</a></li>		        
		        <li><a href="sAdminInvoiceReceivableList">Invoice Receivable</a></li>
		        <li><a href="sAdminInvoicePayableList">Invoice Payable</a></li>
		        <li><a href="#">Local Expense</a></li>		       
		        <li><a href="sAdminBankingDetailList">Banking</a></li>		        
		      </ul>
	<%
		}
		else if(topMenu.equals("Admin")){
	%>
			<h1>Panel Admin</h1>
		      <ul>
		        <li><a href="languagelist">Language</a></li>
		        <li><a href="countryList">Country</a></li>
		        <li><a href="cultureList">Culture</a></li>
		        <li><a href="regionList">Region</a></li>
		        <li><a href="companyList">Company</a></li>
		       <li><a href="categoryList">Category</a></li>
		        <li><a href="subCategoryList">SubCategory</a></li>
		        <li><a href="questionTypeList">Question Type</a></li>
		        <li><a href="registrationQuestionList">Registration Question </a></li>
                <li><a href="questionList">Global Question</a></li>
		         <li><a href="cultureQuestionView">Culture Question</a></li>
		        <li><a href="panelList">Panel</a></li>
		      </ul>
	<%
		}
		else if(topMenu.equals("Vendors")){
	%>
			<h1>Vendors</h1>
				<ul>
				   <li><a href="inProgress">Dashboard</a></li>
				   <li><a href="vendorList">Vendor</a></li>
				   <li><a href="inProgress">Purchase Order</a></li>
				   <li><a href="inProgress">Vendor Bids</a></li>						   				    
				</ul>
	<%
		}
		else if(topMenu.equals("PasmtAdmin")){
	%>
			<h1>Admin</h1>
				<ul>
				   <li><a href="surveyCategoryList">Survey Category</a></li>
				   <li><a href="sAdminUserList">User</a></li>
				   <li><a href="inProgress">Incentive</a></li>
				   <li><a href="inProgress">Template</a></li>
				   <li><a href="inProgress">Subject Line</a></li>
				   <li><a href="inProgress">Messages</a></li>						   				    
				</ul>
	<%
		}
		else if(topMenu.equals("Profile")){
			%>
					<h1>Profile</h1>
						<ul>
						   <li><a href="editProfileForm">User Info</a></li>
						   <li><a href="changePasswordForm">Change Password</a></li>						 						   				    
						</ul>
			<%
				}
	%>
</div>