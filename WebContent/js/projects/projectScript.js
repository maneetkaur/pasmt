function getProposalInformation(proposalId){
	window.location.href="addProjectForm?proposalId="+proposalId;
}

function getContactInfo(contactId, contactType){
	if(contactId==0){
		if(contactType==1){
			document.getElementById('pmContact').value = "";
			document.getElementById('pmEmail').value = "";
		}
		else if(contactType==2){
			document.getElementById('tlContact').value = "";
			document.getElementById('tlEmail').value = "";
		}
		return;
	}
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = function(){			
			if (request.readyState != 4) return;
			if (request.status != 200) return;			
			var result = request.responseText.split(":");
			if(contactType==1){
				document.getElementById('pmContact').value = result[1];
				document.getElementById('pmEmail').value = result[0];
			}
			else if(contactType==2){
				document.getElementById('tlContact').value = result[1];
				document.getElementById('tlEmail').value = result[0];
			}			
			//request.abort();
			//request = null;
		};
		request.open("GET","getContactInfo?contactId=" + contactId + "&action=getContactInfo",true);
		request.send(true);	
 	}
}

function editProjectInit(){	
	var result = pmContactInfo.split(":");
	document.getElementById('pmContact').value = result[1];
	document.getElementById('pmEmail').value = result[0];
	
	result = tlContactInfo.split(":");
	document.getElementById('tlContact').value = result[1];
	document.getElementById('tlEmail').value = result[0];
}

function projectValidation(from){
	pass=projectValidationStatus(from);
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
}

function projectValidationStatus(from) {
	pass=true;
	//Check all the mandatory fields
	if((from==0 && document.getElementById('proposalId').value=="0") || 
		document.getElementById('projectStatus').value=="0" || document.getElementById('teamName').value=="" || 
		document.getElementById('projectManager').value=="0" || document.getElementById('teamLeader').value=="0"){
		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	var cultures = document.getElementsByName('culture');
	var count = 0;
	for(var i=0; i<cultures.length; i++){
		if(cultures[i].checked==true){
			count++;
		}
	}
	if(count==0){
		document.getElementById('validation').innerHTML="Please select atleast one culture.";
		return false;
	}
	
	return true;
}