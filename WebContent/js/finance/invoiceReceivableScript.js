var stAmount =0.0;

function getProjectInformation(projectSummaryId){
	window.location.href="sAdminAddInvoiceReceivableForm?projectSummaryId="+projectSummaryId;
}

function displayServiceTax(stApplicable){
	if(stApplicable=="1"){
		document.getElementById('serviceTax').disabled = false;
		document.getElementById('serviceTax').value = 12.36;
		applyServiceTax(12.36);
	}
	else{
		document.getElementById('serviceTax').disabled = true;
		document.getElementById('serviceTax').value = 0;
		applyServiceTax(0);
	}
}

function applyServiceTax(serviceTaxVal){
	var amount = document.getElementById('projectValue').value;;
	if(serviceTaxVal!=""){		
		stAmount = Number(amount) * Number(serviceTaxVal)/100;		
	}
	else{
		stAmount=0.0;
	}
	stAmount = parseFloat(stAmount).toFixed(2);
	document.getElementById('stAmount').value = currencyVal + " " + stAmount;
	amount = parseFloat(Number(stAmount) + Number(amount)).toFixed(2);
	document.getElementById('grandTotal').value = currencyVal + " " + amount;
	currencyFormat(amount, document.getElementById('amount'));
}

function irInitialize(from){
	if(reqSize==0){
		i=1;
	}
	else{
		i=reqSize;
	}
	rowCost="0.00";
	setup=0.0, unitPrice=0.0, totalcost=0.0;
	for(var j=0; j<i; j++){
		if(document.getElementById('r_service' + j)){	
			var setup = document.getElementById('r_setup' + j).value;
			var unitPrice = document.getElementById('r_cpi' + j).value;	
			if(document.getElementById('r_service' + j).value =="Sample"){
				//quantity
				var quantity = document.getElementById('r_sample' + j).value;
				
				if(quantity !="" && unitPrice !=""){
					rowCost = parseFloat((quantity * unitPrice)+ Number(setup)).toFixed(2);
				}
			}
			else {				
				if(unitPrice!=""){
					rowCost = parseFloat(Number(unitPrice)+Number(setup)).toFixed(2);
				}
				else{
					rowCost = parseFloat(setup).toFixed(2);
				}
			}
			totalcost =Number(totalcost) + Number(rowCost);	
			document.getElementById('r_total' + j).value = currencyVal + " " + rowCost;
		}
	}
	totalcost = parseFloat(totalcost).toFixed(2);
	document.getElementById('totalSum').value = currencyVal + " " +totalcost;
	document.getElementById('grandTotal').value = currencyVal + " " + document.getElementById('projectValue').value;
	if(from==0){
		document.getElementById('stAmount').value = currencyVal + " 0.00";
		currencyFormat(document.getElementById('projectValue').value, document.getElementById('amount'));
	}
	else if(from==1){
		var stApp = document.getElementById('serviceTaxApplicable').value;
		if(stApp==0 || stApp==2){
			document.getElementById('serviceTax').disabled = true;
			document.getElementById('serviceTax').value = 0;
			applyServiceTax(0);
		}
		else if(stApp==1){
			document.getElementById('serviceTax').disabled = false;
			var st = document.getElementById('serviceTax').value;
			applyServiceTax(st);
		}
		currencyFormat(document.getElementById('amountUsd').value, document.getElementById('amountUsd'));
	}
}

function invoiceReceivableValidation(from){
	pass = invoiceReceivableValidationStatus(from);
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";		
	}
	return pass;
}

function invoiceReceivableValidationStatus(from) {
	pass=true;
	var sta = document.getElementById('serviceTaxApplicable').value;
	var stValue = document.getElementById('serviceTax').value;
	//Check all the mandatory fields
	if((from==0 && document.getElementById('projectSummaryId').value=="0") || 
		document.getElementById('month').value=="" || document.getElementById('invoiceTo').value=="0" ||
		document.getElementById('clientPO').value=="" || document.getElementById('transactionType').value=="0" ||
		document.getElementById('description').value=="" || sta=="0" ||
		(sta=="1" && stValue == "") || document.getElementById('amountUsd').value=="" || 
		document.getElementById('paymentMode').value=="0" ||
		document.getElementById('paymentTerms').value=="" || document.getElementById('bank_detail_id').value=="0" ){
		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	
	//check for integer values of service tax, usd amount and payment terms
	if(sta=="1"){
		if(!(stValue>=0)){
			document.getElementById('validation').innerHTML="The service tax can only be in numbers";
			return false;
		}
	}
	var amountUsd = document.getElementById('amountUsd').value;
	amountUsd = amountUsd.replace(",", "");
	if(!( amountUsd >= 0)){
		document.getElementById('validation').innerHTML="The Amount in USD can only be in numbers";
		return false;
	}
	if(!(document.getElementById('paymentTerms').value >= 0)){
		document.getElementById('validation').innerHTML="The payment terms can only be in numbers";
		return false;
	}
	
	//For edit
	if(from == 1){
		var paymentStatusVal = document.getElementById('paymentStatus').value;
		if(paymentStatusVal == "0"){
			document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
			return false;
		}
		//If status is declined, then the payment remarks are mandatory
		else if(paymentStatusVal=="3"){
			if(document.getElementById('paymentRemarks').value==""){
				document.getElementById('validation').innerHTML="Please enter payment remarks";
				return false;
			}
		}
		//If payment status is paid, then the payment details must be entered
		else if(paymentStatusVal=="2"){
			//paymentModeReceived
			if(document.getElementById('paymentMadeOn').value=="" || 
					document.getElementById('paymentModeReceived').value=="0" ||					
					document.getElementById('receivedBankId').value=="0" ||
					(document.getElementById('transactionId').value=="" &&
					document.getElementById('netBankingNo').value=="" &&
					document.getElementById('chequeNo').value=="")){
				document.getElementById('validation').innerHTML="Please enter payment details as the payment has" +
						" been made";
				return false;
			}
		}
	}
}

function calculateDueDate(paymentTerms){
	var raisedOnVal = document.getElementById('raisedOn').value;
	var myDate = new Date(raisedOnVal);
	myDate.setDate(myDate.getDate()+Number(paymentTerms));
	var dueDate = myDate.toDateString();
	dueDate = dueDate.substring(4,10) + "," + dueDate.substring(10);
	document.getElementById('dueOn').value = dueDate;
	var overDueVal = parseInt(((new Date()) - myDate)/(1000*60*60*24));
	if(overDueVal > 0){
		document.getElementById('overDueBy').value = overDueVal;
	}
	else{
		document.getElementById('overDueBy').value = 0;
	}
}

function getBankAccList(bankName, from){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	
	if (request) 
	{
		request.onreadystatechange = function(){				
			if (request.readyState != 4) return;
			if (request.status != 200) return;						
			
			var response = request.responseText;
			var list = response.replace(/^\s+|\s+$/g, "");
			var jsondata = eval("("+list+")");
			var option='';			
			for(var i=0; i<(jsondata.selectoption0[0].option).length; i++){
				option = option+'<option value="' + jsondata.selectoption0[0].option[i].value + '" >' + 
					jsondata.selectoption0[0].option[i].param + '</option>';
			}
			if(from==1){
				document.getElementById("bank_detail_id").innerHTML = option;
				document.getElementById("bank_detail_id").disabled = false;
			}
			else if(from==2){
				document.getElementById("receivedBankId").innerHTML = option;
				document.getElementById("receivedBankId").disabled = false;
			}
		};
		request.open("GET","getBankAccList?bankName=" + bankName + "&action=getBankAccList",true);
		request.send(true);
 	}
}