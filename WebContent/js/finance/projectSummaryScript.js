var quantitySum, setupSum, eQuantitySum, eSetupSum, totalSum=0, rowCost=0, eRowCost=0, eTotalSum=0, currencyVal="",
	dquantitySum, dTotalSum=0, dRowCost=0, projectValue=0;

function addQuantity() {
	quantitySum = 0;
	for(var j=0; j<i; j++){
		if(document.getElementById('r_sample' + j)){
		  if(document.getElementById('r_service' + j).value == 2){
			  if(document.getElementById('r_sample' + j).value!=""){
				  quantitySum = quantitySum + (parseInt)(document.getElementById('r_sample' + j).value);
			  }
		  }
		  else{
			if(document.getElementById('r_service' + j).value != 0){
				if(document.getElementById('r_sample' + j).value==""){
					document.getElementById('r_sample' + j).value="NA";
				}
			}		
		  }
		}
	}
	document.getElementById('quantitySum').value=quantitySum;
}

function addSetup() {
	setupSum = 0;
	for(var j=0; j<i; j++){
		if(document.getElementById('r_setup' + j)){
			if(document.getElementById('r_setup' + j).value!=""){
				setupSum = setupSum + parseFloat(document.getElementById('r_setup' + j).value);
			  }		  
		}
	}
	document.getElementById('setupSum').value= currencyVal + " " + parseFloat(setupSum).toFixed(2);
}

function addEQuantity() {
	eQuantitySum = 0;
	for(var j=0; j<exp_i; j++){
		if(document.getElementById('e_quantity' + j)){
			if(document.getElementById('e_quantity' + j).value!=""){
				  eQuantitySum = eQuantitySum + (parseInt)(document.getElementById('e_quantity' + j).value);
			  }		  
		}
	}
	document.getElementById('eQuantitySum').value=eQuantitySum;
}

function addESetup() {
	eSetupSum = 0;
	for(var j=0; j<exp_i; j++){
		if(document.getElementById('e_setup' + j)){
			if(document.getElementById('e_setup' + j).value!=""){
				eSetupSum = eSetupSum + (parseInt)(document.getElementById('e_setup' + j).value);
			  }		  
		}
	}
	document.getElementById('eSetupSum').value=currencyVal + " " + parseFloat(eSetupSum).toFixed(2);
}

function rowCostFun(id, from){
	rowCost="0.00";
	if(from==1){	//coming from quantity/sample
		id = id.substring(8);
	}
	else if(from==2){ //coming from unit price/cpi
		id= id.substring(5);
	}
	else if(from==3){ //coming from setup
		id= id.substring(7);
	}
	if(document.getElementById('r_survey' + id)){		
		var setup = document.getElementById('r_setup' + id).value;
		var unitPrice = document.getElementById('r_cpi' + id).value;
		if(setup!=""){
			document.getElementById('r_setup' + id).value = parseFloat(setup).toFixed(2);
		}
		else{
			setup=0.0;
		}
		if(unitPrice!=""){
			document.getElementById('r_cpi' + id).value = parseFloat(unitPrice).toFixed(2);
		}
		if(document.getElementById('r_service' + id).value ==2){
			//quantity
			var quantity = document.getElementById('r_sample' + id).value;
			
			if(quantity !="" && unitPrice !=""){
				rowCost = parseFloat((quantity * unitPrice)+ Number(setup)).toFixed(2);
			}
		}
		else {
			if(unitPrice!=""){
				rowCost = parseFloat(Number(unitPrice)+Number(setup)).toFixed(2);
			}
			else{
				rowCost = parseFloat(setup).toFixed(2);
			}
		}
	}
	document.getElementById('r_total' + id).value = currencyVal + " " + rowCost;
	totalCost();
}

function totalCost() {
	totalSum = 0.00;
	for(var j=0; j<i; j++){	
		if(document.getElementById('r_total' + j)){
		  if(document.getElementById('r_total' + j).value!=""){
			  rowTotal = document.getElementById('r_total' + j).value;
			  if(currencyVal!=""){
				  rowTotal = rowTotal.substring(3);
			  }
			  totalSum = totalSum + parseFloat(rowTotal);			  
		  }
		}
	}
	totalSum = parseFloat(totalSum).toFixed(2);
	document.getElementById('totalSum').value= currencyVal + " "+ totalSum;	
	
	//changed
	calculateProjectValue();
}

function eRowCostFun(id, from){
	eRowCost="0.00";
	if(from==1){	//coming from quantity
		id = id.substring(10);
	}
	else if(from==2){ //coming from unit price
		id= id.substring(11);
	}
	else if(from==3){ //coming from setup
		id= id.substring(7);
	}
	else if(from==4){//coming from project minimum
		id= id.substring(9);
	}
	if(document.getElementById('e_expenseOn' + id)){
		var quantity = document.getElementById('e_quantity' + id).value;
		var setup = document.getElementById('e_setup' + id).value;
		var unitPrice = document.getElementById('e_unitPrice' + id).value;
		var projMin = document.getElementById('e_projMin' + id).value;
		if(setup!=""){
			document.getElementById('e_setup' + id).value = parseFloat(setup).toFixed(2);
		}
		else{
			setup=0.0;
		}
		if(unitPrice!=""){
			document.getElementById('e_unitPrice' + id).value = parseFloat(unitPrice).toFixed(2);
		}
		if(projMin!=""){
			document.getElementById('e_projMin' + id).value = parseFloat(projMin).toFixed(2);
		}
		else{
			projMin=0.0;
		}
				
		if(quantity !="" && unitPrice !=""){
			eRowCost = parseFloat((quantity * unitPrice)+ Number(setup)).toFixed(2);
		}		
		if(parseFloat(projMin) > parseFloat(eRowCost)){
			eRowCost = parseFloat(projMin).toFixed(2);
		}
	}
	document.getElementById('e_total' + id).value = currencyVal + " " + eRowCost;
	totalECost();
}

function totalECost() {
	eTotalSum = 0.00;
	for(var j=0; j<exp_i; j++){	
		if(document.getElementById('e_total' + j)){
		  if(document.getElementById('e_total' + j).value!=""){
			  rowTotal = document.getElementById('e_total' + j).value;
			  if(currencyVal!=""){
				  rowTotal = rowTotal.substring(3);
			  }
			  eTotalSum = eTotalSum + parseFloat(rowTotal);			  
		  }
		}
	}
	eTotalSum = parseFloat(eTotalSum).toFixed(2);
	document.getElementById('eTotalSum').value= currencyVal + " "+ eTotalSum;
	currencyFormat(eTotalSum, document.getElementById('projectExpense'));
	
	if(document.getElementById('margin')){
		totalSum = document.getElementById('projectValue').value;
		if(totalSum !=""){
			totalSum = totalSum.replace(",","");			
		}else{
			totalSum=0.0;
		}
		margin = (parseFloat(totalSum) - parseFloat(eTotalSum))/parseFloat(totalSum) * 100;
		margin = parseFloat(margin).toFixed(2);
		document.getElementById('margin').value = margin;
	}
}

//change values based on service selected
function populateValues(service, id){
	id = id.substring(9);
	if(service!=2 && service !=0){	
		document.getElementById('r_sample'+id).value = "NA";		
		document.getElementById('r_sample'+id).disabled = true;
	}
	else{
		document.getElementById('r_sample'+id).value = "";
		document.getElementById('r_sample'+id).disabled = false;
	}
	addQuantity();
	rowCostFun(id,4);
}

//Function to get Project Details
function getProjectDetails(projectId){
	if(projectId==0){
		return;
	}
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeMgr; 
		var a=request.open("GET","getProjectDetails?projectId=" + projectId + "&action=getProjectDetails",true);
		request.send(true);	
 	}
}

function handleStateChangeMgr()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			if((request.responseText).length != 0)
			{		
					var projectArray = request.responseText.split(";");	
					document.getElementById('clientName').value = projectArray[0];
					document.getElementById('projectName').value = projectArray[1];
					document.getElementById('projectType').value = projectArray[2];
					document.getElementById('accountManager').value = projectArray[3];
					currencyVal=projectArray[4];
					document.getElementById('currPM').value = currencyVal;
					document.getElementById('currPV').value = currencyVal;
					document.getElementById('currPE').value = currencyVal;
					for(var j=0; j<i; j++){
						if(document.getElementById('currencyVal' + j)){
							document.getElementById('currencyVal' + j).value = currencyVal;
							document.getElementById('currUnit' + j).value = currencyVal;
							document.getElementById('currSetup' + j).value = currencyVal;
						}						
					}
					for(var j=0; j<exp_i; j++){
						if(document.getElementById('e_currency' + j)){
							document.getElementById('e_currency' + j).value = currencyVal;
							document.getElementById('currESetup' + j).value = currencyVal;
							document.getElementById('currEPM' + j).value = currencyVal;
							document.getElementById('currEUnit' + j).value = currencyVal;
						}						
					}
					for(var j=0; j<ded_i; j++){
						if(document.getElementById('dCurrUnit' + j)){
							document.getElementById('dCurrUnit' + j).value = currencyVal;							
						}						
					}
					document.getElementById('projectMinimum').value = projectArray[5];
					if(projectArray[2]=="Tracker"){				
						document.getElementById('waveMonth').disabled = false;
					}
					else{
						document.getElementById('waveMonth').value="NA";
						document.getElementById('waveMonth').disabled = true;
					}
			}
			request.abort();
			request = null;
}

function editInitialize(){
	//initialize i
	if(incomeRow==0){
		i=1;
	}
	else{
		i=incomeRow;
	}
	//initialize exp_i
	if(expenseRow==0){
		exp_i=1;
	}
	else{
		exp_i=expenseRow;
	}
	//initialize ded_i 20 May'14
	if(deductionRow==0){
		ded_i=1;
	}
	else{
		ded_i=deductionRow;
	}
	
	var projectArray = projectInfo.split(";");	
	document.getElementById('clientName').value = projectArray[0];
	document.getElementById('projectName').value = projectArray[1];
	document.getElementById('projectType').value = projectArray[2];
	document.getElementById('accountManager').value = projectArray[3];
	currencyVal=projectArray[4];
	document.getElementById('currPM').value = currencyVal;
	document.getElementById('currPV').value = currencyVal;
	document.getElementById('currPE').value = currencyVal;
	for(var j=0; j<i; j++){
		//check the service and disable/enable quantity
		service = document.getElementById('r_service'+j).value;
		if(service != 2 && service != 0){
			document.getElementById('r_sample'+j).value = "NA";
			document.getElementById('r_sample'+j).disabled = true;			
		}
		if(document.getElementById('currencyVal' + j)){
			document.getElementById('currencyVal' + j).value = currencyVal;
			document.getElementById('currUnit' + j).value = currencyVal;
			document.getElementById('currSetup' + j).value = currencyVal;
		}						
	}
	for(var j=0; j<exp_i; j++){
		if(document.getElementById('e_currency' + j)){
			document.getElementById('e_currency' + j).value = currencyVal;
			document.getElementById('currESetup' + j).value = currencyVal;
			document.getElementById('currEPM' + j).value = currencyVal;
			document.getElementById('currEUnit' + j).value = currencyVal;
		}						
	}
	//Changes for deduction
	for(var j=0; j<ded_i; j++){
		if(document.getElementById('dCurrUnit' + j)){
			document.getElementById('dCurrUnit' + j).value = currencyVal;							
		}						
	}
	document.getElementById('projectMinimum').value = projectArray[5];
	
	if(projectArray[2]=="Tracker"){				
		document.getElementById('waveMonth').disabled = false;
	}
	else{
		document.getElementById('waveMonth').value="NA";
		document.getElementById('waveMonth').disabled = true;
	}
	
	addQuantity(); addSetup();
	addEQuantity(); addESetup();
	addDedQuantity();
	editRowCost(); totalCost(); totalECost(); totalDCost();
	getRowValues();
}

function editRowCost(){
	rowCost="0.00";
	for(var j=0; j<i; j++){
		if(document.getElementById('r_survey' + j)){		
			var setup = document.getElementById('r_setup' + j).value;
			var unitPrice = document.getElementById('r_cpi' + j).value;
			if(setup!=""){
				document.getElementById('r_setup' + j).value = parseFloat(setup).toFixed(2);
			}
			else{
				setup=0.0;
			}
			if(unitPrice!=""){
				document.getElementById('r_cpi' + j).value = parseFloat(unitPrice).toFixed(2);
			}
			if(document.getElementById('r_service' + j).value ==2){
				//quantity
				var quantity = document.getElementById('r_sample' + j).value;
				
				if(quantity !="" && unitPrice !=""){
					rowCost = parseFloat((quantity * unitPrice)+ Number(setup)).toFixed(2);
				}
			}
			else {
				if(unitPrice!=""){
					rowCost = parseFloat(Number(unitPrice)+Number(setup)).toFixed(2);
				}
				else{
					rowCost = parseFloat(setup).toFixed(2);
				}
			}
			document.getElementById('r_total' + j).value = currencyVal + " " + rowCost;
		}
	}	
	
	eRowCost = "0.00";
	for(var j=0; j<exp_i; j++){
		if(document.getElementById('e_expenseOn' + j)){
			var quantity = document.getElementById('e_quantity' + j).value;
			var setup = document.getElementById('e_setup' + j).value;
			var unitPrice = document.getElementById('e_unitPrice' + j).value;
			var projMin = document.getElementById('e_projMin' + j).value;
			if(setup!=""){
				document.getElementById('e_setup' + j).value = parseFloat(setup).toFixed(2);
			}
			else{
				setup=0.0;
			}
			if(unitPrice!=""){
				document.getElementById('e_unitPrice' + j).value = parseFloat(unitPrice).toFixed(2);
			}
			if(projMin!=""){
				document.getElementById('e_projMin' + j).value = parseFloat(projMin).toFixed(2);
			}
			else{
				projMin=0.0;
			}
					
			if(quantity !="" && unitPrice !=""){
				eRowCost = parseFloat((quantity * unitPrice)+ Number(setup)).toFixed(2);
			}		
			if(parseFloat(projMin) > parseFloat(eRowCost)){
				eRowCost = parseFloat(projMin).toFixed(2);
			}
		}
		document.getElementById('e_total' + j).value = currencyVal + " " + eRowCost;
	}
	
	//Changes for deduction - Maneet
	dRowCost="0.00";
	for(var j=0; j<ded_i; j++){
		if(document.getElementById('d_survey' + j)){
			var quantity = document.getElementById('d_sample' + j).value;
			var unitPrice = document.getElementById('d_cpi' + j).value;
			if(quantity==""){
				quantity=0;
			}
			
			if(unitPrice!=""){
				document.getElementById('d_cpi' + j).value = parseFloat(unitPrice).toFixed(2);
			}
			else{
				unitPrice=0.0;
			}		
			dRowCost =	parseFloat(quantity * unitPrice).toFixed(2);		
		}
		document.getElementById('d_total' + j).value = currencyVal + " " + dRowCost;
	}
	
}

var incValues = new Array();
var expValues = new Array();
var dedValues = new Array();
function getRowValues(){		
	for(var j=0;j<incomeRow;j++)
    {
        incValues[j]=new Array();
        incValues[j][0]=document.getElementById('r_survey'+j).value;
        incValues[j][1]=document.getElementById('r_country'+j).value;			
		incValues[j][2]=document.getElementById('r_service'+j).value;
		incValues[j][3]=document.getElementById('r_audience'+j).value;
		incValues[j][4]=document.getElementById('r_desc'+j).value;
		incValues[j][5]=document.getElementById('r_sample'+j).value;
		incValues[j][6]=document.getElementById('r_cpi'+j).value;
		incValues[j][7]=document.getElementById('r_setup'+j).value;					
    }
	
	for(var j=0;j<expenseRow;j++)
    {
		expValues[j]=new Array();
		expValues[j][0]=document.getElementById('e_expenseOn'+j).value;
		expValues[j][1]=document.getElementById('e_expenseType'+j).value;			
		expValues[j][2]=document.getElementById('e_vendor'+j).value;
		expValues[j][3]=document.getElementById('e_quantity'+j).value;
		expValues[j][4]=document.getElementById('e_unitPrice'+j).value;
		expValues[j][5]=document.getElementById('e_projMin'+j).value;
		expValues[j][6]=document.getElementById('e_setup'+j).value;
		expValues[j][7]=document.getElementById('e_survey'+j).value;
		expValues[j][8]=document.getElementById('e_country'+j).value;		
    }
	
	//Changes for deduction - Maneet 20 May'14
	for(var j=0; j<deductionRow; j++){
		dedValues[j] = new Array();
		dedValues[j][0] = document.getElementById('d_survey' + j).value;
		dedValues[j][1] = document.getElementById('d_country' + j).value;
		dedValues[j][2] = document.getElementById('d_service' + j).value;
		dedValues[j][3] = document.getElementById('d_desc' + j).value;
		dedValues[j][4] = document.getElementById('d_reason' + j).value;
		dedValues[j][5] = document.getElementById('d_sample' + j).value;
		dedValues[j][6] = document.getElementById('d_cpi' + j).value;
	}
}

var incomeIdList, expenseIdList, deductionIdList;
function checkRowUpdate(){
	incomeIdList="";  
	for(var k=0;k<incomeRow;k++)
      {
            if(incValues[k][0]!=document.getElementById('r_survey'+k).value || 
            		incValues[k][1]!=document.getElementById('r_country'+k).value || 
            		incValues[k][2]!=document.getElementById('r_service'+k).value || 
            		incValues[k][3]!=document.getElementById('r_audience'+k).value || 
            		incValues[k][4]!=document.getElementById('r_desc'+k).value  || 
            		incValues[k][5]!=document.getElementById('r_sample'+k).value ||
            		incValues[k][6]!=document.getElementById('r_cpi'+k).value  || 
            		incValues[k][7]!=document.getElementById('r_setup'+k).value)
            {  
            	incomeIdList+=k+",";
            }         
      }
	  document.getElementById("incomeIdList").value = incomeIdList;
	  //for expense
	  expenseIdList="";  
	  for(var k=0;k<expenseRow;k++)
      {
            if(expValues[k][0]!=document.getElementById('e_expenseOn'+k).value || 
            		expValues[k][1]!=document.getElementById('e_expenseType'+k).value || 
            		expValues[k][2]!=document.getElementById('e_vendor'+k).value || 
            		expValues[k][3]!=document.getElementById('e_quantity'+k).value || 
            		expValues[k][4]!=document.getElementById('e_unitPrice'+k).value  || 
            		expValues[k][5]!=document.getElementById('e_projMin'+k).value ||
            		expValues[k][6]!=document.getElementById('e_setup'+k).value ||
            		expValues[k][7]!=document.getElementById('e_survey'+k).value ||
    				expValues[k][8]!=document.getElementById('e_country'+k).value)
            {  
            	expenseIdList+=k+",";
            }         
      }
	  document.getElementById("expenseIdList").value = expenseIdList;
	  
	  deductionIdList = "";
	//Changes for deduction - Maneet 20 May'14
		for(var k=0; k<deductionRow; k++){
			if(dedValues[k][0] != document.getElementById('d_survey' + k).value ||
			dedValues[k][1] != document.getElementById('d_country' + k).value ||
			dedValues[k][2] != document.getElementById('d_service' + k).value ||
			dedValues[k][3] != document.getElementById('d_desc' + k).value ||
			dedValues[k][4] != document.getElementById('d_reason' + k).value ||
			dedValues[k][5] != document.getElementById('d_sample' + k).value ||
			dedValues[k][6] != document.getElementById('d_cpi' + k).value)
			{
				deductionIdList+=k+",";
			}
		}
		document.getElementById("deductionIdList").value = deductionIdList;
}

function projectSummaryValidation(from){
	pass=projectSummaryValidationStatus(from);
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
		//Enable project value and project expense fields
		document.getElementById('projectValue').disabled = false;
		document.getElementById('projectExpense').disabled = false;
		if(document.getElementById('projectType').value!="Tracker"){
			document.getElementById('waveMonth').disabled = false;
			document.getElementById('waveMonth').value = "";
		}
		if(from==1){
			document.getElementById('margin').disabled = false;
		}
	}
	return pass;
}


function projectSummaryValidationStatus(from) {
	pass=true;
	//Check all the mandatory fields
	if((from==0 && document.getElementById('projectId').value=="0") || 
		document.getElementById('closingMonth').value=="" ||
		(document.getElementById('projectType').value=="Tracker" && document.getElementById('waveMonth').value=="")){
		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	
	//One income row is mandatory
	if(document.getElementById('r_survey0').value=="" || document.getElementById('r_country0').value=="0" ||
		document.getElementById('r_service0').value=="" || document.getElementById('r_audience0').value=="0" ||
		document.getElementById('r_desc0').value=="" || document.getElementById('r_sample0').value=="0" ||
		document.getElementById('r_cpi0').value=="" || document.getElementById('r_setup0').value=="0"){
		
		document.getElementById('validation').innerHTML="Please enter atleast one income row.";
		return false;
	}
	
	//One expense row is mandatory
	if(document.getElementById('e_expenseOn0').value=="0" || document.getElementById('e_expenseType0').value=="0" ||
		document.getElementById('e_vendor0').value=="-1" || document.getElementById('e_quantity0').value=="" ||
		document.getElementById('e_unitPrice0').value=="" || document.getElementById('e_projMin0').value=="" ||
		document.getElementById('e_setup0').value=="" || document.getElementById('e_survey0').value=="" ||
		document.getElementById('e_country0').value=="0"){
		
		document.getElementById('validation').innerHTML="Please enter atleast one expense row.";
		return false;
	}
	
	/* For income rows
	 * if one the fields in the row has values, then the complete row should have values
	 * Quantity to setup fee should be positive integers
	 */
	for(var j=0, count=0; j<i; j++){
		if(document.getElementById('r_survey' + j)){
			count=count+1;
			//if one of the value in the row exists then the whole row must have values
			if(document.getElementById('r_survey' + j).value!="" || document.getElementById('r_country' + j).value!="0" ||
				document.getElementById('r_service' + j).value!="" || document.getElementById('r_audience' + j).value!="0" ||
				document.getElementById('r_desc' + j).value!="" || document.getElementById('r_sample' + j).value!="0" ||
				document.getElementById('r_cpi' + j).value!="" || document.getElementById('r_setup' + j).value!="0"){
					
				if(document.getElementById('r_survey' + j).value=="" || document.getElementById('r_country' + j).value=="0" ||
					document.getElementById('r_service' + j).value=="" || document.getElementById('r_audience' + j).value=="0" ||
					document.getElementById('r_desc' + j).value=="" || document.getElementById('r_sample' + j).value=="0" ||
					document.getElementById('r_cpi' + j).value=="" || document.getElementById('r_setup' + j).value=="0"){
					
					document.getElementById('validation').innerHTML="Please enter all the values in income row "
						+ count;
					return false;
				}
			}
			
			//Check if values are positive integers
			if(document.getElementById('r_sample' + j).value!=""){
				if(document.getElementById('r_service'+j).value==2){
					var sample_size = document.getElementById('r_sample' + j).value;
					if(!(sample_size>=0)){
						document.getElementById('validation').innerHTML="The quantity in income can only be in numbers";
						return false;
					}
				}				
			}			
			if(document.getElementById('r_cpi' + j).value!=""){
				var sample_size = document.getElementById('r_cpi' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The unit price in income can only be in numbers";
					return false;
				}
			}
			if(document.getElementById('r_setup' + j).value!=""){
				var sample_size = document.getElementById('r_setup' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The setup fee in income can only be in numbers";
					return false;
				}
			}
		}
	}
	
	/* For expense rows
	 * if one the fields in the row has values, then the complete row should have values
	 * Quantity to setup fee should be positive integers
	 */
	for(var j=0, exp_count=0; j<exp_i; j++){
		if(document.getElementById('e_expenseOn' + j)){
			exp_count=exp_count+1;
			//if one of the value in the row exists then the whole row must have values
			if(document.getElementById('e_expenseOn' + j).value!="0" || document.getElementById('e_expenseType' + j).value!="0" ||
				document.getElementById('e_vendor' + j).value!="-1" || document.getElementById('e_quantity' + j).value!="" ||
				document.getElementById('e_unitPrice' + j).value!="" || document.getElementById('e_projMin' + j).value!="" ||
				document.getElementById('e_setup' + j).value!="" || document.getElementById('e_survey' + j).value!="" ||
				document.getElementById('e_country' + j).value!="0"){
					
				if(document.getElementById('e_expenseOn' + j).value=="0" || document.getElementById('e_expenseType' + j).value=="0" ||
					document.getElementById('e_vendor' + j).value=="-1" || document.getElementById('e_quantity' + j).value=="" ||
					document.getElementById('e_unitPrice' + j).value=="" || document.getElementById('e_projMin' + j).value=="" ||
					document.getElementById('e_setup' + j).value=="" || document.getElementById('e_survey' + j).value=="" ||
					document.getElementById('e_country' + j).value=="0"){
						
					document.getElementById('validation').innerHTML="Please enter all the values in expense row "
						+ exp_count;
					return false;
				}
			}
			
			//Check for positive integers
			if(document.getElementById('e_quantity' + j).value!=""){
				var sample_size = document.getElementById('e_quantity' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The quantity in expense can only be in numbers";
					return false;
				}
			}
			if(document.getElementById('e_unitPrice' + j).value!=""){
				var sample_size = document.getElementById('e_unitPrice' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The unit price in expense can only be in numbers";
					return false;
				}
			}
			if(document.getElementById('e_projMin' + j).value!=""){
				var sample_size = document.getElementById('e_projMin' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The project minimum in expense can only be " +
							"in numbers";
					return false;
				}
			}
			if(document.getElementById('e_setup' + j).value!=""){
				var sample_size = document.getElementById('e_setup' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The setup fee in expense can only be in numbers";
					return false;
				}
			}
		}
	}
	
	return true;
}

/*
 * Changes for Deduction details - Added by Maneet 20 May'14
 */
function addDedQuantity() {
	dquantitySum = 0;
	for(var j=0; j<ded_i; j++){
		if(document.getElementById('d_sample' + j)){
			if(document.getElementById('d_sample' + j).value!=""){
				dquantitySum = dquantitySum + (parseInt)(document.getElementById('d_sample' + j).value);
			  }		  
		}
	}
	document.getElementById('dedQuantitySum').value=dquantitySum;
}

function dRowCostFun(id, from){
	dRowCost="0.00";
	if(from==1){	//coming from quantity/sample
		id = id.substring(8);
	}
	else if(from==2){ //coming from unit price/cpi
		id= id.substring(5);
	}
	if(document.getElementById('d_survey' + id)){
		var quantity = document.getElementById('d_sample' + id).value;
		var unitPrice = document.getElementById('d_cpi' + id).value;
		if(quantity==""){
			quantity=0;
		}		
		if(unitPrice!=""){
			document.getElementById('d_cpi' + id).value = parseFloat(unitPrice).toFixed(2);
		}
		else{
			unitPrice=0.0;
		}		
		dRowCost =	parseFloat(quantity * unitPrice).toFixed(2);		
	}
	document.getElementById('d_total' + id).value = currencyVal + " " + dRowCost;
	totalDCost();
}

function totalDCost() {
	dTotalSum = 0.00;
	for(var j=0; j<ded_i; j++){	
		if(document.getElementById('d_total' + j)){
		  if(document.getElementById('d_total' + j).value!=""){
			  rowTotal = document.getElementById('d_total' + j).value;
			  if(currencyVal!=""){
				  rowTotal = rowTotal.substring(3);
			  }
			  dTotalSum = dTotalSum + parseFloat(rowTotal);			  
		  }
		}
	}
	dTotalSum = parseFloat(dTotalSum).toFixed(2);
	document.getElementById('dedTotalSum').value= currencyVal + " "+ dTotalSum;
	calculateProjectValue();
}

function calculateProjectValue(){
	var projectMinimum = document.getElementById('projectMinimum').value;
	if(projectMinimum==""){
		projectMinimum=0.00;
	}
	else{
		projectMinimum = projectMinimum.replace(",", "");
		projectMinimum = parseFloat(projectMinimum).toFixed(2);
	}
	totalSum = document.getElementById('totalSum').value;
	if(totalSum==""){
		totalSum=0.00;
	}
	else if(currencyVal!=""){
		totalSum = totalSum.substring(3);
		totalSum = parseFloat(totalSum).toFixed(2);
	}
	
	dTotalSum = document.getElementById('dedTotalSum').value;
	if(dTotalSum==""){
		dTotalSum=0.00;
	}
	else if(currencyVal!=""){
		dTotalSum = dTotalSum.substring(3);
		dTotalSum = parseFloat(dTotalSum).toFixed(2);
	}
	projectValue = parseFloat(totalSum-dTotalSum).toFixed(2);
	
	if(parseFloat(projectValue)<parseFloat(projectMinimum)){
		projectValue = projectMinimum;
	}
	currencyFormat(projectValue, document.getElementById('projectValue'));
	if(document.getElementById('margin')){
		eTotalSum = document.getElementById('projectExpense').value;
		if(eTotalSum !=""){
			eTotalSum = eTotalSum.replace(",","");			
		}else{
			eTotalSum=0.0;
		}
		margin = (parseFloat(projectValue) - parseFloat(eTotalSum))/parseFloat(projectValue) * 100;
		margin = parseFloat(margin).toFixed(2);
		document.getElementById('margin').value = margin;
	}
}
