function validateEmail(email){
	var email_value = document.getElementById(email).value;
	var atpos=email_value.indexOf("@");
	var dotpos=email_value.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_value.length)
	{
	  return false;
	}
	return true;
}


/**** calculate due date ***********/
function AddDaysInDate(invaoiceDate,noOfDays)
{
	 var payment_term=parseInt(noOfDays);
		var endDate=new Date(invaoiceDate);
		endDate.setDate(endDate.getDate()+payment_term);
		function pad(s) { return (s < 10) ? '0' + s : s; }
		var dd=pad(endDate.getDate());
		var mm=pad(endDate.getMonth()+1);
		var yy=endDate.getFullYear();
		var dueDate=yy+'-'+mm+'-'+dd;
		return dueDate;
		
}

/***** convert no into currency format *********/
function currencyFormat(n,$field)
{
	var num=n.replace(/,/g,"");
	var currency=parseFloat(num).toFixed(2).replace(/./g, function(c, i, a) {
	    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
	});
	document.getElementById($field.name).value=currency;
	//return currency;
}