var preName = '';

function validateUsername(username){
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeUsername; 
		var a=request.open("GET","validateUsername?userName=" + username + "&action=validateUsername",true);
		request.send(true);
 	}
}

function handleStateChangeUsername()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgUsername').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
					document.getElementById('username').value = preName;				
			}
			request.abort();
			request = null;
}

function userValidationStatus(){
	var accessRoles = document.getElementsByName('accessRole');
	var accessRole="";
	for(var i = 0; i < accessRoles.length; i++){
	    if(accessRoles[i].checked){
	    	accessRole = accessRoles[i].value;
	    }
	}
	if(document.getElementById('username').value == "" || document.getElementById('password').value == "" || 
			document.getElementById('employeeName').value == "" || document.getElementById('role').value == "0" ||
			accessRole =="" || document.getElementById('emailId').value==""){
		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	//Check if email id is in valid format
	if(document.getElementById('emailId').value != ""){
		if(!validateEmail('emailId')){
			document.getElementById('validation').innerHTML="The email id format is not valid.";
			return false;
		}
	}
	return true;
}

function userValidation(){
	var pass = true;
	pass=userValidationStatus();
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
}
