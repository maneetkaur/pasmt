var preName='', preCode='', preUrl='';
var filePass=true;
function activateCsat(csatOption) {
	if(csatOption=="1"){
		document.getElementById("csatOptions").disabled=false;
	}
	else if(csatOption=="0"){
		document.getElementById("csatOptions").disabled=true;
	}
}

function validateEmail(email){
	var email_value = document.getElementById(email).value;
	var atpos=email_value.indexOf("@");
	var dotpos=email_value.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_value.length)
	{
	  return false;
	}
	return true;
}

function clientValidationStatus(from) {
	pass=true;
	//Check all the mandatory fields
	if(document.getElementById('client_name').value=="" || document.getElementById('client_code').value=="" ||
		document.getElementById('region').value=="0" || document.getElementById('client_type').value=="0" || 
		document.getElementById('acct_manager_name').value=="0" || document.getElementById('primaryContact.name').value=="" || 
		document.getElementById('primaryContact.jobTitle').value=="" || document.getElementById('primaryContact.email').value=="" 
		|| document.getElementById('primaryContact.phoneNo').value=="" || document.getElementById('address').value=="" || 
		document.getElementById('city').value=="" || document.getElementById('state').value=="" || 
		document.getElementById('postal_code').value=="" || document.getElementById('country_name').value=="0" || 
		document.getElementById('websiteUrl').value=="" || document.getElementById('bill_name').value=="" || 
		document.getElementById('bill_job_title').value=="" || document.getElementById('bill_email').value=="" || 
		document.getElementById('bill_phone_no').value=="" || document.getElementById('bill_address').value=="" || 
		document.getElementById('bill_city').value=="" || document.getElementById('bill_state').value=="" || 
		document.getElementById('bill_postal_code').value=="" || document.getElementById('bill_country').value=="0" || 
		document.getElementById('bill_acc_payable_email').value==""){		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	
	//Check that the extension number should be a positive number
	if(document.getElementById('primaryContact.extno').value!=""){
		var extno = document.getElementById('primaryContact.extno').value;
		if(!(extno>=0)){
			document.getElementById('validation').innerHTML="The extension number can only be in numbers";
			return false;
		}
	}
	
	//Check if email ids are in valid format
	if(document.getElementById('primaryContact.email').value != ""){
		if(!validateEmail('primaryContact.email')){
			document.getElementById('validation').innerHTML="The email id format of the primary contact is not valid.";
			return false;
		}
	}
	if(document.getElementById('rfqEmail').value != ""){
		if(!validateEmail('rfqEmail')){
			document.getElementById('validation').innerHTML="The email id format of the RFQ email is not valid.";
			return false;
		}
	}
	
	if(document.getElementById('bill_email').value != ""){
		if(!validateEmail('bill_email')){
			document.getElementById('validation').innerHTML="The email id format in billing detail is not valid.";
			return false;
		}
	}
	
	//Check if bill extension number is a positive number or not
	if(document.getElementById('bill_ext_no').value!=""){
		var extno = document.getElementById('bill_ext_no').value;
		if(!(extno>=0)){
			document.getElementById('validation').innerHTML="The extension number in billing detail can only be in numbers";
			return false;
		}
	}
	
	if(document.getElementById('bill_acc_payable_email').value != ""){
		if(!validateEmail('bill_acc_payable_email')){
			document.getElementById('validation').innerHTML="The email id of account payable email is not valid.";
			return false;
		}
	}
	
	/*If none other validation has failed, loop and check for additional contacts
	 * For edit page, the names of existing contacts cannot be empty
	 */
	
	if(from==1){
		for(var k=0; k<rowNumber; k++){
			if(document.getElementById('add_name'+k).value==""){
				document.getElementById('validation').innerHTML="The names of existing additional contacts cannot be empty.";
				return false;
			}
		}
	}
	
	for(var j=0; j<i; j++){
	  if(document.getElementById('add_name'+j)!=null){
		if(document.getElementById('add_name'+j).value!=""){
			if(document.getElementById('add_job'+j).value=="" || 
					document.getElementById('add_email'+j).value==""){
				document.getElementById('validation').innerHTML="Please enter the job title and email of the additional contacts.";
				return false;
			}
			else if(document.getElementById('add_ext'+j).value!=""){
				var extno = document.getElementById('add_ext'+j).value;
				if(!(extno>=0)){
					document.getElementById('validation').innerHTML="The extension number can only be in numbers";
					return false;
				}
			}
			if(document.getElementById('add_email'+j).value != ""){
				if(!validateEmail('add_email'+j)){
					document.getElementById('validation').innerHTML="The email id format of the additional contact" + 
					" is not valid.";
					return false;
				}
			}
		 }
	  }
	}
	
	return true;
}

function clientValidation(from){
	pass=clientValidationStatus(from);
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
}

function tAndCValidation(from){
	pass=clientValidationStatus(from);
	if(pass==false){
		document.getElementById('validation_tandc').innerHTML="Please check the data in the Client Details form";
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation_tandc').innerHTML="";
	}
	return pass;
}

function signedValidation(fileName, id){
	var extIndex=fileName.lastIndexOf('\\');
	fileName = fileName.substring(extIndex+1);
	
	extIndex=fileName.lastIndexOf(".");
	var extension=fileName.substring(extIndex+1);
	id = id.substring(8);
	
	if(extension.length>4){
		filePass=false;
		document.getElementById('validation_signed').innerHTML = "File extension cannot be longer than 4 characters";
		document.getElementById('signedIO' + id).value="";
		return;
	}
	if(extension!="pdf" && extension!="jpeg" && extension!="jpg" && extension != "png"){
		filePass=false;
		document.getElementById('validation_signed').innerHTML = "You can only upload a PDF file or a JPEG/PNG image";		
		document.getElementById('signedIO' + id).value="";
		return;
	}
	filePass=true;
	document.getElementById('validation_signed').innerHTML = "";
}
//
function guidelineValidation(fileName, id){
	var extIndex=fileName.lastIndexOf('\\');
	fileName = fileName.substring(extIndex+1);
	
	extIndex=fileName.lastIndexOf(".");
	var extension=fileName.substring(extIndex+1);
	id = id.substring(9);
	
	if(extension.length>4){
		filePass=false;
		document.getElementById('validation_guideline').innerHTML = "File extension cannot be longer than 4 characters";
		document.getElementById('guideline' + id).value="";
		return;
	}
	if(extension!="pdf" && extension!="doc" && extension!="docx"){
		filePass=false;
		document.getElementById('validation_guideline').innerHTML = "You can only upload a PDF or a MS-Word document";		
		document.getElementById('guideline' + id).value="";
		return;
	}
	filePass=true;
	document.getElementById('validation_guideline').innerHTML = "";
}

function validateClientName(name){
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeName; 
		var a=request.open("GET","validateName?clientName=" + name + "&action=validateName",true);
		request.send(true);	
 	}
}

function handleStateChangeName()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgClientName').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
					document.getElementById('client_name').value = preName;				
			}
			request.abort();
			request = null;
}

function validateClientCode(code){
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeCode; 
		var a=request.open("GET","validateCode?clientCode=" + code + "&action=validateCode",true);
		request.send(true);
 	}
}

function handleStateChangeCode()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgClientCode').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
					document.getElementById('client_code').value = preCode;				
			}
			request.abort();
			request = null;
}

function validateWebUrl(webUrl){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeWeb; 
		var a=request.open("GET","validateWebUrl?webUrl=" + webUrl + "&action=validateWebUrl",true);
		request.send(true);	
 	}
}

function handleStateChangeWeb()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgWebUrl').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{									
					document.getElementById('websiteUrl').value = preUrl;				
			}
			request.abort();
			request = null;
}