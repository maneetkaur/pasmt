var sampleSum, incidenceSum, loiSum, feasibilitySum, totalSum, rowCost=0, currencyVal="";

function populateValues(service, id){
	id = id.substring(9);
	if(service!=2 && service !=0){	
		document.getElementById('r_sample'+id).value = "NA";		
		document.getElementById('r_sample'+id).disabled = true;
		document.getElementById('r_feas'+id).value = "NA";		
		document.getElementById('r_feas'+id).disabled = true;
	}
	else{
		document.getElementById('r_sample'+id).value = "";
		document.getElementById('r_sample'+id).disabled = false;
		document.getElementById('r_feas'+id).value = "";
		document.getElementById('r_feas'+id).disabled = false;
	}
	addSample(); addFeasibility();
	rowCostFun(id,3);
}

function showCurrency(currency){
	if(currency=="--Select--"){
		currency="";
	}
	currencyVal = currency;
	for(var j=0; j<i; j++){
		if(document.getElementById('currencyVal' + j)){
			document.getElementById('currencyVal' + j).value=currency;
		}
	}
	editRowCost();
	totalCost();
}

function addSample() {
	sampleSum = 0;
	for(var j=0; j<i; j++){
		if(document.getElementById('r_sample' + j)){
		  if(document.getElementById('r_service' + j).value == 2){
			  if(document.getElementById('r_sample' + j).value!=""){
					sampleSum = sampleSum + (parseInt)(document.getElementById('r_sample' + j).value);
			  }
		  }
		  else{
			if(document.getElementById('r_service' + j).value != 0){
				if(document.getElementById('r_sample' + j).value==""){
					document.getElementById('r_sample' + j).value="NA";
				}
			}		
		  }
		}
	}
	document.getElementById('sampleSum').value=sampleSum;
}
/*function addIncidence() {
incidenceSum = 0;
for(var j=0; j<i; j++){		
	if(document.getElementById('r_incidence' + j)){
	  if(document.getElementById('r_incidence' + j).value!="" && document.getElementById('r_incidence' + j).value!="-1"){  
		incidenceSum = incidenceSum + (parseInt)(document.getElementById('r_incidence' + j).value);
	  }
	}
}
document.getElementById('incidenceSum').value=incidenceSum;
}

function addLoi() {
loiSum = 0;
for(var j=0; j<i; j++){		
	if(document.getElementById('r_loi' + j)){
		if(document.getElementById('r_loi' + j).value!=""){
			loiSum = loiSum + (parseInt)(document.getElementById('r_loi' + j).value);
		}
	}
}
document.getElementById('loiSum').value=loiSum;
}*/

function addFeasibility() {
	feasibilitySum = 0;
	for(var j=0; j<i; j++){		
		if(document.getElementById('r_feas' + j)){
		  if(document.getElementById('r_service' + j).value == 2){
			  if(document.getElementById('r_feas' + j).value!=""){
				feasibilitySum = feasibilitySum + (parseInt)(document.getElementById('r_feas' + j).value);
			  }
		  }
		  else{
			if(document.getElementById('r_service' + j).value != 0){
				if(document.getElementById('r_feas' + j).value==""){
					document.getElementById('r_feas' + j).value="NA";
				}
			}		
		  }
		}
	}
	document.getElementById('feasibilitySum').value=feasibilitySum;
}

function rowCostFun(id, from){
	rowCost="0.00";
	if(from==1){
		id = id.substring(5);
	}
	else if(from==0){
		id= id.substring(6);
	}
	if(document.getElementById('r_feas' + id)){
		var cpi = document.getElementById('r_cpi' + id).value;
		if(cpi!=""){
			document.getElementById('r_cpi' + id).value = parseFloat(cpi).toFixed(2);
		}
		
		if(document.getElementById('r_service' + id).value ==2){
			var sample = document.getElementById('r_feas' + id).value;		
			if(sample !="" && cpi !=""){
				rowCost = parseFloat(sample * cpi).toFixed(2);
			}
		}
		else{
			if(cpi!=""){
				rowCost = parseFloat(cpi).toFixed(2);
			}			
		}
		
	}
	document.getElementById('r_total' + id).value = currencyVal + " " + rowCost;
	totalCost();
}

function editRowCost(){
	rowCost="0.00";
	for(var j=0; j<i; j++){
		if(document.getElementById('r_feas' + j)){
			var cpi = document.getElementById('r_cpi' + j).value;
			if(cpi!=""){
				document.getElementById('r_cpi' + j).value = parseFloat(cpi).toFixed(2);
			}
			if(document.getElementById('r_service' + j).value ==2){
				var sample = document.getElementById('r_feas' + j).value;				
				if(sample !="" && cpi !=""){
					rowCost = parseFloat(sample * cpi).toFixed(2);
				}
			}
			else{
				if(cpi!=""){
					rowCost = parseFloat(cpi).toFixed(2);
				}			
			}
			document.getElementById('r_total' + j).value = currencyVal + " " + rowCost;
		}
	}
}

function totalCost() {
	totalSum = 0.00;
	for(var j=0; j<i; j++){	
		if(document.getElementById('r_total' + j)){
		  if(document.getElementById('r_total' + j).value!=""){
			  rowTotal = document.getElementById('r_total' + j).value;
			  if(currencyVal!=""){
				  rowTotal = rowTotal.substring(3);
			  }
			  totalSum = totalSum + parseFloat(rowTotal);			  
		  }
		}
	}
	totalSum = parseFloat(totalSum).toFixed(2);
	document.getElementById('totalSum').value= currencyVal + " "+ totalSum;
}

function bidValidation(){
	pass=bidValidationStatus();
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
}

function bidValidationStatus() {
	pass=true;
	//Check all the mandatory fields
	if(document.getElementById('bidClient').value=="0" || document.getElementById('bidName').value=="" ||
		document.getElementById('projectType').value=="0" || document.getElementById('productType').value=="0" || 
		document.getElementById('billingCurrency').value=="0" || document.getElementById('biddingStatus').value=="0" || 
		document.getElementById('accountManager').value=="0" || document.getElementById('bidDate').value=="" ||
		document.getElementById('projectSpecs').value==""){
		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	
	//One requirement row is mandatory
	if(document.getElementById('r_country0').value=="0" || document.getElementById('r_service0').value=="0" ||
			document.getElementById('r_audience0').value=="0" || document.getElementById('r_desc0').value=="" || 
			document.getElementById('r_sample0').value=="" || document.getElementById('r_incidence0').value=="-1" || 
			document.getElementById('r_question0').value=="" || document.getElementById('r_loi0').value=="" ||
			document.getElementById('r_feas0').value=="" || document.getElementById('r_cpi0').value==""){
		
			document.getElementById('validation').innerHTML="Please enter atleast one requirement row";
			return false;
		}
	
	/*
	 * if one the fields in the row has values, then the complete row should have values
	 * Sample size to Quoted CPI should be positive integers
	 */	
	for(var j=0, count=0; j<i; j++){
		//if the first element of the row exists, the whole row exists
		if(document.getElementById('r_country' + j)){
			count=count+1;
			//if one of the value in the row exists then the whole row must have values
			if(document.getElementById('r_country' + j).value!="0" || document.getElementById('r_service'+j).value!="0" ||
				document.getElementById('r_audience'+j).value!="0" || document.getElementById('r_desc'+j).value!="" || 
				document.getElementById('r_sample'+j).value!="" || document.getElementById('r_incidence'+j).value!="-1" || 
				document.getElementById('r_question'+j).value!="" || document.getElementById('r_loi'+j).value!="" ||
				document.getElementById('r_feas'+j).value!="" || document.getElementById('r_cpi'+j).value!=""){
				
				if(document.getElementById('r_country'+j).value=="0" || document.getElementById('r_service'+j).value=="0" ||
					document.getElementById('r_audience'+j).value=="0" || document.getElementById('r_desc'+j).value=="" || 
					document.getElementById('r_sample'+j).value=="" || document.getElementById('r_incidence'+j).value=="-1" || 
					document.getElementById('r_question'+j).value=="" || document.getElementById('r_loi'+j).value=="" ||
					document.getElementById('r_feas'+j).value=="" || document.getElementById('r_cpi'+j).value==""){
						document.getElementById('validation').innerHTML="Please enter all the values in requirement row "
							+ count;
						return false;
					}
				}
		}
		
		if(document.getElementById('r_sample' + j)){
			if(document.getElementById('r_sample' + j).value!=""){
				if(document.getElementById('r_service'+j).value==2){
					var sample_size = document.getElementById('r_sample' + j).value;
					if(!(sample_size>=0)){
						document.getElementById('validation').innerHTML="The sample size can only be in numbers";
						return false;
					}
				}				
			}
		}
		if(document.getElementById('r_question' + j)){
			if(document.getElementById('r_question' + j).value!=""){
				var sample_size = document.getElementById('r_question' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The no. of questions can only be in numbers";
					return false;
				}
			}
		}
		if(document.getElementById('r_loi' + j)){
			if(document.getElementById('r_loi' + j).value!=""){
				var sample_size = document.getElementById('r_loi' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The LOI can only be in numbers";
					return false;
				}
			}
		}
		if(document.getElementById('r_feas' + j)){
			if(document.getElementById('r_feas' + j).value!=""){
				if(document.getElementById('r_service'+j).value==2){
					var sample_size = document.getElementById('r_feas' + j).value;
					if(!(sample_size>=0)){
						document.getElementById('validation').innerHTML="The maximum feasibility can only be in numbers";
						return false;
					}
				}				
			}
		}
		if(document.getElementById('r_cpi' + j)){
			if(document.getElementById('r_cpi' + j).value!=""){
				var sample_size = document.getElementById('r_cpi' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The CPI can only be in numbers";
					return false;
				}
			}
		}
	}
	return true;
}

//Function to change account manager based on client
function getAccountManager(clientId){
	if(clientId==0){
		return;
	}
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeMgr; 
		var a=request.open("GET","getAccountManager?clientId=" + clientId + "&action=getAccountManager",true);
		request.send(true);	
 	}
}

function handleStateChangeMgr()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			if((request.responseText).length != 0)
			{		
					document.getElementById('accountManager').value = request.responseText;				
			}
			request.abort();
			request = null;
}

//Validation for document name
function documentValidation(fileName, id){
	var extIndex=fileName.lastIndexOf('\\');
	fileName = fileName.substring(extIndex+1);
	
	extIndex=fileName.lastIndexOf(".");
	var extension=fileName.substring(extIndex+1);
	id = id.substring(8);
	
	if(extension.length>4){
		filePass=false;
		document.getElementById('validation_document').innerHTML = "File extension cannot be longer than 4 characters";
		document.getElementById('document' + id).value="";
		return;
	}
	if(extension!="xls" && extension!="xlsx" && extension!="doc" && extension!="docx" && extension!="pdf"){
		filePass=false;
		document.getElementById('validation_document').innerHTML = "You can only upload a MS-Excel, MS-Word or a PDF document";		
		document.getElementById('document' + id).value="";
		return;
	}
	filePass=true;
	document.getElementById('validation_document').innerHTML = "";
}

function showStatusReason(bidStatus){
	document.getElementById('statusReasonDiv').style.display = 'none';
	if(bidStatus==2 || bidStatus==4){
		document.getElementById('statusReasonDiv').style.display = 'block';
	}
}
