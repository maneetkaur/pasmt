var sampleSum, feasibilitySum, totalSum=0, rowCost=0, currencyVal="", pass=true;

function populateValues(service, id){
	/*id = id.substring(9);
	if(service!=2 && service !=0){	
		document.getElementById('r_sample'+id).value = "NA";		
		document.getElementById('r_sample'+id).disabled = true;
	}
	else{
		document.getElementById('r_sample'+id).value = "";
		document.getElementById('r_sample'+id).disabled = false;
	}
	addSample();
	rowCostFun(id,3);*/
}

function editInitialize(){
	initialize();
	document.getElementById('startDate').value = startDateVal;
	document.getElementById('endDate').value = endDateVal;
}

function initialize(){
	currencyVal = document.getElementById('bid.billingCurrency').value;
	var service;
	if(reqSize==0){
		i=1;
	}
	else{
		i=reqSize;
	}
	//enter the currency value in all the fields
	for(var j=0; j<reqSize; j++){		
		if(document.getElementById('currencyVal'+j)){
			document.getElementById('currencyVal'+j).value = currencyVal;
			document.getElementById('currencyValF'+j).value = currencyVal;
			//update sample size and unit price
			service = document.getElementById('r_service'+j).value;
			if(service != 2 && service != 0 && service!="Sample"){
				document.getElementById('r_sample'+j).value = "NA";
				document.getElementById('r_sample'+j).disabled = true;
			}
		}		
	}
	document.getElementById('currPV').value=currencyVal;
	document.getElementById('currPM').value=currencyVal;
	
	addSample();
	editRowCost();totalCost();
}

function getBidInformation(bidId){
	window.location.href="addProposalForm?bidId="+bidId;
}

function addSample() {
	sampleSum = 0;
	for(var j=0; j<i; j++){
		if(document.getElementById('r_sample' + j)){
		  if(document.getElementById('r_service' + j).value == 2 || 
				  document.getElementById('r_service' + j).value == "Sample"){
			  if(document.getElementById('r_sample' + j).value!=""){
				sampleSum = sampleSum + (parseInt)(document.getElementById('r_sample' + j).value);
			  }
		  }
		  else{
			if(document.getElementById('r_service' + j).value != 0){
				if(document.getElementById('r_sample' + j).value==""){
					document.getElementById('r_sample' + j).value="NA";
				}
			}			
		  }
		}
	}
	document.getElementById('sampleSum').value=sampleSum;
}

/*function addFeasibility() {
	feasibilitySum = 0;
	for(var j=0; j<i; j++){		
		if(document.getElementById('r_feas' + j)){
		  if(document.getElementById('r_feas' + j).value!=""){
			feasibilitySum = feasibilitySum + (parseInt)(document.getElementById('r_feas' + j).value);
		  }
		}
	}
	document.getElementById('feasibilitySum').value=feasibilitySum;
}*/

function rowCostFun(id, from){
	rowCost="0.00";
	if(from==1){//coming from cpi
		id = id.substring(5);
	}
	else if(from==0){//coming from feasibility/Setup fee
		id= id.substring(6);
	}
	else if(from==2){//coming from sample size
		id=id.substring(8);
	}
	if(document.getElementById('r_feas' + id)){		
		var setup = document.getElementById('r_cpi' + id).value;
		var unitPrice = document.getElementById('r_feas' + id).value;
		if(setup!=""){
			document.getElementById('r_cpi' + id).value = parseFloat(setup).toFixed(2);
		}
		else{
			setup=0.0;
		}
		if(unitPrice!=""){
			document.getElementById('r_feas' + id).value = parseFloat(unitPrice).toFixed(2);
		}
		if(document.getElementById('r_service' + id).value ==2){			
			var sample = document.getElementById('r_sample' + id).value;
			
			if(sample !="" && unitPrice !=""){
				rowCost = parseFloat((sample * unitPrice)+ Number(setup)).toFixed(2);
			}
		}
		else {
			if(unitPrice!=""){
				rowCost = parseFloat(Number(unitPrice)+Number(setup)).toFixed(2);
			}
			else{
				rowCost = parseFloat(setup).toFixed(2);
			}
		}
	}
	document.getElementById('r_total' + id).value = currencyVal + " " + rowCost;
	totalCost();
}

function editRowCost(){
	rowCost="0.00";
	for(var j=0; j<i; j++){
		if(document.getElementById('r_feas' + j)){
			var setup = document.getElementById('r_cpi' + j).value;
			var unitPrice = document.getElementById('r_feas' + j).value;
			if(setup!=""){
				document.getElementById('r_cpi' + j).value = parseFloat(setup).toFixed(2);
				setup = parseFloat(setup).toFixed(2);
			}
			else{
				setup=0.0;
			}
			if(unitPrice!=""){
				document.getElementById('r_feas' + j).value = parseFloat(unitPrice).toFixed(2);
			}
			
			if(document.getElementById('r_service' + j).value ==2 ||
					document.getElementById('r_service' + j).value == "Sample"){
				var sample = document.getElementById('r_sample' + j).value;
											
				if(sample !="" && unitPrice !=""){
					rowCost = parseFloat((sample * unitPrice)+ Number(setup)).toFixed(2);
				}
			}
			else {
				if(unitPrice!=""){
					rowCost = parseFloat(Number(unitPrice)+Number(setup)).toFixed(2);
				}
				else{
					rowCost = parseFloat(setup).toFixed(2);
				}
			}
			document.getElementById('r_total' + j).value = currencyVal + " " + rowCost;
		}
	}
}

function totalCost() {
	totalSum = 0.00;
	for(var j=0; j<i; j++){	
		if(document.getElementById('r_total' + j)){
		  if(document.getElementById('r_total' + j).value!=""){
			  rowTotal = document.getElementById('r_total' + j).value;
			  if(currencyVal!=""){
				  rowTotal = rowTotal.substring(3);
			  }
			  totalSum = totalSum + parseFloat(rowTotal);			  
		  }
		}
	}
	totalSum = parseFloat(totalSum).toFixed(2);
	document.getElementById('totalSum').value= currencyVal + " "+ totalSum;
	var projectMinimum = document.getElementById('projectMinimum').value;
	if(projectMinimum==""){
		projectMinimum="0.00";
	}
	else{
		projectMinimum = parseFloat(projectMinimum).toFixed(2);
	}
	document.getElementById('projectMinimum').value = parseFloat(projectMinimum).toFixed(2);
	if(parseFloat(totalSum)<parseFloat(projectMinimum)){
		document.getElementById('projectValue').value = projectMinimum;
	}
	else{
		document.getElementById('projectValue').value = totalSum;
	}
}

//Validation for document name
function documentValidation(fileName, id){
	var extIndex=fileName.lastIndexOf('\\');
	fileName = fileName.substring(extIndex+1);
	
	extIndex=fileName.lastIndexOf(".");
	var extension=fileName.substring(extIndex+1);
	id = id.substring(8);
	
	if(extension.length>4){
		filePass=false;
		document.getElementById('validation_document').innerHTML = "File extension cannot be longer than 4 characters";
		document.getElementById('document' + id).value="";
		return;
	}
	if(extension!="xls" && extension!="xlsx" && extension!="doc" && extension!="docx" && extension!="pdf"){
		filePass=false;
		document.getElementById('validation_document').innerHTML = "You can only upload a MS-Excel, MS-Word or a PDF document";		
		document.getElementById('document' + id).value="";
		return;
	}
	filePass=true;
	document.getElementById('validation_document').innerHTML = "";
}

function proposalValidation(){
	pass=proposalValidationStatus();
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
	//return false;
}

function proposalValidationStatus(){
	pass=true;
	//Check all the mandatory fields
	if(document.getElementById('bid.bidCode').value=="0" || document.getElementById('clientContact').value=="0" ||
		document.getElementById('clientProjectManager').value=="0" || document.getElementById('purchaseOrderNo').value=="" || 
		document.getElementById('targetAudience').value=="" || document.getElementById('notesOnTargeting').value=="" || 
		document.getElementById('startDate').value=="" || document.getElementById('endDate').value=="" ||
		document.getElementById('projectMinimum').value=="" || document.getElementById('projectValue').value=="" ||
		document.getElementById('bid.projectSpecs').value==""){
		
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	
	//One requirement row is mandatory
	if(document.getElementById('r_country0').value=="0" || document.getElementById('r_service0').value=="0" ||
			document.getElementById('r_audience0').value=="0" || document.getElementById('r_desc0').value=="" || 
			document.getElementById('r_sample0').value=="" || document.getElementById('r_incidence0').value=="-1" || 
			document.getElementById('r_question0').value=="" || document.getElementById('r_loi0').value=="" ||
			document.getElementById('r_feas0').value=="" || document.getElementById('r_cpi0').value==""){
		
			document.getElementById('validation').innerHTML="Please enter atleast one requirement row";
			return false;
		}
	
	/*
	 * if one the fields in the row has values, then the complete row should have values
	 * Sample size to Quoted CPI should be positive integers
	 */	
	for(var j=0, count=0; j<i; j++){
		//if the first element of the row exists, the whole row exists
		if(document.getElementById('r_country' + j)){
			count=count+1;
			//if one of the value in the row exists then the whole row must have values
			if(document.getElementById('r_country' + j).value!="0" || document.getElementById('r_service'+j).value!="0" ||
				document.getElementById('r_audience'+j).value!="0" || document.getElementById('r_desc'+j).value!="" || 
				document.getElementById('r_sample'+j).value!="" || document.getElementById('r_incidence'+j).value!="-1" || 
				document.getElementById('r_question'+j).value!="" || document.getElementById('r_loi'+j).value!="" ||
				document.getElementById('r_feas'+j).value!="" || document.getElementById('r_cpi'+j).value!=""){
				
				if(document.getElementById('r_country'+j).value=="0" || document.getElementById('r_service'+j).value=="0" ||
					document.getElementById('r_audience'+j).value=="0" || document.getElementById('r_desc'+j).value=="" || 
					document.getElementById('r_sample'+j).value=="" || document.getElementById('r_incidence'+j).value=="-1" || 
					document.getElementById('r_question'+j).value=="" || document.getElementById('r_loi'+j).value=="" ||
					document.getElementById('r_feas'+j).value=="" || document.getElementById('r_cpi'+j).value==""){
						document.getElementById('validation').innerHTML="Please enter all the values in requirement row "
							+ count;
						return false;
					}
				}
		}
		
		if(document.getElementById('r_sample' + j)){
			if(document.getElementById('r_sample' + j).value!=""){
				if(document.getElementById('r_service'+j).value==2){
					var sample_size = document.getElementById('r_sample' + j).value;
					if(!(sample_size>=0)){
						document.getElementById('validation').innerHTML="The sample size can only be in numbers";
						return false;
					}
				}				
			}
		}
		if(document.getElementById('r_question' + j)){
			if(document.getElementById('r_question' + j).value!=""){
				var sample_size = document.getElementById('r_question' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The no. of questions can only be in numbers";
					return false;
				}
			}
		}
		if(document.getElementById('r_loi' + j)){
			if(document.getElementById('r_loi' + j).value!=""){
				var sample_size = document.getElementById('r_loi' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The LOI can only be in numbers";
					return false;
				}
			}
		}
		if(document.getElementById('r_feas' + j)){
			if(document.getElementById('r_feas' + j).value!=""){
				var sample_size = document.getElementById('r_feas' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The maximum feasibility can only be in numbers";
					return false;
				}
			}
		}
		if(document.getElementById('r_cpi' + j)){
			if(document.getElementById('r_cpi' + j).value!=""){
				var sample_size = document.getElementById('r_cpi' + j).value;
				if(!(sample_size>=0)){
					document.getElementById('validation').innerHTML="The CPI can only be in numbers";
					return false;
				}
			}
		}
	}
	//check that the end date should be greater than or equal to start date
	var startDate = document.getElementById('startDate').value;
	var endDate = document.getElementById('endDate').value;
	var yr, mon, date, endYr, endMon, endDate;
	yr= parseInt(startDate.substring(0,4));	
	mon = parseInt(startDate.substring(5,7));
	date = parseInt(startDate.substring(8,10));
	endYr= parseInt(endDate.substring(0,4));	
	endMon = parseInt(endDate.substring(5,7));
	endDate = parseInt(endDate.substring(8,10));
	if(endYr<yr){
		document.getElementById('validation').innerHTML="The end date should be higher than the start date";
		return false;
	}
	else if(endYr==yr){
		if(endMon<mon){
			document.getElementById('validation').innerHTML="The end date should be higher than the start date";
			return false;
		}
		else if(endMon==mon){
			if(endDate<date){
				document.getElementById('validation').innerHTML="The end date should be higher than the start date";
				return false;
			}
		}
	}
	yr= mon= date= endYr= endMon= endDate=null;
	
	return true;
}