function passwordValidationStatus(){
	var password = document.getElementById('password').value;
	var confirmPassword = document.getElementById('confirmPassword').value;
	
	if(document.getElementById('oldPassword').value == "" || password == "" || confirmPassword == ""){
		document.getElementById('validation').innerHTML="Please enter all the mandatory fields.";
		return false;
	}
	if(password != confirmPassword){
		document.getElementById('validation').innerHTML="Password and Confirm Password fields do not match.";
		return false;
	}
}

function passwordValidation(){
	var pass = true;
	pass=passwordValidationStatus();
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
}