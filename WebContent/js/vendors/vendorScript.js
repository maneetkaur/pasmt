var preName='', preCode='', preUrl='', valPass=true;
var pixelInstruction = '<b>Note:</b><br>Use #id# to pass the panelist id in the URL, if any. Refer the examples below: <br>' +
'&lt;iframe src="http://www.irbureau.com/pixel.jsp?transaction_id=#id#"&gt;&lt;/iframe&gt;  OR<br>' +
'&lt;img width="1" height="1" src="http://www.irbureau.com/pixel.jsp?transaction_id=#id#" /&gt;  OR<br>' +
'&lt;script type="text/javascript"&gt;[code goes here]&lt;/script&gt;';
var serverInstruction = '<b>Note:</b><br>Use #id1# to pass the panelist id in the URL. Refer the example below: <br>' +
'http://survey.irbureau.com/vendorEndPages/vendor.jsp?vid1=#id1#';

function validateVendorName(name){
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeName; 
		var a=request.open("GET","validateVendorName?vendorName=" + name + "&action=validateVendorName",true);
		request.send(true);	
 	}
}

function handleStateChangeName()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgVendorName').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
					document.getElementById('vendorName').value = preName;				
			}
			request.abort();
			request = null;
}

function validateVendorCode(code){
	
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeCode; 
		var a=request.open("GET","validateVendorCode?vendorCode=" + code + "&action=validateVendorCode",true);
		request.send(true);	
 	}
}

function handleStateChangeCode()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgVendorCode').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{
					document.getElementById('vendorCode').value = preCode;				
			}
			request.abort();
			request = null;
}

function validateWebUrl(webUrl){
	if (window.ActiveXObject) 
    {          
     	request = new ActiveXObject("Microsoft.XMLHTTP"); 
    } 
    else if (window.XMLHttpRequest) 
    {         
        request = new XMLHttpRequest();          
    } 
	if (request) 
	{
		request.onreadystatechange = handleStateChangeWeb; 
		var a=request.open("GET","validateVendorUrl?webUrl=" + webUrl + "&action=validateVendorUrl",true);
		request.send(true);	
 	}
}

function handleStateChangeWeb()
{	
			if (request.readyState != 4) return;
			if (request.status != 200) return;
			document.getElementById('msgWebUrl').innerHTML = request.responseText;
			if((request.responseText).length != 0)
			{									
					document.getElementById('websiteUrl').value = preUrl;				
			}
			request.abort();
			request = null;
}

function validateEmail(email){
	var email_value = document.getElementById(email).value;
	var atpos=email_value.indexOf("@");
	var dotpos=email_value.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email_value.length)
	{
	  return false;
	}
	return true;
}

function documentValidation(fileName,id){
	var extIndex=fileName.lastIndexOf('\\');
	fileName = fileName.substring(extIndex+1);
	
	extIndex=fileName.lastIndexOf(".");
	var extension=fileName.substring(extIndex+1);
	
	if(extension.length>4){
		filePass=false;
		document.getElementById('validation_document').innerHTML = "File extension cannot be longer than 4 characters";
		document.getElementById(id).value="";
		return;
	}
	if(extension!="doc" && extension!="docx" && extension!="xls" && extension!="xlsx" && extension!="pdf" && 
			extension!="jpeg" && extension!="jpg" && extension != "png"){
		filePass=false;
		document.getElementById('validation_document').innerHTML = "You can only upload a PDF, Word, Excel File" +
				" or a JPEG/PNG image";		
		document.getElementById(id).value="";
		return;
	}
	filePass=true;
	document.getElementById('validation_document').innerHTML = "";
}

function vendorValidation(from){
	pass=vendorValidationStatus(from);
	if(pass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation').innerHTML="";
	}
	return pass;
	//return false;
}

function vendorValidationStatus(from){
	//Check all the mandatory fields
	if(document.getElementById('vendorName').value=="" || document.getElementById('vendorCode').value=="" ||
		document.getElementById('vendorType').value=="0" || document.getElementById('accountManager').value=="0" || 
		document.getElementById('vendorStatus').value=="0" || document.getElementById('primaryContact.name').value=="" || 
		document.getElementById('primaryContact.jobTitle').value=="" || document.getElementById('primaryContact.email').value=="" 
		|| document.getElementById('primaryContact.phoneNo').value=="" || document.getElementById('address').value=="" || 
		document.getElementById('city').value=="" || document.getElementById('state').value=="" || 
		document.getElementById('postalCode').value=="" || document.getElementById('country').value=="0" || 
		document.getElementById('websiteUrl').value=="" || document.getElementById('phoneNo').value=="" || 		 		
		(from==0 && document.getElementById('vendorForm').value=="") ){
			document.getElementById('validation').innerHTML="Please enter all the mandatory fields. ";		
		return false;
	}
	
	//Check that the extension number should be a positive number
	if(document.getElementById('primaryContact.extno').value!=""){
		var extno = document.getElementById('primaryContact.extno').value;
		if(!(extno>=0)){
			document.getElementById('validation').innerHTML="The extension number can only be in numbers";			
			return false;
		}
	}
	
	//Check if email ids are in valid format
	if(document.getElementById('primaryContact.email').value != ""){
		if(!validateEmail('primaryContact.email')){
			document.getElementById('validation').innerHTML="The email id format of the primary contact is not valid.";
			return false;
		}
	}
	if(document.getElementById('rfqEmail').value != ""){
		if(!validateEmail('rfqEmail')){
			document.getElementById('validation').innerHTML="The email id format of the RFQ email is not valid.";
			return false;
		}
	}
	
	/*If none other validation has failed, loop and check for additional contacts
	 * For edit page, the names of existing contacts cannot be empty
	 */
	
	if(from==1){
		for(var k=0; k<rowNumber; k++){
			if(document.getElementById('add_name'+k).value==""){
				document.getElementById('validation').innerHTML="The names of existing additional contacts cannot be" +
						" empty.";
				/*document.getElementById('validation_billing').innerHTML="The names of existing additional contacts" +
						" cannot be empty.";*/
				return false;
			}
		}
	}
	
	for(var j=0; j<i; j++){
		if(document.getElementById('add_name'+j)!=null){
			if(document.getElementById('add_name'+j).value!=""){
				if(document.getElementById('add_job'+j).value=="" || 
						document.getElementById('add_email'+j).value==""){
					document.getElementById('validation').innerHTML="Please enter the job title and email of the" +
							" additional contacts.";					
					return false;
				}
				else if(document.getElementById('add_ext'+j).value!=""){
					var extno = document.getElementById('add_ext'+j).value;
					if(!(extno>=0)){
						document.getElementById('validation').innerHTML="The extension number can only be in numbers";						
						return false;
					}
				}
				if(document.getElementById('add_email'+j).value != ""){
					if(!validateEmail('add_email'+j)){
						document.getElementById('validation').innerHTML="The email id format of the additional contact" + 
						" is not valid.";						
						return false;
					}
				}
			 }
		  }
		}
	
	return true;
	
}

function openDivRecruit(trackingTypeVal, divName) {
	document.getElementById('tracking' + divName + 'Div').style.display='none';
	document.getElementById('variableDiv').style.display='none';
	if(divName!=""){
		document.getElementById('tracking' + divName + 'UrlDiv').style.display='none';
	}
	if(trackingTypeVal==2){
		document.getElementById('tracking' + divName + 'Div').style.display='none';
		document.getElementById('tracking' + divName + 'UrlDiv').style.display='block';
		document.getElementById('variableDiv').style.display='block';
		//urlInstructionOsbt
	}
	else if(trackingTypeVal==3){
		document.getElementById('tracking' + divName + 'Div').style.display='block';
		document.getElementById('tracking' + divName + 'Span').innerHTML = 'Pixel Code';
		document.getElementById('codeInstruction' + divName).innerHTML = pixelInstruction;
	}
	else if(trackingTypeVal==4){
		document.getElementById('tracking' + divName + 'Div').style.display='block';
		document.getElementById('tracking' + divName + 'Span').innerHTML = 'Server to Server URL';
		document.getElementById('codeInstruction' + divName).innerHTML = serverInstruction;
	}
}

function openRoutingFields(routingVal, rDivName) {
	if(routingVal==1){
		document.getElementById('maxRouted' + rDivName + 'Row').style.display='block';
		document.getElementById('maxQualified' + rDivName + 'Row').style.display='block';
	}
	else{
		document.getElementById('maxRouted' + rDivName + 'Row').style.display='none';
		document.getElementById('maxQualified' + rDivName + 'Row').style.display='none';
	}
}

function initialize(){
	//For recruitment tracking
	var val = document.forms["recruitForm"];
	var trackingTypeVal = val.elements["trackingType"].value;
	if(trackingTypeVal==3){
		document.getElementById('trackingDiv').style.display='block';
		document.getElementById('trackingSpan').innerHTML = 'Pixel Code';
		document.getElementById('codeInstruction').innerHTML = pixelInstruction;
	}
	else if(trackingTypeVal==4){
		document.getElementById('trackingDiv').style.display='block';
		document.getElementById('trackingSpan').innerHTML = 'Server to Server URL';
		document.getElementById('codeInstruction').innerHTML = serverInstruction;
	}
	
	//For OSBT tracking
	val = document.forms["osbtForm"];
	trackingTypeVal = val.elements["trackingType"].value;
	if(trackingTypeVal==2){
		document.getElementById('trackingOsbtUrlDiv').style.display='block';
	}
	else if(trackingTypeVal==3){
		document.getElementById('trackingOsbtDiv').style.display='block';
		document.getElementById('trackingOsbtSpan').innerHTML = 'Pixel Code';
		document.getElementById('codeInstructionOsbt').innerHTML = pixelInstruction;
	}
	else if(trackingTypeVal==4){
		document.getElementById('trackingOsbtDiv').style.display='block';
		document.getElementById('trackingOsbtSpan').innerHTML = 'Server to Server URL';
		document.getElementById('codeInstructionOsbt').innerHTML = serverInstruction;
	}
	var routingVal = val.elements["routingAllowed"].value;
	if(routingVal==1){
		document.getElementById('maxRoutedRow').style.display='block';
		document.getElementById('maxQualifiedRow').style.display='block';
	}
	
	//For Survey Tracking
	val = document.forms["surveyForm"];
	trackingTypeVal = val.elements["trackingType"].value;
	if(trackingTypeVal==2){
		document.getElementById('trackingSurveyUrlDiv').style.display='block';
		document.getElementById('variableDiv').style.display='block';
	}
	else if(trackingTypeVal==3){
		document.getElementById('trackingSurveyDiv').style.display='block';
		document.getElementById('trackingSurveySpan').innerHTML = 'Pixel Code';		
		document.getElementById('codeInstructionSurvey').innerHTML = pixelInstruction;
	}
	else if(trackingTypeVal==4){
		document.getElementById('trackingSurveyDiv').style.display='block';
		document.getElementById('trackingSurveySpan').innerHTML = 'Server to Server URL';
		document.getElementById('codeInstructionSurvey').innerHTML = serverInstruction;
	}
	
	routingVal = val.elements["routingAllowed"].value;
	if(routingVal==1){
		document.getElementById('maxRoutedSurveyRow').style.display='block';
		document.getElementById('maxQualifiedSurveyRow').style.display='block';
	}
}

function billingValidation(){
	valPass = true;
	if(document.getElementById('paymentType').value=="0" || document.getElementById('billingCurrency').value=="0" ||
		document.getElementById('paymentMode').value=="0" || document.getElementById('accountRecEmail').value==""){
			valPass = false;
			document.getElementById('validation_billing').innerHTML="Please enter all the mandatory fields. ";
	}
	if(valPass && document.getElementById('paypalEmail').value!="" && !validateEmail('paypalEmail')){
		document.getElementById('validation_billing').innerHTML="The email id format of the paypal email is not valid.";
		valPass = false;
	}
	if(valPass && document.getElementById('accountRecEmail').value!="" && !validateEmail('accountRecEmail')){
		document.getElementById('validation_billing').innerHTML = "The email id format of the account receivable" +
				" email is not valid.";
		valPass = false;
	}
	
	if(valPass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation_billing').innerHTML="";
	}
	return valPass;
}

//Validation for recruitment tracking
function recruitValidation(){
	valPass=recruitValidationStatus();
	if(valPass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation_recruitment').innerHTML="";
	}
	return valPass;
}
function recruitValidationStatus() {
	val = document.forms["recruitForm"];
	if(val.elements['trackingType'].value=="0" || val.elements['cpa'].value=="" ||
		val.elements['variables[0].variableName'].value=="" || val.elements['successPage'].value=="0"){
			document.getElementById('validation_recruitment').innerHTML="Please enter all the mandatory fields. ";
			return false;
	}
	if((val.elements['trackingType'].value=="3" || val.elements['trackingType'].value=="4") &&
			val.elements['urlCode'].value == ""){
		document.getElementById('validation_recruitment').innerHTML="Please enter all the mandatory fields. ";
		return false;
	}
	//Check that the CPA should be a positive number
	if(val.elements['cpa'].value!=""){
		var cpa = val.elements['cpa'].value;
		if(!(cpa>=0)){
			document.getElementById('validation_recruitment').innerHTML="The CPA can only be in numbers";
			return false;
		}
	}
	return true;
}

//Validation for OSBT tracking
function osbtValidation(){
	valPass=osbtValidationStatus();
	if(valPass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation_osbt').innerHTML="";
	}
	return valPass;
}
function osbtValidationStatus() {
	val = document.forms["osbtForm"];
	if(val.elements['trackingType'].value=="0" || val.elements['cpa'].value=="" ||
		val.elements['variables[0].variableName'].value==""){
			document.getElementById('validation_osbt').innerHTML="Please enter all the mandatory fields. ";
			return false;
	}
	if((val.elements['trackingType'].value=="3" || val.elements['trackingType'].value=="4") &&
			val.elements['urlCode'].value == ""){
		document.getElementById('validation_osbt').innerHTML="Please enter all the mandatory fields. ";
		return false;
	}
	if(val.elements['trackingType'].value=="2"){
		if(val.elements['completeUrl'].value=="" || val.elements['terminateUrl'].value=="" ||
			val.elements['quotaFullUrl'].value=="" || val.elements['securityTerminateUrl'].value=="" ||
			val.elements['errorsUrl'].value==""){
				document.getElementById('validation_osbt').innerHTML="Please enter all the mandatory fields. ";
				return false;
		}
	}
	//Check that the CPA should be a positive number
	var tempVal;
	tempVal = val.elements['cpa'].value;
	if(tempVal != ""){
		if(!(tempVal >= 0)){
			document.getElementById('validation_osbt').innerHTML="The CPA can only be in numbers";
			return false;
		}
	}
	//for max routed
	var routingAllVal = val.elements['routingAllowed'].value;
	if(routingAllVal=="1"){
		tempVal = val.elements['maxRouted'].value;
		if(tempVal != ""){
			if(!(tempVal >= 0)){
				document.getElementById('validation_osbt').innerHTML="Max no. of routed surveys per respondent" +
						" can only be in numbers";
				return false;
			}
		}
		tempVal = val.elements['maxQualified'].value;
		if(tempVal != ""){
			if(!(tempVal >= 0)){
				document.getElementById('validation_osbt').innerHTML="Max no. of qualified surveys per respondent" +
						" can only be in numbers";
				return false;
			}
		}
	}
	tempVal = val.elements['minConversion'].value;
	if(tempVal != ""){
		if(!(tempVal >= 0)){
			document.getElementById('validation_osbt').innerHTML="Minimum Conversion for routing can only be in " +
					"numbers";
			return false;
		}
	}
	tempVal = val.elements['maxLoi'].value;
	if(tempVal != ""){
		if(!(tempVal >= 0)){
			document.getElementById('validation_osbt').innerHTML="Maximum LOI for routing can only be in numbers";
			return false;
		}
	}
	return true;
}

//Validation for Survey tracking
function surveyValidation(){
	valPass=surveyValidationStatus();
	if(valPass==false){
		window.scrollTo(0, 0);
	}
	else{
		document.getElementById('validation_surveys').innerHTML="";
	}
	return valPass;
}
function surveyValidationStatus() {
	val = document.forms["surveyForm"];
	if(val.elements['trackingType'].value=="0" || val.elements['variables[0].variableName'].value==""){
			document.getElementById('validation_surveys').innerHTML="Please enter all the mandatory fields. ";
			return false;
	}
	if((val.elements['trackingType'].value=="3" || val.elements['trackingType'].value=="4") &&
			val.elements['urlCode'].value == ""){
		document.getElementById('validation_surveys').innerHTML="Please enter all the mandatory fields. ";
		return false;
	}
	if(val.elements['trackingType'].value=="2"){
		if(val.elements['completeUrl'].value=="" || val.elements['terminateUrl'].value=="" ||
			val.elements['quotaFullUrl'].value=="" || val.elements['securityTerminateUrl'].value=="" ||
			val.elements['errorsUrl'].value==""){
				document.getElementById('validation_surveys').innerHTML="Please enter all the mandatory fields. ";
				return false;
		}
	}
	//Check that the fields should be a positive number
	var tempVal;
	//for max routed
	var routingAllVal = val.elements['routingAllowed'].value;
	if(routingAllVal=="1"){
		tempVal = val.elements['maxRouted'].value;
		if(tempVal != ""){
			if(!(tempVal >= 0)){
				document.getElementById('validation_surveys').innerHTML="Max no. of routed surveys per respondent" +
						" can only be in numbers";
				return false;
			}
		}
		tempVal = val.elements['maxQualified'].value;
		if(tempVal != ""){
			if(!(tempVal >= 0)){
				document.getElementById('validation_surveys').innerHTML="Max no. of qualified surveys per respondent" +
						" can only be in numbers";
				return false;
			}
		}
		tempVal = val.elements['cpc'].value;
		if(tempVal != ""){
			if(!(tempVal >= 0)){
				document.getElementById('validation_surveys').innerHTML="CPC for routed completes can only be" +
						" in numbers";
				return false;
			}
		}
	}
	tempVal = val.elements['minConversion'].value;
	if(tempVal != ""){
		if(!(tempVal >= 0)){
			document.getElementById('validation_surveys').innerHTML="Minimum Conversion for routing can only be in " +
					"numbers";
			return false;
		}
	}
	tempVal = val.elements['maxLoi'].value;
	if(tempVal != ""){
		if(!(tempVal >= 0)){
			document.getElementById('validation_surveys').innerHTML="Maximum LOI for routing can only be in numbers";
			return false;
		}
	}
	return true;
}