<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Pasmt</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="pasmt" content="Codrops" />
<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="css/style.css" type="text/css" />
<script type="text/javascript">
	function loginValidation() {
		var pass=true;
		var uname=document.getElementById('username').value;
		var passwd=document.getElementById('password').value;
		if(uname==""){
			
			document.getElementById('validation').innerHTML="Please enter username";
			pass=false;	
		}
		else if(passwd==""){
			document.getElementById('validation').innerHTML="Please enter password";
			pass=false;	
		}
		return pass;
	}
	
</script>


</head>
<body>
<!--Login Start -->
<div class="loginbox">
  <div class="roundbox">
    <table  style="margin-left:40px; margin-top:5px;"width="431" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="107" height="55" align="left" valign="middle"><img src="images/logo.jpg" width="87" height="45"></td>
        <td width="324" align="left" valign="middle"><strong>PANEL AND SURVEY MANAGEMENT TOOL</strong><br>
          <span class="grytxt">PASMT Version 2.0      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered by IRB</span></td>
      </tr>
    </table>
  </div>
  <div class="formbox">
  	<form method="POST" action="<c:url value="/j_spring_security_check" />">
	    <table width="392" border="0" align="right" cellpadding="0" cellspacing="0">
	      <tr>
	        <td>&nbsp;</td>
	      </tr>
	      <tr>
	        <td>&nbsp;</td>
	      </tr>
	      
	      <tr>
	      	<td colspan="3" >
	      		<span id="validation" style="color:red;">&nbsp;
	      			<c:if test="${not empty param.error}">
	      				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}					  
					</c:if>
	      		</span>
	      	</td>
	      </tr>
	      <tr>
	        <td height="42" align="left" valign="middle">	        	
	        	<input type="text" id="username" name="j_username" class="inptbg" placeholder="ENTER USERNAME">
	        </td>
	      </tr>
	      <tr>
	        <td height="38" align="left" valign="top">	        	
	        	<input type="password" id="password" name="j_password" class="inptbg" placeholder="PASSWORD">
	        </td>
	      </tr>
	      <tr>
	        <td><table width="287" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td width="189" align="left" valign="middle"><a href="#" class="forgttlink">Forgot Password?</a></td>
	              <td width="80" align="left" valign="middle"><input type="submit" name="Submit" id="Submit" value="Submit" class="btnsubmit" onclick="return loginValidation();">
	              </td>
	            </tr>
	          </table></td>
	      </tr>
	    </table>
    </form>
  </div>
</div>
<!--Login Start -->
</body>
<!-- sweet captch -->
<script type="text/javascript">
var sweetcaptcha = new require('sweetcaptcha')(133951, 1e9ab662e3841a7588f2a951c1cedadf, d51a923b49c2da995d0e42c88f0da2d3); // your sweetCaptcha application credentials, see https://sweetcatpcha.com/accounts/signin

// Get your captcha

function getCaptcha(callback) {

  sweetcaptcha.api('get_html', callback); // callback should be function(err, html) {...}

}


// Validate using sckey and scvalue, hidden inputs from previous HTML

function validateCaptcha(captchaKey, captchaValue, callback) {

  // callback = function(err, isValid) {...}
  sweetcaptcha.api('check', {sckey: captchaKey, scvalue: captchaValue}, function(err, response){
    if (err) return callback(err);

    if (response === 'true') {
      // valid captcha
      return callback(null, true); 
    }

    // invalid captcha
    callback(null, false);

  });
  
}
</script>
</html>